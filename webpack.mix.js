const { mix } = require('laravel-mix');

const stylus = require('stylus');
const nib = require('nib');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin')
var FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
//  */

mix.options({
  uglify: false, 
});


var webpackConfig = {
  // ...
	plugins: [
		new FriendlyErrorsWebpackPlugin(),
    	new UglifyJSPlugin()
	],
}

mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

mix.js('resources/assets/js/dashboard/properties.js', 'public/js/dashboard-properties.js').version();

mix.js('resources/assets/js/dashboard/create_property.js', 'public/js/dashboard-create-property.js').version();

mix.js('resources/assets/js/dashboard/users.js', 'public/js/dashboard-users.js').version();



mix.stylus(
		'./resources/assets/stylus/main.styl', 
		'./public/assets/css/main.css', 
		{
			use: nib()
		}
	);

mix.stylus(
		'./resources/assets/stylus/excel/main.styl', 
		'./public/assets/css/excel.css', 
		{
			use: nib()
		}
	);

mix.js('./resources/assets/js/sections/main.js', 
	   './public/assets/js/main.js').version();

mix.js('./resources/assets/js/sections/property.js', 
	   './public/assets/js/property.js').version();

mix.js('./resources/assets/js/sections/mi-inversion.js', 
	   './public/assets/js/mi-inversion.js').version();

mix.js('./resources/assets/js/sections/mis-inversiones.js', 
	   './public/assets/js/mis-inversiones.js').version();

mix.js('./resources/assets/js/sections/loginRegister.js', 
	   './public/assets/js/login-register.js').version();

mix.js('./resources/assets/js/sections/cuentaBancaria.js', 
	   './public/assets/js/cuenta-bancaria.js').version();

mix.js('./resources/assets/js/sections/mi-mercado.js', 
	   './public/assets/js/mi-mercado.js').version();

mix.js('./resources/assets/js/sections/marketplace.js', 
	   './public/assets/js/marketplace.js').version();

mix.js('./resources/assets/js/sections/mis-movimientos.js', 
	   './public/assets/js/mis-movimientos.js').version();


// mix.browserSync({
//       open: 'external',
//       host: 'crowdestate.on',
//       proxy: 'localhost',
//       port: 80 // for work mamp
// });*/