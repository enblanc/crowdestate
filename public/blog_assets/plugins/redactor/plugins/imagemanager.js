/**
 *
 * @title ImageManager redactor plugin
 * @author Eric Lagarda
 * @date 29-03-2016
 *
 */

(function($)
{
    $.Redactor.prototype.imagemanager = function()
    {
        return {
            init: function()
            {

                if (!this.opts.imageManagerUrl){
                    return;
                }

                var button = this.button.add('imagemanager', 'ImageManager');
                this.button.addCallback(button, this.imagemanager.openDialog);
            },
            openDialog: function(rule, type)
            {
                $.fancybox({
                    width		: 950,
                    height	    : 500,
                    type		: 'iframe',
                    href        : this.opts.imageManagerUrl,
                    fitToView   : false,
                    autoScale   : false,
                    autoSize    : false
                });
            },
            set: function(data)
            {
                var name = data.name.slice(0, -4);
                var img = $('<img src="' + data.path + '"  alt="'+ name +'" title="'+ name +'" />');
                var current = this.selection.getBlock() || this.selection.getCurrent();

                if (current) $(current).after(img);
                else
                {
                    this.insert.html(img);
                }

                this.code.sync();
                $.fancybox.close();
            }
        };
    };
})(jQuery);