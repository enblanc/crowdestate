 // make the delete button work in the first result page
register_delete_button_action();

// make the delete button work on subsequent result pages
$('#datatable').on( 'draw.dt',   function () {
    register_delete_button_action();
} ).dataTable();

function register_delete_button_action() {
    $("[data-button-type=delete]").unbind('click');
    // CRUD Delete
    // ask for confirmation before deleting an item
    $("[data-button-type=delete]").click(function(e) {
        e.preventDefault();
        var delete_button = $(this);
        var delete_url = $(this).attr('href');
        
        swal({  title: "Estas seguro de querer borrar este registro?",
                text: "El registro será borrado de la Base de Datos",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Si, borrar!",
                cancelButtonText: "Cancelar",
                closeOnConfirm: true
            }, function(isConfirm){
                if (isConfirm) {

                    $.ajax({
                        url: delete_url,
                        beforeSend: function (request){
                            request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
                        },
                        type: 'DELETE',
                        success: function(result) {
                            // Show an alert with the result
                            new PNotify({
                                title: "Registro Borrado",
                                text: "Registro borrado correctamente.",
                                type: "info"
                            });
                            // delete the row from the table
                            delete_button.parentsUntil('tr').parent().remove();
                        },
                        error: function(result) {
                            // Show an alert with the result
                            new PNotify({
                                title: "No Borrado",
                                text: "Existió algún error y su registro no pudo ser borrado.",
                                type: "danger"
                            });
                        }
                    });

                }
            });

    });
}