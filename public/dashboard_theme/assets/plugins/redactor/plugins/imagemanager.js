/**
 *
 * @title ImageManager redactor plugin
 * @author Eric Lagarda
 * @date 29-03-2016
 *
 */

(function($)
{
    $.Redactor.prototype.imagemanager = function()
    {
        return {
            langs: {
                en: {
                    "imagemanager": "Insert image",
                },
                es: {
                    "imagemanager": "Insertar imagen",
                }
            },
            init: function()
            {

                if (!this.opts.imageManagerUrl){
                    return;
                }

                if(!jQuery().fancybox) {
                    return;
                }

                var button = this.button.add('imagemanager', this.lang.get('imagemanager'));
                this.button.setIcon(button, '<i class="re-icon-image"></i>');
                this.button.addCallback(button, this.imagemanager.openDialog);
            },
            openDialog: function(rule, type)
            {

                $.fancybox({
                    width		: 950,
                    height	    : 500,
                    type		: 'iframe',
                    href        : this.opts.imageManagerUrl,
                    fitToView   : false,
                    autoScale   : false,
                    autoSize    : false
                });
            },
            set: function(data)
            {
                var name = data.name.slice(0, -4);
                var img = $('<img src="' + data.path + '"  alt="'+ name +'" title="'+ name +'" />');
                var current = this.selection.block() || this.selection.current();
                console.log(img);
                if (current) $(current).after(img);
                else
                {
                    console.log(img);
                    var html = this.code.get();
                    console.log(html);
                        // buffer
                    this.buffer.set();

                    // insert
                    this.air.collapsed();
                    this.insert.html(img);
                }

                this.code.sync();
                $.fancybox.close();
            }
        };
    };
})(jQuery);