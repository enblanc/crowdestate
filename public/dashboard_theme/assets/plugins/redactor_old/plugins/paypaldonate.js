/**
 *
 * @title Paypal Donate Button redactor plugin
 * @author Eric Lagarda
 * @date 29-03-2016
 *
 */

(function($)
{
    $.Redactor.prototype.paypaldonate = function()
    {
        return {
            reUrlYoutube: /https?:\/\/(?:[0-9A-Z-]+\.)?(?:youtu\.be\/|youtube\.com\S*[^\w\-\s])([\w\-]{11})(?=[^\w\-]|$)(?![?=&+%\w.-]*(?:['"][^<>]*>|<\/a>))[?=&+%\w.-]*/ig,
            reUrlVimeo: /https?:\/\/(www\.)?vimeo.com\/(\d+)($|\/)/,
            getTemplate: function()
            {
                return String()
                    + '<section id="redactor-modal-paypaldonate-insert">'
                    + '<label>' + this.lang.get('paypal_text') + '</label>'
                    + '<input id="redactor-insert-paypal-text" class="form-control"></input>'
                    + '<label>' + this.lang.get('paypal_email') + '</label>'
                    + '<input id="redactor-insert-paypal-email" class="form-control"></input>'
                    + '<label>' + this.lang.get('paypal_country') + '</label>'
                    + '<select id="redactor-insert-paypal-country" class="form-control">'
                    + '<option value="ES">España</option>'
                    + '<option value="US">United States</option>'
                    + '</select>'
                    + '<label>' + this.lang.get('paypal_currency') + '</label>'
                    + '<select id="redactor-insert-paypal-currency" class="form-control">'
                        + '<option value="EUR">EUR</option>'
                        + '<option value="USD">USD</option>'
                    + '</select>'
                    + '</section>';
            },
            init: function()
            {
                var button = this.button.add('paypaldonate', 'PayPal Donate', this.lang.get('paypal'));
                this.button.addCallback(button, this.paypaldonate.show);
            },
            show: function()
            {
                this.modal.addTemplate('paypaldonate', this.paypaldonate.getTemplate());

                this.modal.load('paypaldonate', this.lang.get('paypal'), 700);
                this.modal.createCancelButton();

                var button = this.modal.createActionButton(this.lang.get('insert'));
                button.on('click', this.paypaldonate.insert);

                this.selection.save();
                this.modal.show();

                $('#redactor-insert-paypal-text').focus();

            },
            insert: function()
            {
                var text = $('#redactor-insert-paypal-text').val();
                var email = $('#redactor-insert-paypal-email').val();
                var country = $('#redactor-insert-paypal-country').val();
                var currency = $('#redactor-insert-paypal-currency').val();

                var form ='<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">' +
                                '<input type="hidden" name="cmd" value="_donations">' +
                                '<input type="hidden" name="business" value="'+ email +'">' +
                                '<input type="hidden" name="lc" value="'+ country +'">' +
                                '<input type="hidden" name="item_name" value="'+ text +'">' +
                                '<input type="hidden" name="no_note" value="0"> ' +
                                '<input type="hidden" name="currency_code" value="'+ currency +'">' +
                                '<input type="hidden" name="bn" value="PP-DonationsBF:btn_donate_LG.gif:NonHostedGuest">' +
                                '<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donate_LG.gif" border="0" name="submit" alt="PayPal">' +
                                '<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">' +
                            '</form>';


                this.selection.restore();
                this.modal.close();

                var current = this.selection.getBlock() || this.selection.getCurrent();

                if (current) $(current).after(form);
                else
                {
                    this.insert.html(form);
                }

                this.code.sync();
            }

        };
    };
})(jQuery);