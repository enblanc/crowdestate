'use strict';
var Admin = function() {
    var checkFullScreen = function() {
        var fullScreen = sessionStorage.getItem("fullscreen");
        if (fullScreen == "true") {
            $('.navbar-fullscreen').click();
        }
    }
    var runFullscreenDemo = function() {
            // If browser is IE we need to pass the fullsreen plugin the 'html' selector
            // rather than the 'body' selector. Fixes a fullscreen overflow bug
            var selector = $('html');
            var ua = window.navigator.userAgent;
            var old_ie = ua.indexOf('MSIE ');
            var new_ie = ua.indexOf('Trident/');
            if ((old_ie > -1) || (new_ie > -1)) {
                selector = $('body');
            }
            // Fullscreen Functionality
            var screenCheck = $.fullscreen.isNativelySupported();
            // Attach handler to navbar fullscreen button
            $('.navbar-fullscreen').on('click', function() {
                // Check for fullscreen browser support
                if (screenCheck) {
                    if ($.fullscreen.isFullScreen()) {
                        $.fullscreen.exit();
                        sessionStorage.setItem("fullscreen", false);
                    } else {
                        selector.fullscreen({
                            overflow: 'auto'
                        });
                        sessionStorage.setItem("fullscreen", true);
                    }
                } else {
                    alert('Your browser does not support fullscreen mode.')
                }
            });
        }
        //setItemsActive();
        /**
         * Funcion que se encarga de activar los menús dependiendo la url en la que este
         */
    var setItemsActive = function() {
        var path = window.location.href;
        path = path.replace(/\/$/, "");
        path = decodeURIComponent(path);
        $(".sidebar-menu li > a").each(function() {
            var href = $(this).attr('href');
            // if (path.substring(0, href.length) === href) {
            if (href == path) {
                $(".sidebar-menu").find("li").removeClass('active');
                $(this).closest('li').addClass('active');
                var items = $(this).parentsUntil('.sidebar-menu > li > a');
                // console.log(items);
                items.addClass('active')
                $.each(items, function(key, value) {
                    // console.log(value);
                    if ($(this).prev().hasClass('accordion-toggle')) {
                        $(this).prev().addClass('menu-open').css('display', 'block');
                    }
                });
                //$(this).parentsUntil('.menu-items > li').find('.sub-menu').closest('li').addClass('active');
                //                if($(this).closest('.sub-menu').length > 0){
                //                    $(this).closest('.sub-menu').closest('li').addClass('active');
                //                }
            }
        });
    }
    return {
        init: function() {
            checkFullScreen();
            runFullscreenDemo();
            setItemsActive();
        }
    }
}();