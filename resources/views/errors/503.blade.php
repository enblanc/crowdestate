@extends('front/layouts/general')

@section('content')
    
    <section >
        <div class="wrapper mb-80">
            <div class="xs-4 col center  text-center  mb-80">
                @include('front/svg/crowdestate', ['class' => 'mt-80 mb-16'])

                <h2 class="mb-32">{{ __('La página web está en mantenimiento') }}.<br /></h2>

                <p>{{ __('Por favor, prueba en unos instantes') }}.</p>

            </div>
        </div>
    </section>

@endsection

@section('custom_plugin_js')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="/assets/js/plugins/parsleyjs.min.js"></script>
@endsection

@section('custom_section_js')
    <script src="{{ mix('assets/js/main.js') }}"></script>
@endsection 