@extends('front/layouts/general')

@section('content')
    
    <section>
        <div class="wrapper mb-80">
            <div class="xs-4 col center  text-center  mb-80">
                @include('front/svg/crowdestate', ['class' => 'mt-48 mb-16'])

                <h2 class="mb-32">{{ __('Lo sentimos') }}. <br>
                {{ __('Esta página no está disponible')}}.</h2>

                <p>{{ __('La página que buscas no está disponible o la url es incorrecta') }}. <br>
                {{ __('Puedes ir a la') }} <a href="{{ url('/') }}">{{ __('página de inicio') }}</a> {{ __('o puedes') }}:</p>

                <a href="{{ route('front.property.grid') }}" class="button  mt-40">{{ __('Ver los inmuebles') }}</a>

            </div>
        </div>
    </section>

@endsection

@section('custom_plugin_js')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="/assets/js/plugins/parsleyjs.min.js"></script>
@endsection

@section('custom_section_js')
    <script src="{{ mix('assets/js/main.js') }}"></script>
@endsection 