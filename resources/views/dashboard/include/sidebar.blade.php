<aside id="sidebar_left" class="nano nano-light affix">

    <!-- / Sidebar Left Wrapper  / -->
    <div class="sidebar-left-content nano-content">

        <!-- / Sidebar Header / -->

        <!-- / Sidebar Menu  / -->
        <ul class="nav sidebar-menu">

            <li class="sidebar-label pt30">Menú de Administración</li>

            <!--  CONFIG -->
            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-dashboard"></span>
                    <span class="sidebar-title">Configuraciones</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ url('dashboard/users') }}">
                            <span class="fa fa-file-text-o"></span> Usuarios </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.certifications.index') }}">
                            <span class="fa fa-file-text-o"></span> Certificaciones </a>
                    </li>
                    @role('super')
                        <li>
                            <a href="{{ url('dashboard/pages') }}">
                                <span class="fa fa-file-text-o"></span> Páginas </a>
                        </li>
                    @endrole
                </ul>
            </li>

            <li class="hide">
                <a href="{{ url('dashboard/translations') }}">
                    <span class="fa fa-language"></span>
                    <span class="sidebar-title">Traducciones</span>
                </a>
            </li>
            @role('super')
                <li>
                    <a href="{{ route('configmanager.index') }}">
                        <span class="fa fa-cog"></span>
                        <span class="sidebar-title"> {{ trans('configmanager.title') }}</span>
                    </a>
                </li>
            @endrole

            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-dashboard"></span>
                    <span class="sidebar-title">Inmuebles</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ route('inmuebles.index') }}">
                            <span class="fa fa-file-text-o"></span> Ver inmuebles </a>
                    </li>
                    <li>
                        <a href="{{ route('inmuebles.create') }}">
                            <span class="fa fa-file-text-o"></span> Crear inmueble </a>
                    </li>
                </ul>
            </li>

            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-dashboard"></span>
                    <span class="sidebar-title">Inmuebles Extras</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ route('developer.index') }}">
                            <span class="fa fa-handshake-o"></span> Promotores </a>
                    </li>
                    <li>
                        <a href="{{ route('property-state.index') }}">
                            <span class="fa fa-wrench"></span> Estados </a>
                    </li>
                    <li>
                        <a href="{{ route('property-type.index') }}">
                            <span class="fa fa-building"></span> Tipos de Inmueble </a>
                    </li>
                    <li>
                        <a href="{{ route('property-investment.index') }}">
                            <span class="fa fa-balance-scale"></span> Tipos de Inversión </a>
                    </li>
                </ul>
            </li>

            <li>
                <a href="{{ route('dashboard.marketplace') }}">
                    <span class="fa fa-shopping-basket" aria-hidden="true"></span>
                    <span class="sidebar-title">Marketplace</span>
                </a>
            </li>

            <li>
                <a href="{{ route('dashboard.pagos.index') }}">
                    <span class="fa fa-credit-card"></span>
                    <span class="sidebar-title">Realizar pagos</span>
                </a>
            </li>

            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-comments"></span>
                    <span class="sidebar-title">Blog</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ route('dashboard.blog.index') }}">
                            <span class="fa fa-file-text-o"></span> Ver entradas </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.blog.create') }}">
                            <span class="fa fa-file-text-o"></span> Crear entrada </a>
                    </li>
                </ul>
            </li>

            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-dashboard"></span>
                    <span class="sidebar-title">Páginas</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ url('dashboard/pages/3/edit') }}">
                            <span class="fa fa-file-text-o"></span> Inicio </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/pages/1/edit') }}">
                            <span class="fa fa-file-text-o"></span> Quienes somos </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/pages/2/edit') }}">
                            <span class="fa fa-file-text-o"></span> Como funciona </a>
                    </li>
                    <li>
                        <a href="{{ url('dashboard/pages/4/edit') }}">
                            <span class="fa fa-file-text-o"></span> Información legal </a>
                    </li>
                </ul>
            </li>

            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-dashboard"></span>
                    <span class="sidebar-title">Autoinvests</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ route('dashboard.autoinvests.index') }}">
                            <span class="fa fa-file-text-o"></span> Inicio </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.autoinvests.orders') }}">
                            <span class="fa fa-file-text-o"></span> Ordenes </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.autoinvests.events') }}">
                            <span class="fa fa-file-text-o"></span> Eventos </a>
                    </li>
                </ul>
            </li>

            <li>
                <a class="accordion-toggle" href="#">
                    <span class="fa fa-dashboard"></span>
                    <span class="sidebar-title">Extras</span>
                    <span class="caret"></span>
                </a>
                <ul class="nav sub-nav">
                    <li>
                        <a href="{{ route('faq-type.index') }}">
                            <span class="fa fa-building"></span> Tipos FAQs </a>
                    </li>
                    <li>
                        <a href="{{ route('faqs.index') }}">
                            <span class="fa fa-building"></span> FAQs </a>
                    </li>
                    <li>
                        <a href="{{ route('clickid.index') }}">
                            <span class="fa fa-building"></span> ClickIds </a>
                    </li>
                    <li>
                        <a href="{{ route('dashboard.kyc-documents-status') }}">
                            <span class="fa fa-building"></span> KYC Eventos </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</aside>
