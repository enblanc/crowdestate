<!-- / Topbar Breadcrumb / -->
<header id="topbar" class="alt">
    <div class="topbar-left">
        @if($breadcrumbs)
            <ol class="breadcrumb">
                <li class="breadcrumb-icon">
                    <span class="fa fa-home"></span>
                </li>
                @foreach ($breadcrumbs as $breadcrumb)
                    @if (!$breadcrumb->last)
                        <li class="breadcrumb-link">
                            <a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a>
                        </li>
                    @else
                        <li class="breadcrumb-current-item">{{ $breadcrumb->title }}</li>
                    @endif
                @endforeach
            </ol>
        @endif
    </div>
    @foreach ($breadcrumbs as $breadcrumb)
        @if(isset($breadcrumb->sidebar) && $breadcrumb->sidebar == true)
        <div class="topbar-right visible-sm visible-xs">
            <div class="ib">
                <label for="topbar-text" class="control-label">Mostrar / Ocultar Sidebar</label>
            </div>
            <div class="ib va-m" id="sidebar_right_toggle">
                <div class="navbar-btn btn-group btn-group-number mv0">
                    <button class="btn btn-sm btn-default btn-bordered prn pln">
                        <i class="fa fa-tasks fs22 text-default inline"></i>
                    </button>
                </div>
            </div>
        </div>
        @endif
    @endforeach
</header>