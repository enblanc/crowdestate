<!-- / Header  / -->
<header class="navbar navbar-fixed-top bg-primary">
    <div class="navbar-logo-wrapper bg-primary">
        <a class="navbar-logo-image hidden-xs m20" href="/dashboard">
            <img src="{{ asset('assets/img/logo-brickstarter_white.svg') }}" class="img-responsive" alt="Logo"/>
        </a>
        <span id="sidebar_left_toggle" class="ad ad-lines"></span>
    </div>
    <ul class="nav navbar-nav navbar-left ">
        <li class="hidden-xs">
            {{-- <a class="navbar-fullscreen toggle-active" href="#">
                <span class="glyphicon glyphicon-fullscreen"></span>
            </a> --}}
            <div class="mt15">
                @yield('nav-bar-items')
            </div>
        </li>
    </ul>
    <ul class="nav navbar-nav navbar-right">

        <li class="dropdown dropdown-fuse">
            <a href="#" class="dropdown-toggle fw600" data-toggle="dropdown" aria-expanded="false">
                <span class="hidden-xs"><name>{{ auth()->user()->nombre }}</name> </span>
                <span class="fa fa-caret-down hidden-xs mr15"></span>
                <img src="{{ asset('assets/img/logo_white@2x.png') }}" alt="avatar" class="mw45">
            </a>
            <ul class="dropdown-menu list-group keep-dropdown w250" role="menu">
                <li class="dropdown-footer text-center">
                    <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="btn btn-primary btn-sm btn-bordered">
                        <span class="fa fa-power-off pr5"></span> Cerrar Sesión </a>
                </li>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                </form>
            </ul>
        </li>
    </ul>
</header>