@extends('dashboard.base')

{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
        <style>
            .white-nowrap {
                white-space: nowrap;
            }
            .table-responsive {
                display: block;
                padding: .25rem;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
                -ms-overflow-style: -ms-autohiding-scrollbar;
            }
            .align-items-center {
                -webkit-box-align: center!important;
                -ms-flex-align: center!important;
                align-items: center!important;
            }
            .justify-content-between {
                -webkit-box-pack: justify!important;
                -ms-flex-pack: justify!important;
                justify-content: space-between!important;
            }
            .d-flex {
                display: -webkit-box!important;
                display: -ms-flexbox!important;
                display: flex!important;
            }
            .ml-1 {
                margin-left: .25rem;
            }
            .p-1 {
                padding: 1rem !important;
            }
            .p-0 {
                padding: 0 !important;
            }
            .list-group-item--sm {
                padding: 5px 7px;
            }
        </style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}


{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   AutoInvests Eventos
                </div>
                <div class="pull-right">
	            </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">		        
		        <div class="row">
                    <div class="pull-left">
			            <ul id="list-states" class="list-inline p-t-10">
			                <li>
                                Todas ({{ $events->total() }})
                            </li>
			                <li>|</li>
			                <li>
                                Hoy ({{ $events_today }})
                            </li>
			                <li>|</li>
			                <li>
                                Ayer ({{ $events_yesterday }})
                            </li>
			            </ul>
			        </div>
		           	<div class="">
                        <div>
                            <div class="table-responsive">
                                <table class="table table-striped table-sm">
                                    <thead>
                                        <tr class="white-nowrap">
                                            <th scope="col">ID</th>
                                            <th scope="col">Descripción</th>
                                            <th scope="col">Tipo</th>
                                            <th scope="col">Evaluados</th>
                                            <th scope="col">Ordenes</th>
                                            <th scope="col">Lanzado el</th>
                                            <th scope="col">Finalizó el</th>
                                            <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($events as $key => $event)
                                            <tr class="white-nowrap">
                                                <td>#{{ $event->id }}</td>
                                                <td>
                                                    {{ $event->description }}
                                                </td>
                                                <td>
                                                    @if ($event->is_property)
                                                        <span class="badge badge-primary">
                                                            Propiedad
                                                        </span>
                                                    @else
                                                        <span class="badge badge-danger">
                                                            Marketplace
                                                        </span>
                                                    @endif
                                                </td>
                                                <td><strong>{{ $event->evaluated }}</strong> (Autoinvests)</td>
                                                <td>
                                                    {{ $event->orders->count() }}
                                                </td>
                                                <td>
                                                    {{ $event->created_at }}
                                                </td>
                                                <td>
                                                    {{ $event->updated_at }}
                                                </td>
                                                <td>
                                                    <button onclick="toggleLogs({{ $key }})" class="btn btn-sm btn-primary">
                                                        <svg width="15" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7"></path></svg>
                                                    </button>
                                                </td>
                                            </tr>
                                            <tr id="logs-{{ $key }}" style="display: none">
                                                <td colspan="9" class="p-0">
                                                    @if (is_array($event->parameters))
                                                        <div class="p-1">
                                                            <div>Parametros evaluados:</div>
                                                            <div>
                                                                @forelse ($event->parameters as $key => $parameter)
                                                                    <div class="badge badge-warning">
                                                                        <span style="opacity: .8">{{ __("autoinvest.$key") }}: </span>
                                                                        @if (isset($properties[$key]))
                                                                            <span>
                                                                                {{ isset($properties[$key][$parameter]) ? $properties[$key][$parameter] : $parameter }}
                                                                            </span>
                                                                        @else
                                                                            <span>
                                                                                {{ $parameter }}
                                                                            </span>
                                                                        @endif
                                                                    </div>
                                                                @empty
                                                                    Sin parametros
                                                                @endforelse
                                                            </div>
                                                        </div>
                                                    @else
                                                        <div class="p-1">
                                                            Sin parametros
                                                        </div>
                                                    @endif
                                                    @if (is_array($event->logs))
                                                        <ul class="list-group">
                                                            @forelse ($event->logs as $log)
                                                                @if (is_array($log))
                                                                    @foreach ($log as $key => $item)
                                                                        <li class="list-group-item list-group-item--sm d-flex align-items-center">
                                                                            @includeIf('dashboard.autoinvest.parts.status.'.$key)
                                                                            <span class="ml-1">{{ $item }}</span>
                                                                        </li>
                                                                    @endforeach    
                                                                @else
                                                                    <li class="list-group-item list-group-item--sm d-flex align-items-center">
                                                                        @includeIf('dashboard.autoinvest.parts.status.0')
                                                                        <span class="ml-1">{{ $log }}</span>
                                                                    </li>
                                                                @endif
                                                            @empty
                                                                <li class="list-group-item">No existe logs</li>
                                                            @endforelse
                                                        </ul>
                                                    @endif
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="7" class="text-center">
                                                    Sin eventos del AutoInvests
                                                </td>
                                            </tr>    
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            {{ $events->links() }}
                        </div>
                    </div>
		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
        <script>
            function toggleLogs (id) {
                const element_logs = document.getElementById('logs-' + id);
                console.log(element_logs)
                if (element_logs) {
                    if (element_logs.style.display === "none") {
                        element_logs.style.display = "table-row";
                    } else {
                        element_logs.style.display = "none";
                    }
                }
            }
        </script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}