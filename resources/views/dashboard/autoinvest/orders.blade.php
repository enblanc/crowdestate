@extends('dashboard.base')

{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
        <style>
            .white-nowrap {
                white-space: nowrap;
            }
            .table-responsive {
                display: block;
                padding: .25rem;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
                -ms-overflow-style: -ms-autohiding-scrollbar;
            }
        </style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}


{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   AutoInvests Ordenes
                </div>
                <div class="pull-right">
                    <a href="{{ route('dashboard.autoinvests.index') }}" class="btn btn-primary">Listado AutoInvests</a>
	            </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">		        
		        <div class="row">
                    <div class="col-md-12 pull-left">
			            <ul id="list-states" class="list-inline p-t-10">
			                <li>
                                Todas ({{ $orders->total() }})
                            </li>
			                <li>|</li>
			                <li>
                                Propiedades ({{ $orders_properties }})
                            </li>
			                <li>|</li>
			                <li>
                                Marketplace ({{ $orders_marketplaces }})
                            </li>
			            </ul>
			        </div>
		           	<div class="col-md-12">
                        <div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr class="white-nowrap">
                                            <th scope="col">ID</th>
                                            <th scope="col">Usuario</th>
                                            <th scope="col">Invertido</th>
                                            <th scope="col">Descriptión</th>
                                            <th scope="col">Creado el</th>
                                            <th scope="col">Actualizado el</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($orders as $order)
                                                @php
                                                    $autoinvest = $order->autoinvest;
                                                @endphp
                                            <tr class="white-nowrap">
                                                <td>#{{ $order->id }}</td>
                                                <td>
                                                    {{ $autoinvest ? $autoinvest->user->email : '-'}}
                                                </td>
                                                <td>
                                                    {{ number_format($order->investment, 2, '.', '') }}
                                                    @if ($order->market_order_id)
                                                        (Marketplace)
                                                    @else
                                                        (Propiedad)
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($order->property_id && $order->property)
                                                        Invertió en {{ $order->property->nombre }} ({{ $order->property->id }})
                                                    @elseif ($order->market_order)
                                                        Order MarketPlace #{{ $order->market_order->id }}
                                                    @else
                                                        -
                                                    @endif
                                                </td>
                                                <td>
                                                    {{ $autoinvest ? $autoinvest->created_at : '-' }}
                                                </td>
                                                <td>
                                                    {{ $autoinvest ? $autoinvest->updated_at : '-' }}
                                                </td>
                                            </tr> 
                                        @empty
                                            <tr>
                                                <td colspan="14" class="text-center">
                                                    Sin ordenes realizadas por Autoinvests
                                                </td>
                                            </tr>    
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            {{ $orders->links() }}
                        </div>
                    </div>
		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}