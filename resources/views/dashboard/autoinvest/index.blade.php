@extends('dashboard.base')

{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
        <link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
        <style>
            .white-nowrap {
                white-space: nowrap;
            }
            .table-responsive {
                display: block;
                padding: .25rem;
                width: 100%;
                overflow-x: auto;
                -webkit-overflow-scrolling: touch;
                -ms-overflow-style: -ms-autohiding-scrollbar;
            }
        </style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}


{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   AutoInvests
                </div>
                <div class="pull-right">
                    <a href="{{ route('dashboard.autoinvests.orders') }}" class="btn btn-primary">Ordenes</a>
	            </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">		        
		        <div class="row">
                    <div class="col-md-12 pull-left">
			            <ul id="list-states" class="list-inline p-t-10">
			                <li>
                                Todas ({{ $autoinvests->total() }})
                            </li>
			                <li>|</li>
			                <li>
                                Desactivados ({{ $autoinvests_deactivated }})
                            </li>
			                <li>|</li>
			                <li>
                                Activos ({{ $autoinvests_active }})
                            </li>
                            <li>|</li>
			                <li>
                                Aplicados ({{ $autoinvests_used }})
                            </li>
			            </ul>
			        </div>
		           	<div class="col-md-12">
                        <div>
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr class="white-nowrap">
                                            <th scope="col">ID</th>
                                            <th scope="col">Activo</th>
                                            <th scope="col">Usuario</th>
                                            <th scope="col">Min</th>
                                            <th scope="col">Max</th>
                                            <th scope="col">Portafolio</th>
                                            <th scope="col">Inversión</th>
                                            <th scope="col">TIR</th>
                                            <th scope="col">Periodo</th>
                                            <th scope="col">Ubicación</th>
                                            <th scope="col">Promotor</th>
                                            <th scope="col">Tipo</th>
                                            <th scope="col">Mª Ordenes</th>
                                            <th scope="col">Creado el</th>
                                            <th scope="col">Actualizado el</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($autoinvests as $autoinvest)
                                            <tr class="white-nowrap">
                                                <td>#{{ $autoinvest->id }}</td>
                                                <td>
                                                    @if ($autoinvest->status)
                                                        <svg width="20" stroke="#67d3e0" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                                    @else
                                                        <svg width="20" stroke="red" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 14l2-2m0 0l2-2m-2 2l-2-2m2 2l2 2m7-2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg>
                                                    @endif
                                                </td>
                                                <td>{{ $autoinvest->user->email }} ({{ $autoinvest->user->id }})</td>
                                                <td>{{ number_format($autoinvest->min_investment, 2, '.', '') }}</td>
                                                <td>{{ number_format($autoinvest->max_investment, 2, '.', '') }}</td>
                                                <td>{{ number_format($autoinvest->portfolio_size, 2, '.', '') }}</td>
                                                <td>{{ number_format($autoinvest->investment, 2, '.', '') }}</td>
                                                <td>{{ $autoinvest->tir }}</td>
                                                <td>{{ $autoinvest->period }}</td>
                                                <td>{{ $autoinvest->location == 'all' ? 'Todos' : $autoinvest->location }}</td>
                                                {{-- <td>{{ $autoinvest->country }}</td> --}}
                                                <td>
                                                    {{
                                                        isset($properties['developers'][$autoinvest->developer_id]) ? $properties['developers'][$autoinvest->developer_id] : $autoinvest->developer_id
                                                    }}
                                                </td>
                                                <td>
                                                    {{
                                                        isset($properties['types'][$autoinvest->property_type_id]) ? $properties['types'][$autoinvest->property_type_id] : $autoinvest->developer_id
                                                    }}
                                                </td>
                                                {{-- <td>{{ $autoinvest->risk_scoring }}</td> --}}
                                                <td>{{ count($autoinvest->orders) }}</td>
                                                <td>{{ $autoinvest->created_at }}</td>
                                                <td>{{ $autoinvest->updated_at }}</td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="14" class="text-center">
                                                    No se encontró ningún registro
                                                </td>
                                            </tr>    
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            {{ $autoinvests->links() }}
                        </div>
                    </div>
		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}