@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<!-- Ladda -->
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div id="map-options" class="row">
            <div class="col-md-6">
            	<div class="panel">
					<div class="panel-body form-group">
						@if (count($errors) > 0)
					    <div class="alert alert-danger alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					        <ul>
					            @foreach ($errors->all() as $error)
					                <li>{{ $error }}</li>
					            @endforeach
					        </ul>
					    </div>
					    @endif
						{!! Form::open(['route' => 'dashboard.clientes.store', 'method'=> 'post', 'files' => true ]) !!}
							<input type="hidden" name="lat" v-model="client.lat">
							<input type="hidden" name="lng" v-model="client.lng">
							<div class="row">
								<div class="col-md-12">
									<div class="form-group form-group-default">
										<label>Tipo</label>
										<select class="form-control" name="tipo" v-model="client.type">
											<option value="1">Distribuidor</option>
											<option value="2">Marmolista</option>
											<option value="3">Tienda</option>
										</select>
								  	</div>
								</div>
								<div class="col-md-12">
									<div class="form-group form-group-default">
										<label>Nombre</label>
										<input type="text" class="form-control" name="name" v-model="client.name">
								  	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group form-group-default">
										<label>Contacto</label>
										<input type="text" class="form-control" name="contact" v-model="client.contact">
								  	</div>
								</div>

								<div class="col-md-6">
									<div class="form-group form-group-default">
										<label>Mercado</label>
										<input type="text" class="form-control" name="market" v-model="client.market">
								  	</div>
								</div>

								<div class="form-group form-group-default col-md-4">
									<label>Código Postal</label>
									<input type="text" class="form-control" name="postal" v-model="client.postal">
							  	</div>
							  	<div class="form-group form-group-default col-md-4">
									<label>Teléfono 1</label>
									<input type="text" class="form-control" name="phone_1" v-model="client.phone_1">
							  	</div>
							  	<div class="form-group form-group-default col-md-4">
									<label>Teléfono 2</label>
									<input type="text" class="form-control" name="phone_2" v-model="client.phone_2">
							  	</div>

							  	<div class="form-group form-group-default col-md-6">
									<label>Email</label>
									<input type="text" class="form-control" name="city" v-model="client.email">
							  	</div>
							  	<div class="form-group form-group-default col-md-6">
									<label>Web</label>
									<input type="text" class="form-control" name="web" v-model="client.web">
							  	</div>

							  	<div class="form-group form-group-default col-md-6">
									<label>Ciudad</label>
									<input type="text" class="form-control" name="city" v-model="client.city">
							  	</div>
							  	<div class="form-group form-group-default col-md-6">
									<label>Región</label>
									<input type="text" class="form-control" name="region" v-model="client.region">
							  	</div>

							  	<div class="form-group form-group-default col-md-7">
									<label>Dirección</label>
									<input type="text" class="form-control" name="address" v-model="client.address">
							  	</div>

							  	<div class="form-group form-group-default col-md-5">
									<label>País</label>
									<input type="text" class="form-control" name="country" v-model="client.country">
							  	</div>
								<div class="form-group form-group-default col-md-12">
									<button type="button" id="cancelar" class="btn btn-bordered btn-default ">Cancelar</button>
									<button type="submit" class="btn btn-primary btn-bordered ladda-button" data-style="zoom-in">
										<span class="ladda-label">Guardar</span>
										<span class="ladda-spinner"></span>
									</button>
								</div>
							</div>
						{!! Form::close() !!}
	            	</div>
            	</div>
            </div>
            <div class="col-md-6">
            	<div class="panel">
                    <div class="panel-menu br3 mt20">
                        <div class="form-group">
                            <form method="post" id="geocoding_form">
                                <div class="col-xs-9 prn">
                                    <input type="text" class="form-control" id="searchBox" placeholder="Buscar dirección" v-model="search">
                                    <em for="searchBox" class="state-error" v-if="searchError">No se encuentra la búsqueda</em>
                                </div>
                                <div class="col-xs-3">
                                    <input type="submit" class="btn btn-default light btn-block" value="Buscar"  v-on:click.stop.prevent="searchPlace">

                                </div>
                            </form>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="panel-body">
                        <div id="mapa" class="map"></div>
                    </div>
                </div>
            </div>
		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<!-- Ladda -->
		<script src="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.js') }}"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2lOgxv_s-e0-T5OgtaZKbAZ9JsAgAyXE&libraries=places"></script>
		<script>
			$(document).on('click','#cancelar',function(){
				window.location.href = "{{ route('dashboard.clientes.index') }}";
			});

			var client = {
				type: "{{ old('tipo') }}",
				name: "{{ old('name') }}",
				market: "{{ old('market') }}",
				contact: "{{ old('contact') }}",
				email: "{{ old('email') }}",
				web: "{{ old('web') }}",
				postal: "{{ old('postal') }}",
				phone_1: "{{ old('phone_1') }}",
				phone_2: "{{ old('phone_2') }}",
				city: "{{ old('city') }}",
				region: "{{ old('region') }}",
				address: "{{ old('address') }}",
				country: "{{ old('country') }}",
				lat: "{{ old('lat', 40.415363) }}",
				lng: "{{ old('lng', -3.707398) }}",
			}

			Ladda.bind('.ladda-button', {
			    timeout: 8000
			});
		</script>
		<script src="{{ asset(elixir('assets/js/dashboard-clients.js')) }}"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}