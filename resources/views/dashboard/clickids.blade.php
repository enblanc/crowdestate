@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
		<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
    <script>
        function toggleTr (id) {
            var x = document.getElementById('tr-'+id);
            if (x) {
                if (x.style.display === "none") {
                    x.style.display = "table-row";
                } else {
                    x.style.display = "none";
                }
            }
        }
    </script>
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   Click Ids
                </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">

		        
		        <div class="row">
					
		           	<div class="col-md-12">
                        <div class="panel panel-visible no-box-shadow">
                            
                            <div class="panel-body pn mn">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>CLICK_ID</th>
                                            <th>EMAIL</th>
                                            <th>PROVIDER</th>
                                            <th>LOGS</th>
                                        </tr>
                                        @foreach ($clickids as $item)
                                            <tr onclick="toggleTr({{$item->id}})" style="cursor: pointer">
                                                <th>{{ $item->id }}</th>
                                                <th>{{ $item->click_id }}</th>
                                                <th>{{ $item->user->email }}</th>
                                                <th>{{ $item->provider }}</th>
                                                <th>
                                                    <span
                                                        class="{{ count($item->logs) ? 'label label-info' : 'label label-system' }}">
                                                        {{ count($item->logs) }}
                                                    </span>
                                                </th>
                                            </tr>
                                            <tr id="tr-{{$item->id}}" style="display: none">
                                                <th colspan="5">
                                                    @if (count($item->logs))
                                                        <ul>
                                                        @foreach ($item->logs as $log)
                                                            <li>
                                                                <div>
                                                                    <div class="btn btn-sm {{ $log->status ? 'btn-info' : 'btn-danger' }}"></div>
                                                                    {{ $log->status ? 'success' : 'error' }}
                                                                </div>                                                                
                                                                <div>
                                                                    <strong class="d-block" style="display: block">{{ $log->postback }}</strong>
                                                                    @if ($log->message)
                                                                    <div class="label-muted text-white p4">
                                                                        <span style="font-weight: 100;">
                                                                            {!! $log->message !!}
                                                                        </span>
                                                                    </div>
                                                                    @endif
                                                                </div>
                                                                <br>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                    @else
                                                        <span>SIN EVENTOS</span>
                                                    @endif
                                                </th>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="/dashboard_theme/assets/js/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
		<script src="/dashboard_theme/assets/js/plugins/pnotify/pnotify.js"></script>
    
    	<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
		
		<script src="/dashboard_theme/assets/js/deleteRowModel.js"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}