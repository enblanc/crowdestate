@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
		<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	@endsection
{{-- HEADER STYLES SECTION END--}}

{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   Marketplace ordenes
                </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">
		        <div class="row">
		           	<div class="col-md-12">
                        <div class="panel panel-visible no-box-shadow">
                            <div class="panel-body pn mn">
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <th>ID</th>
                                            <th>Usuario</th>
                                            <th>Propiedad</th>
                                            <th>Tipo</th>
                                            <th>Estado</th>
                                            <th>Actualizado</th>
                                            <th>Creado</th>
                                        </tr>
                                        @foreach ($orders as $order)
                                            <tr id="tr-{{$order->id}}">
                                                <th>
                                                    {{ $order->id }}
                                                </th>
                                                <th>
                                                    <a href="{{ route('dashboard.users.edit', $order->user->id) }}">
                                                        {{ $order->user->email }} ({{ $order->user->id }})
                                                    </a>
                                                </th>
                                                <th>
                                                    {{ $order->getPropertyName() }}
                                                </th>
                                                <th>
                                                    {{ $order->type === 1 ? __('Vender') : __('Compra') }}
                                                </th>
                                                <th>
                                                    {{ $order->status ? 'Activo' : 'Desactivado' }}
                                                </th>
                                                <th>
                                                    {{ $order->updated_at }}
                                                </th>
                                                <th>
                                                    {{ $order->created_at }}
                                                </th>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                        {{ $orders->links() }}
                    </div>
		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}