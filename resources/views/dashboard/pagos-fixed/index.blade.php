@extends('dashboard.base')

{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<!-- Datepicker -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker3.standalone.css">
		<link href="{{ asset('/dashboard_theme/custom-calendar.css') }}" rel="stylesheet" type="text/css" />
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}

    @php
        $count_transactions = count($transactions);
    @endphp

{{-- MAIN CONTENT SECTION START --}}
	@section('content')		
        <div class="row">
            <div class="col-md-8">
                <h3 style="margin-top: 0px">Cantidad de registros ({{ $count_transactions }})</h3>
                <p>El proceso sustituira el valor de la columna <strong>"fecha"</strong> por <strong>"fecha_nueva"</strong></p>
            </div>
        </div>
        <form method="GET" action="{{ route('dashboard.pagos.fixed') }}" style="text-align: right;" class="row">
            {{-- {{ csrf_field() }} --}}
            <div class="col-md-2">
                <input type="text" name="fecha_inicio" required class="datepicker" value="{{ Request::get('fecha_inicio') }}" style="width: 100%; display: block">
            </div>
            <div class="col-md-2">
                <input type="text" name="fecha_fin" required class="datepicker" value="{{ Request::get('fecha_fin') }}" style="width: 100%; display: block">
            </div>
            <div class="col-md-2">
                <button type="submit" class="btn btn-primary" style="width: 100%">Buscar</button>
            </div>
        </form>
        <br>
        @if ($count_transactions)
            <form method="POST" action="{{ route('dashboard.pagos.fixed-post') }}">
                {{ csrf_field() }}
                <input type="hidden" name="fecha_inicio" value="{{ Request::get('fecha_inicio') }}">
                <input type="hidden" name="fecha_fin" value="{{ Request::get('fecha_fin') }}">
                <div class="row">
                    <div class="col">
                        <table class="table w-full">
                            <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col" class="alert alert-warning">Fecha</th>
                                <th scope="col" class="text-white alert-danger">Nueva fecha</th>
                                <th scope="col">Comment</th>
                                <th scope="col">Creación</th>
                            </tr>
                            </thead>
                            <tbody>
                                @foreach ($transactions as $transaction)
                                    <tr>
                                        <th scope="row">
                                            {{ $transaction->id }}
                                        </th>
                                        <td class="alert alert-warning">
                                            <span>
                                                {{ $transaction->fecha }}
                                            </span>
                                        </td>
                                        <td class="text-white alert-danger">
                                            @php
                                                $explode_comment = explode(' ', $transaction->comment);
                                                $new_year = end($explode_comment);
                                                $fecha_nueva = \Carbon\Carbon::parse($transaction->fecha);
                                                $fecha_nueva->year = $new_year;
                                            @endphp
                                            <span >{{ $fecha_nueva->toDateTimeString() }}</span>
                                        </td>
                                        <td>
                                            {{ $transaction->comment }}
                                        </td>
                                        <td>
                                            {{ $transaction->created_at }}
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2">
                        <button type="submit">PROCESAR</button>
                    </div>
                </div>
            </form>
        @endif
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.es.min.js"></script>
    <script>
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            allowInputToggle: true,
            orientation: "auto right"
        });
    </script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}