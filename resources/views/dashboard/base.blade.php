<!DOCTYPE html>
<html>

<head>
    <!-- / Meta and Title / -->
    <meta charset="utf-8">
    <title>Brickstarter Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon.png') }}">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- / Fonts / -->
    <link rel='stylesheet' type='text/css' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- / CSS - theme / -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/skin/default_skin/css/theme.css') }}">
    
    <!-- / CUSTOM CSS / -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/custom.css') }}">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <!-- / Favicon / -->
    <link rel="shortcut icon" href="{{ asset('assets/favicon/favicon.ico') }}">
    
    <!-- / IE8 HTML5 support  / -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    <!-- Scripts -->
    <script>
        window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
    </script>
    
    @yield('header_styles')
    @yield('header_scripts')

</head>

<body class="@yield('classes-body')">

    <!-- / Body Wrap  / -->
    <div id="main">

        @yield('modals')
        
       

        <!-- / Header - Navbar  / -->
        @include('dashboard.include.navbar')
        <!-- / Header - Navbar  / -->

         @yield('global-form-init')
        <!-- / Sidebar  / -->
        @include('dashboard.include.sidebar')
        <!-- / Sidebar  / -->

        

        <!-- / Main Wrapper / -->
        <section id="content_wrapper">

            @yield('breadcrumb')
           
            <!-- / Content / -->
            <section id="content" class="animated fadeIn">

                @yield('content')
                
            </section>
            <!-- / /Content / -->
            
        </section>

        @yield('sidebar-right')


        @yield('global-form-end')
        

    </div>
    <!-- / /Body Wrap  / -->

    <!-- / Scripts / -->
    <!-- / jQuery / -->
    <script
            src="https://code.jquery.com/jquery-1.12.4.js"
            integrity="sha256-Qw82+bXyGq6MydymqBxNPYTaUXXq7c8v3CwiYwLLNXU="
            crossorigin="anonymous">
    </script>
    
    <script 
            src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"
            integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk="
            crossorigin="anonymous">
    </script>

<!-- / Theme Scripts / -->
    <script src="{{ asset('/dashboard_theme/assets/js/utility/utility.js') }}"></script>
    <script src="{{ asset('/dashboard_theme/custom.js') }}"></script>
    <script src="{{ asset('/dashboard_theme/assets/js/main.js') }}"></script>
    <script src="{{ asset('/dashboard_theme/assets/js/demo/widgets_sidebar.js') }}"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {

            "use strict";
            // Init Theme Core
            Core.init();
            // Init Admin
            Admin.init();


        });
    </script>
    
    @include('dashboard.notifications')
@yield('footer_scripts')

<!-- / /Scripts / -->

</body>

</html>
