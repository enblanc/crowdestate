@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('styles')
		
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{!! Breadcrumbs::render('styles') !!}
	@endsection
{{-- BREADCRUMB SECTION END --}}


{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('scripts')
		
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}