@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/allcp/forms/css/forms.css') }}">
    	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.bootstrap.min.css">
    	<style>
			.no-shadow {
			    box-shadow: none !important;
			}
			.allcp-form label, .allcp-form input, .allcp-form button, .allcp-form select, .allcp-form textarea {
			    display: inline-block;
			    max-width: 100%;
			    margin-bottom: 5px;
			    font-weight: 600;
			}
			.allcp-form .file.append-button > input.gui-input {
			    padding-left: 135px;
			}
    	</style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div id="user-div" class="row">

			
				<div class="tab-block mb25">
					<ul class="nav panel-tabs">
						<li class="active">
							<a href="#datos-generales" data-toggle="tab" aria-expanded="true">Crear nuevo usuario</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="col-md-12 tab-pane active" id="datos-generales">
							@include('dashboard.users.parts.datos')
			            </div>
					</div>
				</div>
            
		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
		<script>

			$(document).on('click','#cancelar',function(){
				window.location.href = "{{ route('inmuebles.index') }}";
			});
		</script>
		<script src="{{ asset(mix('js/dashboard-users.js')) }}"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}