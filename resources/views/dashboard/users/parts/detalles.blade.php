
<h5>Detalles del usuario</h5>

<div class="row">

	<div class="col-md-12 mb20">
		@if($user->importeRegalo == 0)
			<p>El usuario no tiene regalos pendientes</p>
		@else
			<div>
				El usuario tiene un importe regalo acumulado de: {{ $user->importeRegalo }}€
			
				<button class="btn btn-primary" v-on:click="checkRegalos">Borrar regalos</button>
			</div>
		@endif
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Tipo:</div>
			{{ $user->profile->tipo }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Teléfono:</div>
			{{ $user->profile->telefono }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Nacionalidad:</div>
			{{ $user->profile->nacionalidad }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Sexo:</div>
			{{ $user->profile->sexo }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Estado civil:</div>
			{{ $user->profile->estadoCivilText }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Profesion:</div>
			{{ $user->profile->profesion }}
	    </div>
	</div>

	<hr class="fluid-width alt short">

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Dirección:</div>
			{{ $user->profile->direccion }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Localidad:</div>
			{{ $user->profile->localidad }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Código postal:</div>
			{{ $user->profile->codigo_postal }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Provincia:</div>
			{{ $user->profile->provincia }}
	    </div>
	</div>


	<hr class="fluid-width alt short">

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Tipo de inversor:</div>
	        @if($user->profile->acreditado == 1)
	        	Inversor acreditado
	        @else
	        	Inversor no acreditado
	        @endif
			
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Dni:</div>
			{{ $user->profile->dni }}
	    </div>
	</div>

	<div class="col-md-3">
		<div class="mb20">
	        <div class="text-dark fw700">Referido por:</div>
			@if($user->referrer)
				Usuario referido por el usuario:
				<a class="btn btn-info" href="{{ route('dashboard.users.edit', $user->referrer->id) }}" target="_blank">{{ $user->referrer->nombreCompleto }}</a>
			@else
				Usuario no referido por nadie
			@endif
	    </div>
	</div>
</div>