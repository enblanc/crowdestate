
<h5>Acreditar el usuario</h5>

<form method="POST" action="{{ route('dashboard.users.estado', ['user' => $user->id]) }}" class="row">
    {{ csrf_field() }}

    <div class="col-md-12">
        <div class="form-group form-group-default"  style="display: flex; flex-wrap: wrap; align-items: center; gap: 18px;">
            <label>¿Enviar notificación por correo?</label>
            <div class="switch switch-primary switch-inline">
                <input id="notific" name="notific" type="checkbox">
                <label for="notific" data-on="SÍ" data-off="NO"></label>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group form-group-default"  style="display: flex; flex-wrap: wrap; align-items: center; gap: 8px;">
            <label style="line-height: 1;margin-bottom: 0;">¿Está seguro de cambiar el estado?</label>
            <input id="confirm" name="confirm" type="checkbox" required style="margin: 0">
        </div>
    </div>

    <div class="form-group form-group-default col-md-12">
        <button type="submit" data-style="zoom-in" class="btn btn-primary btn-bordered ladda-button">
            <span class="ladda-label">Actualizar estado</span>
            <span class="ladda-spinner"></span>
        </button>
    </div>
</form>