<div class="row">


	<div class="col-md-12">
		<h5>Crear pago manual</h5>
	</div>

	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Cantidad a invertir</label>
			<input type="number" class="form-control" v-model="manual.amount" required>
	  	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Propiedad</label>
			<select class="form-control show-arrows" v-model="manual.property" required>
				<option>Elige una propiedad</option>
				<option v-for="property in properties" :value="property">@{{ property.nombre }}</option>
				{{-- @foreach($propiedades as $propiedad)
					<option value="{{ $propiedad->id }}">{{ $propiedad->nombre }}</option>
				@endforeach --}}
			</select>
	  	</div>
	</div>

	<div class="col-md-12" v-if="puedeInvertir">
		<button class="btn btn-primary" v-on:click="revisarInversion">Realizar inversión</button>
	</div>

</div>