@if(isset($user))
	{!! Form::model($user, ['route' => ['dashboard.users.update', $user->id], 'method' => 'put', 'id' => 'update-user-form' ]) !!}
	<input type="hidden" name="id" value="{{ $user->id }}">
@else
	{!! Form::open(['route' => 'dashboard.users.store', 'method'=> 'post', 'files' => true, 'id' => 'create-user-form' ]) !!}
@endif
@if (count($errors) > 0)
<div class="alert alert-danger alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
    <ul>
        @foreach ($errors->all() as $error)
            <li>{!! $error !!}</li>
        @endforeach
    </ul>
</div>
@endif
<div class="row">

	@if(isset($user))
	<div class="col-md-12">
		<h5>Balance: {{ $user->balance }} €</h5>
	</div>
	@endif

	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Nombre</label>
			<input type="text" class="form-control" name="nombre" v-model="user.nombre" required>
	  	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Primer Apellido</label>
			<input type="text" class="form-control" name="apellido1" v-model="user.apellido1" required>
	  	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Segundo Apellido</label>
			<input type="text" class="form-control" name="apellido2" v-model="user.apellido2" required>
	  	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Email</label>
			<input type="email" class="form-control" name="email" v-model="user.email" required>
	  	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Contraseña</label>
			<input type="password" class="form-control" name="password" id="password" v-model="user.password" {{ (!isset($user)) ? 'required' : '' }}>
	  	</div>
	</div>
	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Confirmar contraseña</label>
			<input type="password" class="form-control" name="password_confirmation" data-parsley-equalto="#password" parsley-required="true" {{ (!isset($user)) ? 'required' : '' }}>
	  	</div>
	</div>

	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Regalo que recibe el usuario invitado</label>
			<input type="number" class="form-control" name="gift_referidor" id="gift_referidor" v-model="user.gift_referidor">
	  	</div>
	</div>

	<div class="col-md-4">
		<div class="form-group form-group-default">
			<label>Regalo recibe cuando referidor invierte</label>
			<input type="number" class="form-control" name="gift_recibe" id="gift_recibe" v-model="user.gift_recibe">
	  	</div>
	</div>


	@if(isset($user))

		@if($user->hasRole('captador'))
			<div class="col-md-4">
				<div class="form-group form-group-default">
					<label>Prescriptor: Porcentaje sobre el invertido de sus referidos</label>
					<input type="number" class="form-control" step="any" name="porcentaje_invertido" v-model="user.porcentaje_invertido">
			  	</div>
			</div>
			<div class="col-md-4">
				<div class="form-group form-group-default">
					<label>Prescriptor: Porcentaje sobre lo ganado de sus referidos</label>
					<input type="number" class="form-control" step="any" name="porcentaje_ganado" v-model="user.porcentaje_ganado">
			  	</div>
			</div>
		@endif

		<div class="col-md-11">
			<div class="form-group form-group-default">
				<label>Roles</label>
				<select name="roles[]" id="roles" class="roles-select form-control" data-placeholder="Selecciona uno o más roles" multiple="multiple" v-model="user.realRoles">
					@foreach($roles as $rol)
					<option value="{{ $rol->id }}">{{ $rol->name }}</option>
					@endforeach
				</select>
		  	</div>
		</div>
		<div class="col-md-1">
			<div class="form-group form-group-default">
				<label class="fluid-width">Activado</label>
				<div class="switch switch-primary switch-inline">
					<input type="hidden" name="activado" v-model="activado">
					<input id="activado" type="checkbox" checked="" v-model="user.confirm.is_confirmed">
					<label for="activado" data-on="SÍ" data-off="NO"></label>
				</div>
			</div>
		</div>

	@endif
	<div class="form-group form-group-default col-md-12 mt50">
		<button type="button" id="cancelar" class="btn btn-bordered btn-default ">Cancelar</button>
		@if(isset($user))
		<button type="submit" id="guardar" class="btn btn-primary btn-bordered ladda-button" data-style="zoom-in">
			<span class="ladda-label">Actualizar datos</span>
			<span class="ladda-spinner"></span>
		</button>
		@else
		<button type="submit" id="guardar" class="btn btn-primary btn-bordered ladda-button" data-style="zoom-in">
			<span class="ladda-label">Crear usuario</span>
			<span class="ladda-spinner"></span>
		</button>
		@endif
	</div>
</div>

{!! Form::close() !!}

