
@if ($user->transactions()->count())
    <div class="pull-left">
        <h5>Listado de movimientos - Total invertido: {{ $user->totalInvertido }} €</h5>
    </div>

    <div class="pull-right">
        <a href="{{ route('panel.user.export.transactions', $user->id) }}" target="_blank" class="btn btn-primary">
            {{ __('descargar_movimientos') }}
        </a>
        {{-- <button class="btn btn-primary" id="export">Exportar</button> --}}
    </div>

    <transactions-user user_id="{{ $user->id }}"/>
@else
    <div class="clearfix"></div>
    <h5>El usuario aún no tiene transacciones</h5>
@endif