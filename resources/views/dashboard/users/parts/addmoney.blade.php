<div class="row">


	<div class="col-md-12">
		<h5>Añadir fondos al usuario</h5>
	</div>

	<form id="add_money_form" method="post" action="{{ route('users.add_money') }}">
		{{ csrf_field() }}
		<input type="hidden" name="user_id" value="{{ $user->id }}">

		<div class="col-md-4">
			<div class="form-group form-group-default">
				<label>Cantidad a añadir</label>
				<input type="number" class="form-control" name="amount" v-model="addMoney.amount" step="any" required>
		  	</div>
		</div>

		<div class="col-md-12">
		<button class="btn btn-primary" v-on:click="addMoney">Añadir fondos</button>
	</div>


</div>