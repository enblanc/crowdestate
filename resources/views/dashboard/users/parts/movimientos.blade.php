
@if ($user->transactions->count() > 0)
	<div class="pull-left">
		<h5>Listado de movimientos - Total invertido: {{ $user->totalInvertido }} €</h5>
	</div>
	<div class="pull-right">
		<button class="btn btn-primary" id="export">Exportar</button>
	</div>
	<table class="table table-responsive footable" data-paging="true" data-paging-size="20" data-sorting="true">
		<thead>
			<tr class="system">
				<th data-type="string">Tipo de Movimiento</th>
				<th data-type="number">Importe</th>
				<th data-sorted="false" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY">Fecha</th>
				<th data-sortable="true">Estado</th>
			</tr>
		</thead>
		<tbody>
			@foreach($user->transactions()->orderBy('created_at', 'DESC')->get() as $transaction)
			<tr>
				<td>{!! $transaction->typeText !!}</td>
				<td class="h5 c-2 nowrap text-right append-euro">
					@if ($transaction->type == 1 || $transaction->type == 2 || $transaction->type == 15 || $transaction->type == 20)
					-
					@endif
					{{ $transaction->money }} €
				</td>
				<td class="">{{ $transaction->created_at->format('d/m/Y') }}</td>
				<td class="">{{ $transaction->statusText }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>

	<table id="movimientos-table" class="table hide">
		<thead>
			<tr class="system">
				<th data-type="string">Tipo de Movimiento</th>
				<th data-type="number">Importe</th>
				<th data-sorted="false" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY">Fecha</th>
				<th data-sortable="true">Estado</th>
			</tr>
		</thead>
		<tbody>
			@foreach($user->transactions()->orderBy('created_at', 'DESC')->get() as $transaction)
			<tr>
				<td>{!! $transaction->typeText !!}</td>
				<td class="h5 c-2 nowrap text-right ">
					@if ($transaction->type == 1 || $transaction->type == 2 || $transaction->type == 15 || $transaction->type == 15)
					-
					@endif
					{{ $transaction->money }} €
				</td>
				<td class="">{{ $transaction->created_at->format('d/m/Y') }}</td>
				<td class="">{{ $transaction->statusText }}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
@else
	<div class="clearfix"></div>
	<h5>El usuario aún no tiene transacciones</h5>
	
@endif