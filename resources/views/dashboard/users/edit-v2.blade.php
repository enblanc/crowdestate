@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/allcp/forms/css/forms.css') }}">
    	<link href="{{ asset('dashboard_theme/assets/plugins/js/select2/select2.css') }}" rel="stylesheet" type="text/css" />		
    	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.bootstrap.min.css">
    	<style>
			.no-shadow {
			    box-shadow: none !important;
			}
			.allcp-form label, .allcp-form input, .allcp-form button, .allcp-form select, .allcp-form textarea {
			    display: inline-block;
			    max-width: 100%;
			    margin-bottom: 5px;
			    font-weight: 600;
			}
			.allcp-form .file.append-button > input.gui-input {
			    padding-left: 135px;
			}

			.link-tab {
				float: right !important;
			}
			.link-tab > a.blue {
			    background-color: #67d3e0 !important;
				color: #FFF !important;
			}

			.link-tab > a.red, .link-tab > a.red:hover {
				background-color: #f5393d !important;
    			color: #fffbfb;
			}

			.append-euro:after {
				content: ' €';
			}
			.show-arrows {
				appearance:menulist !important;
				-moz-appearance:menulist !important;
				-webkit-appearance: menulist !important;
			}

    	</style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')

		

		<div id="user-div">


			<!-- Modal INversión manual -->
			<div id="modalcheckInversionManual" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Estás seguro de realizar la inversión manual</h4>
						</div>
						<div class="modal-body">
							<p><strong>Cantidad a invertir</strong>: @{{ manualAmount }}</p>
							<p><strong>Propiedad</strong>: @{{ manual.property.nombre }}</p>
							<form id="inversion_manual_form" method="post" action="{{ route('users.inversion_manual') }}">
								{{ csrf_field() }}
								<input type="hidden" name="user_id" value="{{ $user->id }}">
								<input type="hidden" name="property_id" v-model="manual.property.id">
								<input type="hidden" name="amount" v-model="manual.amount">
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" v-on:click="cancelInversion" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-primary"  v-if="!loading" v-on:click="sendManual">Sí, realizar inversión</button>
							<div class="pull-right ml-10" v-if="loading">
								<pulse-loader :loading="loading" :color="color" :size="size"></pulse-loader>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal Borrar regalos -->
			<div id="modalBorrarRegalos" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">¿Estás seguro de borrar todos los regalos?</h4>
						</div>
						<div class="modal-body">
							<p>Se borrarán todos los regalos del usuario</p>
							<form id="borrar_regalos_form" method="post" action="{{ route('users.borrar_regalos', $user->id)  }}">
								{{ csrf_field() }}
								<input type="hidden" name="borrar" value="1">
								</form>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-primary"  v-if="!loading" v-on:click="borrarRegalos">Sí, borrar regalos</button>
							<div class="pull-right ml-10" v-if="loading">
								<pulse-loader :loading="loading" :color="color" :size="size"></pulse-loader>
							</div>
						</div>
					</div>
				</div>
			</div>


			<!-- Modal Borrar usuario -->
			<div id="modalBorrarUsuario" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">¿Estás seguro de borrar al usuario?</h4>
						</div>
						<div class="modal-body">
							<p>Se borrarán todos los datos del usuario</p>
							<p>Sí pulsas en borrar usuario el proceso no tiene marcha atrás</p>
							<form id="borrar_usuario_form" method="POST" action="{{ route('dashboard.users.destroy', $user->id)  }}">
								{{ csrf_field() }}
								<input name="_method" type="hidden" value="DELETE">
								<input type="hidden" name="borrar" value="1">
								</form>
							</form>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-primary"  v-if="!loading" v-on:click="borrarUsuario">Sí, borrar usuario</button>
							<div class="pull-right ml-10" v-if="loading">
								<pulse-loader :loading="loading" :color="color" :size="size"></pulse-loader>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="text-center" v-if="loading">
				<pulse-loader :loading="loading" :color="color" :size="size"></pulse-loader>
			</div>
			<template v-if="!loading">
				<div  class="row">
					<div class="tab-block mb25">
						<ul class="nav panel-tabs">
							<li class="active">
								<a href="#datos" data-toggle="tab" aria-expanded="true">Editar usuario</a>
							</li>
							<li>
								<a href="#detalles" data-toggle="tab" aria-expanded="true">Detalles</a>
							</li>
							<li>
								<a href="#movimientos" data-toggle="tab" aria-expanded="true">Movimientos</a>
							</li>

							@if($user->hasLemonAccount())
								<li>
									<a href="#manual" data-toggle="tab" aria-expanded="true">Crear inversión manual</a>
								</li>

								<li>
									<a href="#addmoney" data-toggle="tab" aria-expanded="true">Añadir fondos</a>
								</li>
							@endif

							<li>
								<a href="#addmoney-promotional" data-toggle="tab" aria-expanded="true">Añadir promoción</a>
							</li>

							<li>
								<a href="#status" data-toggle="tab" aria-expanded="true">Estado</a>
							</li>

							<li class="link-tab">
								<a class="blue" href="{{ route('dashboard.users.index') }}">Volver a la lista de Usuarios</a>
							</li>

							@if($user->properties->count() == 0 && $user->balance == 0)
								<li class="link-tab">
									<a class="red" href="#" v-on:click="checkBorraUsuario">Borrar usuario</a>
								</li>
							@endif

						</ul>
						<div class="tab-content">
							<div class="col-md-12 tab-pane active" id="datos">
								@include('dashboard.users.parts.datos')
				            </div>
				            <div class="col-md-12 tab-pane" id="detalles">
								@include('dashboard.users.parts.detalles')
				            </div>
				            <div class="col-md-12 tab-pane" id="movimientos">
								@include('dashboard.users.parts.movimientos-v2')
				            </div>
				            @if($user->hasLemonAccount())
								<div class="col-md-12 tab-pane" id="manual">
									@include('dashboard.users.parts.manual')
								</div>
								<div class="col-md-12 tab-pane" id="addmoney">
									@include('dashboard.users.parts.addmoney')
								</div>
				            @endif
				            <div class="col-md-12 tab-pane" id="addmoney-promotional">
								@include('dashboard.users.parts.addmoney-promotional')
				            </div> 
				            <div class="col-md-12 tab-pane" id="status">
								@include('dashboard.users.parts.status')
				            </div> 
						</div>
					</div>
				</div>
				@if(!$user->hasRole('super'))
					<div  class="row">
						<div class="panel panel-widget newsletter-widget">
							<div class="panel-heading">
								<span class="panel-title">Pulsa en el siguiente botón para loguearte como el usuario</span>
							</div>
							<div class="panel-body bg-light dark pb25">
								<form action="{{ route('users.login_as', $user->id) }}" method="POST">
									{{ csrf_field() }}
									<input type="submit" class="btn btn-primary" value="Loguearte como {{ $user->nombreCompleto }}">
								</form>
							</div>
						</div>
					</div>
				@endif
			</template>
		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
		<script src="/dashboard_theme/assets/plugins/js/select2/select2.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.min.js"></script>
		{{-- <script src="//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js"></script> --}}
		{{-- <script src="https://rawgit.com/Snack-X/excelexport.js/master/excelexport.js"></script> --}}
		<script>
			(function(F){
				F.NumberColumn.extend("construct", function(instance, definition){
					this._super(instance, definition);
					this.cleanRegex = new RegExp('[^\-0-9' + F.str.escapeRegExp(this.decimalSeparator) + ']', 'g');
				});
			})(FooTable);

			$(document).on('click','#cancelar',function(){
				window.location.href = "{{ route('dashboard.users.index') }}";
			});

			var userRoute = '{{ url('/') }}/api/v1/user/{{ $user->id }}';
			function activejQueryPlugins() {				
				$('.roles-select').select2();
				/*
					$('.footable').footable();
					@if($user->transactions->count() > 0)
						var ee = excelExport("movimientos-table").parseToCSV().parseToXLS("movimientos usuario {{ $user->id }}");
						$("#export").click(function (event) {
							file = ee.getXLSDataURI();
							var a = document.createElement("a");
							document.body.appendChild(a);
							a.style = "display: none";
							a.href = file;
							a.download = 'Movimientos usuario {{ $user->id }}';
							a.click();
							window.URL.revokeObjectURL(file);
							a.remove();
						});
					@endif
				*/
			}
			var properties = {!! $propiedades !!};
		</script>
		<script src="{{ asset(mix('js/dashboard-users.js')) }}"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}