<!--  Modal  -->
<div id="modal-category" class=" popup-basic popup-lg allcp-form mfp-with-anim mfp-hide">
    <div class="panel">
        <div class="panel-heading">
 			<span class="panel-title">
    			<i class="fa fa-rocket"></i>Añadir Categoría
			</span>
        </div>
        <!--  /Panel Heading  -->
		<input type="hidden" name="catId">
		<div class="panel-body  ph15">
	        <div class="tab-block ">
			    <ul class="nav nav-tabs">
			        @foreach($locales as $locale)
						<li @if($locale->iso == 'es') class="active" @endif>
			            	<a href="#tab_{{ $locale->iso }}" data-toggle="tab">Nombre {{ $locale->language }}</a>
			        	</li>
					@endforeach
			    </ul>
			    <div class="tab-content mh100">
			        
			        @foreach($locales as $locale)
						
			        	<div id="tab_{{ $locale->iso }}" class="tab-pane @if($locale->iso == 'es') active @endif">
			        		<div class="row col-md-12">
								<div class="form-group form-group-default">
	                                <label>Nombre de la categoría</label>
	                                <input type="text" name="name-{{ $locale->iso }}"  class="form-control clear-text" required>
	                            </div>
						    </div>
			        	</div>

					@endforeach
			    </div>
			   {{--  <div class="form-group form-group-default mt20">
	                <label>Color</label>
	                <select name="color_cat" class="colores_categorias_new">
	                    <option value="" data-tipo="cat-0">Sin Color</option>
	                    <option value="#06304c" data-tipo="cat-1">Marino</option>
	                    <option value="#07d090" data-tipo="cat-2">Verde</option>
	                    <option value="#fdd835" data-tipo="cat-3">Naranja</option>
	                    <option value="#eac0ff" data-tipo="cat-4">Rosa</option>
	                    <option value="#00e5ff" data-tipo="cat-5">Azul</option>
	                    <option value="#FF851C" data-tipo="cat-6">Naranja Neón</option>
	                    <option value="#00B295" data-tipo="cat-7">Cyan</option>
	                    <option value="#457B9D" data-tipo="cat-8">Azure</option>
	                    <option value="#E63946" data-tipo="cat-9">Rojo</option>
	                    <option value="#B33F62" data-tipo="cat-10">Bourbon</option>
	                </select>
	            </div> --}}
			</div>
		</div>
		<div class="panel-footer">
            <button type="submit" class="button btn-primary pull-right" id="createCategory">Guardar</button>
            <button type="submit" class="button btn-primary pull-right hide" id="updateCategory">Actualizar</button>
            <div class="clearfix"></div>
        </div>
    </div>
    <!--  /Panel  -->
</div>
<!--  Modal  -->