@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<!-- include REdactor css-->
		<link href="{{ asset('/dashboard_theme/assets/plugins/redactor/redactor.css') }}" rel="stylesheet" type="text/css" />

		<!-- include SimpleColorPicker css-->
		<link type="text/css" rel="stylesheet" href="{{ asset('/blog_assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css') }}">

		<!-- Datepicker -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker3.standalone.css">
		<link href="{{ asset('/dashboard_theme/custom-calendar.css') }}" rel="stylesheet" type="text/css" />

		<!-- Forms -->
		<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/allcp/forms/css/forms.css') }}">

		<!-- FanceBox -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" type="text/css" media="screen" />

		<!--  MagnificPopup  -->
    	<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/js/plugins/magnific/magnific-popup.css') }}">
		
		<!-- SweetAlert -->
    	<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

		<!-- Select2 -->
    	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    	<style type="text/css">
    		#option-related{
		        -webkit-appearance: menulist;
		        margin-right: 10px;
		    }

		    .p-custom{
		        padding: 15px 25px
		    }


		    .imagen-extra{
		        margin-top:20px;
	            padding-left: 10px;
		    }

		    .img-div{
		    	position: relative;
			    display: inline-block; /* added */
			    overflow: hidden; /* added */
		    }

		    .img-div img{
				width:100%
		    }
		    .img-div:hover img{
				opacity: 0.5
		    }
		    .img-div:hover a {
			    opacity: 1; /* added */
			    top: 0; /* added */
			    z-index: 500;
			}

			.img-div:hover a span {
			    top: 50%;
			    position: absolute;
			    left: 0;
			    right: 0;
			    transform: translateY(-50%);
			}

			/* added */
			.img-div a {
			    display: block;
			    position: absolute;
			    top: -100%;
			    opacity: 0;
			    left: 0;
			    bottom: 0;
			    right: 0;
			    text-align: center;
			    color: #FFF;
			}
    	</style>

	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}

	@section('classes-body')
		sb-r-o
	@endsection
	{{-- Sección para los modales --}}
	@section('modals')
		@include('dashboard/blog/modals')
	@endsection

{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{!! Breadcrumbs::render('blog.create') !!}
	@endsection
{{-- BREADCRUMB SECTION END --}}

	

{{-- MAIN CONTENT SECTION START --}}
	@section('content')

		<div class="row">
			<div class="col-md-12">
				@if (count($errors) > 0)
	                <div class="alert alert-danger">
	                    <ul>
	                        @foreach ($errors->all() as $error)
	                            <li>{{ $error }}</li>
	                        @endforeach
	                    </ul>
	                </div>
	            @endif
				<div class="form-group">

					<div class="tab-block mb25">
					    <ul class="nav nav-tabs tabs-bg">
					        @foreach($locales as $locale)
								<li @if($locale->iso == 'es') class="active" @endif>
					            	<a href="#tab_principal_{{ $locale->iso }}" data-toggle="tab">Descripción {{ $locale->language }}</a>
					        	</li>
							@endforeach
					    </ul>
					    <div class="tab-content br-grey">
					        @foreach($locales as $locale)
					        	<div id="tab_principal_{{ $locale->iso }}" class="tab-pane @if($locale->iso == 'es') active @endif">
					        		<div class="row">
										<div class="form-group form-group-default col-md-12">
					                        <label>Título</label>
					                        <input type="text" name="title-{{$locale->iso}}" class="form-control" value="{{ old('title-'.$locale->iso) }}" >
					                    </div>


					                    <div class="form-group form-group-default col-md-12">
					                        <label>Descripción</label>
					                        <textarea id="content-{{$locale->iso}}" name="content-{{$locale->iso}}">{{ old('content-'.$locale->iso) }}</textarea> 
					                    </div>

					                    <div id="div-seo-{{$locale->iso}}">
					                    	<div class="form-group form-group-default col-md-12">
						                        <label>Meta título</label>
						                        <input type="text" name="meta-title-{{$locale->iso}}" maxlength="191" class="form-control" value="{{ old('meta-title-'.$locale->iso) }}" >
						                    </div>
						                    <div class="form-group form-group-default col-md-12">
						                        <label>Meta descripción</label>
						                        <textarea name="meta-description-{{$locale->iso}}" maxlength="191" class="form-control">{{ old('meta-descriptiondescription-'.$locale->iso) }}</textarea>
						                    </div>
						                    <div class="form-group form-group-default col-md-12">
							                    <label>Etiquetas</label>
							                    <input type="text" name="tags-{{$locale->iso}}" id="tags-{{$locale->iso}}" class="form-control tags" value="{{ old('tags-'.$locale->iso) }}">
							                </div>
					                    </div>

					                    {{--Related--}}
					                    {{-- <div class="col-md-12">
											<div id="related-{{$locale->iso}}">
						                        <button type="button" class="btn btn-lg  btn-complete" id="tipo-popover-{{$locale->iso}}" onclick="addRelated('{{$locale->iso}}')">Añadir imagen extra</button>
						                    </div>
					                    </div> --}}
								    </div>
					        	</div>
							@endforeach
							<div class="hide">
								<h5 class="title-divider text-muted mb20">
									Imágenes extra
								</h5>
						        <div class="row">
									<div class="col-md-4">
										<span class="btn btn-primary btn-file btn-block" id="imagen-add">
										    <i class="fa fa-cloud-upload"></i> Añadir imagen
				                      	</span>
									</div>
						        	<div class="clearfix"></div>
			                      	<div class="row">
			                      		<div class="col-md-12 images-extra">

			                      		</div>
			                      	</div>
						        </div>
					       </div>
					    </div>
					</div>
				</div>
			</div>
		</div>

		
	@endsection
{{-- MAIN CONTENT SECTION END --}}
	
	@section('sidebar-right')
		<!--  Sidebar Right  -->
	    <aside id="sidebar_right" class="nano">

	        <!--  Sidebar Right Content  -->
	        <div class="sidebar-right-wrapper nano-content allcp-form theme-primary">
	            <div class="sidebar-block br-n p15">

	                <h5 class="title-divider text-muted mb20">Opciones</h5>
	                <div class="row mb20">
						<div class="col-md-12">
							Fecha de Publicación:
							<div class="input-group date">
							  <input type="text" name="publish_date" class="form-control datepicker" value="{{ old('fecha_publicacion', date('d/m/Y')) }}"><span class="input-group-addon open-calendar"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
	                </div>

					<div class="row mb20">
						<div class="col-md-12">
	                        Estado de la entrada:
                            <label class="field select">
                                <select id="estado" name="state">
                                    <option value="0" {{ (old('estado') == 0) ? 'selected' : '' }}>Borrador</option>
                                    <option value="1" {{ (old('estado') == 1) ? 'selected' : '' }}>Publicada</option>
                                </select>
                                <i class="arrow"></i>
                            </label>
						</div>
					</div>

					<div class="row mb30">
						<div class="col-md-12">	
			                    {{-- <a href="{{ url('dashboard/blog') }}" class="btn btn-bordered btn-danger">Cancelar</a> --}}
								<button type="button" id="cancelar" class="btn btn-bordered btn-default ">Cancelar</button>
								<button type="submit" class="btn btn-bordered btn-primary ">Guardar</button>
							
						</div>
					</div>

					{{-- Sección Categorias --}}
					<h5 class="title-divider text-muted mb20 ">
						Categorías
						<div class="pull-right right-panel-options">
							<a href="#" class="add-category"><small>Añadir</small></a>
						</div>
					</h5>
					<div class="row mb20">
						<div id="categories_container" class="col-md-12">
							@foreach($categories as $category)
								<div class="form-group">
								    <div class="checkbox-custom checkbox-default mb5">
									    <input type="checkbox" {{ ($category->id == 1) ? 'checked="checked"' : '' }} value="{{$category->id}}"" id="category_{{$category->id}}" name="categories[]">
									    <label for="category_{{$category->id}}" class="cb-fix">{{  $category->name }}</label>
									    @if($category->id != 1)
									    	<div class="pull-right cat-options">
												<a class="right-panel-options text-primary editCategory" href="#" data-cat="{{$category->id}}" data-name="{{ $category->name }}" id="edit-category">Editar</a> |
												<a class="right-panel-options text-danger removeCategory" href="#" data-cat="{{$category->id}}" data-name="{{ $category->name }}" id="remove-category">Eliminar</a>
									    	</div>
									    @endif
									</div>
								</div>
							@endforeach
						</div>
					</div>

					{{-- Sección Foto Principal --}}
					<h5 class="title-divider text-muted mb20">
						Imagen Principal
					</h5>
					<div class="row mb20">
						<div class="form-group">
							<div class="col-lg-12">
								<div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
				                    <div class="fileupload-preview fileinput-exists thumbnail mb20" data-trigger="fileinput" style="line-height: 205px;">
				                        <img class="photo-preview" data-src="holder.js/100px250?text=Imagen Principal" id="image_featured" alt="Release Photo" style="display: block;">
				                    </div>
				                    @if ($errors->has('photo'))
				                      <em for="photo" class="state-error photo">{{ $errors->first('photo') }}</em>
				                    @endif
				                    <div class="row">
				                        <div class="col-xs-12">
					                      	<span class="btn btn-primary btn-file btn-block" onclick="uploadImage('image_featured');">
											    <i class="fa fa-cloud-upload"></i> Cargar Foto
					                      	</span>
					                      	<input type="hidden" id="imagen_principal" name="featured_image">
				                        </div>
				                    </div>
				                </div>
				            </div>
						</div>
					</div>
					
	            </div>
	        </div>
	    </aside>
	    <!--  /Sidebar Right  -->

	@endsection

	@section('global-form-init')
		{!! Form::open(['route' => 'dashboard.blog.store', 'method'=> 'post', 'files' => true, 'id' => 'form-store' ]) !!}
	@endsection

	@section('global-form-end')
		{!! Form::close() !!}
	@endsection

{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<!--  Moment  -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/moment/moment.min.js') }}"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/locale/es.js"></script>

		<!-- Redactor -->
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/redactor.js') }}"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/lang/es.js') }}"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/source.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/fullscreen.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/imagemanager.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/video.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/table.js') }}" type="text/javascript"></script>

		<!--  DateTime JS  -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.es.min.js"></script>

		<!-- Media Browser -->
		<script src="{{ asset('dashboard_theme/assets/js/plugins/holder/holder.min.js') }}"></script>

		<!-- FancyBox -->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

		<!-- MagnificPopup -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/magnific/jquery.magnific-popup.js') }}"></script>

		<!-- ColorPicker -->
		<script src="{{ asset('/blog_assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js') }}" type="text/javascript"></script>

		<!-- SweetAlert -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

		<!-- Select2 -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
    	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/es.js"></script>

    	<!-- Tags -->
    	<script src="{{ asset('/blog_assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>

		<!-- PNotify -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/pnotify/pnotify.js') }}"></script>

		<script type="text/javascript">
			
			jQuery(document).ready(function($) {

				$(document).on('click','#cancelar',function(){
					window.location.href = "{{ route('dashboard.blog.index') }}";
				});

				var img = $('#image_featured').get(0);
				Holder.setResizeUpdate(img, false);

				//categories
        		$('select.colores_categorias_new').simplecolorpicker({theme: 'fontawesome'});
        		$('select.colores_categorias').simplecolorpicker({theme: 'fontawesome'});

				$('.open-calendar').click(function(){
					
					$('.datepicker').focus();

				});

				$('.datepicker').datepicker({
					format: "dd/mm/yyyy",
				    language: "es",
				    autoclose: true,
				    allowInputToggle: true,
				    orientation: "auto right"
				});

				// Tags
				$('.tags').tagsinput();


				//Image Browser
		        uploadImage = function(dat){
	                $.fancybox({
	                    width		: 950,
	                    height	    : 500,
	                    type		: 'iframe',
	                    href        : "{{ url('dashboard/filemanager/dialog') }}?type=featured&appendId="+dat,
	                    fitToView   : false,
	                    autoScale   : false,
	                    autoSize    : false
	                });
	            };

		        OnMessage = function(data){
		            var pattern = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif|bmp))/i;
	                if(pattern.test(data.thumb)){
	                	var testFeatured = /^imagen-extra.*/g;
	                	if (testFeatured.test(data.appendId)) {
	                		$("#"+data.appendId).attr({ 'src' : data.thumb}).removeClass('hide');
	                		$("#"+data.appendId+'-text').val(data.path);
	                	} else {
		                    var id = data.appendId;
		                    var id = id.replace('-text', '');
		                    $("#imagen_principal").val(data.path);
		                    $("#"+id).attr({ 'src' : data.thumb});
		                    $("#"+id).removeClass('hide');
	                    }
	                } else {
	                    $("#"+data.appendId).val(data.thumb);
	                }
		            $.fancybox.close();
		        };

	            $(document).on('click', '.delete-related', function(){
	                var divRelated = $(this).parentsUntil('.div-related').parent();
	                var isDB = $(divRelated).data('db');
	                var sure = $(this).data("sure");
	                var button = $(this);

	                if(sure == false){
	                    button.data("sure", true);
	                    button.text("¿Seguro?");
	                    button.toggleClass("btn-danger btn-warning");
	                    setTimeout(check_delete, 5000, button);
	                    return false;
	                }
	                if(isDB == false){
	                    divRelated.remove();
	                }
	            });

	            check_delete = function(button){
	                var sure = button.data("sure");
	                if(sure == true){
	                    button.data("sure", false);
	                    button.html("<i class='fa fa-trash'></i>")
	                    button.toggleClass("btn-warning btn-danger")
	                }
	            };

	            addRelated = function(lang){
	                var lenght = $("#related-"+lang+" > div").length;
	                var counter = parseInt(lenght) + 1;
	                var type = $(".popover-content").find("#option-related option:selected").val();
	                var html = '<div class="related-'+ counter +' div-related clearfix" data-db=false>' +
	                                '<h5>Imagen extra '+ counter +'</h5>' +
	                                '<div class="col-md-8">' +
	                    		         '<div class="form-group form-group-default">' +
	                                        '<input type="text" class="form-control" id="related-'+lang+'-'+counter+'" name="related['+lang+']['+counter+'][val]">' +
	                                    '</div>' +
	                    		    '</div>' +
	                    		    '<div class="col-md-2">' +
                                        '<button type="button" data-inputid="related['+lang+']['+counter+']bt" class="btn btn-default popup_selector file_click" onclick="uploadImage(\'related-'+lang+'-'+counter+'\');">' +
                                            '<i class="fa fa-cloud-upload"></i> Elegir imagen'+
                                        '</button>' +
	                                '</div>' +
	                                '<div class="col-md-2">' +
	                                    '<button type="button" class="btn btn-danger delete-related" data-sure="false"><i class="fa fa-trash"></i></button>' +
	                                '</div>' +
	                            '</div>';
	                $("#related-"+lang).append(html);
	            };

	            //Imágenes extras
		        $(document).on('click', '#imagen-add', function(){
		            var lenght = $("div.imagen-extra").length;
		            var counter = parseInt(lenght) + 1;
		            var html = '<div class="imagen-extra col-md-4">' +
		                    '<input type="hidden" class="form-control" id="imagen-extra-'+counter+'-text" name="extraimages['+counter+']">' +
		                    '<button type="button" data-inputid="imagen-'+counter+'-file" class="btn btn-default popup_selector full-width" onclick="uploadImage(\'imagen-extra-'+counter+'\');">' +
		                    '<i class="fa fa-cloud-upload"></i> Elegir imagen '+counter  +
		                    '</button>' +
		                    '<div class="img-div">' +
		                    	'<img class="output hide" src="" width="100%" height="10%" id="imagen-extra-'+counter+'">' +
		                    	'<a href="#" class="remove-image"><span class="fa fa-trash fa-5x"></span></a>' +
		                    '</div>' +
		                    '</div>';
		            $(".images-extra").append(html);
		        });

		        $(document).on('click', '.remove-image', function(){
		            $(this).parentsUntil('.imagen-extra').parent().remove();
		        });

				//Plugin Redactor
				@foreach($locales as $locale)

					$('#content-{{$locale->iso}}').redactor({
						lang: 'es',
		                minHeight: 350,
		                maxHeight: 800,
		                cleanOnPaste: true,
		                cleanSpaces: true,
		                structure: true,
		                script: false,
		                removeComments: true,
		                removeEmpty: ['strong', 'em', 'span', 'p'],
		                pasteLinkTarget: '_blank',
		                buttonsHide: ['orderedlist', 'image'],
		                formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
		                plugins: ['video', 'source', 'table', 'imagemanager', 'fullscreen'],
		                imageManagerUrl: "{{ url('dashboard/filemanager/dialog') }}?type=editor&editor=content-{{$locale->iso}}",
		            });

				@endforeach
				
				@if(is_array(old('categories')))
					$("input[type='checkbox']").prop('checked', false);
					@foreach(old('categories') as $cat)
						$("input:checkbox[value={{$cat}}]").prop('checked', true);
					@endforeach
				@endif

				@include('dashboard/blog/parts/scriptsCategories')




				//Selects
				function formatRepo (repo) {
		            if (repo.loading) return repo.text;

		            var markup  = "<div class='select2-result-repository clearfix'>" +
		                            "<div class='select2-result-repository__avatar inline-object'><img class='img-responsive mw40 ib mr10' src='" + repo.photo + "' /></div>" +
		                            "<div class='select2-result-repository__meta inline-object'>" +
		                                "<div class='select2-result-repository__title'>" + repo.name + "</div>" +
		                            "</div>" +
		                        "</div>";

		            return markup;
		        }

		        function formatRepoSelection (repo) {
		            return repo.name || repo.text;
		        }

		        
			
			});

		</script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}