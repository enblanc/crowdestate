				/**
				 * Categorias
				 */
				var clearModalCategory = function(){
					$("#modal-category").find('input').val('');
                    $("#modal-category").find('.tab-block a[href="#tab_es"]').tab('show');
                    $("#createCategory").removeClass('hide');
                    $("#updateCategory").addClass('hide');
				}
				// Init Category add
		        var modalCategory = $('.add-category').magnificPopup({
		            type: 'inline',
		            removalDelay: 500,
		            items: {
		                src: '#modal-category',
		                type: 'inline'
		            },
		            mainClass: 'mfp-slideRight',
		            focus: '#focus-blur-loop-select',
		            callbacks: {
		                open: function(item){

		                },
		                close: function(){
		                	clearModalCategory();
		                }
		            },
		            midClick: true
		        });

				// Edit Category modal
				$(document).on('click','.editCategory',function(){
		            var datos = {
		                id : $(this).attr("data-cat")
		            }
		            $.ajax({
		                url: "{{ route('dashboard.blog.category.edit') }}",
		                beforeSend: function (request){
		                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content') );
		                },
		                type: 'get',
		                cache: false,
		                dataType: 'json',
		                data: datos,
		                success: function(data) {



		                    $('[name="catId"]').val(data.id)
		                    $.each(data["languages"], function(i, item) {
		                        $("#modal-category").find('[name="name-'+i+'"]').val(item.name)
		                    });

		                    $("#createCategory").addClass('hide');
                    		$("#updateCategory").removeClass('hide');

		                    modalCategory.magnificPopup('open');


		                },
		                error: function(error){
		                    $('.modal').modal('hide');
		                    $.fn.notifica({
		                        type: "error",
		                        message: "{{Lang::get('blog::blog.database.error')}}"
		                    });
		                }
		            });
		        });
		        

			


				//Save new category
				$(document).on('click','#createCategory',function(e){
		            e.preventDefault();
		            locales = [];
		            @foreach($locales as $locale)
		            	locales.push("{{$locale->iso}}");
	                @endforeach

		                var datos = {
		                        lang : locales,
		                        @foreach($locales as $locale)
		                        'name_{{$locale->iso}}': $('[name="name-{{$locale->iso}}"]').val(),
		                        @endforeach
		                    };
		            //console.log(datos);

		            $.ajax({
		                url: "{{ route('dashboard.blog.category.store') }}",
		                beforeSend: function (request){
		                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content') );
		                },
		                type: 'post',
		                cache: false,
		                dataType: 'json',
		                data: datos,
		                success: function(data) {
		                    $('#modal-category').magnificPopup('close');
		                    var checkbox = '<div class="checkbox-custom checkbox-default mb5"> \
		                                        <input type="checkbox"  value="'+ data.id +'" id="category_'+ data.id +'" name="categories[]">  \
		                                        <label for="category_'+ data.id +'" class="cb-fix">'+ data.name +'</label> \
		                                        <div class="pull-right cat-options"> \
		                                            <a class="right-panel-options text-primary editCategory" href="#" data-cat="'+ data.id +'" id="edit-category">Editar</a> | \
		                                            <a class="right-panel-options text-danger removeCategory" href="#" data-cat="'+ data.id +'" id="remove-category">Eliminar</a> \
		                                        </div> \
		                                    </div>';
		                    $("#categories_container").append(checkbox);
		                    $.fn.notifica({
		                        type: "message",
		                        message: "{{Lang::get('blog::blog.database.category_saved')}}"
		                    });
		                    new PNotify({
		                        title: "Categoría creada",
		                        text: "La categoría se ha guardado correctamente",
		                        type: "info"
		                    });
		                },
		                error: function(error){
		                    $('#modal-category').magnificPopup('close');
		                    new PNotify({
		                        title: "Error",
		                        text: "Error al crear la categoría",
		                        type: "error"
		                    });
		                }
		            });

		            return false;
		        });

				//Actualiza Categoria
				$(document).on('click','#updateCategory',function(){
		            locales = [];
		            @foreach($locales as $locale)
		            	locales.push("{{$locale->iso}}");
	                @endforeach
	                var datos = {
	                    catId : $('[name="catId"]').val(),
	                    lang : locales,
	                    @foreach($locales as $locale)
	                    'name_{{$locale->iso}}': $('[name="name-{{$locale->iso}}"]').val(),
	                    @endforeach
	                };
		            $.ajax({
		                url: "{{ route('dashboard.blog.category.update') }}",
		                beforeSend: function (request){
		                    request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content') );
		                },
		                type: 'post',
		                cache: false,
		                dataType: 'json',
		                data: datos,
		                success: function(data) {
		                    console.log(data);

		                    $('label[for=category_'+data.id+']').text(data.name);

		                    console.log($('label[for=category_'+data.id+']'));

		                    $('#modal-category').magnificPopup('close');
		                    new PNotify({
		                        title: "Categoría actualizada",
		                        text: "La categoría se ha actualizado correctamente",
		                        type: "info"
		                    });

		                },
		                error: function(error){
		                	$('#modal-category').magnificPopup('close');
		                    new PNotify({
		                        title: "Error",
		                        text: "Error al actualizar la categoría",
		                        type: "error"
		                    });
		                }
		            });
		        });
		        
				
				//Elimina categoria
				$(document).on('click','.removeCategory',function(){

					var category = $(this);
					
					swal({  title: "Estas seguro de querer borrar esta categoría?",
		                text: "La categoría será borrada de la Base de Datos",
		                type: "warning",
		                showCancelButton: true,
		                confirmButtonColor: "#DD6B55",
		                confirmButtonText: "Si, borrar!",
		                cancelButtonText: "Cancelar",
		                closeOnConfirm: true
		            }, function(isConfirm){
		                if (isConfirm) {
		                	var datos = {
				                id : category.parentsUntil('.checkbox-custom').parent().find('input').val()
				            }
		                    $.ajax({
		                        url: "{{ route('dashboard.blog.category.delete') }}",
		                        beforeSend: function (request){
		                            request.setRequestHeader( "X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content') );
		                        },
		                        data: datos,
		                        type: 'POST',
		                        success: function(result) {
		                            // Show an alert with the result
		                            new PNotify({
		                                title: "Categoría Borrada",
		                                text: "Categoría borrada correctamente.",
		                                type: "info"
		                            });
		                            // delete the row from the table
		                            category.parentsUntil('.checkbox-custom').parent().remove();
		                        },
		                        error: function(result) {
		                            // Show an alert with the result
		                            new PNotify({
		                                title: "No Borrado",
		                                text: "Existió algún error y su registro no pudo ser borrado.",
		                                type: "danger"
		                            });
		                        }
		                    });

		                }
		            });

				});