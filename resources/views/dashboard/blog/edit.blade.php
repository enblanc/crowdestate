@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<!-- include REdactor css-->
		<link href="{{ asset('/dashboard_theme/assets/plugins/redactor/redactor.css') }}" rel="stylesheet" type="text/css" />

		<!-- include SimpleColorPicker css-->
		<link type="text/css" rel="stylesheet" href="{{ asset('/blog_assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.css') }}">

		<!-- Datepicker -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker3.standalone.css">
		<link href="{{ asset('/dashboard_theme/custom-calendar.css') }}" rel="stylesheet" type="text/css" />

		<!-- Forms -->
		<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/allcp/forms/css/forms.css') }}">

		<!-- FanceBox -->
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" type="text/css" media="screen" />

		<!--  MagnificPopup  -->
    	<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/js/plugins/magnific/magnific-popup.css') }}">
		
		<!-- SweetAlert -->
    	<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

		<!-- Select2 -->
    	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

		<!-- XEditable -->
    	<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/js/plugins/xeditable/css/bootstrap-editable.css') }}">
	
		<style type="text/css">
			.imagen-extra{
		        margin-top:20px;
	            padding-left: 10px;
		    }

		    .img-div{
		    	position: relative;
			    display: inline-block; /* added */
			    overflow: hidden; /* added */
		    }

		    .img-div img{
				width:100%
		    }
		    .img-div:hover img{
				opacity: 0.5
		    }
		    .img-div:hover a {
			    opacity: 1; /* added */
			    top: 0; /* added */
			    z-index: 500;
			}

			.img-div:hover a span {
			    top: 50%;
			    position: absolute;
			    left: 0;
			    right: 0;
			    transform: translateY(-50%);
			}

			/* added */
			.img-div a {
			    display: block;
			    position: absolute;
			    top: -100%;
			    opacity: 0;
			    left: 0;
			    bottom: 0;
			    right: 0;
			    text-align: center;
			    color: #FFF;
			}
		</style>

	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}

	@section('classes-body')
		sb-r-o
	@endsection
	{{-- Sección para los modales --}}
	@section('modals')
		@include('dashboard/blog/modals')
	@endsection

{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{!! Breadcrumbs::render('blog.edit', $post) !!}
	@endsection
{{-- BREADCRUMB SECTION END --}}

	

{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<input type="hidden" name="postId" id="postId" value="{{$post->id}}">
		<input type="hidden" name="image_changed" id="image_changed" value="0">
		<div class="row">
			<div class="col-md-12">
				@if (count($errors) > 0)
	                <div class="alert alert-danger">
	                    <ul>
	                        @foreach ($errors->all() as $error)
	                            <li>{{ $error }}</li>
	                        @endforeach
	                    </ul>
	                </div>
	            @endif
				<div class="form-group">

					<div class="tab-block mb25">
					    <ul class="nav nav-tabs tabs-bg">
					        @foreach($locales as $locale)
								<li @if($locale->iso == 'es') class="active" @endif>
					            	<a href="#tab_principal_{{ $locale->iso }}" data-toggle="tab">Descripción {{ $locale->language }}</a>
					        	</li>
							@endforeach
					    </ul>
					    <div class="tab-content br-grey">
					        
					        @foreach($locales as $locale)
								
					        	<div id="tab_principal_{{ $locale->iso }}" class="tab-pane @if($locale->iso == 'es') active @endif">
					        		<div class="row">
										<div class="form-group form-group-default col-md-12">
					                        <label>Título</label>
					                        @php 
												$baseUrl = route('front.blog.show', '');
											@endphp
					                        <input type="text" name="title-{{$locale->iso}}" class="form-control" value="{{ old('title-'.$locale->iso, $post->translate($locale->iso)->title) }}">
					                        <p class="hint-text small"><strong>Enlace permanente:</strong> {{ $baseUrl . '/' }}<span id="slug-container-{{$locale->iso}}"><span id="slug_modificar-{{$locale->iso}}" data-type="text" data-title="Cambiar enlace permanente">{{$post->translate($locale->iso)->slug}}</span></span>
					                        	<a href="{{ $baseUrl . '/' .$post->translate($locale->iso)->slug }}" target="_blank" class="label label-success">Ver previa</a>
				                        	</p>
                							<input type="hidden" name="slug-{{$locale->iso}}" id="slug-{{$locale->iso}}" value="{{$post->translate($locale->iso)->slug}}">
					                    </div>			                    


					                    <div class="form-group form-group-default col-md-12">
					                        <label>Descripción</label>
					                        <textarea id="content-{{$locale->iso}}" name="content-{{$locale->iso}}">{!! old('content-'.$locale->iso, $post->translate($locale->iso)->content) !!}</textarea> 
					                    </div>

					                    <div id="div-seo-{{$locale->iso}}">
					                    	<div class="form-group form-group-default col-md-12">
						                        <label>Meta título</label>
						                        <input type="text" name="meta-title-{{$locale->iso}}" maxlength="191" class="form-control" value="{{ old('meta-title-'.$locale->iso, $post->translate($locale->iso)->meta_title) }}" >
						                    </div>
						                    <div class="form-group form-group-default col-md-12">
						                        <label>Meta descripción</label>
						                        <textarea name="meta-description-{{$locale->iso}}" maxlength="191" class="form-control">{{ old('meta-descriptiondescription-'.$locale->iso, $post->translate($locale->iso)->meta_description) }}</textarea>
						                    </div>
						                    <div class="form-group form-group-default col-md-12">
							                    <label>Etiquetas</label>
							                    <input type="text" name="tags-{{$locale->iso}}" id="tags-{{$locale->iso}}" class="form-control tags" value="{{ $post->translate($locale->iso)->meta_tags }}">
							                </div>
					                    </div>
								    </div>
					        	</div>
							@endforeach

							{{-- <h5 class="title-divider text-muted mb20">
								Imágenes extra
							</h5>
					        <div class="row">
								<div class="col-md-4">
									<span class="btn btn-primary btn-file btn-block" id="imagen-add">
									    <i class="fa fa-cloud-upload"></i> Añadir imagen
			                      	</span>
								</div>
					        	<div class="clearfix"></div>
		                      	<div class="row">
		                      		<div class="col-md-12 images-extra">
										@php $imageExtras = $post->getMedia('images_extra'); @endphp
							            @if(count($imageExtras) > 0)
							                @php $counter = 1; @endphp
											@foreach($imageExtras as $image)
							                    <div class="imagen-extra col-md-4">
							                        <input type="hidden" class="form-control" id="imagen-extra-1-text" name="extraimages[{{ $counter }}]" value="{{ $image->getUrl('thumb') }}">
							                        <button type="button" data-inputid="imagen-{{ $counter }}-file" class="btn btn-default popup_selector full-width" onclick="uploadImage('imagen-extra-{{ $counter }}');">
							                            <i class="fa fa-cloud-upload"></i> Elegir imagen {{ $counter }}
							                        </button>
							                        <div class="img-div">
							                            <img class="output" src="{{ $image->getUrl('thumb') }}" width="100%" height="10%" id="imagen-extra-{{ $counter }}">
							                            <a href="#" class="remove-image">
							                                <span class="fa fa-trash fa-5x"></span>
							                            </a>
							                        </div>
							                    </div>
							                    @php ++$counter; @endphp
							                @endforeach
							            @endif
		                      		</div>
		                      	</div>
					        </div> --}}
					    </div>
					</div>
				</div>
			</div>
		</div>

		
	@endsection
{{-- MAIN CONTENT SECTION END --}}
	
	@section('sidebar-right')
		<!--  Sidebar Right  -->
	    <aside id="sidebar_right" class="nano">

	        <!--  Sidebar Right Content  -->
	        <div class="sidebar-right-wrapper nano-content allcp-form theme-primary">
	            <div class="sidebar-block br-n p15">

	                <h5 class="title-divider text-muted mb20">Opciones</h5>
	                <div class="row mb20">
						<div class="col-md-12">
							Fecha de Publicación:
							<div class="input-group date">
							@php $fecha = Carbon\Carbon::parse($post->publish_date); @endphp
							  <input type="text" name="publish_date" class="form-control datepicker" value="{{ old('fecha_publicacion', $fecha->format('d/m/Y')) }}"><span class="input-group-addon open-calendar"><i class="glyphicon glyphicon-th"></i></span>
							</div>
						</div>
	                </div>

					<div class="row mb20">
						<div class="col-md-12">
	                        Estado de la entrada:
                            <label class="field select">
                                <select id="estado" name="state">
                                    <option value="0" {{ (old('estado', $post->state) == 0) ? 'selected' : '' }}>Borrador</option>
                                    <option value="1" {{ (old('estado', $post->state) == 1) ? 'selected' : '' }}>Publicada</option>
                                </select>
                                <i class="arrow"></i>
                            </label>
						</div>
					</div>

					<div class="row mb30">
						<div class="col-md-12">	
			                    {{-- <a href="{{ url('dashboard/blog') }}" class="btn btn-bordered btn-danger">Cancelar</a> --}}
								<button type="button" id="cancelar" class="btn btn-bordered btn-default ">Cancelar</button>
								<button type="submit" class="btn btn-bordered btn-primary ">Actualizar</button>
							
						</div>
					</div>

					{{-- Sección Categorias --}}
					<h5 class="title-divider text-muted mb20 ">
						Categorías
						<div class="pull-right right-panel-options">
							<a href="#" class="add-category"><small>Añadir</small></a>
						</div>
					</h5>
					<div class="row mb20">
						<div id="categories_container" class="col-md-12">
							@foreach($categories as $category)
								<div class="form-group">
								    <div class="checkbox-custom checkbox-default mb5">
									    <input type="checkbox" {{ ($category->id == 1) ? 'checked="checked"' : '' }} value="{{$category->id}}"" id="category_{{$category->id}}" name="categories[]">
									    <label for="category_{{$category->id}}" class="cb-fix">{{  $category->name }}</label>
									    @if($category->id != 1)
									    	<div class="pull-right cat-options">
												<a class="right-panel-options text-primary editCategory" href="#" data-cat="{{$category->id}}" data-name="{{ $category->name }}" id="edit-category">Editar</a> |
												<a class="right-panel-options text-danger removeCategory" href="#" data-cat="{{$category->id}}" data-name="{{ $category->name }}" id="remove-category">Eliminar</a>
									    	</div>
									    @endif
									</div>
								</div>
							@endforeach
						</div>
					</div>

					{{-- Sección Foto Principal --}}
					<h5 class="title-divider text-muted mb20">
						Imagen Principal
					</h5>
					<div class="row mb20">
						<div class="form-group">
							<div class="col-lg-12">
								<div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
				                    <div class="fileupload-preview fileinput-exists thumbnail mb20" data-trigger="fileinput" style="line-height: 205px;">
				                        <img class="photo-preview" src="{{ asset($post->getFirstMediaUrl('post_feature', 'thumb')) }}" id="image_featured" alt="Release Photo" style="display: block;" data-holder-rendered="false">
				                    </div>
				                    <div class="row">
				                        <div class="col-xs-12">
					                      	<span class="btn btn-primary btn-file btn-block" onclick="uploadImage('image_featured')">
											    <i class="fa fa-cloud-upload"></i> Actualizar Imágen
					                      	</span>
					                      	<input type="hidden" id="imagen_principal" name="featured_image">
				                        </div>
				                    </div>
				                </div>
				            </div>
						</div>
					</div>
	        </div>
	    </aside>
	    <!--  /Sidebar Right  -->

	@endsection

	@section('global-form-init')
		{!! Form::open(['route' => 'dashboard.blog.update', 'method'=> 'put', 'files' => true ]) !!}
	@endsection

	@section('global-form-end')
		{!! Form::close() !!}
	@endsection

{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<!--  Moment  -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/moment/moment.min.js') }}"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.14.1/locale/es.js"></script>

		<!-- Redactor -->
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/redactor.js') }}"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/lang/es.js') }}"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/source.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/fullscreen.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/imagemanager.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/video.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/table.js') }}" type="text/javascript"></script>

		<!--  DateTime JS  -->
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.es.min.js"></script>

		<!-- Media Browser -->
		<script src="{{ asset('dashboard_theme/assets/js/plugins/holder/holder.min.js') }}"></script>

		<!-- FancyBox -->
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>

		<!-- MagnificPopup -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/magnific/jquery.magnific-popup.js') }}"></script>

		<!-- ColorPicker -->
		<script src="{{ asset('/blog_assets/plugins/jquery-simplecolorpicker/jquery.simplecolorpicker.js') }}" type="text/javascript"></script>

		<!-- SweetAlert -->
		<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>

		<!-- Select2 -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.full.min.js"></script>
    	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/es.js"></script>

		<!-- PNotify -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/pnotify/pnotify.js') }}"></script>

		<!-- Tags -->
    	<script src="{{ asset('/blog_assets/plugins/bootstrap-tag/bootstrap-tagsinput.min.js') }}" type="text/javascript"></script>
	
		<!-- XEditable -->
		<script src="{{ asset('/dashboard_theme/assets/js/plugins/xeditable/js/bootstrap-editable.min.js') }}"></script>
		<script src="{{ asset('/blog_assets/plugins/jslug/jslug.js') }}" type="text/javascript"></script>


		<script type="text/javascript">

			jQuery(function ($) {
				$(document).on('nested:fieldRemoved', function (event) {
					$('[required]', event.field).removeAttr('required');
				});
			});	
			
			jQuery(document).ready(function($) {

				$(document).on('click','#cancelar',function(){
					window.location.href = "{{ route('dashboard.blog.index') }}";
				});

				$('.open-calendar').click(function(){
					
					$('.datepicker').focus();

				});

				$('.datepicker').datepicker({
					format: "dd/mm/yyyy",
				    language: "es",
				    autoclose: true,
				    allowInputToggle: true,
				    orientation: "auto right"
				});


				//tags
				$('.tags').tagsinput();



				//Image Browser
		        uploadImage = function(dat){
	                $.fancybox({
	                    width		: 950,
	                    height	    : 500,
	                    type		: 'iframe',
	                    href        : "{{ url('dashboard/filemanager/dialog') }}?type=featured&appendId="+dat,
	                    fitToView   : false,
	                    autoScale   : false,
	                    autoSize    : false
	                });
	            };

		        OnMessage = function(data){
		            var pattern = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif|bmp))/i;
	                if(pattern.test(data.thumb)){
	                	var testFeatured = /^imagen-extra.*/g;
	                	if (testFeatured.test(data.appendId)) {
	                		$("#"+data.appendId).attr({ 'src' : data.thumb}).removeClass('hide');
	                		$("#"+data.appendId+'-text').val(data.path);
	                	} else {
		                    var id = data.appendId;
		                    var id = id.replace('-text', '');
		                    $("#imagen_principal").val(data.path);
		                    $("#"+id).attr({ 'src' : data.thumb}).removeClass('hide');
		                    $("#image_changed").val(1);
	                    }
	                } else {
	                    $("#"+data.appendId).val(data.thumb);
	                }
		            $.fancybox.close();
		        };


		        function createEditable(lang, slug){
		            $("#slug-"+lang).val(slug);
		            $('#slug_modificar-'+lang).editable("destroy");
		            $('#slug_modificar-'+lang).remove();
		            var span = $('<span />').attr('id', 'slug_modificar-'+lang ).html(slug);
		            $("#slug-container-"+lang).append(span);
		            $('#slug_modificar-'+lang).editable();

		            $('#slug_modificar-'+lang).on('save', function(e, params) {
		                var slug = params.newValue.slug()
		                createEditable(lang, slug);
		            });

		        }

		        //Imágenes extras
		        $(document).on('click', '#imagen-add', function(){
		            var lenght = $("div.imagen-extra").length;
		            var counter = parseInt(lenght) + 1;
		            var html = '<div class="imagen-extra col-md-4">' +
		                    '<input type="hidden" class="form-control" id="imagen-extra-'+counter+'-text" name="extraimages['+counter+']">' +
		                    '<button type="button" data-inputid="imagen-'+counter+'-file" class="btn btn-default popup_selector full-width" onclick="uploadImage(\'imagen-extra-'+counter+'\');">' +
		                    '<i class="fa fa-cloud-upload"></i> Elegir imagen '+counter  +
		                    '</button>' +
		                    '<div class="img-div">' +
		                    	'<img class="output hide" src="" width="100%" height="10%" id="imagen-extra-'+counter+'">' +
		                    	'<a href="#" class="remove-image"><span class="fa fa-trash fa-5x"></span></a>' +
		                    '</div>' +
		                    '</div>';
		            $(".images-extra").append(html);
		        });

		        $(document).on('click', '.remove-image', function(){
		            $(this).parentsUntil('.imagen-extra').parent().remove();
		        });


				//Plugin Redactor
				@foreach($locales as $locale)

					$('#content-{{$locale->iso}}').redactor({
						lang: 'es',
		                minHeight: 350,
		                maxHeight: 800,
		                cleanOnPaste: true,
		                cleanSpaces: true,
		                structure: true,
		                script: false,
		                removeComments: true,
		                removeEmpty: ['strong', 'em', 'span', 'p'],
		                pasteLinkTarget: '_blank',
		                buttonsHide: ['orderedlist', 'image'],
		                formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
		                plugins: ['video', 'source', 'table', 'imagemanager', 'fullscreen'],
		                imageManagerUrl: "{{ url('dashboard/filemanager/dialog') }}?type=editor&editor=content-{{$locale->iso}}",
		            });

		            //Slug Editor
		            $.fn.editable.defaults.mode = 'inline';
		            $('#slug_modificar-{{$locale->iso}}').editable();

		            $('#slug_modificar-{{$locale->iso}}').on('save', function(e, params) {
		                var slug = params.newValue.slug()
		                createEditable("{{$locale->iso}}", slug);
		            });

				@endforeach
                <?php $cats = old('categories', $post->categories()->pluck('id')->all()); ?>
                @if(is_array($cats))

                    $("#categories_container").find("input[type='checkbox']").prop('checked', false);
                    @foreach($cats as $cat)
                        $("#categories_container").find("input:checkbox[value={{$cat}}]").prop('checked', true);
                    @endforeach
                @endif


				@include('dashboard/blog/parts/scriptsCategories')


			});

		</script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}