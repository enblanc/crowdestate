@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<link rel="stylesheet" type="text/css" href="/dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css">
		<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')

	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{!! Breadcrumbs::render('blog.index') !!}
	@endsection
{{-- BREADCRUMB SECTION END --}}


{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">

		    <div class="panel-body">

		        
		        <div class="row">

		        	<div class="col-md-12">
			            <div class="row col-md-9 pull-left">

			            <ul id="list-states" class="list-inline p-t-10">
			                <li data-state='all' class="cursor change_state active">Todas ({{$all}})</li>
			                <li>|</li>
			                <li data-state='published' class="cursor change_state">Publicadas ({{$published}})</li>
			                <li>|</li>
			                <li data-state='trash' class="cursor change_state">Papelera ({{$trash}})</li>
			            </ul>
			            </div>
			            <div class="row col-md-3 pull-right no-margin no-padding">
			                <div class="col-xs-12 no-margin no-padding">
			                    <input type="text" id="search-table" class="form-control pull-right" placeholder="Buscar">
			                </div>
			            </div>
			            
			        </div>
					
		           	<div class="col-md-12">
                        <div class="panel panel-visible no-box-shadow">
                            
                            <div class="panel-body pn">
                                <div class="table-responsive">
                                    <table class="table table-hover" id="datatable" cellspacing="0"
                                           width="100%">
                                        <thead>
	                                        <tr>
							                    <th style="width:15%">Foto</th>
							                    <th>Título</th>
							                    <th style="width:35%">Descripción</th>
							                    <th>F. Publicación</th>
							                    <th>Estado</th>
							                    <th style="width:15%" class="no-sort">Opciones</th>
							                </tr>
	                                    </thead>
                                       
                                        <tbody>
                                       		
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

		        </div>
		    </div>
		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')

    	<script src="/dashboard_theme/assets/js/plugins/datatables/media/js/jquery.dataTables.js"></script>
	    <script src="/dashboard_theme/assets/js/plugins/datatables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
	    <script src="/dashboard_theme/assets/js/plugins/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js"></script>
	    <script src="/dashboard_theme/assets/js/plugins/datatables/media/js/dataTables.bootstrap.js"></script>
	    <script src="//cdn.datatables.net/plug-ins/1.10.12/api/fnReloadAjax.js"></script>
	    <script src="/dashboard_theme/assets/js/plugins/pnotify/pnotify.js"></script>
	    <script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
		<script src="{{ asset('/filemanager_assets/vendor/jquery.lazy-master/jquery.lazy.min.js') }}" type="text/javascript"></script>
		<script type="text/javascript">
			var table;
				var initTableWithSearch = function() {
			    table = $('#datatable');
			    	var settings = {
				        "sDom": "<'table-responsive't><'row'<F ip>>",
				        "destroy": true,
				        "responsive": true,
				        "scrollCollapse": true,
				        "processing": true,
			            "serverSide": true,
				        "language": {
							"url": "//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json"
						},
						"ajax" : "{{ url('dashboard/blog/') }}/data_blog?type=all",
			            "fnDrawCallback" : function (oSettings) {
			                $('.lazy').lazy({
			                    bind: "event",
			                    effect: 'fadeIn',
			                    visibleOnly: true,
			                    onError: function(element) {
			                        element.removeClass('loading-gif');
			                    },
			                    afterLoad: function(element) {
			                        element.removeClass('loading-gif');
			                    },
			                    onFinishedAll: function(element) {
			                        $('img').removeClass('loading-gif');
			                    }
			                });
			            },
				        "iDisplayLength": 20,
				        "columnDefs": [ {
			    			"targets"	 : 'no-sort',
			    			"orderable": false,
			    			"searchable": false
			    			}],
			            "order": [[ 3, "desc" ]],
						"columns": [
					        {data: 'photo', name: 'photo', 'searchable' : false, 'orderable' : false},
					        {data: 'title', name: 'title'},
					        {data: 'content', name: 'content'},
					        {data: 'publish_date', name: 'publish_date'},
					        {data: 'state', name: 'state'},
					        {data: 'opciones', name: 'opciones','searchable' : false, 'orderable' : false, 'class' : 'options text-right'}
					    ]
				    };

			    	table.dataTable(settings);
				    // search box for table
				    $('#search-table').keyup(function() {
				        table.fnFilter($(this).val());
				    });
				}

			initTableWithSearch();

		    $(document).on('click','.removePost',function(){
		        var id = $(this).attr("data-id");
		        $("#remove-postID").val(id);
		        
		    });


		    $(document).on('click','.removeForcePost',function(){
		        var id = $(this).attr("data-id");
		        $("#remove-force-postID").val(id);
		        
		    });

		    $(document).on('click','#removeForcePostYes',function(){
		        var datos = {
		            id : $("#remove-force-postID").val()
		        }
		        console.log(datos);
		        $.ajax({
		            url: "{{ url('dashboard/blog/remove_force') }}",
		            beforeSend: function (request){
		                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content') );
		            },
		            type: 'post',
		            cache: false,
		            dataType: 'json',
		            data: datos,
		            success: function(data) {
		                $('.modal').modal('hide');
		                location.reload();                
		            },
		            error: function(error){
		                $('.modal').modal('hide');
		                new PNotify({
		                    title: "Error",
		                    text: "{{Lang::get('blog::blog.database.error')}}",
		                    type: "error"
		                });
		            }
		        });
		        
		    });


		    $(document).on('click','.restorePost',function(){
		        var datos = {
		            id : $(this).attr("data-id")
		        }
		        console.log(datos);
		        $.ajax({
		            url: "{{ route('dashboard.blog.restore') }}",
		            beforeSend: function (request){
		                request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content') );
		            },
		            type: 'post',
		            cache: false,
		            dataType: 'json',
		            data: datos,
		            success: function(data) {
		                $('.modal').modal('hide');
		                location.reload();
		            },
		            error: function(error){
		                $('.modal').modal('hide');
		                new PNotify({
		                    title: "Error",
		                    text: "{{Lang::get('blog::blog.database.error')}}",
		                    type: "error"
		                });
		            }
		        });
		        
		    });

		    $(document).on('click','.change_state',function(){
		        var state = $(this).data("state");
		        var newUrl = "{{ url('dashboard/blog/') }}/data_blog?type="+state;
		        table.fnReloadAjax(newUrl);

		        $("#list-states").find("li").removeClass("active");

		        $(this).addClass("active");
		    });
		    

		    $(document).on('click','.close_modal',function(){
		        $('.modal').modal('hide');
		    });


		    //Delete
			// make the delete button work in the first result page
			register_delete_button_action();
			register_force_delete_button_action();

			// make the delete button work on subsequent result pages
			$('#datatable').on( 'draw.dt',   function () {
			    register_delete_button_action();
			    register_force_delete_button_action();
			} ).dataTable();

			function register_delete_button_action() {
			    $("[data-button-type=delete]").unbind('click');
			    // CRUD Delete
			    // ask for confirmation before deleting an item
			    $("[data-button-type=delete]").click(function(e) {
			        e.preventDefault();
			        var delete_button = $(this);
			        var delete_url = $(this).attr('href');
			        
			        swal({  title: "Estas seguro?",
			                text: "La entrada será enviada a la papelera y no estará visible en la web",
			                type: "warning",
			                showCancelButton: true,
			                confirmButtonColor: "#DD6B55",
			                confirmButtonText: "Si, a la papelera!",
			                cancelButtonText: "Cancelar",
			                closeOnConfirm: true
			            }, function(isConfirm){
			                if (isConfirm) {

			                    $.ajax({
			                        url: delete_url,
			                        beforeSend: function (request){
			                            request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
			                        },
			                        type: 'DELETE',
			                        success: function(result) {
			                            // Show an alert with the result
			                            new PNotify({
			                                title: "Registro Borrado",
			                                text: "Registro borrado correctamente.",
			                                type: "info"
			                            });
			                            // delete the row from the table
			                            delete_button.parentsUntil('tr').parent().remove();
			                            setTimeout(location.reload(), 2000);
			                            
			                        },
			                        error: function(result) {
			                            // Show an alert with the result
			                            new PNotify({
			                                title: "No Borrado",
			                                text: "Existió algún error y su registro no pudo ser borrado.",
			                                type: "danger"
			                            });
			                        }
			                    });

			                }
			            });

			    });
			}

			function register_force_delete_button_action() {
			    $("[data-button-type=forceDelete]").unbind('click');
			    // CRUD Delete
			    // ask for confirmation before deleting an item
			    $("[data-button-type=forceDelete]").click(function(e) {
			        e.preventDefault();
			        var delete_button = $(this);
			        var delete_url = $(this).attr('href');
			        
			        swal({  title: "Estas seguro de eliminarla para siempre?",
			                text: "La entrada será eliminada completamente de la Base de Datos",
			                type: "warning",
			                showCancelButton: true,
			                confirmButtonColor: "#DD6B55",
			                confirmButtonText: "Si, eliminar!",
			                cancelButtonText: "Cancelar",
			                closeOnConfirm: true
			            }, function(isConfirm){
			                if (isConfirm) {

			                    $.ajax({
			                        url: delete_url,
			                        beforeSend: function (request){
			                            request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
			                        },
			                        type: 'DELETE',
			                        success: function(result) {
			                            // Show an alert with the result
			                            new PNotify({
			                                title: "Registro Borrado",
			                                text: "Registro borrado correctamente.",
			                                type: "info"
			                            });
			                            // delete the row from the table
			                            delete_button.parentsUntil('tr').parent().remove();
			                            setTimeout(location.reload(), 2000);
			                            
			                        },
			                        error: function(result) {
			                            // Show an alert with the result
			                            new PNotify({
			                                title: "No Borrado",
			                                text: "Existió algún error y su registro no pudo ser borrado.",
			                                type: "danger"
			                            });
			                        }
			                    });

			                }
			            });

			    });
			}

		</script>

	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}