@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
		<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="_panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   KYS Eventos
                </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="_panel-body">
                <div class="panel panel-visible no-box-shadow">
                    <div class="table-responsive">
                        {!! $dataTable->table(['id' => 'datatable', 'class' => 'table table-hover']) !!}
                    </div>
                </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="/dashboard_theme/assets/js/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
		<script src="/dashboard_theme/assets/js/plugins/pnotify/pnotify.js"></script>
    
    	<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
		
		{!! $dataTable->scripts() !!}
		<script src="/dashboard_theme/assets/js/deleteRowModel.js"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}