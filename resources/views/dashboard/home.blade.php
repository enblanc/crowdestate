@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<style> 
			.table-fixed thead {
			  width: 100%;
			}
			.table-fixed tbody {
			  height: 230px;
			  overflow-y: auto;
			  width: 100%;
			}
			.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
			  display: block;
			}
			.table-fixed tbody td, .table-fixed thead > tr> th {
			  float: left;
			  border-bottom-width: 0;
			}
			 .table-fixed tbody tr td{
			 	height: 40px;
			 }
		</style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}

	@section('nav-bar-items')
		Pago sin validar en LemonWay
		<form class="inline-block" method="POST" action="{{ route('dashboard.api.pagosinvalidar') }}">
			{{ csrf_field() }}
			@if(!pago_sin_confirmar())
				<input type="hidden" name="tipo" value="1">
				<input type="submit" value="Activar" class="btn btn-system ml10">
			@else
				<input type="hidden" name="tipo" value="0">
				<input type="submit" value="Desactivar" class="btn btn-system ml10">
			@endif
		</form>
	@endsection


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}


{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="chute chute-center">
			<div class="row">
				<div class="col-sm-12 col-xl-12">
					<div class="alert alert-info">
						<i class="fa fa-info pr10"></i>
						<strong>Bienvenidos al dashboard</strong>
					</div>
				</div>
				
				@if(count($propertiesOutDate) > 0)
	                <div class="col-sm-12 col-xl-6">
	                    <div class="panel">
	                    	<div class="panel-heading">
	                            <span class="panel-title">Inmuebles que han pasado de fecha</span>
	                        </div>
	                        <div class="panel-body ">
								<div class="table-responsive">
								    <table class="table table-striped table-fixed">
								        <thead class="bg-dark">
								        <tr>
								            <th class="br-t-n col-xs-3">Referencia</th>
								            <th class="br-t-n col-xs-3">Nombre</th>
								            <th class="br-t-n col-xs-3">Fecha Finalización</th>
								            <th class="br-t-n col-xs-3">Opciones</th>
								        </tr>
								        </thead>
								        <tbody class="scroller-sm scroller-primary">
								        	@foreach($propertiesOutDate as $property)
										        <tr>
										            <td class="text-left col-xs-3">{{ $property->ref }}</td>
										            <td class="text-left col-xs-3">{{ $property->nombre }}</td>
										            <td class="text-left col-xs-3">{{ $property->fechaFinalizacion->format('d/m/Y') }}</td>
										            <td class="text-right col-xs-3 ">
										            	<a href="{{ route('inmuebles.edit', $property->id) }}" class="btn btn-xs btn-info">Editar inmueble</a>
									            	</td>
										        </tr>
									        @endforeach
								        </tbody>
								    </table>
								</div>
	                        </div>
	                    </div>
	                </div>
                @endif
                @if(count($inversiones) > 0)
	                <div class="col-sm-12 col-xl-6">
	                	<div class="panel">
	                		<div class="panel-heading">
	                            <span class="panel-title">Últimas 25 inversiones</span>
	                        </div>
	                        <div class="panel-body">
								<div class="table-responsive">
								    <table class="table table-striped table-fixed">
								        <thead class="bg-dark">
								        <tr>
								            <th class="br-t-n col-xs-3">Usuario</th>
								            <th class="br-t-n col-xs-3">Inmueble</th>
								            <th class="br-t-n col-xs-3">Cantidad</th>
								            <th class="br-t-n col-xs-3">Opciones</th>
								        </tr>
								        </thead>
								        <tbody>
								        	@foreach($inversiones as $inversion)
										        <tr>
										            <td class="text-left col-xs-3">{{ $inversion->user->nombre }} {{ $inversion->user->apellido1 }}</td>
										            <td class="text-left col-xs-3">{{ $inversion->propertyUser()->nombre }}</td>
										            <td class="text-left col-xs-3">{{ $inversion->money }} €</td>
										            <td class="text-left col-xs-3">
										            	<a href="{{ route('dashboard.users.edit', $inversion->user->id) }}"><span class="label label-info ml5">Ver usuario</span></a>
									            	</td>
										        </tr>
									        @endforeach
								        </tbody>
								    </table>
								</div>
	                        </div>
	                    </div>
	                </div>
                @endif
            </div>
		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}