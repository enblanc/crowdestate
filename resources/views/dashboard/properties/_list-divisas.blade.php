<!doctype html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://cdn.tailwindcss.com"></script>
</head>

<body>
    <div class="bg-white overflow-hidden rounded-lg">
        <div class="px-4 py-5 sm:p-6">
            <div class="px-4 sm:px-6 lg:px-8 space-y-5">
                <div class="sm:flex sm:items-center">
                    <div class="sm:flex-auto">
                        <h1 class="text-xl font-semibold text-gray-900">
                            {{ $datos->propiedad_name }}
                        </h1>
                        <p class="mt-2 text-xl text-gray-700">
                            TOTAL_INVERTIDO: <span class="bg-yellow-400 font-semibold">{{ number_format($datos->cantidadAbonarTotal, 2, ",", ".") }}</span> <br>
                            OBJETIVO: <span class="bg-yellow-400 font-semibold">{{ number_format($datos->objetivo, 2, ",", ".") }}</span> <br>
                        </p>
                    </div>
                </div>
                <div class="flex items-center gap-5">
                    <div>
                        <ul>
                            <li>
                                <span class="text-xl font-semibold text-gray-500">NO RESIDENTE</span>
                            </li>
                            <li>(<strong>INVERSIÓN_CLIENTE</strong> * <strong>TOTAL_A_ABONAR</strong>) / <strong>OBJETIVO</strong></li>
                        </ul>
                    </div>
                    <div>
                        <ul>
                            <li>
                                <span class="text-xl font-semibold text-gray-500">NO RESIDENTE</span>
                            </li>
                            <li>
                                ((<strong>INVERSIÓN_CLIENTE</strong> * <strong>TOTAL_A_ABONAR</strong>) / <strong>OBJETIVO</strong>) * <strong>0.81</strong> <span class="text-gray">(QUE ES -19% POR SER RESIDENTE)</span>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="-mx-4 mt-10 ring-1 ring-gray-300 sm:-mx-6 md:mx-0 md:rounded-lg">
                    <table class="min-w-full divide-y divide-gray-300">
                        <thead>
                            <tr>
                                <th scope="col" class="py-3.5 w-1 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                    property_id
                                </th>
                                <th scope="col"
                                    class="hidden px-3 w-1 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell">
                                    user_id
                                </th>
                                <th scope="col"
                                    class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell">
                                    Nombre Completo
                                </th>
                                <th scope="col"
                                    class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell">
                                    Pagado
                                </th>
                                {{-- <th scope="col"
                                    class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell">
                                    Cant.Inversiones
                                </th> --}}
                                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                    Invertido
                                </th>
                                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                    Residente
                                </th>
                                <th scope="col"
                                    class="hidden px-3 py-3.5 text-left text-sm font-semibold text-gray-900 lg:table-cell">
                                    %
                                </th>
                                <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                    A pagar
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($datos->datos_users as $duser)
                                <tr>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell">{{ $datos->propiedad_id }}</td>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell">{{ $duser['user_id'] }}</td>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell">{{ $duser['user_nombre'] }}</td>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell font-bold {{ $duser['pagado'] ? 'bg-yellow-300' : 'bg-red-300' }}">{{ $duser['pagado'] ? 'SI' : 'NO' }}</td>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell">{{ number_format($duser['totalInvertido'], 2, ",", ".") }}</td>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell">{{ $duser['tribute'] ? 'TRUE' : 'FALSE'}}</td>
                                    <td class="hidden px-3 text-sm text-gray-500 lg:table-cell">{{ number_format(($duser['totalInvertido'] * 100)/$datos->cantidadAbonarTotal, 2, ",", ".") }}%</td>
                                    <td class="hidden px-3 text-sm text-gray-500 font-semibold lg:table-cell">{{ number_format($duser['cantidadAbonarUsuario'], 2, ",", ".") }}</td>
                                </tr>
                            @endforeach
                            <!-- More plans... -->
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
