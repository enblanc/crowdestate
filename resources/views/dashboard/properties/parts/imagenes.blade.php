<div class="row">
    <dropzone   ref="dropzoneEl"
                id="myVueDropzone" 
                url="{{ route('inmuebles.photos.store', $property->id) }}"
                v-on:vdropzone-success="showSuccess"
                :use-custom-dropzone-options="true"
                :dropzone-options="dzOptions"
                :use-font-awesome="true"
                :max-file-size-in-m-b="5"
                v-on:vdropzone-error="removeFilePhoto">
    </dropzone>
</div>

<div class="row">
	
     <photo-grid :photos="photos"></photo-grid>

</div>