<div class="row">
	<div v-if="property.resumen">
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Periodo de Financiación</label>
				<input type="number" class="form-control" name="periodo_financiacion" v-model="property.periodo_financiacion" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Horizonte temporal</label>
				<input type="text" class="form-control" name="horizonte_temporal" v-model="property.horizonte_temporal" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Valor de Mercado</label>
				<input type="number" class="form-control" name="valor_mercado" v-model="property.valor_mercado" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Valor de Compra</label>
				<input type="number" class="form-control" name="valor_compra" v-model="property.valor_compra" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Coste Total</label>
				<input type="number" class="form-control" name="coste_total" v-model="property.coste_total" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Hipoteca</label>
				<input type="number" class="form-control" name="hipoteca" v-model="property.hipoteca" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Objetivo</label>
				<input type="number" class="form-control" name="objetivo" v-model="property.objetivo" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Importe Mínimo</label>
				<input type="number" class="form-control" name="importe_minimo" v-model="property.importe_minimo" disabled>
		  	</div>
		</div>
		
		<div class="clearfix"></div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Rentabilidad Alquiler Bruta</label>
				<input type="number" class="form-control" name="rentabilidad_alquiler_bruta" v-model="property.rentabilidad_alquiler_bruta" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Rentabilidad Alquiler Neta</label>
				<input type="number" class="form-control" name="rentabilidad_alquiler_neta" v-model="property.rentabilidad_alquiler_neta" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Rentabilidad Objetivo Bruta</label>
				<input type="number" class="form-control" name="rentabilidad_objetivo_bruta" v-model="property.rentabilidad_objetivo_bruta" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Rentabilidad Objetivo Neta</label>
				<input type="number" class="form-control" name="rentabilidad_objetivo_neta" v-model="property.rentabilidad_objetivo_neta" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Rentabilidad Anual</label>
				<input type="number" class="form-control" name="rentabilidad_anual" v-model="property.rentabilidad_anual" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Rentabilidad Total</label>
				<input type="number" class="form-control" name="rentabilidad_total" v-model="property.rentabilidad_total" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Tasa interna rentabilidad</label>
				<input type="number" class="form-control" name="tasa_interna_rentabilidad" v-model="property.tasa_interna_rentabilidad" disabled>
		  	</div>
		</div>

		<div class="clearfix"></div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Coste obras</label>
				<input type="number" class="form-control" name="coste_obras" v-model="property.coste_obras" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Impuestos/Trámites</label>
				<input type="number" class="form-control" name="impuestros_tramites" v-model="property.impuestos_tramites" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Registro Sociedad</label>
				<input type="number" class="form-control" name="registro_sociedad" v-model="property.registro_sociedad" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Otros Costes</label>
				<input type="number" class="form-control" name="otros_costes" v-model="property.otros_costes" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Imprevistos</label>
				<input type="number" class="form-control" name="imprevistos" v-model="property.imprevistos" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Caja Mínima</label>
				<input type="number" class="form-control" name="caja_minima" v-model="property.caja_minima" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Honorarios CrowdEstate</label>
				<input type="number" class="form-control" name="honorarios" v-model="property.honorarios" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Ingresos Anuales Alquiler</label>
				<input type="number" class="form-control" name="ingresos_anuales_alquiler" v-model="property.ingresos_anuales_alquiler" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Importe Venta</label>
				<input type="number" class="form-control" name="importe_venta" v-model="property.importe_venta" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Compraventa Margen Bruto</label>
				<input type="number" class="form-control" name="compra_venta_margen_bruto" v-model="property.compra_venta_margen_bruto" disabled>
		  	</div>
		</div>
	</div>
	<hr class="col-md-12">
	<div class="clearfix"></div>
	<div class="alert alert-dark alert-dismissable">
		<button type="button" class="pull-right btn btn-sm btn-primary" data-dismiss="alert" aria-hidden="true">Entendido</button>
		<i class="fa fa-info pr10"></i>
		Estos datos solo se pueden modificar subiendo un nuevo archivo excel de resumen.
	</div>
	<div class="allcp-form theme-primary col-md-6">
		<h5>Añadir un nuevo archivo de resumen</h5>
		<div class="col-md-8 form-group form-group-default">
			<label>Archivo de resumen</label>
			<label class="field prepend-icon append-button file">
				<span class="button btn-primary">Elegir archivo excel</span>
				<input type="file" class="gui-file" name="resumen" id="file1" @change="uploadResumenFile">
				<input type="text" class="gui-input" id="upload-resumen-file" placeholder="Ninguno">
				<label class="field-icon">
					<i class="fa fa-cloud-upload"></i>
				</label>
			</label>
			<template v-for="(error, key) in resumen_errors">
				<em for="upload-resumen-file" class="state-error" v-html="error[0]"></em>
			</template>
			
		</div>
		<div class="col-md-4">
			<div class="text-center mt25">
				<pulse-loader :loading="resumen.loading" :color="color" :size="size"></pulse-loader>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<h5>Archivos de resumenes subidos</h5>
		@if ($property->getMedia($property->mediaCollectionExcelResumen)->count() > 0)
		<table class="table table-responsive footable"  data-sorting="true">
			<thead>
				<tr class="system">
					<th data-type="string">Archivo</th>
					<th data-sorted="true" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY">Fecha subida</th>
					<th data-sortable="false">Descargar</th>
				</tr>
			</thead>
			<tbody>
				@foreach($property->getMedia($property->mediaCollectionExcelResumen) as $excel)
				<tr>
					<td>{{ $excel->name }}</td>
					<td>{{ $excel->getCustomProperty('date') }}</td>
					<td><a href="{{ $excel->getUrl() }}" class="btn btn-system" download>Descargar</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<p>No hay archivos subidos</p>
		@endif
	</div>
</div>