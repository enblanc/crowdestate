<div class="row">
    <div class="tab-block mb25">
        <ul class="nav nav-tabs">
            @foreach($locales as $locale)
                <li @if($locale->iso == 'es') class="active" @endif>
                    <a href="#tab_docs_{{ $locale->iso }}"  data-ref="docs_{{ $locale->iso }}" data-toggle="tab">Documentos {{ $locale->language }}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content br-grey">
            @foreach($locales as $locale)
                <div id="tab_docs_{{ $locale->iso }}" class="tab-pane col-md-12 @if($locale->iso == 'es') active @endif" style="min-height: 800px">
                    <div class="col-md-6 br-r">
                        <h4>Documentos Públicos</h4>
                        <div class="col-md-12">
                            <dropzone   ref="dropzoneDocPublic_{{ $locale->iso }}"
                                        id="dropzone-documents-publicos_{{ $locale->iso }}" 
                                        url="{{ route('inmuebles.documents.public.store', $property->id) }}"
                                        v-on:vdropzone-success="showSuccessDocumentPublic"
                                        :use-custom-dropzone-options="true"
                                        :dropzone-options="documentUploadOptions.{{ $locale->iso }}"
                                        :use-font-awesome="true"
                                        :max-file-size-in-m-b="15"
                                        v-on:vdropzone-error="removeFileDocumentPublic">
                            </dropzone>
                        </div>
                        @php
                            //Continuar aquí. No se por qué no pasa  los datos a la variable $iso;
                        @endphp
                        <div class="col-md-12">
                            <div class="mt20">
                                 <documents-grid :documents="property.documents.{{ $locale->iso }}.public" locale-lang="{{ $locale->iso }}"></documents-grid>
                            </div>
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <h4>Documentos usuarios registrados</h4>
                        <div class="col-md-12">
                            <dropzone   ref="dropzoneDocPrivate_{{ $locale->iso }}"
                                        id="dropzone-documents-private_{{ $locale->iso }}" 
                                        url="{{ route('inmuebles.documents.private.store', $property->id) }}"
                                        v-on:vdropzone-success="showSuccessDocumentPrivate"
                                        :use-custom-dropzone-options="true"
                                        :dropzone-options="documentUploadOptions.{{ $locale->iso }}"
                                        :use-font-awesome="true"
                                        :max-file-size-in-m-b="15"
                                        v-on:vdropzone-error="removeFileDocumentPrivate">
                            </dropzone>
                        </div>
                        <div class="col-md-12">
                            <div class="mt20">
                                 <documents-grid :documents="property.documents.{{ $locale->iso }}.private" locale-lang="{{ $locale->iso }}"></documents-grid>
                            </div>
                        </div>
                        
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</div>
