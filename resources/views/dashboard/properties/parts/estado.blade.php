<div class="row">
	<div class="alert alert-dark alert-dismissable">
		<button type="button" class="pull-right btn btn-sm btn-primary" data-dismiss="alert" aria-hidden="true">Entendido</button>
		<i class="fa fa-info pr10"></i>
		Desde esta sección podrás cambiar el estado del inmueble. Recuerda estar seguro del cambio pues esta acción no se puede revertir.
	</div>

	<div class="alert alert-warning">
		<i class="fa fa-info pr10"></i>
		El estado actual del inmueble es: <strong>@{{ property.state.nombre }}</strong>
		@if($property->remainingDays < 0)
		- <strong>¡Sin embargo hemos detectado que el inmueble se ha pasado de la fecha de finalización: {{ $property->fechaFinalizacion->format('d/m/Y') }}!</strong>
		@endif
	</div>


	{!! Form::model($property, ['route' => ['inmuebles.update', $property->id], 'method' => 'put', 'files' => true, 'id' => 'update-estado-form' ]) !!}
	
		<div class="col-md-3">
			<div class="form-group form-group-default">
				<label>Estado inmueble</label>
				<select class="form-control show-arrows" name="property_state_id" v-model="property.state.id" v-on:change="checkEstado" required>
					<option value="">-- Selecciona una opción--</option>
					@foreach($states as $state)
						<option value="{{ $state->id }}" :disabled="checkEstadoDisabled({{ $state->id }})">{{ $state->nombre }}</option>
					@endforeach
				</select>
				<template v-for="(error, key) in estado_errors">
					<em for="property_state_id" class="state-error" v-html="error"></em>
				</template>
		  	</div>
		  	
		</div>

		<div class="col-md-12">
			<button type="button" v-on:click="confirmData" id="guardar" class="btn btn-primary btn-bordered ladda-button" data-style="zoom-in">
				<span class="ladda-label">Actualizar estado del inmueble</span>
				<span class="ladda-spinner"></span>
			</button>
		</div>
	{!! Form::close() !!}
	
</div>

<div class="row">

</div>