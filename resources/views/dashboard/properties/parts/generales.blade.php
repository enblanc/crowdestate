@if (isset($property))
    {!! Form::model($property, [
        'route' => ['inmuebles.update', $property->id],
        'method' => 'put',
        'files' => true,
        'id' => 'update-property-form',
    ]) !!}
@else
    {!! Form::open([
        'route' => 'inmuebles.store',
        'method' => 'post',
        'files' => true,
        'id' => 'create-property-form',
    ]) !!}
@endif
@if (count($errors) > 0)
    <div class="alert alert-danger alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{!! $error !!}</li>
            @endforeach
        </ul>
    </div>
@endif

@if (isset($property))
    <div class="alert alert-warning" v-if="property.private">
        <i class="fa fa-info pr10"></i>
        Este inmueble será accesible desde la url: {{ route('front.property.show', $property->slug) }} <br>
        Sin embargo, a pesar de que sea privado, para que se muestre tiene estar publicado. Es decir, tiene que tener
        imágenes, haber subido el excel del resumen, el de la estimación y que la fecha de publicación sea igual o menor
        que el día actual.
    </div>
@endif


<div class="row">
    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Ref</label>
            <input type="text" class="form-control" name="ref" v-model="property.ref" required maxlength="10">
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-group-default">
            <label>Nombre</label>
            <input type="text" class="form-control" name="nombre" v-model="property.nombre" required>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group form-group-default">
            <label>Slug</label>
            <input type="text" class="form-control" name="slug" v-model="property.slug" required>
        </div>
    </div>
    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Fecha Publicación</label>
            <input type="text" class="form-control datepicker" name="fecha_publicacion"
                v-model="property.fecha_publicacion" required>
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group form-group-default">
            <label>Catastro</label>
            <input type="text" class="form-control" name="catastro" v-model="property.catastro" minlength="20"
                maxlength="20">
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group form-group-default">
            <label>Ciudad</label>
            <input type="text" class="form-control" name="ubicacion" v-model="property.ubicacion" required>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Antigüedad</label>
            <input type="number" min="1900" max="2599" step="1" class="form-control" name="antiguedad"
                v-model="property.antiguedad">
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group form-group-default">
            <label>Barrio</label>
            <input type="text" class="form-control" name="barrio" v-model="property.barrio" required>
        </div>
    </div>

    <div class="col-md-10">
        <div class="form-group form-group-default">
            <label>Dirección</label>
            <div class="input-group">
                <input type="text" class="form-control" name="direccion" v-model="property.direccion" required>
                <em for="searchBox" class="state-error" v-if="searchError">No se encuentra la búsqueda</em>
                <div class="input-group-addon pn">
                    <button class="btn btn-default light btn-block" v-on:click.prevent.default="searchPlace">Buscar
                        direccion en mapa</button>
                </div>
            </div>

            <input type="hidden" name="lat" v-model="property.lat">
            <input type="hidden" name="lng" v-model="property.lng">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label class="fluid-width">Zona Premium</label>
            <div class="switch switch-primary switch-inline">
                <input type="hidden" name="zona_premium" v-model="isPremiumZone">
                <input id="zona_premium" type="checkbox" checked="" v-model="property.zona_premium">
                <label for="zona_premium" data-on="SÍ" data-off="NO"></label>
            </div>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group form-group-default">
            <label>Tipo inmueble</label>
            <select class="form-control show-arrows" name="property_type_id" v-model="property.type.id" required>
                <option value="">-- Selecciona una opción--</option>
                @foreach ($types as $type)
                    <option value="{{ $type->id }}">{{ $type->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>



    <div class="col-md-3">
        <div class="form-group form-group-default">
            <label>Tipo inversión</label>
            <select class="form-control show-arrows" name="investment_id" v-model="property.investment.id" required>
                <option value="">-- Selecciona una opción--</option>
                @foreach ($investments as $investment)
                    <option value="{{ $investment->id }}">{{ $investment->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3" v-if="property.developer">
        <div class="form-group form-group-default">
            <label>Promotor</label>
            <select class="form-control show-arrows" name="developer_id" v-model="property.developer.id" required>
                <option value="">-- Selecciona una opción--</option>
                @foreach ($developers as $developer)
                    <option value="{{ $developer->id }}">{{ $developer->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>

    <div class="col-md-3">
        <div class="form-group form-group-default">
            <label>Orden</label>
            <select class="form-control show-arrows" name="orden" v-model="property.orden">
                <option value="">-- Selecciona una opción--</option>
                @for ($i = 1; $i < 101; $i++)
                    <option value="{{ $i }}">{{ $i }}</option>
                @endfor
            </select>
        </div>
    </div>

    <div class="col-md-12">
        <hr class="mt10 mb20">
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Habitaciones</label>
            <input type="number" class="form-control ui-spinner-input spinner" name="habitaciones"
                v-model="property.habitaciones">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Toilets</label>
            <input type="number" class="form-control" name="toilets" v-model="property.toilets">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Metros</label>
            <input type="number" class="form-control" name="metros" v-model="property.metros" required>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Garaje</label>
            <input type="number" class="form-control" name="garaje" v-model="property.garaje">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Piscina</label>
            <input type="number" class="form-control" name="piscina" v-model="property.piscina">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Ascensor</label>
            <input type="number" class="form-control" name="ascensor" v-model="property.ascensor">
        </div>
    </div>

    <div class="col-md-10">
        <div class="form-group form-group-default">
            <label>Gestor</label>
            <input type="text" class="form-control" name="gestor" v-model="property.gestor" required>
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label class="fluid-width">Inmueble Privado</label>
            <div class="switch switch-primary switch-inline">
                <input type="hidden" name="private" v-model="isPrivate">
                <input id="private" type="checkbox" checked="" v-model="property.private">
                <label for="private" data-on="SÍ" data-off="NO"></label>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <hr class="mt10 mb20">
    </div>
    <div class="col-md-12">
        <h6>Estado de inversión</h6>
    </div>
    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Núm. Inversores</label>
            <input type="number" step="1" class="form-control" name="fake_inversores"
                v-model="property.fake_inversores">
        </div>
    </div>

    <div class="col-md-2">
        <div class="form-group form-group-default">
            <label>Cantidad invertida</label>
            <input type="number" step="any" class="form-control" name="fake_invertido"
                v-model="property.fake_invertido">
        </div>
    </div>

    <div class="col-md-12">
        <hr class="mt10 mb20">
    </div>
    <div class="col-md-12">
        <h6>Otrs títulos y slugs</h6>
    </div>

    @if (isset($details))
        @foreach ($locales as $locale)
            @if ($locale->iso !== 'es')
                <div class="col-md-2">
                    <div class="form-group form-group-default">
                        <label>Nombre {{ $locale->language }}</label>
                        <input type="text" class="form-control" name="{{ $locale->iso }}[title]"
                            value="{{ checkValueTranslated($locale->iso . '[title]', $details, $locale->iso, 'title') }}">
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group form-group-default">
                        <label>Slug {{ $locale->language }}</label>
                        <input type="text" class="form-control" name="{{ $locale->iso }}[slug]"
                            value="{{ checkValueTranslated($locale->iso . '[slug]', $details, $locale->iso, 'slug') }}">
                    </div>
                </div>
            @endif
        @endforeach
    @endif
    {{-- {{ checkValueTranslated($iso.'[descripcion]', $details, $iso, 'descripcion') }} --}}
</div>

<hr>
<div class="row">
    <div class="col-xs-12">
        <div id="mapa" v-if="showMap" class="map"></div>
    </div>

</div>
<div class="row">
    <div class="form-group form-group-default col-md-12 mt50">
        <button type="button" id="cancelar" class="btn btn-bordered btn-default ">Cancelar</button>
        @if (isset($property))
            <button type="submit" id="guardar" class="btn btn-primary btn-bordered ladda-button"
                data-style="zoom-in">
                <span class="ladda-label">Actualizar datos</span>
                <span class="ladda-spinner"></span>
            </button>
        @else
            <button type="submit" id="guardar" class="btn btn-primary btn-bordered ladda-button"
                data-style="zoom-in">
                <span class="ladda-label">Crear inmueble</span>
                <span class="ladda-spinner"></span>
            </button>
        @endif
    </div>
</div>
{!! Form::close() !!}
