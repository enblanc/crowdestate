<evolucion-table :evolucion="property.currentEvolution" :year="evolucion.years.selected">
	<template slot="year-selector">
		<label>Selecciona un año</label>
		<select id="location" name="location" v-on:change="cambiaTablaEvolucion">
			@foreach($property->availableEvolutionsYears as $year)
				<option value="{{ $year }}" {{ ($year == date('Y')) ? 'selected' : '' }}>{{ $year }}</option>
			@endforeach
		</select>
		<div class="pull-right" v-if="evolucion.loading">
			<pulse-loader :loading="evolucion.loading" :color="color" :size="evolucion.size"></pulse-loader>
		</div>
	</template>
</evolucion-table>

<hr class="col-md-12">

<div class="clearfix"></div>
<div class="alert alert-dark alert-dismissable">
	<button type="button" class="pull-right btn btn-sm btn-primary" data-dismiss="alert" aria-hidden="true">Entendido</button>
	<i class="fa fa-info pr10"></i>
	Estos datos solo se pueden modificar subiendo un nuevo archivo excel de seguimiento.
</div>
<div class="allcp-form theme-primary col-md-7">
	<h5>Añadir un nuevo archivo de seguimiento</h5>
	<div class="help-block">
		<i class="fa fa-info"></i> Seleccióna el mes del que quieres actualizar los datos y elige el archivo excel
	</div>
	<div class="col-md-3 form-group  form-group-default">
		<label>Elige el mes</label>
		<label class="field select">
			<select id="evolution-file" v-model="evolucion.months.selected">
				<option v-for="option in evolucion.months.options" v-bind:value="option.value">
				    @{{ option.text }}
			  	</option>
			</select>
			<i class="arrow"></i>
		</label>
	</div>

	<div class="col-md-3 form-group  form-group-default">
		<label>Elige el año</label>
		<label class="field select">
			<select id="evolution-file" v-model="evolucion.years.selected">
				<option v-for="year in evolucion.years.options" v-bind:value="year">
				    @{{ year }}
			  	</option>
			</select>
			<i class="arrow"></i>
		</label>
	</div>
	<div class="col-md-6 form-group form-group-default">
		<label>Archivo de seguimiento</label>
		<label class="field prepend-icon append-button file">
			<span class="button btn-primary">Elegir archivo</span>
			<input type="file" class="gui-file" name="evolucion" id="file1" @change="uploadEvolutionFile">
			<input type="text" class="gui-input" id="upload-evolucion-file" placeholder="Ninguno">
			<label class="field-icon">
				<i class="fa fa-cloud-upload"></i>
			</label>
		</label>
		<template v-for="(error, key) in evolucion_errors">
			<em for="upload-evolucion-file" class="state-error" v-html="error[0]"></em>
		</template>
		
	</div>
	<div class="col-md-3">
		<div class="text-center mt25">
			<pulse-loader :loading="evolucion.upload.loading" :color="color" :size="size"></pulse-loader>
		</div>
	</div>
</div>
<div class="col-md-5">
	<h5>Archivos de seguimiento subidos</h5>
	@if ($property->evolutions->count() > 0)
	<table class="table table-responsive footable"  data-sorting="true">
		<thead>
			<tr class="system">
				<th data-type="string">Archivo</th>
				<th data-sorted="true" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY">Fecha subida</th>
				<th>Mes</th>
				<th data-sortable="false">Descargar</th>
			</tr>
		</thead>
		<tbody>
			@foreach($property->evolutions as $evolucionData)
				@php
					$excel = $evolucionData->media;	
				@endphp
				@if($excel)
				<tr>
					<td>{{ $excel->name }}</td>
					<td>{{ $excel->getCustomProperty('date') }}</td>
					<td>{{ ucfirst($evolucionData->fecha->format('F')) }}</td>
					<td><a href="{{ $excel->getUrl() }}" class="btn btn-system" download>Descargar</a></td>
				</tr>
				@endif
			@endforeach
		</tbody>
	</table>
	@else
		<p>No hay archivos subidos</p>
	@endif
</div>