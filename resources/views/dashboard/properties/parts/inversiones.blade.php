<div>
	<div v-if="!inversores.loading">
		<div class="alert alert-dark alert-dismissable" v-if="!inversores.users">
			<button type="button" class="pull-right btn btn-sm btn-primary" data-dismiss="alert" aria-hidden="true">Entendido</button>
			<i class="fa fa-info pr10"></i>
			Este inmueble todavía no tiene inversiones. Las inversiones se mostrarán en esta pestaña.
		</div>

		<div v-if="inversores.users">
			<div class="pull-left">
				<h5>Listado de Inversores</h5>
			</div>
			<div class="pull-right">
				<a href="{{ route('property.inversores.export', $property->id) }}" target="_blank">
					Exportar
				</a>
			</div>

			<table class="table table-responsive footable" data-paging="true" data-paging-size="30" data-sorting="true">
				<thead>
					<tr class="system">
						<th data-type="string">Nombre</th>
						<th data-type="number">Inversión</th>
						<th data-sorted="false" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY hh:mm">Fecha y hora</th>
						<th data-sortable="true">Opciones</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="user in inversores.users">
						<td>@{{ user.nombre }} @{{ user.apellido1 }} @{{ user.apellido2 }}</td>
						<td class="h5 c-2 nowrap text-right append-euro">@{{ user.total }}</td>
						<td>@{{ user.created_at }}</td>
						<td>
							<a :href="user.route_edit" class="btn btn-xs btn-info" target="_blank">
								Ver usuario
							</a>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div v-else>
		<pulse-loader :loading="inversores.loading" :color="color" :size="size"></pulse-loader>
	</div>
</div>