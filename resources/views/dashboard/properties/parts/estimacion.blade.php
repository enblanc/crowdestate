<div class="row">
	<div v-if="property.resumen">
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Número de reservas</label>
				<input type="number" class="form-control" name="numero_reservas" v-model="estimacion_data.numero_reservas" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Grado de ocupación</label>
				<input type="text" class="form-control" name="grado_ocupacion" v-model="estimacion_data.grado_ocupacion" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Días reservados</label>
				<input type="number" class="form-control" name="dias_reservados" v-model="estimacion_data.dias_reservados" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Días no reservados</label>
				<input type="number" class="form-control" name="dias_no_reservados" v-model="estimacion_data.dias_no_reservados" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Días bloqueados</label>
				<input type="number" class="form-control" name="dias_bloqueados" v-model="estimacion_data.dias_bloqueados" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Tarifa Media día</label>
				<input type="number" class="form-control" name="tarifa_media_dia" v-model="estimacion_data.tarifa_media_dia" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Estimación ingresos anuales</label>
				<input type="number" class="form-control" name="estimacion_ingresos_anuales" v-model="estimacion_data.estimacion_ingresos_anuales" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Satisfacción clientes</label>
				<input type="number" class="form-control" name="satisfaccion_clientes" v-model="estimacion_data.satisfaccion_clientes" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Número de invitados</label>
				<input type="number" class="form-control" name="satisfaccion_clientes" v-model="estimacion_data.numero_invitados" disabled>
		  	</div>
		</div>

		<div class="col-md-12">
			<hr class="mt10 mb20">
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Ingresos totales</label>
				<input type="number" class="form-control" name="ingresos_totales" v-model="estimacion_data.ingresos_totales" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Ingresos alquiler</label>
				<input type="number" class="form-control" name="ingresos_alquiler" v-model="estimacion_data.ingresos_alquiler" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Ingresos venta</label>
				<input type="number" class="form-control" name="ingresos_venta" v-model="estimacion_data.ingresos_venta" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Gastos explotación</label>
				<input type="number" class="form-control" name="gastos_explotacion" v-model="estimacion_data.gastos_explotacion" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Publicidad prensa</label>
				<input type="number" class="form-control" name="publicidad_prensa" v-model="estimacion_data.publicidad_prensa" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Otro material</label>
				<input type="number" class="form-control" name="otro_material" v-model="estimacion_data.otro_material" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Gastos ventas</label>
				<input type="number" class="form-control" name="gastos_ventas" v-model="estimacion_data.gastos_ventas" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>I + D</label>
				<input type="number" class="form-control" name="i_mas_d" v-model="estimacion_data.i_mas_d" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Arrendamientos</label>
				<input type="number" class="form-control" name="arrendamientos" v-model="estimacion_data.arrendamientos" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Comisiones CrowdEstate</label>
				<input type="number" class="form-control" name="comisiones_crowdestate" v-model="estimacion_data.comisiones_crowdestate" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Gestión</label>
				<input type="number" class="form-control" name="gestion" v-model="estimacion_data.gestion" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Suministros</label>
				<input type="number" class="form-control" name="suministros" v-model="estimacion_data.suministros" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>IBI</label>
				<input type="number" class="form-control" name="ibi" v-model="estimacion_data.ibi" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Otras tasas</label>
				<input type="number" class="form-control" name="otras_tasas" v-model="estimacion_data.otras_tasas" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Amortizacion</label>
				<input type="number" class="form-control" name="amortizacion" v-model="estimacion_data.amortizacion" disabled>
		  	</div>
		</div>


		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Subenciones</label>
				<input type="number" class="form-control" name="subvenciones" v-model="estimacion_data.subvenciones" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Enajenación Inmovilizado</label>
				<input type="number" class="form-control" name="enajenacion_inmovilizado" v-model="estimacion_data.enajenacion_inmovilizado" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Resultado explotación</label>
				<input type="number" class="form-control" name="resultado_explotacion" v-model="estimacion_data.resultado_explotacion" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Resultado financiero</label>
				<input type="number" class="form-control" name="resultado_financiero" v-model="estimacion_data.resultado_financiero" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Ingresos financieros</label>
				<input type="number" class="form-control" name="ingresos_financieros" v-model="estimacion_data.ingresos_financieros" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Gastos financieros</label>
				<input type="number" class="form-control" name="subvenciones" v-model="estimacion_data.gastos_financieros" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Otros instrumentos financieros</label>
				<input type="number" class="form-control" name="otros_instrumentos_financieros" v-model="estimacion_data.otros_instrumentos_financieros" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Resultado antes impuestos</label>
				<input type="number" class="form-control" name="resultado_antes_impuestos" v-model="estimacion_data.resultado_antes_impuestos" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Impuestos</label>
				<input type="number" class="form-control" name="impuestos" v-model="estimacion_data.impuestos" disabled>
		  	</div>
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Resultados del ejercicio</label>
				<input type="number" class="form-control" name="resultados_ejercicio" v-model="estimacion_data.resultados_ejercicio" disabled>
		  	</div>
		</div>

		<div class="col-md-12">
			<hr class="mt10 mb20">
		</div>

		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Dividendos generados</label>
				<input type="number" class="form-control" name="dividendos_generados" v-model="estimacion_data.dividendos_generados" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Dividendos abonar</label>
				<input type="number" class="form-control" name="dividendos_abonar" v-model="estimacion_data.dividendos_abonar" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Total dividendos abonados</label>
				<input type="number" class="form-control" name="total_dividendos_abonados" v-model="estimacion_data.total_dividendos_abonados" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Dividendos cartera</label>
				<input type="number" class="form-control" name="dividendos_cartera" v-model="estimacion_data.dividendos_cartera" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Total dividendos devengados</label>
				<input type="number" class="form-control" name="total_dividendos_devengados" v-model="estimacion_data.total_dividendos_devengados" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Revalorización ibmueble</label>
				<input type="number" class="form-control" name="revalorazacion_inmueble" v-model="estimacion_data.revalorazacion_inmueble" disabled>
		  	</div>
		</div>
		<div class="col-md-2">
			<div class="form-group form-group-default">
				<label>Valor actual inmueble</label>
				<input type="number" class="form-control" name="valor_actual_inmueble" v-model="estimacion_data.valor_actual_inmueble" disabled>
		  	</div>
		</div>
	</div>
	<hr class="col-md-12">
	<div class="clearfix"></div>
	<div class="alert alert-dark alert-dismissable">
		<button type="button" class="pull-right btn btn-sm btn-primary" data-dismiss="alert" aria-hidden="true">Entendido</button>
		<i class="fa fa-info pr10"></i>
		Estos datos solo se pueden modificar subiendo un nuevo archivo excel de estimación.
	</div>
	<div class="allcp-form theme-primary col-md-6">
		<h5>Añadir un nuevo archivo de estimación</h5>
		<div class="col-md-8 form-group form-group-default">
			<label>Archivo de estimación</label>
			<label class="field prepend-icon append-button file">
				<span class="button btn-primary">Elegir archivo excel</span>
				<input type="file" class="gui-file" name="estimacion" id="file1" @change="uploadEstimacionFile">
				<input type="text" class="gui-input" id="upload-estimacion-file" placeholder="Ninguno">
				<label class="field-icon">
					<i class="fa fa-cloud-upload"></i>
				</label>
			</label>
			<template v-for="(error, key) in resumen_errors">
				<em for="upload-estimacion-file" class="state-error" v-html="error[0]"></em>
			</template>
			
		</div>
		<div class="col-md-4">
			<div class="text-center mt25">
				<pulse-loader :loading="estimacion.loading" :color="color" :size="size"></pulse-loader>
			</div>
		</div>
	</div>
	<div class="col-md-6">
		<h5>Archivos de estimación subidos</h5>
		@if ($property->getMedia($property->mediaCollectionExcelEstimacion)->count() > 0)
		<table class="table table-responsive footable"  data-sorting="true">
			<thead>
				<tr class="system">
					<th data-type="string">Archivo</th>
					<th data-sorted="true" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY">Fecha subida</th>
					<th data-sortable="false">Descargar</th>
				</tr>
			</thead>
			<tbody>
				@foreach($property->getMedia($property->mediaCollectionExcelEstimacion) as $excel)
				<tr>
					<td>{{ $excel->name }}</td>
					<td>{{ $excel->getCustomProperty('date') }}</td>
					<td><a href="{{ $excel->getUrl() }}" class="btn btn-system" download>Descargar</a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
		@else
			<p>No hay archivos subidos</p>
		@endif
	</div>
</div>