{!! Form::model($property, ['route' => ['inmuebles.detalles', $property->id], 'method' => 'put', 'files' => true, 'id' => 'update-property-ficha-form' ]) !!}

<input type="hidden" name="details" value="1">

<div class="tab-block mb25">
    <ul class="nav nav-tabs">
        @foreach($locales as $locale)
			<li @if($locale->iso == 'es') class="active" @endif>
            	<a href="#tab_ficha_{{ $locale->iso }}" data-toggle="tab">Descripción {{ $locale->language }}</a>
        	</li>
		@endforeach
    </ul>
    <div class="tab-content br-grey">
    	@foreach($locales as $locale)
    		@php
    			$iso = $locale->iso;
    		@endphp
    		<div id="tab_ficha_{{ $iso }}" class="tab-pane col-md-12 @if($iso == 'es') active @endif">

	    		{{-- Aquí va el contenido para cada idioma --}}

	    		<div class="form-group form-group-default">
					<label>Descripción</label>
				    <textarea name="{{ $iso }}[descripcion]" id="descripcion_{{ $iso }}">{{ checkValueTranslated($iso.'[descripcion]', $details, $iso, 'descripcion') }}</textarea>
				</div>
				<div class="form-group form-group-default">
					<label>Información del inmueble</label>
				    <textarea name="{{ $iso }}[ficha]" id="ficha_{{ $iso }}">{{ checkValueTranslated($iso.'[ficha]', $details, $iso, 'ficha') }}</textarea>
				</div>
				<div class="form-group form-group-default">
					<label>Rendimiento de mercado</label>
				    <textarea name="{{ $iso }}[rendimiento_mercado]" id="rendimiento_mercado_{{ $iso }}" >{{ checkValueTranslated($iso.'[rendimiento_mercado]', $details, $iso, 'rendimiento_mercado') }}</textarea>
				</div>
				<hr>

				<div class="row mb20">
					@php
						$url = url(config('filemanager.defaultRoute').'/dialog')."?type=featured&appendId=similares_first_".$iso;
					@endphp
					<h5>Rendimiento similar 1 -- {{ $details['es']->id }}</h5>
					<div class="col-md-3">
						<div class="fileupload fileupload-new" data-provides="fileupload">
							<input type="hidden">
						    <div class="fileupload-preview fileinput-exists thumbnail mb20" data-trigger="fileinput" style="line-height: 205px;">
						        <img src="{{ isset($details[$iso]) ? url($details[$iso]->getFirstRelatedImage()) : '' }}" data-src="holder.js/100px250?text=Imagen Relacionado 1" id="similares_first_{{ $iso }}-image" alt="Imagen Relacionado 1" id="similares_first-image" style="display: block;">
						    </div>
						    <div class="row">
						        <div class="col-xs-12">
						          	<span class="btn btn-primary btn-file btn-block">
						                <span class="fileupload-exists" onclick="uploadFile('{{ $url }}');"><i class="fa fa-cloud-upload"></i> Actualizar Foto</span>
									    <span class="fileupload-new" onclick="uploadFile('{{ $url }}');"><i class="fa fa-cloud-upload"></i> Cargar Foto</span>
									    <input type="hidden" name="{{ $iso }}[similares_first_image]" id="similares_first_{{ $iso }}-text">
						          	</span>
						        </div>
						    </div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label>Nombre</label>
								<input type="text" class="form-control" name="{{ $iso }}[similares_first_name]" value="{{ checkValueTranslated($iso.'[similares_first_name]', $details, $iso, 'similares_first_name') }}">
						  	</div>
						</div>
						<div class="col-md-12">
							<div class="form-group form-group-default">
								<label>Descripción 1</label>
								<input type="text" class="form-control" name="{{ $iso }}[similares_first_data_1]" value="{{ checkValueTranslated($iso.'[similares_first_data_1]', $details, $iso, 'similares_first_data_1') }}">
						  	</div>
						</div>
						<div class="col-md-12">
							<div class="form-group form-group-default">
								<label>Descripción 2</label>
								<input type="text" class="form-control" name="{{ $iso }}[similares_first_data_2]" value="{{ checkValueTranslated($iso.'[similares_first_data_2]', $details, $iso, 'similares_first_data_2') }}">
						  	</div>
						</div>
					</div>			                	
				</div>

				<div class="row">
					@php
						$url = url(config('filemanager.defaultRoute').'/dialog')."?type=featured&appendId=similares_second_".$iso;
					@endphp
					<h5>Rendimiento similar 2</h5>
					<div class="col-md-3">
						<div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
						    <div class="fileupload-preview fileinput-exists thumbnail mb20" data-trigger="fileinput" style="line-height: 205px;">
						        <img src="{{ isset($details[$iso]) ? url($details[$iso]->getSecondRelatedImage()) : '' }}"data-src="holder.js/100px250?text=Imagen Relacionado 2" alt="Imagen Relacionado 2" id="similares_second_{{ $iso }}-image" style="display: block;">
						    </div>
						    @if ($errors->has('similares_second_image'))
						      <em for="photo" class="state-error photo">{{ $errors->second('similares_second_image') }}</em>
						    @endif
						    <div class="row">
						        <div class="col-xs-12">
						          	<span class="btn btn-primary btn-file btn-block">
						                <span class="fileupload-exists" onclick="uploadFile('{{ $url }}');"><i class="fa fa-cloud-upload"></i> Actualizar Foto</span>
									    <span class="fileupload-new" onclick="uploadFile('{{ $url }}');"><i class="fa fa-cloud-upload"></i> Cargar Foto</span>
									    <input type="hidden" name="{{ $iso }}[similares_second_image]" id="similares_second_{{ $iso }}-text">
						          	</span>
						        </div>
						    </div>
						</div>
					</div>
					<div class="col-md-9">
						<div class="col-md-6">
							<div class="form-group form-group-default">
								<label>Nombre</label>
								<input type="text" class="form-control" name="{{ $iso }}[similares_second_name]"  value="{{ checkValueTranslated($iso.'[similares_second_name]', $details, $iso, 'similares_second_name') }}">
						  	</div>
						</div>
						<div class="col-md-12">
							<div class="form-group form-group-default">
								<label>Descripción 1</label>
								<input type="text" class="form-control" name="{{ $iso }}[similares_second_data_1]"  value="{{ checkValueTranslated($iso.'[similares_second_data_1]', $details, $iso, 'similares_second_data_1') }}">
						  	</div>
						</div>
						<div class="col-md-12">
							<div class="form-group form-group-default">
								<label>Descripción 2</label>
								<input type="text" class="form-control" name="{{ $iso }}[similares_second_data_2]"  value="{{ checkValueTranslated($iso.'[similares_second_data_2]', $details, $iso, 'similares_second_data_2') }}">
						  	</div>
						</div>
					</div>		
				</div>
				<hr>

				<div class="row">
					<h5>Primera razón</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[invertir_first_title]" value="{{ checkValueTranslated($iso.'[invertir_first_title]', $details, $iso, 'invertir_first_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[invertir_first_data]" value="{{ checkValueTranslated($iso.'[invertir_first_data]', $details, $iso, 'invertir_first_data') }}">
					</div>
				</div>

				<div class="row">
					<h5>Segunda razón</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[invertir_second_title]" value="{{ checkValueTranslated($iso.'[invertir_second_title]', $details, $iso, 'invertir_second_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[invertir_second_data]" value="{{ checkValueTranslated($iso.'[invertir_second_data]', $details, $iso, 'invertir_second_data') }}">
					</div>
				</div>

				<div class="row">
					<h5>Tercera razón</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[invertir_third_title]" value="{{ checkValueTranslated($iso.'[invertir_third_title]', $details, $iso, 'invertir_third_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[invertir_third_data]" value="{{ checkValueTranslated($iso.'[invertir_third_data]', $details, $iso, 'invertir_third_data') }}">
					</div>
				</div>
				<hr>
				<div class="row">
					<h5>Primera ventaja localización</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[localizacion_first_title]" value="{{ checkValueTranslated($iso.'[localizacion_first_title]', $details, $iso, 'localizacion_first_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[localizacion_first_data]" value="{{ checkValueTranslated($iso.'[localizacion_first_data]', $details, $iso, 'localizacion_first_data') }}">
					</div>
				</div>

				<div class="row">
					<h5>Segunda ventaja localización</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[localizacion_second_title]" value="{{ checkValueTranslated($iso.'[localizacion_second_title]', $details, $iso, 'localizacion_second_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[localizacion_second_data]" value="{{ checkValueTranslated($iso.'[localizacion_second_data]', $details, $iso, 'localizacion_second_data') }}">
					</div>
				</div>

				<div class="row">
					<h5>Tercera ventaja localización</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[localizacion_third_title]" value="{{ checkValueTranslated($iso.'[localizacion_third_title]', $details, $iso, 'localizacion_third_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[localizacion_third_data]" value="{{ checkValueTranslated($iso.'[localizacion_third_data]', $details, $iso, 'localizacion_third_data') }}">
					</div>
				</div>
				<hr>
				<div class="row">
					<h5>Mercado 1</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[mercado_first_title]" value="{{ checkValueTranslated($iso.'[mercado_first_title]', $details, $iso, 'mercado_first_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[mercado_first_data]" value="{{ checkValueTranslated($iso.'[mercado_first_data]', $details, $iso, 'mercado_first_data') }}">
					</div>
				</div>

				<div class="row">
					<h5>Mercado 2</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[mercado_second_title]" value="{{ checkValueTranslated($iso.'[mercado_second_title]', $details, $iso, 'mercado_second_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[mercado_second_data]" value="{{ checkValueTranslated($iso.'[mercado_second_data]', $details, $iso, 'mercado_second_data') }}">
					</div>
				</div>

				<div class="row">
					<h5>Mercado 3</h5>
					<div class="col-md-4">
						<label>Título</label>
						<input type="text" class="form-control" name="{{ $iso }}[mercado_third_title]" value="{{ checkValueTranslated($iso.'[mercado_third_title]', $details, $iso, 'mercado_third_title') }}">
					</div>
					<div class="col-md-8">
						<label>Descripción</label>
						<input type="text" class="form-control" name="{{ $iso }}[mercado_third_data]" value="{{ checkValueTranslated($iso.'[mercado_third_data]', $details, $iso, 'mercado_third_data') }}">
					</div>
				</div>

			</div>
    	@endforeach
    </div>
</div>


<div class="row">
	<div class="form-group form-group-default col-md-12 mt50">
		<button type="button" id="cancelar" class="btn btn-bordered btn-default ">Cancelar</button>
		<button type="submit" id="guardar" class="btn btn-primary btn-bordered ladda-button" data-style="zoom-in">
			<span class="ladda-label">Actualizar información</span>
			<span class="ladda-spinner"></span>
		</button>
	</div>
</div>
{!! Form::close() !!}