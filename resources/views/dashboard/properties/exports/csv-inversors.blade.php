<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">

<head>
    <style>
        .cabecera-1 {
            background-color: #67d3e0
        }
        .cabecera-2 {
            background-color: #a5eff7
        }
        .font-bold {
            font-weight: 600
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="container">
            <div class="row">
                <div class="xs-4 col">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tr class="cabecera-1">
                            <th colspan="4" class="cabecera-1">
                                {{ $title }}
                            </th>
                        </tr>
                        <tr class="system cabecera-2">
                            <td class="font-bold" data-type="string">Nombre</td>
                            <td class="font-bold" >Inversión</td>
                            <td class="font-bold" data-sorted="false" data-direction="DESC" data-type="date" data-format-string="DD-MM-YYYY hh::mm">Fecha y hora</td>
                            <td class="font-bold" data-sortable="true">Opciones</td>
                        </tr>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->nombre }} {{ $user->apellido1 }} {{ $user->apellido2 }}</td>
                                <td class="h5 c-2 nowrap text-right append-euro">{{ $user->pivot->total }} €</td>
                                <td class="">{{ $user->pivot->created_at->format('d/m/Y h:m') }}</td>
                                <td class=""><a href="{{ route('dashboard.users.edit', $user->id) }}" class="btn btn-xs btn-info" target="_blank">Ver usuario</a></td>
                            </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
