@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/allcp/forms/css/forms.css') }}">
    	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.bootstrap.min.css">
    	<link href="{{ asset('dashboard_theme/assets/plugins/redactor/redactor.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" type="text/css" media="screen" />
		<!-- Datepicker -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker3.standalone.css">
		<link href="{{ asset('/dashboard_theme/custom-calendar.css') }}" rel="stylesheet" type="text/css" />
    	<style>
			.no-shadow {
			    box-shadow: none !important;
			}
			.allcp-form label, .allcp-form input, .allcp-form button, .allcp-form select, .allcp-form textarea {
			    display: inline-block;
			    max-width: 100%;
			    margin-bottom: 5px;
			    font-weight: 600;
			}
			.allcp-form .file.append-button > input.gui-input {
			    padding-left: 135px;
			}

			body.basic-gallery #mixitup-container .mix, body.basic-gallery #mixitup-container .gap {
				display: inline-block;
				width: 23%;
				margin: 10px !important;
			}

			#mixitup-container {
			    text-align: inherit !important;
			}

			.image-panel {
			    box-shadow: 0 0 0 1px #e5eaee !important;
			    -webkit-box-shadow: 0 0 0 1px #e5eaee !important;
			}

			.image-mix  {
				position: relative;
			}
			.hover-img {
			    transition: .5s ease;
			    opacity: 0;
			    position: absolute;
		        top: 45%;
				left: 34%;
			    transform: translate3d(0, 0, 0);
			}

			.image-mix img {
				opacity: 1;
				transition: .5s ease;
				backface-visibility: hidden;
			}

			.image-mix:hover img{
				opacity: 0.3;
			}

			.image-mix:hover .hover-img {
				opacity: 1;
			}
			.show-arrows {
				appearance:menulist !important;
				-moz-appearance:menulist !important;
				-webkit-appearance: menulist !important;
			}

    	</style>
		<style>
			.row {
				margin-left: -5px;
    			margin-right: -5px;
			}
			[class*="col-"] {
				padding-left: 5px;
				padding-right: 5px;
			}
		</style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div id="create-property" class="row">

			
				<div class="tab-block mb25">
					<ul class="nav panel-tabs">
						<li class="active">
							<a href="#datos-generales" data-toggle="tab" aria-expanded="true">Datos generales</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="col-md-12 tab-pane active" id="datos-generales">
							@include('dashboard.properties.parts.generales')
			            </div>
					</div>
				</div>
            
		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3d3NKYsKp6iI-fnFardQZW-9lQEIJeYY&libraries=places"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.js') }}"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
		<script src="{{ asset('dashboard_theme/assets/plugins/redactor/redactor.js') }}"></script>
		<script src="{{ asset('dashboard_theme/assets/plugins/redactor/plugins/fullscreen.js') }}" type="text/javascript"></script>
		<script src="{{ asset('dashboard_theme/assets/plugins/redactor/plugins/imagemanager.js') }}" type="text/javascript"></script>
		<script src="{{ asset('dashboard_theme/assets/plugins/redactor/plugins/video.js') }}" type="text/javascript"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.es.min.js"></script>
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
		<script>
			$(document).on('click','#cancelar',function(){
				window.location.href = "{{ route('inmuebles.index') }}";
			});

			document.addEventListener("mousewheel", function(event){
			    if(document.activeElement.type === "number"){
			        document.activeElement.blur();
			    }
			});

			var property = {
				developer: {
					id: "{{ old('developer_id') }}"
				},
				type: {
					id: "{{ old('property_type_id') }}"
				},
				investment: {
					id: "{{ old('investment_id') }}",
				},
				zona_premium: "{{ old('zona_premium', true) }}",
				private: "{{ old('private', true) }}",
				antiguedad: "{{ old('antiguedad') }}",
				fecha_publicacion: "{{ old('fecha_publicacion') }}",
				orden: "{{ old('orden') }}",
				fake_invertido: "{{ old('fake_invertido') }}",
				fake_inversores: "{{ old('fake_inversores') }}",
				ref: "{{ old('ref') }}",
				nombre: "{{ old('nombre') }}",
				slug: "{{ old('slug') }}",
				catastro: "{{ old('catastro') }}",
				ubicacion: "{{ old('ubicacion') }}",
				barrio: "{{ old('barrio') }}",
				direccion: "{{ old('direccion') }}",
				zona_premium: "{{ old('zona_premium') }}",
				habitaciones: "{{ old('habitaciones') }}",
				toilets: "{{ old('toilets') }}",
				metros: "{{ old('metros') }}",
				garaje: "{{ old('garaje') }}",
				piscina: "{{ old('piscina') }}",
				ascensor: "{{ old('ascensor') }}",
				gestor: "{{ old('gestor') }}",
				descripcion: "{{ old('descripcion') }}",
				ficha: "{{ old('ficha') }}",
				lat: "{{ old('lat', 40.415363) }}",
				lng: "{{ old('lng', -3.707398) }}",
				resumen: false,
			}

			Ladda.bind('.ladda-button', {
			    timeout: 8000
			});
			
			function activejQueryPlugins() {
				descripcionRedactor = $('#descripcion').redactor({
					minHeight: 350,
					maxHeight: 800,
					cleanOnPaste: true,
					cleanSpaces: true,
					removeComments: true,
					removeEmpty: ['strong', 'em', 'span', 'p'],
					buttonsHide: ['orderedlist', 'image'],
					formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
					plugins: ['fullscreen',  'video', 'imagemanager'],
					imageManagerUrl: "/dashboard/filemanager/dialog?type=editor&editor=descripcion",
					syncBeforeCallback: function(html) {
						window.formEditProperty.property.descripcion = $('#descripcion').val();
						return html;
					},
					changeCallback: function() {
						window.formEditProperty.property.descripcion = $('#descripcion').val();
					}
				});

				fichaRedactor = $('#ficha').redactor({
					minHeight: 350,
					maxHeight: 800,
					cleanOnPaste: true,
					cleanSpaces: true,
					removeComments: true,
					removeEmpty: ['strong', 'em', 'span', 'p'],
					buttonsHide: ['orderedlist', 'image'],
					formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
					plugins: ['fullscreen',  'video', 'imagemanager'],
					imageManagerUrl: "/dashboard/filemanager/dialog?type=editor&editor=ficha",
					syncBeforeCallback: function(html) {
						window.formEditProperty.property.ficha = $('#ficha').val();
						return html;
					},
					changeCallback: function() {
						window.formCreateProperty.property.ficha = $('#ficha').val();
					}
				});


				$('input[maxlength]').maxlength({
		        	alwaysShow: true,
		            threshold: 15,
		            placement: "centered-right",
		        });

		        $('.datepicker').datepicker({
					format: "dd/mm/yyyy",
				    language: "es",
				    autoclose: true,
				    allowInputToggle: true,
				    orientation: "auto right",
				    startDate: '+0'
				});
			}      

			function setDescription(htmlData) {
				descripcionRedactor.redactor('code.set', htmlData);
			}

			function setFicha(htmlData) {
				fichaRedactor.redactor('code.set', htmlData);
			}
		</script>
		<script src="{{ asset(mix('js/dashboard-create-property.js')) }}"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}