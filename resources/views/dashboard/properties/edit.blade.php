@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/allcp/forms/css/forms.css') }}">
    	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.bootstrap.min.css">
    	<link href="{{ asset('dashboard_theme/assets/plugins/redactor/redactor.css') }}" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.css" type="text/css" media="screen" />
		<!-- Datepicker -->
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker3.standalone.css">
		<link href="{{ asset('/dashboard_theme/custom-calendar.css') }}" rel="stylesheet" type="text/css" />
    	<style>
			.no-shadow {
			    box-shadow: none !important;
			}
			.allcp-form label, .allcp-form input, .allcp-form button, .allcp-form select, .allcp-form textarea {
			    display: inline-block;
			    max-width: 100%;
			    margin-bottom: 5px;
			    font-weight: 600;
			}
			.allcp-form .file.append-button > input.gui-input {
			    padding-left: 135px;
			}

			body.basic-gallery #mixitup-container .mix, body.basic-gallery #mixitup-container .gap {
				display: inline-block;
				width: 23%;
				margin: 10px !important;
			}

			#mixitup-container {
			    text-align: inherit !important;
			}

			.image-panel {
			    box-shadow: 0 0 0 1px #e5eaee !important;
			    -webkit-box-shadow: 0 0 0 1px #e5eaee !important;
			}

			.image-mix  {
				position: relative;
			}
			.hover-img {
			    transition: .5s ease;
			    opacity: 0;
			    position: absolute;
		        top: 45%;
				left: 34%;
			    transform: translate3d(0, 0, 0);
			}

			.hover-img.pdf {
				left: 20%;
			}

			.image-mix img {
				opacity: 1;
				transition: .5s ease;
				backface-visibility: hidden;
			}

			.image-mix:hover img{
				opacity: 0.3;
			}

			.image-mix:hover .hover-img {
				opacity: 1;
			}

			.show-arrows {
				appearance:menulist !important;
				-moz-appearance:menulist !important;
				-webkit-appearance: menulist !important;
			}

			.table-fixed thead {
			  width: 100%;
			}
			.table-fixed tbody {
			  max-height: 800px;
			  overflow-y: auto;
			  width: 100%;
			}
			.table-fixed thead, .table-fixed tbody, .table-fixed tr, .table-fixed td, .table-fixed th {
			  display: block;
			}
			.table-fixed tbody td, .table-fixed thead > tr> th {
			  float: left;
			  border-bottom-width: 0;
			}
			 .table-fixed tbody tr td{
			 	height: 40px;
			 }
			 .append-euro:after {
				content: ' €';
			}
    	</style>
	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')


		<div id="edit-property" class="row">
			
			<!-- Modal Resumen -->
			<div id="modalResumen" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Vista previa archivo de resumen</h4>
						</div>
						<div class="modal-body">

							<template v-if="hasResumenUpdateErrors">
								<div class="alert alert-danger">
									<ul>
										<li v-html="resumen_update_errors.message"></li>
									</ul>
								</div>
							</template>
							
							<resumen-modal :resumen="temporalResumen"></resumen-modal>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<div class="pull-right ml-10" v-if="resumen.loading_update">
								<pulse-loader :loading="resumen.loading_update" :color="color" :size="size"></pulse-loader>
							</div>
							<button type="button" class="btn btn-primary" v-if="!resumen.loading_update" v-on:click="updateResumen">Guardar resumen</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal Estimacion -->
			<div id="modalEstimacion" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Vista previa archivo de estimación</h4>
						</div>
						<div class="modal-body">

							<template v-if="hasEstimacionUpdateErrors">
								<div class="alert alert-danger">
									<ul>
										<li v-html="estimacion_update_errors.message"></li>
									</ul>
								</div>
							</template>
							
							<estimacion-modal :estimacion="temporalEstimacion"></estimacion-modal>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<div class="pull-right ml-10" v-if="estimacion.loading_update">
								<pulse-loader :loading="estimacion.loading_update" :color="color" :size="size"></pulse-loader>
							</div>
							<button type="button" class="btn btn-primary" v-if="!estimacion.loading_update" v-on:click="updateEstimacion">Guardar seguimiento</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal Evolucion -->
			<div id="modalEvolucion" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Vista previa archivo de evolución</h4>
						</div>
						<div class="modal-body">

							<template v-if="hasEvolucionUpdateErrors">
								<div class="alert alert-danger">
									<ul>
										<li v-html="evolucion_update_errors.message"></li>
									</ul>
								</div>
							</template>
							
							<evolucion-modal :evolucion="temporalEvolucion" :year="evolucion.years.selected" :mes="evolucion.months.selected"></evolucion-modal>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
							<div class="pull-right ml-10" v-if="evolucion.upload.loading_update">
								<pulse-loader :loading="evolucion.upload.loading_update" :color="color" :size="size"></pulse-loader>
							</div>
							<button type="button" class="btn btn-primary" v-if="!evolucion.upload.loading_update" v-on:click="updateEvolucion">Guardar seguimiento</button>
						</div>
					</div>
				</div>
			</div>

			<!-- Modal Confirm -->
			<div id="modalConfirm" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">¿Estás seguro de procesar el cambio de estado?</h4>
						</div>
						<div class="modal-body">
							<p v-if="currentEstado() == 2">Una vez procesado el cambio el inmueble estará disponible para poder invertir.</p>
							<p v-if="currentEstado() == 3">Se va a proceder a transferir todas las inversiones de este inmueble a tu cuenta LemonWay.</p>

							<p v-if="currentEstado() == 3">
								<strong>Invertido</strong>: @{{ invertidoOrFake }} <br>
								<strong>Dinero en Lemonway</strong>: @{{ moneyFromWallet }} <br>
							</p>

							<p v-if="currentEstado() == 4">Se va a proceder a transferir todas las inversiones de este inmueble a tu cuenta LemonWay. <br> Los inversores comenzarán a poder acceder a la vista detallada de los datos de evolución</p>
							<p v-if="currentEstado() == 4">
								<strong>Invertido</strong>: @{{ invertidoOrFake }} <br>
								<strong>Dinero en Lemonway</strong>: @{{ moneyFromWallet }} <br>
							</p>

							<p v-if="currentEstado() == 5">Se va a proceder a pagar los dividendos del último mes y a pagar también las ganancias obtenidas de la venta. No hace falta que pagues el último mes de dividendos.</p>
							<p v-if="currentEstado() == 6">Se va a proceder a devolver a los usuarios todo lo que han invertido. Y a pagarles el 5% de la promoción.</p>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" v-on:click="cancelCambio" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-primary" v-on:click="cambiarEstado">Sí, cambiar el estado</button>
						</div>
					</div>
				</div>
			</div>


			<!-- Modal Add money -->
			<div id="modalAddMoney" class="modal fade" role="dialog" data-backdrop="static">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">Aún no se ha completado el objetivo</h4>
						</div>
						<div class="modal-body">
							<p>Aún no se ha completado el objetivo. Pero solo quedan @{{ estado_diferencia }} €. ¿Deseas pagarlo tu?</p>
							<
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" v-on:click="cancelCambio" data-dismiss="modal">Cancelar</button>
							<button type="button" class="btn btn-primary"  v-if="!pagar_diferencia.loading" v-on:click="pagarDiferencia">Sí, pago yo la diferencia!</button>
							<div class="pull-right ml-10" v-if="pagar_diferencia.loading">
								<pulse-loader :loading="pagar_diferencia.loading" :color="color" :size="size"></pulse-loader>
							</div>
						</div>
					</div>
				</div>
			</div>


			<div class="text-center" v-if="loading">
				<pulse-loader :loading="loading" :color="color" :size="size"></pulse-loader>
			</div>
			<template v-if="!loading">
				<div class="tab-block mb25">
					<ul class="nav panel-tabs">
						<li class="active">
							<a href="#datos-generales" data-ref="generales" data-toggle="tab" aria-expanded="true">Datos generales</a>
						</li>
						<li>
							<a href="#descripcion-ficha" data-ref="ficha" data-toggle="tab" aria-expanded="false">Descripción y Ficha</a>
						</li>
						<li>
							<a href="#imagenes" data-ref="imagenes" data-toggle="tab" aria-expanded="false">Imágenes</a>
						</li>
						<li>
							<a href="#documentos" data-ref="documentos" data-toggle="tab" aria-expanded="false">Documentos</a>
						</li>
						<li>
							<a href="#datos-economicos-resumen" data-ref="resumen" data-toggle="tab" aria-expanded="false">Resumen datos económicos</a>
						</li>
						<li>
							<a href="#datos-economicos-estimacion" data-ref="estimacion" data-toggle="tab" aria-expanded="false">Estimación anual</a>
						</li>
						<li>
							<a href="#datos-economicos-evolucion" data-ref="evolucion" data-toggle="tab" aria-expanded="false">Evolución datos económicos</a>
						</li>
						<li>
							<a href="#estado" data-ref="estado" data-toggle="tab" aria-expanded="false">Estado</a>
						</li>
						@if($property->state->id >= 2)
							<li>
								<a href="#inversiones" data-ref="inversiones" data-toggle="tab" aria-expanded="false">Inversores</a>
							</li>
						@endif
						<li>
							<a href="#autoinvest" data-ref="autoinvest" data-toggle="tab" aria-expanded="false">
								Autoinvest
							</a>
						</li>
					</ul>
					<div class="tab-content">

						<div class="col-md-12 tab-pane active" id="datos-generales">
							@include('dashboard.properties.parts.generales')
			            </div>
			            <div class="col-md-12 tab-pane" id="descripcion-ficha">
			            	@include('dashboard.properties.parts.ficha')
			            </div>
			            <div class="col-md-12 tab-pane" id="imagenes">
							@include('dashboard.properties.parts.imagenes')
						</div>
						<div class="col-md-12 tab-pane" id="documentos">
							@include('dashboard.properties.parts.documentos')
						</div>
						<div class="col-md-12 tab-pane" id="datos-economicos-resumen">
							@include('dashboard.properties.parts.resumen')
						</div>
						<div class="col-md-12 tab-pane" id="datos-economicos-estimacion">
							@include('dashboard.properties.parts.estimacion')
						</div>
			            <div class="col-md-12 tab-pane" id="datos-economicos-evolucion">
	            			@include('dashboard.properties.parts.evolucion')
			            </div>
			            <div class="col-md-12 tab-pane" id="estado">
	            			@include('dashboard.properties.parts.estado')
			            </div>
			            @if($property->state->id >= 2)
							<div class="col-md-12 tab-pane" id="inversiones">
								@include('dashboard.properties.parts.inversiones')
							</div>
						@endif
						<div class="col-md-12 tab-pane" id="autoinvest">
							<div class="row">
								<div class="col-md-12">
									<a href="{{ route('dev.autoinvest-property', $property->id) }}" target="_blank" class="btn btn-primary btn-bordered ladda-button">
										TEST AUTOINVEST
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="footer">
						
					</div>
				</div>
			
            </template>
			<div class="hide form-resumen-hidden">
				<form method="POST" v-on:submit.prevent="uploadResumenForm" enctype="multipart/form-data" id="upload-resumen">
				</form>
			</div>
			

		</div>
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD3d3NKYsKp6iI-fnFardQZW-9lQEIJeYY&libraries=places"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.js') }}"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/fileupload/fileupload.js') }}"></script>
		<script src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/holder/2.9.4/holder.min.js') }}"></script>
		<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.5/jquery.fancybox.min.js"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/redactor.js') }}"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/lang/es.js') }}"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/source.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/fullscreen.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/imagemanager.js') }}" type="text/javascript"></script>
	    <script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/video.js') }}" type="text/javascript"></script>
		<script src="{{ asset('/dashboard_theme/assets/plugins/redactor/plugins/table.js') }}" type="text/javascript"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/maxlength/bootstrap-maxlength.min.js') }}" type="text/javascript"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/js/bootstrap-datepicker.min.js"></script>
		<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.1/locales/bootstrap-datepicker.es.min.js"></script>
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-footable/3.1.4/footable.min.js"></script>
		<script src="https://rawgit.com/Snack-X/excelexport.js/master/excelexport.js"></script>
		<script>

			var locales = {!! json_encode($locales->pluck('iso')) !!};

			$(document).on('click','#cancelar',function(){
				window.location.href = "{{ route('inmuebles.index') }}";
			});

			document.addEventListener("mousewheel", function(event){
			    if(document.activeElement.type === "number"){
			        document.activeElement.blur();
			    }
			});

			Ladda.bind('.ladda-button', {
			    timeout: 8000
			});

			$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {
			  	
			  	var target = $(e.target).data("ref") // activated tab
			  
				if (target == 'ficha') {
					var firstImage = document.getElementById('similares_first-image');
					var secondImage = document.getElementById('similares_second-image');

					Holder.run({
						images: [ firstImage, secondImage]
					});
				}


				if (target == "imagenes" || target == "documentos" || target == "docs_en") {
					$('body').addClass('basic-gallery');
					$('body').addClass('basic-gallery');

					//Lanzar también cuando cambien pestañas de documentos
					// if (typeof reloadVueMasonry === "function") { 
					// 	reloadVueMasonry();
					// }
					$("#mixitup-container").width($("#mixitup-container").width() + 1);
					window.formEditProperty.refreshMasonry();
				} else {
					$('body').removeClass('basic-gallery');
				}


				@foreach($locales as $locale)
					let count_{{ $locale->iso }} = 0;
					if (target == "docs_{{ $locale->iso }}") {
						if (count_{{ $locale->iso }} === 0) {
							$('body').addClass('basic-gallery');
							window.formEditProperty.refreshMasonry();
							// if (typeof reloadVueMasonry === "function") { 
							// 	reloadVueMasonry();
							// }
							count_{{ $locale->iso }}++;
						}
					}
				@endforeach


			});
			

			let descripcionRedactor, fichaRedactor;
			
			function activejQueryPlugins() {

				@foreach($locales as $locale)
					descripcionRedactor = $('#descripcion_{{ $locale->iso }}').redactor({
						lang: 'es',
		                minHeight: 350,
		                maxHeight: 800,
		                cleanOnPaste: true,
		                cleanSpaces: true,
		                structure: true,
		                script: false,
		                removeComments: true,
		                removeEmpty: ['strong', 'em', 'span', 'p'],
		                pasteLinkTarget: '_blank',
		                buttonsHide: ['orderedlist', 'image'],
		                formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
		                plugins: ['video', 'source', 'table', 'imagemanager', 'fullscreen'],
						imageManagerUrl: "/dashboard/filemanager/dialog?type=editor&editor=descripcion_{{ $locale->iso }}",
						syncBefore: function(html) {
							window.formEditProperty.property.descripcion = $('#descripcion').val();
							return html;
						},
						change: function() {
							window.formEditProperty.property.descripcion = $('#descripcion').val();
						}
					});

					fichaRedactor = $('#ficha_{{ $locale->iso }}').redactor({
						lang: 'es',
		                minHeight: 350,
		                maxHeight: 800,
		                cleanOnPaste: true,
		                cleanSpaces: true,
		                structure: true,
		                script: false,
		                removeComments: true,
		                removeEmpty: ['strong', 'em', 'span', 'p'],
		                pasteLinkTarget: '_blank',
		                buttonsHide: ['orderedlist', 'image'],
		                formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
		                plugins: ['video', 'source', 'table', 'imagemanager', 'fullscreen'],
						imageManagerUrl: "/dashboard/filemanager/dialog?type=editor&editor=ficha_{{ $locale->iso }}",
						syncBefore: function(html) {
							window.formEditProperty.property.ficha = $('#ficha').val();
							return html;
						},
						change: function() {
							window.formCreateProperty.property.ficha = $('#ficha').val();
						}
					});

					rendimientoRedactor = $('#rendimiento_mercado_{{ $locale->iso }}').redactor({
						lang: 'es',
		                minHeight: 350,
		                maxHeight: 800,
		                cleanOnPaste: true,
		                cleanSpaces: true,
		                structure: true,
		                script: false,
		                removeComments: true,
		                removeEmpty: ['strong', 'em', 'span', 'p'],
		                pasteLinkTarget: '_blank',
		                buttonsHide: ['orderedlist', 'image'],
		                formatting: ['p', 'blockquote', 'h2', 'h3', 'h4'],
		                plugins: ['video', 'source', 'table', 'imagemanager', 'fullscreen'],
						imageManagerUrl: "/dashboard/filemanager/dialog?type=editor&editor=rendimiento_mercado_{{ $locale->iso }}",
						syncBefore: function(html) {
							window.formEditProperty.property.ficha = $('#ficha').val();
							return html;
						},
						change: function() {
							window.formCreateProperty.property.ficha = $('#ficha').val();
						}
					});
				@endforeach


				$('input[maxlength]').maxlength({
		        	alwaysShow: true,
		            threshold: 15,
		            placement: "centered-right",
		        });

		        $('.datepicker').datepicker({
					format: "dd/mm/yyyy",
				    language: "es",
				    autoclose: true,
				    allowInputToggle: true,
				    orientation: "auto right",
				    startDate: '+0'
				});


		        $('.footable').footable();

		        uploadFile = function(url){
		            $.fancybox({
		                width       : 950,
		                height      : 500,
		                type        : 'iframe',
		                href        : url,
		                fitToView   : false,
		                autoScale   : false,
		                autoSize    : false
		            });
		        };

		        OnMessage = function(data){
		            var pattern = /([a-z\-_0-9\/\:\.]*\.(jpg|jpeg|png|gif|bmp))/i;
		            if(data.appendId != null){
		               
		                if(pattern.test(data.thumb)){
		                    var id = data.appendId;
		                    var id = id.replace('-text', '');
		                    $("#"+id+"-text").val(data.path);
		                    $("#"+id+"-image").attr({ 'src' : data.thumb});
		                } else {
		                    $("#"+data.appendId).val(data.path);
		                }
		            }
		            $.fancybox.close();
		        };
			}      

			function activeExport () {
				var ee = excelExport("inversiones-table").parseToCSV().parseToXLS("Inversiones inmueble {{ $property->ref }}");

				$("#export").click(function (event) {
					file = ee.getXLSDataURI();
					var a = document.createElement("a");
					document.body.appendChild(a);
					a.style = "display: none";
					a.href = file;
					a.download = 'Inversiones inmueble {{ $property->ref }}';
					a.click();
					window.URL.revokeObjectURL(file);
					a.remove();
				}); 
			}

			function setDescription(htmlData) {
				descripcionRedactor.redactor('code.set', htmlData);
			}

			function setFicha(htmlData) {
				fichaRedactor.redactor('code.set', htmlData);
			}
		</script>
		<script>
			var propertyInversoresRoute = "{{ route('property.inversores', $property->id) }}";
			var propertyRoute = "{{ route('inmuebles.show', $property->id) }}";
			var propertyResumenRoute = "{{ route('dashboard.api.inmuebles.resumen', $property->id) }}";
			var propertyUpdateResumenRoute = "{{ route('inmuebles.resumen', $property->id) }}";
			var propertyEstimacionRoute = "{{ route('dashboard.api.inmuebles.estimacion', $property->id) }}";
			var propertyUpdateEstimacionRoute = "{{ route('inmuebles.estimacion', $property->id) }}";
			var propertyEvolutionDataRoute = "{{ route('dashboard.api.inmuebles.evolution-year', $property->id) }}";
			var propertyEvolucionRoute = "{{ route('dashboard.api.inmuebles.evolucion', $property->id) }}";
			var propertyUpdateEvolucionRoute = "{{ route('inmuebles.evolucion', $property->id) }}";
			var propertyDestroyPhotoRoute = "{{ route('inmuebles.photos.destroy') }}";
			var propertyPrincipalPhotoRoute = "{{ route('inmuebles.photos.principal', $property->id) }}";
			var propertyTextoPhotoRoute = "{{ route('inmuebles.photos.texto') }}";
			var propertyDestroyDocumentsRoute = "{{ route('inmuebles.documents.destroy') }}";
			var propertyAbonarDividendos = "{{ route('dashboard.api.inmuebles.evolucion.abonar') }}";
			var propertyAbonarDividendosPreview = "{{ route('dashboard.api.inmuebles.evolucion.abonar.preview', ['property_id' => $property->id, 'year' => '_year', 'mes' => '_mes']) }}";
			var payDiferenceRoute = "{{ route('dashboard.api.inmuebles.pagar-diferencia', $property->id) }}";
		</script>
		<script src="{{ asset(mix('js/dashboard-properties.js')) }}"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}
