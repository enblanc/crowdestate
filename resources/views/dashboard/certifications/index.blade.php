@extends('dashboard.base')

{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
		<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

		<!-- Forms -->
		<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/allcp/forms/css/forms.css') }}">

		<!-- Ladda -->
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">

	@endsection
{{-- HEADER STYLES SECTION END--}}

{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}

{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   Certificaciones
                </div>
                <div class="pull-right" style="display: flex; align-items: center; justify-content: center; margin: auto; flex-wrap: wrap; gap: 4px">
					<a href="{{ asset('certificaciones-template.xlsx') }}" class="btn btn-primary" target="_blank">TEMPLATE</a>
	                {!! Form::open(['route' => 'dashboard.certifications.post', 'method'=> 'post', 'files' => true, 'id' => 'uploadformCertification' ]) !!}
	                	<div class="col-md-12 allcp-form" id="datos-generales">
		                	<div class="form-group form-group-default" style="margin-bottom: 0">
		                		<label class="field prepend-icon append-button file">
                                    <span class="button btn-primary">Subir nuevo archivo</span>
                                    <input type="file" class="gui-file" name="excel" id="excel" onchange="document.getElementById('uploader1').value = this.value ;document.forms['uploadformCertification'].submit();"
                                    	accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                    <input type="text" class="gui-input" id="uploader1" placeholder="Elije un archivo">
                                    <label class="field-icon">
                                        <i class="fa fa-cloud-upload"></i>
                                    </label>
                                </label>
		                	</div>
		                </div>
	                {!! Form::close() !!}
	            </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">
		        <div class="row">
		           	<div class="col-md-12">
                        <div class="panel panel-visible no-box-shadow">
                            <div class="panel-body pn">
                                <div class="table-responsive">
                                    {!! $dataTable->table(['id' => 'datatable', 'class' => 'table table-hover']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="/dashboard_theme/assets/js/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
		<script src="/dashboard_theme/assets/js/plugins/pnotify/pnotify.js"></script>
    
    	<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
		
		{!! $dataTable->scripts() !!}
		<script src="/dashboard_theme/assets/js/deleteRowModel.js"></script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}