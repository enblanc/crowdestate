@extends('dashboard.base')


{{-- HEADER STYLES SECTION START --}}
	@section('header_styles')
		<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/datatables/examples/resources/bootstrap/3/dataTables.bootstrap.css') }}">
		<link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

		<!-- Forms -->
		<link rel="stylesheet" type="text/css" href="{{ asset('/dashboard_theme/assets/allcp/forms/css/forms.css') }}">

		<!-- Ladda -->
    	<link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">

	@endsection
{{-- HEADER STYLES SECTION END--}}


{{-- HEADER SCRIPTS SECTION START --}}
	@section('header_scripts')
		
	@endsection
{{-- HEADER SCRIPTS SECTION END --}}


{{-- BREADCRUMB SECTION START --}}
	@section('breadcrumb')
		{{-- {!! Breadcrumbs::render('dashboard') !!} --}}
	@endsection
{{-- BREADCRUMB SECTION END --}}



{{-- MAIN CONTENT SECTION START --}}
	@section('content')
		<div class="panel panel-visible">
		    <div class="panel-heading">
                <div class="panel-title hidden-xs pull-left">
                   Pagos
                </div>
                <div class="pull-right">

	                {!! Form::open(['route' => 'dashboard.pagos.upload', 'method'=> 'post', 'files' => true, 'id' => 'uploadform' ]) !!}

	                	<div class="col-md-12 allcp-form" id="datos-generales">
		                	<div class="form-group form-group-default">
		                		<label class="field prepend-icon append-button file">
                                    <span class="button btn-primary">Subir nuevo archivo</span>
                                    <input type="file" class="gui-file" name="excel" id="excel" onchange="document.getElementById('uploader1').value = this.value ;document.forms['uploadform'].submit();"
                                    	accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel">
                                    <input type="text" class="gui-input" id="uploader1" placeholder="Elije un archivo">
                                    <label class="field-icon">
                                        <i class="fa fa-cloud-upload"></i>
                                    </label>
                                </label>

		                	</div>
		                </div>

	                {!! Form::close() !!}
	            </div>
	            <div class="clearfix"></div>
            </div>

		    <div class="panel-body">

		        
		        <div class="row">
					
		           	<div class="col-md-12">
                        <div class="panel panel-visible no-box-shadow">
                            
                            <div class=""> {{-- panel-body pn --}}

                                <div class="table-responsive">
                                    {!! $dataTable->table(['id' => 'datatable', 'class' => 'table table-hover']) !!}
                                </div>

                                <div class="alert alert-dark alert-dismissable mb30 alert-block mt20">
								    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
								    <h3 class="mt5 text-white">
									    Tipos permitidos - 
										<a href="{{ asset('assets/docs/pagos_ejemplos.xlsx') }}" download="ejemplo_pagos.xlsx" class="text-white fs18">
									    	Descargar ejemplo
									    </a>
									</h3>

								    <ul>
								    	<li><strong>0</strong> - Pago con tarjeta</li>
								    	<li><strong>3</strong> - Ingreso por transferencia</li>
								    	<li><strong>4</strong> - Devolución</li>
								    	<li><strong>13</strong> - Divindendos generados por Inmueble</li>
								    	<li><strong>14</strong> - Venta de Inmueble</li>
								    	<li><strong>15</strong> - Regalo</li>
								    	<li><strong>16</strong> - Devolución inmueble no conseguido</li>
								    	<li><strong>18</strong> - Diviendos generados del 5%</li>
								    	<li><strong>22</strong> - Regalo Brickstarter</li>
								    </ul>
								    
								</div>
                            </div>
                        </div>
                    </div>

		        </div>
		    </div>
		</div>
		
	@endsection
{{-- MAIN CONTENT SECTION END --}}


{{-- FOOTER SCRIPTS SECTION START --}}
	@section('footer_scripts')
		<script src="/dashboard_theme/assets/js/plugins/datatables/media/js/jquery.dataTables.js"></script>
		<script src="{{ asset('dashboard_theme/assets/js/plugins/datatables/media/js/dataTables.bootstrap.js') }}"></script>
		<script src="/dashboard_theme/assets/js/plugins/pnotify/pnotify.js"></script>
    
    	<script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
		
		{!! $dataTable->scripts() !!}
		<script type="text/javascript">
			register_button_action();

			// make the delete button work on subsequent result pages
			$('#datatable').on( 'draw.dt',   function () {
			    register_button_action();
			} ).dataTable();

			function register_button_action() {
			    $("[data-button-type=restart]").unbind('click');
			    // CRUD Delete
			    // ask for confirmation before deleting an item
			    $("[data-button-type=restart]").click(function(e) {
			        e.preventDefault();
			        var restart_button = $(this);
			        var restart_url = $(this).attr('href');
			        
			        swal({  title: "Volver a intentar el pago",
			                text: "Vas a volver a intentar el pago",
			                type: "info",
			                showCancelButton: true,
			                confirmButtonColor: "#67d3e0",
			                confirmButtonText: "Si, reintentar!",
			                cancelButtonText: "Cancelar",
			                closeOnConfirm: true
			            }, function(isConfirm){
			                if (isConfirm) {

			                    $.ajax({
			                        url: restart_url,
			                        beforeSend: function (request){
			                            request.setRequestHeader("X-CSRF-TOKEN", $('meta[name="csrf-token"]').attr('content'));
			                        },
			                        type: 'POST',
			                        success: function(result) {
			                            // Show an alert with the result
			                            new PNotify({
			                                title: "Reintentando pago",
			                                text: "El pago se está procesando.",
			                                type: "info"
			                            });

			                            $('#datatable').DataTable().ajax.reload();
			                        },
			                        error: function(result) {
			                            // Show an alert with the result
			                            new PNotify({
			                                title: "No se ha podido reintentar el pago",
			                                text: "Existió algún error y su pago no se ha podido reintentar.",
			                                type: "danger"
			                            });
			                        }
			                    });

			                }
			            });

			    });
			}
		</script>
	@endsection
{{-- FOOTER SCRIPTS SECTION END --}}