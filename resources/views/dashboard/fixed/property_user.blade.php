<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <script src="https://cdn.tailwindcss.com"></script>
    </head>

    <body>
        @if ($propertyUser->count())
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 my-10">
                <div class="max-w-3xl mx-auto">
                    <div class="">
                        <div class="sm:flex sm:items-center">
                            <div class="sm:flex-auto">
                                <h1 class="text-xl font-semibold text-gray-900">Registros menores a 0.0001</h1>
                                <p class="mt-2 text-sm text-gray-700"></p>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                                    <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                        <table class="min-w-full divide-y divide-gray-300">
                                            <thead class="bg-gray-50">
                                                <tr>
                                                    <th scope="col"
                                                        class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                                        ID</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        USER_ID</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        TOTAL</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        CAMBIO</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        UPDATED AT</th>
                                                    <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                                                        <span class="sr-only">Edit</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="divide-y divide-gray-200 bg-white">
                                                @foreach ($propertyUser as $item)
                                                    <tr>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->id }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->user_id }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->total }}
                                                        <td class="bg-yellow-200 text-center whitespace-nowrap px-1 text-sm font-bold text-gray-900">
                                                            {{ round($item->total, 2) }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->updated_at }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if ($propertyUserNegative->count())
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 my-10">
                <div class="max-w-3xl mx-auto">
                    <div class="">
                        <div class="sm:flex sm:items-center">
                            <div class="sm:flex-auto">
                                <h1 class="text-xl font-semibold text-gray-900">Registros negativos</h1>
                                <p class="mt-2 text-sm text-gray-700"></p>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                                    <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                        <table class="min-w-full divide-y divide-gray-300">
                                            <thead class="bg-gray-50">
                                                <tr>
                                                    <th scope="col"
                                                        class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                                        ID</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        USER_ID</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        TOTAL</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        UPDATED AT</th>
                                                    <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                                                        <span class="sr-only">Edit</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="divide-y divide-gray-200 bg-white">
                                                @foreach ($propertyUserNegative as $item)
                                                    <tr>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->id }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->user_id }}
                                                        </td>
                                                        <td class="bg-yellow-200 whitespace-nowrap px-1 text-sm font-bold text-gray-900">
                                                            {{ $item->total }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->updated_at }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if ($marketOrders->count())
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8 my-10">
                <div class="max-w-3xl mx-auto">
                    <div class="">
                        <div class="sm:flex sm:items-center">
                            <div class="sm:flex-auto">
                                <h1 class="text-xl font-semibold text-gray-900">Ordenes activas tipo "Vender" ({{ $marketOrders->count() }})</h1>
                                <p class="mt-2 text-sm text-gray-700"></p>
                            </div>
                        </div>
                        <div class="flex flex-col">
                            <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
                                <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
                                    <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                                        <table class="min-w-full divide-y divide-gray-300">
                                            <thead class="bg-gray-50">
                                                <tr>
                                                    <th scope="col"
                                                        class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                                                        ID</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        USER_ID</th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        TYPE
                                                    </th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        QUANTITY
                                                    </th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        PRICE
                                                    </th>
                                                    <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">
                                                        UPDATED AT</th>
                                                    <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                                                        <span class="sr-only">Edit</span>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody class="divide-y divide-gray-200 bg-white">
                                                @foreach ($marketOrders as $item)
                                                    <tr>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->id }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->user_id }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900">
                                                            {{ $item->type }}
                                                        </td>
                                                        <td class="bg-yellow-200 whitespace-nowrap px-1 text-sm font-bold text-gray-900">
                                                            {{ $item->quantity }}
                                                        </td>
                                                        <td class="bg-yellow-200 whitespace-nowrap px-1 text-sm font-bold text-gray-900">
                                                            {{ $item->price }}
                                                        </td>
                                                        <td class="whitespace-nowrap px-1 text-sm font-medium text-gray-900 sm:pl-6">
                                                            {{ $item->updated_at }}
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </body>
</html>
