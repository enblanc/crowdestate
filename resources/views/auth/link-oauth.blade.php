{{-- 
    ==========================================================================
    #BIENVENIDO A CROWDESTATE
    ========================================================================== 
    *
    * DESCRIPCIÓN
    * 
    * Página del inmueble. Consta de X bloques:
    *   - XX (carpeta parts)
    *   - XX (cp)
    *
    *
    * AVISOS
    *
    * Ninguno.
    *
    *
    --}}








{{--==========================================================================
    #LAYOUT 
    ========================================================================== --}}

    @extends('front/layouts/general')






{{--==========================================================================
    #CONTENT 
    ========================================================================== --}}
    
    @section('content')
        
        <section>
            <div class="xs-4 m-7 l-6 col center  clearfix  bg-w  pt-8 pb-24  mv-m-48">
                <div class="xs-4 col center  text-center  mb-48">

                    <h2 class="mt-8">{{ __('Acceso mediante') {{ ucfirst($provider) }}</h2>

                </div>

                <form class="ph-24 mt-16 [ js-loading ]" role="form" method="POST"  action="{{ route('link.account') }}">
                    {{ csrf_field() }}

                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <p class="c-error  text-center">{{ $error }}</p>
                        @endforeach
                    @endif
                    
                    <input type="hidden" name="provider" value="{{ $provider }}">
                    <input type="hidden" name="provider_id" value="{{ $providerId }}">
                    <input type="hidden" name="user_id" value="{{ $user->id }}">

                    <p>{{ __('Hola :name, ya tienes creada una cuenta con nosotros, sin embargo podemos vincular tu cuenta con', ['name' => $user->nombre]) }} {{ ucfirst($provider) }}.</p>

                    
                    <div class="clearfix p-relative  checkbox__wrapper">
                    
                        <input type="checkbox" id="terminos" class="custom-checkbox__input" name="terminos" required
                        data-parsley-errors-messages-disabled>
                        <label for="terminos">
                            <span class="custom-checkbox__new  |  v-top  mt-8"></span> 
                            <p class="custom-checkbox__text  |  small-text  mv-0">{{ __('Acepto los') }} <a target="_blank" href="{{ route('front.legal') }}">{{ __('Términos y Condiciones de Brickstarter') }}</a> {{ __('y') }} <a target="_blank" href="{{ route('front.legal-lemonway') }}">Lemonway</a> </p>
                        </label>

                    </div>

                    <input class="button button--full button--mvl-full  |  mt-40" type="submit" value="Unir mi cuenta con {{ ucfirst($provider) }}">

    {{--                 <button class="button button--full  |  mt-24" type="submit" 
                                         v-show="!register.loading" @click="validate"> Crear una cuenta </button>

                                    <div class="button button--full button--loader  |  mt-24" v-show="register.loading">Enviando</div>  --}}
                    
                </form>

                </div>
            </div>
        </section>

    @endsection






{{--==========================================================================
    #ASSETS 
    ========================================================================== --}}
    
    {{-- CSS --}}

    {{-- 
    @section('header_css')    @endsection
    @section('header_assets') @endsection 
    --}}
    @section('header_css')
        <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

    {{-- JS --}}

    @section('custom_plugin_js')
        <script src="/assets/js/plugins/parsleyjs.min.js"></script>
    @endsection


    {{-- JS --}}

    @section('custom_section_js')
        <script src="{{ mix('assets/js/main.js') }}"></script>
    @endsection 
