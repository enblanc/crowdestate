{{-- 
    ==========================================================================
    #BIENVENIDO A CROWDESTATE
    ========================================================================== 
    *
    * DESCRIPCIÓN
    * 
    * Página del inmueble. Consta de X bloques:
    *   - XX (carpeta parts)
    *   - XX (cp)
    *
    *
    * AVISOS
    *
    * Ninguno.
    *
    *
    --}}








{{--==========================================================================
    #LAYOUT 
    ========================================================================== --}}

    @extends('front/layouts/general')






{{--==========================================================================
    #CONTENT 
    ========================================================================== --}}
    
    @section('content')
        
        <section>
            <div class="xs-4 m-7 l-6 col center  clearfix  bg-w  pt-8 pb-24  mv-m-48">
                <div class="xs-4 col center  text-center  mb-48">

                    <h2 class="mt-8">{{ __('Alta de usuario') }}</h2>

                </div>



                <div class="social--buttons first-social  xs-4 m-4 l-4 col ">
                    <a href="{{ route('social.login', 'facebook') }}" class="social-button facebook socicon-facebook"><span>Facebook</span></a>
                </div>
                
                <div class="social--buttons  xs-4 m-4 l-4 col">
                    <a href="{{ route('social.login', 'google') }}" class="social-button google socicon-google"><span>Google</span></a>
                </div>

                <div class="social--buttons last-social  xs-4 m-4 l-4 col">
                    <a href="{{ route('social.login', 'linkedin') }}" class="social-button linkedin socicon-linkedin"><span>Linkedin</span></a>
                </div>

                <div class="clearfix"></div>

                <div class="or mt-16">{{ __('O mediante tus datos') }}</div>


                <form class="ph-24 mt-32 [ js-loading ]" id="form-register" role="form" method="POST"  action="{{ route('register') }}">
                    {{ csrf_field() }}

                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <p class="c-error  text-center">{{ $error }}</p>
                        @endforeach
                    @endif
                    

    
                    <h5 class="mv-0">{{ __('Datos personales') }}</h5>
                    <input class="custom-input" type="text" placeholder="{{ __('Nombre') }}" name="nombre" value="{{ old('nombre') }}" required>
                    <input class="custom-input" type="text" placeholder="{{ __('Primer apellido') }}" name="apellido1" value="{{ old('apellido1') }}" required>
                    <input class="custom-input" type="text" placeholder="{{ __('Segundo apellido') }}" name="apellido2" value="{{ old('apellido2') }}" >

                    <div class="clearfix p-relative  checkbox__wrapper mt-8 mb-16">
                    
                        <input
                            type="checkbox"
                            id="tribute"
                            class="custom-checkbox__input"
                            name="tribute"
                            {{ (old('tribute')) ? 'checked' : '' }}>
                        <label for="tribute" class="pt-4">
                            <span class="custom-checkbox__new  |  v-top  mt-8"></span> 
                            <p class="custom-checkbox__text  |  small-text  mv-0">{{ __('Tributo en España') }}</p>
                        </label>

                        <v-popover :open="tributeOpen" :trigger="'manual'" :placement="'bottom'" class="">
                            <small>
                                <span v-on:click="tributeOpen = !tributeOpen" class="c-pointer">{{ __('Aceptas las condiciones') }}</span>
                            </small>
                            <template slot="popover">
                                <small>
                                    {!! __('TributePopup') !!}
                                </small>

                                <a v-on:click="tributeOpen = !tributeOpen" class="right">Close</a>
                            </template>
                        </v-popover>

                    </div>

                    <h5 class="mt-4 mb-0">{{ __('Cuenta') }}</h5>


                    <select class="custom-input  custom-input--select" name="lang" id="lang" required>
                        <option value="en" disabled>{{ __('Language') }}</option>
                        <option value="es" {{ (current_locale() == 'es') ? 'selected' : '' }}>{{ __('Español') }}</option>
                        <option value="en" {{ (current_locale() == 'en') ? 'selected' : '' }}>{{ __('Inglés') }}</option>
                        <option value="de" {{ (current_locale() == 'de') ? 'selected' : '' }}>{{ __('Alemán') }}</option>
                    </select>


                    <input class="custom-input" type="email" placeholder="{{ __('E-mail') }}" name="email" value="{{ old('email') }}" required>
                    <input class="custom-input" type="password"  placeholder="{{ __('Contraseña') }}" name="password" required data-parsley-minlength="8" data-parsley-pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$">
                    <p class="small-text  mv-0 pt-0 pb-8">{{ __('Mínimo 8 caracteres con números y letras') }}</p>
                    <input class="custom-input  mb-24" type="text" name="referred" value="{{ old('referred', Cookie::get('referral')) }}" placeholder="{{ __('Código promocional (Opcional)') }}">


                    <div class="clearfix p-relative  checkbox__wrapper">
                    
                        <input type="checkbox" id="newsletter" class="custom-checkbox__input" name="newsletter">
                        <label for="newsletter">
                            <span class="custom-checkbox__new  |  v-top  mt-8"></span> 
                            <p class="custom-checkbox__text  |  small-text  mv-0">{{ __('Suscribirme al newsletter') }}</p>
                        </label>

                    </div>


                    <div class="clearfix p-relative  checkbox__wrapper">
                    
                        <input type="checkbox" id="terminos" class="custom-checkbox__input" name="terminos" required
                        data-parsley-errors-messages-disabled>
                        <label for="terminos">
                            <span class="custom-checkbox__new  |  v-top  mt-8"></span> 
                            <p class="custom-checkbox__text  |  small-text  mv-0">{{ __('Acepto los') }} <a target="_blank" href="{{ route('front.legal') }}">{{ __('Términos y Condiciones de Brickstarter') }}</a> {{ __('y') }} <a target="_blank" href="{{ route('front.legal-lemonway') }}">Lemonway</a> </p>
                        </label>

                    </div>

                     <div data-sitekey="{{ env('RECAPTCHA_SITEKEY') }}" class="g-recaptcha mt-24"></div>

                    <input class="button button--full button--mvl-full  |  mt-24" id="submit" type="submit" value="{{ __('Crear una cuenta') }}">

    {{--                 <button class="button button--full  |  mt-24" type="submit" 
                                         v-show="!register.loading" @click="validate"> Crear una cuenta </button>

                                    <div class="button button--full button--loader  |  mt-24" v-show="register.loading">Enviando</div>  --}}
                    
                </form>

                
            </div>
        </section>

    @endsection






{{--==========================================================================
    #ASSETS 
    ========================================================================== --}}
    
    {{-- CSS --}}

    {{-- 
    @section('header_css')    @endsection
    @section('header_assets') @endsection 
    --}}
    @section('header_css')
        <link rel="stylesheet" href="https://s3.amazonaws.com/icomoon.io/114779/Socicon/style.css?u8vidh">
        <link  rel="stylesheet" href="{{ asset('assets/css/social.css') }}?v1">
        <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

    {{-- JS --}}

    @section('custom_plugin_js')
        <script src="/assets/js/plugins/parsleyjs.min.js"></script>
    @endsection


    {{-- JS --}}

    @section('custom_section_js')
        <script src="{{ mix('assets/js/main.js') }}"></script>
        <script src="https://www.google.com/recaptcha/api.js?hl={{ current_locale() }}" async defer></script>
        {{-- <script src="https://www.google.com/recaptcha/api.js?hl={{ current_locale() }}&render={{ env('RECAPTCHA_SITEKEY') }}" async="" defer=""></script> --}}


    @endsection 
