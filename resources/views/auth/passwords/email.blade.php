{{-- 
    ==========================================================================
    #BIENVENIDO A CROWDESTATE
    ========================================================================== 
    *
    * DESCRIPCIÓN
    * 
    * Página del inmueble. Consta de X bloques:
    *   - XX (carpeta parts)
    *   - XX (cp)
    *
    *
    * AVISOS
    *
    * Ninguno.
    *
    *
    --}}








{{--==========================================================================
    #LAYOUT 
    ========================================================================== --}}

    @extends('front/layouts/general')






{{--==========================================================================
    #CONTENT 
    ========================================================================== --}}
    
    @section('content')
        
        <section>
            <div class="xs-4 m-7 l-6 col center  clearfix  bg-w  pt-8 pb-24  mv-m-48">
                <div class="xs-4 col center  text-center  mb-48">

                    <h2 class="mt-8">{{ __('Recuperar contraseña') }}</h2>

                </div>

                <form class="ph-24 mt-16 [ js-loading ]" role="form" method="POST"  action="{{ route('password.email') }}">
                    {{ csrf_field() }}

                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <p class="c-error  text-center">{{ $error }}</p>
                        @endforeach
                    @endif
                    

                    <p class="text-center  mb-32">{{ __('Indícanos tu e-mail para recuperar tu contraseña') }}</p>
                    <input class="custom-input" type="email" placeholder="{{ __('E-mail') }}" name="email" required>

                    <input class="button button--full button--mvl-full  |  mt-40" type="submit" value="{{ __('Recuperar mi contraseña') }}">

                    
                </form>

                </div>
            </div>
        </section>

    @endsection






{{--==========================================================================
    #ASSETS 
    ========================================================================== --}}
    
    {{-- CSS --}}

    {{-- 
    @section('header_css')    @endsection
    @section('header_assets') @endsection 
    --}}
    @section('header_css')
        <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

    {{-- JS --}}

    @section('custom_plugin_js')
        <script src="/assets/js/plugins/parsleyjs.min.js"></script>
    @endsection


    {{-- JS --}}

    @section('custom_section_js')
        <script src="{{ mix('assets/js/main.js') }}"></script>
    @endsection 
