{{-- 
    ==========================================================================
    #BIENVENIDO A CROWDESTATE
    ========================================================================== 
    *
    * DESCRIPCIÓN
    * 
    * Página del inmueble. Consta de X bloques:
    *   - XX (carpeta parts)
    *   - XX (cp)
    *
    *
    * AVISOS
    *
    * Ninguno.
    *
    *
    --}}








{{--==========================================================================
    #LAYOUT 
    ========================================================================== --}}

    @extends('front/layouts/general')


{{--==========================================================================
    #CONTENT 
    ========================================================================== --}}
    
    @section('content')
        
        <section>
            <div class="xs-4 m-7 l-6 col center  clearfix  bg-w  pt-8 pb-24  mv-m-48">
                <div class="xs-4 col center  text-center  mb-48">

                    <h2 class="mt-8">{{ __('Iniciar sesión') }}</h2>

                </div>

                <div class="mt-0 clearfix">
                    <div class="social--buttons first-social  xs-4 m-4 l-4 col ">
                        <a href="{{ route('social.login', 'facebook') }}" class="social-button facebook socicon-facebook"><span>Facebook</span></a>
                    </div>
                    
                    <div class="social--buttons  xs-4 m-4 l-4 col">
                        <a href="{{ route('social.login', 'google') }}" class="social-button google socicon-google"><span>Google</span></a>
                    </div>

                    <div class="social--buttons last-social  xs-4 m-4 l-4 col">
                        <a href="{{ route('social.login', 'linkedin') }}" class="social-button linkedin socicon-linkedin"><span>Linkedin</span></a>
                    </div>

                </div>

                <div class="or mt-16">{{ __('O mediante email') }}</div>


                <form class="ph-24 mt-16 [ js-loading ]" role="form" method="POST"  action="{{ route('login') }}">
                    {{ csrf_field() }}

                    @if (count($errors) > 0)
                        @foreach ($errors->all() as $error)
                            <p class="c-error  text-center">{{ $error }}</p>
                        @endforeach
                    @endif
                    

                    <input class="custom-input" type="email" name="email" placeholder="E-mail" value="{{ old('email') }}" required>
                    <input class="custom-input" type="password" name="password" placeholder="Contraseña"required>

                    <a class="d-block text-center  mt-16 mb-16   [ js-modal ]" href="{{ route('recuperar-password') }}">
                        {{ __( 'He olvidado mi contraseña') }}
                    </a>

                    <input class="button button--full button--mvl-full  |  mt-16" type="submit" value="{{ __('Iniciar sesión') }}">

                    



                        
                        

                    
                </form>

                </div>
            </div>
        </section>

    @endsection






{{--==========================================================================
    #ASSETS 
    ========================================================================== --}}
    
    {{-- CSS --}}

    {{-- 
    @section('header_css')    @endsection
    @section('header_assets') @endsection 
    --}}
    @section('header_css')
        <link rel="stylesheet" href="https://s3.amazonaws.com/icomoon.io/114779/Socicon/style.css?u8vidh">
        <link  rel="stylesheet" href="{{ asset('assets/css/social.css') }}?v1">
        <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

    {{-- JS --}}

    @section('custom_plugin_js')
        <script src="/assets/js/plugins/parsleyjs.min.js"></script>
    @endsection


    {{-- JS --}}

    @section('custom_section_js')
        <script src="{{ mix('assets/js/main.js') }}"></script>
    @endsection 
