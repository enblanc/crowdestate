@extends('dashboard.base')

@section('header_styles')
   <link type="text/css" rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
@endsection

@section('content')

<div class="panel panel-primary">
    <div class="panel-heading">
        <span class="panel-title">{{ trans('backup.title') }}</span>
        <div class="pull-right">
            <button id="create-new-backup-button" href="{{ route('backup.create') }}" class="btn btn-primary"  data-loading-text="Creating backup..."><span class="ladda-label"><i class="fa fa-plus"></i> {{ trans('backup.create') }}</span></button>
        </div>
    </div>
    <div class="panel-body">
        <table class="table table-hover table-responsive">
        <thead>
          <tr class="primary dark">
            <th>#</th>
            <th>{{ trans('backup.header.date') }}</th>
            <th class="text-right">{{ trans('backup.header.size') }}</th>
            <th>{{ trans('backup.header.actions') }}</th>
          </tr>
        </thead>
        <tbody>
          @foreach($backups as $k => $b)
          <tr>
            <td scope="row">{{ $k+1 }}</td>
            <td>{{ \Carbon\Carbon::createFromTimeStamp($b['last_modified'])->formatLocalized('%d %B %Y, %H:%M') }}</td>
            <td class="text-right">{{ round((int)$b['file_size']/1048576, 2).' MB' }}</td>
            <td>
                <a class="btn btn-sm btn-primary" href="{{ route('backup.download', '?disk='.$b['disk'].'&file_name='.urlencode($b['file_name'])) }}"><i class="fa fa-cloud-download"></i> {{ trans('backup.download') }}</a>
                <a class="btn btn-sm btn-danger" data-button-type="delete" href="{{ route('backup.delete', '?disk='.$b['disk'].'&file_name='.urlencode($b['file_name'])) }}"><i class="fa fa-trash-o"></i> {{ trans('backup.delete') }}</a>              
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
    </div>
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
</div>
@endsection

@section('footer_scripts')
    <script src="{{ asset('dashboard_theme/assets/js/plugins/pnotify/pnotify.js') }}" type="text/javascript"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js" type="text/javascript"></script>
<script>
  jQuery(document).ready(function($) {

    // capture the Create new backup button
    $("#create-new-backup-button").click(function(e) {
        e.preventDefault();
        var create_backup_url = $(this).attr('href');
        $btn = $(this);
        $btn.button('loading');

        

        // do the backup through ajax
        $.ajax({
                url: create_backup_url,
                type: 'POST',
                beforeSend: function (request){
                    request.setRequestHeader("X-CSRF-TOKEN", $('[name="_token"]').val());
                },
                success: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "{{ trans('backup.create_confirmation_title') }}",
                        text: "{{ trans('backup.create_confirmation_message') }}",
                        type: "info"
                    });
                    // Stop loading
                    $btn.button('reset');

                    // refresh the page to show the new file
                    // setTimeout(function(){ location.reload(); }, 3000);
                },
                error: function(result) {
                    // Show an alert with the result
                    new PNotify({
                        title: "{{ trans('backup.create_error_title') }}",
                        text: "{{ trans('backup.create_error_message') }}",
                        type: "danger"
                    });
                    // Stop loading
                    $btn.button('reset');
                }
            });
    });

    // capture the delete button
    $("[data-button-type=delete]").click(function(e) {
        e.preventDefault();
        var delete_button = $(this);
        var delete_url = $(this).attr('href');

        swal({  title: "{!! trans('backup.delete_confirm') !!}",
                    text: "{!! trans('backup.delete_confirm_message') !!}",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "",
                    cancelButtonText: "{{ trans('backup.cancel') }}",
                    closeOnConfirm: true
                }, function(isConfirm){
                    if (isConfirm) {

                        $.ajax({
                            url: delete_url,
                            beforeSend: function (request){
                                request.setRequestHeader("X-CSRF-TOKEN", $('[name="_token"]').val());
                            },
                            type: 'DELETE',
                            success: function(result) {
                                // Show an alert with the result
                                new PNotify({
                                    title: "{{ trans('backup.delete_confirmation_title') }}",
                                    text: "{{ trans('backup.delete_confirmation_message') }}",
                                    type: "info"
                                });
                                // delete the row from the table
                                delete_button.parentsUntil('tr').parent().remove();
                            },
                            error: function(result) {
                                // Show an alert with the result
                                new PNotify({
                                    title: "{{ trans('backup.delete_error_title') }}",
                                    text: "{{ trans('backup.delete_error_message') }}",
                                    type: "dark"
                                });
                            }
                        });

                    } else {

                        new PNotify({
                            title: "{{ trans('backup.delete_cancel_title') }}",
                            text: "{{ trans('backup.delete_cancel_message') }}",
                            type: "dark"
                        });

                    }
                });


        });
  });
</script>
@endsection
