@extends('dashboard.base')

@section('breadcrumb')
    {!! Breadcrumbs::renderIfExists(\Request::route()->getName()) !!}
@endsection

@section('content')
    {!! Form::open(array('url' => $crud['route'], 'method' => 'post','files' => true, 'class' => 'allcp-form' )) !!}
    <div class="panel panel-visible">
        <div class="panel-heading">

            <div class="panel-title pull-left">
                <h4 class="mb20" id="spy1">{{ trans('crud.add_a_new') }}: {{ _(ucfirst($crud['entity_name'])) }}</h4>
            </div>
            @if (!(isset($crud['view_table_permission']) && !$crud['view_table_permission']))
            <div class="panel-title pull-right">
                <a href="{{ url($crud['route']) }}"><i class="fa fa-angle-double-left"></i> {{ trans('crud.back_to_all') }}</a>
            </div>
            @endif
            <div class="clearfix"></div>
        </div>
        <div class="panel-body ">
            <!-- Default box -->
            
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <!-- load the view from the application if it exists, otherwise load the one in the package -->
            @if( isset($crud["is_translate"]) && $crud["is_translate"] == true )
                @if( view()->exists('vendor.infinety.crud.form_content') )
                    @include('vendor.infinety.crud.form_content_languages')
                @else
                    @include('crud::form_content_languages')
                @endif
            @else
                @if( view()->exists('vendor.infinety.crud.form_content') )
                    @include('vendor.infinety.crud.form_content')
                @else
                    @include('crud::form_content')
                @endif
            @endif
        </div>
        <div class="panel-footer">
            <span>{{ trans('crud.after_saving') }}:</span>

            <div class="mt5">
                <div class="col-md-4">

                    <label class="block mt15 option option-primary">
                        <input type="radio" name="redirect_after_save" value="{{ $crud['route'] }}" checked="">
                        <span class="radio"></span>{{ trans('crud.go_to_the_table_view') }}
                    </label>

                </div>
                <div class="col-md-4">
                    <label class="block mt15 option option-primary">
                        <input type="radio" name="redirect_after_save" value="{{ $crud['route'].'/create' }}" id="create_new_item" >
                        <span class="radio"></span>{{ trans('crud.let_me_add_another_item') }}
                    </label>
                </div>
                <div class="col-md-4">
                    <label class="block mt15 option option-primary">
                        <input type="radio" name="redirect_after_save" value="current_item_edit" id="edit_new_item" >
                        <span class="radio"></span>{{ trans('crud.edit_the_new_item') }}
                    </label>
                </div>
            </div>
            <div class="mt20 inline-object">
                <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />

                <button type="submit" class="btn ladda-button btn-system" data-style="expand-right">
                    <span class="ladda-label">
                        <i class="fa fa-save"></i> {{ _(trans('crud.add')) }}
                    </span>
                    <span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
                </button>
                <a href="{{ url($crud['route']) }}" class="btn btn-default ladda-button" data-style="zoom-in"><span class="ladda-label">{{ trans('crud.cancel') }}</span></a>
            </div>
        </div>
        
    </div>
    {!! Form::close() !!}
@stop

@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.css') }}">
    @yield('styles_crud')
@endsection

@section('footer_scripts')
    <script src="{{ asset('dashboard_theme/assets/js/plugins/ladda/ladda.min.js') }}"></script>
    <script type="text/javascript">
        // Init Ladda Plugin
        $( document ).ready(function() {

            Ladda.bind('.ladda-button', {
                timeout: 4000
            });

            // Simulate loading progress on buttons with ".ladda-button" class
            Ladda.bind('.progress-button', {
                callback: function(instance) {
                    var progress = 0;
                    var interval = setInterval(function() {
                        progress = Math.min(progress + Math.random() * 0.1, 1);
                        instance.setProgress(progress);

                        if (progress === 1) {
                            instance.stop();
                            clearInterval(interval);
                        }
                    }, 200);
                }
            });

        });
    </script>
    @yield('scripts_crud')
@endsection