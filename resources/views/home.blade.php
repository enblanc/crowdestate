@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    <pre>Url de referencia: {{ auth()->user()->affiliateUrl }}</pre>

                    <pre>Roles: {{ auth()->user()->roles->implode('name', ', ')  }}</pre>
                    
                    @if(auth()->user()->referrals->count() > 0)
                        <pre>Usuarios referidos: {{ auth()->user()->referrals->implode('nombre', ', ') }}</pre>
                    @endif

                    @if(auth()->user()->referrer)
                        <pre>Referido por: {{ auth()->user()->referrer->nombre }}</pre>
                    @endif

                    @if(auth()->user()->giftsAsReferral->count() > 0)
                        <pre>Tu proximo regalo por ser referidor es de: <strong>{{ auth()->user()->giftsAsReferral->first()->user_amount }}€</strong></pre>
                    @endif

                    @if(auth()->user()->giftsAsReferred->count() > 0)
                        <pre>Tu proximo regalo por haber sido referido es de: <strong>{{ auth()->user()->giftsAsReferred->first()->referral_amount }}€</strong></pre>
                    @endif
    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
