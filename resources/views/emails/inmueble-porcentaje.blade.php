@component('mail::message')
{{-- Greeting --}}

# {{ __('Inmueble al :percent', ['percent' => $porcentaje]) }}%


{{-- Intro Lines --}}
<p>{{ __('Te queríamos informar que el inmueble') }} <strong>{{ $property->nombre }}</strong> {{ __('a alcanzado el') }} {{ $porcentaje }} %</p>


@lang('emails.general.regards')<br>{{ config('app.name') }}

@endcomponent