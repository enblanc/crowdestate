@component('mail::message')
{{-- Greeting --}}

# Nuevo acreditado en la web


{{-- Intro Lines --}}
<p>La siguiente persona se ha acreditado en contacto desde la página web</p>
<p><strong>Usuario ID:</strong> {{ $user->id }}</p>
<p><strong>Nombre:</strong> {{ $user->nombre }} {{ $user->apellido1 }} {{ $user->apellido2 }}</p>
<p><strong>Email:</strong> {{ $user->email }}</p>
<p><strong>LemonWay Wallet:</strong> {{ $user->lemonway_id }}</p>


 @lang('emails.general.regards')<br>{{ config('app.name') }}

@endcomponent