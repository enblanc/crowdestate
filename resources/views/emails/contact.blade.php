@component('mail::message')
{{-- Greeting --}}

# Nuevo contacto desde la web


{{-- Intro Lines --}}
<p>La siguiente persona se ha puesto en contacto desde la página web</p>
<p><strong>Nombre:</strong> {{ $nombre }}</p>
<p><strong>Email:</strong> {{ $email }}</p>
<p><strong>Teléfono:</strong> {{$telefono}}</p>
<p><strong>Comentarios:</strong> {{ $comentarios }}</p>
<!-- Salutation -->

 @lang('emails.general.regards')<br>{{ config('app.name') }}


@endcomponent
