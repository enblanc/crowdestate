{{-- 
	==========================================================================
	#BIENVENIDO A CROWDESTATE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section>
			<div class="wrapper mb-80">
				<div class="xs-4 col center  text-center  mb-80">
					@include('front/svg/crowdestate', ['class' => 'mt-48 mb-16'])

					<h2>{{ __('¡Gracias por invertir!') }}<br>
					</h2>

					<p>{{ __('Te avisaremos cuando haya novedades referentes a la inversión') }}.</p>

					<a href="{{ route('panel.user.inversion.general') }}" class="button  mt-40">{{ __('Consulta tus inversiones') }}</a>
					
				</div>
			</div>
		</section>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}"></script>

		<!-- Google Code for Inversi&oacute;n Conversion Page -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 829591566;
			var google_conversion_label = "Y6KlCPXfkngQjqDKiwM";
			var google_remarketing_only = false;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/829591566/?label=Y6KlCPXfkngQjqDKiwM&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript>

	@endsection 
