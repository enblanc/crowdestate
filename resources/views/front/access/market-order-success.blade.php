{{-- 
	==========================================================================
	#Market order success
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section>
			<div class="wrapper mb-80">
				<div class="xs-4 col center  text-center  mb-80">
					@include('front/svg/crowdestate', ['class' => 'mt-48 mb-16'])

					<h2>{{ __('¡Orden procesada correctamente!') }}<br>
					</h2>

					<a href="{{ route('panel.user.inversion.general') }}" class="button  mt-40">{{ __('Consulta tus inversiones') }}</a> <br>
					<a href="{{ route('front.market.list') }}" class="button mt-40">{{ __('Mercado') }}</a>
				</div>
			</div>
		</section>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}"></script>
	@endsection 
