{{-- 
	==========================================================================
	#BIENVENIDO A CROWDESTATE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section>
			<div class="wrapper mb-80">
				<div class="xs-4 col center  text-center  mb-80">

					<h2 class="mt-48">{{ __('Obtener nueva contraseña') }}</h2>

				</div>

				<form class="xs-4 m-8 l-6 col center  ph-32 mt-40" role="form" method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                    <input type="hidden" name="token" value="{{ $token }}">

                    @if (count($errors) > 0)
			            @foreach ($errors->all() as $error)
			                <p class="c-error  text-center">{{ $error }}</p>
			            @endforeach
					@endif
					

					<input class="custom-input" type="email" name="email" placeholder="{{ __('E-mail') }}"  value="{{ $email or old('email') }}" required autofocus>
					<input class="custom-input" type="password" name="password" placeholder="{{ __('Contraseña') }}" required>
					<input class="custom-input" type="password" name="password_confirmation" placeholder="{{ __('Confirmar contraseña') }}" required>


				 	<input class="button button--full  |  mt-56" type="submit" value="{{ __('Guardar nueva contraseña') }}">
					
				</form>

				</div>
			</div>
		</section>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
	@endsection


	{{-- JS --}}

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}"></script>
	@endsection 
