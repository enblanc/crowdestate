{{-- 
	==========================================================================
	#PROPERTY SINGLE - TABS RESUMEN PROYECTO
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}	

	<div class="tabs-selector__item  tabs-selector__item--is-active" id="tab-resumen">

		
		{{-- #RAZONES INVERTIR --}}
		<div class="wrapper  clearfix  mb-64">
			

			{{-- titulo --}}
			<div class="xs-4 col  mb-40">
				<h3 class="mv-8">{{ __('¿Por qué invertir en este apartamento?') }}</h3>
				<div class="separator--horizontal"></div>
			</div>
			{{-- fin titulo --}}

			

			{{-- razon 1 --}}
			<div class="xs-4 m-4 col  mb-32 mb-l-0">

				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->invertir_first_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->invertir_first_data }}</p>
				</div>
				
			</div>
			{{-- fin razon 1 --}}


			{{-- razon 2 --}}
			<div class="xs-4 m-4 col  mb-32 mb-l-0">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->invertir_second_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->invertir_second_data }}</p>
				</div>

			</div>
			{{-- fin razon 2 --}}


			{{-- razon 3 --}}
			<div class="xs-4 m-4 col">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->invertir_third_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->invertir_third_data }}</p>
				</div>

			</div>
			{{-- fin razon 3 --}}


		</div>
		{{-- FIN RAZONES INVERTIR --}}
			
			
		

		{{-- #INFORMACION DEL PROYECTO --}}
		<div class="wrapper">


			{{-- titulo --}}
			<div class="xs-4 col  mb-24">
				<h3 class="mv-8">{{ __('Información sobre el apartamento') }}</h3>
				<div class="separator--horizontal"></div>
			</div>
			{{-- fin titulo --}}


			{{-- descripcion --}}
			<div class="xs-4 l-8 col  pr-24  mb-64">
				{!! $property->getTranslationOrDefault()->descripcion !!}
			</div>
			{{-- fin descripcion --}}
			

			{{-- similares --}}
			<div class="xs-4 l-4 col  ph-0 mb-64">
				@include('front/property-single/assets/apartamentos-similares')
			</div>
			{{-- fin similares --}}


		</div>
		{{-- FIN INFORMACION DEL PROYECTO --}}

		




		{{-- #DATOS --}}
		<div class="bg-4  mt-8 mb-80">
			<div class="wrapper  pv-32">
			
			@php
			$datos = [
				[ __('Ciudad'), $property->ubicacion],
				[ __('Barrio'), $property->barrio],
				[ __('Antigüedad'), $property->antiguedad],
				[ __('Referencia Catastral'), $property->catastro],
				[ __('Tipología del inmueble'), $property->type ? $property->type->nombre : null],
				[ __('Tipo de inversión'), $property->investment ? $property->investment->nombre: null],
				[ __('Metros cuadrados'), $property->metros],
				[ __('Nº de habitaciones'), $property->habitaciones],
				[ __('Nº de baños'), $property->toilets],
				[ __('Ascensor'), $property->ascensor],
				[ __('Piscina'), $property->piscina],
				[ __('Garaje'), $property->garaje],
				[ __('Zona premium'), $property->premium],
				[ __('Gestor'), $property->gestor],
				[ __('Promotor'), $property->developer ? $property->developer->nombre : null],
			];
			$index = 0;
			@endphp

			@foreach ($datos as $dato)

				@if($dato[1] && $dato[1]!='')
				
					@if($index%2 == 0 && $index != 0)
						
						@if($index%4 == 0)
							<div class="clearfix from-m"></div>
						@else
							<div class="clearfix only-m"></div>
						@endif
		
					@endif

					<div class="xs-4 m-6 l-3 col  mv-16">
						<p class="pmv-0">{{ $dato[0] }}</p>
						<p class="small-text c-2  pmv-0">
							
							@if($index == 0)
									@include('front/svg/localizacion')
							@endif
								
							@php 
								if($dato[1] == '1' && ($dato[0] !== __('Nº de baños') && $dato[0] !== __('Nº de habitaciones'))){
									echo __('Si');
								}else{
									echo $dato[1];
								}
							@endphp

						</p>
					</div>

					@php 
						$index++;
					@endphp

				@endif

			@endforeach
				
			</div>
		</div>
		{{-- fIN DATOS --}}






		{{-- APARTADO LOCALIZACIÓN --}}
		@php $map_number = '' @endphp
		@include('front/property-single/assets/localizacion')

		

	</div>