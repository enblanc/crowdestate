{{-- 
	==========================================================================
	#PROPERTY SINGLE - TABS DOCUMENTOS
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}	

	<div class="tabs-selector__item" id="tab-documentos">


		{{--
			Si el usuario no está registrado hay que mostrar el siguiente bloque
			y ocultar el resto :
			
			@include('front/property-single/assets/contenido-bloqueado') 
		--}}



		<div class="wrapper mt-56 mb-40">

			@if(!auth()->check())
				<p class="text-center  mb-48">{{ __('Esta es la documentación básica') }}. {{ __('Si quieres ver toda la información del inmueble') }} <a href="{{ route('registro') }}">{{ __('regístrate') }}</a> {{ __('o') }} <a href="{{ route('login') }}">{{ __('inicia sesión') }}</a>.</p>
			@endif

			@php
				$docsPublicos = $property->getDocumentsByTypeAndLocale('public');
        		$docsPrivados = $property->getDocumentsByTypeAndLocale('private');
			@endphp

			@if(count($docsPublicos) == 0 && count($docsPrivados) == 0)
				<p class="text-center  mb-48">{{ __('En breve tendremos documentación sobre el inmueble') }}</p>
			@endif

			@if(count($docsPublicos) > 0)
				@foreach ($docsPublicos as $item)
					<div class="xs-4 m-6 col  mt-8 mb-40">
						<a class="card card--link  |  d-table  bg-4 bgh-m exclude-svg  pmv-0" href="{{ url($item['url']) }}" download>

							<span class="d-cell v-middle text-center  pa-24">
								@include('front/svg/file', ['class' => 'card__asset'])
								<h4 class="card__title">@include('front/svg/download', ['class' => 'card__asset  v-sub mr-8']) {{ $item['name'] }}</h4>
							</span>

						</a>
					</div>
				@endforeach
			@endif

			@if(auth()->check())
				@if(count($docsPrivados) > 0)
					@foreach ($docsPrivados as $item)
						<div class="xs-4 m-6 col  mt-8 mb-40">
							<a class="card card--link  |  d-table  bg-4 bgh-m exclude-svg  pmv-0" href="{{ url($item['url']) }}" download>

								<span class="d-cell v-middle text-center  pa-24">
									@include('front/svg/file', ['class' => 'card__asset'])
									<h4 class="card__title">@include('front/svg/download', ['class' => 'card__asset  v-sub mr-8']) {{ $item['name'] }}</h4>
								</span>

							</a>
						</div>
					@endforeach
				@endif
			@endif
		</div>
	</div>