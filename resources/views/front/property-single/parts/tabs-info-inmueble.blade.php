{{-- 
	==========================================================================
	#PROPERTY SINGLE - TABS INFORMACIÓN INMUEBLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}	

	<div class="tabs-selector__item" id="tab-informacion">
		
		
		@if(!auth()->check())
			@include('front/property-single/assets/contenido-bloqueado') 
		@else

		@if($property->getTranslationOrDefault()->mercado_first_title != '' && $property->getTranslationOrDefault()->mercado_second_title != '' && $property->getTranslationOrDefault()->mercado_third_data != '')

			{{-- #RAZONES INVERTIR --}}
		<div class="wrapper  clearfix  mb-64">
	
		

			{{-- titulo --}}
			<div class="xs-4 col  mb-40">
				<h3 class="mv-8">{{ __('¿Por qué invertir en este apartamento?') }}</h3>
				<div class="separator--horizontal"></div>
			</div>
			{{-- fin titulo --}}
			
			{{-- razon 1 --}}
			<div class="xs-4 m-4 col  mb-32 mb-l-0">

				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->mercado_first_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->mercado_first_data }}</p>
				</div>
				
			</div>
			{{-- fin razon 1 --}}


			{{-- razon 2 --}}
			<div class="xs-4 m-4 col  mb-32 mb-l-0">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->mercado_second_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->mercado_second_data }}</p>
				</div>

			</div>
			{{-- fin razon 2 --}}


			{{-- razon 3 --}}
			<div class="xs-4 m-4 col">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->mercado_third_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->mercado_third_data }}</p>
				</div>

			</div>
			{{-- fin razon 3 --}}


		</div>
		{{-- FIN RAZONES INVERTIR --}}
		@endif

			{{-- #DESCRIPCION DEL PROYECTO --}}
			<div class="wrapper">


				{{-- titulo --}}
				<div class="xs-4 col  mb-24">
					<h3 class="mv-8">{{ __('Resumen del proyecto') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}


				{{-- descripcion --}}
				<div class="xs-4 col  pr-24  mb-64">
					{!! $property->getTranslationOrDefault()->ficha !!}
				</div>
				{{-- fin descripcion --}}

				
				{{-- similares --}}
				{{-- <div class="xs-4 l-4 col ph-0  mb-64">
					@include('front/property-single/assets/promotor')
					&nbsp;
				</div> --}}
				{{-- fin similares --}}


			</div>
			{{-- #DESCRIPCION DEL PROYECTO --}}


			



			{{-- #RESUMEN FINANCIERO --}}
			<div class="wrapper  clearfix">


				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Resumen de la operación') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
			

			</div>
			<div class="custom-table__wrapper  |  wrapper">

				<div class="custom-table__col  |  xs-4 col  mb-80">

					
					{{-- datos falsos --}}
					@php
						$datos = [
							[ __('Precio de compra del inmueble (inc. reforma)'), formatThousands($property->valor_compra).' €'],
							[ __('Hipoteca'), formatThousands($property->hipoteca).' €'],
							[ __('Coste de obras iniciales'), formatThousands($property->coste_obras).' €'],
							[ __('Impuestos, trámites y otros costes legales iniciales'), formatThousands($property->impuestos_tramites).' €'],
							[ __('Constitución y Registro Sociedad'), formatThousands($property->registro_sociedad).' €'],
							[ __('Otros costes de Adquisición del inmueble'), formatThousands($property->otros_costes).' €'],
							[ __('Imprevistos'), formatThousands($property->imprevistos).' €'],
							[ __('Caja mínima de seguridad'), formatThousands($property->caja_minima).' €'],
							[ __('Honorarios Brickstarter (Solo de la compra)'), formatThousands($property->honorarios).' €']
						]
					@endphp
					{{-- fin datos falsos --}}
					

					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach
					</table>
					{{-- fin tabla --}}
					
					{{-- separador tabla --}}
					<div class="separator-dashed--horizontal  |  mv-40"></div>

					{{-- tabla 4: resultado total --}}
					<table class="custom-table custom-table--total  |  mt-40">
						<tr class="bg-m">
							<td class="h4 c-w">{{ __('Total') }}</td>
							<td class="h4 c-w text-right">{{ formatThousands($property->totalGastos) }} €</td>
						</tr>
					</table>
					{{-- fin tabla 4 --}}


				</div>
			</div>
			{{-- FIN RESUMEN FINANCIERO --}}






			{{-- #PREVISION ECONOMICA --}}
			<div class="wrapper  clearfix">


				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Previsión económica') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
			
			</div>
			<div class="custom-table__wrapper  |  wrapper  mb-80">
				

				<div class="custom-table__col  |  xs-4 col">

					
					{{-- datos falsos --}}
					@php
						$datos = [
							[ __('Ingresos anuales por alquiler'), formatThousands($property->ingresos_anuales_alquiler).' €'],
							[ __('Importe a recibir por la venta'), formatThousands($property->importe_venta).' €'],
							[ __('Margen Bruto (Precio venta – Precio compra)'), formatThousandsNotZero($property->compra_venta_margen_bruto).' %']
						]
					@endphp
					{{-- fin datos falsos --}}
					

					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach
					</table>
					{{-- fin tabla --}}


				</div>
			</div>
			{{-- FIN PREVISION ECONOMICA --}}
				

			{{-- APARTADO LOCALIZACIÓN --}}
			@php $map_number = 2 @endphp
			{{-- @include('front/property-single/assets/localizacion')			 --}}
		@endif


	</div>