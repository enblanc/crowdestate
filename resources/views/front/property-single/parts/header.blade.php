{{-- 
	==========================================================================
	#PROPERTY SINGLE - HEADER
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#VISIBLE
	========================================================================== --}}

	<section class="overlay overlay--gradient  |  p-relative bg   [ js-modal c-pointer ]" modal-ref="galeria" style="background-image: url('{{ $property->getFirstMediaUrl($property->mediaCollection) }}');">
		<div class="property__foto  |  wrapper  p-relative  pv-40">


		
			{{-- #NOMBRE Y DIRECCIÓN --}}
			<div class="xs-4 m-6 col  all-w  pt-80  from-l">

				<h1 class="h2">{{ $property->nombre }}</h1>
				<p class="big-text">{{ $property->direccion }}</p>

			</div>
			{{-- FIN NOMBRE Y DIRECCIÓN --}}




			<div class="xs-4 m-1 col false  from-l"></div>




			{{-- #BLOQUE DATOS DESTACADOS --}}
			<div class="xs-4 m-6 l-5 col  from-l   [ not-modal c-default ]">
				<div class="xs-4  p-relative clearfix  bg-w-95  pv-24 ph-32">
					

					{{-- datos objetivo --}}
					<div class="left">
						<p class="small-text  pmv-0">{{ __('Financiado') }}</p>
						<h4 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h4>
					</div>
					<div class="right">
						<p class="small-text  pmv-0">{{ __('Total') }}</p>
						<h4 class="pmv-0">{{ formatThousandsNotDecimals($property->objetivo) }} €</h4>
					</div>
					<div class="clearfix  pb-2"></div>

					<div class="bar  |  mv-8">
						<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
					</div>

					<p class="small-text  left  pmv-0">{{ $property->fakeOrRealInversores }} {{ __('Inversores') }}</p>
					<p class="small-text  right  pmv-0">
						@if($property->state->id == 2)
							@if($property->porcentajeCompletado == 100)
								{{ __('Completado') }}
							@elseif($property->remainingDays < 0)
								{{ __('No conseguido') }}
							@else
								{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
							@endif
						@else
							{{ $property->state->nombre }}
						@endif
					</p>

					<div class="clearfix  pb-2 mb-32"></div>
					{{-- fin datos objetivo --}}

					
					{{-- bloque 1 de datos --}}
					@php
						$datos = [
							[__('Total de la Operación:'), formatThousands($property->coste_total).' €'],
							[__('Hipoteca:'), formatThousands($property->hipoteca).' €'],
							[__('Total a Financiar:'), formatThousands($property->objetivo).' €']
						]
					@endphp

					@foreach ($datos as $dato)
						<div class="d-table  xs-4">
							<div class="d-cell v-middle  pr-16">
								<p class="small-text nowrap  pmv-0">{{ $dato[0] }}</p>
							</div>
							<div class="d-cell  xs-4  v-middle"><span class="line"></span></div>
							<div class="d-cell v-middle  pl-16">
								<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
							</div>
						</div>
					@endforeach
					<div class="clearfix mb-24"></div>
					{{-- fin bloque 1 de datos --}}
					
					
					{{-- bloque 2 de datos --}}
					@php
						$datos2 = [
							[ __('Tasa Interna de Rentabilidad (TIR):'), formatThousands($property->tasa_interna_rentabilidad).' %'],
							[ __('Rentabilidad Alquiler Bruta:'), formatThousands($property->rentabilidad_alquiler_bruta).' %'],
							[ __('Rentabilidad Alquiler Neta:'), formatThousands($property->rentabilidad_alquiler_neta).' %'],
							[ __('Cash on Cash'), formatThousands($property->rentabilidad_objetivo_bruta).' %'],
							[ __('Rentabilidad Financiera (ROE)'), formatThousands($property->rentabilidad_objetivo_neta).' %']
						]
					@endphp

					@foreach ($datos2 as $dato)
						<div class="d-table  xs-4">
							<div class="d-cell v-middle  pr-16">
								<p class="small-text nowrap  pmv-0">{{ $dato[0] }}</p>
							</div>
							<div class="d-cell  xs-4  v-middle"><span class="line"></span></div>
							<div class="d-cell v-middle  pl-16">
								<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
							</div>
						</div>
					@endforeach
					{{-- fin bloque 2 de datos --}}


					<a href="" class="small-text  d-block  mt-24 mb-32   [ js-open-info ]" data-ref="extra-info">
						@include('front/svg/duda', ['class' => 'v-middle']) {{ __('¿Qué significan estas cifras?') }}
					</a>
					@if($property->state->id == 2 && $property->porcentajeCompletado != 100 && $property->remainingDays > 0)
						@if(auth()->check())
							<form action="{{ route('front.property.invertir.check') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="property_id" value="{{ $property->id }}">
								<button class="button button--full" type="submit">{{ __('Invertir') }}</button>
							</form>
						@else
							<a class="button button--full" href="{{ route('login') }}">{{ __('Invertir') }}</a>
						@endif
					@endif



					{{-- explicación --}}
					<div class="info__explanation  |  bg-1 all-w  ph-32 pt-32 pb-24 o-scroll" id="extra-info">
						@include('front/svg/close', ['class' => 'info__close js-close-info c-pointer'])
						<p class="small-text  mv-8">
							<strong>{{ __('Total operación') }}:</strong> {{ __('Inversión total en el proyecto') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Hipoteca') }}:</strong> {{ __('Financiación bancaria que se va a solicitar para la consecución del proyecto') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Total a Financiar') }}:</strong> {{ __('Financiación solicitada a inversores') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('TIR') }}:</strong> {{ __('Rentabilidad que ofrece la inversión') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Rentabilidad Alquiler Bruta') }}:</strong> {{ __('Total de los ingresos anuales provenientes del alquiler del inmueble dividido entre el coste total de la operación') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Rentabilidad Alquiler Neta') }}:</strong> {{ __('Rentabilidad del inversor por el alquiler (una vez descontados todos los gastos del proyecto), respecto al total de la operación') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Cash on Cash') }}:</strong> {{ __('Ingresos obtenidos por el accionista en relacion a su inversión') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('ROE') }}:</strong> {{ __('Rentabilidad Financiera Neta para el Accionista') }}.
						</p>
						
					</div>
					{{-- fin explicación --}}
					

				</div>
			</div>
			{{-- FIN BLOQUE DATOS DESTACADOS --}}
			
			


			{{-- #MOSTRAR GALERÍA --}}
			<a href="#" class="gallery__button  |  ch-w  pmv-0">
				@include('front/svg/camara', ['class' => 'v-baseline mr-8']) {{ __('Galería de fotos') }}
			</a>
			{{-- FIN MOSTRAR GALERÍA --}}



		</div>
	</section>







{{--==========================================================================
	#MODAL
	========================================================================== --}}
	
	<section class="to-m">

		<div class="wrapper">
		{{-- #NOMBRE Y DIRECCIÓN --}}
			<div class="xs-4 m-6 col  pv-40">

				<h2>{{ $property->nombre }}</h2>
				<p class="big-text">{{ $property->direccion }}</p>

			</div>

			<div class="m-6 col  only-m  mt-72">
				{{-- datos objetivo --}}
					
					<p class="small-text  pmv-0">{{ __('Financiado') }}</p>
					<h4 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h4>
					
					<p class="small-text  pmv-0">{{ __('Total') }}</p>
					<h4 class="pmv-0">{{ formatThousands($property->objetivo) }} €</h4>
					
					<div class="clearfix  pb-2"></div>

					<div class="bar  |  mv-8">
						<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
					</div>

					<p class="small-text  left  pmv-0">{{ $property->fakeOrRealInversores }} Inversores</p>
					<p class="small-text  right  pmv-0">
						@if($property->state->id == 2)
							@if($property->porcentajeCompletado == 100)
								{{ __('Completado') }}
							@elseif($property->remainingDays < 0)
								{{ __('No conseguido') }}
							@else
								{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
							@endif
						@else
							{{ $property->state->nombre }}
						@endif
					</p>

					<div class="clearfix  pb-2 mb-32"></div>
					{{-- fin datos objetivo --}}
			</div>
			{{-- FIN NOMBRE Y DIRECCIÓN --}}
		</div>






			{{-- #BLOQUE DATOS DESTACADOS --}}
		<div class="separator--horizontal"></div>
		<div class="wrapper  clearfix">
			<div class="xs-4  col">
				<div class="xs-4  p-relative clearfix  bg-w-95  pv-24">
					

					{{-- datos objetivo --}}
					<div class="only-xs">
					
						<p class="small-text  pmv-0">{{ __('Financiado') }}</p>
						<h4 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h4>
						
						<p class="small-text  pmv-0">{{ __('Total') }}</p>
						<h4 class="pmv-0">{{ formatThousands($property->objetivo) }} €</h4>
						
						<div class="clearfix  pb-2"></div>

						<div class="bar  |  mv-8">
							<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
						</div>

						<p class="small-text  left  pmv-0">{{ $property->fakeOrRealInversores }} Inversores</p>
						<p class="small-text  right  pmv-0">
							@if($property->state->id == 2)
								@if($property->porcentajeCompletado == 100)
									{{ __('Completado') }}
								@elseif($property->remainingDays < 0)
									{{ __('No conseguido') }}
								@else
									{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
								@endif
							@else
								{{ $property->state->nombre }}
							@endif
						</p>

					</div>
					<div class="clearfix  pb-2 mb-32"></div>
					{{-- fin datos objetivo --}}

					
					{{-- bloque 1 de datos --}}
					<div class="xs-4 m-6 left">
	

						@foreach ($datos as $dato)
							<div class="pb-8">
								<p class="small-text nowrap  pmv-0">{{ $dato[0] }}</p>
								<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
							</div>
						@endforeach
						<div class="clearfix mb-24"></div>
					</div>
						{{-- fin bloque 1 de datos --}}


						
					
					{{-- bloque 2 de datos --}}
					<div class="xs-4 m-6 left">

						@foreach ($datos2 as $dato)
							<div class="pb-8">
								<p class="small-text nowrap  pmv-0">{{ $dato[0] }}</p>
								<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
							</div>
						@endforeach
					</div>
					<div class="clearfix"></div>
					{{-- fin bloque 2 de datos --}}


					<a href="" class="small-text  d-block  mt-24 mb-32   [ js-open-info ]" data-ref="extra-info2">
						@include('front/svg/duda', ['class' => 'v-middle']) {{ __('¿Qué significan estas cifras?') }}
					</a>

					@if($property->state->id == 2 && $property->porcentajeCompletado != 100 && $property->remainingDays > 0)
						@if(auth()->check())
							<form action="{{ route('front.property.invertir.check') }}" method="POST">
								{{ csrf_field() }}
								<input type="hidden" name="property_id" value="{{ $property->id }}">
								<button class="button button--full" type="submit">{{ __('Invertir') }}</button>
							</form>
						@else
							<a class="button button--full  mb-16" href="{{ route('login') }}">{{ __('Invertir') }}</a>
						@endif
					@endif

					



					{{-- explicación --}}
					<div class="info__explanation  |  bg-1 all-w  ph-16 pt-48 pb-24" id="extra-info2">
						@include('front/svg/close', ['class' => 'info__close js-close-info c-pointer'])
						<p class="small-text  mv-8">
							<strong>{{ __('Total operación') }}:</strong> {{ __('Inversión total en el proyecto') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Hipoteca') }}:</strong> {{ __('Financiación bancaria que se va a solicitar para la consecución del proyecto') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Total a Financiar') }}:</strong> {{ __('Financiación solicitada a inversores') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('TIR') }}:</strong> {{ __('Rentabilidad que ofrece la inversión') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Rentabilidad Alquiler Bruta') }}:</strong> {{ __('Total de los ingresos anuales provenientes del alquiler del inmueble dividido entre el coste total de la operación') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Rentabilidad Alquiler Neta') }}:</strong> {{ __('Rentabilidad del inversor por el alquiler (una vez descontados todos los gastos del proyecto), respecto al total de la operación') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('Cash on Cash') }}:</strong> {{ __('Ingresos obtenidos por el accionista en relacion a su inversión') }}.
						</p>
						<p class="small-text  mv-8">
							<strong>{{ __('ROE') }}:</strong> {{ __('Rentabilidad Financiera Neta para el Accionista') }}.
						</p>
					</div>
					{{-- fin explicación --}}
					

				</div>
			</div>
			{{-- FIN BLOQUE DATOS DESTACADOS --}}
		</div>
		<div class="separator--horizontal"></div>
	</section>




{{--==========================================================================
	#MODAL
	========================================================================== --}}

	<div class="modal modal--full-page" id="galeria">
		<div class="modal__container">
			


			{{-- #TÍTULO --}}
			<div class="modal__header  pb-32">
				<h3 class="c-w  mv-0">
					<span class="not-xs-inline">{{ $property->nombre }}</span>
					<span class="right c-pointer  [ js-modal-close ]">@include('front/svg/close', ['class' => 'modal__svg'])</span>
				</h3>
			</div>
			{{-- FIN TÍTULO --}}


			
			{{-- #SLIDE --}}
			<div class="modal__content">
				<div class="slide slide--ambientes  owl-carousel [ js-slide-full ]">

					@foreach($property->getMedia($property->mediaCollection) as $image)
						<div class="slide__item  p-relative">
							<div class="slide__foto  |  p-relative">
								<img src="{{ $image->getUrl() }}" alt="">
							</div>
							<div class="slide__text  |  p-relative">
								<p class="c-w">{{ $image->getCustomProperty('text', '') }}</p>
							</div>
						</div>
					@endforeach

				</div>
				
				@if($property->getMedia($property->mediaCollection)->count() > 1)
					<div class="slide__arrow slide__arrow--left  not-xs    [ js-slide-full--left  ]">
						@include('front/svg/arrow-left')
					</div>
					<div class="slide__arrow slide__arrow--right  not-xs   [ js-slide-full--right ]">
						@include('front/svg/arrow-right')
					</div>
				@endif
				
			</div>
			{{-- FIN SLIDE --}}



		</div>
	</div>