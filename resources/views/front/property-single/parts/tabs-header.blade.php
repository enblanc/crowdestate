{{-- 
	==========================================================================
	#PROPERTY SINGLE - TABS HEADER
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}	

	<section class="pb-80">
		
		<div class="tabs-selector  |  text-center  mb-40">
			<div class="wrapper">
				<div class="xs-4 p-relative  ph-0">

					
					{{-- #SELECTORES DE BLOQUE: el 1º siempre activo (.tab-selector--is-active) --}}
					<div class="tab-selector xs-4 l-fifth col  ph-0   [ js-tab-selector ]   tab-selector--is-active" data-ref="tab-resumen">
						<h5 class="tab-selector__title  |  p-relative  pmv-0">{{ __('Información del inmueble') }}</h5>
					</div>

					<div class="tab-selector xs-4 l-fifth col  ph-0   [ js-tab-selector ]" data-ref="tab-informacion">
						<h5 class="tab-selector__title  |  p-relative  pmv-0">{{ __('Resumen del proyecto') }}</h5>
					</div>

					<div class="tab-selector xs-4 l-fifth col  ph-0   [ js-tab-selector ]" data-ref="tab-financiera">
						<h5 class="tab-selector__title  |  p-relative  pmv-0">{{ __('Información financiera') }}</h5>
					</div>

					<div class="tab-selector xs-4 l-fifth col  ph-0   [ js-tab-selector ]" data-ref="tab-documentos">
						<h5 class="tab-selector__title  |  p-relative  pmv-0">{{ __('Documentación') }}</h5>
					</div>

					<div class="tab-selector xs-4 l-fifth col  ph-0   [ js-tab-selector ]" data-ref="tab-mercado">
						<h5 class="tab-selector__title  |  p-relative  pmv-0">{{ __('Estudio de mercado') }}</h5>
					</div>
					{{-- FIN SELECTORES DE BLOQUE --}}

					
					{{-- #TESTIGO DE BLOQUE ELEGIDO --}}
					<div class="tab-selector__active">
						<h5 class="tab-selector__title tab-selector__selected  |  c-w  to-m  text-left pmv-0 pl-24">{{ __('Resumen del proyecto') }}</h5>
					</div>
						

				</div>
			</div>	
		</div>

