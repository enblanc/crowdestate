{{-- 
	==========================================================================
	#PROPERTY SINGLE - TABS INFORMACIÓN FINANCIERA
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}	

	<div class="tabs-selector__item" id="tab-financiera">
		
		

		
		@if(!auth()->check())
			@include('front/property-single/assets/contenido-bloqueado') 
		@else
			{{-- #INTRO --}}
			<div class="wrapper">
				<div class="xs-4 col  text-center  mb-40">
					<p class="big-text">{{ __('Los datos mostrados en este apartado son una previsión a un año tras la compra del inmueble') }}</p>
				</div>
			</div>
			{{-- FIN INTRO --}}


			



			{{-- #DIVIDENDOS --}}
			<div class="wrapper  clearfix">
				

				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Dividendos') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
			

			</div>
			<div class="custom-table__wrapper  |  wrapper  clearfix">

				<div class="custom-table__col  |  xs-4 col  mb-80">
					

					{{-- datos falsos --}}
					@php
						$datos = [
							[__('Total dividendos devengados'), formatThousands($property->predicted->first()->total_dividendos_devengados).' €'],
							[__('Dividendos abonados'), formatThousands($property->predicted->first()->total_dividendos_abonados).' €'],
							[__('Dividendos en cartera'), formatThousands($property->predicted->first()->dividendos_cartera).' €']
							
						]
					@endphp
					{{-- fin datos falsos --}}
					

					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach
					</table>
					{{-- fin tabla --}}


				</div>
			</div>
			{{-- FIN DIVIDENDOS --}}






			{{-- #RESULTADO EXPLOTACIÓN --}}
			<div class="wrapper  clearfix">
				

				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Resultado de explotación') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
			


			</div>
			<div class="custom-table__wrapper  |  wrapper  clearfix">

				<div class="custom-table__col  |  xs-4 col  mb-80">
					

					{{-- datos falsos 1 --}}
					@php
						$datos = cleanArray([
							[__('Ingresos alquiler'), formatThousands($property->predicted->first()->ingresos_alquiler).' €'],
							[__('Otros Ingresos'), formatThousands($property->predicted->first()->otros_ingresos).' €']
						]);
					@endphp
					{{-- fin datos falsos 1 --}}

					
					{{-- tabla 1 --}}
					<table class="custom-table {{ (count($datos) > 1) ? 'custom-table--parcial' : '' }}">
						<tr class="text-left">
							<th class="h4">{{ __('Ingresos') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach

						@if(count($datos) > 1 )
						<tr>
							<td class="h4">{{ __('Ingresos totales') }}</td>
							<td class="h4 text-right">{{ formatThousands($property->predicted->first()->ingresos_totales) }} €</td>
						</tr>
						@endif

					</table>
					{{-- fin tabla 1 --}}
					

					{{-- separador tabla 1 - 2 --}}
					<div class="separator-dashed--horizontal  |  mt-40 mb-16"></div>
					

					{{-- datos falsos 2 --}}
					@php
						$datos = cleanArray([
							[ __('Publicidad y prensa'), formatThousands($property->predicted->first()->publicidad_prensa).' €'],
							[ __('Otro material'), formatThousands($property->predicted->first()->otro_material).' €'],
							[ __('I + D'), formatThousands($property->predicted->first()->i_mas_d).' €'],
							[ __('Arrendamientos'), formatThousands($property->predicted->first()->arrendamientos).' €'],
							[ __('Comisión Brickstarter'), formatThousands($property->predicted->first()->comisiones_crowdestate).' €'],
							[ __('Gestión'), formatThousands($property->predicted->first()->gestion).' €'],
							[ __('Suministros'), formatThousands($property->predicted->first()->suministros).' €'],
							[ __('IBI'), formatThousands($property->predicted->first()->ibi).' €'],
							[ __('Otras Tasas'), formatThousands($property->predicted->first()->otras_tasas).' €']
						]);
					@endphp
					{{-- fin datos falsos 2 --}}
					

					{{-- tabla 2 --}}
					<table class="custom-table custom-table--parcial">
						<tr class="text-left">
							<th class="h4">{{ __('Gastos') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach
						<tr>
							<td class="h4">{{ __('Gastos totales') }}</td>
							<td class="h4 text-right">{{ formatThousands($property->predicted->first()->gastos_explotacion) }} €</td>
						</tr>
					</table>
					{{-- fin tabla 2 --}}

					
					{{-- separador tabla 2 - 3 --}}
					<div class="separator-dashed--horizontal  |  mt-40 mb-16"></div>
					

					{{-- datos falsos 3 --}}
					@php
						$datos = cleanArray([
							[ __('Amortización'), formatThousands($property->predicted->first()->amortizacion).' €'],
							[ __('Imputación de subvenciones'), formatThousands($property->predicted->first()->subvenciones).' €'],
							[ __('Resultado de la enajenación del inmovilizado'), formatThousands($property->predicted->first()->enajenacion_inmovilizado).' €']
						]);
					@endphp
					{{-- fin datos falsos 3 --}}
					
					
					{{-- tabla 3 --}}
					@if(count($datos)!=0)
						<table class="custom-table">
							<tr class="text-left">
								<th class="h4">{{ __('Otras partidas') }}</th>
								<th class="h4 text-right">{{ __('Valor') }}</th>
							</tr>
							@foreach ($datos as $dato)
								<tr>
									<td>{{ $dato[0] }}</td>
									<td class="text-right">{{ $dato[1] }}</td>
								</tr>
							@endforeach
						</table>
					
					{{-- fin tabla 3 --}}
					

					{{-- separador tabla 3 - 4 --}}
					<div class="separator-dashed--horizontal  |  mv-40"></div>
					@endif

					{{-- tabla 4: resultado total --}}
					<table class="custom-table custom-table--total  |  mt-40">
						<tr class="bg-m">
							<td class="h4 c-w">{{ __('Resultado de explotación') }}</td>
							<td class="h4 c-w text-right">{{ formatThousands($property->predicted->first()->resultado_explotacion) }} €</td>
						</tr>
					</table>
					{{-- fin tabla 4 --}}


				</div>
			</div>
			{{-- FIN RESULTADO EXPLOTACIÓN --}}





			
			{{-- #RESULTADO FINANCIERO --}}
			<div class="wrapper  clearfix">
				

				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Resultado financiero') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}



			</div>
			<div class="custom-table__wrapper  |  wrapper  clearfix">


				<div class="custom-table__col  |  xs-4 col  mb-80">
					

					{{-- datos falsos --}}
					@php
						$datos = [
							[ __('Ingresos financieros'), formatThousands($property->predicted->first()->ingresos_financieros).' €'],
							[ __('Gastos financieros'), formatThousands($property->predicted->first()->gastos_financieros).' €'],
							// ['Diferencias de tipo de cambio', formatThousands($property->predicted->first()->ingresos_totales).' €'],
							[ __('Enajenación instrumentos financiero'), formatThousands($property->predicted->first()->otros_instrumentos_financieros).' €']
						]
					@endphp
					{{-- fin datos falsos --}}


					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							@if($dato[1]!=0)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
							@endif
						@endforeach
					</table>
					{{-- tabla 1 --}}

					{{-- separador tabla --}}
					<div class="separator-dashed--horizontal  |  mv-40"></div>

					{{-- tabla 4: resultado total --}}
					<table class="custom-table custom-table--total  |  mt-40">
						<tr class="bg-m">
							<td class="h4 c-w">{{ __('Resultado financiero') }}</td>
							<td class="h4 c-w text-right">{{ formatThousands($property->predicted->first()->resultado_financiero) }} €</td>
						</tr>
					</table>
					{{-- fin tabla 4 --}}


				</div>
			</div>
			{{-- FIN RESULTADO FINANCIERO --}}







			{{-- #RESULTADO EJERCICIO --}}
			<div class="wrapper  clearfix">


				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Resultado del ejercicio') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
			


			</div>
			<div class="custom-table__wrapper  |  wrapper  clearfix">

				<div class="custom-table__col  |  xs-4 col  mb-80">


					{{-- datos falsos --}}
					@php
						$datos = [
							[ __('Resultado antes del impuesto (Resultado de explotación + Resultado financiero)'), formatThousands($property->predicted->first()->resultado_antes_impuestos).' €'],
							[ __('Impuestos'), formatThousands($property->predicted->first()->impuestos).' €']
						]
					@endphp
					{{-- fin datos falsos --}}

					
					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach

					</table>
					{{-- fin tabla --}}

					{{-- separador tabla --}}
					<div class="separator-dashed--horizontal  |  mv-40"></div>

					{{-- tabla 4: resultado total --}}
					<table class="custom-table custom-table--total  |  mt-40">
						<tr class="bg-m">
							<td class="h4 c-w">{{ __('Resultado del ejercicio') }}</td>
							<td class="h4 c-w text-right">{{ formatThousands($property->predicted->first()->resultados_ejercicio) }} €</td>
						</tr>
					</table>
					{{-- fin tabla 4 --}}


				</div>
			</div>
			{{-- FIN RESULTADO EJERCICIO --}}






			{{-- #INFORMACION PATRIMONIAL --}}
			<div class="wrapper  clearfix">
				
				
				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Información patrimonial') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
			


			</div>
			<div class="custom-table__wrapper  |  wrapper  clearfix">

				<div class="custom-table__col  |  xs-4 col  mb-80">

					
					{{-- datos falsos --}}
					@php
						$datos = [
							[ __('Revalorización del inmueble'), formatThousands($property->predicted->first()->revalorizacion_inmueble).' %'],
							[ __('Valor actual del inmueble'), formatThousands($property->predicted->first()->valor_actual_inmueble).' €']
						]
					@endphp
					{{-- fin datos falsos --}}


					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach
					</table>
					{{-- fin tabla --}}


				</div>
			</div>
			{{-- FIN INFORMACION PATRIMONIAL --}}






			{{-- #INFORMACION RESERVAS --}}
			<div class="wrapper  clearfix">


				{{-- titulo --}}
				<div class="xs-4 col  mb-16">
					<h3 class="mv-8">{{ __('Ocupación') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}
				
			

			</div>
			<div class="custom-table__wrapper  |  wrapper  clearfix">

			
				<div class="custom-table__col  |  xs-4 col  mb-80">
					

					{{-- datos falsos --}}
					@php
						$datos = [
							[ __('Número de reservas'), round($property->predicted->first()->numero_reservas)],
							[ __('Grado de ocupación'), formatThousands($property->predicted->first()->grado_ocupacion).' %'],
							[ __('Días reservados'), round(nodecimals($property->predicted->first()->dias_reservados))],
							[ __('Días no reservados'), round($property->predicted->first()->dias_no_reservados)],
							[ __('Días bloqueados'), round($property->predicted->first()->dias_bloqueados)],
							[ __('Tarifa media por día'), formatThousands($property->predicted->first()->tarifa_media_dia).' €'],
							[ __('Estimación ingresos anuales'), formatThousands($property->predicted->first()->estimacion_ingresos_anuales).' €'],
							[ __('Grado satisfacción clientes'), formatThousands($property->predicted->first()->satisfaccion_clientes).' %'],
							[ __('Número de invitados'), round($property->predicted->first()->numero_invitados)]
						]
					@endphp
					{{-- fin datos falsos --}}
					

					{{-- tabla --}}
					<table class="custom-table custom-table--total">
						<tr class="text-left">
							<th class="h4">{{ __('Concepto') }}</th>
							<th class="h4 text-right">{{ __('Valor') }}</th>
						</tr>
						@foreach ($datos as $dato)
							<tr>
								<td>{{ $dato[0] }}</td>
								<td class="text-right">{{ $dato[1] }}</td>
							</tr>
						@endforeach
					</table>
					{{-- fin tabla --}}


				</div>
			</div>
			{{-- FIN INFORMACION RESERVAS --}}
		@endif		

	</div>