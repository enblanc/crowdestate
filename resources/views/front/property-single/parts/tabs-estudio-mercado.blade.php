{{-- 
	==========================================================================
	#PROPERTY SINGLE - TABS DOCUMENTOS
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}	

	<div class="tabs-selector__item" id="tab-mercado">

		@if(!auth()->check())
			@include('front/property-single/assets/contenido-bloqueado') 
		@else
			{{-- Else: lo que sea --}}
			<div class="wrapper mt-56 mb-40">

				{{-- titulo --}}
				<div class="xs-4 col  mb-24">
					<h3 class="mv-8">{{ __('Estudio de mercado') }}</h3>
					<div class="separator--horizontal"></div>
				</div>
				{{-- fin titulo --}}


				{{-- descripcion --}}
				<div class="xs-4 col  pr-24  mb-64">
					{!! $property->getTranslationOrDefault()->rendimiento_mercado !!}
				</div>
				{{-- fin descripcion --}}

			</div>
		@endif
	</div>