{{-- 
	==========================================================================
	#PROPERTY SINGLE - SIMULADOR
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#CONTENT
	========================================================================== --}}

	<section>
		<div class="wrapper  pv-64">
			<div class="xs-4 m-6 col  text-center text-left-m">
	
				@if($property->state->id == 4 && auth()->check() && auth()->user()->hasInversion($property))
					<a class="button button--secondary" href="{{ route('panel.user.inversion.property', $property->id) }}">{{ __('Ver evolución del inmueble') }}</a>
				@else
					<button class="button button--secondary   [ js-modal ]" modal-ref="simulador">{{ __('Simular Inversión') }}</button>
				@endif



			</div>
			<div class="xs-4 m-6 col  text-center text-right-m  mt-40 mt-m-0">

				<h5 class="d-block d-inblock-m  mb-16 mb-m-8">{{ __('Compartir') }}:</h5>

				<a class="share__circle  |  pmv-0 ml-0 ml-m-16 ml-l-32" 
				   href="http://www.facebook.com/share.php?u={{ $property->url }}&title={{ $property->nombre }}" target="_blank">
						@include('front/svg/facebook', ['class' => ''])</a>

				<a class="share__circle  |  pmv-0 ml-16 ml-l-32"
				   href="http://twitter.com/intent/tweet?status={{ $property->nombre }}+{{ $property->url }}" target="_blank">
						@include('front/svg/twitter', ['class' => ''])</a>

				<a class="share__circle  |  pmv-0 ml-16 ml-l-32" 
				   href="http://www.linkedin.com/shareArticle?mini=true&url={{ $property->url }}&title={{ $property->nombre }}&source={{ $property->url }}" target="_blank">
						@include('front/svg/linkedin', ['class' => ''])</a>

			</div>
		</div>






{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup" id="simulador">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-8 l-6 col center  clearfix  bg-w  pt-m-24 ph-8 ph-m-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}




							{{-- #MODAL CONTENT --}}
							<div class="xs-3 m-12 col">
								<h4>{{ __('Simulador de inversiones') }}</h4>
							</div>

							<div class="xs-4 m-4 col  text-right-m">
								<p class="big-text  mv-0 mt-m-32 mb-m-16">{{ __('Si inviertes') }}:</p>
							</div>
							<div class="xs-4 m-8 col">
								<input class="modal__input  |  h3 c-2" type="number"  min="{{ $property->importe_minimo }}" 
									v-model="inversion" @change="limitMin()" @keyup.esc="limitMin()"> <span class="h3">€</span>
								<p class="small-text  pmv-0">{{ __('El importe mínimo de inversión es') }} @{{ limiteInversion }} €</p>
							</div>
							<div class="clearfix mb-24"></div>

							<div class="xs-4 m-4 col  text-right-m">
								<p class="big-text  mv-0 mv-m-16">{{ __('Ganas') }}:</p>
							</div>
							<div class="xs-4 m-8 col">
								<h3 class="c-m mv-0 mt-m-8">@{{ beneficioTotal }} €</h3>
								<p class="small-text  mt-0">
									{{ __('En total por el') }} @{{ rentabilidadAcumulada }}% {{ __('de Rentabilidad de Acumulada Neta') }}.
								</p>
							</div>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>

		<input type="hidden" id="rentabilidad_acumulada" value="{{ $property->rentabilidad_anual }}">
		<input type="hidden" id="minimo_invertir" value="{{ $property->importe_minimo }}">
		<input type="hidden" id="defecto_invertir" value="2000">



	</section>





