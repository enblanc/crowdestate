{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		@include('front/property-single/parts/header')
		@include('front/property-single/parts/simulator')

		@include('front/property-single/parts/tabs-header')
		@include('front/property-single/parts/tabs-resumen-proyecto')
		@include('front/property-single/parts/tabs-info-inmueble')
		@include('front/property-single/parts/tabs-info-financiera')
		@include('front/property-single/parts/tabs-documentos')
		@include('front/property-single/parts/tabs-estudio-mercado')
		@include('front/property-single/parts/tabs-close')

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		{{-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> --}}
		<script src="/assets/js/plugins/owl.carousel.min.js"></script>
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGAr8g4c7s7v54upfkWrBurnxvdtJJDzE"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/property.js') }}" async></script>
		<script>
		$(document).ready(function() {
			$(window).on("load", function() {
				var owlFull = $('.js-slide-full');

				owlFull.owlCarousel({
					items:1,
					// margin:10,
					// autoHeight:true,  
					animateOut: 'fadeOut',
					loop: true,
					nav: false,
					dots: true,
					autoplay: true,
					autoplayTimeout: 6000,
					autoplayHoverPause: false,
					autoplaySpeed: 1000,
					smartSpeed: 1000,
					navSpeed: 1000,
					dotSpeed: 1000,
					// touchDrag: false,

				});



			$('.js-slide-full--left').on("touchstart click", function(e) {
				e.preventDefault();
			    owlFull.trigger('prev.owl.carousel');
			})
			$('.js-slide-full--right').on("touchstart click", function(e) {
				e.preventDefault();
			    owlFull.trigger('next.owl.carousel');
			})
		});
	});
		</script>

	@endsection 
