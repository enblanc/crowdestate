{{-- #LOCALIZACION --}}
		<div class="wrapper  clearfix  mb-80">
			

			{{-- titulo --}}
			<div class="xs-4 col  mb-40">
				<h3 class="mv-8">{{ __('Localización') }}</h3>
				<div class="separator--horizontal"></div>
			</div>
			{{-- fin titulo --}}


			{{-- mapa --}}
			<div class="xs-4 col   mb-48">
				<div id="map{{ $map_number }}" class="property__map  |  xs-4" data-lat="{{ $property->lat }}" data-lng="{{ $property->lng }}"></div>
			</div>
			{{-- fin mapa --}}

			


			{{-- bloque texto 1 --}}
			<div class="xs-4 m-4 col  mb-32 mb-l-0">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->localizacion_first_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->localizacion_first_data }}</p>
				</div>
				
			</div>
			{{-- fin bloque texto 1 --}}


			{{-- bloque texto 2 --}}
			<div class="xs-4 m-4 col  mb-32 mb-l-0">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->localizacion_second_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->localizacion_second_data }}</p>
				</div>

			</div>
			{{-- fin bloque texto 2 --}}


			{{-- bloque texto 3 --}}
			<div class="xs-4 m-4 col">
				
				<span class="icon--circle  left"></span>
				<div class="text__icon--big  right">
					<h4 class="mt-0">{{ $property->getTranslationOrDefault()->localizacion_third_title }}</h4>
					<p  class="">{{ $property->getTranslationOrDefault()->localizacion_third_data }}</p>
				</div>

			</div>
			{{-- fin bloque texto 3 --}}


		</div>
		{{-- FIN LOCALIZACION --}}