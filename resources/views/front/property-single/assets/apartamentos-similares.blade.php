<div class="xs-4 col">
	<h4 class="mb-8">{{ __('Rendimiento de apartamentos similares') }}</h4>
	<p class="small-text mv-8  mt-8 mb-0">{{ __('Así son de rentables apartamentos similares a éste que están generando beneficios a sus inversores') }}</p>
</div>

<div class="clearfix"></div>



<div class="xs-4 m-6 l-12 col  mv-40">
	<div class="clearfix  bg-4  pb-24">

	@if($property->getTranslationOrDefault()->getMedia($property->getTranslationOrDefault()->mediaRelatedFirstCollection)->count() > 0)
		<div class="related__image overlay overlay--vertical  |  xs-4 col bg  pt-80 pb-16 mb-16" 
			style="background-image: url('{{ $property->getTranslationOrDefault()->getFirstMediaUrl($property->getTranslationOrDefault()->mediaRelatedFirstCollection) }}')">
			<h4 class="p-relative  c-w  mt-40 mb-0">{{ $property->getTranslationOrDefault()->similares_first_name }}</h4>	
		</div>
	@else 
		<div class="xs-4 col">
			<h4 class="mv-16 mb-0">{{ $property->getTranslationOrDefault()->similares_first_name }}</h4>	
		</div>
	@endif
		
		<div class="xs-2 col">
			<span class="pt-8 left">@include('front/svg/arrow-up')</span>
			<p class="text__icon right small-text  mv-8">{{ $property->getTranslationOrDefault()->similares_first_data_1 }}</p>
		</div>

		<div class="xs-2 col">
			<span class="pt-8 left">@include('front/svg/arrow-up')</span>
			<p class="text__icon right small-text  mv-8">{{ $property->getTranslationOrDefault()->similares_first_data_2 }}</p>
		</div>

		<div class="clearfix"></div>
	</div>
</div>




<div class="xs-4 m-6 l-12 col  mt-m-40 mt-l-0 mb-24">
	<div class="clearfix  bg-4  pb-24">

	@if($property->getTranslationOrDefault()->getMedia($property->getTranslationOrDefault()->mediaRelatedSecondCollection)->count() > 0)
		<div class="related__image overlay overlay--vertical  |  xs-4 col bg  pt-80 pb-16 mb-16" 
			style="background-image: url('{{ $property->getTranslationOrDefault()->getFirstMediaUrl($property->getTranslationOrDefault()->mediaRelatedSecondCollection) }}')">
			<h4 class="p-relative  c-w  mt-40 mb-0">{{ $property->getTranslationOrDefault()->similares_second_name }}</h4>	
		</div>
	@else 
		<div class="xs-4 col">
			<h4 class="mv-16 mb-0">{{ $property->getTranslationOrDefault()->similares_second_name }}</h4>	
		</div>
	@endif
		
		<div class="xs-2 col">
			<span class="pt-8 left">@include('front/svg/arrow-up')</span>
			<p class="text__icon right small-text  mv-8">{{ $property->getTranslationOrDefault()->similares_second_data_1 }}</p>
		</div>

		<div class="xs-2 col">
			<span class="pt-8 left">@include('front/svg/arrow-up')</span>
			<p class="text__icon right small-text  mv-8">{{ $property->getTranslationOrDefault()->similares_second_data_2 }}</p>
		</div>

		<div class="clearfix"></div>
	</div>
</div>