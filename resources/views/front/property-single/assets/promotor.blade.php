<div class="xs-4 col">
	<h4 class="mb-0">{{ __('Promotor del inmueble') }}</h4>
	{{-- <p class="small-text mv-8  mt-8 mb-0">Así son de rentables apartamentos similares a éste que están generando beneficios a sus inversores</p> --}}
</div>

<div class="clearfix"></div>



<div class="xs-4 col  mv-8">

	<h5 class="mb-0">{{ __('Nombre') }}:</h5>
	<p class="mt-0">Srita. Aina Palomo Tercero</p>

	<h5 class="mb-0">{{ __('Dirección') }}:</h5>
	<p class="mt-0">733 Raquel Springs Suite 331\n
         West Fernandoside, WY 76820</p>

	<h5 class="mb-0">{{ __('Email') }}:</h5>
	<a class="mt-0" href="mailto:ggriego@mendez.com">ggriego@mendez.com</a>

	<h5 class="mb-0">{{ __('Contacto') }}:</h5>
	<p class="mt-0">Ing. Roberto Herrera Tercero</p>
	
</div>


