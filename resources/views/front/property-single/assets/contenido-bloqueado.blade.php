<div class="wrapper">
	<div class="xs-4 col  text-center  mt-56">
		<p class="big-text">@include('front/svg/candado', ['class' => 'mr-8']) {{ __('Este contenido solo está disponible para usuarios registrados') }}</p>
		<p class="small-text c-2">{{ __('Regístrate gratis y podrás acceder a muchos más datos de cada inmueble antes de invertir') }}</p>

		<a class="d-block  mt-32 mb-0" href="{{ route('login') }}">{{ __('Iniciar sesión') }}</a> 
		<p class="small-text d-block  mv-0">o</p> 
		<a class="button  mt-8" href="{{ route('registro') }}">{{ __('Registrarme') }}</a>
	</div>
</div>