{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		<section class="overlay overlay--gradient  |  p-relative bg" style="background-image: url('{{ $property->getFirstMediaUrl($property->mediaCollection) }}');">
			<div class="wrapper  p-relative  pv-40">


				<div class="xs-4 m-6 col  all-w  pt-80">

					<h1 class="h2">{{ $property->nombre }}</h1>
					<p class="big-text">{{ $property->direccion }}</p>

				</div>
				<div class="xs-4 m-1 col false  from-l"></div>


				<div class="xs-4 m-6 l-5 col">
					<div class="xs-4 clearfix  bg-w  pv-24 ph-32">
						
						<div class="left">
							<p class="small-text  pmv-0">Financiado</p>
							<h4 class="c-m  pmv-0">121.400€ (67%)</h4>
						</div>
						<div class="right">
							<p class="small-text  pmv-0">Total</p>
							<h4 class="pmv-0">302.450€</h4>
						</div>
						<div class="clearfix"></div>

						<div class="bar  |  mv-8">
							<span class="bar__status  |  d-block"></span>
						</div>

						<p class="small-text  left  pmv-0">273 Inversores</p>
						<p class="small-text  right  pmv-0">Quedan 57 días</p>

						<div class="clearfix mb-32"></div>


						@php
							$datos = [
								['Valor de mercado:', '356.987 €'],
								['Valor de compra:', '234.500 €'],
								['Hipoteca:', '67.950 €'],
								['Total Operación:', '302.450 € ']
								]
						@endphp

						@foreach ($datos as $dato)
							<div class="d-table  xs-4">
								<div class="d-cell  pr-16">
									<p class="small-text nowrap  pmv-0">{{ $dato[0] }}</p>
								</div>
								<div class="d-cell  xs-4  v-middle"><span class="line"></span></div>
								<div class="d-cell  pl-16">
									<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
								</div>
							</div>
						@endforeach
						
						<div class="clearfix mb-24"></div>

						@php
							$datos2 = [
								['Rentabilidad Alquiler Bruta:', '12 %'],
								['Rentabilidad Alquiler Neta:', '8,5 %'],
								['Rentabilidad Operación Bruta:', '18 %'],
								['Retanbilidad Operación Neta:', '13,4 %'],
								['Tasa Interna de Rentabilidad:', '5,1 %']
								]
						@endphp

						@foreach ($datos2 as $dato)
							<div class="d-table  xs-4">
								<div class="d-cell  pr-16">
									<p class="small-text nowrap  pmv-0">{{ $dato[0] }}</p>
								</div>
								<div class="d-cell  xs-4  v-middle"><span class="line"></span></div>
								<div class="d-cell  pl-16">
									<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
								</div>
							</div>
						@endforeach

						<a href="" class="small-text  d-block  mt-24 mb-32">@include('front/svg/duda', ['class' => 'v-middle']) ¿Qué significan estas cifras?</a>

						<button class="button button--full">Invertir</button>
						


					</div>
					
				</div>
				

				<a href="#" class="gallery__button  |  pmv-0  [ js-modal ]" modal-ref="galeria">
					@include('front/svg/camara', ['class' => 'v-baseline']) Galería de fotos</a>

			</div>
		</section>





		<div class="modal modal--full-page" id="galeria">
			<div class="modal__container">
				
				<div class="modal__header  pb-48">
					<h2 class="uppercase mv-0 c-w">
						Hola
						<span class="modal__close  right cursor  [ js-modal-close ]">@include('front/svg/close', ['class' => 'modal__svg'])</span>
					</h2>
				</div>
				
				<div class="modal__content">
					<div class="slide slide--ambientes  owl-carousel [ js-slide-full ]">
						
						{{-- @php
							$images = [
									'https://unsplash.it/1920/1080?image=1040',
									'https://unsplash.it/1920/1080?image=1041',
									'https://unsplash.it/1920/1080?image=1042'
								];
						@endphp --}}

						@foreach($property->getMedia($property->mediaCollection) as $image)
							<div class="slide__item  p-relative">
								<img src="{{ $image->getUrl() }}" alt="">
							</div>
						@endforeach

					</div>
					
					@if($property->getMedia($property->mediaCollection)->count() > 1)
						<div class="slide__arrow slide__arrow--left    [ js-slide-full--left  ]">
							{{-- @include('front/svg/arrow-left') --}}
						</div>
						<div class="slide__arrow slide__arrow--right   [ js-slide-full--right ]">
							{{-- @include('front/svg/arrow-right') --}}
						</div>
					@endif
					
				</div>
			</div>
		</div>
	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="{{ asset('assets/js/plugins/owl.carousel.min.js') }}"></script>

	@endsection

	@section('custom_section_js')
		{{-- <script src="{{ mix('assets/js/main.js') }}"></script> --}}
	@endsection 
