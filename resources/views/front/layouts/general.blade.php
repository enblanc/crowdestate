{{--==========================================================================
	#HEADER 
	========================================================================== --}}

	@include('front/structure/header')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	<div class="container" id="app">
		@yield('content')	

		{{-- @include('front/structure/pop-registro')
		@include('front/structure/pop-login')
		@include('front/structure/pop-recuperar') --}}
		@include('front/structure/pop-recuperar-ok')
		@include('front/structure/pop-alta')
		@include('front/structure/pop-borrada')
	</div>






{{--==========================================================================
	#FOOTER
	========================================================================== --}}		
		
	@include('front/structure/footer')

	{{-- Tests --}}
	{{-- {{ get_url(Route::currentRouteName(), Route::current()->parameters(), 'en') }} --}}
	