{{--==========================================================================
	#HEADER 
	========================================================================== --}}

	@include('front/structure/header')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	<div class="container">
		@yield('content')	
		
		<div id="app">
			{{-- @include('front/structure/pop-registro')
			@include('front/structure/pop-login')
			@include('front/structure/pop-recuperar') --}}
			@include('front/structure/pop-alta')
			@include('front/structure/pop-borrada')
		</div>
	</div>






{{--==========================================================================
	#FOOTER
	========================================================================== --}}		
		
	@include('front/structure/footer')