{{--==========================================================================
	#HEADER 
	========================================================================== --}}

	@include('front/structure/header', ["loaded" => ' body--loaded'])






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	<div class="container" >
		
		<div id="dashboard">
			@include('front/structure/dash-proceso-invertir')
			@include('front/structure/dash-proceso-sacar')
			@include('front/structure/dash-cuerpo')
		</div>


			
		<div id="app">
			{{-- @include('front/structure/pop-registro')
			@include('front/structure/pop-login')
			@include('front/structure/pop-recuperar') --}}
			@include('front/structure/pop-alta')
			@include('front/structure/pop-borrada')
		</div>
	</div>






{{--==========================================================================
	#FOOTER
	========================================================================== --}}		
		
	@include('front/structure/footer')