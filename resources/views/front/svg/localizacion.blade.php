@php
	if ( isset($class) ) {
		$clase = $class;
	} else {
		$clase = '';
	}
@endphp
<svg class="{{ $clase }}" xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16">
    <path fill="#36D9B6" fill-rule="nonzero" d="M6 0C2.686 0 0 2.505 0 5.594 0 8.32 1.656 11.952 6 16c4.344-4.048 6-7.68 6-10.406C12 2.504 9.314 0 6 0zm0 7.03c-1.238 0-2.242-.935-2.242-2.089 0-1.152 1.004-2.088 2.242-2.088s2.24.936 2.24 2.088c0 1.154-1.002 2.088-2.24 2.088z"/>
</svg>
