<svg class="{{ $class }}" xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
    <path fill="#212121" fill-rule="nonzero" d="M0 0v18h9.586v-6.953H7.243V8.209h2.343V5.825a3.139 3.139 0 0 1 3.14-3.14h2.449v2.553h-1.753a.997.997 0 0 0-.997.998v1.973h2.706l-.374 2.838h-2.332V18H18V0H0z"/>
</svg>
