<svg class="{{ $class }}" xmlns="http://www.w3.org/2000/svg" width="71" height="88" viewBox="0 0 71 88">
    <path fill="" fill-rule="evenodd" d="M44 26V4l22 22H44zm6.65-26H8.875C3.98 0 0 3.951 0 8.8v70.4C0 84.049 3.98 88 8.875 88h53.25C67.02 88 71 84.049 71 79.2V20.178L50.65 0z"/>
</svg>
