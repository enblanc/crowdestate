{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		{{-- #QUIENES SOMOS --}}
		<section class="pb-32">
			<div class="wrapper  pv-56">
				<div class="xs-4 m-10 l-8 col center  text-center">
					<h2>{{ $extras->titulo_cabecera }}</h2>
					<p class="big-text">{{ $extras->texto_cabecera }}</p>
				</div>
			</div>



			<div class="wrapper  d-table  mb-80">
				<div class="xs-4 col  only-xs">
					<img class="mb-24" src="{{ $images->imagen_experiencia }}" alt="">
				</div>
				<div class="xs-4 m-6 col  none d-cell-m v-middle  text-center text-left-m  pr-l-48">
					<h3>{{ $extras->titulo_experiencia }}</h3>
					<p>{{ $extras->texto_experiencia }}</p>
				</div>
				<div class="xs-4 m-6 col  none not-xs-cell v-middle">
					<img src="{{ $images->imagen_experiencia }}" alt="">
				</div>
			</div>



			<div class="wrapper  d-table  mb-80">
				<div class="xs-4 m-6 col  none d-cell-m v-middle">
					<img src="{{ $images->imagen_economia }}" alt="">
				</div>
				<div class="xs-4 m-6 col  none d-cell-m v-middle  text-center text-left-m  pl-l-48">
					<h3>{{ $extras->titulo_economia }}</h3>
					<p>{{ $extras->texto_economia }}</p>
				</div>
			</div>
		</section>
		{{-- FIN QUIENES SOMOS --}}





		{{-- #NUESTRO EQUIPO --}}
		<section class="bg-4">
			<div class="wrapper">
				<div class="xs-4 col  text-center">
					<h3 class="mt-72 mb-56">{{ $extras->titulo_equipo }}</h3>
				</div>
			</div>
			<div class="wrapper  text-center text-left-m  pb-40">
				<div class="xs-4 m-6 l-4 col mb-48">
					<h4 class="mv-0">{{ $extras->nombre_persona1 }}</h4>
					<p class="mv-0">{{ $extras->posicion_persona1 }}</p>
					<p class="c-2  pmv-0">{{ $extras->info_persona1 }}</p>
				</div>
				<div class="xs-4 m-6 l-4 col mb-48">
					<h4 class="mv-0">{{ $extras->nombre_persona2 }}</h4>
					<p class="mv-0">{{ $extras->posicion_persona2 }}</p>
					<p class="c-2  pmv-0">{{ $extras->info_persona2 }}</p>
				</div>
				<div class="clearfix  only-m"></div>
				<div class="xs-4 m-6 l-4 col mb-48">
					<h4 class="mv-0">{{ $extras->nombre_persona3 }}</h4>
					<p class="mv-0">{{ $extras->posicion_persona3 }}</p>
					<p class="c-2  pmv-0">{{ $extras->info_persona3 }}</p>
				</div>
				<div class="clearfix  from-l"></div>
				<div class="xs-4 m-6 l-4 col mb-48">
					<h4 class="mv-0">{{ $extras->nombre_persona4 }}</h4>
					<p class="mv-0">{{ $extras->posicion_persona4 }}</p>
					<p class="c-2  pmv-0">{{ $extras->info_persona4 }}</p>
				</div>
				<div class="clearfix  only-m"></div>
				<div class="xs-4 m-6 l-4 col mb-48">
					<h4 class="mv-0">{{ $extras->nombre_persona5 }}</h4>
					<p class="mv-0">{{ $extras->posicion_persona5 }}</p>
					<p class="c-2  pmv-0">{{ $extras->info_persona5 }}</p>
				</div>
				<div class="xs-4 m-6 l-4 col mb-48">
					<h4 class="mv-0">{{ $extras->nombre_persona6 }}</h4>
					<p class="mv-0">{{ $extras->posicion_persona6 }}</p>
					<p class="c-2  pmv-0">{{ $extras->info_persona6 }}</p>
				</div>
			</div>
		</section>
		{{-- FIN NUESTRO EQUIPO --}}

		{{-- #QUIENES SOMOS --}}
		<section class="pb-32">
			<div class="wrapper  pv-56">
				<div class="xs-4 m-10 l-8 col center  text-center">
					<p class="big-text">{{ $extras->texto_fse }}</p>
				</div>
			</div>

		</section>
		{{-- FIN QUIENES SOMOS --}}



		
		@if(!auth()->user())

		{{-- #REGISTRATE --}}
		<section>
			<div class="wrapper  pv-80  mt-8 mb-56">
				<div class="xs-4 col  text-center">
					<p class="big-text  mb-56">{{ __('Regístrate en Brickstarter y comienza a ahorrar tu dinero') }}</p>
					<a class="button" href="{{ route('registro') }}">{{ __('Registrarme') }}</a>
				</div>
			</div>
		</section>
		{{-- FIN REGISTRATE --}}

		@endif


		@include('front/home/parts/contacto')

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		{{-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> --}}
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
