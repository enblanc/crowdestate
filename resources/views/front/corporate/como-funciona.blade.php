{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		{{-- #CABECERA --}}
		<section class="overlay overlay--gray  |  p-relative bg" style="background-image: url({{ $images->foto_cabecera }});">
			<div class="wrapper  pv-80">
				<div class="xs-4 col all-w  pv-m-64">
					<h1 class="not-xs">{{ $extras->titulo_cabecera }}</h1>
					<h1 class="h2  only-xs">{{ $extras->titulo_cabecera }}</h1>
				</div>
			</div>
		</section>
		{{-- FIN CABECERA --}}
		




		{{-- #POR QUÉ --}}
		<section>
			<div class="wrapper  pv-56">
				<div class="xs-4 m-10 l-8 col center  text-center">
					<h2>{{ $extras->titulo_principal }}</h2>
					<p class="big-text">{{ $extras->texto_principal }}</p>
				</div>
			</div>

			<div class="wrapper  d-table">
				<div class="xs-4 m-6 col  none d-cell v-middle  text-center text-left-m  pr-l-56">
					<h3>{{ $extras->titulo_maxima }}</h3>
					<p>{{ $extras->texto_maxima }}</p>
				</div>
				<div class="xs-4 m-6 col  none d-cell v-middle">
					<img src="{{ $images->imagen_maxima }}" alt="">
				</div>
			</div>

			<div class="p-relative  mv-80">
				<img class="center-img  p-absolute-m  max-height" src="{{ $images->imagen_inmuebles }}" alt="">
				<div class="wrapper">
					<div class="xs-4 m-6 col false"></div>
					<div class="xs-4 m-6 col  text-center text-left-m  pv-24 pl-m-32">
						<h3>{{ $extras->titulo_inmuebles }}</h3>
						{!! nl2br($extras->texto_inmuebles) !!}
					</div>
				</div>
			</div>
		</section>
		{{-- FIN POR QUÉ --}}



		{{-- #QUOTE 1 --}}
		<section class="bg-m">
			<div class="wrapper  pv-80">
				<div class="xs-4 l-10 col center  all-w  pv-16">
					<h6>{!! $extras->texto_gracias !!}</h6>
				</div>
			</div>
		</section>
		{{-- FIN QUOTE 1 --}}





		{{-- #PROCESO --}}
		<section>
			<div class="wrapper  pv-56">
				<div class="xs-4 m-10 l-8 col center  text-center">
					<h2>{{ $extras->titulo_funciona }}</h2>
					<p class="big-text">{!! nl2br($extras->texto_funciona) !!}</p>
				</div>
			</div>

			<div class="wrapper  d-table-m  mb-80 mb-m-40">
				<div class="xs-4 m-8 l-7 col  none d-cell-m v-middle  text-center text-left-m">
					<div class="step__index--big  |  d-inblock  h2 text-center  mt-32">1</div>
					<div class="step__text--big  |  right">
						<h3>{{ $extras->titulo_funciona_1 }}</h3>
						<p>{{ $extras->texto_funciona_1 }}</p>	
					</div>					
				</div>
				<div class="xs-4 m-4 l-5 col  none d-cell-m v-middle text-center">
					<img class="mt-24 mt-m-0" src="{{ asset('assets/img/icono-apartamento.svg') }}" alt="">
				</div>
			</div>

			<div class="wrapper  d-table-m  mb-80 mb-m-40">
				<div class="xs-4 m-4 l-5 col  none not-xs-cell v-middle text-center ">
					<img class="mt-0" src="{{ asset('assets/img/icono-llaveinglesa.svg') }}" alt="">
				</div>
				<div class="xs-4 m-8 l-7 col  none d-cell-m v-middle  text-center text-left-m">
					<div class="step__index--big  |  d-inblock  h2 text-center  mt-32">2</div>
					<div class="step__text--big  |  right">
						<h3>{{ $extras->titulo_funciona_2 }}</h3>
						<p>{{ $extras->texto_funciona_2 }}</p>	
					</div>					
				</div>
				<div class="xs-4 col  none only-xs text-center ">
					<img class="mt-24" src="{{ asset('assets/img/icono-llaveinglesa.svg') }}" alt="">
				</div>
			</div>

			<div class="wrapper  d-table-m  mb-80 mb-m-40">
				<div class="xs-4 m-8 l-7 col  none d-cell-m v-middle  text-center text-left-m">
					<div class="step__index--big  |  d-inblock  h2 text-center  mt-32">3</div>
					<div class="step__text--big  |  right">
						<h3>{{ $extras->titulo_funciona_3 }}</h3>
						<p>{{ $extras->texto_funciona_3 }}</p>	
					</div>					
				</div>
				<div class="xs-4 m-4 l-5 col  none d-cell-m v-middle text-center">
					<img class="mt-24 mt-m-0" src="{{ asset('assets/img/icono-explotacion.svg') }}" alt="">
				</div>
			</div>

			<div class="wrapper  d-table-m  mb-80 mb-m-40">
				<div class="xs-4 m-4 l-5 col  none not-xs-cell v-middle text-center ">
					<img class="mt-0" src="{{ asset('assets/img/icono-like.svg') }}" alt="">
				</div>
				<div class="xs-4 m-8 l-7 col  none d-cell-m v-middle  text-center text-left-m">
					<div class="step__index--big  |  d-inblock  h2 text-center  mt-32">4</div>
					<div class="step__text--big  |  right">
						<h3>{{ $extras->titulo_funciona_4 }}</h3>
						<p>{{ $extras->texto_funciona_4 }}</p>	
					</div>					
				</div>
				<div class="xs-4 col  none only-xs text-center ">
					<img class="mt-24" src="{{ asset('assets/img/icono-like.svg') }}" alt="">
				</div>
			</div>

			<div class="wrapper  d-table-m  mb-80">
				<div class="xs-4 m-8 l-7 col  none d-cell-m v-middle  text-center text-left-m">
					<div class="step__index--big  |  d-inblock  h2 text-center  mt-32">5</div>
					<div class="step__text--big  |  right">
						<h3>{{ $extras->titulo_funciona_5 }}</h3>
						<p>{{ $extras->texto_funciona_5 }}</p>	
					</div>					
				</div>
				<div class="xs-4 m-4 l-5 col  none d-cell-m v-middle text-center">
					<img class="mt-24 mt-m-0" src="{{ asset('assets/img/icono-bandera.svg') }}" alt="">
				</div>
			</div>

		</section>
		{{-- FIN PROCESO --}}




		{{-- #QUOTE 2 --}}
		<section class="bg-m">
			<div class="wrapper  pv-80">
				<div class="xs-4 l-10 col center  all-w  pv-16">
					<h6>{{ $extras->texto_invirtiendo }}</h6>
				</div>
			</div>
		</section>
		{{-- FIN QUOTE 2 --}}



		@if(!auth()->user())

		{{-- #REGISTRATE --}}
		<section>
			<div class="wrapper  pv-80  mt-8 mb-56">
				<div class="xs-4 col  text-center">
					<p class="big-text  mb-56">{{ __('Regístrate en Brickstarter y comienza a ahorrar tu dinero') }}</p>
					<a class="button" href="{{ route('registro') }}">{{ __('Registrarme') }}</a>
				</div>
			</div>
		</section>
		{{-- FIN REGISTRATE --}}

		@endif

		@include('front/home/parts/contacto')

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		{{-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> --}}
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
