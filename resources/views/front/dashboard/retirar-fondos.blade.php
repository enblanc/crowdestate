{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<div class="xs-4 l-11 col  mt-24  mb-24">

			@php
				$balance = $moneyToOut;
			@endphp
			
			@if($moneyToOut == 0) 
				<p>{{ __('Necesitas') }} <a href="{{ route('panel.user.money.show') }}">{{ __('agregar fondos') }}</a> {{ __('para poder retirarlos.') }}</p>
			@elseif(auth()->user()->bankAccounts->count() == 0)
				<p>{{ __('Necesitas agregar alguna cuenta bancaria para poder realizar la operación.') }} <a href="{{ route('panel.user.bankaccounts.show') }}">{{ __('Agregar cuenta bancaria') }}</a>
			@else
				<p>{{ __('Transfiere dinero de tu cuenta a cualquiera de') }} <a href="{{ route('panel.user.bankaccounts.show') }}">{{ __('tus Cuentas Bancarias') }}</a>. {{ __('Actualmente puedes transferir hasta un máximo de') }} {{ formatThousandsNotZero($balance) }} €. {{ __('Para transferir más fondos ponte en') }} <a href="{{ route('contact') }}">{{ __('contacto con nosotros') }}</a></p>
			@endif
				
			@if(!auth()->user()->hasValidBankAccounts())
				<p>{{ __('Lo sentimos, pero tus') }} <a href="{{ route('panel.user.bankaccounts.show') }}">{{ __('Cuentas Bancarias') }}</a> {{ __('aún están siendo validadas.') }}
			@endif

		</div>

		<div class="xs-4 col">
	   		@if (count($errors) > 0)
	            @foreach ($errors->all() as $error)
	                <p class="c-error  text-center">{{ $error }}</p>
	            @endforeach
			@endif

			@if(session()->has('moneyout_success'))
				<p class="c-success text-center">{{ __('La transacción se ha realizado correctamente. En unos días recibirás el dinero en tu banco.') }}</p>
			@endif

			@if(session()->has('moneyout_error'))
				<p class="c-error text-center">{{ __('Hubo un error y la transacción no se ha completado. Prueba de nuevo, o usa otra cuenta bancaria.') }}</p>
			@endif
		</div>

		@if($balance != 0 && auth()->user()->bankAccounts->count() > 0 && auth()->user()->hasValidBankAccounts())
		<form class="validate  mt-40   [ js-loading ]" action="{{ route('panel.user.moneyof.add') }}" method="POST"
			  data-parsley-validate=""  data-parsley-trigger="blur">
			
			{{ csrf_field() }}

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Cuenta bancaria') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<select class="custom-input  custom-input--select" name="iban" id="iban" required>
						<option value=""  selected>{{ __('Selecciona una cuenta bancaria') }}</option>
						@foreach($bankAccounts as $account)
							<option value="{{ $account->id }}">{{ $account->iban }}</option>
						@endforeach
						{{-- <option value="1">option 1</option>
						<option value="2">option 2</option> --}}
					</select>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Concepto') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="concepto" placeholder="{{ __('Concepto') }}" value="{{ old('concepto') }}" required
						   data-parsley-maxlength="140">
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="mt-m-48">{{ __('Cantidad') }}</h5>
				</div>
				<div class="xs-4 m-6 col">
					<input class="modal__input  |  h3 c-2" name="cantidad" type="number" step="any"  min="0" max="{{ $balance }}" required><span class="h3">€</span>
				</div>
			</div>

			

			<div class="clearfix">
				<div class="xs-4 m-3 col false"></div>
				<div class="xs-4 m-9 col">
					<input class="button button--small button--mvl-full  |  mt-40" type="submit" value="{{ __('Solicitar Transferencia') }}">
				</div>
			</div>

		</form>
		@endif
	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
