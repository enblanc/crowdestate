{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		
		{{-- #AÑADIR CUENTA --}}

		<div class="xs-4 col mt-40">
			<h4 class="c-2">{{ __('Movimientos') }}</h4>
		</div>

		<div class="xs-4 col   table__scroll">
			

			@if(count($historical) > 0)
				<table class="custom-table   [ js-paginate ]">
					<tr class="text-left">
						<th class="h5">{{ __('Inversión') }}</th>
						<th class="h5">{{ __('Tipo') }}</th>
						<th class="h5 nowrap">{{ __('Cantidad') }}</th>
						<th class="h5 nowrap">{{ __('Precio') }}</th>
						<th class="h5 nowrap">{{ __('Descuento/Prima') }}</th>
						<th class="h5">{{ __('Fecha') }}</th>
					</tr>
					@foreach($historical as $order)
						@if ($order->user_id == auth()->id())
							<tr>
								<td>{!! $order->getPropertyName() !!}</td>
								<td class="nowrap">
									@if ($order->type == 1)
										{{ __('He vendido') }}
									@else
										{{ __('He comprado') }}
									@endif
								</td>
								<td class="nowrap">{{ formatThousandsNotZero($order->quantity) }}€</td>
								<td class="nowrap">
									{{ formatThousandsNotZero($order->priceMoney) }}€
								</td>
								<td class="nowrap">{{ formatThousands($order->price) }}%</td>
								<td>{{ $order->updatedDateFormatted }}</td>
							</tr>
						@else

							@if ($order->type == 1)
								<tr>
									<td>{!! $order->getPropertyName() !!}</td>
									<td class="nowrap">{{ __('He comprado') }}</td>
									<td class="nowrap">{{ formatThousandsNotZero($order->quantity) }}€</td>
									<td class="nowrap">{{ formatThousandsNotZero($order->priceMoney) }}€</td>
									<td class="nowrap">{{ formatThousands($order->price) }}%</td>
									<td>{{ $order->updatedDateFormatted }}</td>
								</tr>
							@else
								<tr>
									<td>{!! $order->getPropertyName() !!}</td>
									<td class="nowrap">{{ __('He vendido') }}</td>
									<td class="nowrap">{{ formatThousandsNotZero($order->quantity) }}€</td>
									<td class="nowrap">{{ formatThousandsNotZero($order->priceMoney) }}€</td>
									<td class="nowrap">{{ formatThousands($order->price) }}%</td>
									<td>{{ $order->updatedDateFormatted }}</td>
								</tr>
							@endif
							
						@endif
					@endforeach
				</table>
			@else
				<p>{{ __('No tienes históricos') }}.</p>
			@endif

			{{-- #COMENTARIO:

				Paginamos en 15 filas
				Eric: Como queráis
				Pues vale
			--}}
		</div>



	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>
		
		<script>

			var $toPaginate, //$('js-paginate'),
				perPage,
				actualPage,
				totalItems,
				pages;

			$(document).ready(function() {

				$toPaginate = $('.custom-table'); //$('js-paginate');
				perPage     = 10;
				actualPage  = 1;
				totalItems  = $toPaginate.find('tr').length - 1;
				pages       = Math.ceil( totalItems / perPage );

				// PAGINACIÓN
				var paginador = '<div class="xs-4 col  text-center  mt-16 mb-56">' + 
					'<ul class="pagination pagination--js">' +
						'<li class="pagination__prev"><a href="" data-rel="prev">«</a></li>';

				for (var i = 0; i < pages; i++) { 
					var active = "";
					if ( i == 0) { active = "active"; }
					paginador += '<li class="' + active + '"><a href="" data-rel="' +(i + 1) + '">' + (i + 1) + '</a></li>'; 
				}			
				paginador += '<li class="pagination__next"><a href="" data-rel="next">»</a></li>' +
					'</ul></div>';


				$toPaginate.after(paginador);
				desabLinks(actualPage);


				$toPaginate.find('tr').each(function () {
					$(this).hide();
				});

				$toPaginate.find('tr').eq(0).show();
				for (var i = 1; i < (perPage + 1); i++) {
					$toPaginate.find('tr').eq(i).show();
				}


				$('body').on('click', '.pagination--js li', function(e) {
					e.preventDefault();

					if ( $(this).hasClass('active') ) {
						return false;
					}

					var dataRel = $(this).find('a').attr('data-rel');

					$('.pagination--js li').removeClass('active');
					
					if (dataRel == "prev") {
						actualPage = actualPage - 1;
						$('.pagination--js li').eq(actualPage).addClass('active');
					} else if (dataRel == "next") {
						actualPage = actualPage + 1;
						$('.pagination--js li').eq(actualPage).addClass('active');
					} else {
						$(this).addClass('active');
						actualPage = Number(dataRel);
					}

					paginate( actualPage );

				})




				function paginate(page) {
					var minItem = page * perPage - perPage + 1;
					var maxItem = (page + 1) * perPage - perPage + 1;

					if (maxItem > totalItems + 1) {
						maxItem = totalItems + 1;
					}
					
					$('.custom-table').find('tr').each(function () {
						$(this).hide();
					});
					$('.custom-table').find('tr').eq(0).show();

					for (var i = minItem; i < maxItem; i++) {
						$('.custom-table').find('tr').eq(i).show();
					}

					$("html, body").animate({ scrollTop: 0 }, 0);

					desabLinks(page);
				}




				function desabLinks (actual) {
					if ( actual == 1 ) {
						$('.pagination__prev').addClass('active');
					} else {
						$('.pagination__prev').removeClass('active');
					}

					if ( actual == pages ) {
						$('.pagination__next').addClass('active');
					} else {
						$('.pagination__next').removeClass('active');
					}
				}



			});
		</script>

	@endsection 
