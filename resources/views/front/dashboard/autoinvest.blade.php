@extends('front/layouts/dashboard')

@section('content')
    <div id="mi-mercado">
        {{--
        @if (auth()->user()->balance)
		    <div class="xs-4 col">
                <p class="c-error text-center">{{ __('Necesitas acreditar tu cuenta antes de poder invertir') }}</p>
            </div>
        @endif
        --}}

        <div class="xs-4 col">
            <h4 class="c-2  mb-24">{{ __('Auto Invest') }}</h4>
        </div>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="mt-40 validate [ js-loading ]" action="{{ route('panel.user.marketplace.autoinvest.update') }}"
            method="POST" enctype="multipart/form-data" data-parsley-validate="" data-parsley-trigger="blur">
            {{ csrf_field() }}

            <div class="clearfix mb-40">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8 pt-l-0 mt-l-0 pb-16 pb-l-0">{{ __('Total a Financiar|autoinvest') }}</h5>
                  
                </div>
                <div class="xs-4 m-9 col">
                    <input type="text" id="slider_portfolio_size"
                        class="slider slider-price {{ $errors->has('portfolio_size') ? 'has-error' : null }}"
                        name="portfolio_size">
                    <span class="info-span">
                        <svg height="19" class="mr-1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24">
                            <path fill="#8b57ec"
                                d="M12,2A10,10,0,1,0,22,12,10.01114,10.01114,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.00917,8.00917,0,0,1,12,20Zm0-8.5a1,1,0,0,0-1,1v3a1,1,0,0,0,2,0v-3A1,1,0,0,0,12,11.5Zm0-4a1.25,1.25,0,1,0,1.25,1.25A1.25,1.25,0,0,0,12,7.5Z" />
                        </svg>
                        <small>
                            {{ __('La cantidad total que quiero invertir') }}
                        </small>
                    </span>
                    <hr style="opacity: .2">
                </div>
            </div>

            <div class="clearfix mb-40 mt-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8 pt-l-0 mt-l-0 pb-16 pb-l-0">{{ __('Inversión Min/Max') }}</h5>
                </div>
                <div class="xs-4 m-9 col">
                    <input type="text" id="slider_prices"
                        class="slider slider-price {{ $errors->has('prices') ? 'has-error' : null }}" name="prices">
                    <span class="info-span">
                        <svg height="19" class="mr-1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24">
                            <path fill="#8b57ec"
                                d="M12,2A10,10,0,1,0,22,12,10.01114,10.01114,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.00917,8.00917,0,0,1,12,20Zm0-8.5a1,1,0,0,0-1,1v3a1,1,0,0,0,2,0v-3A1,1,0,0,0,12,11.5Zm0-4a1.25,1.25,0,1,0,1.25,1.25A1.25,1.25,0,0,0,12,7.5Z" />
                        </svg>
                        <small>
                            {{ __('La cantidad mínima y máxima por cada inversión') }}
                        </small>
                    </span>
                    <hr style="opacity: .2">
                </div>
            </div>

            {{-- City --}}
            <div class="clearfix mb-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8">{{ __('Ciudad') }}</h5>
                </div>
                <div class="xs-4 m-9 col">
                    @php
                        $value = old('location', $autoinvest->location);
                    @endphp
                    <select class="custom-input custom-input--select" name="location" id="location" required>
                        <option value="all" @if ('all' == $value) selected @endif>{{ __('Todos|autoinvest') }}</option>
                        @foreach ($locations as $location)
                            <option
                                value="{{ $location }}"
                                @if ($location == $value) selected @endif
                            >
                                {{ ucfirst($location) }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            {{-- Periodo --}}
            <div class="clearfix mb-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8">{{ __('Período') }}</h5>
                </div>
                <div class="xs-4 m-9 col">
                    <input class="custom-input {{ $errors->has('period') ? 'has-error' : null }}" id="period" type="text"
                        name="period" placeholder="DD/MM/YYYY" required value="{{ old('period',  \Carbon\Carbon::parse($autoinvest->period)->format('d/m/Y')) }}">
                    <span class="info-span">
                        <svg height="19" class="mr-1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24">
                            <path fill="#8b57ec"
                                d="M12,2A10,10,0,1,0,22,12,10.01114,10.01114,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.00917,8.00917,0,0,1,12,20Zm0-8.5a1,1,0,0,0-1,1v3a1,1,0,0,0,2,0v-3A1,1,0,0,0,12,11.5Zm0-4a1.25,1.25,0,1,0,1.25,1.25A1.25,1.25,0,0,0,12,7.5Z" />
                        </svg>
                        <small>
                            {{ __('El tiempo por el cual estará activa la orden') }}
                        </small>
                    </span>
                </div>
            </div>

            {{-- @includeIf('front.dashboard._countries') --}}

            {{-- Tipo del inmueble --}}
            <div class="clearfix mb-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8">{{ __('Tipo del inmueble') }}</h5>
                </div>
                <div class="xs-4 m-9 col">
                    @php
                        $value = old('property_type_id', $autoinvest->property_type_id);
                    @endphp
                    <select
                        class="custom-input  custom-input--select {{ $errors->has('property_type_id') ? 'has-error' : null }}"
                        name="property_type_id" id="property_type_id">
                        <option value="-1"  {{ $value == '-1' || $value == null ? 'selected' : '' }}>
                            {{ __('Todos|autoinvest') }}
                        </option>
                        @foreach ($types as $type)
                            <option value="{{ $type->id }}"
                                {{ $value == $type->id ? 'selected' : '' }}>
                                {{ $type->nombre }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            {{-- TIR --}}
            <div class="clearfix mb-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8">{{ __('TIR|autoinvest') }}</h5>
                </div>
                <div class="xs-4 m-9 col">
                    <input class="custom-input {{ $errors->has('tir') ? 'has-error' : null }}" id="tir" type="number"
                        name="tir" max="100" min="5" step="0.1" required value="{{ old('tir', $autoinvest->tir) }}">
                    <span class="info-span">
                        <svg height="19" class="mr-1" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg"
                            viewBox="0 0 24 24">
                            <path fill="#8b57ec"
                                d="M12,2A10,10,0,1,0,22,12,10.01114,10.01114,0,0,0,12,2Zm0,18a8,8,0,1,1,8-8A8.00917,8.00917,0,0,1,12,20Zm0-8.5a1,1,0,0,0-1,1v3a1,1,0,0,0,2,0v-3A1,1,0,0,0,12,11.5Zm0-4a1.25,1.25,0,1,0,1.25,1.25A1.25,1.25,0,0,0,12,7.5Z" />
                        </svg>
                        <small> % {{ __('Rentabilidad que ofrece la inversión|autoinvest') }} </small>
                    </span>
                </div>
            </div>

            {{-- Developers --}}
            <div class="clearfix mb-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8">
                        {{ __('Promotor del inmueble|autoinvest') }}
                    </h5>
                </div>
                <div class="xs-4 m-9 col">
                    <select
                        class="custom-input  custom-input--select {{ $errors->has('developer_id') ? 'has-error' : null }}"
                        name="developer_id" id="developer_id">
                        @php
                            $value = old('developer_id', $autoinvest->developer_id);
                        @endphp
                        <option
                            value="-1"
                            {{ $value == '-1' || $value == null ? 'selected' : '' }}>
                                {{ __('Todos|autoinvest') }}
                        </option>
                        @foreach ($developers as $developer)
                            <option value="{{ $developer->id }}"
                                {{ $value == $developer->id ? 'selected' : '' }}>
                                {{ $developer->nombre }}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="clearfix mb-16">
                <div class="xs-4 m-3 col  text-right-m">
                    <h5 class="pt-m-8">{{ __('Estado') }}</h5>
                </div>
                <div class="xs-4 m-9 col pt-16">
                    <input type="checkbox" id="status" name="status" class="custom-checkbox__input" {{ ($autoinvest->status) ? 'checked' : '' }}>
                    <label for="status" class="pt-4 left">
                        <span class="custom-checkbox__new  |  v-top  mt-8"></span>
                    </label>
                    {{--
                    <div class="v-popover left mt-4">
                        <div class="trigger" style="display: inline-block;">
                            <small><span class="c-pointer">{{ __('Activo') }}</span></small>
                        </div>
                    </div>
                    --}}
                </div>
            </div>

            <div class="clearfix">
                <div class="xs-4 m-3 col false"></div>
                <div class="xs-4 m-9 col">
                    <input class="button button--small button--mvl-full  |  mt-40" type="submit"
                        value="{{ isset($autoinvest) ?  __('Guardar cambios') : __('Crear') }}">
                </div>
            </div>
        </form>
    </div>
@endsection

@section('header_css')
    <link rel="stylesheet" href="{{ asset('assets/js/plugins/rSlider.min.css') }}">
    <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    <style>
        .info-span {
            opacity: .9;
            display: flex;
            align-items: center;
            line-height: 1;
        }
        .info-span svg {
            margin-right: 5px;
        }
    </style>
@endsection

@section('custom_plugin_js')
    <script src="/assets/js/plugins/parsleyjs.min.js"></script>
    <script src="/assets/js/plugins/rSlider.min.js"></script>
@endsection

@section('custom_section_js')
    <script src="{{ mix('assets/js/main.js') }}" async></script>
    <script>
        @php
            $max_value = old('portfolio_size', $autoinvest->portfolio_size);
        @endphp
        const min_value = 50;
        const max_value = {{ $max_value ? $max_value : auth()->user()->balance }};
        const min_investment = {{ $autoinvest->min_investment ? $autoinvest->min_investment : 0 }};
        const max_investment = {{ $autoinvest->max_investment ? $autoinvest->max_investment : auth()->user()->balance }};

        const min_value_portfolio = 50;
        const max_value_portfolio = {{ auth()->user()->balance }};  //Valor total

        (function() {
            'use strict';
            window.onload = function() {
                var initial = true;
                var sliderRange = initial_slider(min_value, max_value, [
                    {{ old('prices', 'min_investment, max_investment') }}
                ], '#slider_prices');

                console.log({'min_value': min_value_portfolio, max_value_portfolio});
                var sliderMax = initial_slider(min_value_portfolio, max_value_portfolio, [{{ old('portfolio_size', 'max_value') }}],
                    '#slider_portfolio_size', false,
                    function(vals) {
                        if (!initial) {
                            let sliderValues = sliderRange.getValue().split(",");
                            let max = parseInt(vals);
                            sliderRange.destroy();
                            sliderRange = initial_slider(min_value, max, [
                                max > parseInt(sliderValues[0]) ? parseInt(sliderValues[0]) : 50,
                                max > parseInt(sliderValues[1]) ? parseInt(sliderValues[1]) : max,
                            ],
                                '#slider_prices');
                        } else {
                            initial = false;
                        }
                    }
                );

                document.getElementById('slider_prices').style.opacity = 1
                document.getElementById('slider_portfolio_size').style.opacity = 1
            };

            function initial_slider(min, max, set, target, range = true, onChange = null) {
                return new rSlider({
                    target: target,
                    values: {
                        min,
                        max
                    },
                    step: min_value,
                    range,
                    set,
                    scale: true,
                    labels: false,
                    onChange
                });
            }
        })();
    </script>
@endsection
