{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<div class="xs-4 col  mt-24">
	   		@if (count($errors) > 0)
	            @foreach ($errors->all() as $error)
	                <p class="c-error  text-center">{{ $error }}</p>
	            @endforeach
			@endif

			@if(session()->has('error_lemon'))
				<p class="c-error">{{ session()->get('error_lemon') }}</p>
			@endif

			@if(session()->has('user_accrediting'))
				<p class="c-success text-center">{{ __('¡Datos agregados con éxito! Estamos comprobando los documentos.') }}</p>
			@endif


			@if(session()->has('bank_deleted'))
				<p class="c-success text-center">{{ __('La cuenta bancaria se ha eliminado con éxito.') }}</p>
			@endif

			@if(session()->has('bank_deleted_error'))
				<p class="c-error text-center">{{ __('La cuenta bancaria no se ha podido eliminar. Pónganse en contacto con nosotros. Gracias.') }}</p>
			@endif

		</div>

		
		{{-- #AÑADIR CUENTA --}}
		<form class="validate  mt-40   [ js-loading ]" action="{!! route('panel.user.bankaccounts.add') !!}" method="POST" enctype="multipart/form-data" 
			  data-parsley-validate="" data-parsley-trigger="blur"  id="form-cuenta-bancaria">

			{{ csrf_field() }}
			  
			<div class="xs-4 col">
				<h4 class="c-2">{{ __('Añadir cuenta bancaria') }}</h4>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('IBAN') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="iban" placeholder="{{ __('Código IBAN') }}" required
						   data-parsley-maxlength="32" data-parsley-type="alphanum" value="{{ old('iban') }}" v-model="bank_iban" @blur="checkCountry">
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Titular') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="titular" placeholder="{{ __('Titular de la cuenta') }}" value="{{ old('titular') }}" required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('País') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="pais" placeholder="{{ __('País de la cuenta') }}" value="{{ old('pais') }}" v-model="bank_country" required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Extracto bancario') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<label class="custom-file__label  |  mb-16" title="" data-text="{{ __('Seleccionar archivo') }}" data-file="{{ __('Sube una imagen') }}">
						<input class="custom-file__input" type="file" name="extracto" required onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))" />
					</label>
					<p class="small-text mt-0">{{ __('Necesitamos un .jpg o un .png de un extracto bancario con esta cuenta para saber que es tuya y poder validarla. Máximo 3mb.') }}</p>

					{{-- 
						Los estados serían:

						No subido (No hay archivo)
						Pendiente de guardar (Hay archivo pero los cambios no están guardados)
						Pendiente de aprobación 
						Aprobado
					--}}
					<h5 class="d-inline">{{ __('Estado') }}:</h5> <p class="d-inline  c-2">{{ __('No subido') }}</p>
				</div>
			</div>

			

			<div class="clearfix">
				<div class="xs-4 m-3 col false"></div>
				<div class="xs-4 m-9 col">
					<input class="button button--small  |  mt-40" type="submit" value="{{ __('Añadir cuenta bancaria') }}">
				</div>
			</div>

		</form>
		{{-- FIN AÑADIR CUENTA --}}


		
		{{-- #CUENTTAS  --}}
		<div class="xs-4 col  mt-56">
			<h4 class="c-2">{{ __('Tus cuentas bancarias') }}</h4>
			
			@if ($bankAccounts->count() > 0)

				@foreach($bankAccounts as $account)

					@if($account->document)

						<form id="remove_bank_{{ $account->id }}" action="{{ route('panel.user.bankaccounts.remove') }}" method="POST">

							{{ csrf_field() }}
							<input type="hidden" name="cuenta_id" value="{{ $account->id }}">

							<div class="bg-4  clearfix  pt-24 ph-24 pb-16  mv-32">
							
								<a class="right  mv-0 mr-8" href="javascript:{}" onclick="document.getElementById('remove_bank_{{ $account->id }}').submit();">{{ __('Eliminar cuenta') }}</a>

								<div class="xs-4 m-3 col  text-right-m">
									<h5 class="mv-4">{{ __('Titular') }}:</h5>
								</div>
								<div class="xs-4 m-6 col">
									<p class="mv-0">{{ $account->titular }}</p>
								</div>
								<div class="clearfix mb-8"></div>


								<div class="xs-4 m-3 col  text-right-m">
									<h5 class="mv-4">{{ __('IBAN') }}:</h5>
								</div>
								<div class="xs-4 m-9 col">
									<p class="mv-0">{{ $account->iban }}</p>
								</div>
								<div class="clearfix mb-8"></div>


								<div class="xs-4 m-3 col  text-right-m">
									<h5 class="mv-4">{{ __('País') }}:</h5>
								</div>
								<div class="xs-4 m-9 col">
									<p class="mv-0">{{ $account->pais }}</p>
								</div>
								<div class="clearfix mb-8"></div>


								<div class="xs-4 m-3 col  text-right-m">
									<h5 class="mv-4">{{ __('Extracto bancario') }}:</h5>
								</div>
								<div class="xs-4 m-9 col">
									@if($account->document->getIban())
									<a class="d-block  mv-0" href="{{ $account->document->getIban()->getUrl() }}" download>{{ __('Descargar') }}</a>
									@endif
								</div>
								<div class="clearfix mb-8"></div>


								<div class="xs-4 m-3 col  text-right-m">
									<h5 class="mv-4">{{ __('Estado') }}:</h5>
								</div>
								<div class="xs-4 m-9 col">
									<p class="mv-0">{{ $account->document->statusText }}</p>
								</div>
								<div class="clearfix mb-8"></div>


							</div>
						</form>
					@endif

				@endforeach
				
			@else
				{{-- El siguiente P te sale solo cuando no hay ninguna cuenta --}}
				<p>{{ __('No has añadido ninguna cuenta bancaria todavía') }}</p>
			@endif

			
		</div>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>
		{{-- <script src="{{ mix('assets/js/cuenta-bancaria.js') }}"></script> --}}
	@endsection 
