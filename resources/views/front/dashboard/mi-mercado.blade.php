{{--
	==========================================================================
	#PROPERTY SINGLE
	==========================================================================
	*
	* DESCRIPCIÓN
	*
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT
	========================================================================== --}}

	@section('content')
	<div class="" id="mi-mercado">

		<div class="xs-4 col mt-40">

			<h4 class="c-2  mb-24">{{ __('Añade una nueva orden de mercado') }}</h4>
			
			<form class="mt-40 validate [ js-loading ]" id="new-order" :action="formAction" :method="formMethod" enctype="multipart/form-data" 
			  data-parsley-validate="" data-parsley-trigger="blur">

			  	@if (session()->has('order_error'))
				  <p class="c-w bg-error text-center">{{ session()->get('order_error') }}</p>
		  		@endif
				  
			  	@if(session()->has('order_success'))
					<p class="alert c-success text-center">{{ session()->get('order_success') }}.</p>
				@endif

				{{ csrf_field() }}

				<template v-if="form.id">

					<p class="bg-t">{{ __('Estas editando tu orden con id') }}: <strong>@{{ form.hashid }}</strong>
						<a class="c-s pl-16" href="#" v-on:click="cancelEdit">{{ __('Cancelar edición') }}</a>
					</p>

					<input name="_method" type="hidden" value="PUT">
					<input type="hidden" name="id" v-model="form.id">

				</template>

				<div class="clearfix mb-16">
					<div class="xs-4 m-3 col  text-right-m">
						<h5 class="pt-m-8">{{ __('Tipo') }}</h5>
					</div>
					<div class="xs-4 m-9 col">
						<select class="custom-input  custom-input--select" name="type" id="type" v-model="form.type" v-on:change="resetForm" required :disabled="blockEdit">

							<option value="">{{ __('Elige un tipo') }}</option>

							<option value="1">{{ __('Vender') }}</option>
							<option value="2">{{ __('Comprar') }}</option>

						</select>
					</div>
				</div>

				<template v-if="form.type == 1">
					<input type="hidden" name="quantity" v-model="form.quantity">
					<input type="hidden" name="price" v-model="form.pricePercent">
					<input type="hidden" name="status" v-model="form.status">

					<div class="clearfix mb-16">
						<div class="xs-4 m-3 col  text-right-m">
							<h5 class="pt-m-8">{{ __('Inversión') }}</h5>
						</div>
						<div class="xs-4 m-9 col">
							<select class="custom-input  custom-input--select" name="inversion" id="inversion" v-on:change="getInversion" v-model="form.property" required :disabled="blockEdit">

								<option value="">{{ __('Elige una inversión') }}</option>

								@foreach($inversions as $inversion)
									@if ($inversion->pivot->total != 0)
										<option value="{{ $inversion->pivot->id }}">{{ $inversion->nombre }} ({{ __('Invertido') }} {{ formatThousandsNotZero($inversion->pivot->total) }}€)</option>
									@endif
								@endforeach

							</select>
						</div>
					</div>

					<div class="bg-info" v-if="inversionWithoutMoney">
						<div class="">
							<svg class="block v-middle" xmlns="http://www.w3.org/2000/svg" fill="#fff" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
							{{ __('Inversión ya en venta. Revisa tus órdenes') }}.
						</div>
					</div>

					<template  v-if="inversionTotal">
						<div class="bg-info" v-if="inversionWithOrder">
							<div class="">
								<svg class="block v-middle" xmlns="http://www.w3.org/2000/svg" fill="#fff" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
								{{ __('Ya existe una o más ordenes de venta para esta inversión') }}.
								{{ __('Venta máxima:') }} @{{ inversionTotal | currency('€', 2, { symbolOnLeft : false }) }}
							</div>
						</div>						

						<div class="clearfix mb-32 mb-m-24">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Cantidad a vender') }}</h5>
							</div>
							<div class="xs-4 m-9 pt-m-8 col">
								<div class="mt-m-24">
									<vue-slider 
										v-model="range.percent"
										:min="0"
										:max="inversionTotal"
										:interval="0.01"
										:tooltip="'none'"
										:marks="range.marks"
									>
									</vue-slider>
								</div>
							</div>
						</div>

						<div class="clearfix mb-16">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Importe a vender') }}</h5>
							</div>
							<div class="xs-4 m-9 col">
								<div class="mt-m-16 pt-m-4">
									@{{ formatCurrency(form.quantity) }} /  @{{ percentOrder }}%
								</div>
							</div>
						</div>

						<div class="clearfix mb-32 mb-m-24">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Precio de venta') }}</h5>
							</div>
							<div class="xs-4 m-9 pt-m-8 col">
								<div class="mt-m-24">
									<vue-slider 
										v-model="range.percentSold"
										:min="-15"
										:max="15"
										:interval="0.01"
										:tooltip="'none'"
										:process="range.process1"
										:marks="range.marksSoldAndBuy"
									>
									</vue-slider>
								</div>
							</div>
						</div>

						<div class="clearfix">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Vender por') }}</h5>
							</div>
							<div class="xs-4 m-9 col">
								<div class="mt-m-16 pt-m-4">
									@{{ formatCurrency(form.price) }} /  @{{ range.percentSold}}%
								</div>
							</div>
						</div>
						
						<div class="clearfix mb-16">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Estado') }}</h5>
							</div>
							<div class="xs-4 m-9 pt-m-16 col">
								<div class="mt-m-4">
									<toggle-button v-model="form.status" />
								</div>
							</div>
						</div>
					</template>
				</template>

				<template v-if="form.type == 2">
					<input type="hidden" name="quantity" v-model="form.quantity">
					<input type="hidden" name="price" v-model="form.pricePercent">
					<input type="hidden" name="status" v-model="form.status">

					<div class="clearfix mb-16">
						<div class="xs-4 m-3 col  text-right-m">
							<h5 class="pt-m-8">{{ __('Propiedad') }}</h5>
						</div>
						<div class="xs-4 m-9 col">
							<select class="custom-input  custom-input--select" name="property" id="property" v-on:change="getPropertyToBuy" v-model="form.property" required :disabled="blockEdit">

								<option value="">{{ __('Elige un inmueble') }}</option>

								@foreach($availables as $property)
								
									<option value="{{ $property->id }}">{{ $property->nombre }}</option>

								@endforeach

							</select>
						</div>
					</div>
					<template v-if="form.property">
						<div class="clearfix mb-16">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Cantidad') }}</h5>
							</div>
							<div class="xs-4 m-9 col">
								<input class="custom-input modal__input--eur" type="number" step="any" v-model="form.quantity" />
								<span class="h4 eur--icon">{{ __('€') }}</span>
							</div>
						</div>


						<div class="clearfix mb-32 mb-m-24">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Descuento/Prima') }}</h5>
							</div>
							<div class="xs-4 m-9 pt-m-8 col">
								<div class="mt-m-24">
									<vue-slider 
										v-model="range.percentBuy"
										:min="-15"
										:max="15"
										:interval="0.01"
										:tooltip="'none'"
										:process="range.process1"
										:marks="range.marksSoldAndBuy"
									>
									</vue-slider>
								</div>
							</div>
						</div>

						<div class="clearfix">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Comprar por') }}</h5>
							</div>
							<div class="xs-4 m-9 col">
								<div class="mt-m-16 pt-m-4">
									@{{ formatCurrency(priceToBuy) }} /  @{{ range.percentBuy}}%
								</div>
							</div>
						</div>
						

						<div class="clearfix mb-16">
							<div class="xs-4 m-3 col  text-right-m">
								<h5 class="pt-m-8">{{ __('Estado') }}</h5>
							</div>
							<div class="xs-4 m-9 pt-m-16 col">
								<div class="mt-m-4">
									<toggle-button v-model="form.status" />
								</div>
							</div>
						</div>

					</template>
				</template>
				

				<template v-if="addOrderButton">
					<template v-if="!blockEdit">

						<div class="clearfix">
							<div class="xs-4 m-3 col false"></div>
							<div class="xs-4 m-9 col">
								<input class="button button--mvl-full button--small   |  mt-40" type="submit" value="{{ __('Agregar orden') }}">

								<a class="ml-40 mt-40 c-2" href="#" v-on:click="cancelEdit">{{ __('Cancelar') }}</a>
							</div>
						</div>

					</template>
					<template v-else>
						<div class="clearfix">
							<div class="xs-4 m-3 col false"></div>
							
							<div class="xs-4 m-9 col">
								<template v-if="blockEdit">
									<input class="button button--mvl-full button--small   |  mt-40" type="submit" value="{{ __('Actualizar orden') }}">

									<span class="button button--mvl-full button--small bg-error  |  ml-m-16 mt-40  |  no-loading" v-on:click="removeOrder(form)" > {{ __('Eliminar') }}</span>
								</template>
							</div>

							<div class="xs-4 m-3 col false"></div>
							<div class="xs-4 m-9 col mt-24">
								<a class="c-2" href="#" v-on:click="cancelEdit">{{ __('Cancelar') }}</a>
							</div>
						</div>
					</template>
					
				</template>

			</form>

		</div>

		@if(count($orders) > 0)

		<div class="xs-4 col">

			<h4 class=" c-2 mt-48  mb-24">{{ __('Historial de órdenes') }}</h4>

			<div class="xs-4 mr-16 table__scroll" style="overflow-x: scroll">
				<table class="custom-table [ js-paginate ]">
					<tr class="text-left">
						<th class="h5">{{ __('Inversión') }}</th>
						<th class="h5">{{ __('Tipo') }}</th>
						<th class="h5 nowrap">{{ __('Cantidad') }}</th>
						<th class="h5 nowrap">{{ __('Precio') }}</th>
						<th class="h5">{{ __('Estado') }}</th>
						<th class="h5">{{ __('Opciones') }}</th>
					</tr>
					@foreach($orders as $order)
						<tr>
							@php
								if ($order->type == 1)
									$property = $order->inversion->property;
								else
									$property = $order->property;

								$propertySold = $property->property_state_id == 5;
							@endphp
							@if ($order->type == 1)
								<td class="nowrap">{{ $order->inversion->property->nombre }} <br> ({{ __('Invertido') }} {{ $order->inversion->total }}€)</td>
							@else
								<td class="nowrap">{{ $order->property->nombre }}</td>
							@endif

							<td class="">
								@if ($order->type == 1)
									{{ __('Vender') }}
								@else
									{{ __('Comprar') }}
								@endif
							</td>
							
							<td class="h5 c-2 nowrap text-right">
								@if ($order->type == 1)
									{{ formatThousandsNotZero($order->quantity) }}€ ({{ $order->percentQuantity }}%)
								@else
									{{ formatThousandsNotZero($order->quantity) }}€
								@endif
							</td>

							<td class="h5 c-2 nowrap text-right">{{ formatThousandsNotZero($order->priceMoney) }}€ ({{ $order->price }}%)</td>
							<td class="c-m">
								@if ($order->status)
									<svg xmlns="http://www.w3.org/2000/svg" fill="#37d9b6" width="16" height="16" viewBox="0 0 24 24"><circle cx="12" cy="12" r="12"/></svg>
								@else
									<svg xmlns="http://www.w3.org/2000/svg" fill="#ee596b" width="16" height="16" viewBox="0 0 24 24"><circle cx="12" cy="12" r="12"/></svg>
								@endif
							</td>
							<td class="">
								@if ($propertySold)
									<span class="c-s c-pointer" v-on:click="removeOrder({{ $order }})">{{ __('Eliminar') }}</span>
								@else
									<span class="c-s c-pointer" v-on:click="editOrder({{ $order->id }})">{{ __('Editar') }}</span>
								@endif
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>


		<div class="modal modal--popup" :class="{'modal--is-open' : modalDeleteOrder}" id="delete-order">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer" v-on:click="cancelDelete">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}

							{{-- #MODAL CONTENT --}}
								<h4 class="text-center" v-if="deleteForm.type == 2">{{ __('¿Borrar orden de compra?') }}</h4>
								<h4 class="text-center" v-if="deleteForm.type == 1">{{ __('¿Borrar orden de venta?') }}</h4>

								<div class="separator--horizontal"></div>

								<form class="[ js-loading ]" method="POST" action="{{ route('panel.user.marketplace.delete') }}">

									{{ csrf_field() }}
									{{ method_field('DELETE') }}

									<input type="hidden" name="id" v-model="deleteForm.id">

									<div class="ph-24 mt-24">
										
										<p>
											<strong>{{ __('Inversión') }}</strong>: @{{ deleteForm.property }} <br>

											<template v-if="deleteForm.type == 1">
												<strong>{{ __('Cantidad') }}</strong>: @{{ formatCurrency(deleteForm.quantity) }} / @{{ deleteForm.quantityPercent }}% <br>

												<strong>{{ __('Precio') }}</strong>: @{{ formatCurrency(deleteForm.price) }} (@{{ deleteForm.pricePercent }}%)<br>
											</template>

											<template v-if="deleteForm.type == 2">
												<strong>{{ __('Cantidad') }}</strong>: @{{ formatCurrency(deleteForm.quantity) }}<br>

												<strong>{{ __('Precio') }}</strong>: @{{ formatCurrency(deleteForm.price) }} (@{{ deleteForm.pricePercent }}%)<br>
											</template>


										</p>

										<h4 class="mv-0 text-center">{{ __('¿Estas seguro?') }}</h4>
										<button class="button button--full  |  mt-24" type="submit">
											{{ __('Eliminar orden') }}
										</button>
										
									</div>
								</form>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>

		@endif

	</div>
	@endsection






{{--==========================================================================
	#ASSETS
	========================================================================== --}}

	{{-- CSS --}}


	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script>
			var apiAll = "{{ route('api.user.all-orders') }}";
			var apiUrl = "{{ route('api.user.inversion') }}";
			var apiUrlProperties = "{{ route('property.store') }}";
			var apiGet = "{{ route('api.user.order-get') }}";
			var localeIso = "{{ localization()->getCurrentLocale() }}";

			var formCreate = "{{ route('panel.user.marketplace.save') }}";
			var formUpdate = "{{ route('panel.user.marketplace.update') }}";
		</script>
		<script src="{{ mix('assets/js/mi-mercado.js') }}" async></script>
		<script>

			var $toPaginate, //$('js-paginate'),
				perPage,
				actualPage,
				totalItems,
				pages;

			$(document).ready(function() {

				$toPaginate = $('.custom-table'); //$('js-paginate');
				perPage     = 10;
				actualPage  = 1;
				totalItems  = $toPaginate.find('tr').length - 1;
				pages       = Math.ceil( totalItems / perPage );

				// PAGINACIÓN
				var paginador = '<div class="xs-4 col  text-center  mt-16 mb-56">' + 
					'<ul class="pagination pagination--js">' +
						'<li class="pagination__prev"><a href="" data-rel="prev">«</a></li>';

				for (var i = 0; i < pages; i++) { 
					var active = "";
					if ( i == 0) { active = "active"; }
					paginador += '<li class="' + active + '"><a href="" data-rel="' +(i + 1) + '">' + (i + 1) + '</a></li>'; 
				}			
				paginador += '<li class="pagination__next"><a href="" data-rel="next">»</a></li>' +
					'</ul></div>';


				$toPaginate.after(paginador);
				desabLinks(actualPage);


				$toPaginate.find('tr').each(function () {
					$(this).hide();
				});

				$toPaginate.find('tr').eq(0).show();
				for (var i = 1; i < (perPage + 1); i++) {
					$toPaginate.find('tr').eq(i).show();
				}


				$('body').on('click', '.pagination--js li', function(e) {
					e.preventDefault();

					if ( $(this).hasClass('active') ) {
						return false;
					}

					var dataRel = $(this).find('a').attr('data-rel');

					$('.pagination--js li').removeClass('active');
					
					if (dataRel == "prev") {
						actualPage = actualPage - 1;
						$('.pagination--js li').eq(actualPage).addClass('active');
					} else if (dataRel == "next") {
						actualPage = actualPage + 1;
						$('.pagination--js li').eq(actualPage).addClass('active');
					} else {
						$(this).addClass('active');
						actualPage = Number(dataRel);
					}

					paginate( actualPage );

				})

				function paginate(page) {
					var minItem = page * perPage - perPage + 1;
					var maxItem = (page + 1) * perPage - perPage + 1;

					if (maxItem > totalItems + 1) {
						maxItem = totalItems + 1;
					}
					
					$('.custom-table').find('tr').each(function () {
						$(this).hide();
					});
					$('.custom-table').find('tr').eq(0).show();

					for (var i = minItem; i < maxItem; i++) {
						$('.custom-table').find('tr').eq(i).show();
					}

					$("html, body").animate({ scrollTop: 0 }, 0);

					desabLinks(page);
				}

				function desabLinks (actual) {
					if ( actual == 1 ) {
						$('.pagination__prev').addClass('active');
					} else {
						$('.pagination__prev').removeClass('active');
					}

					if ( actual == pages ) {
						$('.pagination__next').addClass('active');
					} else {
						$('.pagination__next').removeClass('active');
					}
				}
			});
		</script>

	@endsection
