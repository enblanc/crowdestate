{{-- 
	==========================================================================
	#POPUP - BORRAR CUENTA
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup" id="borrar-cuenta">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}


							{{-- #MODAL CONTENT --}}
								<h4 class="text-center">{{ __('Borrar cuenta') }}</h4>
								<div class="separator--horizontal"></div>
								




								
								
								@if(auth()->user()->hasLemonAccount())

									{{-- #SI NO TIENE FONDOS --}}
									@if(auth()->user()->balance == 0)

										<form class="ph-24 mt-24   [ validate js-loading ]" method="POST" action="{{ route('panel.user.acount.close') }}"
											data-parsley-validate="" data-parsley-trigger="blur"> 
											{{ csrf_field() }}
											<p class="text-center  mb-32">{{ __('Introduce tu contraseña para poder borrar tu cuenta.') }}</p>
											<input class="custom-input" v-bind:class="{'has-error' : closeAccount.error}" id="password-borrar" type="password" name="password" placeholder="{{ __('Contraseña') }}" v-model="closeAccount.password" @blur="checkPassword" required>
											<span class="help-block filled custom" v-if="closeAccount.error">
												<div class="parsley-required">@{{ closeAccount.error }}</div>
											</span>
										 	<input class="button button--full  |  mt-72 mb-16" type="submit" value="{{ __('Borrar cuenta') }}" :disabled="closeAccount.loading">
										</form>

									@endif
									{{-- FIN SI NO TIENE FONDOS --}}

									{{-- #SI TIENE FONDOS PERO NO TIENE INVERSIONES --}}
									@if(auth()->user()->balance > 0 && auth()->user()->properties->count() == 0)


										<div class="ph-24 mt-24">
											<p class="text-center ph-m-24">{{ __('Para poder borrar tu cuenta, antes tienes que retirar todos tus fondos.') }}</p>
											<a class="button button--full  |  mt-48 mb-16" href="{{ route('panel.user.moneyof.show') }}">{{ __('Retirar fondos') }}</a>
										</div>

									@endif
									{{-- FIN SI TIENE FONDOS PERO NO TIENE INVERSIONES --}}

									{{-- #SI TIENE INVERSIONES --}}
									@if(auth()->user()->properties->count() > 0)

										<div class="ph-24 mt-24">
											<p class="text-center ph-m-24">{{ __('Lo sentimos, pero con inversiones activas en inmuebles no se puede borrar la cuenta.') }}  {{ __('Si tienes alguna duda, ponte en') }} <a href="{{ route('contact') }}">{{ __('contacto con nosotros') }}</a>.</p>
										</div>

									@endif
									{{-- FIN SI TIENE INVERSIONES --}}

								@else

									<form class="ph-24 mt-24   [ validate js-loading ]" method="POST" action="{{ route('panel.user.acount.close') }}"
										data-parsley-validate="" data-parsley-trigger="blur"> 
										{{ csrf_field() }}
										<p class="text-center  mb-32">{{ __('Introduce tu contraseña para poder borrar tu cuenta.') }}</p>
										<input class="custom-input" v-bind:class="{'has-error' : closeAccount.error}" id="password-borrar" type="password" name="password" placeholder="{{ __('Contraseña') }}" v-model="closeAccount.password" @blur="checkPassword" required>
										<span class="help-block filled custom" v-if="closeAccount.error">
											<div class="parsley-required">@{{ closeAccount.error }}</div>
										</span>
									 	<input class="button button--full  |  mt-72 mb-16" type="submit" value="{{ __('Borrar cuenta') }}" :disabled="closeAccount.loading">
									</form>


								@endif
								






								



								




							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





