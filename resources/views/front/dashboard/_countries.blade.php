<div class="clearfix mb-16">
    <div class="xs-4 m-3 col  text-right-m">
        <h5 class="pt-m-8">{{ __('País') }}</h5>
    </div>
    <div class="xs-4 m-9 col">
        <select class="custom-input  custom-input--select" name="country" id="country" required>
            <option value="">{{ __('País de registro') }}</option>
            <option value="-1" {{ (old('country', $autoinvest->country) == '-1') ? 'selected' : '' }}>{{ __('Todos') }}</option>
            <option value="ES" {{ (old('country', $autoinvest->country) == 'ES') ? 'selected' : '' }}>{{ __('España') }}</option>
            <option value="AF" {{ (old('country', $autoinvest->country) == 'AF') ? 'selected' : '' }}>{{ __('Afganistán') }}</option>
            <option value="AL" {{ (old('country', $autoinvest->country) == 'AL') ? 'selected' : '' }}>{{ __('Albania') }}</option>
            <option value="DE" {{ (old('country', $autoinvest->country) == 'DE') ? 'selected' : '' }}>{{ __('Alemania') }}</option>
            <option value="AD" {{ (old('country', $autoinvest->country) == 'AD') ? 'selected' : '' }}>{{ __('Andorra') }}</option>
            <option value="AO" {{ (old('country', $autoinvest->country) == 'AO') ? 'selected' : '' }}>{{ __('Angola') }}</option>
            <option value="AI" {{ (old('country', $autoinvest->country) == 'AI') ? 'selected' : '' }}>{{ __('Anguilla') }}</option>
            <option value="AQ" {{ (old('country', $autoinvest->country) == 'AQ') ? 'selected' : '' }}>{{ __('Antártida') }}</option>
            <option value="AG" {{ (old('country', $autoinvest->country) == 'AG') ? 'selected' : '' }}>{{ __('Antigua y Barbuda') }}</option>
            <option value="AN" {{ (old('country', $autoinvest->country) == 'AN') ? 'selected' : '' }}>{{ __('Antillas Holandesas') }}</option>
            <option value="SA" {{ (old('country', $autoinvest->country) == 'SA') ? 'selected' : '' }}>{{ __('Arabia Saudí') }}</option>
            <option value="DZ" {{ (old('country', $autoinvest->country) == 'DZ') ? 'selected' : '' }}>{{ __('Argelia') }}</option>
            <option value="AR" {{ (old('country', $autoinvest->country) == 'AR') ? 'selected' : '' }}>{{ __('Argentina') }}</option>
            <option value="AM" {{ (old('country', $autoinvest->country) == 'AM') ? 'selected' : '' }}>{{ __('Armenia') }}</option>
            <option value="AW" {{ (old('country', $autoinvest->country) == 'AW') ? 'selected' : '' }}>{{ __('Aruba') }}</option>
            <option value="AU" {{ (old('country', $autoinvest->country) == 'AU') ? 'selected' : '' }}>{{ __('Australia') }}</option>
            <option value="AT" {{ (old('country', $autoinvest->country) == 'AT') ? 'selected' : '' }}>{{ __('Austria') }}</option>
            <option value="AZ" {{ (old('country', $autoinvest->country) == 'AZ') ? 'selected' : '' }}>{{ __('Azerbaiyán') }}</option>
            <option value="BS" {{ (old('country', $autoinvest->country) == 'BS') ? 'selected' : '' }}>{{ __('Bahamas') }}</option>
            <option value="BH" {{ (old('country', $autoinvest->country) == 'BH') ? 'selected' : '' }}>{{ __('Bahrein') }}</option>
            <option value="BD" {{ (old('country', $autoinvest->country) == 'BD') ? 'selected' : '' }}>{{ __('Bangladesh') }}</option>
            <option value="BB" {{ (old('country', $autoinvest->country) == 'BB') ? 'selected' : '' }}>{{ __('Barbados') }}</option>
            <option value="BE" {{ (old('country', $autoinvest->country) == 'BE') ? 'selected' : '' }}>{{ __('Bélgica') }}</option>
            <option value="BZ" {{ (old('country', $autoinvest->country) == 'BZ') ? 'selected' : '' }}>{{ __('Belice') }}</option>
            <option value="BJ" {{ (old('country', $autoinvest->country) == 'BJ') ? 'selected' : '' }}>{{ __('Benin') }}</option>
            <option value="BM" {{ (old('country', $autoinvest->country) == 'BM') ? 'selected' : '' }}>{{ __('Bermudas') }}</option>
            <option value="BY" {{ (old('country', $autoinvest->country) == 'BY') ? 'selected' : '' }}>{{ __('Bielorrusia') }}</option>
            <option value="MM" {{ (old('country', $autoinvest->country) == 'MM') ? 'selected' : '' }}>{{ __('Birmania') }}</option>
            <option value="BO" {{ (old('country', $autoinvest->country) == 'BO') ? 'selected' : '' }}>{{ __('Bolivia') }}</option>
            <option value="BA" {{ (old('country', $autoinvest->country) == 'BA') ? 'selected' : '' }}>{{ __('Bosnia y Herzegovina') }}</option>
            <option value="BW" {{ (old('country', $autoinvest->country) == 'BW') ? 'selected' : '' }}>{{ __('Botswana') }}</option>
            <option value="BR" {{ (old('country', $autoinvest->country) == 'BR') ? 'selected' : '' }}>{{ __('Brasil') }}</option>
            <option value="BN" {{ (old('country', $autoinvest->country) == 'BN') ? 'selected' : '' }}>{{ __('Brunei') }}</option>
            <option value="BG" {{ (old('country', $autoinvest->country) == 'BG') ? 'selected' : '' }}>{{ __('Bulgaria') }}</option>
            <option value="BF" {{ (old('country', $autoinvest->country) == 'BF') ? 'selected' : '' }}>{{ __('Burkina Faso') }}</option>
            <option value="BI" {{ (old('country', $autoinvest->country) == 'BI') ? 'selected' : '' }}>{{ __('Burundi') }}</option>
            <option value="BT" {{ (old('country', $autoinvest->country) == 'BT') ? 'selected' : '' }}>{{ __('Bután') }}</option>
            <option value="CV" {{ (old('country', $autoinvest->country) == 'CV') ? 'selected' : '' }}>{{ __('Cabo Verde') }}</option>
            <option value="KH" {{ (old('country', $autoinvest->country) == 'KH') ? 'selected' : '' }}>{{ __('Camboya') }}</option>
            <option value="CM" {{ (old('country', $autoinvest->country) == 'CM') ? 'selected' : '' }}>{{ __('Camerún') }}</option>
            <option value="CA" {{ (old('country', $autoinvest->country) == 'CA') ? 'selected' : '' }}>{{ __('Canadá') }}</option>
            <option value="TD" {{ (old('country', $autoinvest->country) == 'TD') ? 'selected' : '' }}>{{ __('Chad') }}</option>
            <option value="CL" {{ (old('country', $autoinvest->country) == 'CL') ? 'selected' : '' }}>{{ __('Chile') }}</option>
            <option value="CN" {{ (old('country', $autoinvest->country) == 'CN') ? 'selected' : '' }}>{{ __('China') }}</option>
            <option value="CY" {{ (old('country', $autoinvest->country) == 'CY') ? 'selected' : '' }}>{{ __('Chipre') }}</option>
            <option value="VA" {{ (old('country', $autoinvest->country) == 'VA') ? 'selected' : '' }}>{{ __('Ciudad del Vaticano') }}</option>
            <option value="CO" {{ (old('country', $autoinvest->country) == 'CO') ? 'selected' : '' }}>{{ __('Colombia') }}</option>
            <option value="KM" {{ (old('country', $autoinvest->country) == 'KM') ? 'selected' : '' }}>{{ __('Comores') }}</option>
            <option value="CG" {{ (old('country', $autoinvest->country) == 'CG') ? 'selected' : '' }}>{{ __('Congo') }}</option>
            <option value="CD" {{ (old('country', $autoinvest->country) == 'CD') ? 'selected' : '' }}>{{ __('Congo, República Democrática de') }}</option>
            <option value="KR" {{ (old('country', $autoinvest->country) == 'KR') ? 'selected' : '' }}>{{ __('Corea') }}</option>
            <option value="KP" {{ (old('country', $autoinvest->country) == 'KP') ? 'selected' : '' }}>{{ __('Corea del Norte') }}</option>
            <option value="CI" {{ (old('country', $autoinvest->country) == 'CI') ? 'selected' : '' }}>{{ __('Costa de Marfíl') }}</option>
            <option value="CR" {{ (old('country', $autoinvest->country) == 'CR') ? 'selected' : '' }}>{{ __('Costa Rica') }}</option>
            <option value="HR" {{ (old('country', $autoinvest->country) == 'HR') ? 'selected' : '' }}>{{ __('Croacia (Hrvatska)') }}</option>
            <option value="CU" {{ (old('country', $autoinvest->country) == 'CU') ? 'selected' : '' }}>{{ __('Cuba') }}</option>
            <option value="DK" {{ (old('country', $autoinvest->country) == 'DK') ? 'selected' : '' }}>{{ __('Dinamarca') }}</option>
            <option value="DJ" {{ (old('country', $autoinvest->country) == 'DJ') ? 'selected' : '' }}>{{ __('Djibouti') }}</option>
            <option value="DM" {{ (old('country', $autoinvest->country) == 'DM') ? 'selected' : '' }}>{{ __('Dominica') }}</option>
            <option value="EC" {{ (old('country', $autoinvest->country) == 'EC') ? 'selected' : '' }}>{{ __('Ecuador') }}</option>
            <option value="EG" {{ (old('country', $autoinvest->country) == 'EG') ? 'selected' : '' }}>{{ __('Egipto') }}</option>
            <option value="SV" {{ (old('country', $autoinvest->country) == 'SV') ? 'selected' : '' }}>{{ __('El Salvador') }}</option>
            <option value="AE" {{ (old('country', $autoinvest->country) == 'AE') ? 'selected' : '' }}>{{ __('Emiratos Árabes Unidos') }}</option>
            <option value="ER" {{ (old('country', $autoinvest->country) == 'ER') ? 'selected' : '' }}>{{ __('Eritrea') }}</option>
            <option value="SI" {{ (old('country', $autoinvest->country) == 'SI') ? 'selected' : '' }}>{{ __('Eslovenia') }}</option>
            <option value="US" {{ (old('country', $autoinvest->country) == 'US') ? 'selected' : '' }}>{{ __('Estados Unidos') }}</option>
            <option value="EE" {{ (old('country', $autoinvest->country) == 'EE') ? 'selected' : '' }}>{{ __('Estonia') }}</option>
            <option value="ET" {{ (old('country', $autoinvest->country) == 'ET') ? 'selected' : '' }}>{{ __('Etiopía') }}</option>
            <option value="FJ" {{ (old('country', $autoinvest->country) == 'FJ') ? 'selected' : '' }}>{{ __('Fiji') }}</option>
            <option value="PH" {{ (old('country', $autoinvest->country) == 'PH') ? 'selected' : '' }}>{{ __('Filipinas') }}</option>
            <option value="FI" {{ (old('country', $autoinvest->country) == 'FI') ? 'selected' : '' }}>{{ __('Finlandia') }}</option>
            <option value="FR" {{ (old('country', $autoinvest->country) == 'FR') ? 'selected' : '' }}>{{ __('Francia') }}</option>
            <option value="GA" {{ (old('country', $autoinvest->country) == 'GA') ? 'selected' : '' }}>{{ __('Gabón') }}</option>
            <option value="GM" {{ (old('country', $autoinvest->country) == 'GM') ? 'selected' : '' }}>{{ __('Gambia') }}</option>
            <option value="GE" {{ (old('country', $autoinvest->country) == 'GE') ? 'selected' : '' }}>{{ __('Georgia') }}</option>
            <option value="GH" {{ (old('country', $autoinvest->country) == 'GH') ? 'selected' : '' }}>{{ __('Ghana') }}</option>
            <option value="GI" {{ (old('country', $autoinvest->country) == 'GI') ? 'selected' : '' }}>{{ __('Gibraltar') }}</option>
            <option value="GD" {{ (old('country', $autoinvest->country) == 'GD') ? 'selected' : '' }}>{{ __('Granada') }}</option>
            <option value="GR" {{ (old('country', $autoinvest->country) == 'GR') ? 'selected' : '' }}>{{ __('Grecia') }}</option>
            <option value="GL" {{ (old('country', $autoinvest->country) == 'GL') ? 'selected' : '' }}>{{ __('Groenlandia') }}</option>
            <option value="GP" {{ (old('country', $autoinvest->country) == 'GP') ? 'selected' : '' }}>{{ __('Guadalupe') }}</option>
            <option value="GU" {{ (old('country', $autoinvest->country) == 'GU') ? 'selected' : '' }}>{{ __('Guam') }}</option>
            <option value="GT" {{ (old('country', $autoinvest->country) == 'GT') ? 'selected' : '' }}>{{ __('Guatemala') }}</option>
            <option value="GY" {{ (old('country', $autoinvest->country) == 'GY') ? 'selected' : '' }}>{{ __('Guayana') }}</option>
            <option value="GF" {{ (old('country', $autoinvest->country) == 'GF') ? 'selected' : '' }}>{{ __('Guayana Francesa') }}</option>
            <option value="GN" {{ (old('country', $autoinvest->country) == 'GN') ? 'selected' : '' }}>{{ __('Guinea') }}</option>
            <option value="GQ" {{ (old('country', $autoinvest->country) == 'GQ') ? 'selected' : '' }}>{{ __('Guinea Ecuatorial') }}</option>
            <option value="GW" {{ (old('country', $autoinvest->country) == 'GW') ? 'selected' : '' }}>{{ __('Guinea-Bissau') }}</option>
            <option value="HT" {{ (old('country', $autoinvest->country) == 'HT') ? 'selected' : '' }}>{{ __('Haití') }}</option>
            <option value="HN" {{ (old('country', $autoinvest->country) == 'HN') ? 'selected' : '' }}>{{ __('Honduras') }}</option>
            <option value="HU" {{ (old('country', $autoinvest->country) == 'HU') ? 'selected' : '' }}>{{ __('Hungría') }}</option>
            <option value="IN" {{ (old('country', $autoinvest->country) == 'IN') ? 'selected' : '' }}>{{ __('India') }}</option>
            <option value="ID" {{ (old('country', $autoinvest->country) == 'ID') ? 'selected' : '' }}>{{ __('Indonesia') }}</option>
            <option value="IQ" {{ (old('country', $autoinvest->country) == 'IQ') ? 'selected' : '' }}>{{ __('Irak') }}</option>
            <option value="IR" {{ (old('country', $autoinvest->country) == 'IR') ? 'selected' : '' }}>{{ __('Irán') }}</option>
            <option value="IE" {{ (old('country', $autoinvest->country) == 'IE') ? 'selected' : '' }}>{{ __('Irlanda') }}</option>
            <option value="BV" {{ (old('country', $autoinvest->country) == 'BV') ? 'selected' : '' }}>{{ __('Isla Bouvet') }}</option>
            <option value="CX" {{ (old('country', $autoinvest->country) == 'CX') ? 'selected' : '' }}>{{ __('Isla de Christmas') }}</option>
            <option value="IS" {{ (old('country', $autoinvest->country) == 'IS') ? 'selected' : '' }}>{{ __('Islandia') }}</option>
            <option value="KY" {{ (old('country', $autoinvest->country) == 'KY') ? 'selected' : '' }}>{{ __('Islas Caimán') }}</option>
            <option value="CK" {{ (old('country', $autoinvest->country) == 'CK') ? 'selected' : '' }}>{{ __('Islas Cook') }}</option>
            <option value="CC" {{ (old('country', $autoinvest->country) == 'CC') ? 'selected' : '' }}>{{ __('Islas de Cocos o Keeling') }}</option>
            <option value="FO" {{ (old('country', $autoinvest->country) == 'FO') ? 'selected' : '' }}>{{ __('Islas Faroe') }}</option>
            <option value="HM" {{ (old('country', $autoinvest->country) == 'HM') ? 'selected' : '' }}>{{ __('Islas Heard y McDonald') }}</option>
            <option value="FK" {{ (old('country', $autoinvest->country) == 'FK') ? 'selected' : '' }}>{{ __('Islas Malvinas') }}</option>
            <option value="MP" {{ (old('country', $autoinvest->country) == 'MP') ? 'selected' : '' }}>{{ __('Islas Marianas del Norte') }}</option>
            <option value="MH" {{ (old('country', $autoinvest->country) == 'MH') ? 'selected' : '' }}>{{ __('Islas Marshall') }}</option>
            <option value="UM" {{ (old('country', $autoinvest->country) == 'UM') ? 'selected' : '' }}>{{ __('Islas menores de Estados Unido') }}</option>
            <option value="PW" {{ (old('country', $autoinvest->country) == 'PW') ? 'selected' : '' }}>{{ __('Islas Palau') }}</option>
            <option value="SB" {{ (old('country', $autoinvest->country) == 'SB') ? 'selected' : '' }}>{{ __('Islas Salomón') }}</option>
            <option value="SJ" {{ (old('country', $autoinvest->country) == 'SJ') ? 'selected' : '' }}>{{ __('Islas Svalbard y Jan Mayen') }}</option>
            <option value="TK" {{ (old('country', $autoinvest->country) == 'TK') ? 'selected' : '' }}>{{ __('Islas Tokelau') }}</option>
            <option value="TC" {{ (old('country', $autoinvest->country) == 'TC') ? 'selected' : '' }}>{{ __('Islas Turks y Caicos') }}</option>
            <option value="VI" {{ (old('country', $autoinvest->country) == 'VI') ? 'selected' : '' }}>{{ __('Islas Vírgenes (EEUU)') }}</option>
            <option value="VG" {{ (old('country', $autoinvest->country) == 'VG') ? 'selected' : '' }}>{{ __('Islas Vírgenes (Reino Unido)') }}</option>
            <option value="WF" {{ (old('country', $autoinvest->country) == 'WF') ? 'selected' : '' }}>{{ __('Islas Wallis y Futuna') }}</option>
            <option value="IL" {{ (old('country', $autoinvest->country) == 'IL') ? 'selected' : '' }}>{{ __('Israel') }}</option>
            <option value="IT" {{ (old('country', $autoinvest->country) == 'IT') ? 'selected' : '' }}>{{ __('Italia') }}</option>
            <option value="JM" {{ (old('country', $autoinvest->country) == 'JM') ? 'selected' : '' }}>{{ __('Jamaica') }}</option>
            <option value="JP" {{ (old('country', $autoinvest->country) == 'JP') ? 'selected' : '' }}>{{ __('Japón') }}</option>
            <option value="JO" {{ (old('country', $autoinvest->country) == 'JO') ? 'selected' : '' }}>{{ __('Jordania') }}</option>
            <option value="KZ" {{ (old('country', $autoinvest->country) == 'KZ') ? 'selected' : '' }}>{{ __('Kazajistán') }}</option>
            <option value="KE" {{ (old('country', $autoinvest->country) == 'KE') ? 'selected' : '' }}>{{ __('Kenia') }}</option>
            <option value="KG" {{ (old('country', $autoinvest->country) == 'KG') ? 'selected' : '' }}>{{ __('Kirguizistán') }}</option>
            <option value="KI" {{ (old('country', $autoinvest->country) == 'KI') ? 'selected' : '' }}>{{ __('Kiribati') }}</option>
            <option value="KW" {{ (old('country', $autoinvest->country) == 'KW') ? 'selected' : '' }}>{{ __('Kuwait') }}</option>
            <option value="LA" {{ (old('country', $autoinvest->country) == 'LA') ? 'selected' : '' }}>{{ __('Laos') }}</option>
            <option value="LS" {{ (old('country', $autoinvest->country) == 'LS') ? 'selected' : '' }}>{{ __('Lesotho') }}</option>
            <option value="LV" {{ (old('country', $autoinvest->country) == 'LV') ? 'selected' : '' }}>{{ __('Letonia') }}</option>
            <option value="LB" {{ (old('country', $autoinvest->country) == 'LB') ? 'selected' : '' }}>{{ __('Líbano') }}</option>
            <option value="LR" {{ (old('country', $autoinvest->country) == 'LR') ? 'selected' : '' }}>{{ __('Liberia') }}</option>
            <option value="LY" {{ (old('country', $autoinvest->country) == 'LY') ? 'selected' : '' }}>{{ __('Libia') }}</option>
            <option value="LI" {{ (old('country', $autoinvest->country) == 'LI') ? 'selected' : '' }}>{{ __('Liechtenstein') }}</option>
            <option value="LT" {{ (old('country', $autoinvest->country) == 'LT') ? 'selected' : '' }}>{{ __('Lituania') }}</option>
            <option value="LU" {{ (old('country', $autoinvest->country) == 'LU') ? 'selected' : '' }}>{{ __('Luxemburgo') }}</option>
            <option value="MK" {{ (old('country', $autoinvest->country) == 'MK') ? 'selected' : '' }}>{{ __('Macedonia') }}</option>
            <option value="MG" {{ (old('country', $autoinvest->country) == 'MG') ? 'selected' : '' }}>{{ __('Madagascar') }}</option>
            <option value="MY" {{ (old('country', $autoinvest->country) == 'MY') ? 'selected' : '' }}>{{ __('Malasia') }}</option>
            <option value="MW" {{ (old('country', $autoinvest->country) == 'MW') ? 'selected' : '' }}>{{ __('Malawi') }}</option>
            <option value="MV" {{ (old('country', $autoinvest->country) == 'MV') ? 'selected' : '' }}>{{ __('Maldivas') }}</option>
            <option value="ML" {{ (old('country', $autoinvest->country) == 'ML') ? 'selected' : '' }}>{{ __('Malí') }}</option>
            <option value="MT" {{ (old('country', $autoinvest->country) == 'MT') ? 'selected' : '' }}>{{ __('Malta') }}</option>
            <option value="MA" {{ (old('country', $autoinvest->country) == 'MA') ? 'selected' : '' }}>{{ __('Marruecos') }}</option>
            <option value="MQ" {{ (old('country', $autoinvest->country) == 'MQ') ? 'selected' : '' }}>{{ __('Martinica') }}</option>
            <option value="MU" {{ (old('country', $autoinvest->country) == 'MU') ? 'selected' : '' }}>{{ __('Mauricio') }}</option>
            <option value="MR" {{ (old('country', $autoinvest->country) == 'MR') ? 'selected' : '' }}>{{ __('Mauritania') }}</option>
            <option value="YT" {{ (old('country', $autoinvest->country) == 'YT') ? 'selected' : '' }}>{{ __('Mayotte') }}</option>
            <option value="MX" {{ (old('country', $autoinvest->country) == 'MX') ? 'selected' : '' }}>{{ __('México') }}</option>
            <option value="FM" {{ (old('country', $autoinvest->country) == 'FM') ? 'selected' : '' }}>{{ __('Micronesia') }}</option>
            <option value="MD" {{ (old('country', $autoinvest->country) == 'MD') ? 'selected' : '' }}>{{ __('Moldavia') }}</option>
            <option value="MC" {{ (old('country', $autoinvest->country) == 'MC') ? 'selected' : '' }}>{{ __('Mónaco') }}</option>
            <option value="MN" {{ (old('country', $autoinvest->country) == 'MN') ? 'selected' : '' }}>{{ __('Mongolia') }}</option>
            <option value="MS" {{ (old('country', $autoinvest->country) == 'MS') ? 'selected' : '' }}>{{ __('Montserrat') }}</option>
            <option value="MZ" {{ (old('country', $autoinvest->country) == 'MZ') ? 'selected' : '' }}>{{ __('Mozambique') }}</option>
            <option value="NA" {{ (old('country', $autoinvest->country) == 'NA') ? 'selected' : '' }}>{{ __('Namibia') }}</option>
            <option value="NR" {{ (old('country', $autoinvest->country) == 'NR') ? 'selected' : '' }}>{{ __('Nauru') }}</option>
            <option value="NP" {{ (old('country', $autoinvest->country) == 'NP') ? 'selected' : '' }}>{{ __('Nepal') }}</option>
            <option value="NI" {{ (old('country', $autoinvest->country) == 'NI') ? 'selected' : '' }}>{{ __('Nicaragua') }}</option>
            <option value="NE" {{ (old('country', $autoinvest->country) == 'NE') ? 'selected' : '' }}>{{ __('Níger') }}</option>
            <option value="NG" {{ (old('country', $autoinvest->country) == 'NG') ? 'selected' : '' }}>{{ __('Nigeria') }}</option>
            <option value="NU" {{ (old('country', $autoinvest->country) == 'NU') ? 'selected' : '' }}>{{ __('Niue') }}</option>
            <option value="NF" {{ (old('country', $autoinvest->country) == 'NF') ? 'selected' : '' }}>{{ __('Norfolk') }}</option>
            <option value="NO" {{ (old('country', $autoinvest->country) == 'NO') ? 'selected' : '' }}>{{ __('Noruega') }}</option>
            <option value="NC" {{ (old('country', $autoinvest->country) == 'NC') ? 'selected' : '' }}>{{ __('Nueva Caledonia') }}</option>
            <option value="NZ" {{ (old('country', $autoinvest->country) == 'NZ') ? 'selected' : '' }}>{{ __('Nueva Zelanda') }}</option>
            <option value="OM" {{ (old('country', $autoinvest->country) == 'OM') ? 'selected' : '' }}>{{ __('Omán') }}</option>
            <option value="NL" {{ (old('country', $autoinvest->country) == 'NL') ? 'selected' : '' }}>{{ __('Países Bajos') }}</option>
            <option value="PA" {{ (old('country', $autoinvest->country) == 'PA') ? 'selected' : '' }}>{{ __('Panamá') }}</option>
            <option value="PG" {{ (old('country', $autoinvest->country) == 'PG') ? 'selected' : '' }}>{{ __('Papúa Nueva Guinea') }}</option>
            <option value="PK" {{ (old('country', $autoinvest->country) == 'PK') ? 'selected' : '' }}>{{ __('Paquistán') }}</option>
            <option value="PY" {{ (old('country', $autoinvest->country) == 'PY') ? 'selected' : '' }}>{{ __('Paraguay') }}</option>
            <option value="PE" {{ (old('country', $autoinvest->country) == 'PE') ? 'selected' : '' }}>{{ __('Perú') }}</option>
            <option value="PN" {{ (old('country', $autoinvest->country) == 'PN') ? 'selected' : '' }}>{{ __('Pitcairn') }}</option>
            <option value="PF" {{ (old('country', $autoinvest->country) == 'PF') ? 'selected' : '' }}>{{ __('Polinesia Francesa') }}</option>
            <option value="PL" {{ (old('country', $autoinvest->country) == 'PL') ? 'selected' : '' }}>{{ __('Polonia') }}</option>
            <option value="PT" {{ (old('country', $autoinvest->country) == 'PT') ? 'selected' : '' }}>{{ __('Portugal') }}</option>
            <option value="PR" {{ (old('country', $autoinvest->country) == 'PR') ? 'selected' : '' }}>{{ __('Puerto Rico') }}</option>
            <option value="QA" {{ (old('country', $autoinvest->country) == 'QA') ? 'selected' : '' }}>{{ __('Qatar') }}</option>
            <option value="GB" {{ (old('country', $autoinvest->country) == 'GB') ? 'selected' : '' }}>{{ __('Reino Unido') }}</option>
            <option value="CF" {{ (old('country', $autoinvest->country) == 'CF') ? 'selected' : '' }}>{{ __('República Centroafricana') }}</option>
            <option value="CZ" {{ (old('country', $autoinvest->country) == 'CZ') ? 'selected' : '' }}>{{ __('República Checa') }}</option>
            <option value="ZA" {{ (old('country', $autoinvest->country) == 'ZA') ? 'selected' : '' }}>{{ __('República de Sudáfrica') }}</option>
            <option value="DO" {{ (old('country', $autoinvest->country) == 'DO') ? 'selected' : '' }}>{{ __('República Dominicana') }}</option>
            <option value="SK" {{ (old('country', $autoinvest->country) == 'SK') ? 'selected' : '' }}>{{ __('República Eslovaca') }}</option>
            <option value="RE" {{ (old('country', $autoinvest->country) == 'RE') ? 'selected' : '' }}>{{ __('Reunión') }}</option>
            <option value="RW" {{ (old('country', $autoinvest->country) == 'RW') ? 'selected' : '' }}>{{ __('Ruanda') }}</option>
            <option value="RO" {{ (old('country', $autoinvest->country) == 'RO') ? 'selected' : '' }}>{{ __('Rumania') }}</option>
            <option value="RU" {{ (old('country', $autoinvest->country) == 'RU') ? 'selected' : '' }}>{{ __('Rusia') }}</option>
            <option value="EH" {{ (old('country', $autoinvest->country) == 'EH') ? 'selected' : '' }}>{{ __('Sahara Occidental') }}</option>
            <option value="KN" {{ (old('country', $autoinvest->country) == 'KN') ? 'selected' : '' }}>{{ __('Saint Kitts y Nevis') }}</option>
            <option value="WS" {{ (old('country', $autoinvest->country) == 'WS') ? 'selected' : '' }}>{{ __('Samoa') }}</option>
            <option value="AS" {{ (old('country', $autoinvest->country) == 'AS') ? 'selected' : '' }}>{{ __('Samoa Americana') }}</option>
            <option value="SM" {{ (old('country', $autoinvest->country) == 'SM') ? 'selected' : '' }}>{{ __('San Marino') }}</option>
            <option value="VC" {{ (old('country', $autoinvest->country) == 'VC') ? 'selected' : '' }}>{{ __('San Vicente y Granadinas') }}</option>
            <option value="SH" {{ (old('country', $autoinvest->country) == 'SH') ? 'selected' : '' }}>{{ __('Santa Helena') }}</option>
            <option value="LC" {{ (old('country', $autoinvest->country) == 'LC') ? 'selected' : '' }}>{{ __('Santa Lucía') }}</option>
            <option value="ST" {{ (old('country', $autoinvest->country) == 'ST') ? 'selected' : '' }}>{{ __('Santo Tomé y Príncipe') }}</option>
            <option value="SN" {{ (old('country', $autoinvest->country) == 'SN') ? 'selected' : '' }}>{{ __('Senegal') }}</option>
            <option value="SC" {{ (old('country', $autoinvest->country) == 'SC') ? 'selected' : '' }}>{{ __('Seychelles') }}</option>
            <option value="SL" {{ (old('country', $autoinvest->country) == 'SL') ? 'selected' : '' }}>{{ __('Sierra Leona') }}</option>
            <option value="SG" {{ (old('country', $autoinvest->country) == 'SG') ? 'selected' : '' }}>{{ __('Singapur') }}</option>
            <option value="SY" {{ (old('country', $autoinvest->country) == 'SY') ? 'selected' : '' }}>{{ __('Siria') }}</option>
            <option value="SO" {{ (old('country', $autoinvest->country) == 'SO') ? 'selected' : '' }}>{{ __('Somalia') }}</option>
            <option value="LK" {{ (old('country', $autoinvest->country) == 'LK') ? 'selected' : '' }}>{{ __('Sri Lanka') }}</option>
            <option value="PM" {{ (old('country', $autoinvest->country) == 'PM') ? 'selected' : '' }}>{{ __('St Pierre y Miquelon') }}</option>
            <option value="SZ" {{ (old('country', $autoinvest->country) == 'SZ') ? 'selected' : '' }}>{{ __('Suazilandia') }}</option>
            <option value="SD" {{ (old('country', $autoinvest->country) == 'SD') ? 'selected' : '' }}>{{ __('Sudán') }}</option>
            <option value="SE" {{ (old('country', $autoinvest->country) == 'SE') ? 'selected' : '' }}>{{ __('Suecia') }}</option>
            <option value="CH" {{ (old('country', $autoinvest->country) == 'CH') ? 'selected' : '' }}>{{ __('Suiza') }}</option>
            <option value="SR" {{ (old('country', $autoinvest->country) == 'SR') ? 'selected' : '' }}>{{ __('Surinam') }}</option>
            <option value="TH" {{ (old('country', $autoinvest->country) == 'TH') ? 'selected' : '' }}>{{ __('Tailandia') }}</option>
            <option value="TW" {{ (old('country', $autoinvest->country) == 'TW') ? 'selected' : '' }}>{{ __('Taiwán') }}</option>
            <option value="TZ" {{ (old('country', $autoinvest->country) == 'TZ') ? 'selected' : '' }}>{{ __('Tanzania') }}</option>
            <option value="TJ" {{ (old('country', $autoinvest->country) == 'TJ') ? 'selected' : '' }}>{{ __('Tayikistán') }}</option>
            <option value="TF" {{ (old('country', $autoinvest->country) == 'TF') ? 'selected' : '' }}>{{ __('Territorios franceses del Sur') }}</option>
            <option value="TP" {{ (old('country', $autoinvest->country) == 'TP') ? 'selected' : '' }}>{{ __('Timor Oriental') }}</option>
            <option value="TG" {{ (old('country', $autoinvest->country) == 'TG') ? 'selected' : '' }}>{{ __('Togo') }}</option>
            <option value="TO" {{ (old('country', $autoinvest->country) == 'TO') ? 'selected' : '' }}>{{ __('Tonga') }}</option>
            <option value="TT" {{ (old('country', $autoinvest->country) == 'TT') ? 'selected' : '' }}>{{ __('Trinidad y Tobago') }}</option>
            <option value="TN" {{ (old('country', $autoinvest->country) == 'TN') ? 'selected' : '' }}>{{ __('Túnez') }}</option>
            <option value="TM" {{ (old('country', $autoinvest->country) == 'TM') ? 'selected' : '' }}>{{ __('Turkmenistán') }}</option>
            <option value="TR" {{ (old('country', $autoinvest->country) == 'TR') ? 'selected' : '' }}>{{ __('Turquía') }}</option>
            <option value="TV" {{ (old('country', $autoinvest->country) == 'TV') ? 'selected' : '' }}>{{ __('Tuvalu') }}</option>
            <option value="UA" {{ (old('country', $autoinvest->country) == 'UA') ? 'selected' : '' }}>{{ __('Ucrania') }}</option>
            <option value="UG" {{ (old('country', $autoinvest->country) == 'UG') ? 'selected' : '' }}>{{ __('Uganda') }}</option>
            <option value="UY" {{ (old('country', $autoinvest->country) == 'UY') ? 'selected' : '' }}>{{ __('Uruguay') }}</option>
            <option value="UZ" {{ (old('country', $autoinvest->country) == 'UZ') ? 'selected' : '' }}>{{ __('Uzbekistán') }}</option>
            <option value="VU" {{ (old('country', $autoinvest->country) == 'VU') ? 'selected' : '' }}>{{ __('Vanuatu') }}</option>
            <option value="VE" {{ (old('country', $autoinvest->country) == 'VE') ? 'selected' : '' }}>{{ __('Venezuela') }}</option>
            <option value="VN" {{ (old('country', $autoinvest->country) == 'VN') ? 'selected' : '' }}>{{ __('Vietnam') }}</option>
            <option value="YE" {{ (old('country', $autoinvest->country) == 'YE') ? 'selected' : '' }}>{{ __('Yemen') }}</option>
            <option value="YU" {{ (old('country', $autoinvest->country) == 'YU') ? 'selected' : '' }}>{{ __('Yugoslavia') }}</option>
            <option value="ZM" {{ (old('country', $autoinvest->country) == 'ZM') ? 'selected' : '' }}>{{ __('Zambia') }}</option>
            <option value="ZW" {{ (old('country', $autoinvest->country) == 'ZW') ? 'selected' : '' }}>{{ __('Zimbabue') }}</option>
        </select>
    </div>
</div>