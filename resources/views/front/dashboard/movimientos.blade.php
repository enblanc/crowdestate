{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}

	{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')

	{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	@section('content')
		<div id="movements">
			<div class="xs-4 col mt-40 mb-40 flex items-center" style="justify-content: space-between">
				<h4 class="c-2 d-inline">{{ __('Movimientos') }}</h4>
				@if(count($transactions) > 0)
					<div>
						<a href="{{ route('panel.user.export.transactions',  auth()->user()->id) }}" class="button button--secondary button--small button--mvl-full mt-l-0 ml-l-40" v-if="loaded">
							{{ __('descargar_movimientos') }}
							{{-- <download-csv 
								:data="movements"
								:fields="fieldsExport"
								:name="'{{ __('Movimientos') }}.csv'"
								:delimiter="';'"
								v-on:export-started="exportingData"
								v-on:export-finished="dataExported"
								:disabled="exporting"
							>
								<template v-if="exporting">
									{{ __('descargando_movimientos') }}
								</template>
								<template v-else>
									{{ __('descargar_movimientos') }}
								</template>

							</download-csv> --}}
						</a>
					</div>
				@endif
			</div>

			<div class="xs-4 col table__scroll">
				@if(count($transactions) > 0)
					<vue-good-table
						:columns="columns"
						:rows="movements"
						styleClass="vgt-table striped condensed"
						:pagination-options="{
							enabled: true,
							mode: 'records',
							perPage: 25,
							position: 'top',
							perPageDropdown: [10, 25, 50, 100],
							dropdownAllowAll: true,
							setCurrentPage: 1,
							nextLabel: strings.get('general.siguiente'),
							prevLabel: strings.get('general.anterior'),
							rowsPerPageLabel: strings.get('general.movimientos-por-pagina'),
							ofLabel: strings.get('general.de'),
							allLabel: strings.get('general.todos')
						}"
						:sort-options="{
							enabled: true,
							initialSortBy: {field: 'date', type: 'desc'}
						}">
						<div slot="emptystate">
							{{ __('Cargando datos') }}
					  	</div>
						<template slot="table-row" slot-scope="props">
							<span v-if="props.column.field == 'money'">
								@{{ formatCurrency(props.row.money) }}
							</span>
							<span v-else-if="props.column.field == 'type'" v-html="props.row.type">
							</span>
							<span v-else-if="props.column.field == 'type'" v-html="props.row.type">

							</span>
							<span v-else>
								@{{ props.formattedRow[props.column.field] }}
							</span>
						</template>
					</vue-good-table>
				@else
					<p>{{ __('Actualmente aún no tienes movimientos.') }}</p>
				@endif
			</div>
		</div>
	@endsection
	{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
		<script type="text/javascript" src="libs/FileSaver/FileSaver.min.js"></script>
		<style>
			.footer__navigation__page-btn {
				display: inline !important;
			}

			.vgt-wrap__footer .footer__navigation__page-btn .chevron {
				margin: 6px 0 !important;
			}
		</style>
    @endsection

	{{-- JS --}}
	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
		<script type="text/javascript">
			var api = "{{ route('api.user.movements') }}";
			var fechaFormato = "{{ current_locale_date_format_js() }}";
		</script>
		<script src="{{ mix('assets/js/mis-movimientos.js') }}" async></script>
	@endsection 
