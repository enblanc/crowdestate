{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<form class="mt-40 validate   [ js-loading ]" method="POST" action="{{ route('panel.user.password.update') }}" 
			  data-parsley-validate="" data-parsley-trigger="blur">

			{{ csrf_field() }}
			
			@if(session()->has('pass_saved'))
				<p class="c-success">{{ __('Los cambios han sido guardados correctamente.') }}</p>
			@endif

			@if(session()->has('pass_no_match'))
				<p class="c-error">{{ __('La contraseña introducida no coincide con la que tienes actualmente en tu usuario.') }}</p>
			@endif

			@if(session()->has('pass_no_same'))
				<p class="c-error">{{ __('Las contraseñas nuevas introducidas no coinciden.') }}</p>
			@endif

			@if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    <p class="c-error">{{ $error }}</p>
                @endforeach
            @endif

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Contraseña actual') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="password" placeholder="{{ __('Nueva contraseña') }}" name="oldpassword" required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Nueva contraseña') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="password" placeholder="{{ __('Nueva contraseña') }}" name="newpassword" id="newpassword"
					data-parsley-minlength="8" data-parsley-pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$" required >
					<p class="small-text  mv-0 pt-0 pb-8">{{ __('Mínimo 8 caracteres con números y letras') }}</p>
				</div>
			</div>

			<div class="clearfix">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Repetir contraseña') }}</h5>

				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="password" placeholder="{{ __('Repetir nueva contraseña') }}" name="repeatpassword"  data-parsley-equalto="#newpassword" required>
				</div>
			</div>

			<div class="clearfix">
				<div class="xs-4 m-3 col false"></div>
				<div class="xs-4 m-9 col">
					<input class="button button--small button--mvl-full  |  mt-40" type="submit" value="{{ __('Cambiar contraseña') }}">
				</div>
			</div>

		</form>

		{{-- @include ('front.contact.errors')  --}}

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
