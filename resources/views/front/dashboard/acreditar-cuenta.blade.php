@extends('front/layouts/dashboard')

@section('content')

	<form class="mt-40 validate   [ js-loading ]" action="{{ route('panel.user.accredit.save') }}" method="POST" enctype="multipart/form-data" 
			data-parsley-validate="" data-parsley-trigger="blur">

		{{ csrf_field() }}

		
		<div class="xs-4 col">

			@if (count($errors) > 0)
				@foreach ($errors->all() as $error)
					<p class="c-error">{{ $error }}</p>
				@endforeach
			@endif

			@if(session()->has('msg_invertir'))
				<p class="c-error">{{ session()->get('msg_invertir') }}</p>
			@endif

			@if(session()->has('error_lemon'))
				<p class="c-error">{{ session()->get('error_lemon') }}</p>
			@endif

			@if(!$activado && !$pendiente && !$noForm)
				<p  class="mt-0">{{ __('Acredita tu cuenta para poder invertir rellenando los siguientes datos. Tardaremos aproximadamente 24 horas en validar la documentación aportada.') }}</p>
			@endif

			@if(!$activado && !$pendiente && $noForm)
				<p class="c-success">{{ __('Estamos comprobando la documentación. En un máximo de 4 horas tu cuenta estará operativa y podrás empezar a invertir. Te enviaremos un correo cuando esté todo listo. Bienvenido a Brickstarter.') }}</p>

				@if(pago_sin_confirmar())
					<p  class="c-success">{{ __('Recuerda que aunque tu cuenta no esté aún confirmada, puedes realizar inversiónes por un valor máximo de 250€.') }}</p>
				@endif

			@endif

			@if(!$activado && $pendiente && !session()->has('error_lemon'))
				@if(auth()->user()->DniDocument && auth()->user()->DniDocument->status != '0')
					<p class="c-error">{{ __('Por favor, sube de nuevo la documentación necesario, ya que la anterior no ha podido ser validada correctamente.') }}</p>
				@else 
					<p class="c-success">{{ __('Estamos comprobando la documentación. En un máximo de 4 horas tu cuenta estará operativa y podrás empezar a invertir. Te enviaremos un correo cuando esté todo listo. Bienvenido a Brickstarter.') }}</p>

					@if(pago_sin_confirmar())
						<p  class="c-success">{{ __('Recuerda que aunque tu cuenta no esté aún confirmada, puedes realizar inversiónes por un valor máximo de 250€.') }}</p>
					@endif
				@endif
			@endif

			@if($activado && !$pendiente)
				<p class="c-success">{{ __('Tu cuenta ha sido acreditada. ¡Ya puedes transferir fondos a tu cuenta e invertir!') }}</p>
			@endif
		</div>


		{{-- #TIPO INVERSOR 
		<div class="xs-4 col">
			<h4 class="c-2">Tipo de inversor</h4>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">Particular o Empresa</h5>
			</div>
			<div class="xs-4 m-9 col">
				<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="tipo" id="tipo" required>
					<option value="" {{ (old('tipo', auth()->user()->profile->tipo) == 0) ? 'selected' : '' }}>Selecciona tipo de actividad</option>
					<option value="1" {{ (old('tipo', auth()->user()->profile->tipo) == 'Particular' || old('tipo', auth()->user()->profile->tipo) == '1') ? 'selected' : '' }}>Particular</option>
					<option value="2" {{ (old('tipo', auth()->user()->profile->tipo) == 'Empresa' || old('tipo', auth()->user()->profile->tipo) == '2') ? 'selected' : '' }}>Empresa</option>
				</select>
			</div>
		</div>
		{{-- FIN TIPO INVERSOR --}}
		{{-- #TIPO DE INVERSOR --}}
		<div class="xs-4 col  mt-24">
			<h4 class="c-2"></h4>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col false">
			</div>
			<div class="xs-4 m-9 col">
					<input type="radio" id="checkbox-particular" class="custom-radio__input" name="tipo" value="1" checked {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}  v-model="accountType">
					<label class="d-inblock  mr-40  {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" for="checkbox-particular">
						<p class="custom-radio__text"><span class="custom-radio__new"></span>{{ __('Inversor particular') }}</p>
					</label>
					<input type="radio" id="checkbox-empresa" class="custom-radio__input" value="2" name="tipo"  v-model="accountType">
					<label class="d-inblock  {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" for="checkbox-empresa">
						<p class="custom-radio__text"><span class="custom-radio__new"></span>{{ __('Empresa') }}</p>
					</label>
			</div>
		</div>
		{{-- FIN TIPO DE INVERSOR --}}


		{{-- #DATOS PERSONALES --}}
		<div class="xs-4 col  mt-24">
			<h4 class="c-2" v-if="accountType == 2">{{ __('Datos del representante') }}</h4>
			<h4 class="c-2" v-else>{{ __('Datos personales') }}</h4>
		</div>

		{{-- TELEFONO --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Teléfono') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" type="text" name="telefono" value="{{ old('telefono', auth()->user()->profile->telefono) }}" placeholder="{{ __('Teléfono') }}" required {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
			</div>
		</div>

		{{-- NACIONALIDAD --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Nacionalidad') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="nacionalidad" id="nacionalidad" required>
					<option value="ES" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ES') ? 'selected' : '' }}>{{ __('España') }}</option>
					<option disabled>──────────</option>
					<option value="AF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AF') ? 'selected' : '' }}>{{ __('Afganistán') }}</option>
					<option value="AL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AL') ? 'selected' : '' }}>{{ __('Albania') }}</option>
					<option value="DE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'DE') ? 'selected' : '' }}>{{ __('Alemania') }}</option>
					<option value="AD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AD') ? 'selected' : '' }}>{{ __('Andorra') }}</option>
					<option value="AO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AO') ? 'selected' : '' }}>{{ __('Angola') }}</option>
					<option value="AI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AI') ? 'selected' : '' }}>{{ __('Anguilla') }}</option>
					<option value="AQ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AQ') ? 'selected' : '' }}>{{ __('Antártida') }}</option>
					<option value="AG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AG') ? 'selected' : '' }}>{{ __('Antigua y Barbuda') }}</option>
					<option value="AN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AN') ? 'selected' : '' }}>{{ __('Antillas Holandesas') }}</option>
					<option value="SA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SA') ? 'selected' : '' }}>{{ __('Arabia Saudí') }}</option>
					<option value="DZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'DZ') ? 'selected' : '' }}>{{ __('Argelia') }}</option>
					<option value="AR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AR') ? 'selected' : '' }}>{{ __('Argentina') }}</option>
					<option value="AM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AM') ? 'selected' : '' }}>{{ __('Armenia') }}</option>
					<option value="AW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AW') ? 'selected' : '' }}>{{ __('Aruba') }}</option>
					<option value="AU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AU') ? 'selected' : '' }}>{{ __('Australia') }}</option>
					<option value="AT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AT') ? 'selected' : '' }}>{{ __('Austria') }}</option>
					<option value="AZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AZ') ? 'selected' : '' }}>{{ __('Azerbaiyán') }}</option>
					<option value="BS" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BS') ? 'selected' : '' }}>{{ __('Bahamas') }}</option>
					<option value="BH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BH') ? 'selected' : '' }}>{{ __('Bahrein') }}</option>
					<option value="BD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BD') ? 'selected' : '' }}>{{ __('Bangladesh') }}</option>
					<option value="BB" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BB') ? 'selected' : '' }}>{{ __('Barbados') }}</option>
					<option value="BE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BE') ? 'selected' : '' }}>{{ __('Bélgica') }}</option>
					<option value="BZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BZ') ? 'selected' : '' }}>{{ __('Belice') }}</option>
					<option value="BJ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BJ') ? 'selected' : '' }}>{{ __('Benin') }}</option>
					<option value="BM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BM') ? 'selected' : '' }}>{{ __('Bermudas') }}</option>
					<option value="BY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BY') ? 'selected' : '' }}>{{ __('Bielorrusia') }}</option>
					<option value="MM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MM') ? 'selected' : '' }}>{{ __('Birmania') }}</option>
					<option value="BO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BO') ? 'selected' : '' }}>{{ __('Bolivia') }}</option>
					<option value="BA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BA') ? 'selected' : '' }}>{{ __('Bosnia y Herzegovina') }}</option>
					<option value="BW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BW') ? 'selected' : '' }}>{{ __('Botswana') }}</option>
					<option value="BR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BR') ? 'selected' : '' }}>{{ __('Brasil') }}</option>
					<option value="BN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BN') ? 'selected' : '' }}>{{ __('Brunei') }}</option>
					<option value="BG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BG') ? 'selected' : '' }}>{{ __('Bulgaria') }}</option>
					<option value="BF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BF') ? 'selected' : '' }}>{{ __('Burkina Faso') }}</option>
					<option value="BI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BI') ? 'selected' : '' }}>{{ __('Burundi') }}</option>
					<option value="BT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BT') ? 'selected' : '' }}>{{ __('Bután') }}</option>
					<option value="CV" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CV') ? 'selected' : '' }}>{{ __('Cabo Verde') }}</option>
					<option value="KH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KH') ? 'selected' : '' }}>{{ __('Camboya') }}</option>
					<option value="CM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CM') ? 'selected' : '' }}>{{ __('Camerún') }}</option>
					<option value="CA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CA') ? 'selected' : '' }}>{{ __('Canadá') }}</option>
					<option value="TD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TD') ? 'selected' : '' }}>{{ __('Chad') }}</option>
					<option value="CL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CL') ? 'selected' : '' }}>{{ __('Chile') }}</option>
					<option value="CN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CN') ? 'selected' : '' }}>{{ __('China') }}</option>
					<option value="CY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CY') ? 'selected' : '' }}>{{ __('Chipre') }}</option>
					<option value="VA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VA') ? 'selected' : '' }}>{{ __('Ciudad del Vaticano') }}</option>
					<option value="CO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CO') ? 'selected' : '' }}>{{ __('Colombia') }}</option>
					<option value="KM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KM') ? 'selected' : '' }}>{{ __('Comores') }}</option>
					<option value="CG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CG') ? 'selected' : '' }}>{{ __('Congo') }}</option>
					<option value="CD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CD') ? 'selected' : '' }}>{{ __('Congo, República Democrática de') }}</option>
					<option value="KR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KR') ? 'selected' : '' }}>{{ __('Corea') }}</option>
					<option value="KP" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KP') ? 'selected' : '' }}>{{ __('Corea del Norte') }}</option>
					<option value="CI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CI') ? 'selected' : '' }}>{{ __('Costa de Marfíl') }}</option>
					<option value="CR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CR') ? 'selected' : '' }}>{{ __('Costa Rica') }}</option>
					<option value="HR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'HR') ? 'selected' : '' }}>{{ __('Croacia (Hrvatska)') }}</option>
					<option value="CU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CU') ? 'selected' : '' }}>{{ __('Cuba') }}</option>
					<option value="DK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'DK') ? 'selected' : '' }}>{{ __('Dinamarca') }}</option>
					<option value="DJ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'DJ') ? 'selected' : '' }}>{{ __('Djibouti') }}</option>
					<option value="DM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'DM') ? 'selected' : '' }}>{{ __('Dominica') }}</option>
					<option value="EC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'EC') ? 'selected' : '' }}>{{ __('Ecuador') }}</option>
					<option value="EG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'EG') ? 'selected' : '' }}>{{ __('Egipto') }}</option>
					<option value="SV" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SV') ? 'selected' : '' }}>{{ __('El Salvador') }}</option>
					<option value="AE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AE') ? 'selected' : '' }}>{{ __('Emiratos Árabes Unidos') }}</option>
					<option value="ER" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ER') ? 'selected' : '' }}>{{ __('Eritrea') }}</option>
					<option value="SI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SI') ? 'selected' : '' }}>{{ __('Eslovenia') }}</option>
					<option value="US" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'US') ? 'selected' : '' }}>{{ __('Estados Unidos') }}</option>
					<option value="EE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'EE') ? 'selected' : '' }}>{{ __('Estonia') }}</option>
					<option value="ET" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ET') ? 'selected' : '' }}>{{ __('Etiopía') }}</option>
					<option value="FJ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'FJ') ? 'selected' : '' }}>{{ __('Fiji') }}</option>
					<option value="PH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PH') ? 'selected' : '' }}>{{ __('Filipinas') }}</option>
					<option value="FI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'FI') ? 'selected' : '' }}>{{ __('Finlandia') }}</option>
					<option value="FR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'FR') ? 'selected' : '' }}>{{ __('Francia') }}</option>
					<option value="GA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GA') ? 'selected' : '' }}>{{ __('Gabón') }}</option>
					<option value="GM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GM') ? 'selected' : '' }}>{{ __('Gambia') }}</option>
					<option value="GE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GE') ? 'selected' : '' }}>{{ __('Georgia') }}</option>
					<option value="GH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GH') ? 'selected' : '' }}>{{ __('Ghana') }}</option>
					<option value="GI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GI') ? 'selected' : '' }}>{{ __('Gibraltar') }}</option>
					<option value="GD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GD') ? 'selected' : '' }}>{{ __('Granada') }}</option>
					<option value="GR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GR') ? 'selected' : '' }}>{{ __('Grecia') }}</option>
					<option value="GL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GL') ? 'selected' : '' }}>{{ __('Groenlandia') }}</option>
					<option value="GP" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GP') ? 'selected' : '' }}>{{ __('Guadalupe') }}</option>
					<option value="GU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GU') ? 'selected' : '' }}>{{ __('Guam') }}</option>
					<option value="GT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GT') ? 'selected' : '' }}>{{ __('Guatemala') }}</option>
					<option value="GY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GY') ? 'selected' : '' }}>{{ __('Guayana') }}</option>
					<option value="GF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GF') ? 'selected' : '' }}>{{ __('Guayana Francesa') }}</option>
					<option value="GN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GN') ? 'selected' : '' }}>{{ __('Guinea') }}</option>
					<option value="GQ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GQ') ? 'selected' : '' }}>{{ __('Guinea Ecuatorial') }}</option>
					<option value="GW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GW') ? 'selected' : '' }}>{{ __('Guinea-Bissau') }}</option>
					<option value="HT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'HT') ? 'selected' : '' }}>{{ __('Haití') }}</option>
					<option value="HN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'HN') ? 'selected' : '' }}>{{ __('Honduras') }}</option>
					<option value="HU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'HU') ? 'selected' : '' }}>{{ __('Hungría') }}</option>
					<option value="IN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IN') ? 'selected' : '' }}>{{ __('India') }}</option>
					<option value="ID" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ID') ? 'selected' : '' }}>{{ __('Indonesia') }}</option>
					<option value="IQ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IQ') ? 'selected' : '' }}>{{ __('Irak') }}</option>
					<option value="IR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IR') ? 'selected' : '' }}>{{ __('Irán') }}</option>
					<option value="IE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IE') ? 'selected' : '' }}>{{ __('Irlanda') }}</option>
					<option value="BV" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'BV') ? 'selected' : '' }}>{{ __('Isla Bouvet') }}</option>
					<option value="CX" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CX') ? 'selected' : '' }}>{{ __('Isla de Christmas') }}</option>
					<option value="IS" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IS') ? 'selected' : '' }}>{{ __('Islandia') }}</option>
					<option value="KY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KY') ? 'selected' : '' }}>{{ __('Islas Caimán') }}</option>
					<option value="CK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CK') ? 'selected' : '' }}>{{ __('Islas Cook') }}</option>
					<option value="CC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CC') ? 'selected' : '' }}>{{ __('Islas de Cocos o Keeling') }}</option>
					<option value="FO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'FO') ? 'selected' : '' }}>{{ __('Islas Faroe') }}</option>
					<option value="HM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'HM') ? 'selected' : '' }}>{{ __('Islas Heard y McDonald') }}</option>
					<option value="FK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'FK') ? 'selected' : '' }}>{{ __('Islas Malvinas') }}</option>
					<option value="MP" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MP') ? 'selected' : '' }}>{{ __('Islas Marianas del Norte') }}</option>
					<option value="MH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MH') ? 'selected' : '' }}>{{ __('Islas Marshall') }}</option>
					<option value="UM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'UM') ? 'selected' : '' }}>{{ __('Islas menores de Estados Unido') }}</option>
					<option value="PW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PW') ? 'selected' : '' }}>{{ __('Islas Palau') }}</option>
					<option value="SB" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SB') ? 'selected' : '' }}>{{ __('Islas Salomón') }}</option>
					<option value="SJ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SJ') ? 'selected' : '' }}>{{ __('Islas Svalbard y Jan Mayen') }}</option>
					<option value="TK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TK') ? 'selected' : '' }}>{{ __('Islas Tokelau') }}</option>
					<option value="TC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TC') ? 'selected' : '' }}>{{ __('Islas Turks y Caicos') }}</option>
					<option value="VI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VI') ? 'selected' : '' }}>{{ __('Islas Vírgenes (EEUU)') }}</option>
					<option value="VG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VG') ? 'selected' : '' }}>{{ __('Islas Vírgenes (Reino Unido)') }}</option>
					<option value="WF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'WF') ? 'selected' : '' }}>{{ __('Islas Wallis y Futuna') }}</option>
					<option value="IL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IL') ? 'selected' : '' }}>{{ __('Israel') }}</option>
					<option value="IT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'IT') ? 'selected' : '' }}>{{ __('Italia') }}</option>
					<option value="JM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'JM') ? 'selected' : '' }}>{{ __('Jamaica') }}</option>
					<option value="JP" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'JP') ? 'selected' : '' }}>{{ __('Japón') }}</option>
					<option value="JO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'JO') ? 'selected' : '' }}>{{ __('Jordania') }}</option>
					<option value="KZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KZ') ? 'selected' : '' }}>{{ __('Kazajistán') }}</option>
					<option value="KE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KE') ? 'selected' : '' }}>{{ __('Kenia') }}</option>
					<option value="KG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KG') ? 'selected' : '' }}>{{ __('Kirguizistán') }}</option>
					<option value="KI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KI') ? 'selected' : '' }}>{{ __('Kiribati') }}</option>
					<option value="KW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KW') ? 'selected' : '' }}>{{ __('Kuwait') }}</option>
					<option value="LA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LA') ? 'selected' : '' }}>{{ __('Laos') }}</option>
					<option value="LS" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LS') ? 'selected' : '' }}>{{ __('Lesotho') }}</option>
					<option value="LV" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LV') ? 'selected' : '' }}>{{ __('Letonia') }}</option>
					<option value="LB" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LB') ? 'selected' : '' }}>{{ __('Líbano') }}</option>
					<option value="LR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LR') ? 'selected' : '' }}>{{ __('Liberia') }}</option>
					<option value="LY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LY') ? 'selected' : '' }}>{{ __('Libia') }}</option>
					<option value="LI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LI') ? 'selected' : '' }}>{{ __('Liechtenstein') }}</option>
					<option value="LT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LT') ? 'selected' : '' }}>{{ __('Lituania') }}</option>
					<option value="LU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LU') ? 'selected' : '' }}>{{ __('Luxemburgo') }}</option>
					<option value="MK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MK') ? 'selected' : '' }}>{{ __('Macedonia') }}</option>
					<option value="MG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MG') ? 'selected' : '' }}>{{ __('Madagascar') }}</option>
					<option value="MY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MY') ? 'selected' : '' }}>{{ __('Malasia') }}</option>
					<option value="MW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MW') ? 'selected' : '' }}>{{ __('Malawi') }}</option>
					<option value="MV" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MV') ? 'selected' : '' }}>{{ __('Maldivas') }}</option>
					<option value="ML" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ML') ? 'selected' : '' }}>{{ __('Malí') }}</option>
					<option value="MT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MT') ? 'selected' : '' }}>{{ __('Malta') }}</option>
					<option value="MA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MA') ? 'selected' : '' }}>{{ __('Marruecos') }}</option>
					<option value="MQ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MQ') ? 'selected' : '' }}>{{ __('Martinica') }}</option>
					<option value="MU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MU') ? 'selected' : '' }}>{{ __('Mauricio') }}</option>
					<option value="MR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MR') ? 'selected' : '' }}>{{ __('Mauritania') }}</option>
					<option value="YT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'YT') ? 'selected' : '' }}>{{ __('Mayotte') }}</option>
					<option value="MX" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MX') ? 'selected' : '' }}>{{ __('México') }}</option>
					<option value="FM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'FM') ? 'selected' : '' }}>{{ __('Micronesia') }}</option>
					<option value="MD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MD') ? 'selected' : '' }}>{{ __('Moldavia') }}</option>
					<option value="MC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MC') ? 'selected' : '' }}>{{ __('Mónaco') }}</option>
					<option value="MN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MN') ? 'selected' : '' }}>{{ __('Mongolia') }}</option>
					<option value="MS" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MS') ? 'selected' : '' }}>{{ __('Montserrat') }}</option>
					<option value="MZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'MZ') ? 'selected' : '' }}>{{ __('Mozambique') }}</option>
					<option value="NA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NA') ? 'selected' : '' }}>{{ __('Namibia') }}</option>
					<option value="NR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NR') ? 'selected' : '' }}>{{ __('Nauru') }}</option>
					<option value="NP" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NP') ? 'selected' : '' }}>{{ __('Nepal') }}</option>
					<option value="NI" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NI') ? 'selected' : '' }}>{{ __('Nicaragua') }}</option>
					<option value="NE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NE') ? 'selected' : '' }}>{{ __('Níger') }}</option>
					<option value="NG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NG') ? 'selected' : '' }}>{{ __('Nigeria') }}</option>
					<option value="NU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NU') ? 'selected' : '' }}>{{ __('Niue') }}</option>
					<option value="NF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NF') ? 'selected' : '' }}>{{ __('Norfolk') }}</option>
					<option value="NO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NO') ? 'selected' : '' }}>{{ __('Noruega') }}</option>
					<option value="NC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NC') ? 'selected' : '' }}>{{ __('Nueva Caledonia') }}</option>
					<option value="NZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NZ') ? 'selected' : '' }}>{{ __('Nueva Zelanda') }}</option>
					<option value="OM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'OM') ? 'selected' : '' }}>{{ __('Omán') }}</option>
					<option value="NL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'NL') ? 'selected' : '' }}>{{ __('Países Bajos') }}</option>
					<option value="PA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PA') ? 'selected' : '' }}>{{ __('Panamá') }}</option>
					<option value="PG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PG') ? 'selected' : '' }}>{{ __('Papúa Nueva Guinea') }}</option>
					<option value="PK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PK') ? 'selected' : '' }}>{{ __('Paquistán') }}</option>
					<option value="PY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PY') ? 'selected' : '' }}>{{ __('Paraguay') }}</option>
					<option value="PE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PE') ? 'selected' : '' }}>{{ __('Perú') }}</option>
					<option value="PN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PN') ? 'selected' : '' }}>{{ __('Pitcairn') }}</option>
					<option value="PF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PF') ? 'selected' : '' }}>{{ __('Polinesia Francesa') }}</option>
					<option value="PL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PL') ? 'selected' : '' }}>{{ __('Polonia') }}</option>
					<option value="PT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PT') ? 'selected' : '' }}>{{ __('Portugal') }}</option>
					<option value="PR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PR') ? 'selected' : '' }}>{{ __('Puerto Rico') }}</option>
					<option value="QA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'QA') ? 'selected' : '' }}>{{ __('Qatar') }}</option>
					<option value="GB" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'GB') ? 'selected' : '' }}>{{ __('Reino Unido') }}</option>
					<option value="CF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CF') ? 'selected' : '' }}>{{ __('República Centroafricana') }}</option>
					<option value="CZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CZ') ? 'selected' : '' }}>{{ __('República Checa') }}</option>
					<option value="ZA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ZA') ? 'selected' : '' }}>{{ __('República de Sudáfrica') }}</option>
					<option value="DO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'DO') ? 'selected' : '' }}>{{ __('República Dominicana') }}</option>
					<option value="SK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SK') ? 'selected' : '' }}>{{ __('República Eslovaca') }}</option>
					<option value="RE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'RE') ? 'selected' : '' }}>{{ __('Reunión') }}</option>
					<option value="RW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'RW') ? 'selected' : '' }}>{{ __('Ruanda') }}</option>
					<option value="RO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'RO') ? 'selected' : '' }}>{{ __('Rumania') }}</option>
					<option value="RU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'RU') ? 'selected' : '' }}>{{ __('Rusia') }}</option>
					<option value="EH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'EH') ? 'selected' : '' }}>{{ __('Sahara Occidental') }}</option>
					<option value="KN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'KN') ? 'selected' : '' }}>{{ __('Saint Kitts y Nevis') }}</option>
					<option value="WS" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'WS') ? 'selected' : '' }}>{{ __('Samoa') }}</option>
					<option value="AS" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'AS') ? 'selected' : '' }}>{{ __('Samoa Americana') }}</option>
					<option value="SM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SM') ? 'selected' : '' }}>{{ __('San Marino') }}</option>
					<option value="VC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VC') ? 'selected' : '' }}>{{ __('San Vicente y Granadinas') }}</option>
					<option value="SH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SH') ? 'selected' : '' }}>{{ __('Santa Helena') }}</option>
					<option value="LC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LC') ? 'selected' : '' }}>{{ __('Santa Lucía') }}</option>
					<option value="ST" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ST') ? 'selected' : '' }}>{{ __('Santo Tomé y Príncipe') }}</option>
					<option value="SN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SN') ? 'selected' : '' }}>{{ __('Senegal') }}</option>
					<option value="SC" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SC') ? 'selected' : '' }}>{{ __('Seychelles') }}</option>
					<option value="SL" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SL') ? 'selected' : '' }}>{{ __('Sierra Leona') }}</option>
					<option value="SG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SG') ? 'selected' : '' }}>{{ __('Singapur') }}</option>
					<option value="SY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SY') ? 'selected' : '' }}>{{ __('Siria') }}</option>
					<option value="SO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SO') ? 'selected' : '' }}>{{ __('Somalia') }}</option>
					<option value="LK" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'LK') ? 'selected' : '' }}>{{ __('Sri Lanka') }}</option>
					<option value="PM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'PM') ? 'selected' : '' }}>{{ __('St Pierre y Miquelon') }}</option>
					<option value="SZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SZ') ? 'selected' : '' }}>{{ __('Suazilandia') }}</option>
					<option value="SD" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SD') ? 'selected' : '' }}>{{ __('Sudán') }}</option>
					<option value="SE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SE') ? 'selected' : '' }}>{{ __('Suecia') }}</option>
					<option value="CH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'CH') ? 'selected' : '' }}>{{ __('Suiza') }}</option>
					<option value="SR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'SR') ? 'selected' : '' }}>{{ __('Surinam') }}</option>
					<option value="TH" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TH') ? 'selected' : '' }}>{{ __('Tailandia') }}</option>
					<option value="TW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TW') ? 'selected' : '' }}>{{ __('Taiwán') }}</option>
					<option value="TZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TZ') ? 'selected' : '' }}>{{ __('Tanzania') }}</option>
					<option value="TJ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TJ') ? 'selected' : '' }}>{{ __('Tayikistán') }}</option>
					<option value="TF" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TF') ? 'selected' : '' }}>{{ __('Territorios franceses del Sur') }}</option>
					<option value="TP" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TP') ? 'selected' : '' }}>{{ __('Timor Oriental') }}</option>
					<option value="TG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TG') ? 'selected' : '' }}>{{ __('Togo') }}</option>
					<option value="TO" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TO') ? 'selected' : '' }}>{{ __('Tonga') }}</option>
					<option value="TT" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TT') ? 'selected' : '' }}>{{ __('Trinidad y Tobago') }}</option>
					<option value="TN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TN') ? 'selected' : '' }}>{{ __('Túnez') }}</option>
					<option value="TM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TM') ? 'selected' : '' }}>{{ __('Turkmenistán') }}</option>
					<option value="TR" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TR') ? 'selected' : '' }}>{{ __('Turquía') }}</option>
					<option value="TV" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'TV') ? 'selected' : '' }}>{{ __('Tuvalu') }}</option>
					<option value="UA" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'UA') ? 'selected' : '' }}>{{ __('Ucrania') }}</option>
					<option value="UG" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'UG') ? 'selected' : '' }}>{{ __('Uganda') }}</option>
					<option value="UY" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'UY') ? 'selected' : '' }}>{{ __('Uruguay') }}</option>
					<option value="UZ" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'UZ') ? 'selected' : '' }}>{{ __('Uzbekistán') }}</option>
					<option value="VU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VU') ? 'selected' : '' }}>{{ __('Vanuatu') }}</option>
					<option value="VE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VE') ? 'selected' : '' }}>{{ __('Venezuela') }}</option>
					<option value="VN" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'VN') ? 'selected' : '' }}>{{ __('Vietnam') }}</option>
					<option value="YE" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'YE') ? 'selected' : '' }}>{{ __('Yemen') }}</option>
					<option value="YU" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'YU') ? 'selected' : '' }}>{{ __('Yugoslavia') }}</option>
					<option value="ZM" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ZM') ? 'selected' : '' }}>{{ __('Zambia') }}</option>
					<option value="ZW" {{ (old('nacionalidad', auth()->user()->profile->nacionalidad) == 'ZW') ? 'selected' : '' }}>{{ __('Zimbabue') }}</option>
				</select>
			</div>
		</div>

		{{-- CIUDAD --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Ciudad de nacimiento') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input
					class="custom-input"
					type="text"
					name="ciudad"
					value="{{ old('ciudad', auth()->user()->profile->ciudad) }}"
					placeholder="{{ __('Ciudad') }}"
					required
					>
			</div>
		</div>

		{{-- SESO --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Sexo') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="sexo" id="sexo" required>
					<option value=""  {{ (old('sexo', auth()->user()->profile->sexo) == 0) ? 'selected' : '' }}>{{ __('Selecciona sexo') }}</option>
					<option value="1" {{ (old('sexo', auth()->user()->profile->sexo) == __('Masculino')) ? 'selected' : '' }}>{{ __('Masculino') }}</option>
					<option value="2" {{ (old('sexo', auth()->user()->profile->sexo) == __('Femenino')) ? 'selected' : '' }}>{{ __('Femenino') }}</option>
				</select>
			</div>
		</div>

		{{-- FECHA DE NACIMIENTO --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Fecha de nacimiento') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" id="prueba" type="{{ (isset($noForm) && $noForm == true) ? 'text' : 'text' }}" name="birthdate" value="{{ old('birthdate', (auth()->user()->profile->birthdate !== null) ? Carbon\Carbon::parse(auth()->user()->profile->birthdate)->format('d/m/Y') : years_before(25)) }}" placeholder="DD/MM/YYYY" required {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
			</div>
		</div>

		{{-- ESTADO CIVIL --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Estado civil') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="estado_civil" id="estado_civil" required>
					<option value="" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == null) ? 'selected' : '' }}>{{ __('Selecciona tu estado civil') }}</option>
					<option value="1" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == 1) ? 'selected' : '' }}>{{ __('Soltero/a') }}</option>
					<option value="2" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == 2) ? 'selected' : '' }}>{{ __('Casado/a separación de bienes') }}</option>
					<option value="6" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == 6) ? 'selected' : '' }}>{{ __('Casado/a gananciales') }}</option>
					<option value="3" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == 3) ? 'selected' : '' }}>{{ __('Separado/a') }}</option>
					<option value="4" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == 4) ? 'selected' : '' }}>{{ __('Divorciado/a') }}</option>
					<option value="5" {{ (old('estado_civil', auth()->user()->profile->estado_civil) == 5) ? 'selected' : '' }}>{{ __('Viudo/a') }}</option>
				</select>
			</div>
		</div>

		{{-- PROFESION --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Profesión') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" type="text" name="profesion" value="{{ old('profesion', auth()->user()->profile->profesion) }}" placeholder="{{ __('Profesión') }}" required {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
			</div>
		</div>

		{{-- DNI/NIE/PASAPORTE --}}
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('DNI/NIE/Pasaporte') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" type="text" name="dni" value="{{ old('dni', auth()->user()->profile->dni) }}" placeholder="{{ __('Número de DNI') }}" {{ (auth()->user()->activado) ? 'readonly' : '' }} {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }} {{ (auth()->user()->profile->estado) ? '' : 'required' }}>
			</div>
		</div>

		<div class="clearfix mb-0">
			<div class="xs-4 m-3 col false">
			</div>
			<div class="xs-4 m-9 col">
				<p>{{ __('Necesitamos las dos caras de tu identificación para la acreditación de la cuenta.') }} <span class="from-m-inline">{{ __('Si entras desde un dispositivo móvil, podrás hacer uso de su cámara para hacer una foto a tu documento.') }}</span></p>
			</div>
		</div>

		<div class="clearfix mb-0  |  only-l">
			<div class="xs-4 m-3 col false">
			</div>
			<div class="xs-4 m-9 col">
					<input type="radio" id="checkbox-archivos" name="tipoArchivo" class="custom-radio__input" value="1" checked {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }} v-model="tipoSubida">
					<label class="d-inblock  mr-40" for="checkbox-archivos">
						<p class="custom-radio__text"><span class="custom-radio__new"></span>{{ __('Subir archivos') }}</p>
					</label>
					<input type="radio" id="checkbox-webcam"  name="tipoArchivo" class="custom-radio__input" value="2" v-model="tipoSubida">
					<label class="d-inblock  {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" for="checkbox-webcam">
						<p class="custom-radio__text"><span class="custom-radio__new"></span>{{ __('Utilizar webcam') }}</p>
					</label>
			</div>
		</div>

		<div class="clearfix mb-0">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Imagen frontal') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<template v-if="tipoSubida == '1'">
					<label class="custom-file__label  |  mb-16" title="" data-text="{{ __('Seleccionar archivo') }}" data-file="{{ __('Sube una imagen') }}">
						<input class="custom-file__input" type="file" accept="image/*" name="imagen_dni"  onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))"  {{ (auth()->user()->profile->estado) ? '' : 'required' }} />
					</label>
				</template>
				<template v-if="tipoSubida == '2'">
					<label class="custom-file__label  |  mb-16" :title="webcam.front" data-text="{{ __('Abrir webcam') }}" data-file="{{ __('Realiza una foto con la webcam') }}">
						<input class="custom-file__input" type="file" accept="image/*;capture=camera" id="imagen_dni_front" v-on:click.prevent="openModalWebcamFront" capture="camera" {{ (auth()->user()->profile->estado) ? '' : 'required' }} />
					</label>
					<input type="hidden" name="webcam_front" v-model="webcam.front">
				</template>
			</div>
		</div>
		
		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Imagen trasera') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<template v-if="tipoSubida == '1'">
					<label class="custom-file__label  |  mb-16" title="" data-text="{{ __('Seleccionar archivo') }}" data-file="{{ __('Sube una imagen') }}">
						<input class="custom-file__input" type="file" accept="image/*" name="imagen_dni_dos"  onchange="this.parentNode.setAttribute('title', this.value.replace(/^.*[\\/]/, ''))"  {{ (auth()->user()->profile->estado) ? '' : 'required' }} />
					</label>
					<p class="small-text c-2 mt-0">{{ __('.jpg o .png, máximo 3mb.') }}</p>
				</template>
				<template v-if="tipoSubida == '2'">
					<label class="custom-file__label  |  mb-16" :title="webcam.back" data-text="{{ __('Abrir webcam') }}" data-file="{{ __('Realiza una foto con la webcam') }}">
						<input class="custom-file__input" type="file" accept="image/*;capture=camera" id="imagen_dni_back" v-on:click.prevent="openModalWebcamBack" capture="camera" {{ (auth()->user()->profile->estado) ? '' : 'required' }} />
					</label>
					<input type="hidden" name="webcam_back" v-model="webcam.back">
				</template>
				
				@if(auth()->user()->DniDocument)
				<h5 class="d-inline">
					Estado:</h5> <p class="d-inline  c-2"> 
					{{ auth()->user()->DniDocument->statusText }}
					@if(auth()->user()->DniDocument && auth()->user()->DniDocument->isValid)
						- <a href="{{ asset(auth()->user()->DniDocument->getDni()->getUrl()) }}" download>{{ __('Descargar') }}</a>
					@endif
				</p>
				@endif

			</div>
		</div>
		{{-- FIN DATOS PERSONALES --}}


		{{-- #DATOS EMPRESA --}}
		<div v-if="accountType == 2">
			<div class="xs-4 col  mt-24">
				<h4 class="c-2">{{ __('Datos empresa') }}</h4>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Razón social') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="company_rsocial" value="{{ old('company_rsocial', auth()->user()->profile->company_rsocial) }}" placeholder="{{ __('Razón social') }}" {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }} required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('CIF') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="company_cif" value="{{ old('company_cif', auth()->user()->profile->company_cif) }}" placeholder="{{ __('CIF') }}" {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }} required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Página web') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="company_website" value="{{ old('company_website', auth()->user()->profile->company_website) }}" placeholder="{{ __('www.paginaweb.com') }}" placeholder="Página web" {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }} required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Descripción') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" name="company_description" value="{{ old('company_description', auth()->user()->profile->company_description) }}" placeholder="{{ __('Descripción de la empresa') }}" {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }} required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('País') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="company_country" id="pais-registro" required>
						<option value="">{{ __('País de registro') }}</option>
						<option value="ES" {{ (old('company_country', auth()->user()->profile->company_country) == 'ES') ? 'selected' : '' }}>{{ __('España') }}</option>
						<option disabled>──────────</option>
						<option value="AF" {{ (old('company_country', auth()->user()->profile->company_country) == 'AF') ? 'selected' : '' }}>{{ __('Afganistán') }}</option>
						<option value="AL" {{ (old('company_country', auth()->user()->profile->company_country) == 'AL') ? 'selected' : '' }}>{{ __('Albania') }}</option>
						<option value="DE" {{ (old('company_country', auth()->user()->profile->company_country) == 'DE') ? 'selected' : '' }}>{{ __('Alemania') }}</option>
						<option value="AD" {{ (old('company_country', auth()->user()->profile->company_country) == 'AD') ? 'selected' : '' }}>{{ __('Andorra') }}</option>
						<option value="AO" {{ (old('company_country', auth()->user()->profile->company_country) == 'AO') ? 'selected' : '' }}>{{ __('Angola') }}</option>
						<option value="AI" {{ (old('company_country', auth()->user()->profile->company_country) == 'AI') ? 'selected' : '' }}>{{ __('Anguilla') }}</option>
						<option value="AQ" {{ (old('company_country', auth()->user()->profile->company_country) == 'AQ') ? 'selected' : '' }}>{{ __('Antártida') }}</option>
						<option value="AG" {{ (old('company_country', auth()->user()->profile->company_country) == 'AG') ? 'selected' : '' }}>{{ __('Antigua y Barbuda') }}</option>
						<option value="AN" {{ (old('company_country', auth()->user()->profile->company_country) == 'AN') ? 'selected' : '' }}>{{ __('Antillas Holandesas') }}</option>
						<option value="SA" {{ (old('company_country', auth()->user()->profile->company_country) == 'SA') ? 'selected' : '' }}>{{ __('Arabia Saudí') }}</option>
						<option value="DZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'DZ') ? 'selected' : '' }}>{{ __('Argelia') }}</option>
						<option value="AR" {{ (old('company_country', auth()->user()->profile->company_country) == 'AR') ? 'selected' : '' }}>{{ __('Argentina') }}</option>
						<option value="AM" {{ (old('company_country', auth()->user()->profile->company_country) == 'AM') ? 'selected' : '' }}>{{ __('Armenia') }}</option>
						<option value="AW" {{ (old('company_country', auth()->user()->profile->company_country) == 'AW') ? 'selected' : '' }}>{{ __('Aruba') }}</option>
						<option value="AU" {{ (old('company_country', auth()->user()->profile->company_country) == 'AU') ? 'selected' : '' }}>{{ __('Australia') }}</option>
						<option value="AT" {{ (old('company_country', auth()->user()->profile->company_country) == 'AT') ? 'selected' : '' }}>{{ __('Austria') }}</option>
						<option value="AZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'AZ') ? 'selected' : '' }}>{{ __('Azerbaiyán') }}</option>
						<option value="BS" {{ (old('company_country', auth()->user()->profile->company_country) == 'BS') ? 'selected' : '' }}>{{ __('Bahamas') }}</option>
						<option value="BH" {{ (old('company_country', auth()->user()->profile->company_country) == 'BH') ? 'selected' : '' }}>{{ __('Bahrein') }}</option>
						<option value="BD" {{ (old('company_country', auth()->user()->profile->company_country) == 'BD') ? 'selected' : '' }}>{{ __('Bangladesh') }}</option>
						<option value="BB" {{ (old('company_country', auth()->user()->profile->company_country) == 'BB') ? 'selected' : '' }}>{{ __('Barbados') }}</option>
						<option value="BE" {{ (old('company_country', auth()->user()->profile->company_country) == 'BE') ? 'selected' : '' }}>{{ __('Bélgica') }}</option>
						<option value="BZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'BZ') ? 'selected' : '' }}>{{ __('Belice') }}</option>
						<option value="BJ" {{ (old('company_country', auth()->user()->profile->company_country) == 'BJ') ? 'selected' : '' }}>{{ __('Benin') }}</option>
						<option value="BM" {{ (old('company_country', auth()->user()->profile->company_country) == 'BM') ? 'selected' : '' }}>{{ __('Bermudas') }}</option>
						<option value="BY" {{ (old('company_country', auth()->user()->profile->company_country) == 'BY') ? 'selected' : '' }}>{{ __('Bielorrusia') }}</option>
						<option value="MM" {{ (old('company_country', auth()->user()->profile->company_country) == 'MM') ? 'selected' : '' }}>{{ __('Birmania') }}</option>
						<option value="BO" {{ (old('company_country', auth()->user()->profile->company_country) == 'BO') ? 'selected' : '' }}>{{ __('Bolivia') }}</option>
						<option value="BA" {{ (old('company_country', auth()->user()->profile->company_country) == 'BA') ? 'selected' : '' }}>{{ __('Bosnia y Herzegovina') }}</option>
						<option value="BW" {{ (old('company_country', auth()->user()->profile->company_country) == 'BW') ? 'selected' : '' }}>{{ __('Botswana') }}</option>
						<option value="BR" {{ (old('company_country', auth()->user()->profile->company_country) == 'BR') ? 'selected' : '' }}>{{ __('Brasil') }}</option>
						<option value="BN" {{ (old('company_country', auth()->user()->profile->company_country) == 'BN') ? 'selected' : '' }}>{{ __('Brunei') }}</option>
						<option value="BG" {{ (old('company_country', auth()->user()->profile->company_country) == 'BG') ? 'selected' : '' }}>{{ __('Bulgaria') }}</option>
						<option value="BF" {{ (old('company_country', auth()->user()->profile->company_country) == 'BF') ? 'selected' : '' }}>{{ __('Burkina Faso') }}</option>
						<option value="BI" {{ (old('company_country', auth()->user()->profile->company_country) == 'BI') ? 'selected' : '' }}>{{ __('Burundi') }}</option>
						<option value="BT" {{ (old('company_country', auth()->user()->profile->company_country) == 'BT') ? 'selected' : '' }}>{{ __('Bután') }}</option>
						<option value="CV" {{ (old('company_country', auth()->user()->profile->company_country) == 'CV') ? 'selected' : '' }}>{{ __('Cabo Verde') }}</option>
						<option value="KH" {{ (old('company_country', auth()->user()->profile->company_country) == 'KH') ? 'selected' : '' }}>{{ __('Camboya') }}</option>
						<option value="CM" {{ (old('company_country', auth()->user()->profile->company_country) == 'CM') ? 'selected' : '' }}>{{ __('Camerún') }}</option>
						<option value="CA" {{ (old('company_country', auth()->user()->profile->company_country) == 'CA') ? 'selected' : '' }}>{{ __('Canadá') }}</option>
						<option value="TD" {{ (old('company_country', auth()->user()->profile->company_country) == 'TD') ? 'selected' : '' }}>{{ __('Chad') }}</option>
						<option value="CL" {{ (old('company_country', auth()->user()->profile->company_country) == 'CL') ? 'selected' : '' }}>{{ __('Chile') }}</option>
						<option value="CN" {{ (old('company_country', auth()->user()->profile->company_country) == 'CN') ? 'selected' : '' }}>{{ __('China') }}</option>
						<option value="CY" {{ (old('company_country', auth()->user()->profile->company_country) == 'CY') ? 'selected' : '' }}>{{ __('Chipre') }}</option>
						<option value="VA" {{ (old('company_country', auth()->user()->profile->company_country) == 'VA') ? 'selected' : '' }}>{{ __('Ciudad del Vaticano') }}</option>
						<option value="CO" {{ (old('company_country', auth()->user()->profile->company_country) == 'CO') ? 'selected' : '' }}>{{ __('Colombia') }}</option>
						<option value="KM" {{ (old('company_country', auth()->user()->profile->company_country) == 'KM') ? 'selected' : '' }}>{{ __('Comores') }}</option>
						<option value="CG" {{ (old('company_country', auth()->user()->profile->company_country) == 'CG') ? 'selected' : '' }}>{{ __('Congo') }}</option>
						<option value="CD" {{ (old('company_country', auth()->user()->profile->company_country) == 'CD') ? 'selected' : '' }}>{{ __('Congo, República Democrática de') }}</option>
						<option value="KR" {{ (old('company_country', auth()->user()->profile->company_country) == 'KR') ? 'selected' : '' }}>{{ __('Corea') }}</option>
						<option value="KP" {{ (old('company_country', auth()->user()->profile->company_country) == 'KP') ? 'selected' : '' }}>{{ __('Corea del Norte') }}</option>
						<option value="CI" {{ (old('company_country', auth()->user()->profile->company_country) == 'CI') ? 'selected' : '' }}>{{ __('Costa de Marfíl') }}</option>
						<option value="CR" {{ (old('company_country', auth()->user()->profile->company_country) == 'CR') ? 'selected' : '' }}>{{ __('Costa Rica') }}</option>
						<option value="HR" {{ (old('company_country', auth()->user()->profile->company_country) == 'HR') ? 'selected' : '' }}>{{ __('Croacia (Hrvatska)') }}</option>
						<option value="CU" {{ (old('company_country', auth()->user()->profile->company_country) == 'CU') ? 'selected' : '' }}>{{ __('Cuba') }}</option>
						<option value="DK" {{ (old('company_country', auth()->user()->profile->company_country) == 'DK') ? 'selected' : '' }}>{{ __('Dinamarca') }}</option>
						<option value="DJ" {{ (old('company_country', auth()->user()->profile->company_country) == 'DJ') ? 'selected' : '' }}>{{ __('Djibouti') }}</option>
						<option value="DM" {{ (old('company_country', auth()->user()->profile->company_country) == 'DM') ? 'selected' : '' }}>{{ __('Dominica') }}</option>
						<option value="EC" {{ (old('company_country', auth()->user()->profile->company_country) == 'EC') ? 'selected' : '' }}>{{ __('Ecuador') }}</option>
						<option value="EG" {{ (old('company_country', auth()->user()->profile->company_country) == 'EG') ? 'selected' : '' }}>{{ __('Egipto') }}</option>
						<option value="SV" {{ (old('company_country', auth()->user()->profile->company_country) == 'SV') ? 'selected' : '' }}>{{ __('El Salvador') }}</option>
						<option value="AE" {{ (old('company_country', auth()->user()->profile->company_country) == 'AE') ? 'selected' : '' }}>{{ __('Emiratos Árabes Unidos') }}</option>
						<option value="ER" {{ (old('company_country', auth()->user()->profile->company_country) == 'ER') ? 'selected' : '' }}>{{ __('Eritrea') }}</option>
						<option value="SI" {{ (old('company_country', auth()->user()->profile->company_country) == 'SI') ? 'selected' : '' }}>{{ __('Eslovenia') }}</option>
						<option value="US" {{ (old('company_country', auth()->user()->profile->company_country) == 'US') ? 'selected' : '' }}>{{ __('Estados Unidos') }}</option>
						<option value="EE" {{ (old('company_country', auth()->user()->profile->company_country) == 'EE') ? 'selected' : '' }}>{{ __('Estonia') }}</option>
						<option value="ET" {{ (old('company_country', auth()->user()->profile->company_country) == 'ET') ? 'selected' : '' }}>{{ __('Etiopía') }}</option>
						<option value="FJ" {{ (old('company_country', auth()->user()->profile->company_country) == 'FJ') ? 'selected' : '' }}>{{ __('Fiji') }}</option>
						<option value="PH" {{ (old('company_country', auth()->user()->profile->company_country) == 'PH') ? 'selected' : '' }}>{{ __('Filipinas') }}</option>
						<option value="FI" {{ (old('company_country', auth()->user()->profile->company_country) == 'FI') ? 'selected' : '' }}>{{ __('Finlandia') }}</option>
						<option value="FR" {{ (old('company_country', auth()->user()->profile->company_country) == 'FR') ? 'selected' : '' }}>{{ __('Francia') }}</option>
						<option value="GA" {{ (old('company_country', auth()->user()->profile->company_country) == 'GA') ? 'selected' : '' }}>{{ __('Gabón') }}</option>
						<option value="GM" {{ (old('company_country', auth()->user()->profile->company_country) == 'GM') ? 'selected' : '' }}>{{ __('Gambia') }}</option>
						<option value="GE" {{ (old('company_country', auth()->user()->profile->company_country) == 'GE') ? 'selected' : '' }}>{{ __('Georgia') }}</option>
						<option value="GH" {{ (old('company_country', auth()->user()->profile->company_country) == 'GH') ? 'selected' : '' }}>{{ __('Ghana') }}</option>
						<option value="GI" {{ (old('company_country', auth()->user()->profile->company_country) == 'GI') ? 'selected' : '' }}>{{ __('Gibraltar') }}</option>
						<option value="GD" {{ (old('company_country', auth()->user()->profile->company_country) == 'GD') ? 'selected' : '' }}>{{ __('Granada') }}</option>
						<option value="GR" {{ (old('company_country', auth()->user()->profile->company_country) == 'GR') ? 'selected' : '' }}>{{ __('Grecia') }}</option>
						<option value="GL" {{ (old('company_country', auth()->user()->profile->company_country) == 'GL') ? 'selected' : '' }}>{{ __('Groenlandia') }}</option>
						<option value="GP" {{ (old('company_country', auth()->user()->profile->company_country) == 'GP') ? 'selected' : '' }}>{{ __('Guadalupe') }}</option>
						<option value="GU" {{ (old('company_country', auth()->user()->profile->company_country) == 'GU') ? 'selected' : '' }}>{{ __('Guam') }}</option>
						<option value="GT" {{ (old('company_country', auth()->user()->profile->company_country) == 'GT') ? 'selected' : '' }}>{{ __('Guatemala') }}</option>
						<option value="GY" {{ (old('company_country', auth()->user()->profile->company_country) == 'GY') ? 'selected' : '' }}>{{ __('Guayana') }}</option>
						<option value="GF" {{ (old('company_country', auth()->user()->profile->company_country) == 'GF') ? 'selected' : '' }}>{{ __('Guayana Francesa') }}</option>
						<option value="GN" {{ (old('company_country', auth()->user()->profile->company_country) == 'GN') ? 'selected' : '' }}>{{ __('Guinea') }}</option>
						<option value="GQ" {{ (old('company_country', auth()->user()->profile->company_country) == 'GQ') ? 'selected' : '' }}>{{ __('Guinea Ecuatorial') }}</option>
						<option value="GW" {{ (old('company_country', auth()->user()->profile->company_country) == 'GW') ? 'selected' : '' }}>{{ __('Guinea-Bissau') }}</option>
						<option value="HT" {{ (old('company_country', auth()->user()->profile->company_country) == 'HT') ? 'selected' : '' }}>{{ __('Haití') }}</option>
						<option value="HN" {{ (old('company_country', auth()->user()->profile->company_country) == 'HN') ? 'selected' : '' }}>{{ __('Honduras') }}</option>
						<option value="HU" {{ (old('company_country', auth()->user()->profile->company_country) == 'HU') ? 'selected' : '' }}>{{ __('Hungría') }}</option>
						<option value="IN" {{ (old('company_country', auth()->user()->profile->company_country) == 'IN') ? 'selected' : '' }}>{{ __('India') }}</option>
						<option value="ID" {{ (old('company_country', auth()->user()->profile->company_country) == 'ID') ? 'selected' : '' }}>{{ __('Indonesia') }}</option>
						<option value="IQ" {{ (old('company_country', auth()->user()->profile->company_country) == 'IQ') ? 'selected' : '' }}>{{ __('Irak') }}</option>
						<option value="IR" {{ (old('company_country', auth()->user()->profile->company_country) == 'IR') ? 'selected' : '' }}>{{ __('Irán') }}</option>
						<option value="IE" {{ (old('company_country', auth()->user()->profile->company_country) == 'IE') ? 'selected' : '' }}>{{ __('Irlanda') }}</option>
						<option value="BV" {{ (old('company_country', auth()->user()->profile->company_country) == 'BV') ? 'selected' : '' }}>{{ __('Isla Bouvet') }}</option>
						<option value="CX" {{ (old('company_country', auth()->user()->profile->company_country) == 'CX') ? 'selected' : '' }}>{{ __('Isla de Christmas') }}</option>
						<option value="IS" {{ (old('company_country', auth()->user()->profile->company_country) == 'IS') ? 'selected' : '' }}>{{ __('Islandia') }}</option>
						<option value="KY" {{ (old('company_country', auth()->user()->profile->company_country) == 'KY') ? 'selected' : '' }}>{{ __('Islas Caimán') }}</option>
						<option value="CK" {{ (old('company_country', auth()->user()->profile->company_country) == 'CK') ? 'selected' : '' }}>{{ __('Islas Cook') }}</option>
						<option value="CC" {{ (old('company_country', auth()->user()->profile->company_country) == 'CC') ? 'selected' : '' }}>{{ __('Islas de Cocos o Keeling') }}</option>
						<option value="FO" {{ (old('company_country', auth()->user()->profile->company_country) == 'FO') ? 'selected' : '' }}>{{ __('Islas Faroe') }}</option>
						<option value="HM" {{ (old('company_country', auth()->user()->profile->company_country) == 'HM') ? 'selected' : '' }}>{{ __('Islas Heard y McDonald') }}</option>
						<option value="FK" {{ (old('company_country', auth()->user()->profile->company_country) == 'FK') ? 'selected' : '' }}>{{ __('Islas Malvinas') }}</option>
						<option value="MP" {{ (old('company_country', auth()->user()->profile->company_country) == 'MP') ? 'selected' : '' }}>{{ __('Islas Marianas del Norte') }}</option>
						<option value="MH" {{ (old('company_country', auth()->user()->profile->company_country) == 'MH') ? 'selected' : '' }}>{{ __('Islas Marshall') }}</option>
						<option value="UM" {{ (old('company_country', auth()->user()->profile->company_country) == 'UM') ? 'selected' : '' }}>{{ __('Islas menores de Estados Unido') }}</option>
						<option value="PW" {{ (old('company_country', auth()->user()->profile->company_country) == 'PW') ? 'selected' : '' }}>{{ __('Islas Palau') }}</option>
						<option value="SB" {{ (old('company_country', auth()->user()->profile->company_country) == 'SB') ? 'selected' : '' }}>{{ __('Islas Salomón') }}</option>
						<option value="SJ" {{ (old('company_country', auth()->user()->profile->company_country) == 'SJ') ? 'selected' : '' }}>{{ __('Islas Svalbard y Jan Mayen') }}</option>
						<option value="TK" {{ (old('company_country', auth()->user()->profile->company_country) == 'TK') ? 'selected' : '' }}>{{ __('Islas Tokelau') }}</option>
						<option value="TC" {{ (old('company_country', auth()->user()->profile->company_country) == 'TC') ? 'selected' : '' }}>{{ __('Islas Turks y Caicos') }}</option>
						<option value="VI" {{ (old('company_country', auth()->user()->profile->company_country) == 'VI') ? 'selected' : '' }}>{{ __('Islas Vírgenes (EEUU)') }}</option>
						<option value="VG" {{ (old('company_country', auth()->user()->profile->company_country) == 'VG') ? 'selected' : '' }}>{{ __('Islas Vírgenes (Reino Unido)') }}</option>
						<option value="WF" {{ (old('company_country', auth()->user()->profile->company_country) == 'WF') ? 'selected' : '' }}>{{ __('Islas Wallis y Futuna') }}</option>
						<option value="IL" {{ (old('company_country', auth()->user()->profile->company_country) == 'IL') ? 'selected' : '' }}>{{ __('Israel') }}</option>
						<option value="IT" {{ (old('company_country', auth()->user()->profile->company_country) == 'IT') ? 'selected' : '' }}>{{ __('Italia') }}</option>
						<option value="JM" {{ (old('company_country', auth()->user()->profile->company_country) == 'JM') ? 'selected' : '' }}>{{ __('Jamaica') }}</option>
						<option value="JP" {{ (old('company_country', auth()->user()->profile->company_country) == 'JP') ? 'selected' : '' }}>{{ __('Japón') }}</option>
						<option value="JO" {{ (old('company_country', auth()->user()->profile->company_country) == 'JO') ? 'selected' : '' }}>{{ __('Jordania') }}</option>
						<option value="KZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'KZ') ? 'selected' : '' }}>{{ __('Kazajistán') }}</option>
						<option value="KE" {{ (old('company_country', auth()->user()->profile->company_country) == 'KE') ? 'selected' : '' }}>{{ __('Kenia') }}</option>
						<option value="KG" {{ (old('company_country', auth()->user()->profile->company_country) == 'KG') ? 'selected' : '' }}>{{ __('Kirguizistán') }}</option>
						<option value="KI" {{ (old('company_country', auth()->user()->profile->company_country) == 'KI') ? 'selected' : '' }}>{{ __('Kiribati') }}</option>
						<option value="KW" {{ (old('company_country', auth()->user()->profile->company_country) == 'KW') ? 'selected' : '' }}>{{ __('Kuwait') }}</option>
						<option value="LA" {{ (old('company_country', auth()->user()->profile->company_country) == 'LA') ? 'selected' : '' }}>{{ __('Laos') }}</option>
						<option value="LS" {{ (old('company_country', auth()->user()->profile->company_country) == 'LS') ? 'selected' : '' }}>{{ __('Lesotho') }}</option>
						<option value="LV" {{ (old('company_country', auth()->user()->profile->company_country) == 'LV') ? 'selected' : '' }}>{{ __('Letonia') }}</option>
						<option value="LB" {{ (old('company_country', auth()->user()->profile->company_country) == 'LB') ? 'selected' : '' }}>{{ __('Líbano') }}</option>
						<option value="LR" {{ (old('company_country', auth()->user()->profile->company_country) == 'LR') ? 'selected' : '' }}>{{ __('Liberia') }}</option>
						<option value="LY" {{ (old('company_country', auth()->user()->profile->company_country) == 'LY') ? 'selected' : '' }}>{{ __('Libia') }}</option>
						<option value="LI" {{ (old('company_country', auth()->user()->profile->company_country) == 'LI') ? 'selected' : '' }}>{{ __('Liechtenstein') }}</option>
						<option value="LT" {{ (old('company_country', auth()->user()->profile->company_country) == 'LT') ? 'selected' : '' }}>{{ __('Lituania') }}</option>
						<option value="LU" {{ (old('company_country', auth()->user()->profile->company_country) == 'LU') ? 'selected' : '' }}>{{ __('Luxemburgo') }}</option>
						<option value="MK" {{ (old('company_country', auth()->user()->profile->company_country) == 'MK') ? 'selected' : '' }}>{{ __('Macedonia') }}</option>
						<option value="MG" {{ (old('company_country', auth()->user()->profile->company_country) == 'MG') ? 'selected' : '' }}>{{ __('Madagascar') }}</option>
						<option value="MY" {{ (old('company_country', auth()->user()->profile->company_country) == 'MY') ? 'selected' : '' }}>{{ __('Malasia') }}</option>
						<option value="MW" {{ (old('company_country', auth()->user()->profile->company_country) == 'MW') ? 'selected' : '' }}>{{ __('Malawi') }}</option>
						<option value="MV" {{ (old('company_country', auth()->user()->profile->company_country) == 'MV') ? 'selected' : '' }}>{{ __('Maldivas') }}</option>
						<option value="ML" {{ (old('company_country', auth()->user()->profile->company_country) == 'ML') ? 'selected' : '' }}>{{ __('Malí') }}</option>
						<option value="MT" {{ (old('company_country', auth()->user()->profile->company_country) == 'MT') ? 'selected' : '' }}>{{ __('Malta') }}</option>
						<option value="MA" {{ (old('company_country', auth()->user()->profile->company_country) == 'MA') ? 'selected' : '' }}>{{ __('Marruecos') }}</option>
						<option value="MQ" {{ (old('company_country', auth()->user()->profile->company_country) == 'MQ') ? 'selected' : '' }}>{{ __('Martinica') }}</option>
						<option value="MU" {{ (old('company_country', auth()->user()->profile->company_country) == 'MU') ? 'selected' : '' }}>{{ __('Mauricio') }}</option>
						<option value="MR" {{ (old('company_country', auth()->user()->profile->company_country) == 'MR') ? 'selected' : '' }}>{{ __('Mauritania') }}</option>
						<option value="YT" {{ (old('company_country', auth()->user()->profile->company_country) == 'YT') ? 'selected' : '' }}>{{ __('Mayotte') }}</option>
						<option value="MX" {{ (old('company_country', auth()->user()->profile->company_country) == 'MX') ? 'selected' : '' }}>{{ __('México') }}</option>
						<option value="FM" {{ (old('company_country', auth()->user()->profile->company_country) == 'FM') ? 'selected' : '' }}>{{ __('Micronesia') }}</option>
						<option value="MD" {{ (old('company_country', auth()->user()->profile->company_country) == 'MD') ? 'selected' : '' }}>{{ __('Moldavia') }}</option>
						<option value="MC" {{ (old('company_country', auth()->user()->profile->company_country) == 'MC') ? 'selected' : '' }}>{{ __('Mónaco') }}</option>
						<option value="MN" {{ (old('company_country', auth()->user()->profile->company_country) == 'MN') ? 'selected' : '' }}>{{ __('Mongolia') }}</option>
						<option value="MS" {{ (old('company_country', auth()->user()->profile->company_country) == 'MS') ? 'selected' : '' }}>{{ __('Montserrat') }}</option>
						<option value="MZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'MZ') ? 'selected' : '' }}>{{ __('Mozambique') }}</option>
						<option value="NA" {{ (old('company_country', auth()->user()->profile->company_country) == 'NA') ? 'selected' : '' }}>{{ __('Namibia') }}</option>
						<option value="NR" {{ (old('company_country', auth()->user()->profile->company_country) == 'NR') ? 'selected' : '' }}>{{ __('Nauru') }}</option>
						<option value="NP" {{ (old('company_country', auth()->user()->profile->company_country) == 'NP') ? 'selected' : '' }}>{{ __('Nepal') }}</option>
						<option value="NI" {{ (old('company_country', auth()->user()->profile->company_country) == 'NI') ? 'selected' : '' }}>{{ __('Nicaragua') }}</option>
						<option value="NE" {{ (old('company_country', auth()->user()->profile->company_country) == 'NE') ? 'selected' : '' }}>{{ __('Níger') }}</option>
						<option value="NG" {{ (old('company_country', auth()->user()->profile->company_country) == 'NG') ? 'selected' : '' }}>{{ __('Nigeria') }}</option>
						<option value="NU" {{ (old('company_country', auth()->user()->profile->company_country) == 'NU') ? 'selected' : '' }}>{{ __('Niue') }}</option>
						<option value="NF" {{ (old('company_country', auth()->user()->profile->company_country) == 'NF') ? 'selected' : '' }}>{{ __('Norfolk') }}</option>
						<option value="NO" {{ (old('company_country', auth()->user()->profile->company_country) == 'NO') ? 'selected' : '' }}>{{ __('Noruega') }}</option>
						<option value="NC" {{ (old('company_country', auth()->user()->profile->company_country) == 'NC') ? 'selected' : '' }}>{{ __('Nueva Caledonia') }}</option>
						<option value="NZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'NZ') ? 'selected' : '' }}>{{ __('Nueva Zelanda') }}</option>
						<option value="OM" {{ (old('company_country', auth()->user()->profile->company_country) == 'OM') ? 'selected' : '' }}>{{ __('Omán') }}</option>
						<option value="NL" {{ (old('company_country', auth()->user()->profile->company_country) == 'NL') ? 'selected' : '' }}>{{ __('Países Bajos') }}</option>
						<option value="PA" {{ (old('company_country', auth()->user()->profile->company_country) == 'PA') ? 'selected' : '' }}>{{ __('Panamá') }}</option>
						<option value="PG" {{ (old('company_country', auth()->user()->profile->company_country) == 'PG') ? 'selected' : '' }}>{{ __('Papúa Nueva Guinea') }}</option>
						<option value="PK" {{ (old('company_country', auth()->user()->profile->company_country) == 'PK') ? 'selected' : '' }}>{{ __('Paquistán') }}</option>
						<option value="PY" {{ (old('company_country', auth()->user()->profile->company_country) == 'PY') ? 'selected' : '' }}>{{ __('Paraguay') }}</option>
						<option value="PE" {{ (old('company_country', auth()->user()->profile->company_country) == 'PE') ? 'selected' : '' }}>{{ __('Perú') }}</option>
						<option value="PN" {{ (old('company_country', auth()->user()->profile->company_country) == 'PN') ? 'selected' : '' }}>{{ __('Pitcairn') }}</option>
						<option value="PF" {{ (old('company_country', auth()->user()->profile->company_country) == 'PF') ? 'selected' : '' }}>{{ __('Polinesia Francesa') }}</option>
						<option value="PL" {{ (old('company_country', auth()->user()->profile->company_country) == 'PL') ? 'selected' : '' }}>{{ __('Polonia') }}</option>
						<option value="PT" {{ (old('company_country', auth()->user()->profile->company_country) == 'PT') ? 'selected' : '' }}>{{ __('Portugal') }}</option>
						<option value="PR" {{ (old('company_country', auth()->user()->profile->company_country) == 'PR') ? 'selected' : '' }}>{{ __('Puerto Rico') }}</option>
						<option value="QA" {{ (old('company_country', auth()->user()->profile->company_country) == 'QA') ? 'selected' : '' }}>{{ __('Qatar') }}</option>
						<option value="GB" {{ (old('company_country', auth()->user()->profile->company_country) == 'GB') ? 'selected' : '' }}>{{ __('Reino Unido') }}</option>
						<option value="CF" {{ (old('company_country', auth()->user()->profile->company_country) == 'CF') ? 'selected' : '' }}>{{ __('República Centroafricana') }}</option>
						<option value="CZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'CZ') ? 'selected' : '' }}>{{ __('República Checa') }}</option>
						<option value="ZA" {{ (old('company_country', auth()->user()->profile->company_country) == 'ZA') ? 'selected' : '' }}>{{ __('República de Sudáfrica') }}</option>
						<option value="DO" {{ (old('company_country', auth()->user()->profile->company_country) == 'DO') ? 'selected' : '' }}>{{ __('República Dominicana') }}</option>
						<option value="SK" {{ (old('company_country', auth()->user()->profile->company_country) == 'SK') ? 'selected' : '' }}>{{ __('República Eslovaca') }}</option>
						<option value="RE" {{ (old('company_country', auth()->user()->profile->company_country) == 'RE') ? 'selected' : '' }}>{{ __('Reunión') }}</option>
						<option value="RW" {{ (old('company_country', auth()->user()->profile->company_country) == 'RW') ? 'selected' : '' }}>{{ __('Ruanda') }}</option>
						<option value="RO" {{ (old('company_country', auth()->user()->profile->company_country) == 'RO') ? 'selected' : '' }}>{{ __('Rumania') }}</option>
						<option value="RU" {{ (old('company_country', auth()->user()->profile->company_country) == 'RU') ? 'selected' : '' }}>{{ __('Rusia') }}</option>
						<option value="EH" {{ (old('company_country', auth()->user()->profile->company_country) == 'EH') ? 'selected' : '' }}>{{ __('Sahara Occidental') }}</option>
						<option value="KN" {{ (old('company_country', auth()->user()->profile->company_country) == 'KN') ? 'selected' : '' }}>{{ __('Saint Kitts y Nevis') }}</option>
						<option value="WS" {{ (old('company_country', auth()->user()->profile->company_country) == 'WS') ? 'selected' : '' }}>{{ __('Samoa') }}</option>
						<option value="AS" {{ (old('company_country', auth()->user()->profile->company_country) == 'AS') ? 'selected' : '' }}>{{ __('Samoa Americana') }}</option>
						<option value="SM" {{ (old('company_country', auth()->user()->profile->company_country) == 'SM') ? 'selected' : '' }}>{{ __('San Marino') }}</option>
						<option value="VC" {{ (old('company_country', auth()->user()->profile->company_country) == 'VC') ? 'selected' : '' }}>{{ __('San Vicente y Granadinas') }}</option>
						<option value="SH" {{ (old('company_country', auth()->user()->profile->company_country) == 'SH') ? 'selected' : '' }}>{{ __('Santa Helena') }}</option>
						<option value="LC" {{ (old('company_country', auth()->user()->profile->company_country) == 'LC') ? 'selected' : '' }}>{{ __('Santa Lucía') }}</option>
						<option value="ST" {{ (old('company_country', auth()->user()->profile->company_country) == 'ST') ? 'selected' : '' }}>{{ __('Santo Tomé y Príncipe') }}</option>
						<option value="SN" {{ (old('company_country', auth()->user()->profile->company_country) == 'SN') ? 'selected' : '' }}>{{ __('Senegal') }}</option>
						<option value="SC" {{ (old('company_country', auth()->user()->profile->company_country) == 'SC') ? 'selected' : '' }}>{{ __('Seychelles') }}</option>
						<option value="SL" {{ (old('company_country', auth()->user()->profile->company_country) == 'SL') ? 'selected' : '' }}>{{ __('Sierra Leona') }}</option>
						<option value="SG" {{ (old('company_country', auth()->user()->profile->company_country) == 'SG') ? 'selected' : '' }}>{{ __('Singapur') }}</option>
						<option value="SY" {{ (old('company_country', auth()->user()->profile->company_country) == 'SY') ? 'selected' : '' }}>{{ __('Siria') }}</option>
						<option value="SO" {{ (old('company_country', auth()->user()->profile->company_country) == 'SO') ? 'selected' : '' }}>{{ __('Somalia') }}</option>
						<option value="LK" {{ (old('company_country', auth()->user()->profile->company_country) == 'LK') ? 'selected' : '' }}>{{ __('Sri Lanka') }}</option>
						<option value="PM" {{ (old('company_country', auth()->user()->profile->company_country) == 'PM') ? 'selected' : '' }}>{{ __('St Pierre y Miquelon') }}</option>
						<option value="SZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'SZ') ? 'selected' : '' }}>{{ __('Suazilandia') }}</option>
						<option value="SD" {{ (old('company_country', auth()->user()->profile->company_country) == 'SD') ? 'selected' : '' }}>{{ __('Sudán') }}</option>
						<option value="SE" {{ (old('company_country', auth()->user()->profile->company_country) == 'SE') ? 'selected' : '' }}>{{ __('Suecia') }}</option>
						<option value="CH" {{ (old('company_country', auth()->user()->profile->company_country) == 'CH') ? 'selected' : '' }}>{{ __('Suiza') }}</option>
						<option value="SR" {{ (old('company_country', auth()->user()->profile->company_country) == 'SR') ? 'selected' : '' }}>{{ __('Surinam') }}</option>
						<option value="TH" {{ (old('company_country', auth()->user()->profile->company_country) == 'TH') ? 'selected' : '' }}>{{ __('Tailandia') }}</option>
						<option value="TW" {{ (old('company_country', auth()->user()->profile->company_country) == 'TW') ? 'selected' : '' }}>{{ __('Taiwán') }}</option>
						<option value="TZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'TZ') ? 'selected' : '' }}>{{ __('Tanzania') }}</option>
						<option value="TJ" {{ (old('company_country', auth()->user()->profile->company_country) == 'TJ') ? 'selected' : '' }}>{{ __('Tayikistán') }}</option>
						<option value="TF" {{ (old('company_country', auth()->user()->profile->company_country) == 'TF') ? 'selected' : '' }}>{{ __('Territorios franceses del Sur') }}</option>
						<option value="TP" {{ (old('company_country', auth()->user()->profile->company_country) == 'TP') ? 'selected' : '' }}>{{ __('Timor Oriental') }}</option>
						<option value="TG" {{ (old('company_country', auth()->user()->profile->company_country) == 'TG') ? 'selected' : '' }}>{{ __('Togo') }}</option>
						<option value="TO" {{ (old('company_country', auth()->user()->profile->company_country) == 'TO') ? 'selected' : '' }}>{{ __('Tonga') }}</option>
						<option value="TT" {{ (old('company_country', auth()->user()->profile->company_country) == 'TT') ? 'selected' : '' }}>{{ __('Trinidad y Tobago') }}</option>
						<option value="TN" {{ (old('company_country', auth()->user()->profile->company_country) == 'TN') ? 'selected' : '' }}>{{ __('Túnez') }}</option>
						<option value="TM" {{ (old('company_country', auth()->user()->profile->company_country) == 'TM') ? 'selected' : '' }}>{{ __('Turkmenistán') }}</option>
						<option value="TR" {{ (old('company_country', auth()->user()->profile->company_country) == 'TR') ? 'selected' : '' }}>{{ __('Turquía') }}</option>
						<option value="TV" {{ (old('company_country', auth()->user()->profile->company_country) == 'TV') ? 'selected' : '' }}>{{ __('Tuvalu') }}</option>
						<option value="UA" {{ (old('company_country', auth()->user()->profile->company_country) == 'UA') ? 'selected' : '' }}>{{ __('Ucrania') }}</option>
						<option value="UG" {{ (old('company_country', auth()->user()->profile->company_country) == 'UG') ? 'selected' : '' }}>{{ __('Uganda') }}</option>
						<option value="UY" {{ (old('company_country', auth()->user()->profile->company_country) == 'UY') ? 'selected' : '' }}>{{ __('Uruguay') }}</option>
						<option value="UZ" {{ (old('company_country', auth()->user()->profile->company_country) == 'UZ') ? 'selected' : '' }}>{{ __('Uzbekistán') }}</option>
						<option value="VU" {{ (old('company_country', auth()->user()->profile->company_country) == 'VU') ? 'selected' : '' }}>{{ __('Vanuatu') }}</option>
						<option value="VE" {{ (old('company_country', auth()->user()->profile->company_country) == 'VE') ? 'selected' : '' }}>{{ __('Venezuela') }}</option>
						<option value="VN" {{ (old('company_country', auth()->user()->profile->company_country) == 'VN') ? 'selected' : '' }}>{{ __('Vietnam') }}</option>
						<option value="YE" {{ (old('company_country', auth()->user()->profile->company_country) == 'YE') ? 'selected' : '' }}>{{ __('Yemen') }}</option>
						<option value="YU" {{ (old('company_country', auth()->user()->profile->company_country) == 'YU') ? 'selected' : '' }}>{{ __('Yugoslavia') }}</option>
						<option value="ZM" {{ (old('company_country', auth()->user()->profile->company_country) == 'ZM') ? 'selected' : '' }}>{{ __('Zambia') }}</option>
						<option value="ZW" {{ (old('company_country', auth()->user()->profile->company_country) == 'ZW') ? 'selected' : '' }}>{{ __('Zimbabue') }}</option>
					</select>
				</div>
			</div>
		</div>
		{{-- FIN DATOS EMPRESA --}}


		{{-- #DIRECCION --}}
		<div class="xs-4 col  mt-24">
			<h4 class="c-2" v-if="accountType == 2">{{ __('Dirección de la empresa') }}</h4>
			<h4 class="c-2" v-else>{{ __('Dirección') }}</h4>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Dirección') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" type="text" name="direccion" value="{{ old('direccion', auth()->user()->profile->direccion) }}" placeholder="{{ __('Dirección') }}" required {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Localidad') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" type="text" name="localidad" value="{{ old('localidad', auth()->user()->profile->localidad) }}" placeholder="{{ __('Localidad') }}" required {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Código postal') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<input class="custom-input" name="codigo_postal" value="{{ old('codigo_postal', auth()->user()->profile->codigo_postal) }}" placeholder="{{ __('Código postal') }}" required {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Provincia') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="provincia" id="provincia" required>

					<option value="" {{ (old('provincia', auth()->user()->profile->provincia) == null) ? 'selected' : '' }}>{{ __('Selecciona tu provincia') }}</option>
					<option disabled>_________</option>

					<option value="NA" {{ (old('provincia', auth()->user()->profile->provincia) == 'NA') ? 'selected' : '' }}>{{ __('N/A') }}
					</option>

					<option disabled>_________</option>
					
					<option value='Alava' {{ (old('provincia', auth()->user()->profile->provincia) == 'Alava') ? 'selected' : '' }}>Álava</option>
					<option value='Albacete' {{ (old('provincia', auth()->user()->profile->provincia) == 'Albacete') ? 'selected' : '' }}>Albacete</option>
					<option value='Alicante' {{ (old('provincia', auth()->user()->profile->provincia) == 'Alicante') ? 'selected' : '' }}>Alicante/Alacant</option>
					<option value='Almeria' {{ (old('provincia', auth()->user()->profile->provincia) == 'Almeria') ? 'selected' : '' }}>Almería</option>
					<option value='Asturias' {{ (old('provincia', auth()->user()->profile->provincia) == 'Asturias') ? 'selected' : '' }}>Asturias</option>
					<option value='Avila' {{ (old('provincia', auth()->user()->profile->provincia) == 'Avila') ? 'selected' : '' }}>Ávila</option>
					<option value='Badajoz' {{ (old('provincia', auth()->user()->profile->provincia) == 'Badajoz') ? 'selected' : '' }}>Badajoz</option>
					<option value='Barcelona' {{ (old('provincia', auth()->user()->profile->provincia) == 'Barcelona') ? 'selected' : '' }}>Barcelona</option>
					<option value='Burgos' {{ (old('provincia', auth()->user()->profile->provincia) == 'Burgos') ? 'selected' : '' }}>Burgos</option>
					<option value='Caceres' {{ (old('provincia', auth()->user()->profile->provincia) == 'Caceres') ? 'selected' : '' }}>Cáceres</option>
					<option value='Cadiz' {{ (old('provincia', auth()->user()->profile->provincia) == 'Cadiz') ? 'selected' : '' }}>Cádiz</option>
					<option value='Cantabria' {{ (old('provincia', auth()->user()->profile->provincia) == 'Cantabria') ? 'selected' : '' }}>Cantabria</option>
					<option value='Castellon' {{ (old('provincia', auth()->user()->profile->provincia) == 'Castellon') ? 'selected' : '' }}>Castellón/Castelló</option>
					<option value='Ceuta' {{ (old('provincia', auth()->user()->profile->provincia) == 'Ceuta') ? 'selected' : '' }}>Ceuta</option>
					<option value='Ciudadreal' {{ (old('provincia', auth()->user()->profile->provincia) == 'Ciudadreal') ? 'selected' : '' }}>Ciudad Real</option>
					<option value='Cordoba' {{ (old('provincia', auth()->user()->profile->provincia) == 'Cordoba') ? 'selected' : '' }}>Córdoba</option>
					<option value='Cuenca' {{ (old('provincia', auth()->user()->profile->provincia) == 'Cuenca') ? 'selected' : '' }}>Cuenca</option>
					<option value='Girona' {{ (old('provincia', auth()->user()->profile->provincia) == 'Girona') ? 'selected' : '' }}>Girona</option>
					<option value='LasPalmas' {{ (old('provincia', auth()->user()->profile->provincia) == 'LasPalmas') ? 'selected' : '' }}>Las Palmas</option>
					<option value='Granada' {{ (old('provincia', auth()->user()->profile->provincia) == 'Granada') ? 'selected' : '' }}>Granada</option>
					<option value='Guadalajara' {{ (old('provincia', auth()->user()->profile->provincia) == 'Guadalajara') ? 'selected' : '' }}>Guadalajara</option>
					<option value='Guipuzcoa' {{ (old('provincia', auth()->user()->profile->provincia) == 'Guipuzcoa') ? 'selected' : '' }}>Guipúzcoa</option>
					<option value='Huelva' {{ (old('provincia', auth()->user()->profile->provincia) == 'Huelva') ? 'selected' : '' }}>Huelva</option>
					<option value='Huesca' {{ (old('provincia', auth()->user()->profile->provincia) == 'Huesca') ? 'selected' : '' }}>Huesca</option>
					<option value='IllesBalears' {{ (old('provincia', auth()->user()->profile->provincia) == 'IllesBalears') ? 'selected' : '' }}>Illes Balears</option>
					<option value='Jaen' {{ (old('provincia', auth()->user()->profile->provincia) == 'Jaen') ? 'selected' : '' }}>Jaén</option>
					<option value='Acoruña' {{ (old('provincia', auth()->user()->profile->provincia) == 'Acoruña') ? 'selected' : '' }}>A Coruña</option>
					<option value='Larioja' {{ (old('provincia', auth()->user()->profile->provincia) == 'Larioja') ? 'selected' : '' }}>La Rioja</option>
					<option value='Leon' {{ (old('provincia', auth()->user()->profile->provincia) == 'Leon') ? 'selected' : '' }}>León</option>
					<option value='Lleida' {{ (old('provincia', auth()->user()->profile->provincia) == 'Lleida') ? 'selected' : '' }}>Lleida</option>
					<option value='Lugo' {{ (old('provincia', auth()->user()->profile->provincia) == 'Lugo') ? 'selected' : '' }}>Lugo</option>
					<option value='Madrid' {{ (old('provincia', auth()->user()->profile->provincia) == 'Madrid') ? 'selected' : '' }}>Madrid</option>
					<option value='Malaga' {{ (old('provincia', auth()->user()->profile->provincia) == 'Malaga') ? 'selected' : '' }}>Málaga</option>
					<option value='Melilla' {{ (old('provincia', auth()->user()->profile->provincia) == 'Melilla') ? 'selected' : '' }}>Melilla</option>
					<option value='Murcia' {{ (old('provincia', auth()->user()->profile->provincia) == 'Murcia') ? 'selected' : '' }}>Murcia</option>
					<option value='Navarra' {{ (old('provincia', auth()->user()->profile->provincia) == 'Navarra') ? 'selected' : '' }}>Navarra</option>
					<option value='Ourense' {{ (old('provincia', auth()->user()->profile->provincia) == 'Ourense') ? 'selected' : '' }}>Ourense</option>
					<option value='Palencia' {{ (old('provincia', auth()->user()->profile->provincia) == 'Palencia') ? 'selected' : '' }}>Palencia</option>
					<option value='Pontevedra' {{ (old('provincia', auth()->user()->profile->provincia) == 'Pontevedra') ? 'selected' : '' }}>Pontevedra</option>
					<option value='Salamanca' {{ (old('provincia', auth()->user()->profile->provincia) == 'Salamanca') ? 'selected' : '' }}>Salamanca</option>
					<option value='Segovia' {{ (old('provincia', auth()->user()->profile->provincia) == 'Segovia') ? 'selected' : '' }}>Segovia</option>
					<option value='Sevilla' {{ (old('provincia', auth()->user()->profile->provincia) == 'Sevilla') ? 'selected' : '' }}>Sevilla</option>
					<option value='Soria' {{ (old('provincia', auth()->user()->profile->provincia) == 'Soria') ? 'selected' : '' }}>Soria</option>
					<option value='Tarragona' {{ (old('provincia', auth()->user()->profile->provincia) == 'Tarragona') ? 'selected' : '' }}>Tarragona</option>
					<option value='SantaCruzTenerife' {{ (old('provincia', auth()->user()->profile->provincia) == 'SantaCruzTenerife') ? 'selected' : '' }}>Santa Cruz de Tenerife</option>
					<option value='Teruel' {{ (old('provincia', auth()->user()->profile->provincia) == 'Teruel') ? 'selected' : '' }}>Teruel</option>
					<option value='Toledo' {{ (old('provincia', auth()->user()->profile->provincia) == 'Toledo') ? 'selected' : '' }}>Toledo</option>
					<option value='Valencia' {{ (old('provincia', auth()->user()->profile->provincia) == 'Valencia') ? 'selected' : '' }}>Valencia/Valéncia</option>
					<option value='Valladolid' {{ (old('provincia', auth()->user()->profile->provincia) == 'Valladolid') ? 'selected' : '' }}>Valladolid</option>
					<option value='Vizcaya' {{ (old('provincia', auth()->user()->profile->provincia) == 'Vizcaya') ? 'selected' : '' }}>Vizcaya</option>
					<option value='Zamora' {{ (old('provincia', auth()->user()->profile->provincia) == 'Zamora') ? 'selected' : '' }}>Zamora</option>
					<option value='Zaragoza' {{ (old('provincia', auth()->user()->profile->provincia) == 'Zaragoza') ? 'selected' : '' }}>Zaragoza</option>
				</select>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('País') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<select class="custom-input  custom-input--select {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" name="pais" id="pais" required>
					<option value="ES" {{ (old('pais', auth()->user()->profile->pais) == 'ES') ? 'selected' : '' }}>{{ __('España') }}</option>
					<option disabled>──────────</option>
					<option value="AF" {{ (old('pais', auth()->user()->profile->pais) == 'AF') ? 'selected' : '' }}></option>
					<option value="AL" {{ (old('pais', auth()->user()->profile->pais) == 'AL') ? 'selected' : '' }}>{{ __('Albania') }}</option>
					<option value="DE" {{ (old('pais', auth()->user()->profile->pais) == 'DE') ? 'selected' : '' }}>{{ __('Alemania') }}</option>
					<option value="AD" {{ (old('pais', auth()->user()->profile->pais) == 'AD') ? 'selected' : '' }}>{{ __('Andorra') }}</option>
					<option value="AO" {{ (old('pais', auth()->user()->profile->pais) == 'AO') ? 'selected' : '' }}>{{ __('Angola') }}</option>
					<option value="AI" {{ (old('pais', auth()->user()->profile->pais) == 'AI') ? 'selected' : '' }}>{{ __('Anguilla') }}</option>
					<option value="AQ" {{ (old('pais', auth()->user()->profile->pais) == 'AQ') ? 'selected' : '' }}>{{ __('Antártida') }}</option>
					<option value="AG" {{ (old('pais', auth()->user()->profile->pais) == 'AG') ? 'selected' : '' }}>{{ __('Antigua y Barbuda') }}</option>
					<option value="AN" {{ (old('pais', auth()->user()->profile->pais) == 'AN') ? 'selected' : '' }}>{{ __('Antillas Holandesas') }}</option>
					<option value="SA" {{ (old('pais', auth()->user()->profile->pais) == 'SA') ? 'selected' : '' }}>{{ __('Arabia Saudí') }}</option>
					<option value="DZ" {{ (old('pais', auth()->user()->profile->pais) == 'DZ') ? 'selected' : '' }}>{{ __('Argelia') }}</option>
					<option value="AR" {{ (old('pais', auth()->user()->profile->pais) == 'AR') ? 'selected' : '' }}>{{ __('Argentina') }}</option>
					<option value="AM" {{ (old('pais', auth()->user()->profile->pais) == 'AM') ? 'selected' : '' }}>{{ __('Armenia') }}</option>
					<option value="AW" {{ (old('pais', auth()->user()->profile->pais) == 'AW') ? 'selected' : '' }}>{{ __('Aruba') }}</option>
					<option value="AU" {{ (old('pais', auth()->user()->profile->pais) == 'AU') ? 'selected' : '' }}>{{ __('Australia') }}</option>
					<option value="AT" {{ (old('pais', auth()->user()->profile->pais) == 'AT') ? 'selected' : '' }}>{{ __('Austria') }}</option>
					<option value="AZ" {{ (old('pais', auth()->user()->profile->pais) == 'AZ') ? 'selected' : '' }}>{{ __('Azerbaiyán') }}</option>
					<option value="BS" {{ (old('pais', auth()->user()->profile->pais) == 'BS') ? 'selected' : '' }}>{{ __('Bahamas') }}</option>
					<option value="BH" {{ (old('pais', auth()->user()->profile->pais) == 'BH') ? 'selected' : '' }}>{{ __('Bahrein') }}</option>
					<option value="BD" {{ (old('pais', auth()->user()->profile->pais) == 'BD') ? 'selected' : '' }}>{{ __('Bangladesh') }}</option>
					<option value="BB" {{ (old('pais', auth()->user()->profile->pais) == 'BB') ? 'selected' : '' }}>{{ __('Barbados') }}</option>
					<option value="BE" {{ (old('pais', auth()->user()->profile->pais) == 'BE') ? 'selected' : '' }}>{{ __('Bélgica') }}</option>
					<option value="BZ" {{ (old('pais', auth()->user()->profile->pais) == 'BZ') ? 'selected' : '' }}>{{ __('Belice') }}</option>
					<option value="BJ" {{ (old('pais', auth()->user()->profile->pais) == 'BJ') ? 'selected' : '' }}>{{ __('Benin') }}</option>
					<option value="BM" {{ (old('pais', auth()->user()->profile->pais) == 'BM') ? 'selected' : '' }}>{{ __('Bermudas') }}</option>
					<option value="BY" {{ (old('pais', auth()->user()->profile->pais) == 'BY') ? 'selected' : '' }}>{{ __('Bielorrusia') }}</option>
					<option value="MM" {{ (old('pais', auth()->user()->profile->pais) == 'MM') ? 'selected' : '' }}>{{ __('Birmania') }}</option>
					<option value="BO" {{ (old('pais', auth()->user()->profile->pais) == 'BO') ? 'selected' : '' }}>{{ __('Bolivia') }}</option>
					<option value="BA" {{ (old('pais', auth()->user()->profile->pais) == 'BA') ? 'selected' : '' }}>{{ __('Bosnia y Herzegovina') }}</option>
					<option value="BW" {{ (old('pais', auth()->user()->profile->pais) == 'BW') ? 'selected' : '' }}>{{ __('Botswana') }}</option>
					<option value="BR" {{ (old('pais', auth()->user()->profile->pais) == 'BR') ? 'selected' : '' }}>{{ __('Brasil') }}</option>
					<option value="BN" {{ (old('pais', auth()->user()->profile->pais) == 'BN') ? 'selected' : '' }}>{{ __('Brunei') }}</option>
					<option value="BG" {{ (old('pais', auth()->user()->profile->pais) == 'BG') ? 'selected' : '' }}>{{ __('Bulgaria') }}</option>
					<option value="BF" {{ (old('pais', auth()->user()->profile->pais) == 'BF') ? 'selected' : '' }}>{{ __('Burkina Faso') }}</option>
					<option value="BI" {{ (old('pais', auth()->user()->profile->pais) == 'BI') ? 'selected' : '' }}>{{ __('Burundi') }}</option>
					<option value="BT" {{ (old('pais', auth()->user()->profile->pais) == 'BT') ? 'selected' : '' }}>{{ __('Bután') }}</option>
					<option value="CV" {{ (old('pais', auth()->user()->profile->pais) == 'CV') ? 'selected' : '' }}>{{ __('Cabo Verde') }}</option>
					<option value="KH" {{ (old('pais', auth()->user()->profile->pais) == 'KH') ? 'selected' : '' }}>{{ __('Camboya') }}</option>
					<option value="CM" {{ (old('pais', auth()->user()->profile->pais) == 'CM') ? 'selected' : '' }}>{{ __('Camerún') }}</option>
					<option value="CA" {{ (old('pais', auth()->user()->profile->pais) == 'CA') ? 'selected' : '' }}>{{ __('Canadá') }}</option>
					<option value="TD" {{ (old('pais', auth()->user()->profile->pais) == 'TD') ? 'selected' : '' }}>{{ __('Chad') }}</option>
					<option value="CL" {{ (old('pais', auth()->user()->profile->pais) == 'CL') ? 'selected' : '' }}>{{ __('Chile') }}</option>
					<option value="CN" {{ (old('pais', auth()->user()->profile->pais) == 'CN') ? 'selected' : '' }}>{{ __('China') }}</option>
					<option value="CY" {{ (old('pais', auth()->user()->profile->pais) == 'CY') ? 'selected' : '' }}>{{ __('Chipre') }}</option>
					<option value="VA" {{ (old('pais', auth()->user()->profile->pais) == 'VA') ? 'selected' : '' }}>{{ __('Ciudad del Vaticano') }}</option>
					<option value="CO" {{ (old('pais', auth()->user()->profile->pais) == 'CO') ? 'selected' : '' }}>{{ __('Colombia') }}</option>
					<option value="KM" {{ (old('pais', auth()->user()->profile->pais) == 'KM') ? 'selected' : '' }}>{{ __('Comores') }}</option>
					<option value="CG" {{ (old('pais', auth()->user()->profile->pais) == 'CG') ? 'selected' : '' }}>{{ __('Congo') }}</option>
					<option value="CD" {{ (old('pais', auth()->user()->profile->pais) == 'CD') ? 'selected' : '' }}>{{ __('Congo, República Democrática de') }}</option>
					<option value="KR" {{ (old('pais', auth()->user()->profile->pais) == 'KR') ? 'selected' : '' }}>{{ __('Corea') }}</option>
					<option value="KP" {{ (old('pais', auth()->user()->profile->pais) == 'KP') ? 'selected' : '' }}>{{ __('Corea del Norte') }}</option>
					<option value="CI" {{ (old('pais', auth()->user()->profile->pais) == 'CI') ? 'selected' : '' }}>{{ __('Costa de Marfíl') }}</option>
					<option value="CR" {{ (old('pais', auth()->user()->profile->pais) == 'CR') ? 'selected' : '' }}>{{ __('Costa Rica') }}</option>
					<option value="HR" {{ (old('pais', auth()->user()->profile->pais) == 'HR') ? 'selected' : '' }}>{{ __('Croacia (Hrvatska)') }}</option>
					<option value="CU" {{ (old('pais', auth()->user()->profile->pais) == 'CU') ? 'selected' : '' }}>{{ __('Cuba') }}</option>
					<option value="DK" {{ (old('pais', auth()->user()->profile->pais) == 'DK') ? 'selected' : '' }}>{{ __('Dinamarca') }}</option>
					<option value="DJ" {{ (old('pais', auth()->user()->profile->pais) == 'DJ') ? 'selected' : '' }}>{{ __('Djibouti') }}</option>
					<option value="DM" {{ (old('pais', auth()->user()->profile->pais) == 'DM') ? 'selected' : '' }}>{{ __('Dominica') }}</option>
					<option value="EC" {{ (old('pais', auth()->user()->profile->pais) == 'EC') ? 'selected' : '' }}>{{ __('Ecuador') }}</option>
					<option value="EG" {{ (old('pais', auth()->user()->profile->pais) == 'EG') ? 'selected' : '' }}>{{ __('Egipto') }}</option>
					<option value="SV" {{ (old('pais', auth()->user()->profile->pais) == 'SV') ? 'selected' : '' }}>{{ __('El Salvador') }}</option>
					<option value="AE" {{ (old('pais', auth()->user()->profile->pais) == 'AE') ? 'selected' : '' }}>{{ __('Emiratos Árabes Unidos') }}</option>
					<option value="ER" {{ (old('pais', auth()->user()->profile->pais) == 'ER') ? 'selected' : '' }}>{{ __('Eritrea') }}</option>
					<option value="SI" {{ (old('pais', auth()->user()->profile->pais) == 'SI') ? 'selected' : '' }}>{{ __('Eslovenia') }}</option>
					<option value="US" {{ (old('pais', auth()->user()->profile->pais) == 'US') ? 'selected' : '' }}>{{ __('Estados Unidos') }}</option>
					<option value="EE" {{ (old('pais', auth()->user()->profile->pais) == 'EE') ? 'selected' : '' }}>{{ __('Estonia') }}</option>
					<option value="ET" {{ (old('pais', auth()->user()->profile->pais) == 'ET') ? 'selected' : '' }}>{{ __('Etiopía') }}</option>
					<option value="FJ" {{ (old('pais', auth()->user()->profile->pais) == 'FJ') ? 'selected' : '' }}>{{ __('Fiji') }}</option>
					<option value="PH" {{ (old('pais', auth()->user()->profile->pais) == 'PH') ? 'selected' : '' }}>{{ __('Filipinas') }}</option>
					<option value="FI" {{ (old('pais', auth()->user()->profile->pais) == 'FI') ? 'selected' : '' }}>{{ __('Finlandia') }}</option>
					<option value="FR" {{ (old('pais', auth()->user()->profile->pais) == 'FR') ? 'selected' : '' }}>{{ __('Francia') }}</option>
					<option value="GA" {{ (old('pais', auth()->user()->profile->pais) == 'GA') ? 'selected' : '' }}>{{ __('Gabón') }}</option>
					<option value="GM" {{ (old('pais', auth()->user()->profile->pais) == 'GM') ? 'selected' : '' }}>{{ __('Gambia') }}</option>
					<option value="GE" {{ (old('pais', auth()->user()->profile->pais) == 'GE') ? 'selected' : '' }}>{{ __('Georgia') }}</option>
					<option value="GH" {{ (old('pais', auth()->user()->profile->pais) == 'GH') ? 'selected' : '' }}>{{ __('Ghana') }}</option>
					<option value="GI" {{ (old('pais', auth()->user()->profile->pais) == 'GI') ? 'selected' : '' }}>{{ __('Gibraltar') }}</option>
					<option value="GD" {{ (old('pais', auth()->user()->profile->pais) == 'GD') ? 'selected' : '' }}>{{ __('Granada') }}</option>
					<option value="GR" {{ (old('pais', auth()->user()->profile->pais) == 'GR') ? 'selected' : '' }}>{{ __('Grecia') }}</option>
					<option value="GL" {{ (old('pais', auth()->user()->profile->pais) == 'GL') ? 'selected' : '' }}>{{ __('Groenlandia') }}</option>
					<option value="GP" {{ (old('pais', auth()->user()->profile->pais) == 'GP') ? 'selected' : '' }}>{{ __('Guadalupe') }}</option>
					<option value="GU" {{ (old('pais', auth()->user()->profile->pais) == 'GU') ? 'selected' : '' }}>{{ __('Guam') }}</option>
					<option value="GT" {{ (old('pais', auth()->user()->profile->pais) == 'GT') ? 'selected' : '' }}>{{ __('Guatemala') }}</option>
					<option value="GY" {{ (old('pais', auth()->user()->profile->pais) == 'GY') ? 'selected' : '' }}>{{ __('Guayana') }}</option>
					<option value="GF" {{ (old('pais', auth()->user()->profile->pais) == 'GF') ? 'selected' : '' }}>{{ __('Guayana Francesa') }}</option>
					<option value="GN" {{ (old('pais', auth()->user()->profile->pais) == 'GN') ? 'selected' : '' }}>{{ __('Guinea') }}</option>
					<option value="GQ" {{ (old('pais', auth()->user()->profile->pais) == 'GQ') ? 'selected' : '' }}>{{ __('Guinea Ecuatorial') }}</option>
					<option value="GW" {{ (old('pais', auth()->user()->profile->pais) == 'GW') ? 'selected' : '' }}>{{ __('Guinea-Bissau') }}</option>
					<option value="HT" {{ (old('pais', auth()->user()->profile->pais) == 'HT') ? 'selected' : '' }}>{{ __('Haití') }}</option>
					<option value="HN" {{ (old('pais', auth()->user()->profile->pais) == 'HN') ? 'selected' : '' }}>{{ __('Honduras') }}</option>
					<option value="HU" {{ (old('pais', auth()->user()->profile->pais) == 'HU') ? 'selected' : '' }}>{{ __('Hungría') }}</option>
					<option value="IN" {{ (old('pais', auth()->user()->profile->pais) == 'IN') ? 'selected' : '' }}>{{ __('India') }}</option>
					<option value="ID" {{ (old('pais', auth()->user()->profile->pais) == 'ID') ? 'selected' : '' }}>{{ __('Indonesia') }}</option>
					<option value="IQ" {{ (old('pais', auth()->user()->profile->pais) == 'IQ') ? 'selected' : '' }}>{{ __('Irak') }}</option>
					<option value="IR" {{ (old('pais', auth()->user()->profile->pais) == 'IR') ? 'selected' : '' }}>{{ __('Irán') }}</option>
					<option value="IE" {{ (old('pais', auth()->user()->profile->pais) == 'IE') ? 'selected' : '' }}>{{ __('Irlanda') }}</option>
					<option value="BV" {{ (old('pais', auth()->user()->profile->pais) == 'BV') ? 'selected' : '' }}>{{ __('Isla Bouvet') }}</option>
					<option value="CX" {{ (old('pais', auth()->user()->profile->pais) == 'CX') ? 'selected' : '' }}>{{ __('Isla de Christmas') }}</option>
					<option value="IS" {{ (old('pais', auth()->user()->profile->pais) == 'IS') ? 'selected' : '' }}>{{ __('Islandia') }}</option>
					<option value="KY" {{ (old('pais', auth()->user()->profile->pais) == 'KY') ? 'selected' : '' }}>{{ __('Islas Caimán') }}</option>
					<option value="CK" {{ (old('pais', auth()->user()->profile->pais) == 'CK') ? 'selected' : '' }}>{{ __('Islas Cook') }}</option>
					<option value="CC" {{ (old('pais', auth()->user()->profile->pais) == 'CC') ? 'selected' : '' }}>{{ __('Islas de Cocos o Keeling') }}</option>
					<option value="FO" {{ (old('pais', auth()->user()->profile->pais) == 'FO') ? 'selected' : '' }}>{{ __('Islas Faroe') }}</option>
					<option value="HM" {{ (old('pais', auth()->user()->profile->pais) == 'HM') ? 'selected' : '' }}>{{ __('Islas Heard y McDonald') }}</option>
					<option value="FK" {{ (old('pais', auth()->user()->profile->pais) == 'FK') ? 'selected' : '' }}>{{ __('Islas Malvinas') }}</option>
					<option value="MP" {{ (old('pais', auth()->user()->profile->pais) == 'MP') ? 'selected' : '' }}>{{ __('Islas Marianas del Norte') }}</option>
					<option value="MH" {{ (old('pais', auth()->user()->profile->pais) == 'MH') ? 'selected' : '' }}>{{ __('Islas Marshall') }}</option>
					<option value="UM" {{ (old('pais', auth()->user()->profile->pais) == 'UM') ? 'selected' : '' }}>{{ __('Islas menores de Estados Unido') }}</option>
					<option value="PW" {{ (old('pais', auth()->user()->profile->pais) == 'PW') ? 'selected' : '' }}>{{ __('Islas Palau') }}</option>
					<option value="SB" {{ (old('pais', auth()->user()->profile->pais) == 'SB') ? 'selected' : '' }}>{{ __('Islas Salomón') }}</option>
					<option value="SJ" {{ (old('pais', auth()->user()->profile->pais) == 'SJ') ? 'selected' : '' }}>{{ __('Islas Svalbard y Jan Mayen') }}</option>
					<option value="TK" {{ (old('pais', auth()->user()->profile->pais) == 'TK') ? 'selected' : '' }}>{{ __('Islas Tokelau') }}</option>
					<option value="TC" {{ (old('pais', auth()->user()->profile->pais) == 'TC') ? 'selected' : '' }}>{{ __('Islas Turks y Caicos') }}</option>
					<option value="VI" {{ (old('pais', auth()->user()->profile->pais) == 'VI') ? 'selected' : '' }}>{{ __('Islas Vírgenes (EEUU)') }}</option>
					<option value="VG" {{ (old('pais', auth()->user()->profile->pais) == 'VG') ? 'selected' : '' }}>{{ __('Islas Vírgenes (Reino Unido)') }}</option>
					<option value="WF" {{ (old('pais', auth()->user()->profile->pais) == 'WF') ? 'selected' : '' }}>{{ __('Islas Wallis y Futuna') }}</option>
					<option value="IL" {{ (old('pais', auth()->user()->profile->pais) == 'IL') ? 'selected' : '' }}>{{ __('Israel') }}</option>
					<option value="IT" {{ (old('pais', auth()->user()->profile->pais) == 'IT') ? 'selected' : '' }}>{{ __('Italia') }}</option>
					<option value="JM" {{ (old('pais', auth()->user()->profile->pais) == 'JM') ? 'selected' : '' }}>{{ __('Jamaica') }}</option>
					<option value="JP" {{ (old('pais', auth()->user()->profile->pais) == 'JP') ? 'selected' : '' }}>{{ __('Japón') }}</option>
					<option value="JO" {{ (old('pais', auth()->user()->profile->pais) == 'JO') ? 'selected' : '' }}>{{ __('Jordania') }}</option>
					<option value="KZ" {{ (old('pais', auth()->user()->profile->pais) == 'KZ') ? 'selected' : '' }}>{{ __('Kazajistán') }}</option>
					<option value="KE" {{ (old('pais', auth()->user()->profile->pais) == 'KE') ? 'selected' : '' }}>{{ __('Kenia') }}</option>
					<option value="KG" {{ (old('pais', auth()->user()->profile->pais) == 'KG') ? 'selected' : '' }}>{{ __('Kirguizistán') }}</option>
					<option value="KI" {{ (old('pais', auth()->user()->profile->pais) == 'KI') ? 'selected' : '' }}>{{ __('Kiribati') }}</option>
					<option value="KW" {{ (old('pais', auth()->user()->profile->pais) == 'KW') ? 'selected' : '' }}>{{ __('Kuwait') }}</option>
					<option value="LA" {{ (old('pais', auth()->user()->profile->pais) == 'LA') ? 'selected' : '' }}>{{ __('Laos') }}</option>
					<option value="LS" {{ (old('pais', auth()->user()->profile->pais) == 'LS') ? 'selected' : '' }}>{{ __('Lesotho') }}</option>
					<option value="LV" {{ (old('pais', auth()->user()->profile->pais) == 'LV') ? 'selected' : '' }}>{{ __('Letonia') }}</option>
					<option value="LB" {{ (old('pais', auth()->user()->profile->pais) == 'LB') ? 'selected' : '' }}>{{ __('Líbano') }}</option>
					<option value="LR" {{ (old('pais', auth()->user()->profile->pais) == 'LR') ? 'selected' : '' }}>{{ __('Liberia') }}</option>
					<option value="LY" {{ (old('pais', auth()->user()->profile->pais) == 'LY') ? 'selected' : '' }}>{{ __('Libia') }}</option>
					<option value="LI" {{ (old('pais', auth()->user()->profile->pais) == 'LI') ? 'selected' : '' }}>{{ __('Liechtenstein') }}</option>
					<option value="LT" {{ (old('pais', auth()->user()->profile->pais) == 'LT') ? 'selected' : '' }}>{{ __('Lituania') }}</option>
					<option value="LU" {{ (old('pais', auth()->user()->profile->pais) == 'LU') ? 'selected' : '' }}>{{ __('Luxemburgo') }}</option>
					<option value="MK" {{ (old('pais', auth()->user()->profile->pais) == 'MK') ? 'selected' : '' }}>{{ __('Macedonia') }}</option>
					<option value="MG" {{ (old('pais', auth()->user()->profile->pais) == 'MG') ? 'selected' : '' }}>{{ __('Madagascar') }}</option>
					<option value="MY" {{ (old('pais', auth()->user()->profile->pais) == 'MY') ? 'selected' : '' }}>{{ __('Malasia') }}</option>
					<option value="MW" {{ (old('pais', auth()->user()->profile->pais) == 'MW') ? 'selected' : '' }}>{{ __('Malawi') }}</option>
					<option value="MV" {{ (old('pais', auth()->user()->profile->pais) == 'MV') ? 'selected' : '' }}>{{ __('Maldivas') }}</option>
					<option value="ML" {{ (old('pais', auth()->user()->profile->pais) == 'ML') ? 'selected' : '' }}>{{ __('Malí') }}</option>
					<option value="MT" {{ (old('pais', auth()->user()->profile->pais) == 'MT') ? 'selected' : '' }}>{{ __('Malta') }}</option>
					<option value="MA" {{ (old('pais', auth()->user()->profile->pais) == 'MA') ? 'selected' : '' }}>{{ __('Marruecos') }}</option>
					<option value="MQ" {{ (old('pais', auth()->user()->profile->pais) == 'MQ') ? 'selected' : '' }}>{{ __('Martinica') }}</option>
					<option value="MU" {{ (old('pais', auth()->user()->profile->pais) == 'MU') ? 'selected' : '' }}>{{ __('Mauricio') }}</option>
					<option value="MR" {{ (old('pais', auth()->user()->profile->pais) == 'MR') ? 'selected' : '' }}>{{ __('Mauritania') }}</option>
					<option value="YT" {{ (old('pais', auth()->user()->profile->pais) == 'YT') ? 'selected' : '' }}>{{ __('Mayotte') }}</option>
					<option value="MX" {{ (old('pais', auth()->user()->profile->pais) == 'MX') ? 'selected' : '' }}>{{ __('México') }}</option>
					<option value="FM" {{ (old('pais', auth()->user()->profile->pais) == 'FM') ? 'selected' : '' }}>{{ __('Micronesia') }}</option>
					<option value="MD" {{ (old('pais', auth()->user()->profile->pais) == 'MD') ? 'selected' : '' }}>{{ __('Moldavia') }}</option>
					<option value="MC" {{ (old('pais', auth()->user()->profile->pais) == 'MC') ? 'selected' : '' }}>{{ __('Mónaco') }}</option>
					<option value="MN" {{ (old('pais', auth()->user()->profile->pais) == 'MN') ? 'selected' : '' }}>{{ __('Mongolia') }}</option>
					<option value="MS" {{ (old('pais', auth()->user()->profile->pais) == 'MS') ? 'selected' : '' }}>{{ __('Montserrat') }}</option>
					<option value="MZ" {{ (old('pais', auth()->user()->profile->pais) == 'MZ') ? 'selected' : '' }}>{{ __('Mozambique') }}</option>
					<option value="NA" {{ (old('pais', auth()->user()->profile->pais) == 'NA') ? 'selected' : '' }}>{{ __('Namibia') }}</option>
					<option value="NR" {{ (old('pais', auth()->user()->profile->pais) == 'NR') ? 'selected' : '' }}>{{ __('Nauru') }}</option>
					<option value="NP" {{ (old('pais', auth()->user()->profile->pais) == 'NP') ? 'selected' : '' }}>{{ __('Nepal') }}</option>
					<option value="NI" {{ (old('pais', auth()->user()->profile->pais) == 'NI') ? 'selected' : '' }}>{{ __('Nicaragua') }}</option>
					<option value="NE" {{ (old('pais', auth()->user()->profile->pais) == 'NE') ? 'selected' : '' }}>{{ __('Níger') }}</option>
					<option value="NG" {{ (old('pais', auth()->user()->profile->pais) == 'NG') ? 'selected' : '' }}>{{ __('Nigeria') }}</option>
					<option value="NU" {{ (old('pais', auth()->user()->profile->pais) == 'NU') ? 'selected' : '' }}>{{ __('Niue') }}</option>
					<option value="NF" {{ (old('pais', auth()->user()->profile->pais) == 'NF') ? 'selected' : '' }}>{{ __('Norfolk') }}</option>
					<option value="NO" {{ (old('pais', auth()->user()->profile->pais) == 'NO') ? 'selected' : '' }}>{{ __('Noruega') }}</option>
					<option value="NC" {{ (old('pais', auth()->user()->profile->pais) == 'NC') ? 'selected' : '' }}>{{ __('Nueva Caledonia') }}</option>
					<option value="NZ" {{ (old('pais', auth()->user()->profile->pais) == 'NZ') ? 'selected' : '' }}>{{ __('Nueva Zelanda') }}</option>
					<option value="OM" {{ (old('pais', auth()->user()->profile->pais) == 'OM') ? 'selected' : '' }}>{{ __('Omán') }}</option>
					<option value="NL" {{ (old('pais', auth()->user()->profile->pais) == 'NL') ? 'selected' : '' }}>{{ __('Países Bajos') }}</option>
					<option value="PA" {{ (old('pais', auth()->user()->profile->pais) == 'PA') ? 'selected' : '' }}>{{ __('Panamá') }}</option>
					<option value="PG" {{ (old('pais', auth()->user()->profile->pais) == 'PG') ? 'selected' : '' }}>{{ __('Papúa Nueva Guinea') }}</option>
					<option value="PK" {{ (old('pais', auth()->user()->profile->pais) == 'PK') ? 'selected' : '' }}>{{ __('Paquistán') }}</option>
					<option value="PY" {{ (old('pais', auth()->user()->profile->pais) == 'PY') ? 'selected' : '' }}>{{ __('Paraguay') }}</option>
					<option value="PE" {{ (old('pais', auth()->user()->profile->pais) == 'PE') ? 'selected' : '' }}>{{ __('Perú') }}</option>
					<option value="PN" {{ (old('pais', auth()->user()->profile->pais) == 'PN') ? 'selected' : '' }}>{{ __('Pitcairn') }}</option>
					<option value="PF" {{ (old('pais', auth()->user()->profile->pais) == 'PF') ? 'selected' : '' }}>{{ __('Polinesia Francesa') }}</option>
					<option value="PL" {{ (old('pais', auth()->user()->profile->pais) == 'PL') ? 'selected' : '' }}>{{ __('Polonia') }}</option>
					<option value="PT" {{ (old('pais', auth()->user()->profile->pais) == 'PT') ? 'selected' : '' }}>{{ __('Portugal') }}</option>
					<option value="PR" {{ (old('pais', auth()->user()->profile->pais) == 'PR') ? 'selected' : '' }}>{{ __('Puerto Rico') }}</option>
					<option value="QA" {{ (old('pais', auth()->user()->profile->pais) == 'QA') ? 'selected' : '' }}>{{ __('Qatar') }}</option>
					<option value="GB" {{ (old('pais', auth()->user()->profile->pais) == 'GB') ? 'selected' : '' }}>{{ __('Reino Unido') }}</option>
					<option value="CF" {{ (old('pais', auth()->user()->profile->pais) == 'CF') ? 'selected' : '' }}>{{ __('República Centroafricana') }}</option>
					<option value="CZ" {{ (old('pais', auth()->user()->profile->pais) == 'CZ') ? 'selected' : '' }}>{{ __('República Checa') }}</option>
					<option value="ZA" {{ (old('pais', auth()->user()->profile->pais) == 'ZA') ? 'selected' : '' }}>{{ __('República de Sudáfrica') }}</option>
					<option value="DO" {{ (old('pais', auth()->user()->profile->pais) == 'DO') ? 'selected' : '' }}>{{ __('República Dominicana') }}</option>
					<option value="SK" {{ (old('pais', auth()->user()->profile->pais) == 'SK') ? 'selected' : '' }}>{{ __('República Eslovaca') }}</option>
					<option value="RE" {{ (old('pais', auth()->user()->profile->pais) == 'RE') ? 'selected' : '' }}>{{ __('Reunión') }}</option>
					<option value="RW" {{ (old('pais', auth()->user()->profile->pais) == 'RW') ? 'selected' : '' }}>{{ __('Ruanda') }}</option>
					<option value="RO" {{ (old('pais', auth()->user()->profile->pais) == 'RO') ? 'selected' : '' }}>{{ __('Rumania') }}</option>
					<option value="RU" {{ (old('pais', auth()->user()->profile->pais) == 'RU') ? 'selected' : '' }}>{{ __('Rusia') }}</option>
					<option value="EH" {{ (old('pais', auth()->user()->profile->pais) == 'EH') ? 'selected' : '' }}>{{ __('Sahara Occidental') }}</option>
					<option value="KN" {{ (old('pais', auth()->user()->profile->pais) == 'KN') ? 'selected' : '' }}>{{ __('Saint Kitts y Nevis') }}</option>
					<option value="WS" {{ (old('pais', auth()->user()->profile->pais) == 'WS') ? 'selected' : '' }}>{{ __('Samoa') }}</option>
					<option value="AS" {{ (old('pais', auth()->user()->profile->pais) == 'AS') ? 'selected' : '' }}>{{ __('Samoa Americana') }}</option>
					<option value="SM" {{ (old('pais', auth()->user()->profile->pais) == 'SM') ? 'selected' : '' }}>{{ __('San Marino') }}</option>
					<option value="VC" {{ (old('pais', auth()->user()->profile->pais) == 'VC') ? 'selected' : '' }}>{{ __('San Vicente y Granadinas') }}</option>
					<option value="SH" {{ (old('pais', auth()->user()->profile->pais) == 'SH') ? 'selected' : '' }}>{{ __('Santa Helena') }}</option>
					<option value="LC" {{ (old('pais', auth()->user()->profile->pais) == 'LC') ? 'selected' : '' }}>{{ __('Santa Lucía') }}</option>
					<option value="ST" {{ (old('pais', auth()->user()->profile->pais) == 'ST') ? 'selected' : '' }}>{{ __('Santo Tomé y Príncipe') }}</option>
					<option value="SN" {{ (old('pais', auth()->user()->profile->pais) == 'SN') ? 'selected' : '' }}>{{ __('Senegal') }}</option>
					<option value="SC" {{ (old('pais', auth()->user()->profile->pais) == 'SC') ? 'selected' : '' }}>{{ __('Seychelles') }}</option>
					<option value="SL" {{ (old('pais', auth()->user()->profile->pais) == 'SL') ? 'selected' : '' }}>{{ __('Sierra Leona') }}</option>
					<option value="SG" {{ (old('pais', auth()->user()->profile->pais) == 'SG') ? 'selected' : '' }}>{{ __('Singapur') }}</option>
					<option value="SY" {{ (old('pais', auth()->user()->profile->pais) == 'SY') ? 'selected' : '' }}>{{ __('Siria') }}</option>
					<option value="SO" {{ (old('pais', auth()->user()->profile->pais) == 'SO') ? 'selected' : '' }}>{{ __('Somalia') }}</option>
					<option value="LK" {{ (old('pais', auth()->user()->profile->pais) == 'LK') ? 'selected' : '' }}>{{ __('Sri Lanka') }}</option>
					<option value="PM" {{ (old('pais', auth()->user()->profile->pais) == 'PM') ? 'selected' : '' }}>{{ __('St Pierre y Miquelon') }}</option>
					<option value="SZ" {{ (old('pais', auth()->user()->profile->pais) == 'SZ') ? 'selected' : '' }}>{{ __('Suazilandia') }}</option>
					<option value="SD" {{ (old('pais', auth()->user()->profile->pais) == 'SD') ? 'selected' : '' }}>{{ __('Sudán') }}</option>
					<option value="SE" {{ (old('pais', auth()->user()->profile->pais) == 'SE') ? 'selected' : '' }}>{{ __('Suecia') }}</option>
					<option value="CH" {{ (old('pais', auth()->user()->profile->pais) == 'CH') ? 'selected' : '' }}>{{ __('Suiza') }}</option>
					<option value="SR" {{ (old('pais', auth()->user()->profile->pais) == 'SR') ? 'selected' : '' }}>{{ __('Surinam') }}</option>
					<option value="TH" {{ (old('pais', auth()->user()->profile->pais) == 'TH') ? 'selected' : '' }}>{{ __('Tailandia') }}</option>
					<option value="TW" {{ (old('pais', auth()->user()->profile->pais) == 'TW') ? 'selected' : '' }}>{{ __('Taiwán') }}</option>
					<option value="TZ" {{ (old('pais', auth()->user()->profile->pais) == 'TZ') ? 'selected' : '' }}>{{ __('Tanzania') }}</option>
					<option value="TJ" {{ (old('pais', auth()->user()->profile->pais) == 'TJ') ? 'selected' : '' }}>{{ __('Tayikistán') }}</option>
					<option value="TF" {{ (old('pais', auth()->user()->profile->pais) == 'TF') ? 'selected' : '' }}>{{ __('Territorios franceses del Sur') }}</option>
					<option value="TP" {{ (old('pais', auth()->user()->profile->pais) == 'TP') ? 'selected' : '' }}>{{ __('Timor Oriental') }}</option>
					<option value="TG" {{ (old('pais', auth()->user()->profile->pais) == 'TG') ? 'selected' : '' }}>{{ __('Togo') }}</option>
					<option value="TO" {{ (old('pais', auth()->user()->profile->pais) == 'TO') ? 'selected' : '' }}>{{ __('Tonga') }}</option>
					<option value="TT" {{ (old('pais', auth()->user()->profile->pais) == 'TT') ? 'selected' : '' }}>{{ __('Trinidad y Tobago') }}</option>
					<option value="TN" {{ (old('pais', auth()->user()->profile->pais) == 'TN') ? 'selected' : '' }}>{{ __('Túnez') }}</option>
					<option value="TM" {{ (old('pais', auth()->user()->profile->pais) == 'TM') ? 'selected' : '' }}>{{ __('Turkmenistán') }}</option>
					<option value="TR" {{ (old('pais', auth()->user()->profile->pais) == 'TR') ? 'selected' : '' }}>{{ __('Turquía') }}</option>
					<option value="TV" {{ (old('pais', auth()->user()->profile->pais) == 'TV') ? 'selected' : '' }}>{{ __('Tuvalu') }}</option>
					<option value="UA" {{ (old('pais', auth()->user()->profile->pais) == 'UA') ? 'selected' : '' }}>{{ __('Ucrania') }}</option>
					<option value="UG" {{ (old('pais', auth()->user()->profile->pais) == 'UG') ? 'selected' : '' }}>{{ __('Uganda') }}</option>
					<option value="UY" {{ (old('pais', auth()->user()->profile->pais) == 'UY') ? 'selected' : '' }}>{{ __('Uruguay') }}</option>
					<option value="UZ" {{ (old('pais', auth()->user()->profile->pais) == 'UZ') ? 'selected' : '' }}>{{ __('Uzbekistán') }}</option>
					<option value="VU" {{ (old('pais', auth()->user()->profile->pais) == 'VU') ? 'selected' : '' }}>{{ __('Vanuatu') }}</option>
					<option value="VE" {{ (old('pais', auth()->user()->profile->pais) == 'VE') ? 'selected' : '' }}>{{ __('Venezuela') }}</option>
					<option value="VN" {{ (old('pais', auth()->user()->profile->pais) == 'VN') ? 'selected' : '' }}>{{ __('Vietnam') }}</option>
					<option value="YE" {{ (old('pais', auth()->user()->profile->pais) == 'YE') ? 'selected' : '' }}>{{ __('Yemen') }}</option>
					<option value="YU" {{ (old('pais', auth()->user()->profile->pais) == 'YU') ? 'selected' : '' }}>{{ __('Yugoslavia') }}</option>
					<option value="ZM" {{ (old('pais', auth()->user()->profile->pais) == 'ZM') ? 'selected' : '' }}>{{ __('Zambia') }}</option>
					<option value="ZW" {{ (old('pais', auth()->user()->profile->pais) == 'ZW') ? 'selected' : '' }}>{{ __('Zimbabue') }}</option>
				</select>
			</div>
		</div>
		{{-- FIN DIRECCION --}}






		{{-- #TIPO DE INVERSOR --}}
		<div class="xs-4 col  mt-24">
			<h4 class="c-2">{{ __('Acreditación de inversor') }}</h4>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col false">
			</div>
			<div class="xs-4 m-9 col">
					<input type="radio" id="checkbox-ID1" class="custom-radio__input" name="acreditado" value="0" checked {{ (isset($noForm) && $noForm == true) ? 'readonly' : '' }}>
					<label class="d-inblock  mr-40  {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" for="checkbox-ID1">
						<p class="custom-radio__text"><span class="custom-radio__new"></span>{{ __('Inversor no acreditado') }}</p>
					</label>
					<input type="radio" id="checkbox-ID2" class="custom-radio__input" value="1" name="acreditado">
					<label class="d-inblock  {{ (isset($noForm) && $noForm == true) ? 'pe-none' : '' }}" for="checkbox-ID2">
						<p class="custom-radio__text"><span class="custom-radio__new"></span>{{ __('Inversor acreditado') }}</p>
					</label>
			</div>
		</div>
		{{-- FIN TIPO DE INVERSOR --}}

		<div class="clearfix">
			<div class="xs-4 m-3 col false"></div>
			<div class="xs-4 m-9 col">
				<input class="button button--small button--mvl-full  |  mt-40" type="submit" value="{{ __('Guardar cambios') }}">
			</div>
		</div>

	</form>

@endsection

{{-- CSS --}}
@section('header_css')
	<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
@endsection

{{-- @section('header_assets') @endsection  --}}


{{-- JS --}}
@section('custom_plugin_js')
	<script src="/assets/js/plugins/parsleyjs.min.js"></script>
@endsection

@section('custom_section_js')
	<script>var tipoCuenta = {{ (old('tipo', auth()->user()->profile->tipo) == 'Particular' || old('tipo', auth()->user()->profile->tipo) == '1') ? '1' : '0' }}{{ (old('tipo', auth()->user()->profile->tipo) == 'Empresa' || old('tipo', auth()->user()->profile->tipo) == '2') ? '2' : '' }};</script>
	<script src="{{ mix('assets/js/main.js') }}" async></script>

	@if(session()->has('esperando_primera_acreditacion'))
		<!-- Google Code for Acreditaci&oacute;n Conversion Page -->
		<script type="text/javascript">
			/* <![CDATA[ */
			var google_conversion_id = 829591566;
			var google_conversion_label = "5OwmCIeOq3gQjqDKiwM";
			var google_remarketing_only = false;
			/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
		<noscript>
			<div style="display:inline;">
				<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/829591566/?label=5OwmCIeOq3gQjqDKiwM&amp;guid=ON&amp;script=0"/>
			</div>
		</noscript>
	@endif

@endsection 
