{{--
	==========================================================================
	#PROPERTY SINGLE
	==========================================================================
	*
	* DESCRIPCIÓN
	*
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT
	========================================================================== --}}

	@section('content')

		<div class="cs-4 col mt-40">
			<p class="mt-0">{{ __('Invita a un amigo y recibiréis ambos 15 € en vuestra próxima inversión.') }}</p>
			<p>{{ __('Para hacerlo tienes varias opciones:') }}</p>

			<h4 class="c-2  mt-48 mb-24">{{ __('Compartir link') }}</h4>
			<h5 id="js-clipboard-text" class="mb-0">{{ auth()->user()->affiliateUrl }}</h5>
			<input class="button button--secondary button--small button--mvl-full  |  mt-24   [ js-clipboard ]" type="submit" value="{{ __('Copiar Link') }}">
			<h4 class="c-2  mt-48 mb-24">{{ __('Enviar email') }}</h4>

		<form class="validate   [ js-loading ]" method="POST" action="{{ route('panel.user.invite.post') }}"
			  data-parsley-validate="" data-parsley-trigger="blur">

				{{ csrf_field() }}

				@if(session()->has('user_invited'))
					<p class="c-success text-center">
						{{ __('Se ha enviado su invitación correctamente al email') }} {{ $email_invited ? $email_invited : null }}.
					</p>
				@endif

				@if (count($errors) > 0)
	                @foreach ($errors->all() as $error)
	                    <p class="c-error">{{ $error }}</p>
	                @endforeach
	            @endif

				<div class="xs-4 m-9 col  pl-0">
					<input class="custom-input" type="email" placeholder="{{ __('Email') }}" name="email" required>
				</div>
				<div class="xs-4 m-9 col  pl-0 mt-24">
					<input class="button button--secondary button--small button--mvl-full" type="submit" value="{{ __('Invitar') }}">
				</div>

			</form>

		</div>

		{{-- @include ('front.contact.errors')  --}}

	@endsection






{{--==========================================================================
	#ASSETS
	========================================================================== --}}

	{{-- CSS --}}


	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection
