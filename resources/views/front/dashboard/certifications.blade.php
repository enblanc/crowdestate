@extends('front/layouts/dashboard')


@section('content')
    <div class="xs-4 col mt-40 mb-40 flex items-center flex-content-between">
        <h4 class="c-2 d-inline">{{ __('Certificaciones de retenciones') }}</h4>
    </div>

    <div class="clearfix mb-16">
        <ul class="clear-both">
            @foreach ($certifications as $certification)
                <li class="flex items-center flex-content-between gap-4 w-full h4">
                    <a href="{{ $certification->link }}" target="_blank" class="flex items-center gap-1">
                        <svg width="30" fill="none" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M9 12.75l3 3m0 0l3-3m-3 3v-7.5M21 12a9 9 0 11-18 0 9 9 0 0118 0z"></path>
                        </svg>
                        <span>{{ $certification->label }}</span> | <span>{{ $certification->date }}</span>
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
@endsection

@section('header_css')
    <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
@endsection

@section('custom_plugin_js')
    <script src="/assets/js/plugins/parsleyjs.min.js"></script>
@endsection

@section('custom_section_js')
    <script src="{{ mix('assets/js/main.js') }}" async></script>
@endsection
