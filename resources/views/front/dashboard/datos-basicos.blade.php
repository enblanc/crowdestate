{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- pop-borrar (carpeta parts)
	*	- pop-borrada (en carpeta structure)
	*
	*
	* AVISOS
	*
	* 	- El archivo pop-borrar tiene los 3 estados: sin fondos, con fondos, con inversiones.
	*	- El archivo de éxito de borrar cuenta es el pop-borrada.
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<form class="mt-40  validate   [ js-loading ]" method="POST" action="{{ route('panel.user.basic.save') }}"
			  data-parsley-validate="" data-parsley-trigger="blur">
			
			{{ csrf_field() }}

			@if(session()->has('user_saved'))
				<p class="c-success text-center">{{ __('Los cambios han sido guardados correctamente.') }}</p>
			@endif
			@if(session()->has('email_sent'))
				<p class="c-success text-center">{{ __('Hemos enviado una mail a tu cuenta para que confirmes el cambio de email.') }}</p>
			@endif
			@if(session()->has('email_changed'))
				<p class="c-success text-center">{{ __('Su email ha sido modificado correctamente') }}</p>
			@endif

			@if(session()->has('errors_close'))
				<p class="c-error text-center">{!! session()->get('errors_close') !!}</p>
			@endif

			
	   		@if (count($errors) > 0)
	            @foreach ($errors->all() as $error)
	                <p class="c-error  text-center">{{ $error }}</p>
	            @endforeach
			@endif
		


			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Nombre') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" placeholder="{{ __('Nombre') }}"  name="nombre" value="{{ $user->nombre }}"  required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Primer apellido') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" placeholder="{{ __('Primer apellido') }}" name="apellido1" value="{{ $user->apellido1 }}" required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Segundo apellido') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="text" placeholder="{{ __('Segundo apellido') }}" name="apellido2" value="{{ $user->apellido2 }}" >
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('E-mail') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<input class="custom-input" type="email" placeholder="E-mail" name="email" value="{{ $user->email }}" required>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Language') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					<select class="custom-input  custom-input--select" name="lang" id="lang" required>
						<option value="es" {{ ($user->locale == 'es') ? 'selected' : '' }}>{{ __('Español') }}</option>
						<option value="en" {{ ($user->locale == 'en') ? 'selected' : '' }}>{{ __('Inglés') }}</option>
						<option value="de" {{ ($user->locale == 'de') ? 'selected' : '' }}>{{ __('Alemán') }}</option>
					</select>
				</div>
			</div>

			<div class="clearfix mb-16">

				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Tributo en España') }}</h5>
				</div>

				<div class="xs-4 m-9 col pt-16">


					<input type="checkbox" id="tribute" class="custom-checkbox__input" name="tribute" {{ ($user->tribute) ? 'checked' : '' }}
                        data-parsley-errors-messages-disabled>
                    <label for="tribute" class="pt-4 left">
                        <span class="custom-checkbox__new  |  v-top  mt-8"></span> 
                    </label>

                    <v-popover :open="tributeOpen" :trigger="'manual'" :placement="'bottom'" class="left mt-4">
                    	<small>
	                		<span v-on:click="tributeOpen = !tributeOpen" class="c-pointer">{{ __('Aceptas las condiciones') }}</span>
	                	</small>
						<template slot="popover">
							<small>
								{!! __('TributePopup') !!}
							</small>

							<a v-close-popover class="right">Close</a>
						</template>
					</v-popover>
                   
                    
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="pt-m-8">{{ __('Newsletter') }}</h5>
				</div>
				<div class="xs-4 m-9 col pt-16">
					<input type="checkbox" id="newsletter" class="custom-checkbox__input" name="newsletter" {{ ($newsletter) ? 'checked' : '' }}
                        data-parsley-errors-messages-disabled>
                        <label for="newsletter" class="pt-4">
                            <span class="custom-checkbox__new  |  v-top  mt-8"></span> 
                        </label>
				</div>
			</div>

			<div class="clearfix">
				<div class="xs-4 m-3 col false"></div>
				<div class="xs-4 m-9 col">
					<input class="button button--mvl-full button--small  |  mt-32" type="submit" value="{{ __('Guardar cambios') }}">
				</div>
			</div>

		</form>






		<div class="xs-4 col  text-right  mt-80">
			<a class="js-modal" href="#" modal-ref="borrar-cuenta">{{ __('Borrar cuenta') }}</a>
		</div>
		@include ('front/dashboard/pop-borrar')

		

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
