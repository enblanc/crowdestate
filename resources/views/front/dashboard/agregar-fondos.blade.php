{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/dashboard')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<div class="xs-4 col">
	   		@if (count($errors) > 0)
	            @foreach ($errors->all() as $error)
	                <p class="c-error  text-center">{{ $error }}</p>
	            @endforeach
			@endif
		</div>

		<div class="xs-4 col">
	   		@if (session()->has('error_lemon'))
	           <p class="c-error  text-center">{{ session()->get('error_lemon') }}</p>
			@endif
		</div>

		<div class="xs-4 col  mt-24">
		
			@if(session()->has('msg_invertir'))
				<p class="c-error">{{ session()->get('msg_invertir') }}</p>
			@endif

			{{-- 
				Este mensaje solo saldría al realizar una acción. 
				En caso de error: No hemos podido completar el ingreso deseado
				En caso de cancelación: El ingreso ha sido cancelado

				Con error o cancelación cambiar ".c-success" por ".c-error"

			 --}}
			@if(session()->has('fondos_agregados') || session()->has('payment_card'))
				<p class="c-success text-center">{{ __('¡Tu ingreso se ha producido con éxito!') }}</p>
			@endif

			@if(session()->has('payment_card_cancel'))
				<p class="c-error  text-center">{{ __('Tu pago ha sido cancelado con éxito') }}</p>
			@endif

			@if(session()->has('payment_card_error'))
				<p class="c-error  text-center">{{ __('Ha ocurrido un error con esta tarjeta y el pago no se ha podido realizar. Por favor, pruebe con otra tarjeta.') }}</p>
			@endif

			@if(session()->has('removed_card') && session()->has('removed_card') == true)
				<p class="c-success text-center">{{ __('Tarjeta eliminada correctamente') }}</p>
			@endif

			@if(session()->has('removed_card') && session()->has('removed_card') == false)
				<p class="c-success text-center">{{ __('La tarjeta no se ha podido eliminar. Por favor, consulta con nuestro servicio de contacto.') }}</p>
			@endif
			
			@php
				$balance = auth()->user()->balance;
			@endphp

			@if( $balance == 0)
				{{ __('Agrega fondos para tener dinero en tu cuenta de Brickstarter') }}
			@endif

			<p class="big-text ">{{ __('Actualmente tienes') }} {{ formatThousandsNotZero($balance) }} € {{ __('en tu cuenta') }}</p>

		</div>

		<form class="mt-40 validate   [ js-loading ]" action="{{ route('panel.user.money.add') }}" method="POST"
			  data-parsley-validate=""  data-parsley-trigger="blur">
			
			{{ csrf_field() }}

			<div class="xs-4 col  mt-8">
				<h4 class="c-2">{{ __('Agregar fondos') }}</h4>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5 class="mt-m-48">{{ __('Cantidad') }}</h5>
				</div>
				<div class="xs-4 m-6 col">
					<input class="modal__input  |  h3 c-2" type="number" name="money" step="any" required><span class="h3">€</span>
				</div>
			</div>

			<div class="clearfix mb-16">
				<div class="xs-4 m-3 col  text-right-m">
					<h5>{{ __('Tarjeta de crédito') }}</h5>
				</div>
				<div class="xs-4 m-9 col">
					@if(count(auth()->user()->cards) > 0)
						@foreach(auth()->user()->cards as $card)
							<input type="radio" id="checkbox-{{ $card->ID }}" class="custom-radio__input" name="tarjeta" value="{{ $card->ID }}" {{ ($loop->first) ? 'checked' : ''}}>
							<label class="d-block" for="checkbox-{{ $card->ID }}">
								<p class="custom-radio__text  mb-0">
									<span class="custom-radio__new"></span>
									{{ $card->EXTRA->NUM }}     {{ $card->EXTRA->EXP }}
									<a class="small-text  |  d-inblock  pmv-0 ml-24" href="#" onclick="event.preventDefault(); document.getElementById('remove-card-{{ $card->ID }}').submit();">
										{{ __('Eliminar tarjeta') }}
									</a>
								</p> 
							</label>
						@endforeach
						<input type="radio" id="checkbox-nueva-tarjeta" class="custom-radio__input" value="new" name="tarjeta">
						<label class="d-block" for="checkbox-nueva-tarjeta">
							<p class="custom-radio__text  mb-0">
								<span class="custom-radio__new"></span>
								{{ __('Agregar una tarjeta de crédito') }}
							</p> 
						</label>
					@else
						<input type="radio" id="checkbox-nueva-tarjeta" class="custom-radio__input" value="new" name="tarjeta" checked>
						<label class="d-block" for="checkbox-nueva-tarjeta">
							<p class="custom-radio__text  mb-0">
								<span class="custom-radio__new"></span>
								{{ __('Agregar una tarjeta de crédito') }}
							</p> 
						</label>
					@endif
						

				</div>
			</div>

			<div class="clearfix">
				<div class="xs-4 m-3 col false"></div>
				<div class="xs-4 m-9 col">
					<input class="button button--small  |  mt-40" type="submit" value="{{ __('Agregar fondos') }}">
				</div>
			</div>

		</form>

		@if(count(auth()->user()->cards) > 0)
			@foreach(auth()->user()->cards as $card)
				<form id="remove-card-{{ $card->ID }}" action="{{ route('panel.user.tj.remove') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
			        <input type="hidden" name="card" value="{{ $card->ID }}">
			    </form>
		   	@endforeach
		@endif



		<div class="separator--horizontal  |  mt-56"></div>


		<div class="xs-4 col  mt-8">
			<h4 class="c-2  mt-56">{{ __('Transferencia bancaria') }}</h4>
			<p class="mb-32">{{ __('Ingresa dinero en tu cuenta Brickstarter mediante una transferencia bancaria directa desde tu banco. Recuerda que el dinero puede tardar entre 2 o 3 días laborables.') }}</p>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Titular de la cuenta') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<div class="custom-input  bg-5">Lemon Way</div>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Banco') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<div class="custom-input  bg-5">Banco de Sabadell</div>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('IBAN') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<div class="custom-input  bg-5">ES1900815375930001180327</div>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('BIC') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<div class="custom-input  bg-5">BSABESBBXXX</div>
			</div>
		</div>

		<div class="clearfix mb-16">
			<div class="xs-4 m-3 col  text-right-m">
				<h5 class="pt-m-8">{{ __('Asunto') }}</h5>
			</div>
			<div class="xs-4 m-9 col">
				<div class="custom-input  bg-5">{{ auth()->user()->lemonway_id }}</div>
				<p class="small-text">{{ __('Es importante únicamente poner este código en el Asunto de la Transferencia para que se añada a tu cuenta Brickstarter.') }}</p>
			</div>
		</div>
	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
