<div class="xs-4 col">
	<h4 class="mt-64 mb-24">{{ __('Quizá te pueda interesar') }}</h4>
</div>

@foreach($related as $post)
	
	<div class="xs-4 m-4 col  mb-32">
		<a class="c-1 ch-s" href="{{ $post->url }}">
			<div class="xs-4 d-block   bg" style="height: 170px; background-image: url('{{ $post->imageThumb }}')" ></div>
			<h5>{{ $post->title }}</h5>
		</a>
	</div>

@endforeach