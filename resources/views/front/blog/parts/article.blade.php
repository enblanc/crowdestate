{{-- 
	==========================================================================
	#BLOG - ARTICLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Cada una de las entradas del blog.
	*
	*
	* AVISOS
	*
	* 	- Falta integrar.
	*	- Acordarse de la etiqueta alt con el título.
	*
	*
	--}}








{{--==========================================================================
	#PART 
	========================================================================== --}}

	<article class="article  |  xs-4  clearfix  mb-64">
		<a class="article__link  |  c-1 ch-s" href="{{ $post->url }}">


			<div class="xs-4 l-6 col">
				<img class="d-block mv-8" src="{{ $post->imageThumb }}" 
					 alt="{{ $post->title }}">
			</div>


			<div class="xs-4 l-6 col">
				<h5 class="article__category  |  c-2  mt-l-0 mb-0">{{ $post->categories->implode('name', ', ') }}</h5>
				<h4 class="mv-8 pr-l-80">{{ $post->title }}</h4>
				<p  class="mv-8">{{ $post->excerpt }}</p>
			</div>
			

		</a>
	</article>