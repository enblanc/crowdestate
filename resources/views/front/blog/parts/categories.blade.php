{{-- 
	==========================================================================
	#BLOG - CATEGORIES
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Menú de categorías del blog.
	*
	*
	* AVISOS
	*
	* 	- El 1º es el estado activo.
	*	- El resto links.
	*
	*
	--}}








{{--==========================================================================
	#PART 
	========================================================================== --}}

	<h4 class="c-2  mt-0">{{ __('Categorías') }}</h4>

	<ul class="pl-0  mb-56">

		@if($page == 'home')
			<li class="no-list">
				<img class="mr-16" src="{{ asset('assets/img/flecha.svg') }}" alt="">
				<span class="text-underline">{{ __('Todos') }}</span>
			</li>
		@else
			<li class="no-list">
				<a class="c-2 ch-m" href="{{ route('front.blog.index') }}">{{ __('Todos') }}</a>
			</li>
		@endif

		@foreach($categories as $category)
			@if($category->id == $page)
				<li class="no-list">
					<img class="mr-16" src="{{ asset('assets/img/flecha.svg') }}" alt="">
					<span class="text-underline">{{ $category->name }}</span>
				</li>
			@else
				<li class="no-list">
					<a class="c-2 ch-m" href="{{ route('front.blog.categories', $category->slug) }}">{{ $category->name }}</a>
				</li>
			@endif
			
		@endforeach
	</ul>


	<h4 class="c-2  mt-0">{{ __('Feed') }}</h4>

	<ul class="pl-0  mb-56">
		<li class="no-list">
			<a class="c-2 ch-m" target="_blank" href="{{ route('front.blog.feed.xml') }}">
				<span class="c-2 ch-m">{{ __('Suscripción Feed') }}</span>
				<img class="mr-16" src="{{ asset('assets/img/rss.svg') }}" alt="">
			</a>
		</li>
	</ul>


