{{-- 
	==========================================================================
	#BLOG
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Archivo del blog. Sirve tanto para la home del blog como para las 
	* categorías. Consta de 2 bloques:
	*	- Categories (carpeta parts)
	*	- article (cp)
	*
	*
	* AVISOS
	*
	* 	- Añadir paginación por defecto donde está comentado.
	* 	- Un article para cada entrada.
	* 	- Paginado a ??
	*	- En la home del blog no debería haber ninguna categoría activa en el
	*	  menú.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<section  class="mv-80">
			<div class="wrapper">
					
				@include('front/blog/parts/header')



				



				<div class="xs-4 m-8 l-9 col  ph-0  right">
					
					@each('front/blog/parts/article', $posts, 'post')

					{{-- Aquí paginación por defecto --}}

					{!! $posts->links() !!}

				</div>

				<div class="xs-4 m-4 l-3 col">

					@include('front/blog/parts/categories')

				</div>



			</div>
		</section>

	@endsection

	




{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}

	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}"></script>
	@endsection 
