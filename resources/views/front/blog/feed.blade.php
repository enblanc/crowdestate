<?xml version="1.0" encoding="UTF-8"?>

<rss version="2.0"
    xmlns:content="http://purl.org/rss/1.0/modules/content/"
    xmlns:wfw="http://wellformedweb.org/CommentAPI/"
    xmlns:dc="http://purl.org/dc/elements/1.1/"
    xmlns:atom="http://www.w3.org/2005/Atom"
    xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
    xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
    >
    <channel>
        <title>Brickstarter</title>
        <link>https://brickstarter.com/</link>
        <description>Brickstarter Blog Fee</description>
        @if(count($posts) > 0)
            @foreach($posts as $post)
                <item>
                    <title>{{ htmlspecialchars($post->title) }}</title>
                    <link>{{ $post->url }}</link>
                    <description><![CDATA[{{ $post->getMetaDesc() }}]]></description>
                    <content:encoded><![CDATA[{!! ($post->content) !!}]]></content:encoded>

                    @foreach($post->categories as $category)
                    <category><![CDATA[{{ $category->name }}]]></category>
                    @endforeach

                    <pubDate>{{ \Carbon\Carbon::parse($post->publish_date)->tz('UTC')->toRfc2822String() }}</pubDate>
                    @if($post->getFirstMedia('post_feature'))
                    <enclosure type="image/jpeg" length="{{ $post->getFirstMedia('post_feature')->size }}" url="{{ url($post->imageFull) }}" />
                    @endif
                    
                </item>
            @endforeach
        @endif
    </channel>
</rss>