{{-- 
	==========================================================================
	#SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Archivo del blog. Sirve tanto para la home del blog como para las 
	* categorías. Consta de 2 bloques:
	*	- Categories (carpeta parts)
	*	- article (cp)
	*
	*
	* AVISOS
	*
	* 	- Todos los enlaces de categorías deberían estar clicables 
	*	  (ninguno seleccionado).
	*	- dentro de .article__style va todo lo del wysiwyg.
	*	- acordarse de los alts
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<section  class="mv-80">
			<div class="wrapper">

				@include('front/blog/parts/header')



				<div class="xs-4 l-12 col right  ph-0">
					<div class="xs-4 col">
					


						{{-- #CABECERA --}}
						<h4 class="c-m  mv-0">{{ $post->categories->implode('name', ', ') }}</h4>
						<h2>{{ $post->title }}</h2>

						<img class="d-block  mv-32" src="{{ $post->imageFull }}" 
							 alt="{{ $post->title }}">

						<div class="xs-4 m-4 left">
							<p>{{ $post->fechaFormat }}</p>
						</div>
						<div class="xs-4 m-8 right  text-right-m">
							<h5 class="d-block d-inblock-m  mb-16 mb-m-8">{{ __('Compartir') }}:</h5>

							<a class="share__circle  |  pmv-0 ml-0 ml-m-16 ml-l-32" 
							   href="http://www.facebook.com/share.php?u={{ $post->url }}&title={{ $post->title }}" target="_blank">
									@include('front/svg/facebook', ['class' => ''])</a>

							<a class="share__circle  |  pmv-0 ml-16 ml-l-32"
							   href="http://twitter.com/intent/tweet?status={{ $post->title }}+{{ $post->url }}" target="_blank">
									@include('front/svg/twitter', ['class' => ''])</a>

							<a class="share__circle  |  pmv-0 ml-16 ml-l-32" 
							   href="http://www.linkedin.com/shareArticle?mini=true&url={{ $post->url }}&title={{ $post->title }}&source={{ $post->url }}" target="_blank">
									@include('front/svg/linkedin', ['class' => ''])</a>
						</div>
						{{-- FIN CABECERA --}}

						
						<div class="clearfix"></div>


						{{-- #NOTICIA --}}
						<div class="article__style  |  both  mt-48 mt-m-0">


							{!! $post->content !!}

						</div>
						{{-- FIN NOTICIA --}}



					</div>
					
					@if($related->count() > 0)
						@include('front/blog/parts/related')
					@endif

				</div>



				<div class="xs-4 l-3 col hide">

					@include('front/blog/parts/categories')

				</div>



			</div>
		</section>

	@endsection

	




{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}

	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}"></script>
	@endsection 
