@extends('front/layouts/dashboard')

@section('content')
    <div class="wrapper mt-20">
        {{-- @include('front/dashboard-inversion/parts/estado') --}}
        @php
            $properties = auth()->user()->getRealProperties([5]);
        @endphp
        <div class="xs-4 col">
            <div class="xs-4 m-6 col">
                <h4 class="c-2  mb-24">{{ __('Inmuebles') }}</h4>
            </div>
            <div class="xs-4 col" id="inmuebles-">
                @each('front/dashboard-inversion/parts/inmuebles', $properties, 'property')
            </div>
        </div>

        {{-- Vendidos --}}
        @php
            $properties = auth()->user()->getRealProperties([1, 2, 3, 4]);
        @endphp
        @if ($properties->count())
            <div class="xs-4 col">
                <div class="xs-4 m-6 col">
                    <h4 class="c-2  mb-24">{{ __('Vendidos') }}</h4>
                </div>
                <div class="xs-4 col">
                    @each('front/dashboard-inversion/parts/inmuebles', $properties, 'property')
                </div>
            </div>
        @endif
    </div>
@endsection

{{-- JS --}}
@section('custom_plugin_js')
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="/assets/js/plugins/parsleyjs.min.js"></script>
    <script src="{{ mix('assets/js/main.js') }}" async></script>
@endsection
