<div class="xs-4 col"  v-if="propertyActual">
	<div class="xs-4 m-8">
		<h4 class="c-2  mt-40">{{ __('Estado de mis inversiones') }}</h4>
		<select name="" id="" class="custom-input custom-input--select  |  mb-32" v-model="actualProperty">
			<option v-for="(property, key) in resultados" :value="key" v-text="property.nombre"></option>
		</select>
	</div>


	<div class="xs-4  bg-4  p-relative clearfix  pv-8 ph-16 mb-40">
		
		<div class="corner--top-right   [ js-open-info ]" data-ref="extra-info-1">
			@include('front/svg/duda', ['class' => 'd-block c-pointer'])
		</div>


		<div class="xs-4 m-6 col">
			<h4 class="mb-0">{{ __('Capital invertido') }}</h4>
			<h5 class="h4 c-2  nowrap  mt-0">@{{ propertyActual.capital_invertido }} € </h5>
		</div>
		<div class="xs-4 m-6 col">
			<h4 class="mb-0">{{ __('Capital comprometido') }}</h4>
			<h5 class="h4 c-2  nowrap  mt-0">@{{ propertyActual.capital_comprometido }} € </h5>
		</div>

		
		{{-- explicación --}}
		<div class="info__explanation  |  bg-1 all-w  pv-16 pr-40 pl-16" id="extra-info-1">
			@include('front/svg/close', ['class' => 'info__close js-close-info c-pointer'])
			<p class="small-text  mv-8">
				<strong>{{ __('Capital invertido') }}:</strong> {{ __('Dinero total invertido en inmuebles') }}
			</p>
			<p class="small-text  mv-8">
				<strong>{{ __('Capital comprometido') }}:</strong> {{ __('Dinero invertido en inmuebles en proceso de adquisición') }}
			</p>
		</div>
		{{-- fin explicación --}}
		
	</div>


	<div class="xs-4 m-6  left  pr-m-16">
		<div class="xs-4  bg-4  p-relative clearfix  pv-8 ph-16 mb-40">

			<div class="corner--top-right   [ js-open-info ]" data-ref="extra-info-2">
				@include('front/svg/duda', ['class' => 'd-block c-pointer'])
			</div>

			<div class="xs-4 col">
				<h4 class="mb-0">{{ __('Has ganado') }}</h4>
				<h5 class="h4 c-2  nowrap  mt-0">@{{ propertyActual.beneficios }} € </h5>
			</div>
			<div class="xs-4 col">
				<p class="xs-2 col mb-0 pl-0">{{ __('Promo') }}</p>
				<h5 class="xs-2 col h4 c-2  nowrap  mb-0">@{{ propertyActual.promo }} € </h5>
			</div>
			<div class="xs-4 col">
				<p class="xs-2 col mb-0 pl-0">{{ __('Dividendos') }}</p>
				<h5 class="xs-2 col h4 c-2  nowrap  mb-0">@{{ propertyActual.dividendos }} € </h5>
			</div>
			<div class="xs-4 col">
				<p class="xs-2 col mb-0 pl-0">{{ __('Plusvalía') }}</p>
				<h5 class="xs-2 col h4 c-2  nowrap  mb-0">@{{ propertyActual.plusvalia }} € </h5>
			</div>

			<div class="xs-4 col">
				<p class="xs-2 col mb-0 pl-0">{{ __('Marketplace') }}</p>
				<h5 class="xs-2 col h4 c-2  nowrap  mb-0">@{{ propertyActual.marketplace }} € </h5>
			</div>


			{{-- explicación --}}
			<div class="info__explanation  |  bg-1 all-w  pr-40 pl-16" id="extra-info-2">
				@include('front/svg/close', ['class' => 'info__close js-close-info c-pointer'])
				<p class="small-text  mv-8">
					<strong>{{ __('Has ganado') }}:</strong> {{ __('Beneficios totales obtenidos por los inmuebles menos los gastos y el IRPF') }}
				</p>
				<p class="small-text  mv-8">
					<strong>{{ __('Promo') }}:</strong> {{ __('Promociones utilizadas') }}
				</p>
				<p class="small-text  mv-8">
					<strong>{{ __('Dividendos') }}:</strong> {{ __('Beneficios obtenidos por los dividendos generados') }}
				</p>
				<p class="small-text  mv-8">
					<strong>{{ __('Plusvalía') }}:</strong> {{ __('Aumento del valor de tu inversión') }}
				</p>
			</div>
			{{-- fin explicación --}}


		</div>
	</div>


	<div class="xs-4 m-6  left  pl-m-16">
		<div class="xs-4  bg-4  p-relative clearfix  pv-8 ph-16 mb-40">

			<div class="corner--top-right   [ js-open-info ]" data-ref="extra-info-3">
				@include('front/svg/duda', ['class' => 'd-block c-pointer'])
			</div>


			<div class="xs-4 col">
				<h4 class="mb-0">{{ __('Revalorización inversión') }}</h4>
				<h5 class="h4 c-2  nowrap  mt-0">@{{ propertyActual.valor_patrimonial_total }} € </h5>
			</div>
			{{-- <div class="xs-4 m-6 col">
				<p class="mb-0">{{ __('Valor inmueble') }}</p>
				<h5 class="h4 c-2  nowrap  mt-0">@{{ propertyActual.valor_inmueble }} € </h5>
			</div>
			<div class="xs-4 m-6 col">
				<p class="mb-0">{{ __('Revalorización') }}</p>
				<h5 class="h4 c-2  nowrap  mt-0">@{{ propertyActual.revalorizacion }} € </h5>
			</div>
 --}}

			{{-- explicación --}}
			<div class="info__explanation  |  bg-1 all-w  pr-40 pl-16" id="extra-info-3">
				@include('front/svg/close', ['class' => 'info__close js-close-info c-pointer'])
				{{-- <p class="small-text  mv-8">
					<strong>{{ __('Valor Patrimonial') }}: </strong> {{ __('Parte proporcional de la revalorización actual del inmueble') }}
				</p>
				<p class="small-text  mv-8">
					<strong>{{ __('Valor inmueble') }}:</strong> {{ __('Valor actual del inmueble') }}
				</p> --}}
				<p class="small-text  mv-8">
					<strong>{{ __('Revalorización inversión') }}:</strong> {{ __('Aumento del valor de tu inversión actual') }}
				</p>
			</div>
			{{-- fin explicación --}}

		</div>
	</div>


	{{-- <div class="xs-4  p-relative both clearfix  bg-m all-w  pv-8 ph-16 mb-40">
		
		<div class="corner--top-right   [ js-open-info ]" data-ref="extra-info-4">
			@include('front/svg/duda-white', ['class' => 'd-block c-pointer'])
		</div>


		<div class="xs-4 m-9 col">
			<h4 class="">{{ __('Valor Patrimonial Total') }}</h4>
		</div>
		<div class="xs-4 m-3 col">
			<h4 class="nowrap">@{{ propertyActual.patrimonio_total }} €</h4>
		</div>

		<div class="info__explanation  |  bg-1 all-w  pt-16 pr-40 pl-16" id="extra-info-4">
			@include('front/svg/close', ['class' => 'info__close js-close-info c-pointer'])
			<p class="small-text  mv-8">
				<strong>{{ __('Valor Patrimonial Total') }}: </strong> {{ __('Es la suma del valor del inmueble más los beneficios totales obtenidos') }}
			</p>
		</div>

	</div> --}}
</div>