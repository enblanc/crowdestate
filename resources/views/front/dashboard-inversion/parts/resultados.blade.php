<section class="mv-m-72">
	<div class="wrapper">
		<div class="xs-4 col">
			<h5 class="left  pr-16 pt-8">{{ __('Resultados por meses') }}</h5>
			<div class="xs-4 m-6 l-5 left">
				<select name="" id="" class="custom-input custom-input--select" v-model="actualMonth">
					<option v-for="(month, index) in allMeses" :value="index" v-text="month"></option>
				</select>
			</div>
		</div>
	</div>

</section>







<section v-if="mesVista">

	{{-- #DIVIDENDOS --}}
	<div class="wrapper  clearfix">
		

		{{-- titulo --}}
		<div class="xs-4 col  mb-16">
			<h3 class="mv-8">{{ __('Dividendos') }}</h3>
			<div class="separator--horizontal"></div>
		</div>
		{{-- fin titulo --}}
	

	</div>
	<div class="custom-table__wrapper  |  wrapper  clearfix">
		<div class="custom-table__col  |  xs-4 col  mb-80">
			

			{{-- tabla --}}
			<table class="custom-table custom-table--total">
				<tr class="text-left">
					<th class="h4">{{ __('Concepto') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr>
					<td>{{ __('Dividendos generados') }}</td>
					<td class="text-right">@{{ number_format(mesVista.dividendos_generados, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Dividendos abonados') }}</td>
					<td class="text-right">@{{ number_format(mesVista.dividendos_abonar, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Dividendos en cartera') }}</td>
					<td class="text-right">@{{ number_format(mesVista.dividendos_cartera, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Total dividendos devengados') }}</td>
					<td class="text-right">@{{ number_format(mesVista.total_dividendos_devengados, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- fin tabla --}}


		</div>
	</div>
	{{-- FIN DIVIDENDOS --}}








	{{-- #RESULTADO EXPLOTACIÓN --}}
	<div class="wrapper  clearfix">
		

		{{-- titulo --}}
		<div class="xs-4 col  mb-16">
			<h3 class="mv-8">{{ __('Resultado de explotación') }}</h3>
			<div class="separator--horizontal"></div>
		</div>
		{{-- fin titulo --}}
	


	</div>
	<div class="custom-table__wrapper  |  wrapper  clearfix">

		<div class="custom-table__col  |  xs-4 col  mb-80">
			

			{{-- tabla 1 --}}
			<table v-if="mesVista.ingresos_alquiler > 0 || mesVista.otros_ingresos > 0" 
				   class="custom-table " :class="{'custom-table--parcial' : mesVista.ingresos_alquiler > 0 && mesVista.otros_ingresos > 0 }">
				<tr class="text-left">
					<th class="h4">{{ __('Ingresos') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr v-if="mesVista.ingresos_alquiler > 0">
					<td>{{ __('Ingresos alquiler') }}</td>
					<td class="text-right">@{{ number_format(mesVista.ingresos_alquiler, 2, ',', '.') }} €</td>
				</tr>
				<tr v-if="mesVista.otros_ingresos > 0">
					<td>{{ __('Otros Ingresos') }}</td>
					<td class="text-right">@{{ number_format(mesVista.otros_ingresos, 2, ',', '.') }} €</td>
				</tr>

				<tr v-if="mesVista.ingresos_alquiler > 0 && mesVista.otros_ingresos > 0">
					<td class="h4">{{ __('Ingresos totales') }}</td>
					<td class="h4 text-right">@{{ number_format(mesVista.ingresos_totales, 2, ',', '.') }} €</td>
				</tr>

			</table>
			{{-- fin tabla 1 --}}
			

			{{-- separador tabla 1 - 2 --}}
			<div class="separator-dashed--horizontal  |  mt-40 mb-16" v-if="mesVista.ingresos_alquiler > 0 || mesVista.otros_ingresos > 0" ></div>
			

			{{-- tabla 2 --}}
			<table class="custom-table  custom-table--parcial">
				<tr class="text-left">
					<th class="h4">{{ __('Gastos') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr>
					<td>{{ __('Publicidad y prensa') }}</td>
					<td class="text-right">@{{ number_format(mesVista.publicidad_prensa, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Otro material') }}</td>
					<td class="text-right">@{{ number_format(mesVista.otro_material, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('I + D') }}</td>
					<td class="text-right">@{{ number_format(mesVista.i_mas_d, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Arrendamientos') }}</td>
					<td class="text-right">@{{ number_format(mesVista.arrendamientos, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Comisión Brickstarter') }}</td>
					<td class="text-right">@{{ number_format(mesVista.comisiones_crowdestate, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Gestión') }}</td>
					<td class="text-right">@{{ number_format(mesVista.gestion, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Suministros') }}</td>
					<td class="text-right">@{{ number_format(mesVista.suministros, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('IBI') }}</td>
					<td class="text-right">@{{ number_format(mesVista.ibi, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Otras Tasas') }}</td>
					<td class="text-right">@{{ number_format(mesVista.otras_tasas, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td class="h4">{{ __('Gastos totales') }}</td>
					<td class="h4 text-right">@{{ number_format(mesVista.gastos_explotacion, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- fin tabla 2 --}}

			
			{{-- separador tabla 2 - 3 --}}
			<div class="separator-dashed--horizontal  |  mt-40 mb-16"></div>
			

			{{-- tabla 3 --}}
				<table class="custom-table" v-if="otrasPartidas">
					<tr class="text-left">
						<th class="h4">{{ __('Otras partidas') }}</th>
						<th class="h4 text-right">{{ __('Valor') }}</th>
					</tr>
					<tr>
						<td>{{ __('Amortización') }}</td>
						<td class="text-right">@{{ number_format(mesVista.amortizacion, 2, ',', '.') }} €</td>
					</tr>
					<tr>
						<td>{{ __('Imputación de subvenciones') }}</td>
						<td class="text-right">@{{ number_format(mesVista.subvenciones, 2, ',', '.') }} €</td>
					</tr>
					<tr>
						<td>{{ __('Resultado de la enajenación del inmovilizado') }}</td>
						<td class="text-right">@{{ number_format(mesVista.enajenacion_inmovilizado, 2, ',', '.') }} €</td>
					</tr>
				</table>
			
			{{-- fin tabla 3 --}}
			

			{{-- separador tabla 3 - 4 --}}
			<div class="separator-dashed--horizontal  |  mv-40"  v-if="otrasPartidas"></div>


			{{-- tabla 4: resultado total --}}
			<table class="custom-table custom-table--total  |  mt-40">
				<tr class="bg-m">
					<td class="h4 c-w">{{ __('Resultado de explotación') }}</td>
					<td class="h4 c-w text-right">@{{ number_format(mesVista.resultado_explotacion, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- fin tabla 4 --}}


		</div>
	</div>
	{{-- FIN RESULTADO EXPLOTACIÓN --}}





	
	{{-- #RESULTADO FINANCIERO --}}
	<div class="wrapper  clearfix">
		

		{{-- titulo --}}
		<div class="xs-4 col  mb-16">
			<h3 class="mv-8">{{ __('Resultado financiero') }}</h3>
			<div class="separator--horizontal"></div>
		</div>
		{{-- fin titulo --}}



	</div>
	<div class="custom-table__wrapper  |  wrapper  clearfix">


		<div class="custom-table__col  |  xs-4 col  mb-80">
			

			{{-- tabla --}}
			<table class="custom-table custom-table--total">
				<tr class="text-left">
					<th class="h4">{{ __('Concepto') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr>
					<td>{{ __('Ingresos financieros') }}</td>
					<td class="text-right">@{{ number_format(mesVista.ingresos_financieros, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Gastos financieros') }}</td>
					<td class="text-right">@{{ number_format(mesVista.gastos_financieros, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Enajenación instrumentos financiero') }}</td>
					<td class="text-right">@{{ number_format(mesVista.otros_instrumentos_financieros, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- tabla 1 --}}

			{{-- separador tabla --}}
			<div class="separator-dashed--horizontal  |  mv-40"></div>

			{{-- tabla 4: resultado total --}}
			<table class="custom-table custom-table--total  |  mt-40">
				<tr class="bg-m">
					<td class="h4 c-w">{{ __('Resultado financiero') }}</td>
					<td class="h4 c-w text-right">@{{ number_format(mesVista.resultado_financiero, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- fin tabla 4 --}}


		</div>
	</div>
	{{-- FIN RESULTADO FINANCIERO --}}







	{{-- #RESULTADO EJERCICIO --}}
	<div class="wrapper  clearfix">


		{{-- titulo --}}
		<div class="xs-4 col  mb-16">
			<h3 class="mv-8">{{ __('Resultado del ejercicio') }}</h3>
			<div class="separator--horizontal"></div>
		</div>
		{{-- fin titulo --}}
	


	</div>
	<div class="custom-table__wrapper  |  wrapper  clearfix">

		<div class="custom-table__col  |  xs-4 col  mb-80">


			{{-- tabla --}}
			<table class="custom-table custom-table--total">
				<tr class="text-left">
					<th class="h4">{{ __('Concepto') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr>
					<td>{{ __('Resultado antes del impuesto (Resultado de explotación + Resultado financiero)') }}</td>
					<td class="text-right">@{{ number_format(mesVista.resultado_antes_impuestos, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Impuestos') }}</td>
					<td class="text-right">@{{ number_format(mesVista.impuestos, 2, ',', '.') }} €</td>
				</tr>

			</table>
			{{-- fin tabla --}}

			{{-- separador tabla --}}
			<div class="separator-dashed--horizontal  |  mv-40"></div>

			{{-- tabla 4: resultado total --}}
			<table class="custom-table custom-table--total  |  mt-40">
				<tr class="bg-m">
					<td class="h4 c-w">{{ __('Resultado del ejercicio') }}</td>
					<td class="h4 c-w text-right">@{{ number_format(mesVista.resultados_ejercicio, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- fin tabla 4 --}}


		</div>
	</div>
	{{-- FIN RESULTADO EJERCICIO --}}






	{{-- #INFORMACION PATRIMONIAL --}}
	<div class="wrapper  clearfix">
		
		
		{{-- titulo --}}
		<div class="xs-4 col  mb-16">
			<h3 class="mv-8">{{ __('Información patrimonial') }}</h3>
			<div class="separator--horizontal"></div>
		</div>
		{{-- fin titulo --}}
	


	</div>
	<div class="custom-table__wrapper  |  wrapper  clearfix">

		<div class="custom-table__col  |  xs-4 col  mb-80">

			
			{{-- tabla --}}
			<table class="custom-table custom-table--total">
				<tr class="text-left">
					<th class="h4">{{ __('Concepto') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr>
					<td>{{ __('Revalorización del inmueble') }}</td>
					<td class="text-right">@{{ number_format(mesVista.revalorizacion_inmueble, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Valor actual del inmueble') }}</td>
					<td class="text-right">@{{ number_format(mesVista.valor_actual_inmueble, 2, ',', '.') }} €</td>
				</tr>
			</table>
			{{-- fin tabla --}}


		</div>
	</div>
	{{-- FIN INFORMACION PATRIMONIAL --}}






	{{-- #INFORMACION RESERVAS --}}
	<div class="wrapper  clearfix">


		{{-- titulo --}}
		<div class="xs-4 col  mb-16">
			<h3 class="mv-8">{{ __('Ocupación') }}</h3>
			<div class="separator--horizontal"></div>
		</div>
		{{-- fin titulo --}}
		
	

	</div>
	<div class="custom-table__wrapper  |  wrapper  clearfix">

	
		<div class="custom-table__col  |  xs-4 col  mb-80">
			

			{{-- tabla --}}
			<table class="custom-table custom-table--total">
				<tr class="text-left">
					<th class="h4">{{ __('Concepto') }}</th>
					<th class="h4 text-right">{{ __('Valor') }}</th>
				</tr>
				<tr>
					<td>{{ __('Número de reservas') }}</td>
					<td class="text-right">@{{ mesVista.numero_reservas }}</td>
				</tr>
				<tr>
					<td>{{ __('Grado de ocupación') }}</td>
					<td class="text-right">@{{ number_format(mesVista.grado_ocupacion, 2, ',', '.') }} %</td>
				</tr>
				<tr>
					<td>{{ __('Días reservados') }}</td>
					<td class="text-right">@{{ mesVista.dias_reservados }}</td>
				</tr>
				<tr>
					<td>{{ __('Días no reservados') }}</td>
					<td class="text-right">@{{ mesVista.dias_no_reservados }}</td>
				</tr>
				<tr>
					<td>{{ __('Días bloqueados') }}</td>
					<td class="text-right">@{{ mesVista.dias_bloqueados }}</td>
				</tr>
				<tr>
					<td>{{ __('Tarifa media por día') }}</td>
					<td class="text-right">@{{ number_format(mesVista.tarifa_media_dia, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Estimación ingresos anuales') }}</td>
					<td class="text-right">@{{ number_format(mesVista.estimacion_ingresos_anuales, 2, ',', '.') }} €</td>
				</tr>
				<tr>
					<td>{{ __('Grado satisfacción clientes') }}</td>
					<td class="text-right">@{{ number_format(mesVista.satisfaccion_clientes, 2, ',', '.') }} %</td>
				</tr>
				<tr>
					<td>{{ __('Número de invitados') }}</td>
					<td class="text-right">@{{ mesVista.numero_invitados }}</td>
				</tr>
			</table>
			{{-- fin tabla --}}


		</div>
	</div>
	{{-- FIN INFORMACION RESERVAS --}}





</section>