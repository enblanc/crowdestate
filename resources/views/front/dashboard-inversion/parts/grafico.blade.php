<section>	
	<div class="wrapper  mv-40">
		<div class="xs-4 m-6 col  pt-16">


			<a class="" href="{{ route('panel.user.inversion.general') }}">
				<img class="mr-8" src="{{ asset('assets/img/triangle-left.svg') }}" alt="">
				{{ __('Volver a mis inversiones') }}</a>

			


		</div>
		<div class="xs-4 m-6 col  text-right-m  pt-16">


			<a href="{{ $property->url }}">{{ __('Ver la ficha del inmueble') }}</a>


		</div>
	</div>

	<div class="wrapper">
		<div class="xs-4 l-12 col">


			<canvas id="myChart" width="400" height="250"></canvas>


		</div>

		<div class="clearfix"></div>
		{{-- <div class="xs-4 l-5 col  mt-40 mt-l-0">

			
			<div class="xs-4 m-6 l-12 left">
				<p class="mv-0">Resultado explotación previsto</p>
				<h4 class="mt-0">{{ $predicted->resultado_explotacion }} €</h4>
			</div>

			<div class="xs-4 m-6 l-12 left">
				<p class="mv-0">Rentabilidad prevista</p>
				<h4 class="mt-0">3,84 %</h4>
			</div>

			<div class="xs-4 m-6 l-12 left">
				<p class="mv-0">Rentabilidad de la plusvalía</p>
				<h4 class="mt-0">2,78 %</h4>
			</div>

			<div class="xs-4 m-6 l-12 left">
				<p class="mv-0">Rentabilidad total esperada</p>
				<h4 class="mt-0  c-m">4,02 %</h4>
			</div>


		</div> --}}
	</div>
</section>