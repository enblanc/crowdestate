@php
	$property = (object) $property
@endphp

<div class="xs-4 m-6 col  mb-48">
	<a href="{{ ($property->tipo == 4) ? route('panel.user.inversion.property', $property->id) : $property->url }}" class="property button__hover  |  exclude-svg  d-block pmv-0">
		{{-- IMAGEN --}}
		<div class="property__picture overlay overlay--vertical  |  p-relative all-w bg"
			style="background-image: url('{{ $property->imagen }}');">

			<div class="property__status {{ ($property->estado_id == 2) ? 'property__status--comprando' : '' }}  |  pv-8 ph-24">
				<h5 class="mv-0">{{ $property->estado }}</h5>
			</div>

			<div class="property__title  |  xs-4 ph-24 pb-16">
				<h4 class="mv-0">{{ $property->nombre }}</h4>
				<p class="small-text  mv-0">
					@include('front/svg/localizacion', ['class' => 'mr-4'])
					{{ $property->direccion }}
				</p>
			</div>

		</div>
		{{-- IMAGEN --}}

		{{-- INFO --}}
		<div class="property__info  |  clearfix  pt-24 ph-24 pb-32">

			<div class="left">
				<h5 class="c-m  pmv-0">{{ $property->fakeOrRealInvertido }} € ({{ $property->porcentajeCompletado }}%)</h5>
			</div>
			<div class="right">
				<h5 class="c-1  pmv-0">{{ $property->objetivo }} €</h5>
			</div>
			<div class="clearfix  pb-2"></div>

			<div class="bar  |  mv-8">
				<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
			</div>

			<p class="small-text c-1  left  pmv-0">{{ $property->fakeOrRealInversores }} {{ __('Inversores') }}</p>
			<p class="small-text c-1  right  pmv-0">{{ $property->remainingDays }}</p>

			<div class="clearfix  pb-2 mb-24"></div>

			<div class="xs-2 left  c-1  pr-16">
				<p class="mv-0 nowrap">{{ __('Has invertido') }}:</p>
				<h4 class="mv-0 nowrap">{{ $property->invertido }} €</h4>
			</div>
			<div class="xs-2 left  c-1  pr-16">
				<p class="mv-0 nowrap">{{ __('Has ganado') }}:</p>
				<h4 class="mv-0 nowrap">{{ $property->beneficios }} €</h4>
			</div>


			<div class="clearfix mb-24"></div>
			{{-- fin bloque de datos --}}

			<button class="button button--small button--full" >
				@if($property->tipo == 4)
					{{ __('Ver detalles') }}
				@else
					{{ __('Ver inmueble') }}
				@endif
			</button>

		</div>
		{{-- INFO --}}
	</a>
</div>
