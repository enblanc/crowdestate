{{-- 
	==========================================================================
	#PROPERTY SINGLE - HEADER
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#VISIBLE
	========================================================================== --}}

	<section class="overlay overlay--gradient  |  p-relative bg" style="background-image: url('{{ $property->getFirstMediaUrl($property->mediaCollection) }}');">
		<div class="property__foto  |  wrapper  p-relative  pv-40">


		
			{{-- #NOMBRE Y DIRECCIÓN --}}
			<div class="xs-4 m-6 col  all-w  pt-24  from-l">

				<h1 class="h2">{{ $property->nombre }}</h1>
				<p class="big-text">{{ $property->direccion }}</p>

			</div>
			{{-- FIN NOMBRE Y DIRECCIÓN --}}




			<div class="xs-4 m-1 col false  from-l"></div>




			{{-- #BLOQUE DATOS DESTACADOS --}}
			<div class="xs-4 m-6 l-5 col  from-l">
				<div class="xs-4  p-relative clearfix  bg-w-95  pv-24 ph-32">
					

					{{-- datos objetivo --}}
					<div class="left">
						<p class="small-text  pmv-0">{{ __('Financiado') }}</p>
						<h4 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h4>
					</div>
					<div class="right">
						<p class="small-text  pmv-0">{{ __('Total') }}</p>
						<h4 class="pmv-0">{{ formatThousandsNotDecimals($property->objetivo) }} €</h4>
					</div>
					<div class="clearfix  pb-2"></div>

					<div class="bar  |  mv-8">
						<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
					</div>

					<p class="small-text  left  pmv-0">{{ $property->fakeOrRealInversores }} {{ __('Inversores') }}</p>
					<p class="small-text  right  pmv-0">
						@if($property->state->id == 2)
							@if($property->porcentajeCompletado == 100)
								{{ __('Completado') }}
							@elseif($property->remainingDays < 0)
								{{ __('No conseguido') }}
							@else
								{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
							@endif
						@else
							{{ $property->state->nombre }}
						@endif
					</p>

					<div class="clearfix  pb-2 mb-32"></div>

					<div class="xs-2 left  pr-16">
						<p class="mv-0">{{ __('Has invertido') }}:</p>
						<h4 class="mv-0">{{ formatThousandsNotZero($invertido) }} €</h4>
					</div>
					<div class="xs-2 left  pr-16">
						<p class="mv-0">{{ __('Has ganado') }}:</p>
						<h4 class="mv-0">{{ formatThousandsNotZero($beneficios) }} €</h4>
					</div>

					<div class="clearfix  mb-24"></div>
					{{-- fin datos objetivo --}}




					

				</div>
			</div>
			{{-- FIN BLOQUE DATOS DESTACADOS --}}
			


		</div>
	</section>







{{--==========================================================================
	#MODAL
	========================================================================== --}}
	
	<section class="to-m">

		<div class="wrapper">
		{{-- #NOMBRE Y DIRECCIÓN --}}
			<div class="xs-4 m-6 col  pv-40">

				<h2 class="h3">{{ $property->nombre }}</h2>
				<p class="big-text">{{ $property->direccion }}</p>

			</div>

			<div class="m-6 col  only-m  mt-72">
				{{-- datos objetivo --}}
					
					<p class="small-text  pmv-0">{{ __('Financiado') }}</p>
					<h4 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h4>
					
					<p class="small-text  pmv-0">{{ __('Total') }}</p>
					<h4 class="pmv-0">{{ formatThousandsNotDecimals($property->objetivo) }} €</h4>
					
					<div class="clearfix  pb-2"></div>

					<div class="bar  |  mv-8">
						<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
					</div>

					<p class="small-text  left  pmv-0">{{ $property->fakeOrRealInversores }} {{ __('Inversores') }}</p>
					<p class="small-text  right  pmv-0">
						@if($property->remainingDays < 1)
							{{ __('Completado') }}
						@else
							{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
						@endif
					</p>

					<div class="clearfix  pb-2 mb-32"></div>
					{{-- fin datos objetivo --}}

					<div class="xs-2 left  pr-16">
						<p class="mv-0">{{ __('Has invertido') }}:</p>
						<h4 class="mv-0">{{ $invertido }} €</h4>
					</div>
					<div class="xs-2 left  pr-16">
						<p class="mv-0">{{ __('Has ganado') }}:</p>
						<h4 class="mv-0">{{ $beneficios }} €</h4>
					</div>
			</div>
			{{-- FIN NOMBRE Y DIRECCIÓN --}}
		</div>






			{{-- #BLOQUE DATOS DESTACADOS --}}
		<div class="separator--horizontal  only-xs"></div>
		<div class="wrapper  clearfix">
			<div class="xs-4  col">
				<div class="xs-4  p-relative clearfix  bg-w-95  pv-24">
					

					{{-- datos objetivo --}}
					<div class="only-xs">
					
						<p class="small-text  pmv-0">{{ __('Financiado') }}</p>
						<h4 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h4>
						
						<p class="small-text  pmv-0">{{ __('Total') }}</p>
						<h4 class="pmv-0">{{ formatThousandsNotDecimals($property->objetivo) }} €</h4>
						
						<div class="clearfix  pb-2"></div>

						<div class="bar  |  mv-8">
							<span class="bar__status  |  d-block" style="width: 100%;"></span>
						</div>

						<p class="small-text  left  pmv-0">{{ $property->fakeOrRealInversores }}  {{ __('Inversores') }}</p>
						<p class="small-text  right  pmv-0">
							@if($property->remainingDays < 1)
								{{ __('Completado') }}
							@else
								{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
							@endif
						</p>

					<div class="clearfix  pb-2 mb-32"></div>
						<div class="xs-2 left  pr-16">
							<p class="mv-0">{{ __('Has invertido') }}:</p>
							<h4 class="mv-0">{{ $invertido }} €</h4>
						</div>
						<div class="xs-2 left  pr-16">
							<p class="mv-0">{{ __('Has ganado') }}:</p>
							<h4 class="mv-0">{{ $beneficios }} €</h4>
						</div>

					</div>

					
					{{-- fin datos objetivo --}}

					
					
						
					



				</div>
			</div>
			{{-- FIN BLOQUE DATOS DESTACADOS --}}
		</div>
		<div class="separator--horizontal"></div>
	</section>



