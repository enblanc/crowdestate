{{-- 
	==========================================================================
	#DASHBOARD - MI INVERSIÓN
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/inversion')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<div id="inversion" class="mb-80">
			@include('front/dashboard-inversion/parts/header')
			@include('front/dashboard-inversion/parts/grafico')
			@include('front/dashboard-inversion/parts/resultados')
		</div>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		{{-- <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script> --}}
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script>
			var apiUrl = "{{ route('property.info', $property->id) }}";
		</script>
		<script src="{{ mix('assets/js/mi-inversion.js') }}" async></script>
	@endsection 
