{{--
	==========================================================================
	#DASHBOARD - RESUMEN PATRIMONIAL
	==========================================================================
	*
	* DESCRIPCIÓN
	*
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT
	========================================================================== --}}

	@extends('front/layouts/dashboard-inversiones')






{{--==========================================================================
	#CONTENT
	========================================================================== --}}

	@section('content')

		<div class="xs-4 l-11  hide  pr-l-24" id="inversion">

			@include('front/dashboard-inversion/parts/estado')

		</div>

	@endsection






{{--==========================================================================
	#ASSETS
	========================================================================== --}}

	{{-- CSS --}}


	@section('header_css')
		{{-- <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script> --}}
    @endsection
	{{-- @section('header_assets') @endsection
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script>
			var apiUrl = "{{ route('api.user.inversiones') }}";
		</script>
		<script src="{{ mix('assets/js/mis-inversiones.js') }}" async></script>
	@endsection
