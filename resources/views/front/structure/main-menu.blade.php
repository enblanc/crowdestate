<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
    {{ csrf_field() }}
</form>
<header class="main-header bg-4" id="header">
    <div class="wrapper">
        <div class="xs-2 l-2 col">
            <a href="{{ route('home') }}">
                <img class="main-header__isotipe | d-block" src="{{ asset('assets/img/logo-brickstarter.svg') }}" alt="brickstarter">
            </a>
        </div>
        <div class="xs-2 col text-right  hamburger-parent">
            <button class="hamburger hamburger--spin   [ js-menu ]" type="button">
                <span class="hamburger__box  d-block">
                    <span class="hamburger__inner"></span>
                </span>
            </button>
        </div>
        <nav class="main-menu | xs-2 m-10 col  text-right">
            @php
                $menuMargin = auth()->check() ? 'mv-16' : 'mv-8';
            @endphp

            <ul class="main-menu__list | ph-0 {{ $menuMargin }}">

                <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-16 mv-l-0 mr-l-24">
                    <a href="{{ route('front.comoFunciona') }}"
                        class="main-menu__anchor  |  c-2 ch-m">{{ __('Cómo funciona') }}</a>
                </li>

                <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-16 mv-l-0 mr-l-24">
                    <a href="{{ route('front.property.grid') }}"
                        class="main-menu__anchor  |  c-2 ch-m">{{ __('Inmuebles') }}</a>
                </li>

                <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-16 mv-l-0 mr-l-24">
                    <a href="{{ route('contact') }}" class="main-menu__anchor  |  c-2 ch-m">{{ __('Contacto') }}</a>
                </li>

                <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-16 mv-l-0 mr-l-24">
                    <a href="{{ route('front.blog.index') }}"
                        class="main-menu__anchor  |  c-2 ch-m">{{ __('Blog') }}</a>
                </li>

                @auth
                    <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-16 mv-l-0 mr-l-24">
                        <a href="{{ route('front.market.list') }}"
                            class="main-menu__anchor  |  c-2 ch-m">{{ __('Mercado') }}</a>
                    </li>
                @endauth

                @ifNoTManteinance
					<li
						class="main-menu__item  |  d-inblock-l no-list from-xl  pv-0 mv-16 mv-l-0 mr-l-24  lang-selector  [ js-open-dropdown ]">

						<span class="c-2 from-m">{{ strtoupper(current_locale()) }}</span>
						<div class="main-menu__dropdown lang-container">

							<div class="dropdown__container  |  text-left  pl-24 pv-24">
								<ul class="pmv-0 pl-0">
									<li class="no-list pmv-0">
										<a class="c-2 ch-m"
											href="{{ get_url(Route::currentRouteName(), Route::current() ? Route::current()->parameters() : [], 'es') }}{{ !auth()->id() ? '?locale=es' : '' }}"
											hreflang="es">
											ES
										</a>
									</li>

									<li class="no-list pmv-0">
										<a class="c-2 ch-m"
											href="{{ get_url(Route::currentRouteName(), Route::current() ? Route::current()->parameters() : [], 'en') }}{{ !auth()->id() ? '?locale=en' : '' }}"
											hreflang="en">
											EN
										</a>
									</li>

									<li class="no-list pmv-0">
										<a class="c-2 ch-m"
											href="{{ get_url(Route::currentRouteName(), Route::current() ? Route::current()->parameters() : [], 'de') }}{{ !auth()->id() ? '?locale=de' : '' }}"
											hreflang="de">
											DE
										</a>
									</li>
								</ul>
							</div>
						</div>


						<div class="lang-mobile">
							<a class="uppercase c-2 ch-m"
								href="{{ get_url(Route::currentRouteName(), Route::current() ? Route::current()->parameters() : [], 'es') }}{{ !auth()->id() ? '?locale=es' : '' }}"
								hreflang="es">
								ES
							</a>
							<span class="c-2 ch-m">|</span>
							<a class="uppercase c-2 ch-m"
								href="{{ get_url(Route::currentRouteName(), Route::current() ? Route::current()->parameters() : [], 'en') }}{{ !auth()->id() ? '?locale=en' : '' }}"
								hreflang="en">
								EN
							</a>
							<span class="c-2 ch-m">|</span>
							<a class="uppercase c-2 ch-m"
								href="{{ get_url(Route::currentRouteName(), Route::current() ? Route::current()->parameters() : [], 'de') }}{{ !auth()->id() ? '?locale=de' : '' }}"
								hreflang="de">
								DE
							</a>
						</div>

					</li>
                @endNotManteinance

                @if (auth()->check())
                    <li
                        class="main-menu__item main-menu__item--fixed  |  d-inblock-l no-list  text-center  pv-0 mv-0   [ js-open-dropdown ]">
                        <a class="main-menu__anchor main-menu__drop-link  |  h5 c-1  mv-0 c-pointer from-l ">{{ auth()->user()->nombre }}
                            <span class="select__triangle ml-8"></span></a>

                        <a class="main-menu__anchor main-menu__drop-link  |  h5 c-1 not-l mv-0"
                            href="
							@if (auth()->user()->properties->count() > 0) {{ route('panel.user.inversion.general') }}
							@elseif (auth()->user()->activado)
								{{ route('panel.user.money.show') }}
							@else
								{{ route('panel.user.accredit.show') }} @endif
							
						">{{ auth()->user()->nombre }}
                            <span class="select__triangle ml-8"></span></a>

                        <div class="main-menu__dropdown">
                            <div class="dropdown__container  |  text-left  pl-24 pv-24">

                                <h5 class="mv-0"><img class="mr-8" src="{{ asset('assets/img/icono-perfil.svg') }}"
                                        alt="">{{ __('Perfil') }}</h5>
                                <ul class="pmv-0 pl-0">
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.basic.show') }}"
                                            class="small-text c-2 ch-m pmv-0">{{ __('Datos básicos') }}</a></li>
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.password.show') }}"
                                            class="small-text c-2 ch-m pmv-0">{{ __('Cambiar contraseña') }}</a></li>
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.accredit.show') }}"
                                            class="small-text c-2 ch-m pmv-0">{{ auth()->user()->activado ? __('Datos Personales') : __('Acreditar cuenta') }}</a>
                                    </li>

                                    @role('super|admin|captador')
                                        <li class="no-list pmv-0"><a href="{{ route('panel.user.invite') }}"
                                                class="small-text c-2 ch-m pmv-0">{{ __('Gana') }} 15€</a></li>
                                    @endrole

                                </ul>

                                <h5 class="mt-16 mb-0"><img class="mr-8"
                                        src="{{ asset('assets/img/icono-inversiones.svg') }}"
                                        alt="">{{ __('Inversiones') }}</h5>
                                <ul class="pmv-0 pl-0">
                                    <li class="no-list pmv-0">
                                        @if (auth()->user()->properties->count() > 0)
                                            <a href="{{ route('panel.user.inversion.resumen') }}"
                                                class="small-text small-text c-2 ch-m pmv-0 {{ auth()->user()->properties->count() == 0? 'pe-none': '' }}">
                                            @else
                                                <a class="small-text ch-m pmv-0 c-3 pe-none" href="">
                                        @endif
                                        {{ __('Resumen patrimonial') }}</a>
                                    </li>
                                    <li class="no-list pmv-0">
                                        @if (auth()->user()->properties->count() > 0)
                                            <a href="{{ route('panel.user.inversion.general') }}"
                                                class="small-text c-2 ch-m pmv-0 {{ auth()->user()->properties->count() == 0? 'pe-none': '' }}">
                                            @else
                                                <a class="small-text ch-m pmv-0 c-3 pe-none" href="">
                                        @endif
                                        {{ __('Mis inversiones') }}</a>
                                    </li>
                                </ul>

                                <h5 class="mt-16 mb-0"><img class="mr-8"
                                        src="{{ asset('assets/img/icono-fondos.svg') }}"
                                        alt="">{{ __('Fondos') }}</h5>
                                <ul class="pmv-0 pl-0">
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.money.show') }}"
                                            class="small-text c-2 ch-m pmv-0 {{ !auth()->user()->activado ? 'c-3 pe-none' : '' }}">{{ __('Añadir fondos') }}</a>
                                    </li>
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.moneyof.show') }}"
                                            class="small-text c-2 ch-m pmv-0 {{ !auth()->user()->activado ? 'c-3 pe-none' : '' }}">{{ __('Retirar Fondos') }}</a>
                                    </li>
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.bankaccounts.show') }}"
                                            class="small-text c-2 ch-m pmv-0 {{ !auth()->user()->activado ? 'c-3 pe-none' : '' }}">{{ __('Cuentas bancarias') }}</a>
                                    </li>
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.transactions.show') }}"
                                            class="small-text c-2 ch-m pmv-0 {{ !auth()->user()->activadoOrInvertido ? 'c-3 pe-none' : '' }}">{{ __('Movimientos') }}</a>
                                    </li>
                                </ul>

                                <h5 class="mt-16 mb-0"><img class="mr-8"
                                        src="{{ asset('assets/img/icono-fondos.svg') }}"
                                        alt="">{{ __('Mercado') }}</h5>
                                <ul class="pmv-0 pl-0">
									
                                    <li class="no-list pmv-0">
										<a class="small-text c-2 ch-m pmv-0" href="{{ route('panel.user.marketplace.autoinvest') }}"> 
											{{ __('Auto Invest') }}
										</a>
									</li>
                                    <li class="no-list pmv-0"><a href="{{ route('panel.user.marketplace.orders') }}"
                                            class="small-text c-2 ch-m pmv-0 {{ !auth()->user()->activado ? 'c-3 pe-none' : '' }}">{{ __('Ver mis órdenes') }}</a>
                                    </li>
                                    <li class="no-list pmv-0"><a
                                            href="{{ route('panel.user.marketplace.historical') }}"
                                            class="small-text c-2 ch-m pmv-0 {{ !auth()->user()->activado ? 'c-3 pe-none' : '' }}">{{ __('Histórico') }}</a>
                                    </li>
                                </ul>

                                <a class="d-block  c-1 ch-m  mb-0" href="{{ route('logout') }}"
                                    onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><img
                                        class="mr-8" src="{{ asset('assets/img/icono-logout.svg') }}"
                                        alt="">{{ __('Cerrar sesión') }}</a>
                            </div>
                        </div>
                    @else
                    <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-24 mv-m-0">
                        <a href="{{ route('registro') }}" class="button button--small"
                            {!! current_locale() == 'de' ? 'style="padding: 0 30px;"' : '' !!}>{{ __('Registro') }}</a>
                    </li>
                    <li class="main-menu__item  |  d-inblock-l no-list  pv-0 mv-16 mv-l-0 ml-l-24">
                        <a href="{{ route('login') }}"
                            class="main-menu__anchor  |  c-2 ch-m">{{ __('Iniciar sesión') }}</a>
                    </li>
                @endif
            </ul>
        </nav>
    </div>
</header>
