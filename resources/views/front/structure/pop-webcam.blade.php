{{-- 
	==========================================================================
	#POPUP - REGISTRO
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup" id="webcam">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}




							{{-- #MODAL CONTENT --}}
								<h4 class="text-center">{{ __('Elegir imagen por webcam') }}</h4>

								<div class="separator--horizontal"></div>

								<div class="ph-24 mt-24">
									<template v-if="!webcam.photo">
										<div id="my_camera" class="center"></div>

										<div class="col center text-center">
											<a href="#" class="button button--secondary button--small |  mt-16 mt-m-16" v-on:click="createSnapshot">{{ __('Crear fotografía') }}</a>
										</div>
									</template>
									
									<template v-else>
										<div id="resultado" class="center">
											<img :src="webcam.photo">
										</div>
										<div class="col center text-center">
											<a href="#" class="button button--secondary  |  ph-24 fs-18 mt-16 mt-m-16" v-on:click="createWebcamService">{{ __('Volver a hacer') }}</a>
											<a href="#" class="button button--secondary  |  ph-24 fs-18 mt-16 mt-m-16" v-on:click="snapshotChossen" v-if="!webcam.saving">{{ __('Elegir fotografía') }}</a>

											<div class="button button--secondary button--loader  text-center |  ph-24 fs-18 mt-16 mt-m-16"  v-if="webcam.saving">{{ __('Guardando imagen') }}</div>

										</div>							
									</template>
									
									

									
								</div>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





