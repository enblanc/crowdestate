<!DOCTYPE html>
	<html lang="es">
		<head>

			<meta charset="utf-8">
			<meta http-equiv="X-UA-Compatible" content="IE=edge">
			<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
			<meta name="work-script" content="{{ csrf_token() }}">
			<link rel=“author” href="humans.txt" />
			{{-- <link rel="icon" type="" href="{{ asset('assets/favicon/favicon.ico') }}"> --}}
			<link rel="icon" type="image/png" href="{{ asset('assets/favicon/favicon.png') }}">

			<meta name="apple-mobile-web-app-capable" content="yes">

			{!! SEO::generate() !!}
			
			<script src="https://use.typekit.net/jvm3hha.js"></script>
			<script>try{Typekit.load({ async: true });}catch(e){}</script>

			<link rel="stylesheet" href="{{ mix('assets/css/main.css') }}">

			@yield('header_css')
			@yield('header_assets')

			<script>
		        window.CrowdEstate = {!! json_encode([
		            'csrfToken' => csrf_token(),
		        ]) !!};
		    </script>

			<!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
			<script type="text/javascript">

			    window.cookieconsent_options = {"message": "{{ __('Este sitio web utiliza cookies para que usted tenga la mejor experiencia de usuario.') }}" ,"dismiss": "{{ __('Aceptar') }}" ,"learnMore": "{{ __('Más información.') }}","link": "{{ route('front.legal') }}","theme":false};
			</script>

			<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.10/cookieconsent.min.js" defer></script>
			<!-- End Cookie Consent plugin -->

			<!--

			/yy+.               `-:-`
			MMMMmo-`            .///.
			MMMMMMms-`           ``` 
			MMMNdNMMNs:`             
			MMMm-:smMMmy/`       ``` 
			MMMm- `-smMMNy/.    :ydh:
			sdds`   `-smMMNh+.  sNMMs
			````      `-odNMNd+.sNMMs
			            `.+dNMNdmMMMs
			 ```           .+hNMMMMMs
			://:`            `/hNMMMs
			-::.               `:yhy-
                                                 
			-->
			
			<!-- Google analytics for brickstarter.com -->
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
			  ga('create', 'UA-102451082-1', 'auto');
			  ga('send', 'pageview');
			</script>

			<!-- Facebook Pixel Code -->
			<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window,document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				 fbq('init', '592568924556947'); 
				fbq('track', 'PageView');
				</script>
				<noscript>
				 <img height="1" width="1" 
				src="https://www.facebook.com/tr?id=592568924556947&ev=PageView
				&noscript=1"/>
			</noscript>
			<!-- End Facebook Pixel Code -->

			<!-- Hotjar Tracking Code for brickstarter.com -->
			<script>
			    (function(h,o,t,j,a,r){
			        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
			        h._hjSettings={hjid:580341,hjsv:5};
			        a=o.getElementsByTagName('head')[0];
			        r=o.createElement('script');r.async=1;
			        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
			        a.appendChild(r);
			    })(window,document,'//static.hotjar.com/c/hotjar-','.js?sv=');
			</script>

			<!-- Facebook Pixel Code -->
			<script>
				!function(f,b,e,v,n,t,s)
				{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};
				if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
				n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];
				s.parentNode.insertBefore(t,s)}(window, document,'script',
				'https://connect.facebook.net/en_US/fbevents.js');
				fbq('init', '194565681162276');
				fbq('track', 'PageView');
			</script>
			<noscript><img height="1" width="1" style="display:none"
				src="https://www.facebook.com/tr?id=194565681162276&ev=PageView&noscript=1"
			/></noscript>
			<!-- End Facebook Pixel Code -->
			
		</head>
		<body class="body {{ ( isset($loaded) ) ? $loaded : '' }}">	
				@include('front/structure/main-menu')



