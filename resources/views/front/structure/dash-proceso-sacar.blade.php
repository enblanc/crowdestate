	@if(auth()->user()->properties->count() > 0 && auth()->user()->transactions()->whereType(1)->count() == 0)
		<section class="bg-m  from-l">
			<div class="wrapper">
				<div class="xs-4 col  all-w  text-center">
					<h5 class="d-inblock v-top  mv-8">{{ __('¡Comienza a ganar dinero!') }}</h5>
					
					<div class="step__item step__item--has-succeed  |  d-inblock v-top  ml-16">
						<span class="step__index  |  v-middle">1</span>
						<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Invierte en un inmueble') }}</p>
					</div>

					<div class="step__item  {{ (auth()->user()->hasValidBankAccounts()) ? 'step__item--has-succeed' : '' }} |  d-inblock v-top  ml-16">
						@if (!auth()->user()->hasValidBankAccounts())
							<a href="{{ route('panel.user.bankaccounts.show') }}">
						@endif
							<span class="step__index  |  v-middle">2</span>
							<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Añade una cuenta bancaria') }}</p>
						@if (!auth()->user()->hasValidBankAccounts())
							</a>
						@endif
					</div>

					<div class="step__item  |  d-inblock v-top  ml-16">
						@if (auth()->user()->transactions()->whereType(1)->count() == 0)
							<a href="{{ route('panel.user.moneyof.show') }}">
						@endif
							<span class="step__index  |  v-middle">3</span>
							<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Retira fondos') }}</p>	
						@if (auth()->user()->transactions()->whereType(1)->count() == 0)
							</a>
						@endif

										
					</div>
				</div>
			</div>
		</section>
	@endif