{{--
	==========================================================================
	#ESTRUCTURA - FOOTER
	==========================================================================
	*
	* DESCRIPCIÓN
	*
	* Footer general de la web.
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}


	@php

		if(!isset($errors)) {
			$errors = false;
		}

	@endphp





{{--==========================================================================
	#CONTENT
	========================================================================== --}}

	<footer class="bg-4  pv-80">

		<div class="wrapper  mt-16 mb-40 mb-m-80">

			<div class="xs-4 m-4 col  mb-40 mb-m-0">
				<h4>{{ __('Redes sociales') }}</h4>
				<a class="share__circle share__circle--small share__circle--dark  |  pmv-0 mr-16 mr-l-32" href="https://www.facebook.com/brickstarters/" target="_blank">
					@include('front/svg/facebook', ['class' => ''])
				</a>
				<a class="share__circle share__circle--small share__circle--dark  |  pmv-0 mr-16 mr-l-32" href="https://twitter.com/brickstarters" target="_blank">
					@include('front/svg/twitter', ['class' => ''])
				</a>
				<a class="share__circle share__circle--small share__circle--dark  |  pmv-0 " href="https://www.linkedin.com/company/brickstarter" target="_blank">
					@include('front/svg/linkedin', ['class' => ''])
				</a>
			</div>
			@if($errors != false && count($errors) > 0)
				@if (count($errors->newsletter) > 0)
		            @foreach ($errors->newsletter->all() as $error)
		                <p class="c-error  text-center">{{ $error }}</p>
		            @endforeach
				@endif
			@endif
			<form action="{{ route('newsletter.add') }}" method="post" class="validate" data-parsley-trigger="blur">

				{{ csrf_field() }}

				<div class="xs-4 m-4 col">
					<h4>{{ __('Newsletter') }}</h4>
					<input type="email" class="custom-input custom-input--small" placeholder="{{ __('Escribe tu e-mail') }}" name="email" required>
				</div>


				<div class="xs-4 m-4 l-3 col">
					<input class="button button--secondary button--small button--full  |  mt-16 mt-m-72" type="submit" value="{{ __('Suscribirme') }}">
				</div>
			</form>
		</div>


		<div class="wrapper  mb-16">

			<div class="xs-4 m-6 col   mb-40 mb-l-0">
				<h4>Brickstarter</h4>
				<a class="c-2 ch-m  d-block d-inline-l  mr-m-32" href="{{ route('front.quienesSomos') }}">{{ __('Quienes somos') }}</a>
				<a class="c-2 ch-m  d-block d-inline-l  mr-m-32" href="{{ route('contact') }}">{{ __('Contacto') }}</a>
				<a class="c-2 ch-m  d-block d-inline-l  mr-m-32" href="{{ route('front.blog.index') }}">{{ __('Blog') }}</a>
				<a class="c-2 ch-m" href="https://brickstarter.targetcircle.com/signup?ref=brickstarter" target="_blank">{{ __('Prescriptores') }}</a>
			</div>

			<div class="xs-4 m-6 col">
				<h4>Invertir</h4>
				<a class="c-2 ch-m  d-block d-inline-l  mr-m-32" href="{{ route('front.property.grid') }}">{{ __('Inmuebles') }}</a>
				<a class="c-2 ch-m  d-block d-inline-l  mr-m-32" href="{{ route('front.comoFunciona') }}">{{ __('Cómo funciona') }}</a>
				<a class="c-2 ch-m" href="{{ route('front.faq.general') }}">{{ __('Preguntas frecuentes') }}</a>
			</div>

			

		</div>

		<div class="wrapper xs-4 mt-64">
			<div class="xs-4 col">
				<div class="separator--horizontal  bg-3"></div>
			</div>
			<div class="xs-4 flex items-center">
				<div class="l-fifth xs-2 col mt-24 pl-32 pr-32">
					<img src="{{ asset('assets/img/logos/union-europea.png') }}" />
				</div>
				<div class="l-fifth xs-2 col mt-24 pl-32 pr-32">
					<img src="{{ asset('assets/img/logos/invattur.png') }}" />
				</div>
				<div class="l-fifth xs-2 col mt-24 pl-32 pr-32">
					<img src="{{ asset('assets/img/logos/valencia-startups.png') }}" />
				</div>

				<div class="l-fifth xs-2 col mt-24 pl-32 pr-32">
					<img src="{{ asset('assets/img/logos/camara.png') }}" />
				</div>


				<div class="l-fifth xs-2 col mt-24 pl-32 pr-32">
					<img src="{{ asset('assets/img/logos/catedra.png') }}" />
				</div>
				
			</div>
		</div>

		<div class="wrapper xs-4 mt-24">
			<div class="xs-4 col">
				<div class="separator--horizontal  bg-3"></div>
			</div>
			<div class="xs-2 col">			
				<p class="small-text c-2  mt-24 mb-0 text-left"><a href="http://www.lemonway.eu/" title="Lemon Way - Payment Institution for the new economy"><img style="vertical-align:middle; width: 150px" src="{{ asset('assets/img/Lemon-Way-Logo-Inline-200px.png') }}" /></a>&nbsp; partner.</p>
				<p class="small-text c-2 mt-8 text-left"> A European payment institution accredited in France by the ACPR (CIB 16568).</p>
			</div>
			
			<div class="xs-2 col">
				<p class="small-text c-2  mt-24 text-right">&copy; Copyright {{ date('Y') }} - Brickstarter - <a href="{{ route('front.legal') }}">{{ __('Términos legales') }}</a></p>
			</div>
			
		</div>
	</footer>


	<div class="loader  modal modal--popup modal--is-open  |  bg-w">
		<div class="xs-4  d-table  fh">
			<div class="d-cell v-middle  text-center">
				
				{{-- <div class="spinner">
					<div class="dot1"></div>
					<div class="dot2"></div>
				</div> --}}
				<div class="sk-folding-cube">
					<div class="sk-cube1 sk-cube"></div>
					<div class="sk-cube2 sk-cube"></div>
					<div class="sk-cube4 sk-cube"></div>
					<div class="sk-cube3 sk-cube"></div>
				</div>

			</div>
		</div>

	</div>







{{--==========================================================================
	#SCRIPTS
	========================================================================== --}}

	<script>
	var routeLogin = "/login";
	var routeRegister = "/register";
	var routeResetEmail = "/password/email";
	var siteKey = "{{ env('INVISIBLE_RECAPTCHA_SITEKEY') }}";
	var localeIso = "{{ localization()->getCurrentLocale() }}";
	
	</script>

	{{-- <script type="text/javascript" src="{{ route('js.localization') }}"></script> --}}
	{{-- <script src="//www.google.com/recaptcha/api.js?render=explicit" async defer></script> --}}
	<script>var _submitForm,_captchaForm,_captchaSubmit;</script>

	{{-- Ancla scripts --}}
	@yield('custom_plugin_js')
	@yield('custom_section_js')

	{{-- <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-xxxxxxxx-1', 'auto');
		ga('send', 'pageview');
	</script> --}}



	<script>
		@if(!auth()->check())
		$(document).ready(function() {
			if(window.location.href.search("[?&]logIn") != -1) {
				window.location = "{{ route('login') }}";
				// $('#login').addClass('modal--is-open');
			}

			if(window.location.href.search("[?&]registerP") != -1) {
				window.location = "{{ route('registro') }}";
				// $('#registro').addClass('modal--is-open');
			}

			if(window.location.href.search("[?&]altaOk") != -1) {
				$('#obtener').addClass('modal--is-open');
			}

			if(window.location.href.search("[?&]recuperarOk") != -1) {
				$('#recuperar-ok').addClass('modal--is-open');
			}
		});

		@endif
	</script>

	<!-- LinkedIn Insight Tag -->
	<script type="text/javascript">
	    _linkedin_partner_id = "1126676";
	    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
	    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
	</script>
	<script type="text/javascript">
	    (function() {
		 	var s = document.getElementsByTagName("script")[0];
		 	var b = document.createElement("script");
		 	b.type = "text/javascript";
		 	b.async = true;
		 	b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
		 	s.parentNode.insertBefore(b, s);
		 })();
	</script>
	<noscript>
	    <img alt="" height="1" src="https://dc.ads.linkedin.com/collect/?pid=1126676&fmt=gif" style="display:none;" width="1"/>
	</noscript>
	<!-- LinkedIn Insight Tag -->
	
</body>
</html>
