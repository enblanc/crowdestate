{{-- 
	==========================================================================
	#POPUP - REGISTRO
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup modal--scroll" id="registro">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24  mv-m-48">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}


							{{-- #MODAL CONTENT --}}
								<h4 class="text-center">Alta de usuario</h4>

								<div class="separator--horizontal"></div>

								<a class=" d-block text-center  mt-16 mb-0" href="{{ route('login') }}">
										Ya tengo cuenta, iniciar sesión.</a>

								<form class="ph-24 mt-16" action="" id="register-form"  v-on:submit.prevent 
								data-parsley-trigger="blur">

									<div v-for="error in errors.register">
										<error-form :error="error"><error-form>
									</div>

									<h5 class="mv-0">Datos personales</h5>
									<input class="custom-input" type="text" placeholder="Nombre" name="nombre" v-model="register.nombre" required>
									<input class="custom-input" type="text" placeholder="Primer apellido" name="apellido1" v-model="register.apellido1" required>
									<input class="custom-input" type="text" placeholder="Segundo apellido" name="apellido2" v-model="register.apellido2" required>

									<h5 class="mt-4 mb-0">Cuenta</h5>
									<input class="custom-input" type="email" placeholder="E-mail" name="email" v-model="register.email"  required>
									<input class="custom-input" type="password"  placeholder="Contraseña" name="password"  v-model="register.password" required data-parsley-minlength="8" data-parsley-pattern="^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$">
									<p class="small-text  mv-0 pt-0 pb-8">Mínimo 8 caracteres con números y letras</p>
									<input class="custom-input  mb-24" type="text" name="referred" placeholder="Código promocional (Opcional)">

									<div class="clearfix p-relative  checkbox__wrapper">
									
										<input type="checkbox" id="terminos" class="custom-checkbox__input" name="terminos" required
										data-parsley-errors-messages-disabled>
										<label for="terminos">
											<span class="custom-checkbox__new  |  v-top  mt-8"></span> 
											<p class="custom-checkbox__text  |  small-text  mv-0">Acepto los <a target="_blank" href="{{ route('front.legal') }}">Términos y Condiciones de Brickstarter</a> y <a target="_blank" href="{{ route('front.legal-lemonway') }}">Lemonway</a> </p>
									 	</label>

								 	</div>

								 	<div id='recaptcha' class="g-recaptcha"></div>
								 	
								 	<button class="button button--full  |  mt-24" type="submit" 
								 		 v-show="!register.loading" @click="validate"> Crear una cuenta </button>

								 	<div class="button button--full button--loader  |  mt-24" v-show="register.loading">Enviando</div> 
									
								</form>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





