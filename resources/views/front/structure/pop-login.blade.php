{{-- 
	==========================================================================
	#POPUP - REGISTRO
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup" id="login">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}




							{{-- #MODAL CONTENT --}}
								<h4 class="text-center">{{ __('Iniciar sesión') }}</h4>

								<div class="separator--horizontal"></div>

								<form class="ph-24 mt-24" id="login-form" method="POST" action="{{ route('login') }}" v-on:submit.prevent data-parsley-trigger="blur">
									{{ csrf_field() }}
									<div v-for="error in errors.login">
										<error-form :error="error"><error-form>
									</div>
									

									<input class="custom-input" type="email" name="email" placeholder="E-mail" v-model="login.email" required>
									<input class="custom-input" type="password" name="password" placeholder="Contraseña" v-model="login.password" required>

									<a class="d-block text-center  mt-16 mb-32   [ js-modal ]" modal-ref="recuperar" href="#">
										{{ __('He olvidado mi contraseña') }}</a>

								 	<input class="button button--full  |  mb-16" type="submit" value="Iniciar sesión" 
								 	v-on:click="postLogin" :disabled="login.button" v-if="!login.loading">
									
									<div class="button button--full button--loader  |  mb-16" v-else>{{ __('Enviando') }}</div> 

									<a class="small-text d-block text-center  mv-8" href="{{ route('registro') }}">
										{{ __('Aún no tengo cuenta, quiero registrarme') }}.</a>
								</form>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





