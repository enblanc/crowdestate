{{-- 
	==========================================================================
	#POPUP - REGISTRO
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup   " id="obtener">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}




							{{-- #MODAL CONTENT --}}
								<h4 class="text-center">{{ __('Alta de usuario') }}</h4>

								<div class="separator--horizontal"></div>

								<div class="ph-24 mt-24">

									@include('front/svg/succeed', ['class' => 'center  mv-32'])
									
									<h4 class="text-center  mb-0">{{ __('¡Gracias por registrarte!') }}</h4>
									<p  class="text-center  mt-0">{{ __('En breves recibirás un e-mail para confirmar tu cuenta en Brickstarter') }}.</p>

									
								</div>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





