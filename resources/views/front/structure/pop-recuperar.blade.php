{{-- 
	==========================================================================
	#POPUP - REGISTRO
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#MODAL
	========================================================================== --}}

		<div class="modal modal--popup" id="recuperar">
			<div class="modal__container  |  xs-4 d-table">
				<div class="d-cell v-middle">
					<div class="wrapper  ph-0">
						<div class="modal__popup  |  xs-4 m-7 l-6 col center  clearfix  bg-w  pt-24 pb-24">
							

							{{-- #CLOSE MODAL --}}
							<span class="modal__close   c-pointer  [ js-modal-close ]">
								@include('front/svg/close', ['class' => 'modal__svg'])
							</span>
							{{-- FIN CLOSE MODAL --}}

							


							{{-- #MODAL CONTENT --}}
								<h4 class="text-center">{{ __('Recuperar contraseña') }}</h4>

								<div class="separator--horizontal"></div>

								<div v-if="email.result">
									<p class="text-center  mb-32">{{ __('Se ha enviado un correo a tu cuenta de email') }}.</p>
								</div>

								<div v-for="error in errors.email">
									<error-form :error="error"><error-form>
								</div>

								<form class="ph-24 mt-24" action="{{ route('password.email') }}" id="email-form" v-on:submit.prevent data-parsley-trigger="blur">

									<p class="text-center  mb-32">{{ __('Indícanos tu e-mail para recuperar tu contraseña') }}</p>

									<input class="custom-input" type="email" placeholder="E-mail" name="email" required>

								 	<input class="button button--full  |  mt-72 mb-16" type="submit" value="{{ __('Recuperar mi contraseña') }}" v-on:click="postResetPassword" v-if="!email.loading">

								 	<div class="button button--full button--loader  |  mb-16" v-else>{{ __('Enviando') }}</div> 
									
								</form>
							{{-- FIN MODAL CONTENT --}}

							
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>





