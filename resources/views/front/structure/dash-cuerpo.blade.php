<section class="pb-80 mb-80">
			<div class="wrapper  mt-40">
				<div class="xs-4 m-10 col">
					<h3 class="mv-0">{{ __('Dashboard') }} {{-- de {{ auth()->user()->nombre }} --}}
						<span class="d-block d-inline-l  h4 c-b  ml-l-24">
								<span class="c-2">{{ __('Fondos') }}:</span> {{ formatThousandsNotZero(auth()->user()->balance) }} €
								@if(auth()->user()->importeRegalo != 0)
									<span class="ml-24 c-2">{{ __('Regalo') }}:</span> {{ formatThousandsNotZero(auth()->user()->importeRegalo) }} €
								@endif
								{{--
									// CHEMA: QUITARLO PORQUE NADIE LO ENTIENDE
									@if(auth()->user()->getPorcentaje5() != 0)
										<span class="ml-24 c-2">{{ __('5% TAE') }}:</span> {{ formatThousandsNotZero(auth()->user()->getPorcentaje5()) }} €
									@endif
								--}}
						</span>
					 </h3>
				</div>
				<div class="xs-4 m-2 col  text-right-m  pt-24">
					<a href="#" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Cerrar sesión') }}</a>
				</div>
				<div class="xs-4 col">
					<div class="separator--horizontal-inverted"></div>
				</div>
			</div>
			<div class="wrapper">
				<div class="xs-4 l-3 col">

					<div class="tab-selector__active  |  p-relative to-m  mt-32   [ js-open-menu ]">
						<h5 class="tab-selector__title tab-selector__selected  |  c-w  to-m  text-left pmv-0 pl-24">
						@php
						switch (Route::currentRouteName()) {
						    case 'panel.user.basic.show':
						        echo _('Datos básicos');
						        break;
						    case 'panel.user.password.show':
						    	echo __('Cambiar contraseña');
						        break;
						    case 'panel.user.accredit.show':
						    	echo (auth()->user()->activado) ? __('Datos Personales') : __('Acreditar cuenta');
						        break;
						    case 'panel.user.money.show':
						    	echo __('Añadir fondos');
						        break;
						    case 'panel.user.get.money':
						    	echo __('Retirar fondos');
						        break;
						    case 'panel.user.bankaccounts.show':
						    	echo __('Cuentas bancarias');
						        break;
						    case 'panel.user.transactions.show':
						    	echo __('Movimientos');
						        break;
					        case 'panel.user.inversion.general':
						    	echo __('Mis Inversiones');
						        break;
						    default:
						    	echo __('Menú');
						    	break;
						}
						@endphp
							{{-- {{ Route::currentRouteName() }} --}}
						</h5>
					</div>

					<div class="drop-menu  |  border--right  clearfix	">
						
						<h4 class="mt-40"><img class="mr-8" width="20px" src="{{ asset('assets/img/icono-perfil.svg') }}" alt="">{{ __('Perfil') }}</h4>
						<ul class="pmv-0 pl-0">

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.basic.show' ) ? 'text-underline c-1' : 'c-2' }} ]" 
								   href="{{ route('panel.user.basic.show') }}">
								   		{{ __('Datos básicos') }}</a>
							</li>
							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() =='panel.user.password.show' ) ? 'text-underline c-1' : 'c-2' }} ]" 
								   href="{{ route('panel.user.password.show') }}">
								   		{{ __('Cambiar contraseña') }}</a>
							</li>

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.accredit.show' ) ? 'text-underline c-1' : 'c-2' }} ]" 
								   href="{{ route('panel.user.accredit.show') }}">
								   		{{ (auth()->user()->activado) ? __('Datos Personales') : __('Acreditar cuenta') }}</a>
							</li>
							
							@if (auth()->user() && auth()->user()->certifications()->count())	
								<li class="no-list pv-0">
									<a class="ch-m pmv-0
										[ {{ ( Route::currentRouteName() == 'panel.user.certifications.index' ) ? 'text-underline c-1' : 'c-2' }} ]" 
										href="{{ route('panel.user.certifications.index') }}">
										{{ __('Certificaciones de retenciones') }}
									</a>
								</li>
							@endif

							@role('super|admin|captador')
							
							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.invite' ) ? 'text-underline c-1' : 'c-2' }} ]" 
								   href="{{ route('panel.user.invite') }}">
								   		{{ __('Gana') }}  15€</a>
							</li>

							@endrole

						</ul>



						<h4 class="mt-40"><img class="mr-8" width="20px" src="{{ asset('assets/img/icono-inversiones.svg') }}" alt="">{{ __('Inversiones') }}</h4>
						<ul class="pmv-0 pl-0">

							<li class="no-list pv-0">
								@if (auth()->user()->properties->count() > 0)
									<a class="ch-m pmv-0 
									   [ {{ ( Route::currentRouteName() == 'panel.user.inversion.resumen' ) ? 'text-underline c-1' : 'c-2' }} ]"
									   href="{{ route('panel.user.inversion.resumen') }}">
								@else 
									<a class="ch-m pmv-0 c-3 pe-none" href="">
								@endif
										{{ __('Resumen patrimonial') }}</a>
							</li>

							<li class="no-list pv-0">
								@if (auth()->user()->properties->count() > 0)
									<a class="ch-m pmv-0 
									   [ {{ ( Route::currentRouteName() == 'panel.user.inversion.general' ) ? 'text-underline c-1' : 'c-2' }} ]"
									   href="{{ route('panel.user.inversion.general') }}">
								@else 
									<a class="ch-m pmv-0 c-3 pe-none" href="">
								@endif
										{{ __('Mis inversiones') }}</a>
							</li>

							

						</ul>

						<h4 class="mt-40"><img class="mr-8" width="20px" src="{{ asset('assets/img/icono-fondos.svg') }}" alt="">{{ __('Fondos') }}</h4>
						<ul class="pmv-0 pl-0">

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.money.show' ) ? 'text-underline c-1' : (auth()->user()->activado ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.money.show') }}"> 
										{{ __('Añadir fondos') }}</a>
							</li>

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.moneyof.show' ) ? 'text-underline c-1' : (auth()->user()->activado ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.moneyof.show') }}">
										{{ __('Retirar Fondos') }}</a>
							</li>

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.bankaccounts.show' ) ? 'text-underline c-1' : (auth()->user()->activado ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.bankaccounts.show') }}">
										{{ __('Cuentas bancarias') }}</a>
							</li>

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.transactions.show' ) ? 'text-underline c-1' : (auth()->user()->activadoOrInvertido ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.transactions.show') }}">
										{{ __('Movimientos') }}</a>
							</li>


						</ul>

						

						<h4 class="mt-40"><img class="mr-8" width="20px" src="{{ asset('assets/img/icono-marketplace.svg') }}" alt="">{{ __('Mercado') }}</h4>

						<ul class="pmv-0 pl-0">

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.marketplace.autoinvest' ) ? 'text-underline c-1' : (auth()->user()->activado ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.marketplace.autoinvest') }}"> 
										{{ __('Auto Invest') }}</a>
							</li>

							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.marketplace.orders' ) ? 'text-underline c-1' : (auth()->user()->activado ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.marketplace.orders') }}"> 
										{{ __('Ver mis órdenes') }}</a>
							</li>


							<li class="no-list pv-0">
								<a class="ch-m pmv-0   
								   [ {{ ( Route::currentRouteName() == 'panel.user.marketplace.historical' ) ? 'text-underline c-1' : (auth()->user()->activado ? 'c-2' : 'c-3 pe-none') }} ]"
								   href="{{ route('panel.user.marketplace.historical') }}"> 
										{{ __('Histórico') }}</a>
							</li>

						</ul>
						

					</div>
				</div>
				<div class="xs-4 l-9 col  ph-0">
					
					@yield('content')

				</div>
			</div>
		</section>