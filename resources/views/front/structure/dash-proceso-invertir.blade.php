	@if(auth()->user()->properties->count() == 0)
		<section class="bg-m  from-l">
			<div class="wrapper">
				<div class="xs-4 col  all-w  text-center">
					<h5 class="d-inblock v-top  mv-8">{{ __('¡Invierte y comienza a ganar dinero!') }}</h5>
					
					<div class="step__item step__item--has-succeed  |  d-inblock v-top  ml-16">
						<span class="step__index  |  v-middle">1</span>
						<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Datos básicos') }}</p>
					</div>

					<div class="step__item {{ (auth()->user()->profile->estado == 1) ? 'step__item--has-succeed' : '' }} |  d-inblock v-top  ml-16">
						@if ( auth()->user()->profile->estado != 1 )
							<a href="{{ route('panel.user.accredit.show') }}">
						@endif
							<span class="step__index  |  v-middle">2</span>
							<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Acredita tu cuenta') }}</p>
						@if ( auth()->user()->profile->estado != 1 )
							</a>
						@endif
					</div>

					<div class="step__item {{ (auth()->user()->balance != 0) ? 'step__item--has-succeed' : '' }} |  d-inblock v-top  ml-16">
						@if ( auth()->user()->balance == 0 && auth()->user()->profile->estado == 1 )
							<a href="{{ route('panel.user.money.show') }}">
						@endif
							<span class="step__index  |  v-middle">3</span>
							<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Ingresa fondos') }}</p>
						@if ( auth()->user()->balance == 0 && auth()->user()->profile->estado == 1 )
							</a>
						@endif
						
					</div>

					<div class="step__item  |  d-inblock v-top  ml-16">
						@if ( auth()->user()->balance != 0 && auth()->user()->balance != 0 && auth()->user()->profile->estado == 1 )
							<a href="{{ route('front.property.grid') }}">
						@endif
							<span class="step__index  |  v-middle">4</span>
							<p class="step__text  |  small-text d-inblock  mv-8">{{ __('Invierte en un inmueble') }}</p>
						@if ( auth()->user()->balance != 0 && auth()->user()->balance != 0 && auth()->user()->profile->estado == 1 )
							</a>
						@endif					
					</div>
				</div>
			</div>
		</section>
	@endif