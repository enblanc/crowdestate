{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#CONTENT 
	========================================================================== --}}


	<div class="accordion  |  mb-40">
		<div class="accordion__header  |  c-pointer   [ js-accordion-open ]">
			<h4 class="accordion__title  |  left  mt-0 mb-8">{{ $title }} </h4>
			<span class="  |  right"><img class="accordion__arrow" src="{{ asset('assets/img/arrow.svg') }}" alt=""></span>
			<div class="separator--horizontal  both"></div>
		</div>
		<div class="accordion__content  |  o-hidden">
			{{ $slot }}
		</div>
	</div>