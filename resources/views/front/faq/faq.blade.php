{{-- 
	==========================================================================
	#PROPERTY SINGLE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section>
			<div class="wrapper">
				<div class="xs-4 col  text-center">
					<h1 class="h3  mt-40 mb-56 mb-m-72">{{ __('Preguntas frecuentes') }}</h1>
				</div>

				<div class="xs-4 m-4 l-3 col">
					<h4 class="c-3  mt-0">{{ __('Categorías') }}</h4>

					<ul class="pl-0  mb-56">
						@foreach ($faqTypes as $faq)
							<li class="no-list">

								@if($faqType->id == $faq->id)
									<img class="mr-16" src="{{ asset('assets/img/flecha.svg') }}" alt="">
									<span class="text-underline">{{ $faq->nombre }}</span>
								@else
									<a class="c-2 ch-m" href="{{ route('front.faq.category', $faq->id) }}">{{ $faq->nombre }}</a>
								@endif

							</li>
						@endforeach
					</ul>
				</div>

				<div class="xs-4 m-8 l-9 col">

					@foreach ($faqTypes[$faqType->id-1]->faqItems as $faq)					


					{{-- route('front.faq.category', $faq->id) --}}

					@component('front/faq/assets/faq-item')
						@slot('title', $faq->nombre)
						{!! $faq->texto !!}
					@endcomponent

					@endforeach

				</div>
			</div>
		</section>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		{{-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> --}}
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
