{{-- 
	==========================================================================
	#HOME
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página de proyectos. Consta de 2 bloques:
	*	- Intro (carpeta parts)
	*	- Grid (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general-esp', [
			'type'       => 'general',
			'breadcrumb' => __('Contacto')
		])






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section>
			<div class="wrapper mt-120">
				
			
				<div class="xs-4 col  text-center">

					<h2 class="c-2">Declaración sobre uso de cookies</h2>

				</div>
				<div class="xs-4 m-10 l-8 col center  pt-12">

					<p>Una cookie es un pequeño archivo de texto que se instala en su terminal de ordenador (u otro dispositivo, como un teléfono móvil) a través de los sitios web que usted visita.</p>


					<h4 class="c-2">1. Para qué se utilizan las cookies</h4>

					<p>Son ampliamente utilizados para conocer la actividad de los visitantes al sitio web, con el fin de mejorar el mismo a partir del almacenamiento y recuperación de los datos de navegación de los usuarios. Las cookies también se utilizan para proporcionar información sobre la navegación de los usuarios a los propietarios de sitios web y sus colaboradores, con el fin de analizar el uso que realizan de la página web y personalizar el contenido.
					</p>


					<h4 class="c-2">2. Información importante sobre cómo utilizamos las cookies</h4>

					<p>Se utilizan las cookies a efectos estadísticos, funcionales y para mejorar nuestros servicios a través del análisis del uso que realizan los visitantes a nuestro sitio web, incluyendo el uso de nuestros servicios de banca por internet. En este último caso, podemos utilizar cookies para almacenar algunos datos de acceso, pero nunca las contraseñas. Las cookies hacen que sea más fácil para el usuario conectarse, navegar y utilizar el sitio web en futuras visitas.</p>

					<p>Algunos de los anunciantes y organizaciones de análisis web con los que se trabaja incorporan cookies en el ordenador del usuario a través del presente sitio web. Nuestros sitios también tienen enlace a infobolsa. Infinety S.L. no controla las cookies utilizadas por esta web externa. Para más información sobre estas cookies web ajenas, aconsejamos revisar sus propias políticas de cookies.
					</p>


					<h4 class="c-2">3. Cookies propias</h4>

					<p>La siguiente tabla muestra las cookies que son fijadas por Infinety S.L. en el sitio web de la entidad y la finalidad para la que se utiliza cada una:</p>

					<h5 class="c-2">Cookies ténicas</h5>

					<p>Las cookies técnicas son aquellas imprescindibles y estrictamente necesarias para el correcto funcionamiento de un portal web y la utilización de las diferentes opciones y servicios que ofrece.</p>

					<p>Por ejemplo, las que sirven para el mantenimiento de la sesión, la gestión del tiempo de respuesta, rendimiento o validación de opciones, utilizar elementos de seguridad, compartir contenido con redes sociales, etc.</p>

					<h5 class="c-2">Cookies de personalización</h5>

					<p>Estas cookies permiten al usuario especificar o personalizar algunas características de las opciones generales de la página web. Por ejemplo, definir el idioma, configuración regional o tipo de navegador.<span class="redactor-invisible-space"><br></span>
					</p>


					<h4 class="c-2">4. Cookies de terceros</h4>

					<p>Existe una serie de proveedores que establecen las cookies con el fin de proporcionar determinados servicios. La siguiente tabla muestra las cookies que se establecen en la página web <a href="http://www.infinety.es" target="_blank" title="Infinety agencia digital">www.infinety.es</a>, por parte de terceros, los fines para los que se utilizan y los enlaces a páginas web donde se puede encontrar más información sobre las cookies:</p>

					<h5 class="c-2">Cookies analíticas</h5>

					<p>Las cookies de Google Analytics se utilizan con el fin de analizar y medir cómo los visitantes usan este sitio web. La información sirve para elaborar informes que permiten mejorar este sitio. Estas cookies recopilan información en forma anónima, incluyendo el número de visitantes al sitio, cómo han llegado al mismo y las páginas que visitó mientras navegaba en nuestro sitio web:</p>

					<p><a href="https://www.google.com/intl/es/policies/privacy/" target="_blank" title="Google politica de privacidad">https://www.google.com/intl/es/policies/privacy/</a></p>
					
					<h5 class="c-2">Cookies tècniques</h5>
					</p><p>Las cookies bannerPrincipal-[ruta]: indica que banners de una página se han visualizado ya para la siguiente vez mostrar uno nuevo.
					</p><p>Las cookies mpLocker-[ruta] : una vez visualizada una maxipubli, no se vuelve a visualizar en la sesión actual.
					</p><p>Las infocookie_1 , infocookie_2 ... : controlan cada una de las capas de aviso que se puedan mostrar en un portal. Una vez aceptadas no se vuelven a mostrar.
					</p><h5 class="c-2">Cookies de personalización</h5><p>Estas cookies permiten al usuario especificar o personalizar algunas características de las opciones generales de la página web. Por ejemplo, definir el idioma, configuración regional o tipo de navegador
					</p><p><a href="http://www.google.es/intl/es/policies/technologies/types/" target="_blank" title="Politica tecnológica de google">Google: http://www.google.es/intl/es/policies/technologies/types/</a>
					</p><h4 class="c-2">5. Cómo desactivar las cookies </h4><p>Para cumplir con la legislación vigente, tenemos que pedir su permiso para gestionar cookies. Si decide no autorizar el tratamiento indicándonos su no conformidad, solo usaríamos las cookies que son imprescindibles para la navegación por nuestra web. En este caso, no almacenaríamos ninguna cookie. En el caso de seguir navegando por nuestro sitio web sin denegar su autorización implica que acepta su uso.
					</p><p>Tenga en cuenta que si rechaza o borra las cookies de navegación, no podremos mantener sus preferencias, algunas características de las páginas no estarán operativas, no podremos ofrecerle servicios personalizados y cada vez que vaya a navegar por nuestra web tendremos que solicitarle de nuevo su autorización para el uso de cookies.
					</p><p>Puede modificar la configuración de su acceso a la página web. Debe saber que es posible eliminar las cookies o impedir que se registre esta información en su equipo en cualquier momento mediante la modificación de los parámetros de configuración de su navegador:
					</p><p>
					<a href="http://windows.microsoft.com/es-es/windows-vista/block-or-allow-cookies" target="_blank" title="Configuración de cookies en internet explorer">Configuración de <em>cookies 		</em>de Internet Explorer<br></a>
					<a href="http://support.mozilla.org/es/kb/habilitar-y-deshabilitar-cookies-que-los-sitios-we" target="_blank" title="Configuración de cookies en firefox">Configuración de <em>cookies 		</em>de Firefox<br></a>
					<a href="https://support.google.com/chrome/answer/95647?hl=es" target="_blank" title="Configuración de cookies en google chrome">Configuración de <em>cookies 		</em>de Google Chrome<br>
					</a><a href="http://support.apple.com/kb/HT1677?viewlocale=es_ES&amp;locale=es_ES" target="_blank" title="Configuración de cookies en safari">Configuración de <em>cookies 		</em>de Safari 	</a><br>
					</p><p>Infinety S.L. no es titular ni patrocina los enlaces indicados anteriormente, por lo que no asume responsabilidad alguna sobre su contenido ni sobre su actualización
					</p><p>El usuario puede revocar su consentimiento para el uso de cookies en su navegador a través de los siguientes enlaces:
					</p><p>
					Google Analytics: <a href="https://tools.google.com/dlpage/gaoptout?hl=None&#10; " target="_blank" title="Google analytics">https://tools.google.com/dlpage/gaoptout?hl=None</a><br>
					Google: A través de los ajustes de cada navegador más arriba indicados para la desactivación o eliminación de cookies.
					</p>

				</div>


			</div>
		</section>

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ asset(elixir('assets/js/main.js')) }}"></script>
	@endsection 
