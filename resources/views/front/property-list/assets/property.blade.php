<div class="xs-4 m-6 l-4 col  mb-48">
	<a href="{{ $property->url }}" class="property button__hover  |  exclude-svg  d-block pmv-0">
		

		{{-- IMAGEN --}}
		<div class="property__picture overlay overlay--vertical  |  p-relative all-w bg" 
			 style="background-image: url('{{ $property->getFirstMediaUrl($property->mediaCollection, 'thumb') }}');">

			<div class="property__status {{ ($property->state->id == 2) ? 'property__status--comprando' : '' }}  |  pv-8 ph-24">
				<h5 class="mv-0">{{ $property->state->nombre }}</h5>
			</div>

			<div class="property__title  |  xs-4 ph-24 pb-16">
				<h4 class="mv-0">{{ $property->nombre }}</h4>
				<p class="small-text  mv-0">
					@include('front/svg/localizacion', ['class' => 'mr-4']) 
					{{ $property->direccion }}
				</p>
			</div>

		</div>
		{{-- IMAGEN --}}



		{{-- INFO --}}
		<div class="property__info  |  clearfix  pt-24 ph-24 pb-32">
			
			<div class="left">
				<h5 class="c-m  pmv-0">{{ formatThousandsNotDecimals($property->fakeOrRealInvertido) }} € ({{ formatThousandsNotDecimals($property->porcentajeCompletado) }}%)</h5>
			</div>
			<div class="right">
				<h5 class="c-1  pmv-0">{{ formatThousandsNotDecimals($property->objetivo) }} €</h5>
			</div>
			<div class="clearfix  pb-2"></div>

			<div class="bar  |  mv-8">
				<span class="bar__status  |  d-block" style="width: {{ $property->porcentajeCompletado }}%;"></span>
			</div>

			<p class="small-text c-1  left  pmv-0">{{ $property->fakeOrRealInversores }} {{ __('Inversores') }}</p>
			<p class="small-text c-1  right  pmv-0">
				@if($property->state->id == 2)
					@if($property->porcentajeCompletado == 100)
						{{ __('Completado') }}
					@elseif($property->remainingDays < 0)
						{{ __('No conseguido') }}
					@else
						{{ __('Quedan') }} {{ $property->remainingDays }} {{ __('días') }}
					@endif
				@else
					{{ $property->state->nombre }}
				@endif
			</p>

			<div class="clearfix  pb-2 mb-24"></div>

			{{-- bloque de datos --}}
			@php
				$datos = [
					[__('Tasa Interna de Rentabilidad (TIR):'), formatThousands($property->tasa_interna_rentabilidad).' %'],
					[__('Rentabilidad Financiera (ROE):'), formatThousands($property->rentabilidad_objetivo_neta).' %'],
					[__('Rentabilidad Alquiler Neta:'), formatThousands($property->rentabilidad_alquiler_neta).' %']
				]
			@endphp

			@foreach ($datos as $dato)
				<div class="d-table  xs-4 pv-8">
					<div class="d-cell v-middle  pr-8">
						<p class="small-text nowrap  c-1  pmv-0">{{ $dato[0] }}</p>
					</div>
					<div class="d-cell  xs-4  v-middle"><span class="line"></span></div>
					<div class="d-cell v-middle  pl-8">
						<h5 class="c-2 nowrap  pmv-0">{{ $dato[1] }}</h5>
					</div>
				</div>
			@endforeach
			<div class="clearfix mb-24"></div>
			{{-- fin bloque de datos --}}

			<button class="button button--small button--full">{{ __('Ver inmueble') }}</button>
			
		</div>
		{{-- INFO --}}



	</a>
</div>