{{-- 
	==========================================================================
	#PROPERTY SINGLE - HEADER
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#VISIBLE
	========================================================================== --}}

	<div class="wrapper">
		@each('front/property-list/assets/property', $properties, 'property')
		<div class="xs-4 col  text-center  mt-16 mb-56">
		{{ $properties->appends(Request::except('page'))->links('vendor.pagination.default') }}
		</div>
	</div>