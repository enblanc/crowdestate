{{-- 
	==========================================================================
	#PROPERTY SINGLE - HEADER
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#VISIBLE
	========================================================================== --}}

	<div class="wrapper mv-32">
		<div class="xs-4 m-7 l-8 col">
			<h2>{{ __('Inmuebles') }}</h2>
		</div>
		<div class="xs-4 m-5 l-4 col">
			<form action="" method="GET">

				<select class="custom-input  custom-input--select  |  mt-m-32" name="estado" onchange="this.form.submit()">
					<option value="todos">{{ __('Todos') }}</option>
					@foreach($states as $state)
						<option value="{{ $state->id }}" {{ (Request::get('estado') == $state->id) ? 'selected' : '' }}>{{ $state->nombre }}</option>
					@endforeach
				</select>
			</form>
		</div>
	</div>