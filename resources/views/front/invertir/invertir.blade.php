{{-- 
	==========================================================================
	#INVERTIR
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section class="pb-64 mb-80">




			<div class="wrapper">
				<div class="xs-4 col  text-center">
					<h1 class="h3  mt-48 mb-0">{{ __('Invertir en') }} {{ $property->nombre }}</h1>


					@if (count($errors) > 0)
			            @foreach ($errors->all() as $error)
			                <p class="c-error  text-center">{{ $error }}</p>
			            @endforeach
					@endif

					@if (session()->has('error_lemon'))
			           <p class="c-error  text-center">{{ session()->get('error_lemon') }}</p>
					@endif
				</div>
			</div>







			<form class="mt-40 validate   [ js-loading ]" action="{{ route('front.property.invertir.post') }}" method="POST" data-parsley-validate="" data-parsley-trigger="blur">

				{{ csrf_field() }}

				<input type="hidden" name="property_id" value="{{ $property->id }}">

				<div class="wrapper">
					<div class="xs-4 m-10 l-8 col center">
						<div class="box  |  clearfix  ph-8 ph-m-24 ph-l-48 ph-xl-72 pv-16">

							

							

							@if (!auth()->user()->activado && pago_sin_confirmar())
								<div class="xs-4 col  text-center">
									@if(auth()->user()->gifts->count() > 0 && auth()->user()->importeRegalo != 0)
										<p class="small-text mb-0">{{ __('Tu inversión será de') }} <strong class="c-2">{{ auth()->user()->importeRegalo }} €</strong> {{ __('más gracias a las inversiones de tus amigos') }}.</p>

										<p class="small-text mb-0">{{ __('El importe de regalo será añadido automáticamente al realizar la inversión') }}</p>
									@endif
									<p class="small-text mb-32">
										{{ __('Recuerda que al no estar acreditado el máximo que puedes invertir es de 250€.') }}
									</p>
								</div>
								<div class="clearfix"></div>
							@else
								{{-- #REGALO --}}
								@if(auth()->user()->gifts->count() > 0  && auth()->user()->importeRegalo != 0)
									<div class="xs-4 col  text-center">
										<p class="small-text ">{{ __('Tu inversión será de') }} <strong class="c-2">{{ auth()->user()->importeRegalo }} €</strong> {{ __('más gracias a las inversiones de tus amigos') }}.</p>
										<p class="small-text mb-32">{{ __('El importe de regalo será añadido automáticamente al realizar la inversión') }}</p>
									</div>
									<div class="clearfix"></div>
								@endif
								{{-- FIN REGALO --}}
							@endif
							



							{{-- #CANTIDAD A INVERTIR --}}
							<div class="xs-4 m-4 l-5 col  text-right-m">
								<h4 class="mt-40">Cantidad a invertir:</h4>
							</div>

							<div class="xs-4 m-8 l-7 col p-relative">
								<input class="modal__input modal__input--full  |  h3 c-2  mb-8" data-parsley-max-message="{{ __('El máximo permitido son 250€') }}" type="number" step="any" {{-- min="{{ $property->importe_minimo }}" --}} name="inversion"  v-model="inversion" required  data-parsley-min="50"
								{{ (!auth()->user()->activado && pago_sin_confirmar()) ? 'max=250 data-parsley-min=50' : '' }}


									{{-- @change="limitMin()" @keyup.esc="limitMin()" --}}  {{-- min="{{ $importeAPagarMinimo }}"  --}}>
								<span class="h3">€</span>
							</div>
							<div class="clearfix"></div>

							<div class="xs-4 m-4 l-5 col  false"></div>
							<div class="xs-4 m-8 l-7 col p-relative">
								<p class="small-text  mt-0">{{ __('El importe mínimo de inversión en este inmueble es de') }} {{ $property->importe_minimo }} €</p>
							</div>
							<div class="clearfix"></div>
							{{-- FIN CANTIDAD A INVERTIR --}}






							{{-- #GANANCIAS --}}
							<div class="xs-4 m-4 l-5  col  text-right-m">
								<h4 class="">{{ __('Ganarías') }}:</h4>
							</div>

							<div class="xs-4 m-8 l-7 col">
								<h3 class="c-m mv-0 mt-m-8">@{{ beneficioTotal }} €</h3>
								<p class="small-text  mt-0">
									{{ __('En total por el') }} @{{ rentabilidadAcumulada }}% {{ __('de Rentabilidad de Acumulada Neta') }}.
								</p>
							</div>
							<div class="clearfix"></div>
							{{-- FIN GANANCIAS --}}






							{{-- #FORMA DE PAGO --}}
							<div class="xs-4 m-4 l-5  col  text-right-m">
								<h4 class="">{{ __('Forma de pago') }}:</h4>
							</div>

							<div class="xs-4 m-8 l-7 col  mb-16">

								<input type="radio" id="fondos" class="custom-radio__input" value="fondos" name="forma-pago" 
									{{ (!$tjOnly) ? 'checked' : ''}}>
								<label class="d-inblock {{ ($tjOnly) ? 'pe-none' : '' }}" for="fondos" >
									<p class="custom-radio__text  mb-0 pb-0"><span class="custom-radio__new"></span>{{ __('Con mis fondos de Brickstarter') }}</p>
								</label>
								<p class="small-text  c-2  mt-0 mb-0 pt-0 pl-32">{{ __('Tienes') }} {{ auth()->user()->balanceFormatted }} € {{ __('en tu cuenta') }}</p>
								

								@if(count(auth()->user()->cards) > 0)
									@foreach(auth()->user()->cards as $card)
										<input type="radio" id="checkbox-{{ $card->ID }}" class="custom-radio__input" name="forma-pago" value="{{ $card->ID }}" {{ ($loop->first && $tjOnly) ? 'checked' : ''}}>
										<label class="d-block" for="checkbox-{{ $card->ID }}">
											<p class="custom-radio__text  mb-0">
												<span class="custom-radio__new"></span>
												{{ $card->EXTRA->NUM }}     {{ $card->EXTRA->EXP }}
											</p> 
										</label>
									@endforeach
									<input type="radio" id="checkbox-nueva-tarjeta" class="custom-radio__input" value="new" name="forma-pago">
									<label class="d-block" for="checkbox-nueva-tarjeta">
										<p class="custom-radio__text  mb-0">
											<span class="custom-radio__new"></span>
											{{ __('Tarjeta de crédito') }}
										</p> 
									</label>
								@else
									<input type="radio" id="checkbox-nueva-tarjeta" class="custom-radio__input" value="new" name="forma-pago" checked>
									<label class="d-block" for="checkbox-nueva-tarjeta">
										<p class="custom-radio__text  mb-0">
											<span class="custom-radio__new"></span>
											{{ __('Tarjeta de crédito') }}
										</p> 
									</label>
								@endif

								
								
							</div>
							{{-- FIN FORMA DE PAGO --}}



						</div>
					</div>
				</div>


				<div class="wrapper">
					<div class="checkbox__wrapper  |  xs-4 col text-center">
						<input type="checkbox" id="terminos" class="custom-checkbox__input" name="terminos" required
							data-parsley-errors-messages-disabled>
						<label for="terminos">
							<p class="custom-checkbox__text  |  small-text  mv-40"><span class="custom-checkbox__new  |  v-top  mr-16"></span>{{ __('Acepto el') }} <a href="{{ asset('assets/docs/contrato_brickstarter.pdf') }}" download="Contrato de Inversión.pdf">{{ __('Contrato de inversión') }}</a> </p>
					 	</label>

					 	<input class="button button--mvl-full" type="submit" value="{{ __('Invertir en el inmueble') }}">
					</div>
				</div>

			</form>
		</section>


		<input type="hidden" id="rentabilidad_acumulada" value="{{ $property->rentabilidad_anual }}">
		<input type="hidden" id="minimo_invertir" value="{{ $property->importe_minimo }}">
		<input type="hidden" id="defecto_invertir" value="{{ $property->importe_minimo }}">

	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		{{-- <script src="/assets/js/plugins/jquery-3.2.1.min.js"></script> --}}
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}" async></script>

	@endsection 
