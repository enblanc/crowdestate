{{-- 
	==========================================================================
	#HOME _ HEAD
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#CONTENT 
	========================================================================== --}}

	<section class="bg--home  bg bg--contain bg--right  |  clearfix  pv-32 pv-l-0 pv-xl-40 mv-m-40">
		<div class="xs-4 l-7 col  pr-16 pr-m-32 pr-xl-40  pl-16 pl-m-32 pl-xl-72">
			@if($cupon === false)
				<h1>{{ $extras->cabecera_title }}</h1>
				<p class="big-text  pr-l-80">{!! nl2br($extras->cabecera_intro) !!}</p>
			@else
				<h1>{{ __('Regístrate ahora en Brickstarter y gana 15 € en tu primera inversión') }}</h1>
				<p class="big-text  pr-l-80">{!! nl2br($extras->cabecera_intro) !!}</p>
			@endif

			@if(auth()->check())
				<a class="button  mt-16" href="{{ route('front.property.grid') }}">{{ __('Ver inmuebles') }}</a>
			@else
				<a class="button  mt-16" href="{{ route('registro') }}">{{ __('Regístrate') }}</a>
			@endif

			<img class="xs-4  to-m  mt-32" src="{{ $images->cabecera_foto }}" alt="">
		</div>
	</section>