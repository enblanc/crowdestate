{{-- 
	==========================================================================
	#HOME _ HEAD
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#CONTENT 
	========================================================================== --}}

	<section class="clearfix  mb-48 mb-m-80">

		<div class="wrapper  text-center  pt-8  mt-56 mt-m-64  mb-40 mb-m-56">
			<div class="xs-4 col">
				<h2>{{ $extras->title_rentables }}</h2>
			</div>
			<div class="xs-4 m-10 l-8 col center">
				<p class="big-text">{{ $extras->intro_rentables }}</p>
			</div>
		</div>

		<div class="home__rentables  |  wrapper  pb-40">
			@each('front/property-list/assets/property', $properties, 'property')
		</div>

	</section>