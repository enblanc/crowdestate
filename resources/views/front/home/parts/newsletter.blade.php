{{-- 
	==========================================================================
	#HOME _ HEAD
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#CONTENT 
	========================================================================== --}}

	<section class="clearfix  pb-48 mb-48 mb-m-80">

		<div class="wrapper  mt-32 mb-32">
			<div class="xs-4 m-10 col center  text-center">
				<h2>{{ $extras->title_enterate }}</h2>
				<p class="big-text">{{ $extras->intro_enterate }}</p>
			</div>
		</div>

		<div class="wrapper">

			@if (count($errors->newsletter) > 0)
	            @foreach ($errors->newsletter->all() as $error)
	                <p class="c-error  text-center">{{ $error }}</p>
	            @endforeach
			@endif

			<form action="{{ route('newsletter.add') }}" method="post" class="validate" 
			data-parsley-validate="" data-parsley-trigger="blur">

				{{ csrf_field() }}

				<div class="xs-4 m-1 l-2 col false from-m"></div>
				<div class="xs-4 m-6 l-5 col">
					<input type="email" class="custom-input" name="email" placeholder="{{ __('Escribe tu e-mail') }}" required>
				</div>
				<div class="xs-4 m-4 l-3 col">
					<input class="button button--secondary button--full button--form" type="submit" value="{{ __('Suscribirme') }}">
				</div>
			</form>
	</section>