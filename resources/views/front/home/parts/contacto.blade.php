<section class="overlay overlay--black  |  p-relative  bg  pv-8" style="background-image: url({{ asset('assets/img/dudas.jpg') }});">
	<div class="wrapper  pv-80">

		<div class="xs-4 l-6 col  all-w  text-center text-left-l">
			<h2 class="mt-0 mb-8">{{ __('¿Tienes dudas?') }}</h2>
			<p class="big-text  mv-0">{{ __('Estaremos encantados de ayudarte') }}</p>
		</div>

		<div class="xs-4 l-6 col  text-center text-right-l  pt-40">
			<a href="{{ route('contact') }}" class="button button--secondary">{{ __('Contacta con nosotros') }}</a>
		</div>

	</div>
</section>