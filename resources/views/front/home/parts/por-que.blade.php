{{-- 
	==========================================================================
	#HOME _ HEAD
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#CONTENT 
	========================================================================== --}}

	<section class="bg-4  clearfix  mb-24 mb-m-64">

		<div class="wrapper  pt-8 mt-40 mt-m-16 mt-l-80 mb-64">
			<div class="xs-4 m-10 l-8 col center  text-center pb-16">
				<h2>{{ $extras->porque_title }}</h2>
				<p class="big-text">{{ $extras->porque_text }}</p>
			</div>

			<div class="video-wrapper xs-4 l-12 pt-0">
				@if(current_locale() === 'en')
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hvAPhure0RQ?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				@elseif(current_locale() === 'de')
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/7yZci-VwF30?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				@else 
					<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/82WZS1XB0wA?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
				@endif
			</div>
		</div>

		{{-- @include('front/home/parts/video') --}}

		<div class="wrapper">
			<div class="xs-4 m-4 col  text-center  mb-72 mb-l-24">
				<img class="d-block  center  mb-40" src="{{ asset('assets/img/turismo.svg') }}" alt="" height="54px">
				<h4 class="ph-l-24">{{ $extras->porque_1 }}</h4>
			</div>
			<div class="xs-4 m-4 col  text-center  mb-72 mb-l-24">
				<img class="d-block  center  mb-40" src="{{ asset('assets/img/grafica.svg') }}" alt="" height="54px">
				<h4 class="ph-l-24">{{ $extras->porque_2 }}</h4>
			</div>
			<div class="xs-4 m-4 col  text-center  mb-72 mb-24">
				<img class="d-block  center  mb-40" src="{{ asset('assets/img/candado.svg') }}" alt="" height="54px">
				<h4 class="ph-l-24">{{ $extras->porque_3 }}</h4>
			</div>
			<div class="xs-4 m-4 col  text-center  mb-64 mb-l-24">
				<img class="d-block  center  mb-40" src="{{ asset('assets/img/big-data.svg') }}" alt="" height="54px">
				<h4 class="ph-l-24">{{ $extras->porque_4 }}</h4>
			</div>
			<div class="xs-4 m-4 col  text-center  mb-64 mb-l-24">
				<img class="d-block  center  mb-40" src="{{ asset('assets/img/rentabilidad.svg') }}" alt="" height="54px">
				<h4 class="ph-l-24">{{ $extras->porque_5 }}</h4>
			</div>
			<div class="xs-4 m-4 col  text-center  mb-24">
				<img class="d-block  center  mb-40" src="{{ asset('assets/img/propietario.svg') }}" alt="" height="54px">
				<h4 class="ph-l-24">{{ $extras->porque_6 }}</h4>
			</div>
		</div>

		<div class="wrapper  text-center  mt-48 mb-80">
			<a class="button button--secondary" href="{{ $extras->porque_boton_link }}">{{ $extras->porque_boton }}</a>
		</div>

	</section>