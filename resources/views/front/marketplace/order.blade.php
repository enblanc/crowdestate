@extends('front/layouts/general')

@section('content')
	
	<section class="pb-64 mb-80">

		<div class="wrapper">
			<div class="xs-4 col  text-center">
				<h1 class="h3  mt-48 mb-0">
					@if($order->type == 1)
						{{ __('Orden de compra') }}
					@else
						{{ __('Orden de venta') }}
					@endif
				</h1>


				@if (count($errors) > 0)
					@foreach ($errors->all() as $error)
						<p class="c-error  text-center">{{ $error }}</p>
					@endforeach
				@endif

				@if (session()->has('error_lemon'))
					<p class="c-error  text-center">{{ session()->get('error_lemon') }}</p>
				@endif
			</div>
		</div>

		<form class="mt-24 validate   [ js-loading ]" action="{{ route('front.market.order') }}" method="POST" data-parsley-validate="" data-parsley-trigger="blur">

			{{ csrf_field() }}

			<input type="hidden" name="order" value="{{ $order->id }}">

			<div class="wrapper">
				<div class="xs-4 m-10 l-8 col center">
					<div class="box  |  clearfix ">

						<div class="property__picture overlay overlay--vertical  |  p-relative all-w bg" style="background-image: url({{ asset($order->getProperty()->getFirstMediaUrl($order->getProperty()->mediaCollection, 'thumb')) }});height:170px">
							<div class="property__title  |  xs-4 ph-24 pb-16">
								<h4 class="mv-0">{{ $order->getPropertyName() }}</h4>
								<p class="small-text  mv-0"><svg xmlns="http://www.w3.org/2000/svg" width="12" height="16" viewBox="0 0 12 16" class="mr-4"><path fill="#36D9B6" fill-rule="nonzero" d="M6 0C2.686 0 0 2.505 0 5.594 0 8.32 1.656 11.952 6 16c4.344-4.048 6-7.68 6-10.406C12 2.504 9.314 0 6 0zm0 7.03c-1.238 0-2.242-.935-2.242-2.089 0-1.152 1.004-2.088 2.242-2.088s2.24.936 2.24 2.088c0 1.154-1.002 2.088-2.24 2.088z"></path></svg>
				
								{{ $order->getProperty()->direccion }}
							</p></div>
						</div>

						<div class=" ph-8 ph-m-24 ph-l-48 ph-xl-72 pv-16">
				

							{{-- #CANTIDAD A INVERTIR --}}
							<div class="xs-4 col  text-center">

								{{-- <img src="{{  }}"> --}}

								<ul class="no-list text-left">
									<li >
										<strong>{{ __('Inversión') }}:</strong> {{ formatThousandsNotZero($order->quantity) }}€
									</li>
									<li >
										<strong>{{ __('Precio') }}:</strong> {{ formatThousandsNotZero($order->priceMoney) }}€
									</li>
									<li >
										<strong>{{ __('Oportunidad') }}:</strong> {{ $order->getPropertyName() }}
									</li>
									<li >
										<strong>{{ __('TIR') }}:</strong> {{ formatThousands($order->getProperty()->tasa_interna_rentabilidad).' %' }}
									</li>
									<li >
										<strong>{{ __('Descuento/Prima') }}:</strong> {{ $order->price }}%
									</li>
								</ul>

								
							</div>
							<div class="clearfix"></div>

							{{-- <div class="xs-4 col text-center">
								<h3 class="c-m mv-0 mt-m-8">
									{{ formatThousandsNotZero($order->priceMoney) }}€
								</h3>
							</div> --}}
							<div class="clearfix"></div>
							{{-- FIN CANTIDAD A INVERTIR --}}

							<div class="clearfix"></div>
							{{-- FIN GANANCIAS --}}

							{{-- #FORMA DE PAGO --}}
							<div class="xs-4  col  text-center">
								<h4 class="">{{ __('¿Deseas realizar la operación?') }}</h4>
								<p class="small-text  c-2  mt-0 mb-0 pt-0 pl-32">{{ __('Tienes') }} {{ auth()->user()->balanceFormatted }} € {{ __('en tu cuenta') }}</p>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div class="wrapper">
				<div class="checkbox__wrapper  |  xs-4 col text-center">
					<input type="checkbox" id="terminos" class="custom-checkbox__input" name="terminos" required
						data-parsley-errors-messages-disabled>
					<label for="terminos">
						<p class="custom-checkbox__text  |  small-text  mt-32"><span class="custom-checkbox__new  |  v-top  mr-16"></span>{{ __('Acepto los') }} <a href="{{ asset('assets/docs/contrato_brickstarter.pdf') }}" download="Contrato de Inversión.pdf">{{ __('Términos y Condiciones de Compraventa') }}</a> </p>
					</label>

					@if ($order->type == 1)
						@if(auth()->user()->balance >= $order->priceMoney)
							<input class="button button--mvl-full mb-24 mt-16"
								type="submit"
								value="{{ __('Confirmo la Inversión') }}">
						@else
							<p class="mt-24">
								No tienes fondos suficientes en lemonway. Por favor, <a href="{{ route('panel.user.money.show') }}">introduce fondos</a>.
							</p>
						@endif
					@else
						<input class="button button--mvl-full mb-24 mt-16"
								type="submit"
								value="{{ __('Confirmo la Desinversión') }}">
					@endif

					{{--
					@if ($order->type == 1 && (auth()->user()->balance > $order->priceMoney))

						@if($order->type == 1)
							<input class="button button--mvl-full mb-24 mt-16"
								type="submit"
								value="{{ __('Confirmo la Inversión') }}">


						@else
							<input class="button button--mvl-full mb-24 mt-16"
								type="submit"
								value="{{ __('Confirmo la Desinversión') }}">
						@endif

					@else
						<p class="mt-24">
							No tienes fondos suficientes en lemonway. Por favor, <a href="{{ route('panel.user.money.show') }}">introduce fondos</a>.
						</p>
					@endif
					--}}
				</div>
			</div>
		</form>
	</section>
@endsection

{{-- JS --}}

@section('custom_plugin_js')
	<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
	<script src="/assets/js/plugins/parsleyjs.min.js"></script>

@endsection

@section('custom_section_js')
	<script src="{{ mix('assets/js/main.js') }}" async></script>
@endsection 
