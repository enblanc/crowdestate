{{-- 
	==========================================================================
	#MARKETPLACE LIST
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#VISIBLE
	========================================================================== --}}

	<div class="wrapper pb-40">
		{{-- <template v-for="(order, index) in orders">
			<property
				:order="order"
				:user-string="'{{ __('Usuario') }}'"
				:by-string="'{{ __('Por') }}'"
				:sell-string="'{{ __('Vende') }}'"
				:buy-string="'{{ __('Compra') }}'"
				:sell-now-string="'{{ __('Vende ahora') }}'"
				:buyl-now-string="'{{ __('Compra ahora') }}'"
			>
			</property>
		</template> --}}

		<vue-good-table v-if="orders && orders.length"
			:columns="columns"
			:rows="orders"
			:sort-options="{
				enabled: true,
			}">
			<template slot="table-row" slot-scope="props">
				<span v-if="props.column.field == 'quantity'">
					@{{ formatCurrency(props.row.quantity) }}
				</span>
				<span v-else-if="props.column.field == 'priceMoney'">
					@{{ formatCurrency(props.row.priceMoney) }}
				</span>
				<span v-else-if="props.column.field == 'linkButton'" v-html="props.row.linkButton">
					
				</span>
				<span v-else>
					@{{props.formattedRow[props.column.field]}}
				</span>
			</template>
	    </vue-good-table>
	</div>