{{-- 
	==========================================================================
	#PROPERTY SINGLE - HEADER
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Null
	*
	*
	* AVISOS
	*
	* Ninguno
	*
	*
	--}}








{{--==========================================================================
	#VISIBLE
	========================================================================== --}}

	<div class="wrapper mv-32">
		<div class="xs-4 col text-center">
			<h2>{{ __('Marketplace') }}</h2>
		</div>
		<div class="xs-4 bg-4 col pa-24"  style="position: relative">
			<div class="row">
				<div v-show="loading" class="loading-absolute">
					<div class="d-cell v-middle text-center">
						<div class="sk-folding-cube">
							<div class="sk-cube1 sk-cube"></div>
							<div class="sk-cube2 sk-cube"></div>
							<div class="sk-cube4 sk-cube"></div>
							<div class="sk-cube3 sk-cube"></div>
						</div>
					</div>
				</div>
				<div class="xs-4 m-2 l-2 col">
					<select class="custom-input  custom-input--select  |" name="type" v-model="type" v-on:change="filterData">
						<option value="">{{ __('Tipo') }}</option>
						<option value="1">{{ __('Vender') }}</option>
						<option value="2">{{ __('Comprar') }}</option>
					</select>
				</div>
				<div class="xs-4 m-5 l-4 col">
					<select class="custom-input  custom-input--select  |" name="property" v-model="propertyId" v-on:change="filterData">
						<option value="">{{ __('Inmuebles') }}</option>
						@foreach($properties as $property)
							<option value="{{ $property->id }}">{{ $property->nombre }}</option>
						@endforeach
					</select>
				</div>
				<div class="xs-4 m-4 l-3 col">
					<select class="custom-input  custom-input--select  |" name="city" v-model="city" v-on:change="filterData">
						<option value="">{{ __('Ciudades') }}</option>
						@foreach($cities as $city)
							<option value="{{ $city }}">{{ $city }}</option>
						@endforeach
					</select>
				</div>
				<div class="xs-4 m-3 l-3 col">
					<input type="number" name="" min="0" step="any" class="custom-input custom-input-text" placeholder="{{__('Precio Máx.') }}" v-model="maxPrice" v-on:input="filterDataPrice">
				</div>
				<div class="xs-4 l-4 left col">
					<div class="clearfix">
						<div class="xs-3 m-11 col">
							<h5 class="pt-m-8 mr-16 left c-pointer" v-on:click="toogleInvested">{{ __('Mis proyectos de inversión') }}</h5>
							<div class="pt-16">
								<input type="checkbox" id="invested" class="custom-checkbox__input" name="invested" v-model="myInvested" v-on:change="filterData">
		                        <label for="invested" class="pt-4">
		                            <span class="custom-checkbox__new  |  v-middle"></span> 
		                        </label>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
	</div>