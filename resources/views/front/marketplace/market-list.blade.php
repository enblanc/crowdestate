{{-- 
	==========================================================================
	# MARKETPLACE LISTING
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')


{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')

		<div id="market">
			@include('front/marketplace/parts/filters')

			@if (auth()->user()->activado)
				@include('front/marketplace/parts/grid')

				<div class="wrapper mv-40">
					<div class="xs-4 col center  text-center ">
						<a href="{{ route('panel.user.marketplace.orders') }}" class="text-center button button--small nowrap">
							{{ __('Mis órdenes') }}
						</a>
					</div>
				</div>
			@else
				<div class="wrapper mb-40">
					<div class="bg-info bg-s">
						<svg class="block v-middle" xmlns="http://www.w3.org/2000/svg" fill="#fff" width="24" height="24" viewBox="0 0 24 24"><path d="M12 2c5.514 0 10 4.486 10 10s-4.486 10-10 10-10-4.486-10-10 4.486-10 10-10zm0-2c-6.627 0-12 5.373-12 12s5.373 12 12 12 12-5.373 12-12-5.373-12-12-12zm-.001 5.75c.69 0 1.251.56 1.251 1.25s-.561 1.25-1.251 1.25-1.249-.56-1.249-1.25.559-1.25 1.249-1.25zm2.001 12.25h-4v-1c.484-.179 1-.201 1-.735v-4.467c0-.534-.516-.618-1-.797v-1h3v6.265c0 .535.517.558 1 .735v.999z"/></svg>
						{{ __('Acredita tu cuenta para poder invertir rellenando los siguientes datos. Tardaremos aproximadamente 24 horas en validar la documentación aportada.') }} 
					</div>

					<h4>
						<a href="{{ route('panel.user.accredit.show') }}">{{ __('Acredita tu cuenta') }}</a>
					</h4>
				</div>
			@endif
			
		</div>


	@endsection






{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	
	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
		<style>
			.vgt-table td {
				vertical-align: middle !important;
			}
		</style>
    @endsection
	{{-- @section('header_assets') @endsection 
	--}}



	{{-- JS --}}

	@section('custom_plugin_js')
		{{-- <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script> --}}
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>

	@endsection

	@section('custom_section_js')
		<script type="text/javascript">
			var api = "{{ route('marketplace.all') }}";
		</script>
		<script src="{{ mix('assets/js/marketplace.js') }}" async></script>

	@endsection 
