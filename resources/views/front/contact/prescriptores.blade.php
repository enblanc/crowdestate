{{-- 
	==========================================================================
	#BIENVENIDO A CROWDESTATE
	========================================================================== 
	*
	* DESCRIPCIÓN
	* 
	* Página del inmueble. Consta de X bloques:
	*	- XX (carpeta parts)
	*	- XX (cp)
	*
	*
	* AVISOS
	*
	* Ninguno.
	*
	*
	--}}








{{--==========================================================================
	#LAYOUT 
	========================================================================== --}}

	@extends('front/layouts/general')






{{--==========================================================================
	#CONTENT 
	========================================================================== --}}
	
	@section('content')
		
		<section>
			<div class="wrapper  mb-80">

				@if(!isset($ok))

				<div class="xs-4 col center  text-center">

					<h2 class="mt-48">{{ __('¿Quiéres ser prescriptor de Brickstarter?') }}</h2>
				</div>
				<div class="xs-4 m-10 l-8 col center  text-center">
					<p class="mt-32">{{ __('¿Quieres ganar dinero con las inversiones de otros usuarios? Contacta con nosotros para descubrir cómo hacerlo convirtiéndote en prescriptor de Brickstarter.') }}</p>
					<p class="mb-0">{{ __('Puedes hacerlo a través del e-mail') }} <a href="mailto:contacto@brickstarter.com">contacto@brickstarter.com</a> {{ __('o del siguiente formulario') }}</p>

				</div>

				<form method="POST" class="xs-4 m-8 l-6 col center  ph-m-32 mv-40" action="{{ route('prescriptores_post') }}">
					{{ csrf_field() }}

					<input class="custom-input" type="text"  placeholder="{{ __('Nombre') }} *" name="nombre" required>
					<input class="custom-input" type="email" placeholder="{{ __('E-mail') }} *" name="email" required>
					<input class="custom-input" type="text"  placeholder="{{ __('Teléfono') }} *" name="telefono" required>
					<textarea class="custom-input" name="comentarios" id="" cols="30" rows="6" placeholder="{{ __('Comentarios') }} *"></textarea>
					

					
				 		<input class="button button--full  |  mt-56" type="submit" value="{{ __('Enviar') }}">


					@include ('front.contact.errors')
				</form>

				@else

					<div class="xs-4 col center  text-center">
						@include('front/svg/crowdestate', ['class' => 'mt-48 mb-16'])

						@if(isset($nombre)) 
							<h3>{{ __('Gracias por contactar') }} {{ $nombre }}</h3>
						@endif

						<p>{{ __('Hemos recibido tu consulta correctamente. Nos pondremos en contacto contigo lo antes posible') }}.</p>

						<p class="pmv-0">{{ __('Puedes contactar también con nosotros a través del e-mail') }} <a href="mailto:contacto@brickstarter.com">contacto@brickstarter.com</a>.</p>

						<a href="{{ route('front.property.grid') }}" class="button  mt-40">
							{{ __('Ver inmuebles') }}
						</a>

					</div>

				@endif
			</div>
		</section>

	@endsection

	




{{--==========================================================================
	#ASSETS 
	========================================================================== --}}
	
	{{-- CSS --}}

	{{-- 
	@section('header_css')    @endsection
	@section('header_assets') @endsection 
	--}}

	@section('header_css')
		<script src="/assets/js/plugins/jquery-3.2.1.min.js"></script>
    @endsection

	{{-- JS --}}

	@section('custom_plugin_js')
		<script src="/assets/js/plugins/parsleyjs.min.js"></script>
	@endsection

	@section('custom_section_js')
		<script src="{{ mix('assets/js/main.js') }}"></script>
	@endsection 
