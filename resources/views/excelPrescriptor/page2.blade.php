<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
{{-- <link href="{{ asset(mix('assets/css/excel.css')) }}" rel="stylesheet"> --}}
<style>
	.cabecera-1{background-color:red}
</style>
</head>

<body>
    <div id="app">
        <div class="container">
		    <div class="row">
		        <div class="xs-4 col">
		        
					<table border="0" cellpadding="0" cellspacing="0">
					
						 <tr class="style-11">
						  <td colspan="2" class="style-15">Resumen
						  Global de las Inversiones por Cliente</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="2" class="style-15">Resumen
						  Global por Cliente (Evolución)</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="9" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Etiquetas de columna</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">&nbsp;</td>
						  <td class="style-27 right">{{ $year }}</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-28 right">Total {{ $year }}</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">1</td>
						  <td class="style-30 right">2</td>
						  <td class="style-30 right">3</td>
						  <td class="style-30 right">4</td>
						  <td class="style-30 right">5</td>
						  <td class="style-30 right">6</td>
						  <td class="style-30 right">7</td>
						  <td class="style-30 right">8</td>
						  <td class="style-30 right">9</td>
						  <td class="style-30 right">10</td>
						  <td class="style-30 right">11</td>
						  <td class="style-30 right">12</td>
						  <td class="style-31 right">Total general</td>
						 </tr>
						 @php 
						 $totalMonth = [];
						 $totalMonth['01'] = 0;
						 $totalMonth['02'] = 0;
						 $totalMonth['03'] = 0;
						 $totalMonth['04'] = 0;
						 $totalMonth['05'] = 0;
						 $totalMonth['06'] = 0;
						 $totalMonth['07'] = 0;
						 $totalMonth['08'] = 0;
						 $totalMonth['09'] = 0;
						 $totalMonth['10'] = 0;
						 $totalMonth['11'] = 0;
						 $totalMonth['12'] = 0;

						 $totalResumen = 0;
						 @endphp
						 @foreach ($arrayResume as $key => $resumeItem)

						 	<tr class="style-11">
						 		<td class="style-32">Usuario: {{ $key }}</td>
						 		
						 		@foreach ($months as $month)
								    
								    <td class="style-33 right">
								   	
									   	@if(isset($resumeItem['totales'][$month]))

									   		@php $totalMonth[$month] = $totalMonth[$month] + $resumeItem['totales'][$month]; @endphp
									   	
									   		{{ $resumeItem['totales'][$month] }}
									   	
									   	@endif
								    
								    </td>

								@endforeach

								@php $totalResumen = $totalResumen + $resumeItem['totales'][$year]; @endphp

								<td class="style-34 right">{{ $resumeItem['totales'][$year] }}</td>

						 	</tr>
						 	
						 	@foreach ($tiposActivos as $keyTipoActivo => $tipoActivo)
							 	@if(isset($resumeItem['totales_movimientos'][$keyTipoActivo][$year]))
								 	<tr class="style-11">
									  <td class="style-36">{{ $tipoActivo }}</td>
									  @foreach ($months as $month)
										    
										    <td class="style-37 right">
											   	
											   	@if(isset($resumeItem['totales_movimientos'][$keyTipoActivo][$month]))
											   	
											   		{{ $resumeItem['totales_movimientos'][$keyTipoActivo][$month] }}
											   	
											   	@endif
										    
										    </td>

										@endforeach

										<td class="style-39 right">{{ $resumeItem['totales_movimientos'][$keyTipoActivo][$year] }}</td>

									 </tr>

									 @foreach($resumeItem['totales_movimientos_inmuebles'][$keyTipoActivo] as $keyInmueble => $inmuebleActivo)
										 <tr class="style-11">
											  <td class="style-11">{{ $keyInmueble }}</td>
											  @foreach ($months as $month)
										    
											    <td class="style-10 right">
												   	
												   	@if(isset($resumeItem['totales_movimientos_inmuebles'][$keyTipoActivo][$keyInmueble][$month]))
												   	
												   		{{ $resumeItem['totales_movimientos_inmuebles'][$keyTipoActivo][$keyInmueble][$month] }}
												   	
												   	@endif
											    
											    </td>
											    


											@endforeach

										    @if(isset($resumeItem['totales_movimientos_inmuebles'][$keyTipoActivo][$keyInmueble][$year]))
										    	<td class="style-39 right">{{ $resumeItem['totales_movimientos_inmuebles'][$keyTipoActivo][$keyInmueble][$year] }}</td>
										    @endif
										</tr>
									@endforeach

								 @endif
							 @endforeach

						 @endforeach

						 <tr class="style-11">
						 	<td class="style-41 right">Total general</td>
						 @foreach($months as $month)

						 	<td class="style-42 right">{{ $totalMonth[$month] }}</td>

						 @endforeach

						 	 <td class="style-42 right">{{ $totalResumen }}</td>
						 
						 </tr>

						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="7" class="style-15">Resumen
						  Global por Cliente (Total)</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Etiquetas de columna</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">{{ $year }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 
						 @foreach ($arrayResume as $key => $resumeItem)
						 	
						 	<tr class="style-11">
						 		<td class="style-32">Usuario: {{ $key }}</td>
								<td class="style-33 right">{{ $resumeItem['totales'][$year] }}</td>
								<td colspan="12" class="style-17">&nbsp;</td>
						 	</tr>
						 	
						 	@foreach ($tiposActivos as $keyTipoActivo => $tipoActivo)
							 	@if(isset($resumeItem['totales_movimientos'][$keyTipoActivo][$year]))

								 	<tr class="style-11">
									  	<td class="style-11">{{ $tipoActivo }}</td>
										<td class="style-10 right">{{ $resumeItem['totales_movimientos'][$keyTipoActivo][$year] }}</td>
										<td colspan="12" class="style-17">&nbsp;</td>

									 </tr>

								 @endif
							 @endforeach

						 @endforeach

						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>
						  <td class="style-42 right">{{ $totalResumen }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-23">
						  <td colspan="14" class="style-24">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">Saldo del cliente</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-43">Suma de importe</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>

						@foreach ($arrayResume as $key => $resumeItem)
						 	
						 	<tr class="style-11">
						 		<td class="style-32">Usuario: {{ $key }}</td>
								<td class="style-33 right">{{ $resumeItem['totales'][$year] }}</td>
								<td colspan="12" class="style-17">&nbsp;</td>
						 	</tr>

						 @endforeach

						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>
						  <td class="style-42 right">{{ $totalResumen }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-23">
						  <td colspan="14" class="style-24">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-44">
						  <td colspan="7" class="style-45">B)
						  Resumen de Inversiones</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="7" class="style-15">Inversiones
						  por cliente y mes</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-46">
						  <td colspan="14" class="style-47">&nbsp;</td>
						 </tr>

						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Etiquetas de columna</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">&nbsp;</td>
						  <td class="style-27 right">{{ $year }}</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-26">&nbsp;</td>
						  <td class="style-28 right">Total {{ $year }}</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">1</td>
						  <td class="style-30 right">2</td>
						  <td class="style-30 right">3</td>
						  <td class="style-30 right">4</td>
						  <td class="style-30 right">5</td>
						  <td class="style-30 right">6</td>
						  <td class="style-30 right">7</td>
						  <td class="style-30 right">8</td>
						  <td class="style-30 right">9</td>
						  <td class="style-30 right">10</td>
						  <td class="style-30 right">11</td>
						  <td class="style-30 right">12</td>
						  <td class="style-31">&nbsp;</td>
						 </tr>

						 @foreach ($arrayResume as $key => $resumeItem)

						 	<tr class="style-11">
						 		<td class="style-32">Usuario: {{ $key }}</td>
						 		
						 		@foreach ($months as $month)
								    
								    <td class="style-33 right">
								   	
									   	@if(isset($resumeItem['totales'][$month]))
									   	
									   		{{ $resumeItem['totales'][$month] }}
									   	
									   	@endif
								    
								    </td>

								@endforeach

								<td class="style-34 right">{{ $resumeItem['totales'][$year] }}</td>

						 	</tr>
						 	
						 	@foreach ($tiposActivos as $keyTipoActivo => $tipoActivo)
							 	@if(isset($resumeItem['totales_movimientos'][$keyTipoActivo][$year]))
								 	<tr class="style-11">
									  <td class="style-11">{{ $tipoActivo }}</td>
									  @foreach ($months as $month)
										    
										    <td class="style-11 right">
											   	
											   	@if(isset($resumeItem['totales_movimientos'][$keyTipoActivo][$month]))
											   	
											   		{{ $resumeItem['totales_movimientos'][$keyTipoActivo][$month] }}
											   	
											   	@endif
										    
										    </td>

										@endforeach

										<td class="style-39 right">{{ $resumeItem['totales_movimientos'][$keyTipoActivo][$year] }}</td>

									 </tr>

								 @endif
							 @endforeach

						 @endforeach

						  <tr class="style-11">
						 	<td class="style-41 right">Total general</td>
						 @foreach($months as $month)

						 	<td class="style-42 right">{{ $totalMonth[$month] }}</td>

						 @endforeach

						 	 <td class="style-42 right">{{ $totalResumen }}</td>
						 
						 </tr>
						 
						 <tr class="style-11">
						  <td class="style-11">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td colspan="7" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="7" class="style-15">Resumen
						  de las Inversiones por cliente</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-23">
						  <td colspan="9" class="style-24">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">Inversiones</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						 </tr>
						 <tr class="style-23">
						  <td colspan="14" class="style-24">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-52">Tipo de Movimiento</td>
						  <td class="style-53">Inversión</td>
						  <td colspan="12" class="style-52">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Importe</td>
						  <td class="style-26">% Participación</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>

						@foreach ($arrayResume as $key => $resumeItem)
							
							@if(isset($resumeItem['totales_movimientos'][2][$year]))
							<tr class="style-11">
							 		<td class="style-32">Usuario: {{ $key }}</td>
							 		<td class="style-32 right">{{ $resumeItem['totales_movimientos'][2][$year] }}</td>
							 		<td class="style-32 right">&nbsp</td>
							 		<td colspan="12" class="style-17">&nbsp;</td>
							 </tr>	

								 @foreach($resumeItem['totales_movimientos_inmuebles'][2] as $keyInmueble => $inmuebleActivo)

								<tr class="style-11">
									  <td class="style-11">{{ $keyInmueble }}</td>
									  <td class="style-10 right">{{ $inmuebleActivo[$year] }}</td>
									  @if (isset($inmuebleActivo['objetivo']) && $inmuebleActivo['objetivo'] !== 0 && $inmuebleActivo['objetivo'] != null)
									  <td class="style-10 right">{{ number_format(abs($inmuebleActivo[$year])*100/$inmuebleActivo['objetivo'], 2, '.', '') }}</td>
									  @endif
									  <td colspan="12" class="style-17">&nbsp;</td>
								</tr>

								 @endforeach

							 @endif

						@endforeach

						<tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>

						 <tr class="style-11">
						  <td class="style-52">Tipo de Movimiento</td>
						  <td class="style-53">Venta de inmueble</td>
						  <td colspan="12" class="style-52">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Importe</td>
						  <td class="style-26">% Participación</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>

						@foreach ($arrayResume as $key => $resumeItem)
							
							@if(isset($resumeItem['totales_movimientos'][14][$year]))
							<tr class="style-11">
							 		<td class="style-32">Usuario: {{ $key }}</td>
							 		<td class="style-32 right">{{ $resumeItem['totales_movimientos'][14][$year] }}</td>
							 		<td class="style-32 right">&nbsp</td>
							 		<td colspan="12" class="style-17">&nbsp;</td>
							 </tr>	

								 @foreach($resumeItem['totales_movimientos_inmuebles'][14] as $keyInmueble => $inmuebleActivo)

								<tr class="style-11">
									  <td class="style-11">{{ $keyInmueble }}</td>
									  <td class="style-10 right">{{ $inmuebleActivo[$year] }}</td>
									  <td class="style-10 right">{{ number_format(abs($resumeItem['totales_movimientos_inmuebles'][2][$keyInmueble][$year])*100/$resumeItem['totales_movimientos_inmuebles'][2][$keyInmueble]['objetivo'], 2, '.', '') }}</td>
									  <td colspan="12" class="style-17">&nbsp;</td>
								</tr>

								 @endforeach

							 @endif

						@endforeach

						<tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>

						 <tr class="style-11">
						  <td class="style-52">Tipo de Movimiento</td>
						  <td class="style-53">Inversión Neta</td>
						  <td colspan="12" class="style-52">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Importe</td>
						  <td class="style-26">% Participación</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>

						@foreach ($arrayResume as $key => $resumeItem)
							
							@if(isset($resumeItem['totales_movimientos'][2][$year]))
								@php $hay = false; @endphp
								@foreach($resumeItem['totales_movimientos_inmuebles'][2] as $keyInmueble => $inmuebleActivo)
									
									 @php 
									 if(!isset($resumeItem['totales_movimientos_inmuebles'][14][$keyInmueble][$year])){
									 	$hay = true;
									 }
									@endphp

								@endforeach

								@if($hay)
								<tr class="style-11">
								 		<td class="style-32">Usuario: {{ $key }}</td>
								 		<td class="style-32 right">{{ $resumeItem['totales_movimientos'][2][$year] }}</td>
								 		<td class="style-32 right">&nbsp</td>
								 		<td colspan="12" class="style-17">&nbsp;</td>
								 </tr>	
								 @endif

								 @foreach($resumeItem['totales_movimientos_inmuebles'][2] as $keyInmueble => $inmuebleActivo)
									
									 @if(!isset($resumeItem['totales_movimientos_inmuebles'][14][$keyInmueble][$year]))
										<tr class="style-11">
											  <td class="style-11">{{ $keyInmueble }}</td>
											  <td class="style-10 right">{{ $inmuebleActivo[$year] }}</td>
											  @if (isset($inmuebleActivo['objetivo']) && $inmuebleActivo['objetivo'] !== 0 && $inmuebleActivo['objetivo'] != null)
											  <td class="style-10 right">{{ number_format(abs($inmuebleActivo[$year])*100/$inmuebleActivo['objetivo'], 2, '.', '') }}</td>
											  @endif
											  <td colspan="12" class="style-17">&nbsp;</td>
										</tr>
									@endif

								 @endforeach

							 @endif

						@endforeach

						<tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						
						 <tr class="style-44">
						  <td colspan="2" class="style-45">C)
						  Resumen de Dividendos</td>
						 <td colspan="10" class="style-11">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">Cobro dividendos</td>
						  <td colspan="13" class="style-11">&nbsp;</td>
						 </tr>
						 <tr class="style-23">
						  <td colspan="14" class="style-24">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-52">Tipo de Movimiento</td>
						  <td class="style-53">cobro
						  dividendos</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  importe</td>
						  <td class="style-26">Etiquetas de columna</td>
						  <td colspan="12" class="style-26">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">&nbsp;</td>
						  <td class="style-27 right">{{ $year }}</td>
						  <td colspan="11" class="style-26">&nbsp;</td>
						  <td class="style-28 right">Total {{ $year }}</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">1</td>
						  <td class="style-30 right">2</td>
						  <td class="style-30 right">3</td>
						  <td class="style-30 right">4</td>
						  <td class="style-30 right">5</td>
						  <td class="style-30 right">6</td>
						  <td class="style-30 right">7</td>
						  <td class="style-30 right">8</td>
						  <td class="style-30 right">9</td>
						  <td class="style-30 right">10</td>
						  <td class="style-30 right">11</td>
						  <td class="style-30 right">12</td>
						  <td class="style-31">&nbsp;</td>
						 </tr>

						 @php 

						 $totalMonth = [];
						 $totalMonth['01'] = 0;
						 $totalMonth['02'] = 0;
						 $totalMonth['03'] = 0;
						 $totalMonth['04'] = 0;
						 $totalMonth['05'] = 0;
						 $totalMonth['06'] = 0;
						 $totalMonth['07'] = 0;
						 $totalMonth['08'] = 0;
						 $totalMonth['09'] = 0;
						 $totalMonth['10'] = 0;
						 $totalMonth['11'] = 0;
						 $totalMonth['12'] = 0;

						 $totalResumen = 0;
						 @endphp

						 @foreach ($arrayResume as $key => $resumeItem)

						 	<tr class="style-11">
						 		
						 		<td class="style-11">Usuario: {{ $key }}</td>
							 	
							 	@foreach ($months as $month)
									    
								    <td class="style-10 right">
								   	
									   	@if(isset($resumeItem['totales_movimientos'][13][$month]))
									   	
									   		{{ $resumeItem['totales_movimientos'][13][$month] }}

									   		@php $totalMonth[$month] = $totalMonth[$month] + $resumeItem['totales_movimientos'][13][$month]; @endphp
									   	
									   	@endif
								    
								    </td>

								@endforeach

									<td class="style-39">
										@if(isset($resumeItem['totales_movimientos'][13][$year]))
									   	
									   		{{ $resumeItem['totales_movimientos'][13][$year] }}

									   		@php $totalResumen = $totalResumen + $resumeItem['totales_movimientos'][13][$year]; @endphp
									   	
									   	@else
									   	
									   		&nbsp;

									   	@endif
									</td>

							</tr>

						 @endforeach

						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>

						  @foreach($months as $month)

						 	<td class="style-42 right">{{ $totalMonth[$month] }}</td>

						  @endforeach

						  <td class="style-39 right">{{ $totalResumen }}</td>
						 </tr>

						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 
						 <tr class="style-44">
						  <td colspan="2" class="style-45">D)
						  Resumen de Comisiones</td>
						  <td class="style-10" colspan="12" >&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="2" class="style-15">Comisiones
						  Generadas por cliente y mes (TOTAL)</td>
						  <td class="style-10" colspan="12" >&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  Comisión</td>
						  <td class="style-26">Etiquetas de columna</td>
						  <td class="style-26" colspan="12" >&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">1</td>
						  <td class="style-30 right">2</td>
						  <td class="style-30 right">3</td>
						  <td class="style-30 right">4</td>
						  <td class="style-30 right">5</td>
						  <td class="style-30 right">6</td>
						  <td class="style-30 right">7</td>
						  <td class="style-30 right">8</td>
						  <td class="style-30 right">9</td>
						  <td class="style-30 right">10</td>
						  <td class="style-30 right">11</td>
						  <td class="style-30 right">12</td>
						  <td class="style-43 right">Total general</td>
						 </tr>

						 @php 
						 $totalMonth = [];
						 $totalMonth['01'] = 0;
						 $totalMonth['02'] = 0;
						 $totalMonth['03'] = 0;
						 $totalMonth['04'] = 0;
						 $totalMonth['05'] = 0;
						 $totalMonth['06'] = 0;
						 $totalMonth['07'] = 0;
						 $totalMonth['08'] = 0;
						 $totalMonth['09'] = 0;
						 $totalMonth['10'] = 0;
						 $totalMonth['11'] = 0;
						 $totalMonth['12'] = 0;

						 $totalResumen = 0;
						 @endphp

						 @foreach ($arrayResume as $key => $resumeItem)

						 	<tr class="style-11">
						  		<td class="style-11">Usuario: {{ $key }}</td>

						  		@foreach ($months as $month)

						  			@php $valor_temp = 0; @endphp

						  			@if(isset($resumeItem['totales_comisiones'][2][$month]))

								   		@php

								   		$totalMonth[$month] = $totalMonth[$month] + $resumeItem['totales_comisiones'][2][$month]; 
								   		$valor_temp = $resumeItem['totales_comisiones'][2][$month];

								   		@endphp
								   	
								   	@endif
								   	@if(isset($resumeItem['totales_comisiones'][13][$month]))

								   		@php

								   		$totalMonth[$month] = $totalMonth[$month] + $resumeItem['totales_comisiones'][13][$month]; 
								   		$valor_temp = $valor_temp + $resumeItem['totales_comisiones'][13][$month];

								   		@endphp
								   	
								   	@endif

								   	<td class="style-10 right">@if($valor_temp!=0) {{ $valor_temp }} @endif</td>

						  		@endforeach

						  		@php $valor_temp = 0; @endphp

					  			@if(isset($resumeItem['totales_comisiones'][2][$year]))

							   		@php

							   		$valor_temp = $resumeItem['totales_comisiones'][2][$year];

							   		@endphp
							   	
							   	@endif
							   	@if(isset($resumeItem['totales_comisiones'][13][$year]))

							   		@php

							   		$valor_temp = $valor_temp + $resumeItem['totales_comisiones'][13][$year];

							   		@endphp
							   	
							   	@endif
							   	@php $totalResumen = $totalResumen + $valor_temp; @endphp
							  	<td class="style-10 right">{{ $valor_temp }}</td>
							</tr>
						 @endforeach

						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>

						  @foreach ($months as $month)

						  	<td class="style-42 right">{{ $totalMonth[$month] }}</td>

						  @endforeach
				
						  <td class="style-42 right">{{ $totalResumen }}</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						  <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 </tr>
						  <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="2" class="style-15">Total
						  Comisiones por Cliente</td>
						  <td class="style-10" colspan="12">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-43">Suma de
						  Comisión</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>

						 @foreach ($arrayResume as $key => $resumeItem)
						 	<tr>
							 	<td class="style-11">Usuario: {{ $key }}</td>

							 	@php $valor_temp = 0; @endphp

					  			@if(isset($resumeItem['totales_comisiones'][2][$year]))

							   		@php

							   		$valor_temp = $resumeItem['totales_comisiones'][2][$year];

							   		@endphp
							   	
							   	@endif
							   	@if(isset($resumeItem['totales_comisiones'][13][$year]))

							   		@php

							   		$valor_temp = $valor_temp + $resumeItem['totales_comisiones'][13][$year];

							   		@endphp
							   	
							   	@endif
							 	<td class="style-10 right">{{ $valor_temp }}</td>
							 	<td colspan="12" class="style-17">&nbsp;</td>
						 	</tr>
						 @endforeach

						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>
						  <td class="style-42 right">{{ $totalResumen }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-46">
						  <td colspan="14" class="style-47">&nbsp;</td>
						 </tr>
						 <tr class="style-60">
						  <td colspan="14" class="style-61">&nbsp;</td>
						 </tr>
						 <tr class="style-60">
						  <td colspan="14" class="style-61">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="2" class="style-15">Total
						  Comisiones por Concepto</td>
						  <td class="style-10" colspan="12">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-43">Suma de
						  Comisión</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">Inversión</td>

						  @php $total_inversion = 0; @endphp
						  @foreach ($arrayResume as $key => $resumeItem)

						  	@if(isset($resumeItem['totales_comisiones'][2][$year]))

						  		@php $total_inversion = $total_inversion + $resumeItem['totales_comisiones'][2][$year]; @endphp

						  	@endif
						  
						  @endforeach

						  <td class="style-10 right">{{ $total_inversion }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11">Cobro de dividendos</td>
						  @php $total_dividendos = 0; @endphp
						  @foreach ($arrayResume as $key => $resumeItem)

						  	@if(isset($resumeItem['totales_comisiones'][13][$year]))

						  		@php 
						  			$total_dividendos = 0;
						  			if (count($resumeItem['totales_comisiones']) >=3 ) {
						  				$total_dividendos = $total_dividendos + $resumeItem['totales_comisiones'][2][$year];
						  			}
						  		@endphp

						  	@endif
						  
						  @endforeach

						  <td class="style-10 right">{{ $total_dividendos }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>
						  <td class="style-42 right">{{ $totalResumen }}</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-60">
						  <td colspan="14" class="style-61">&nbsp;</td>
						 </tr>
						 <tr class="style-60">
						  <td colspan="14" class="style-61">&nbsp;</td>
						 </tr>
						 <tr class="style-60">
						  <td colspan="14" class="style-61">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-15" colspan="2">Comisiones
						  Generadas por cliente y mes relativas a la Inversión</td>
						  <td class="style-10" colspan="12">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="9" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  Comisión</td>
						  <td class="style-26">Etiquetas de columna</td>
						  <td colspan="12" class="style-17">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">1</td>
						  <td class="style-30 right">2</td>
						  <td class="style-30 right">3</td>
						  <td class="style-30 right">4</td>
						  <td class="style-30 right">5</td>
						  <td class="style-30 right">6</td>
						  <td class="style-30 right">7</td>
						  <td class="style-30 right">8</td>
						  <td class="style-30 right">9</td>
						  <td class="style-30 right">10</td>
						  <td class="style-30 right">11</td>
						  <td class="style-30 right">12</td>
						  <td class="style-62 right">Total general</td>
						 </tr>

						 @php 
						 $totalMonth = [];
						 $totalMonth['01'] = 0;
						 $totalMonth['02'] = 0;
						 $totalMonth['03'] = 0;
						 $totalMonth['04'] = 0;
						 $totalMonth['05'] = 0;
						 $totalMonth['06'] = 0;
						 $totalMonth['07'] = 0;
						 $totalMonth['08'] = 0;
						 $totalMonth['09'] = 0;
						 $totalMonth['10'] = 0;
						 $totalMonth['11'] = 0;
						 $totalMonth['12'] = 0;

						 $totalResumen = 0;
						 @endphp

						 @foreach ($arrayResume as $key => $resumeItem)

						 	<tr class="style-11">
						  		<td class="style-11">{{ $key }}</td>

						  		 @foreach ($months as $month)
						  		 
						  		 <td class="style-11 right">
						  		 @if(isset($resumeItem['totales_comisiones'][2][$month]))
						  		 
						  		 {{ $resumeItem['totales_comisiones'][2][$month] }}
						  		 
						  		 @php 
						  		 $totalMonth[$month] = $totalMonth[$month] + $resumeItem['totales_comisiones'][2][$month];
						  		 @endphp

						  		 @endif
						  		 </td>

						  		 @endforeach
						  		 
						  		 <td class="style-11 right">
						  		 @if(isset($resumeItem['totales_comisiones'][2][$year]))
						  			{{ $resumeItem['totales_comisiones'][2][$year] }}
						  			@php $totalResumen = $totalResumen + $resumeItem['totales_comisiones'][2][$year];  @endphp
						  		@endif
						  		</td>

						  	</tr>

						 @endforeach
						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>
						  @foreach ($months as $month)

						  	<td class="style-42 right">{{ $totalMonth[$month] }}</td>

						  @endforeach
				
						  <td class="style-42 right">{{ $totalResumen }}</td>
						 </tr>
						 <tr class="style-46">
						  <td colspan="14" class="style-47">&nbsp;</td>
						 </tr>
						 <tr class="style-46">
						  <td colspan="14" class="style-47">&nbsp;</td>
						 </tr>
						 <tr class="style-46">
						  <td colspan="14" class="style-47">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-15" colspan="2">Comisiones
						  Generadas por cliente y mes relativas a los Dividendos</td>
						  <td class="style-10" colspan="12">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-25">Suma de
						  Comisión</td>
						  <td class="style-26" colspan="2">Etiquetas de columna</td>
						  <td class="style-26" colspan="12">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-29">Etiquetas de
						  fila</td>
						  <td class="style-30 right">1</td>
						  <td class="style-30 right">2</td>
						  <td class="style-30 right">3</td>
						  <td class="style-30 right">4</td>
						  <td class="style-30 right">5</td>
						  <td class="style-30 right">6</td>
						  <td class="style-30 right">7</td>
						  <td class="style-30 right">8</td>
						  <td class="style-30 right">9</td>
						  <td class="style-30 right">10</td>
						  <td class="style-30 right">11</td>
						  <td class="style-30 right">12</td>
						  <td class="style-62 right">Total general</td>
						 </tr>

						 @php 
						 $totalMonth = [];
						 $totalMonth['01'] = 0;
						 $totalMonth['02'] = 0;
						 $totalMonth['03'] = 0;
						 $totalMonth['04'] = 0;
						 $totalMonth['05'] = 0;
						 $totalMonth['06'] = 0;
						 $totalMonth['07'] = 0;
						 $totalMonth['08'] = 0;
						 $totalMonth['09'] = 0;
						 $totalMonth['10'] = 0;
						 $totalMonth['11'] = 0;
						 $totalMonth['12'] = 0;

						 $totalResumen = 0;
						 @endphp

						 @foreach ($arrayResume as $key => $resumeItem)

						 	<tr class="style-11">
						  		<td class="style-11">{{ $key }}</td>

						  		 @foreach ($months as $month)
						  		 
						  		 <td class="style-11 right">
						  		 @if(isset($resumeItem['totales_comisiones'][13][$month]))
						  		 
						  		 {{ $resumeItem['totales_comisiones'][13][$month] }}
						  		 
						  		 @php 
						  		 $totalMonth[$month] = $totalMonth[$month] + $resumeItem['totales_comisiones'][13][$month];
						  		 @endphp

						  		 @endif
						  		 </td>

						  		 @endforeach
						  		 
						  		 <td class="style-11 right">
						  		 @if(isset($resumeItem['totales_comisiones'][13][$year]))
						  			{{ $resumeItem['totales_comisiones'][13][$year] }}
						  			@php $totalResumen = $totalResumen + $resumeItem['totales_comisiones'][13][$year];  @endphp
						  		@endif
						  		</td>

						  	</tr>

						 @endforeach
						 <tr class="style-11">
						  <td class="style-41">Total
						  general</td>
						  @foreach ($months as $month)

						  	<td class="style-42 right">{{ $totalMonth[$month] }}</td>

						  @endforeach
				
						  <td class="style-42 right">{{ $totalResumen }}</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="14" class="style-15">&nbsp;</td>
						 </tr>
						 <tr>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>------------</td>
						 	<td>----------------------</td>
						 </tr>
						</table>

		        </div>
		    </div>
		</div>
    </div>
</body>
</html>
