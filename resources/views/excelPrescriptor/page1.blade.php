<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
{{-- <link href="{{ asset(mix('assets/css/excel.css')) }}" rel="stylesheet"> --}}

<style>
	.cabecera-1{background-color:red}
</style>
</head>


<body>
    <div id="app">
        <div class="container">
		    <div class="row">
		        <div class="xs-4 col">
		        
					<table border="0" cellpadding="0" cellspacing="0">
						<tr class="style-0 xl65">
						  <td colspan="9" class="style-1 xl80">A) Resumen de movimientos</td>
						 </tr>
						 <tr class="style-0 xl66">
						  <td colspan="9" class="style-0 x181">&nbsp;</td>
						 </tr>
						 <tr class="style-11 x166">
						  <td colspan="2" class="style-12 xl85">Comision</td>
						  <td colspan="7" class="style-10 xl66">&nbsp;</td>
						 </tr>
						 <tr class="style-11 xl66">
						  <td class="style-13 xl76">Inversión</td>
						  <td class="style-14 right xl82">{{ $user->porcentaje_invertido }}%</td>
						  <td colspan="7" class="style-10 xl66">&nbsp;</td>
						 </tr>
						 <tr class="style-11 xl66">
						  <td class="style-13 xl76">cobro
						  dividendos</td>
						  <td class="style-14 right xl82">{{ $user->porcentaje_ganado }}%</td>
						  <td colspan="7" class="style-10 xl66">&nbsp;</td>
						 </tr>
						 <tr class="style-11 xl66">
						  <td colspan="9" class="style-11 xl66">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td colspan="9" class="style-15">&nbsp;</td>
						 </tr>
						 <tr class="style-11">
						  <td class="style-11 xl75">Nº Cliente</td>
						  <td class="style-16 xl75">Fecha</td>
						  <td class="style-16 xl75">Tipo Mov.<span style="display:none">vimiento</span></td>
						  <td class="style-16 xl75">Inmueble<span style="display:none">romoción</span></td>
						  <td class="style-16 xl75">Imp.<span style="display:none">e</span></td>
						  <td class="style-16 xl75">Mes</td>
						  <td class="style-16 xl75">año</td>
						  <td class="style-16 xl75">Comisión</td>
						  <td class="style-16 xl75">% Participación</td>
						</tr>

						@php

						$inmueble = '';
						$comision = 0;
						$importe = 0;
						$total = 0;
						$participacion = 0;
						$valorinvertido = 0;

						@endphp

						@foreach ($userTransactions as $transaction)
							@php
								$comision = 0;
								$importe = 0;
								$transId = $transaction->id;
				                // $inmueble = App\Property::whereHas('users', function ($q) use ($transId) {
				                //     $q->where('transaction_id', $transId);
				                // })->first();

				                $inmueble = $transaction->propertyUser();
				                if ($inmueble) {
				                	$inmueble = $inmueble->toArray();
				                }

								switch ($transaction->type) {
						            case 0:
						                $tipo = 'Pago con tarjeta';
						                $importe = $transaction->credit;
						                break;
						            case 1:
						                $tipo = 'Ingreso a mi cuenta';
						                $importe = $transaction->debit;
						                break;
						            case 2:
						                $tipo = 'Inversión en Inmueble';
						                $importe = -$transaction->debit;
						                $comision = formatThousands($transaction->prescriptor);
						                $valorinvertido = $transaction->debit;
						                break;
						            case 3:
						                $tipo = 'Ingreso por transferencia';
						                break;
						            case 4:
						                $tipo = 'Devolución';
						                break;
						            case 13:
						                $tipo = 'Divindendos generados por Inmueble';
						                $importe = $transaction->credit;
						                $comision = formatThousands($transaction->prescriptor);
						                break;
						            case 15:
						                $tipo = 'Regalo';
						                break;
						            default:
						            	break;
						        }
						        $total = $total+$importe;


						        if($inmueble['objetivo'] && $transaction->type == 2){
						        	$participacion = formatThousands($valorinvertido*100/$inmueble['objetivo']);
						        }else{
						        	$participacion = 0;
						        }
						        
							
							@endphp

							<tr class="">
								<td class="style-13 xl76">{{ $transaction->user_id }}</td>
								<td class="style-14 right xl76">{{ Carbon\Carbon::parse($transaction->fecha)->format('d-m-Y') }}</td>
								<td class="style-14 xl76">{{ $tipo }}</td>
								<td class="style-14 xl76">{{ $inmueble['ref'] }}</td>
								<td class="style-14 right xl76">{{ $importe }}</td>
								<td class="style-14 right xl76">{{ Carbon\Carbon::parse($transaction->fecha)->format('M') }}</td>
								<td class="style-14 right xl76">{{ Carbon\Carbon::parse($transaction->fecha)->format('Y') }}</td>
								<td class="style-14 right xl76">{{ $comision }}</td>
								<td class="style-14 right xl76">{{ $participacion }}</td>
							 </tr>
							
						@endforeach
						
						 <tr class="style-11">
						  <td class="style-11">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-10">&nbsp;</td>
						  <td class="style-22">total</td>
						  <td class="style-14 right">{{ $total }}</td>
						  <td colspan="4">&nbsp;</td>
						 </tr>
						 
						</table>

		        </div>
		    </div>
		</div>
    </div>
</body>
</html>
