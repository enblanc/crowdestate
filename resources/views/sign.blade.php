@extends('layouts.app')

@section('content')
<div id="hs-container"></div>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Sign document</div>

                <div class="panel-body">
                 
    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
ç@section('scripts')
 <script type="text/javascript" src="https://s3.amazonaws.com/cdn.hellofax.com/js/embedded.js"></script>
 <script type="text/javascript">
   HelloSign.init("{{ env('HELLOSIGN_CLIENT_KEY') }}");
   HelloSign.open({
        // Set the sign_url passed from the controller.
        url: "{{ $signUrl }}",
        uxVersion: 2,
        allowCancel: false,
        userCulture: HelloSign.CULTURES.ES_ES,
        redirectUrl: '{{ route('sign-response') }}',
        skipDomainVerification: true,
        height: 800,
        // Set the debug mode based on the test mode toggle.
        debug: {{ (env('HELLOSIGN_TEST_MODE') == 1 ? "true" : "false") }},
        // Point at the div we added in the content section.
        container: document.getElementById("hs-container"),
        // Define a callback for processing events.
        messageListener: function(e) {
        if (e.event == 'signature_request_signed') {
            // Process what to do once they are finished signing.
            }
        }
   });
 </script>
@stop