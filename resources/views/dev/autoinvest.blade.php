<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="icon" type="image/x-icon" href="{{ asset('assets/favicon/favicon.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Autoinvest - {{ $property->nombre }}</title>

    <script src="https://cdn.tailwindcss.com"></script>

    <script src="//unpkg.com/alpinejs" defer></script>
</head>

<body class="bg-gray-100">
    <div class="mx-auto max-w-5xl sm:p-6 lg:p-8">
        <div class="rounded-lg overflow-hidden bg-white shadow-lg border border-gray-100">
            <div class="mx-auto max-w-7xl py-5 px-4 text-center sm:px-6 lg:px-8">
                <div class="mt-5 mx-auto w-full | flex justify-center">
                    <div class="w-max-content">
                        <label id="listbox-label" class="sr-only"> Change published status </label>
                        <div class="relative" x-data="{ open: false }">
                            <div class="inline-flex divide-x divide-indigo-600 hover:divide-white rounded-md shadow-sm text-xl">
                                <button
                                    type="button" aria-haspopup="listbox" aria-expanded="true" aria-labelledby="listbox-label"
                                    class="inline-flex divide-x divide-indigo-600 hover:divide-white rounded-md shadow-sm | bg-transparent hover:bg-indigo-500 text-indigo-500 hover:text-white border border-indigo-500 hover:bg-indigo-600 focus:outline-none focus:ring-2 focus:ring-indigo-500 focus:ring-offset-2 focus:ring-offset-gray-50"
                                    @click="open = !open">
                                    <div
                                        class="inline-flex items-center rounded-l-md py-2 pl-3 pr-4 shadow-sm">
                                        <p class="ml-2.5 font-medium">
                                            {{ $property->nombre }}
                                        </p>
                                    </div>
                                    <div
                                        class="inline-flex items-center rounded-l-none rounded-r-md p-2 font-medium h-full">
                                        <span class="sr-only">Change published status</span>
                                        <!-- Heroicon name: mini/chevron-down -->
                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                            viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                            <path fill-rule="evenodd"
                                                d="M5.23 7.21a.75.75 0 011.06.02L10 11.168l3.71-3.938a.75.75 0 111.08 1.04l-4.25 4.5a.75.75 0 01-1.08 0l-4.25-4.5a.75.75 0 01.02-1.06z"
                                                clip-rule="evenodd" />
                                        </svg>
                                    </div>
                                </button>
                            </div>

                            <ul x-show="open"
                                class="overflow-auto max-h-60 | absolute right-0 z-10 mt-2 w-72 origin-top-right divide-y divide-gray-200 rounded-md bg-white shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none"
                                tabindex="-1" role="listbox" aria-labelledby="listbox-label"
                                aria-activedescendant="listbox-option-0">
                                @foreach ($properties as $p)
                                    @if ($property->id != $p->id)
                                        <li class="text-gray-900 cursor-default select-none px-4 py-1 text-sm" id="listbox-option-0"
                                            role="option">
                                            <a href="{{ route('dev.autoinvest-property', $p->id) }}" class="flex flex-col text-left">
                                                <div class="flex justify-between">
                                                    <p class="font-normal">
                                                        <strong>({{ $p->id }})</strong> {{ $p->nombre }}
                                                    </p>
                                                    <span class="text-indigo-500 hidden">
                                                        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg"
                                                            viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                            <path fill-rule="evenodd"
                                                                d="M16.704 4.153a.75.75 0 01.143 1.052l-8 10.5a.75.75 0 01-1.127.075l-4.5-4.5a.75.75 0 011.06-1.06l3.894 3.893 7.48-9.817a.75.75 0 011.05-.143z"
                                                                clip-rule="evenodd" />
                                                        </svg>
                                                    </span>
                                                </div>
                                                <p class="text-gray-500">
                                                    {{ $p->ubicacion }}
                                                </p>
                                            </a>
                                        </li>
                                    @endif
                                @endforeach
                                <!-- More items... -->
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="mt-5 mx-auto w-full">
                    <dl class="flex gap-1 flex-wrap w-full justify-center">
                        <div class="rounded flex flex-col px-5 pt-2 {{ isset($exceptions['remaining_days']) ? 'text-red-600 bg-red-200' : 'text-indigo-600 bg-indigo-100' }}">
                            <dd class="leading-none order-2 text-xl font-bold sm:text-2xl sm:tracking-tight">
                                {{ $property->fecha_finalizacion->format('Y-m-d') }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Fecha de Finalización</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 {{ isset($exceptions['remaining_days']) ? 'text-red-600 bg-red-200' : 'text-indigo-600 bg-indigo-100' }}">
                            <dd class="leading-none order-2 text-xl font-bold sm:text-2xl sm:tracking-tight">
                                {{ $property->remaining_days }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Días restantes</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 {{ isset($exceptions['porcentaje_completado']) ? 'text-red-600 bg-red-200' : 'text-indigo-600 bg-indigo-100' }}">
                            <dd class="leading-none order-2 text-xl font-bold sm:text-2xl sm:tracking-tight">
                                {{ $property->porcentaje_completado }}%
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Completado</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 {{ isset($exceptions['estado']) ? 'text-red-600 bg-red-200' : 'text-indigo-600 bg-indigo-100' }}">
                            <dd class="leading-none order-2 text-xl font-bold sm:text-2xl sm:tracking-tight">
                                {{ $property->state->nombre }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Estado</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 {{ isset($exceptions['estado']) ? 'text-red-600 bg-red-200' : 'text-indigo-600 bg-indigo-100' }}">
                            <dd class="leading-none order-2 text-xl font-bold sm:text-2xl sm:tracking-tight">
                                {{ $property->publicado ? 'Sí' : 'No' }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Publicado?</dt>
                        </div>                        
 
                        <div class="rounded flex flex-col px-5 pt-2 text-indigo-600 bg-indigo-100">
                            <dd class="leading-none order-2 text-2xl font-bold text-indigo-600 sm:text-3xl sm:tracking-tight">
                                {{ $property->ubicacion }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Ubicación</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 text-indigo-600 bg-indigo-100">
                            <dd class="leading-none order-2 text-2xl font-bold text-indigo-600 sm:text-3xl sm:tracking-tight">
                                {{ $property->tasa_interna_rentabilidad }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">TIR</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 text-indigo-600 bg-indigo-100">
                            <dd class="leading-none order-2 text-2xl font-bold text-indigo-600 sm:text-3xl sm:tracking-tight">
                                {{ $property->developer->nombre }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">Developer</dt>
                        </div>
                        <div class="rounded flex flex-col px-5 pt-2 text-indigo-600 bg-indigo-100">
                            <dd class="leading-none order-2 text-2xl font-bold text-indigo-600 sm:text-3xl sm:tracking-tight">
                                {{ $property->type->nombre }}
                            </dd>
                            <dt class="leading-none order-1 text-base font-medium text-gray-500">TIPO</dt>
                        </div>
                    </dl>
                </div>
            </div>

            @if (isset($exceptions))
                <div class="bg-red-50 p-4">
                    <div class="flex">
                        <div class="flex-shrink-0">
                            <svg class="h-6 w-6 text-red-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                fill="currentColor" aria-hidden="true">
                                <path fill-rule="evenodd"
                                    d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.28 7.22a.75.75 0 00-1.06 1.06L8.94 10l-1.72 1.72a.75.75 0 101.06 1.06L10 11.06l1.72 1.72a.75.75 0 101.06-1.06L11.06 10l1.72-1.72a.75.75 0 00-1.06-1.06L10 8.94 8.28 7.22z"
                                    clip-rule="evenodd" />
                            </svg>
                        </div>
                        <div class="ml-2">
                            <h3 class="text-base font-medium text-red-800">
                                Hubo {{ count($exceptions) }} errores con la propiedad
                            </h3>
                            <div class="mt-2 text-red-700">
                                <ul role="list" class="list-disc pl-5">
                                    @foreach ($exceptions as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

            <table class="min-w-full divide-y divide-gray-300">
                <thead class="bg-gray-50">
                    <tr>
                        <th
                            scope="col"class="w-0 | px-2 py-2 text-left text-sm font-semibold text-gray-900 sm:pl-6">
                            #ID
                        </th>
                        <th scope="col" class="w-0 | px-2 py-2 text-left text-sm font-semibold text-gray-900">
                            Cliente
                        </th>
                        <th scope="col" class="w-0 | px-2 py-2 text-left text-sm font-semibold text-gray-900">
                            Inivierte?
                        </th>
                        <th scope="col" class="px-2 py-2 text-left text-sm font-semibold text-gray-900">
                            Mensaje
                        </th>
                    </tr>
                </thead>
                <tbody class="divide-y divide-gray-200 bg-white">
                    @foreach ($investments as $investment)
                        <tr>
                            <td
                                class="w-0 | whitespace-nowrap pX-2 py-2 text-sm font-medium text-gray-900 sm:pl-6">
                                {{ $investment->autoinvest->id }}
                            </td>
                            <td class="w-0 | whitespace-nowrap px-2 py-2 text-sm text-gray-500">
                                <span class="block"><span
                                        class="font-semibold">{{ $investment->user->nombre }}</span>
                                    ({{ $investment->user->id }})</span>
                                <span class="block font-bold">{{ $investment->autoinvest->user->email }}</span>
                            </td>
                            <td
                                class="w-0 | whitespace-nowrap pX-2 py-2 text-sm font-medium text-gray-900 text-center">
                                <span
                                    class="rounded px-2 {{ $investment->status ? 'bg-green-300' : 'bg-gray-400' }}">{{ $investment->status ? 'SI' : 'NO' }}</span>
                            </td>
                            <td class="whitespace-nowrap px-2 py-2 text-sm text-gray-500">
                                {{ $investment->message }}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</body>

</html>
