<?php

return [

    'reset_password'             => [
        'subject'     => 'Passwort zurücksetzen',
        'first_line'  => 'Sie haben diese E-Mail erhalten, weil wir eine Anfrage zum Zurücksetzen Ihres Kontopassworts erhalten haben',
        'button'      => 'Passwort zurücksetzen',
        'second_line' => 'Wenn Sie nicht aufgefordert haben, das Kennwort zurückzusetzen, ist keine zusätzliche Aktion erforderlich.',
    ],

    'confirm_registration'       => [
        'subject'   => 'Bestätigen Sie Ihre E-Mail!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Ihre E-Mail wurde noch nicht bestätigt.',
        'message2'  => 'Um alle Services nutzen zu können, müssen Sie Ihre E-Mail-Adresse bestätigen.',
        'message3'  => 'Klicken Sie auf die folgende Schaltfläche, um Ihr Konto zu bestätigen:',
        'action'    => 'Bestätigen Sie jetzt Ihre E-Mail-Adresse',
    ],

    'general'                    => [
        'hello'     => 'Hallo!',
        'regards'   => 'Grüße,',
        'link'      => ' Wenn Sie ein Problem haben, indem Sie auf die Schaltfläche ":action" klicken, kopieren und fügen Sie den folgenden Link in Ihren Browser ein: ',
        'copyright' => 'Alle Rechte vorbehalten.',
    ],

    'email_resets'               => [
        'subject'   => 'Bestätigen Sie Ihre E-Mail!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Sie haben die E-Mail in Ihrem Admin-Bereich geändert.',
        'message2'  => 'Um diese Änderung zu bestätigen, müssen Sie Ihre E-Mail-Adresse bestätigen.',
        'message3'  => 'Klicken Sie auf die folgende Schaltfläche, um Ihr Konto zu bestätigen:',
        'action'    => 'Bestätigen Sie jetzt Ihre E-Mail-Adresse',
    ],

    'account_document_fail'      => [
        'subject'   => 'Fehler beim Überprüfen des Dokuments',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Bei der Validierung Ihres Dokuments ist ein Problem aufgetreten.',
        'message2'  => 'Der gefundene Fehler ist: :estado.',
        'message3'  => 'Bitte versuchen Sie es mit einem anderen Dokument. Vielen Dank',
        'action'    => 'Guthabenkonto',
        'status_0_doc_put_on_hold'   => 'Documento puesto en espera, esperando otro documento',
        'status_1_received_default_status' => 'Received (awaiting manual verification)',
        'status_2_accepted'  => 'Accepted',
        'status_3_rejected'    => 'Rejected',
        'status_4_rejected_doc_unreadable'    => 'Rejected. Unreadable document (cropped, blurred, dazzled...)',
        'status_5_rejected_doc_expired'    => 'Rejected. expired document',
        'status_6_rejected_wrong_type'    => 'Rejected. wrong type',
        'status_7_rejected_wrong_name'    => 'Rejected. Wrong name',
        'status_8_rejected_duplicate_document'    => 'Rejected. Duplicate document. The same document is already on file',
    ],

    'document_fail'      => [
        'status_2_accepted' => [
            'subject'   => 'Document validation successful!',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status. Accepted',
            'message2'  => 'Cogratulations, your documentation has been validated',
            'message3'  => 'Now, you can start investing!',
            'message4'  => 'If you need any help, do not hesitate to contact us.',
            'action'    => 'Go to BrickStarter',
        ],
        'status_3_rejected' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status. Rejected',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway.',
            'message3'  => 'Please upload a new file.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_4_rejected_doc_unreadable' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status. Rejected - Unreadable document (cropped, blurred, dark...)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway due to the poor image quality.',
            'message3'  => 'Please upload a better quality picture.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_5_rejected_doc_expired' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status: Rejected - Document expired (expiration date exceeded)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway due to the fact that the document has expired.',
            'message3'  => 'Please upload a new document.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_6_rejected_wrong_type' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status: Rejected - Incorrect type (Document not accepted)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway.',
            'message3'  => 'Please upload your ID/Passport.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_7_rejected_wrong_name' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation Status: Rejected - Bad Name (Name not matching user information)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway, as the name does not match your profile.',
            'message3'  => 'Please correct that on your profile and upload a new document.',
            'message4'  => 'If you need any help do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_8_rejected_duplicate_document' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status: Rejected - Duplicate document. The same document is already in the file.',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway as it has been marked as duplicated',
            'message3'  => 'Please upload a new document.',
            'message4'  => 'If you need any help do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
    ],

    'account_accredited'         => [
        'subject'   => 'Akkreditiertes Konto',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir haben gute Nachrichten. Jetzt können Sie anfangen zu investieren!',
        'action'    => 'Fangen Sie an zu investieren',
    ],

    'user_five_percent'          => [
        'subject'              => 'Generierte Interessen!',
        'greetings'            => 'Hallo, :name!',
        'message1'             => 'Wir informieren Sie, dass die Wohnung :inmueble in Betrieb genommen wurde, mit dem Eintritt unserer ersten Gäste. Wir haben Ihnen 5% verkaufsfördernder TAE auf Ihr Bankkonto überwiesen.',
        'message2'             => 'Von der Gesamtsumme haben wir :abonado € auf Ihr Lemonway-Konto eingezahlt, und der Rest :retenido €, wir werden es auf Ihren Namen einzahlen, gemäß der gesetzlichen Verpflichtung.',
        'message3'             => 'Gehen Sie in Ihren Benutzerbereich, um alle Details zu sehen.',
        'message_not_achieved' => 'Wir informieren Sie, dass die Wohnung :inmueble nicht erworben wurde. Wir haben jedoch die 5% Werbeaktion TAE auf Ihr Bankkonto überwiesen.',
        'message_not_spanish'  => 'Wir haben :abonado € auf Ihr Lemonway-Konto eingezahlt.',
        'action'               => 'Siehe meine Investitionen',
    ],

    'user_dividendos'            => [
        'subject'   => 'Generierte Interessen!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir informieren Sie, dass Ihre Investition in die Immobilie :inmueble im Monat :mes insgesamt :total € generiert hat. Von der Gesamtsumme haben wir :precio € auf Ihr Lemonway-Konto eingezahlt, und der Rest :retenido €, wir werden es auf Ihren Namen einzahlen, gemäß der gesetzlichen Verpflichtung.',
        'action'    => 'Siehe meine Investitionen',
    ],

    'user_dividendos_no_tribute' => [
        'subject'   => 'Generierte Interessen!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir informieren Sie, dass Ihre Investition in die Immobilie :inmueble im Monat :mes insgesamt :total € generiert hat, der sich nun auf Ihrem Brickstarter-Konto befindet.',
        'action'    => 'Siehe meine Investitionen',
    ],

    'diviendos_venta'            => [
        'subject'   => 'Interessen pro Verkauf!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir informieren Sie, dass die Immobilie :inmueble verkauft wurde und insgesamt :precio € im Konzept des Kapitalgewinns und der Rendite der Erstinvestition generiert hat.',
        'action'    => 'Sieh meine Bewegungen',
    ],

    'prescriptor_importe'        => [
        'subject'   => 'Betrag bezahlt!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir informieren Sie, dass die Investitionen Ihrer Empfehlungen und deren Dividenden im Monat :mes insgesamt: :precio € generiert haben.',
        'action'    => 'Siehe mein Panel',
    ],

    'prescriptor_excel'          => [
        'subject'   => 'Monatliche Kundenübersicht',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir legen die monatliche Zusammenfassung Ihrer Kunden bei. Sie können die Datei herunterladen, es ist als Anhang.',
    ],

    'bank_accredited'            => [
        'subject'   => 'Akkreditiertes Bankkonto',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Ihr IBAN-Konto wurde bereits bestätigt und Sie können Geld abheben.',
        'action'    => 'Geld abheben',
    ],

    'email_invited'              => [
        'subject'   => 'Dein Freund :name hat dir 15 € in Brickstarter gegeben!',
        'greetings' => 'Hi!',
        'message1'  => ' :name hat dich zu Brickstarter eingeladen. Dank dessen erhalten Sie 15 € für Ihre erste Investition.',
        'message2'  => 'Registrieren Sie sich und erhalten Sie die maximale Rendite für Ihr Geld. ',
        'action'    => 'Besuchen Sie Brickstarter',
    ],

    'user_inversion'             => [
        'subject'   => 'Sie haben in eine Immobilie investiert!',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir bestätigen die Investition von: :total € in die Immobilie :inmueble',
        'action'    => 'Siehe meine Investitionen',
    ],

    'property_failed'            => [
        'subject'   => 'Eigenschaft nicht erreicht :(',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir möchten Sie warnen, dass es nicht möglich war, die Immobilie zu kaufen :inmueble. Der von Ihnen investierte Betrag wurde an Sie zurückerstattet. Insgesamt :total €',
        'action'    => 'Sieh meine Bewegungen',
    ],

    'wire_in'                    => [
        'subject'   => 'Bearbeiteter Transfer',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Das System hat den :amount, der an Lemonway überwiesen wurde, bereits eingezogen. Du hast das Geld bereits auf deinem Konto.',
        'action'    => 'Sieh meine Bewegungen',
    ],

    'money_out_fail'             => [
        'subject'   => 'Keine Auszahlung von Geldern',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Wir hatten Probleme, Geld abzuheben. Besuchen Sie unseren Abschnitt Häufig gestellte Fragen und folgen Sie den Schritten, um Ihr Geld abzuheben.',
        'action'    => 'Siehe Seite Häufig gestellte Fragen',
    ],

    'money_chargueback'          => [
        'subject'   => 'Ihre Zahlung wurde an Sie zurückgeschickt',
        'greetings' => 'Hallo, :name!',
        'message1'  => 'Sie haben kürzlich eine Zahlung auf unserer Plattform getätigt. Die Zahlung wurde jedoch zurückerstattet. Überprüfen Sie Ihren Bewegungsverlauf',
        'action'    => 'Sieh meine Bewegungen',
    ],


];
