<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed'           => 'Diese Kombination aus Zugangsdaten wurde nicht in unserer Datenbank gefunden.',
    'throttle'         => 'Zu viele Loginversuche. Versuchen Sie es bitte in :seconds Sekunden nochmal.',

    'years'            => 'year|years',
    'months'           => 'month|months',
    'days'             => 'day|days',
    'and'              => 'and',

    'message_purchase' => '<span class="c-m">:quantity</span> of property<br> <span class="c-m">:property</span> is sold<br> for a total of',

    'message_sell'     => 'You are going to sell <span class="c-m">:quantity</span><br> of your investment in <span class="c-m">:property</span><br> for a total of',

    'buy_button'       => 'Buy :quantity by :price',
    'sell_button'      => 'Sell :quantity of my invest by :price',
];
