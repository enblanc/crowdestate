<?php

return [

    'home'      => '/',

    'user'      => [
        'acceso'          => 'login',
        'registro'        => 'registrierung',
        'recuperar'       => 'wiederherstellen',
        'oauth'           => [
            'registro' => 'auth/registrierung',
            'unir'     => 'auth/beitreten',
        ],
        'bienvenido'      => 'willkommen',
        'confirmar-email' => 'bestaetigen/e-mail-zurueckgesetzt/{emailresettoken}',
        'confirmado'      => '/bestaetigen/erfolg',
        'nueva-pass'      => 'neues-passwort/{token}',

    ],

    'panel'     => [
        'datos-basicos'          => 'panel/grundinformationen',
        'cambiar-contrasena'     => 'panel/aendere-das-passwort',
        'acreditar-cuenta'       => 'panel/guthabenkonto',
        'invitar'                => 'panel/einladen',
        'agregar-fondos'         => 'panel/fuege-geld-hinzu',
        'movimientos'            => 'panel/bewegungen',
        'cuentas-bancarias'      => 'panel/bankkonten',
        'retirar-fondos'         => 'panel/geld-abheben',
        'mis-inversiones'        => 'panel/meine-investitionen',
        'resumen-patrimonial'    => 'panel/patrimonialzusammenfassung',
        'mis-inversiones-single' => 'panel/meine-investitionen/{id}',
        'mi-mercado'             => 'panel/mein-marktplatz',
        'mercado-historico'      => 'panel/marktplatz-historical',
        'autoinvest'             => 'panel/autoinvest',
        'cetifications'          => 'panel/cetifications',
        'cetification'           => 'panel/cetifications/{certification}',
    ],

    'inmuebles' => [
        'listado' => 'eigenschaften',
        'single'  => 'eigentum/{property}',
    ],

    'mercado'   => [
        'listado' => 'marketplace',
        'show'    => 'marketplace/{token}',
        'exito'   => 'marketplace-success',
    ],

    'invertir'  => [
        'invertir_post' => 'investieren',
        'invertir_get'  => 'investieren/{id}',
        'pago_ok'       => 'investieren/pago/ok',
        'exito'         => 'investition/erfolg',
    ],

    'pagos'     => [
        'exito'     => 'zahlungen/erfolg',
        'cancelado' => 'zahlungen/stornieren',
        'error'     => 'zahlungen/fehler',
    ],

    'paginas'   => [
        'contacto'                   => 'kontakt',
        'prescriptores'              => 'verschreiber',
        'quienes-somos'              => 'wer-wir-sind',
        'como-funciona'              => 'wie-es-funktioniert',
        'faq'                        => 'faq',
        'faq_categoria'              => 'faq/{categoria}',
        'newsletter'                 => 'newsletter',
        'informacion-legal'          => 'rechtsinformation',
        'informacion-legal-lemonway' => 'rechtsinformation-lemonway',
    ],

    'blog'      => [
        'listado'   => 'blog',
        'entrada'   => 'blog/{slug}',
        'categoria' => 'blog/kategorie/{slug}',
    ],

];
