<?php

return [

    'home'      => '/',

    'user'      => [
        'acceso'          => 'login',
        'registro'        => 'register',
        'recuperar'       => 'restore',
        'oauth'           => [
            'registro' => 'auth/register',
            'unir'     => 'auth/join',
        ],
        'bienvenido'      => 'welcome',
        'confirmar-email' => 'confirm/emailreset/{emailresettoken}',
        'confirmado'      => '/confirm/success',
        'nueva-pass'      => 'new-password/{token}',

    ],

    'panel'     => [
        'datos-basicos'          => 'panel/basic-data',
        'cambiar-contrasena'     => 'panel/change-password',
        'acreditar-cuenta'       => 'panel/credit-account',
        'invitar'                => 'panel/invite',
        'agregar-fondos'         => 'panel/add-funds',
        'movimientos'            => 'panel/movements',
        'cuentas-bancarias'      => 'panel/bank-accountss',
        'retirar-fondos'         => 'panel/withdraw-funds',
        'resumen-patrimonial'    => 'panel/patrimonial-summary',
        'mis-inversiones'        => 'panel/my-investments',
        'mis-inversiones-single' => 'panel/my-investments/{id}',
        'mi-mercado'             => 'panel/my-marketplace',
        'mercado-historico'      => 'panel/marketplace-historical',
        'autoinvest'             => 'panel/autoinvest',
        'cetifications'          => 'panel/cetifications',
        'cetification'           => 'panel/cetifications/{certification}',
    ],

    'inmuebles' => [
        'listado' => 'properties',
        'single'  => 'property/{property}',
    ],

    'mercado'   => [
        'listado' => 'marketplace',
        'show'    => 'marketplace/{token}',
        'exito'   => 'marketplace-success',
    ],

    'invertir'  => [
        'invertir_post' => 'invest',
        'invertir_get'  => 'invest/{id}',
        'pago_ok'       => 'invest/payment/ok',
        'exito'         => 'invest/success',
    ],

    'pagos'     => [
        'exito'     => 'payments/success',
        'cancelado' => 'payments/cancel',
        'error'     => 'payments/error',
    ],

    'paginas'   => [
        'contacto'                   => 'contact',
        'prescriptores'              => 'prescribers',
        'quienes-somos'              => 'about-us',
        'como-funciona'              => 'how-does-it-work',
        'faq'                        => 'faq',
        'faq_categoria'              => 'faq/{categoria}',
        'newsletter'                 => 'newsletter',
        'informacion-legal'          => 'legal-information',
        'informacion-legal-lemonway' => 'legal-information-lemonway',
    ],

    'blog'      => [
        'listado'   => 'blog',
        'entrada'   => 'blog/{slug}',
        'categoria' => 'blog/category/{slug}',
    ],

];
