<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed'           => 'These credentials do not match our records.',
    'throttle'         => 'Too many login attempts. Please try again in :seconds seconds.',
    'email'            => 'The email is not in our records.',

    'years'            => 'year|years',
    'months'           => 'month|months',
    'days'             => 'day|days',
    'hours'            => 'hour|hours',
    'minutes'          => 'minute|minutes',
    'seconds'          => 'second|seconds',

    'and'              => 'and',

    'message_purchase' => '<span class="c-m">:quantity</span> of property<br> <span class="c-m">:property</span> is sold<br> for a total of',

    'message_sell'     => 'You are going to sell <span class="c-m">:quantity</span><br> of your investment in <span class="c-m">:property</span><br> for a total of',

    'buy_button'       => 'Buy :quantity by :price',
    'sell_button'      => 'Sell :quantity of my invest by :price',
];
