<?php

return [

    'reset_password'             => [
        'subject'     => 'Restore password',
        'first_line'  => 'You have received this email because we have received a request to reset your account password',
        'button'      => 'Restore password',
        'second_line' => 'If you did not request to reset the password, no further action is required.',
    ],

    'confirm_registration'       => [
        'subject'   => 'Confirm your email!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'Your email has not been verified yet.',
        'message2'  => 'In order to benefit from all the services, you need to verify your email.',
        'message3'  => 'Click on the following button to confirm your account:',
        'action'    => 'Confirm your email now',
    ],

    'general'                    => [
        'hello'     => 'Hello!',
        'regards'   => 'Regards,',
        'link'      => 'If you have any problems clicking on the ":action" button, copy and paste the following link into your browser: ',
        'copyright' => 'All rights reserved.',
    ],

    'email_resets'               => [
        'subject'   => 'Confirm your email!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'You have changed the mail in your admin panel.',
        'message2'  => 'In order to confirm this change, you need to verify your email.',
        'message3'  => 'Click on the following button to confirm your account:',
        'action'    => 'Confirm your email now',
    ],

    'account_document_fail'      => [
        'subject'   => 'Error validating document',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We had a problem validating your document.',
        'message2'  => 'The error found is: :estado.',
        'message3'  => 'Please, try another document. Thank you very much.',
        'action'    => 'Credit account',
        'status_0_doc_put_on_hold'   => 'Documento puesto en espera, esperando otro documento',
        'status_1_received_default_status' => 'Received (awaiting manual verification)',
        'status_2_accepted'    => 'Accepted',
        'status_3_rejected'    => 'Rejected',
        'status_4_rejected_doc_unreadable'        => 'Rejected. Unreadable document (cropped, blurred, dazzled...)',
        'status_5_rejected_doc_expired'           => 'Rejected. expired document',
        'status_6_rejected_wrong_type'            => 'Rejected. wrong type',
        'status_7_rejected_wrong_name'            => 'Rejected. Wrong name',
        'status_8_rejected_duplicate_document'    => 'Rejected. Duplicate document. The same document is already on file',
    ],

    'document_fail'      => [
        'status_2_accepted' => [
            'subject'   => 'Document validation successful!',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status. Accepted',
            'message2'  => 'Cogratulations, your documentation has been validated',
            'message3'  => 'Now, you can start investing!',
            'message4'  => 'If you need any help, do not hesitate to contact us.',
            'action'    => 'Go to BrickStarter',
        ],
        'status_3_rejected' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status. Rejected',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway.',
            'message3'  => 'Please upload a new file.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_4_rejected_doc_unreadable' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status. Rejected - Unreadable document (cropped, blurred, dark...)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway due to the poor image quality.',
            'message3'  => 'Please upload a better quality picture.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_5_rejected_doc_expired' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status: Rejected - Document expired (expiration date exceeded)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway due to the fact that the document has expired.',
            'message3'  => 'Please upload a new document.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_6_rejected_wrong_type' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status: Rejected - Incorrect type (Document not accepted)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway.',
            'message3'  => 'Please upload your ID/Passport.',
            'message4'  => 'If you need any help, do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_7_rejected_wrong_name' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation Status: Rejected - Bad Name (Name not matching user information)',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway, as the name does not match your profile.',
            'message3'  => 'Please correct that on your profile and upload a new document.',
            'message4'  => 'If you need any help do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
        'status_8_rejected_duplicate_document' => [
            'subject'   => 'Error validating document',
            'greetings' => 'Hi, :name !',
            'message1'  => 'Validation status: Rejected - Duplicate document. The same document is already in the file.',
            'message2'  => 'Unfortunately, your documentation has been rejected by Lemonway as it has been marked as duplicated',
            'message3'  => 'Please upload a new document.',
            'message4'  => 'If you need any help do not hesitate to contact us. info@brickstarter.com',
            'action'    => 'Account Accreditation',
        ],
    ],

    'account_accredited'         => [
        'subject'   => 'Accredited account',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We have good news. Now you can start investing!',
        'action'    => 'Start investing',
    ],

    'user_five_percent'          => [
        'subject'              => 'Interest generated!',
        'greetings'            => 'Hello, :name!',
        'message1'             => 'We inform you that the apartment :inmueble has entered into operation, with the entry our first guests. We have paid you 5% promotional TAE in your bank account.',
        'message2'             => 'Of the total, we have paid :abonado € into your Lemonway account, and the rest :retenido €, we will deposit in tax office in your name, according to legal imperative.',
        'message3'             => 'Access to your user area to see all the details.',
        'message_not_achieved' => 'We inform you that the apartment :inmueble has not been acquired. However, we have paid the 5% promotional TAE into your bank account.',
        'message_not_spanish'  => 'We have paid :abonado € into your Lemonway account.',
        'action'               => 'See my investments',
    ],

    'user_dividendos'            => [
        'subject'   => 'Interest generated!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We inform you that your investment in the property :inmueble has generated the month of :mes a total of :total €. Of the total, we have paid :precio € into your Lemonway account, and the rest :retenido €, we will deposit in tax office in your name, according to legal imperative.',
        'action'    => 'See my investments',
    ],

    'user_dividendos_no_tribute' => [
        'subject'   => 'Interest generated!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We inform you that your investment in the property :inmueble has generated the month of :mes a total of :total € that is now in your Brickstarter account.',
        'action'    => 'See my investments',
    ],

    'diviendos_venta'            => [
        'subject'   => 'Interest per sale!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We´d like to inform you that your real estate investment :inmueble has been sold generating a total of :precio in capital gain and return of capital investment',
        'action'    => 'See my movements',
    ],

    'prescriptor_importe'        => [
        'subject'   => 'Amount paid!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We inform you that the investments of your referrals and your dividends have generated the month of :mes a total of :precio €.',
        'action'    => 'See my panel',
    ],

    'prescriptor_excel'          => [
        'subject'   => 'Monthly customer summary',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We enclose the monthly summary of your clients. You can download the file, it is as an attachment.',
    ],

    'bank_accredited'            => [
        'subject'   => 'Accredited bank account',
        'greetings' => 'Hello, :name!',
        'message1'  => 'Your IBAN account has already been confirmed and you can withdraw funds.',
        'action'    => 'Extract funds',
    ],

    'email_invited'              => [
        'subject'   => 'Your friend :name has given you 15 € in Brickstarter!',
        'greetings' => 'Hello!',
        'message1'  => ' :name invited you to Brickstarter. Thanks to that you will receive € 15 on your first investment.',
        'message2'  => 'Register and start to get the maximum return on your money.',
        'action'    => 'Visit Brickstarter',
    ],

    'user_inversion'             => [
        'subject'   => 'You have invested in an Property!',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We confirm the investment of :total € in the property :inmueble',
        'action'    => 'See my investments',
    ],

    'property_failed'            => [
        'subject'   => 'Goal not reached :(',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We wanted to inform you that it has not been possible to fully fund the property :inmueble. The amount you have invested has been returned to you. A total of :total €',
        'action'    => 'See my movements',
    ],

    'wire_in'                    => [
        'subject'   => 'Transfer processed',
        'greetings' => 'Hello, :name!',
        'message1'  => 'The system has received the transfer of :amount made to Lemonway. You have the money in your account.',
        'action'    => 'See my movements',
    ],

    'money_out_fail'             => [
        'subject'   => 'Error withdrawing funds',
        'greetings' => 'Hello, :name!',
        'message1'  => 'We have had problems withdrawing funds. The money is still in your Lemonway account. Access our Frequently Asked Questions section and follow the steps to withdraw your funds.',
        'action'    => 'See Frequently Asked Questions page',
    ],

    'money_chargueback'          => [
        'subject'   => 'Your payment has been returned to you',
        'greetings' => 'Hello, :name!',
        'message1'  => 'You recently made a payment on our platform. However, the payment has been returned. Check your movement history',
        'action'    => 'Consult my movements',
    ],

];
