<?php

return [

    'home'      => '/',

    'user'      => [
        'acceso'          => 'login',
        'registro'        => 'registro',
        'recuperar'       => 'recuperar',
        'oauth'           => [
            'registro' => 'auth/registro',
            'unir'     => 'auth/unir',
        ],
        'bienvenido'      => 'bienvenido',
        'confirmar-email' => 'confirmar/restaurar-email/{emailresettoken}',
        'confirmado'      => '/confirmado/exito',
        'nueva-pass'      => 'nueva-contrasena/{token}',

    ],

    'panel'     => [
        'datos-basicos'          => 'panel/datos-basicos',
        'cambiar-contrasena'     => 'panel/cambiar-contrasena',
        'acreditar-cuenta'       => 'panel/acreditar-cuenta',
        'invitar'                => 'panel/invitar',
        'agregar-fondos'         => 'panel/agregar-fondos',
        'movimientos'            => 'panel/movimientos',
        'cuentas-bancarias'      => 'panel/cuentas-bancarias',
        'retirar-fondos'         => 'panel/retirar-fondos',
        'mis-inversiones'        => 'panel/mis-inversiones',
        'resumen-patrimonial'    => 'panel/resumen-patrimonial',
        'mis-inversiones-single' => 'panel/mis-inversiones/{id}',
        'mi-mercado'             => 'panel/mi-mercado',
        'mercado-historico'      => 'panel/mercado-historico',
        'autoinvest'             => 'panel/autoinvest',
        'cetifications'          => 'panel/certificaciones',
        'cetification'           => 'panel/certificaciones/{certification}',
    ],

    'inmuebles' => [
        'listado' => 'inmuebles/{state?}',
        'single'  => 'propiedad/{property}',
    ],

    'mercado'   => [
        'listado' => 'mercado',
        'show'    => 'mercado/{token}',
        'exito'   => 'mercado-exito',
    ],

    'invertir'  => [
        'invertir_post' => 'invertir',
        'invertir_get'  => 'invertir/{id}',
        'pago_ok'       => 'invertir/pago/ok',
        'exito'         => 'inversion/exito',
    ],

    'pagos'     => [
        'exito'     => 'pagos/correcto',
        'cancelado' => 'pagos/cancelado',
        'error'     => 'pagos/error',
    ],

    'paginas'   => [
        'contacto'                   => 'contacto',
        'prescriptores'              => 'prescriptores',
        'quienes-somos'              => 'quienes-somos',
        'como-funciona'              => 'como-funciona',
        'faq'                        => 'faq',
        'faq_categoria'              => 'faq/{categoria}',
        'newsletter'                 => 'newsletter',
        'informacion-legal'          => 'informacion-legal',
        'informacion-legal-lemonway' => 'informacion-legal-lemonway',
    ],

    'blog'      => [
        'listado'   => 'blog',
        'entrada'   => 'blog/{slug}',
        'categoria' => 'blog/categoria/{slug}',
    ],

];
