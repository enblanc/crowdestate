<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed'           => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle'         => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'email'            => 'El email no se encuentra en nuestros registros.',

    'years'            => 'año|años',
    'months'           => 'mes|meses',
    'days'             => 'día|días',
    'and'              => 'y',

    'message_purchase' => 'Se venden <span class="c-m">:quantity</span><br> del inmueble <span class="c-m">:property</span><br> por un total de',

    'message_sell'     => 'Vas a vender <span class="c-m">:quantity</span><br> del tu inversión en <span class="c-m">:property</span><br> por un total de',

    'buy_button'       => 'Comprar :quantity por :price',
    'sell_button'      => 'Vender :quantity de mi inversión por :price',
];
