<?php

return [

    'reset_password'             => [
        'subject'     => 'Restablecer contraseña',
        'first_line'  => 'Has recibido este email porque hemos recibido una petición para restablecer la contraseña de su cuenta',
        'button'      => 'Restablecer contraseña',
        'second_line' => 'Si no solicitó restablecer la contraseña, no se requiere ninguna acción adicional.',
    ],

    'confirm_registration'       => [
        'subject'   => '¡Confirma tu email!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Tu email aún no ha sido verificado.',
        'message2'  => 'Para poder beneficiarte de todos los servicios, necesitas verificar tu email.',
        'message3'  => 'Haz click en el siguiente botón para confirmar tu cuenta:',
        'action'    => 'Confirma tu email ahora',
    ],

    'general'                    => [
        'hello'     => '¡Hola!',
        'regards'   => 'Un saludo,',
        'link'      => ' Si tienes algún problema haciendo click en el botón de ":action", copia y pega el siguiente enlace en tu navegador: ',
        'copyright' => 'Todos los derechos reservados.',
    ],

    'email_resets'               => [
        'subject'   => '¡Confirma tu email!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Has cambiado el mail en tu panel de administración.',
        'message2'  => 'Para poder confirmar este cambio, necesitas verificar tu email.',
        'message3'  => 'Haz click en el siguiente botón para confirmar tu cuenta:',
        'action'    => 'Confirma tu email ahora',
    ],

    'account_document_fail'      => [
        'subject'   => 'Error al validar documento',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Hemos tenido problema al validar tu documento.',
        'message2'  => 'El error encontrado es: :estado.',
        'message3'  => 'Por favor, prueba con otro documento. Muchas gracias.',
        'action'    => 'Acreditar cuenta',
        'status_0_doc_put_on_hold'   => 'Documento puesto en espera, esperando otro documento',
        'status_1_received_default_status' => 'Recibido (en espera de verificación manual)',
        'status_2_accepted'  => 'Aceptado',
        'status_3_rejected'  => 'Rechazado',
        'status_4_rejected_doc_unreadable'    => 'Rechazado. Documento ilegible (recortado, borroso, deslumbrado...)',
        'status_5_rejected_doc_expired'    => 'Rechazado. Documento caducado (se ha superado la fecha de caducidad)',
        'status_6_rejected_wrong_type'    => 'Rechazado. Tipo incorrecto (Documento no aceptado)',
        'status_7_rejected_wrong_name'    => 'Rechazado. Nombre incorrecto (Nombre que no coincide con la información del usuario)',
        'status_8_rejected_duplicate_document'    => 'Rechazado. Documento duplicado. El mismo documento ya está en el archivo'
    ],

    'document_fail'      => [
        'status_2_accepted' => [
            'subject'   => '¡Validación de documento exitosa!',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Aceptado',
            'message2'  => 'Enhorabuena, ya hemos validado tu información y ya estás acreditado.',
            'message3'  => 'Ya puedes empezar a invertir.',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Ir a BrickStarter',
        ],
        'status_3_rejected' => [
            'subject'   => 'Error al validar documento',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Rechazado',
            'message2'  => 'Lamentamos comunicarte que Lemonway ha rechazado tu solicitud de acreditación. ',
            'message3'  => 'Te rogamos vuelvas a intentarlo.',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Acreditar cuenta',
        ],
        'status_4_rejected_doc_unreadable' => [
            'subject'   => 'Error al validar documento',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Rechazado - Documento ilegible (recortado, borroso, oscuro...)',
            'message2'  => 'Lamentamos comentarte que Lemonway ha rechazado la documentación que has proporcionado, debido a la mala calidad de la imagen.',
            'message3'  => 'Por favor, te rogamos repitas el proceso con una foto en la que la documentación se vea lo más nítida posible y sin brillos.',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Acreditar cuenta',
        ],
        'status_5_rejected_doc_expired' => [
            'subject'   => 'Error al validar documento',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Rechazado - Documento caducado (se ha superado la fecha de caducidad)',
            'message2'  => 'Lamentamos comentarte que Lemonway ha rechazado la documentación que has proporcionado, ya que ha pasado la fecha de validez del documento.',
            'message3'  => 'Por favor, te rogamos repitas el proceso con un documento en vigor.',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Acreditar cuenta',
        ],
        'status_6_rejected_wrong_type' => [
            'subject'   => 'Error al validar documento',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Rechazado - Tipo incorrecto (Documento no aceptado)',
            'message2'  => 'Lamentamos comentarte que Lemonway ha rechazado la documentación que has proporcionado, ya que ha detectado que ese tipo de documento no es válido.',
            'message3'  => 'Por favor, te rogamos repitas el proceso con tu DNI o pasaporte.',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Acreditar cuenta',
        ],
        'status_7_rejected_wrong_name' => [
            'subject'   => 'Error al validar documento',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Rechazado - Nombre incorrecto (Nombre que no coincide con la información del usuario)',
            'message2'  => 'Lamentamos comentarte que Lemonway ha rechazado la documentación que has proporcionado ya que el nombre no concuerda con la información que nos has proporcionado.',
            'message3'  => '',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Acreditar cuenta',
        ],
        'status_8_rejected_duplicate_document' => [
            'subject'   => 'Error al validar documento',
            'greetings' => '¡Hola, :name!',
            'message1'  => 'Estado de validación: Rechazado - Documento duplicado. El mismo documento ya está en el archivo',
            'message2'  => 'Lamentamos comentarte que Lemonway ha rechazado la documentación que has proporcionado ya que detecta que es el mismo documento que nos has facilitado anteriormente.',
            'message3'  => 'Por favor repite el proceso con un nuevo documento.',
            'message4'  => 'Si tienes alguna duda, puedes contactar con nosotros. info@brickstarter.com',
            'action'    => 'Acreditar cuenta',
        ],
    ],

    'account_accredited'         => [
        'subject'   => 'Cuenta acreditada',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Tenemos una buena noticia. ¡Ya puedes empezar a invertir!',
        'action'    => 'Empezar a invertir',
    ],

    'user_five_percent'          => [
        'subject'              => '¡Intereses generados!',
        'greetings'            => '¡Hola, :name!',
        'message1'             => 'Te informamos que el apartamento :inmueble ha entrado en explotación, con la entrada nuestros primeros huéspedes. Te hemos abonado el 5% TAE promocional en tu cuenta bancaria.',
        'message2'             => 'Del total, hemos abonado :abonado € en tu cuenta de Lemonway, y el resto :retenido €, lo ingresaremos en hacienda en tu nombre, según imperativo legal.',
        'message3'             => 'Accede a tu área de usuario para ver todos los detalles.',
        'message_not_achieved' => 'Te informamos que el apartamento :inmueble no se ha podido conseguir. Sin embargo, te hemos abonado el 5% TAE promocional en tu cuenta bancaria.',
        'message_not_spanish'  => 'Hemos abonado :abonado € en tu cuenta de Lemonway.',
        'action'               => 'Ver mis inversiones',
    ],

    'user_dividendos'            => [
        'subject'   => '¡Intereses generados!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te informamos que tu inversión en el inmueble :inmueble te ha generado el mes de :mes un total de :total € en intereses, de los cuales hemos ingresado :precio € en su cuenta de Brickstarter y el resto :retenido € lo ingresaremos en hacienda en tu nombre, según imperativo legal.',
        'action'    => 'Ver mis inversiones',
    ],

    'user_dividendos_no_tribute' => [
        'subject'   => '¡Intereses generados!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te informamos que tu inversión en el inmueble :inmueble te ha generado el mes de :mes un total de :total € en intereses que hemos ingresado en tu cuenta de Brickstarter.',
        'action'    => 'Ver mis inversiones',
    ],

    'diviendos_venta'            => [
        'subject'   => '¡Intereses por venta!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te informamos que el inmueble :inmueble se ha vendido y te ha generado un total de :precio € en concepto de plusvalía y devolución de inversión inicial.',
        'action'    => 'Ver mis movimientos',
    ],

    'prescriptor_importe'        => [
        'subject'   => '¡Importe abonado!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te informamos que las inversiones de tus referidos y sus dividendos te han generado el mes de :mes un total de :precio €.',
        'action'    => 'Ver mi panel',
    ],

    'prescriptor_excel'          => [
        'subject'   => 'Resumen mensual de clientes',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te adjuntamos el resumen mensual de tus clientes. Puedes descargar el archivo, está como adjunto.',
    ],

    'bank_accredited'            => [
        'subject'   => 'Cuenta bancaria acreditada',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Tu cuenta IBAN ya ha sido confirmada y ya puedes extraer fondos.',
        'action'    => 'Extraer fondos',
    ],

    'email_invited'              => [
        'subject'   => '¡Tu amigo :name te ha dado 15€ en Brickstarter!',
        'greetings' => '¡Hola!',
        'message1'  => ' :name te ha invitado a Brickstarter. Gracias a eso recibirás 15 € en tu primera inversión.',
        'message2'  => 'Regístrate ya comienza a sacar la máxima rentabilidad a tu dinero. ',
        'action'    => 'Visitar Brickstarter',
    ],

    'user_inversion'             => [
        'subject'   => '¡Has invertido en un Inmueble!',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te confirmamos la inversión de :total € en el inmueble :inmueble',
        'action'    => 'Ver mis inversiones',
    ],

    'property_failed'            => [
        'subject'   => 'Inmueble no conseguido :(',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Te queríamos avisar de que no ha sido posible comprar el inmueble :inmueble. Se te ha devuelto la cantidad que invertiste. Un total de :total €',
        'action'    => 'Ver mis movimientos',
    ],

    'wire_in'                    => [
        'subject'   => 'Transferencia procesada',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'El sistema ya ha recogido la transferencia de :amount realizada a Lemonway. Ya tienes el dinero en tu cuenta.',
        'action'    => 'Ver mis movimientos',
    ],

    'money_out_fail'             => [
        'subject'   => 'Error al retirar fondos',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Hemos tenido problemas al retirar fondos. El dinero sigue en tu cuenta Lemonway. Accede a nuestra sección de Preguntas Frecuentes y sigue los pasos para retirar tus fondos.',
        'action'    => 'Ver página de Preguntas Frequentes',
    ],

    'money_chargueback'          => [
        'subject'   => 'Tu pago te ha sido devuelto',
        'greetings' => '¡Hola, :name!',
        'message1'  => 'Recientemente hiciste un pago en nuestra plataforma. Sin embargo el pago ha sido devuelto. Consulta tu historial de movimientos.',
        'action'    => 'Ver mis movimientos',
    ],




];
