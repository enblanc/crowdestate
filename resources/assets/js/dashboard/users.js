import '../bootstrap-dashboard';
import 'parsleyjs';
import 'parsleyjs/dist/i18n/es.js';
import PulseLoader from 'vue-spinner/src/PulseLoader.vue'
import TransactionsUser from './../components/TransactionsUser.vue'

window.userVue = new Vue({
	// =========================== #Punto de entrada======================= //

	el: '#user-div',

	components: {
		PulseLoader,
		TransactionsUser
	},

	data: {
		user: {
			nombre: null,
			apellido1: null,
			apellido2: null,
			email: null,
			password: null,
			myRoles: [],
			realRoles: [],
		},
		roles: {
			options: [
				{value: "5", label: "User"},
				{value: "4", label: "Inversor"},
				{value: "3", label: "Prescriptor"},
				{value: "2", label: "Admin"},
			]
		},
		manual: {
			amount: 0,
			property: "Elige una propiedad",
		},
		addMoney: {
			amount: 0
		},
		properties: properties, 
		loading: true,
		color: "#67d3e0",
		size: "26px",
	},

	methods: {

		formValidation() {
	    	// Parsley.setLocale('es');
			var parsleyOptions = {
			    errorClass: 'has-error',
			    successClass: 'has-success',
			    classHandler: function(ParsleyField) {
			        return ParsleyField.$element.parents('.form-group')
			    },
			    errorsContainer: function(ParsleyField) {
			        return ParsleyField.$element.parents('.form-group')
			    },
			    errorsWrapper: '<span class="help-block">',
			    errorTemplate: '<div></div>'
			}

			$('form').parsley( parsleyOptions )
	    },

	    getDataUser() {
	    	window.axios.get(userRoute)
			.then((response) => {
				this.user = response.data.data
				let roles = [];
				_.forEach(this.user.roles, function(rol) {
					roles.push({value: rol.id, label: rol.name})
				});
				this.user.myRoles = roles;
				this.updateRoles()
				this.loading = false
				setTimeout( () => {
					activejQueryPlugins()
				},700)
			})
			.catch((response) => {
				console.log(response)
			})
	    },

	    updateRoles() {

	    	let rolesId = [];

			_.forEach(this.user.myRoles, function(rol) {
			  rolesId.push(parseInt(rol.value))
			});
			this.user.realRoles = rolesId;
	    },

	    revisarInversion() {
	    	$("#modalcheckInversionManual").modal('show');
	    },

	    cancelInversion() {
	    	$("#modalcheckInversionManual").modal('hide');
	    },

	    sendManual() {
	    	$('#inversion_manual_form').submit();
	    },

	    checkRegalos() {
	    	$("#modalBorrarRegalos").modal('show');
	    },

	    borrarRegalos() {
	    	$('#borrar_regalos_form').submit();
	    },

	    checkBorraUsuario() {
	    	$("#modalBorrarUsuario").modal('show');
	    },

	    borrarUsuario() {
	    	$('#borrar_usuario_form').submit();
	    },
	},

	mounted () {
		this.formValidation()
		if (userRoute) {
			this.getDataUser();
		} else {
			this.loading = false;
		}
		
	},

	computed: {

		activado: function() {
			if(this.user.confirm && this.user.confirm.is_confirmed){

				return (this.user.confirm.is_confirmed) ? 1 : 0;	
			} else {
				return 0;
			}
		},

		puedeInvertir () {
			if(this.manual.property === "Elige una propiedad") {
				return false;
			}

			return (this.manual.amount > 0) ? true : false
		},

		manualAmount() {
			return parseInt(this.manual.amount).toLocaleString('es-ES', {
    			style: 'currency', 
			    currency: 'EUR', 
			    minimumFractionDigits: 0,
			    currencyDisplay: "symbol"
			});
		}
	}
})