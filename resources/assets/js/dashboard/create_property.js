 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/




/*	==========================================================================
	#IMPORT
	========================================================================== */

	// import '../../../../public/dashboard_theme/assets/plugins/redactor/redactor.js';
	// import '../../../../public/dashboard_theme/assets/plugins/redactor/plugins/fullscreen.js';
	// import '../../../../public/dashboard_theme/assets/plugins/redactor/plugins/imagemanager.js';
	// import '../../../../public/dashboard_theme/assets/plugins/redactor/plugins/video.js';

	import '../bootstrap-dashboard';
	import 'parsleyjs';
	import 'parsleyjs/dist/i18n/es.js';

	import PulseLoader from 'vue-spinner/src/PulseLoader.vue'
	import Dropzone from 'vue2-dropzone'


	import {VueMasonryPlugin} from 'vue-masonry'
	window.Vue.use(VueMasonryPlugin)


	import PhotoGrid from './../components/PhotosProperties.vue'
	import ResumenModal from './../components/ResumenModal.vue'
	import EvolucionTable from './../components/EvolucionTable.vue'
	import EvolucionModal from './../components/EvolucionModal.vue'

	let d = new Date();
	var currentMonth = d.getMonth();

	var map, coordinates, geocoder, marker;
	var geocoder = new google.maps.Geocoder();

/*	==========================================================================
	#VUE
	========================================================================== */
	window.formEditProperty = new Vue({

		// =========================== #Punto de entrada======================= //
	
		el: '#create-property',

		components: {
		    PulseLoader, Dropzone, PhotoGrid, ResumenModal, EvolucionTable, EvolucionModal
		},

		// =========================== #Datos ================================= //

		data: {
			property: {
				type: {
					id: ""
				},
				state: {
					id: ""
				},
				investment: {
					id: ""
				},
				developer: {
					id: ""
				},
				lat: 39.4666667,
				lng: -0.3666667,
			},
			photos: [],
			photoMixer: null,
			temporalResumen: {},
			temporalEvolucion: {},
			search: null,
			searchError: false,
			showMap: true,
			color: "#67d3e0",
			loading: true,
			size: "26px",
			resumen: {
				file: null,
				loading: false,
				loading_update: false,
			},
			resumen_errors: {},
			resumen_update_errors: {},
			evolucion: {
				loading: false,
				size: "20px",
				upload: {
					loading: false,
					loading_update: false,
					file: null
				},
				months: {
					selected: currentMonth,
					options: [
						{ text: 'Enero', value: 0 },
						{ text: 'Febrero', value: 1 },
						{ text: 'Marzo', value: 2 },
						{ text: 'Abril', value: 3 },
						{ text: 'Mayo', value: 4 },
						{ text: 'Junio', value: 5 },
						{ text: 'Julio', value: 6 },
						{ text: 'Agosto', value: 7 },
						{ text: 'Semptiembre', value: 8 },
						{ text: 'Octubre', value: 9 },
						{ text: 'Noviembre', value: 10 },
						{ text: 'Diciembre', value: 11 },

					]
				}
			},
			evolucion_errors: {},
			evolucion_update_errors: {},
			dzOptions: {
			    dictDefaultMessage: '<i class="fa fa-cloud-upload pt10"></i><span class="main-text"><b>Arrastra imagenes</b> para subirlos</span><br> <span class="sub-text">(o haz click)</span> ',
			    acceptedFiles: '.jpg,.jpeg,.png',
			    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken}
			},
			
			
		},

		// =========================== #Methods =============================== //
		methods: {
			
		    formValidation() {
		    	Parsley.setLocale('es');
				var parsleyOptions = {
				    errorClass: 'has-error',
				    successClass: 'has-success',
				    classHandler: function(ParsleyField) {
				        return ParsleyField.$element.parents('.form-group');
				    },
				    errorsContainer: function(ParsleyField) {
				        return ParsleyField.$element.parents('.form-group');
				    },
				    errorsWrapper: '<span class="help-block">',
				    errorTemplate: '<div></div>'
				};
				$('#create-property-form').parsley( parsleyOptions );
		    },

		    // Inicia el mapa localizado en sus sitio
			initMap () {
				
				// this.showMap = true
				coordinates =  new google.maps.LatLng(parseFloat(39.4666667), parseFloat(-0.3666667));
				
			  	map = new google.maps.Map(document.getElementById('mapa'), {
				    zoom: 16,
				    center: coordinates,
				    scrollwheel: false,
			  	});

			  	window.map = map;

				marker = new google.maps.Marker({
					icon: '/dashboard_theme/assets/img/house-marker.png',
					position: coordinates,
					map: map,
					draggable: true
				});

				marker.addListener('drag', this.markerMoved);
    			marker.addListener('dragend', this.markerMoved);

    			

				// infowindow = new google.maps.InfoWindow();
				// service = new google.maps.places.PlacesService(map);
		    },

		    searchPlace() {
		    	let self = this;
		    	this.searchError = false;
		    	geocoder.geocode(
			        {'address': self.property.direccion}, 
			        function(results, status) { 
			            if (status == google.maps.GeocoderStatus.OK) { 
			                var loc = results[0].geometry.location;
			                marker.setPosition(loc);
			                map.panTo(loc);
			                map.setZoom(16);
			                self.property.lat = parseFloat(loc.lat())
		    				self.property.lng = parseFloat(loc.lng())
			            } 
			            else {
			                self.searchError = true;
			            } 
			        }
			    );
		    },

		    markerMoved(event) {
		    	this.property.lat = parseFloat(event.latLng.lat())
		    	this.property.lng = parseFloat(event.latLng.lng())
		    },

			getPropertyData() {
				this.property = {
					...this.property,
					...property
				}
			},
		},


		created () {
			this.getPropertyData();
			// createRedactor();
			this.loading = false;
			setTimeout( () => {
				this.formValidation()
				activejQueryPlugins()
				this.initMap()
			},500)
		},
		
		mounted () {
			// $('#create-property').parsley().on('field:validate', function() {
			//     var elem = this.$element;

			//     // If the field is not visible, do not apply Parsley validation!
			//     if ( !$( elem ).is( ':visible' ) ) {
			//         this.constraints = [];
			//     }
			// });

			// setTimeout( () => {
			// 	this.initMap();
			// },1000)
		},
		computed: {

			isPremiumZone: function()
			{
				return (this.property.zona_premium) ? 1 : 0;
			},

			isPrivate: function()
			{
				return (this.property.private) ? 1 : 0;
			},

		}
	})

