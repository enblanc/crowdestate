 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/




/*	==========================================================================
	#IMPORT
	========================================================================== */

	// import '../../../../public/dashboard_theme/assets/plugins/redactor/redactor.js';
	// import '../../../../public/dashboard_theme/assets/plugins/redactor/plugins/fullscreen.js';
	// import '../../../../public/dashboard_theme/assets/plugins/redactor/plugins/imagemanager.js';
	// import '../../../../public/dashboard_theme/assets/plugins/redactor/plugins/video.js';

	import '../bootstrap-dashboard';
	import Parsley from 'parsleyjs';
	// import './../utilities/parsley.js'
	import 'parsleyjs/dist/i18n/es.js';

	import PulseLoader from 'vue-spinner/src/PulseLoader.vue'
	import Dropzone from 'vue2-dropzone'

	import {VueMasonryPlugin} from 'vue-masonry'
	window.Vue.use(VueMasonryPlugin)

	import PhotoGrid from './../components/PhotosProperties.vue'
	import DocumentsGrid from './../components/DocumentsGrid.vue'
	import ResumenModal from './../components/ResumenModal.vue'
	import EstimacionModal from './../components/EstimacionModal.vue'
	import EvolucionTable from './../components/EvolucionTable.vue'
	import EvolucionModal from './../components/EvolucionModal.vue'
	import moment from 'moment'

	let d = new Date();
	var currentMonth = d.getMonth();
	var currentYear = d.getFullYear();

	var map, coordinates, geocoder, marker;
	var geocoder = new google.maps.Geocoder();

	Vue.filter('formatDate', function(value) {
	  if (value) {
	    return moment(String(value)).format('DD/MM/YYYY hh:mm')
	  }
	})

	function reloadVueMasonry() {
		console.log('pollos');
		Vue.redrawVueMasonry();
	}

/*	==========================================================================
	#VUE
	========================================================================== */
	window.formEditProperty = new Vue({
		// =========================== #Punto de entrada======================= //
		el: '#edit-property',

		components: {
		    PulseLoader, Dropzone, PhotoGrid, ResumenModal, EstimacionModal, EvolucionTable, EvolucionModal, DocumentsGrid
		},

		// =========================== #Datos ================================= //
		data: {
			property: {},
			inversores: {
				loading: true,
				users: []
			},
			photos: [],
			photoMixer: null,
			temporalResumen: {},
			temporalEstimacion: {},
			temporalEvolucion: {},
			search: null,
			searchError: false,
			showMap: true,
			color: "#67d3e0",
			loading: true,
			size: "26px",
			resumen: {
				file: null,
				loading: false,
				loading_update: false,
			},
			resumen_errors: {},
			resumen_update_errors: {},
			estimacion: {
				file: null,
				loading: false,
				loading_update: false,
			},
			estimacion_data: {
				'numero_reservas': null,
				'grado_ocupacion': null,
				'dias_reservados': null,
				'dias_no_reservados': null,
				'dias_bloqueados': null,
				'tarifa_media_dia': null,
				'estimacion_ingresos_anuales': null,
				'satisfaccion_clientes': null,
				'numero_invitados': null,
				'ingresos_totales': null,
				'ingresos_alquiler': null,
				'ingresos_venta': null,
				'otros_ingresos': null,
				'gastos': null,
				'gastos_explotacion': null,
				'publicidad_prensa': null,
				'otro_material': null,
				'gastos_ventas': null,
				'i_mas_d': null,
				'arrendamientos': null,
				'comisiones_crowdestate': null,
				'gestion': null,
				'suministros': null,
				'ibi': null,
				'otras_tasas': null,
				'amortizacion': null,
				'subvenciones': null,
				'enajenacion_inmovilizado': null,
				'resultado_explotacion': null,
				'resultado_financiero': null,
				'ingresos_financieros': null,
				'gastos_financieros': null,
				'otros_instrumentos_financieros': null,
				'resultado_antes_impuestos': null,
				'impuestos': null,
				'resultados_ejercicio': null,
				'dividendos_generados': null,
				'dividendos_abonar': null,
				'total_dividendos_abonados': null,
				'dividendos_cartera': null,
				'total_dividendos_devengados': null,
				'revalorizacion_inmueble': null,
				'valor_actual_inmueble': null
			},
			estimacion_errors: {},
			estimacion_update_errors: {},
			evolucion: {
				loading: false,
				size: "20px",
				upload: {
					loading: false,
					loading_update: false,
					file: null
				},
				months: {
					selected: currentMonth,
					options: [
						{ text: 'Enero', value: 0 },
						{ text: 'Febrero', value: 1 },
						{ text: 'Marzo', value: 2 },
						{ text: 'Abril', value: 3 },
						{ text: 'Mayo', value: 4 },
						{ text: 'Junio', value: 5 },
						{ text: 'Julio', value: 6 },
						{ text: 'Agosto', value: 7 },
						{ text: 'Semptiembre', value: 8 },
						{ text: 'Octubre', value: 9 },
						{ text: 'Noviembre', value: 10 },
						{ text: 'Diciembre', value: 11 },
					]
				},
				years: {
					selected: currentYear,
					options: []
				}
			},
			evolucion_errors: {},
			evolucion_update_errors: {},
			dzOptions: {
			    dictDefaultMessage: '<i class="fa fa-cloud-upload pt10"></i><span class="main-text"><b>Arrastra imagenes</b> para subirlas</span><br> <span class="sub-text">(o haz click)</span> ',
			    acceptedFiles: '.jpg,.jpeg,.png',
			    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken}

			},
			dzOptionsDocuments: {
			    dictDefaultMessage: '<i class="fa fa-cloud-upload pt10"></i><span class="main-text"><b>Arrastra archivos pdf</b> para subirlos</span><br> <span class="sub-text">(o haz click)</span> ',
			    acceptedFiles: '.pdf',
			    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken}
			},
			estado_inmueble_original : null,
			estado_errors: {},
			estado_diferencia: null,
			pagar_diferencia: {
				loading: false,
				errors: {}
			},
			propertyDetails: null,
			documentUploadOptions: [],
		},


		// =========================== #Methods =============================== //
		methods: {		
			getPropertyData() {
				window.axios.get(propertyRoute)
					.then((response) => {
						this.property = response.data.data
						this.photos = this.property.photos
						if (this.property.predicted) {
							if (this.property.predicted[0]) {
								this.estimacion_data = this.property.predicted[0]	
							}
						}
						this.estado_inmueble_original = this.property.state.id
						this.loading = false;
						setTimeout( () => {
							this.formValidation()
							activejQueryPlugins()
							this.initMap()
						},1000)

						
					})
					.catch((response) => {
						console.log(response)
					})
			},

			getInversoresProperty () {
				this.inversores.loading = true;
				window.axios.get(propertyInversoresRoute)
					.then((response) => {
						this.inversores.users = response.data.data
						this.inversores.loading = false;				
					})
					.catch((response) => {
						this.inversores.loading = false;
					})
			},

		    formValidation() {
		    	// Parsley.setLocale('es');
				var parsleyOptions = {
				    errorClass: 'has-error',
				    successClass: 'has-success',
				    classHandler: function(ParsleyField) {
				        return ParsleyField.$element.parents('.form-group');
				    },
				    errorsContainer: function(ParsleyField) {
				        return ParsleyField.$element.parents('.form-group');
				    },
				    errorsWrapper: '<span class="help-block">',
				    errorTemplate: '<div></div>'
				};
				$('#update-property-form').parsley( parsleyOptions );
		    },
		    // Resumen
		    uploadResumenFile(e) {
		    	let files = e.target.files || e.dataTransfer.files;
			    if (!files.length)
			        return;

			    let value = e.target.value

		    	$("#upload-resumen-file").val(value)

		    	this.resumen.file = new FormData();
	            this.resumen.file.append('resumen', files[0]);

	            this.resumen.loading = true
	            this.resumen_errors = {}
	            this.temporalResumen = {};

		    	window.axios.post(propertyResumenRoute, this.resumen.file)
				.then((response) => {
					this.resumen.loading = false
					$("#modalResumen").modal('show')
					this.temporalResumen = response.data;
					this.resumen_errors = {}
				})
				.catch((error) => {
					if (error.response.data.error) {
						this.resumen_update_errors = error.response.data.error
					} else {
						this.resumen_errors = error.response.data;
					}
					this.resumen.loading = false
				})
		    },

		    updateResumen() {
		    	if (Object.keys(this.temporalResumen).length === 0 && this.temporalResumen.constructor === Object) {
		    		return false;
		    	}

		    	this.resumen.loading_update = true;

		    	window.axios.post(propertyUpdateResumenRoute, this.resumen.file)
				.then((response) => {
					$("#modalResumen").modal('hide')
					this.resumen.loading_update = false
					location.reload(true);
				})
				.catch((error) => {
					if (error.response.data.error) {
						this.resumen_update_errors = error.response.data.error
					} else {
						this.resumen_update_errors =  { message: 'Error al procesar. Vuelve a probar por favor.' };
					}
					this.resumen.loading_update = false
				})
		    },

		    //Estimacion
		    uploadEstimacionFile(e) {
		    	let files = e.target.files || e.dataTransfer.files;
			    if (!files.length)
			        return;

			    let value = e.target.value

		    	$("#upload-estimacion-file").val(value)

		    	this.estimacion.file = new FormData();
	            this.estimacion.file.append('estimacion', files[0]);

	            this.estimacion.loading = true
	            this.estimacion_errors = {}
	            this.temporalestimacion = {};

		    	window.axios.post(propertyEstimacionRoute, this.estimacion.file)
				.then((response) => {
					this.estimacion.loading = false
					$("#modalEstimacion").modal('show')
					this.temporalEstimacion = response.data;
					this.estimacion_errors = {}
				})
				.catch((error) => {
					if (error.response) {
						this.estimacion_errors = error.response.data;
					}
					this.estimacion.loading = false
				})
		    },

		    updateEstimacion() {
		    	if (Object.keys(this.temporalEstimacion).length === 0 && this.temporalEstimacion.constructor === Object) {
		    		return false;
		    	}

		    	this.estimacion.loading_update = true;

		    	window.axios.post(propertyUpdateEstimacionRoute, this.estimacion.file)
				.then((response) => {
					$("#modalEstimacion").modal('hide')
					this.estimacion.loading_update = false
					location.reload(true);
				})
				.catch((error) => {
					if (error.response.data.error) {
						this.estimacion_update_errors = error.response.data.error
					} else {
						this.estimacion_update_errors =  { message: 'Error al procesar. Vuelve a probar por favor.' };
					}
					this.estimacion.loading_update = false
				})
		    },

		    // Evolucion/Seguimiento
		    cambiaTablaEvolucion(e) {
		    	let value = e.target.value;
		    	this.currentYear = value;
		    	this.evolucion.years.selected = parseInt(value);
		    	this.evolucion.loading = true
	    		window.axios.get(propertyEvolutionDataRoute + '/' + value)
				.then((response) => {
					this.evolucion.loading = false
					this.property.currentEvolution = response.data.data;
				})
				.catch((error) => {
					this.evolucion.loading = false
				})
		    },

		    uploadEvolutionFile(e) {
		    	let files = e.target.files || e.dataTransfer.files;
			    if (!files.length)
			        return;

			    let value = e.target.value

		    	$("#upload-evolucion-file").val(value)

		    	this.evolucion.upload.file = new FormData()
	            this.evolucion.upload.file.append('evolucion', files[0])
	            this.evolucion.upload.file.append('mes', this.evolucion.months.selected)
	            this.evolucion.upload.file.append('year', this.evolucion.years.selected)

	            this.evolucion.upload.loading = true
	            this.evolucion_errors = {}
	            this.temporalEvolucion = {};

		    	window.axios.post(propertyEvolucionRoute, this.evolucion.upload.file)
				.then((response) => {
					this.evolucion.upload.loading = false
					$("#modalEvolucion").modal('show')
					this.temporalEvolucion = response.data;
					this.evolucion_errors = {}
				})
				.catch((error) => {
					if (error.response) {
						this.evolucion_errors = error.response.data;
					}
					this.evolucion.upload.loading = false
				})
		    },

		    updateEvolucion() {
		    	if (Object.keys(this.temporalEvolucion).length === 0 && this.temporalEvolucion.constructor === Object) {
		    		return false;
		    	}

		    	this.evolucion.upload.loading_update = true;

		    	window.axios.post(propertyUpdateEvolucionRoute, this.evolucion.upload.file)
				.then((response) => {
					$("#modalEvolucion").modal('hide')
					this.evolucion.upload.loading_update = false
					location.reload(true);
				})
				.catch((error) => {

					if (error.response.data.error) {
						this.evolucion_update_errors = error.response.data.error
					} else {
						this.evolucion_update_errors =  { message: 'Error al procesar. Vuelve a probar por favor.' };
					}
					this.evolucion.upload.loading_update = false
				})
		    },

		    /* MAPA */

		    // Inicia el mapa localizado en sus sitio
			initMap () {
				
				// this.showMap = true
				coordinates =  new google.maps.LatLng(parseFloat(this.property.lat), parseFloat(this.property.lng));
				
			  	map = new google.maps.Map(document.getElementById('mapa'), {
				    zoom: 16,
				    center: coordinates,
				    scrollwheel: false,
			  	});

			  	window.map = map;

				marker = new google.maps.Marker({
					icon: '/dashboard_theme/assets/img/house-marker.png',
					position: coordinates,
					map: map,
					draggable: true
				});

				marker.addListener('drag', this.markerMoved);
    			marker.addListener('dragend', this.markerMoved);

    			

				// infowindow = new google.maps.InfoWindow();
				// service = new google.maps.places.PlacesService(map);
		    },

		    searchPlace() {
		    	let self = this;
		    	this.searchError = false;
		    	geocoder.geocode(
			        {'address': self.property.direccion}, 
			        function(results, status) { 
			            if (status == google.maps.GeocoderStatus.OK) { 
			                var loc = results[0].geometry.location;
			                marker.setPosition(loc);
			                map.panTo(loc);
			                map.setZoom(16);
			                self.property.lat = parseFloat(loc.lat())
		    				self.property.lng = parseFloat(loc.lng())
			            } 
			            else {
			                self.searchError = true;
			            } 
			        }
			    );
		    },

		    markerMoved(event) {
		    	this.property.lat = parseFloat(event.latLng.lat())
		    	this.property.lng = parseFloat(event.latLng.lng())
		    },

		    /* Photos  */
		    showSuccess: function (file, response) {
		        this.photos.push(response.data)

		        let locale = response.data.custom_properties.locale;

		        this.$refs.dropzoneEl.removeFile(file)
		        Vue.redrawVueMasonry()
	      	},
		    
		    removeFilePhoto: function (file) {
		    	this.$refs.dropzoneEl.removeFile(file)
		    },

		    /* Documentos publicos */
		    showSuccessDocumentPublic: function (file, response) {
		    	let locale = response.data.locale;

		    	if (this.property.documents[locale].public === null) {
		    		this.property.documents[locale].public = []
		    	}

		        this.property.documents[locale].public.push(response.data)

		        this.$refs['dropzoneDocPublic_'+locale].removeFile(file)
	      	},

		    removeFileDocumentPublic: function (file, message, xhr) {
		    	let locale = message.error.code
		    	this.$refs['dropzoneDocPublic_'+locale].removeFile(file)
		    	this.$refs['dropzoneDocPublic_'+locale].removeAllFiles();
		    },

			/* Documentos privados */
		    showSuccessDocumentPrivate: function (file, response) {

		    	let locale = response.data.locale;

		    	if (this.property.documents[locale].private === null) {
		    		this.property.documents[locale].private = []
		    	}

		    	this.property.documents[locale].private.push(response.data)
		        this.$refs['dropzoneDocPrivate_'+locale].removeFile(file)
	      	},

		    removeFileDocumentPrivate: function (file, message, xhr) {
		    	let locale = message.error.code
		    	this.$refs['dropzoneDocPrivate_'+locale].removeFile(file)
		    	this.$refs['dropzoneDocPrivate_'+locale].removeAllFiles();
		    },

		    /* Estado methods */

		    checkEstadoDisabled: function(data) {
	    		if (this.estado_inmueble_original == 1) {
		    		if (data == 3 || data == 4 || data == 5) {
		    			return true;
		    		}
		    		return false;
		    	}

		    	if (this.estado_inmueble_original == 2) {
		    		if (data == 1 || data == 5) {
		    			return true;
		    		}
		    		return false;
		    	}

		    	if (this.estado_inmueble_original == 3) {
		    		if (data == 4 || data == 6) {
		    			return false;
		    		}
		    		return true;
		    	}

		    	if (this.estado_inmueble_original == 4) {
		    		if (data == 5 || data == 6) {
		    			return false;
		    		}
		    		return true;
		    	}
		    	
		    	if (this.estado_inmueble_original == 5 || this.estado_inmueble_original == 6) {
		    		return true;
		    	}

		    },

		    currentEstado: function(){
		    	if (this.property.state) {
		    		
		    		if (this.estado_inmueble_original == 2 && this.property.state.id == 3) {
			    		return this.property.state.id
			    	}

			    	if (this.estado_inmueble_original == 2 && this.property.state.id == 4) {
			    		return this.property.state.id
			    	}

			    	if (this.estado_inmueble_original == 3 && this.property.state.id == 4) {
			    		return 41;
			    	}

			    	return this.property.state.id
		    	}
		    	
		    	return false
		    },

		    checkEstado: function(event) {
		    	this.estado_errors = {}
		    },

		    confirmData: function(){

		    	if (this.property.state.id == this.estado_inmueble_original) {
		    		this.estado_errors =  { message: 'Debes elegir un estado diferente al actual' };
		    		return false;
		    	}

		    	// if ( (this.estado_inmueble_original == 2 && this.property.state.id == 3) || (this.estado_inmueble_original == 2 && this.property.state.id == 4) ) {

		    	// 	let inversionMinima = this.property.importe_minimo;
		    	// 	let diferencia = (this.property.objetivo - this.property.walletBalance);
		    	// 	console.log(inversionMinima)
		    	// 	console.log(diferencia)
		    	// 	if (inversionMinima > diferencia && diferencia != 0) {

		    	// 		this.estado_diferencia = parseFloat(diferencia).toFixed(2)
		    	// 		$("#modalAddMoney").modal('show');
			    // 		return false;
		    	// 	}

		    	// 	let porcentajeCompletado = Math.round((this.property.walletBalance * 100) / this.property.objetivo);
		    	// 	if (this.property.walletBalance < this.property.objetivo) {
			    // 		this.estado_errors =  { message: 'Aún no se ha completado el objetivo. Actualmente todas la inversiones suman el ' +  porcentajeCompletado + ' %' };
			    // 		return false;
			    // 	}
		    	// }

		    	$("#modalConfirm").modal('show')
		    },

		    cambiarEstado: function() {
		    	console.log("Cambiar estado!")
		    	$("#modalConfirm").modal('hide')
		    	$("#update-estado-form").submit()
		    },

		    cancelCambio: function(){
		    	this.property.state.id = this.estado_inmueble_original
		    },

		    pagarDiferencia: function(){
		    	this.pagar_diferencia.loading = true

				window.axios.post(payDiferenceRoute)
				.then((response) => {
					console.log(response);
					this.pagar_diferencia.loading = false
					$("#modalAddMoney").modal('hide')
		    		$("#update-estado-form").submit()
				})
				.catch((error) => {

					if (error.response.data.error) {
						this.pagar_diferencia.errors = error.response.data.error
					} else {
						this.pagar_diferencia.errors =  { message: 'Error al procesar. Vuelve a probar por favor.' };
					}
					this.pagar_diferencia.loading = false
				})
		    },

		    addDocumentsData: function (iso) {
		    	let documentIso = {
					dictDefaultMessage: '<i class="fa fa-cloud-upload pt10"></i><span class="main-text"><b>Arrastra archivos pdf</b> para subirlos</span><br> <span class="sub-text">(o haz click)</span> ',
				    acceptedFiles: '.pdf',
				    headers: {'X-CSRF-TOKEN': window.Laravel.csrfToken, 'Locale': iso},
				};

				this.documentUploadOptions[iso] = documentIso;
		    },

		    refreshMasonry() {
		    	this.$redrawVueMasonry()
		    	Vue.redrawVueMasonry()
		    }
		},

		created () {

			_.each(locales, (iso) => this.addDocumentsData(iso));
			this.getPropertyData();
			this.getInversoresProperty();
			// createRedactor();
			
		},
		
		mounted () {
			
			$('#modalResumen').on('hidden.bs.modal', function (e) {
			  	$("#resumen").replaceWith($("#resumen").val('').clone(true));
				$("#upload-resumen-file").val('');
			});


      		this.evolucion.years.options = Array.from({length: 10}, (value, index) => (currentYear -3) + index)

			// $('#create-property').parsley().on('field:validate', function() {
			//     var elem = this.$element;

			//     // If the field is not visible, do not apply Parsley validation!
			//     if ( !$( elem ).is( ':visible' ) ) {
			//         this.constraints = [];
			//     }
			// });

			// setTimeout( () => {
			// 	this.initMap();
			// },1000)
		},

		computed: {

			hasResumenUpdateErrors: function() {
				return Object.keys(this.resumen_update_errors).length;
			},

			hasEstimacionUpdateErrors: function() {
				return Object.keys(this.evolucion_update_errors).length;
			},

			hasEvolucionUpdateErrors: function() {
				return Object.keys(this.evolucion_update_errors).length;
			},

			isPremiumZone: function()
			{
				return (this.property.zona_premium) ? 1 : 0;
			},

			isPrivate: function()
			{
				return (this.property.private) ? 1 : 0;
			},

			invertidoOrFake() {
				return parseInt(this.property.fakeOrRealInvertido).toLocaleString('es-ES', {
	    			style: 'currency', 
				    currency: 'EUR', 
				    minimumFractionDigits: 0,
				    currencyDisplay: "symbol"
				});
			},

			moneyFromWallet() {
				return parseInt(this.property.walletBalance).toLocaleString('es-ES', {
	    			style: 'currency', 
				    currency: 'EUR', 
				    minimumFractionDigits: 0,
				    currencyDisplay: "symbol"
				});
			},

			details() {
				if (this.estado_inmueble_original !== null) {
					console.log('pasa');
					console.log(this.property.details);
					let details = _.keyBy(this.property.details, 'locale');
					this.propertyDetails = details;
					return details;
				}
				
				return null;
			},

		}
	})

 	function reloadVueMasonry() {
		console.log('pollos');
		Vue.redrawVueMasonry();
	}


