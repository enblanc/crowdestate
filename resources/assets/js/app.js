export const header = new Vue({

	// =========================== #Punto de entrada======================= //
	
	el: '#header',



	// =========================== #Componentes =========================== //
	
	// components: {
	// 	'card': card
	// },



	// =========================== #Datos ================================= //
	
	data: {
		productDropdown: false,
		contactDropdown: false,
		searchForm: false,
		searchParam: '',
		searchUrl: searchUrl  //Está definido en el blade del footer
	},



	// =========================== #Computed properties==================== //
	
	// computed: {
		
	// },


	// =========================== #Methods =============================== //
	
	methods: {

		search () {
			if ( this.searchForm == false) {
				this.searchForm = !this.searchForm;
			} else {
				if ( this.searchParam.trim() != '') {

					let busqueda = this.searchParam.toLowerCase().replace(/ /g, '-');

					window.location.href = this.searchUrl + "?term=" + busqueda;
				}
			}

		},

		dropdownChange (product, contact) {
			this.productDropdown = product;
			this.contactDropdown = contact;
		}

	},


});




