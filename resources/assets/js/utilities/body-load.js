

$(document).ready(function() {

	setTimeout(function() {
		$("body").addClass("body--loaded");
	}, 500)

});


$("body").on('click', '.js-page-transition', function (e) {
	e.preventDefault();

	let url = $(this).attr('href');

	$("body").addClass("body--leave");

	setTimeout(function () {
		window.location.href = url;
	},300)
})