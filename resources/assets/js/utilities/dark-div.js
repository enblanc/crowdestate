$(document).ready(function() {


	function darkDiv () {
		let $div = $(".js-dark");

		$div.each(function () {
			let $this = $(this),
				$thisHeight = $this.outerHeight(),
				$thisOffset = $this.offset(),
				$thisOffsetTop = $thisOffset.top,
				$thisOffsetBottom = $thisOffsetTop + $thisHeight,
				scrollTop = $(window).scrollTop();

			if ( scrollTop + 50 >= $thisOffsetTop  &&  scrollTop + 50 <= $thisOffsetBottom ) {
				$("#header").addClass('header--over-dark');
			} else {
				$("#header").removeClass('header--over-dark');
			}

		})
	}

	darkDiv();



		$(window).scroll(function(e) {


			darkDiv()

		})






	/*	==========================================================================
		#ON RESIZE
		========================================================================== */

		$(window).resize(function() {

			darkDiv()

		});
});