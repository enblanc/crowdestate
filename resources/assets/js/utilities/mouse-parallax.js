$(document).ready(function() {

	/*	==========================================================================
		#VARIABLES
		========================================================================== */

		// Calculamos las variables generales
		let scrollTop = $(window).scrollTop();
		let $windowWidth = $(window).width();


		// Variables referentes al mouse parallax
		let request = null,
			mouse = {
				x: 0,
				y: 0
			},
			cx = window.innerWidth / 2,
			cy = window.innerHeight / 2,
			that = null;





	
	/*	==========================================================================
		#FUNCIONES
		========================================================================== */
		
		// Actualización de la animación del parallax del ratón
		function update() {

			// Offset del div
			let divOffset = $(that).offset();

			let dcx = $(that).width() / 2,
				dcy = $(that).height() / 2,

				virtualMouseX = mouse.x - divOffset.left,
				virtualMouseY = mouse.y - divOffset.top,

				dx = virtualMouseX - dcx*2,
				dy = virtualMouseY - dcy,

				tiltx = (dy / cy),
				tilty = -(dx / cx),

				$child = $(that).find(".mouse-parallax-anchor");

			if ( $windowWidth >= 768 ) {
				TweenLite.to($child, 1, {
					transform: 'translate3D(' + tilty * 10 + 'px, ' + tiltx * -10 + 'px, 0)',
					ease: Power2.easeOut
				});
			} else {
				TweenLite.to($child, 1, {
					transform: 'translate3D(0, 0, 0)',
					ease: Power2.easeOut
				});
			}
				

		}





		/* BRUJERÍA: Movimiento de ratón

			Capturamos la posición del raton en la pantalla. Despues ejecutamos la función de recalculado 
			de los fondos estrellados. */


		$('body').on('mousemove', '.js-mouse-parallax', function(event) {

			mouse.x = event.pageX;
			mouse.y = event.pageY;
			that = this;

			cancelAnimationFrame(request);
			request = requestAnimationFrame(update);

		});





	/*	==========================================================================
		#ON SCROLL
		========================================================================== */
		
		$(window).scroll(function(e) {

			// Calculamos las variables generales
			scrollTop = $(window).scrollTop();

		
		})






	/*	==========================================================================
		#ON RESIZE
		========================================================================== */

		$(window).resize(function() {

			// Calculamos las variables generales
			scrollTop = $(window).scrollTop();
			$windowWidth = $(window).width();

			cx = window.innerWidth / 2;
			cy = window.innerHeight / 2;


		});
});
	
	





