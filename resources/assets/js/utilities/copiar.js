function copyToClipboard(elementId) {

  // Create a "hidden" input
  var aux = document.createElement("input");

  // Assign it the value of the specified element
  aux.setAttribute("value", elementId.html());

  // Append it to the body
  document.body.appendChild(aux);

  // Highlight its content
  aux.select();

  // Copy the highlighted text
  document.execCommand("copy");

  // Remove it from the body
  document.body.removeChild(aux);

}

$('body').on('click', '.js-clipboard', function(e) {
	console.log('click');
	e.preventDefault();
	var element = $('#js-clipboard-text');
	copyToClipboard(element);

});