$('body').on('click', '.js-accordion-open', function (e) {
	
	e.preventDefault();

	let $content = $(this).next(),
		$parent  = $(this).parent();

	$content.slideToggle(200);
	$parent.toggleClass('accordion--is-open');

})