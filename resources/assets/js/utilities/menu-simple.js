$(document).ready(function() {

	/*	==========================================================================
		#VARIABLES
		========================================================================== */

		// Calculamos las variables generales
		let scrollTop = $(window).scrollTop();






	/*	==========================================================================
		#FUNCTIONS
		========================================================================== */

		function menuReveal() {
			if ( scrollTop >= 100 ) {
				$("#header").addClass("header--changed header--final");
				$('.breadcrumb').addClass('breadcrumb--is-visible');
			}else {
				$("#header").removeClass("header--changed header--final");
				$('.breadcrumb').removeClass('breadcrumb--is-visible');
			}
		}







	/*	==========================================================================
		#ACTION
		========================================================================== */

		menuReveal();

		$("body").on('click', '.js-menu', function (e) {

			$(".js-menu-anchor").toggleClass('modal--is-open');
			$('html').toggleClass('general--modal-is-open');

		});






	/*	==========================================================================
		#ON SCROLL
		========================================================================== */
		
		$(window).scroll(function(e) {

			// Calculamos las variables generales
			scrollTop = $(window).scrollTop();

			menuReveal()

		})






	/*	==========================================================================
		#ON RESIZE
		========================================================================== */

		$(window).resize(function() {

			scrollTop = $(window).scrollTop();

			menuReveal()

		});
});
	
	





