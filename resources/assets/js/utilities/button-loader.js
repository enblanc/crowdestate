import { strings } from '../utilities/translations';

$(document).on('submit', '.js-loading', function(e) {
	// e.preventDefault();
	// alert("potato");
	let $button = $(this).find('.button').not('.no-loading');
	let customClases = $button.attr('class');
	let customWidth = $button.outerWidth();
	let newItem = `<div class="${customClases} button--loader  text-center" style="min-width: ${customWidth}px;">${strings.get('general.enviando')}</div>`;
	// console.log("mec");
	$button.addClass('hide').after(newItem);

	// $(this).submit();

	// return false;
})