import lscache from 'lscache'
import Lang from 'lang.js'
import { messages }  from '../messages.js'

var strings = new Lang({
	messages: messages,
	locale: localeIso,
	fallback: 'en'
})

export { strings }