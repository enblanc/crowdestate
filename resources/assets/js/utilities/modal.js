$("body").on('click', '.js-modal', function (e) {
	e.stopPropagation();
	
	let ref = $(this).attr("modal-ref");

	$('.modal').removeClass('modal--is-open');
	$('.modal').removeClass('sub-modal--is-open');

	$("#" + ref).addClass('modal--is-open');
	$('html').addClass('modal-is-open');

}).on('click', '.not-modal', function (e) {
	e.stopPropagation();
});

$("body").on('click', '.js-modal-close', function (e) {
 
	$(this).parents('.modal').removeClass('modal--is-open');
	$('html').removeClass('modal-is-open');

	$(this).parents('.modal').removeClass('sub-modal--is-open');
	$(".modal__container--child").removeClass('submodal--is-open')

});



$("body").on('click', '.js-sub-modal', function (e) {
	e.preventDefault;

	let ref = $(this).attr("submodal-ref");

	$(this).parents('.modal').addClass('sub-modal--is-open');
	$("#" + ref).addClass('submodal--is-open');

});

$("body").on('click', '.js-modal-turn', function (e) {

	$(this).parents('.modal').removeClass('sub-modal--is-open');
	$(".modal__container--child").removeClass('submodal--is-open')

});



$("body").on('click', '.modal--popup', function (e) {
	
	$(this).removeClass('modal--is-open');
	$('html').removeClass('modal-is-open');

}).on('click', '.modal__popup', function (e) {
	
	e.stopPropagation();

});





