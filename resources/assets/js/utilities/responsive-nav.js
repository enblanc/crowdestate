$(document).ready(function() {

	/*	==========================================================================
		#VARIABLES
		========================================================================== */

		// Calculamos las variables generales
		let windowWidth = $(window).width();
		let scrollTop = $(window).scrollTop();
		let oldScroll = 0;
		let offset = 50;






	/*	==========================================================================
		#FUNCTIONS
		========================================================================== */

		function fixedNav () {
			let actualScroll = $(window).scrollTop();

			if ( windowWidth >= 1023 || actualScroll == 0 ) {

				$("#header").removeClass('main-header--is-closed main-header--is-responsive');

			} else if ( actualScroll > oldScroll + offset ) {

				$("#header").addClass('main-header--is-closed');
				
				setTimeout(function() {
					$("#header").addClass('main-header--is-responsive');
				}, 300);

				oldScroll = actualScroll;

			} else if ( actualScroll + offset < oldScroll  ) {

				$("#header").removeClass('main-header--is-closed');
				oldScroll = actualScroll;

			}

			// console.log( Math.abs(actualScroll - oldScroll) );
			// if ( Math.abs(actualScroll - oldScroll) > 50 ) {
			// 	console.log( "meh");
			// 	oldScroll = actualScroll;				
			// }
		}


		fixedNav();




	/*	==========================================================================
		#ON SCROLL
		========================================================================== */
		
		$(window).scroll(function(e) {

			// Calculamos las variables generales
			scrollTop = $(window).scrollTop();

			fixedNav();

		})






	/*	==========================================================================
		#ON RESIZE
		========================================================================== */

		$(window).resize(function() {

			windowWidth = $(window).width();
			scrollTop = $(window).scrollTop();

		});
});
	
	





