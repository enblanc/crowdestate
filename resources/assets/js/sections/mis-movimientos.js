 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/

	import '../bootstrap';

	import { strings } from '../utilities/translations';
	
	import '../utilities/body-load';
	import '../utilities/button-loader';
	import '../utilities/menu';
	
	import ToggleButton from 'vue-js-toggle-button'
	Vue.use(ToggleButton)

	// import the styles
	import 'vue-good-table/dist/vue-good-table.css'
	import { VueGoodTable } from 'vue-good-table'

	import JsonCSV from 'vue-json-csv'

	import Vue2Filters from 'vue2-filters'



var charts = new Vue({
	el: '#movements',

	components: {
		'vue-good-table': VueGoodTable,
		'download-csv': JsonCSV
	},

	data: {
		loaded: false,
		api: api,
		movements: [],
		errors: [],
		fechaFormato: fechaFormato,
		strings: strings,
		fieldsExport: ['id', 'type', 'money', 'date', 'status'],
		exporting: false
	},

	methods: {

		getTransactions() {

			window.axios.get(this.api)
				.then((response) => {
					this.movements = response.data.data
					this.loaded = true
				})
				.catch((error) => {
					if (typeof error.response.data === 'object') {
						this.errors = _.flatten(_.toArray(error.response.data));
					} else {
						this.errors = [strings.get('errors.error_general')];
					}
				})
		},

		exportingData() {
			console.log('exporyandp')
			this.exporting = true
		},

		dataExported() {
			console.log('cancelado')
			this.exporting = false
		},

		formatCurrency(value) {
			if (!value) {
				value = 0;
			}
			return this.$options.filters.currency(value, '€', 2, {
				symbolOnLeft: false,
				thousandsSeparator: '.',
				decimalSeparator: ',' 
			}) 
		},
	},

	computed: {
		columns() {
			return [
				{
					label: strings.get('general.tipo'),
					field: 'type',
					html: true,
					sortable: false
				},
				{
					label: strings.get('general.cantidad'),
					field: 'money',
					type: 'decimal'
				},
				{
					label: strings.get('general.fecha'),
					field: 'date',
					type: 'date',
					dateInputFormat: 'yyyy-MM-dd HH:mm:ss',
	    			dateOutputFormat: this.fechaFormato,
					sortable: true,
				},
				{
					label: strings.get('general.estado'),
					field: 'status',
				},
			]
		}
	},

	watch: {

		//

	},

	mounted() {
		this.getTransactions()
	},

	mixins: [Vue2Filters.mixin],
	
});