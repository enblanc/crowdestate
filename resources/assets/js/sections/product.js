import '../bootstrap';
import { strings } from '../utilities/translations';

// import '../app';
import '../utilities/events';
// import '../utilities/modal';

// import card from '../components/card.vue';

import Velocity from 'velocity-animate';
import infiniteScroll from 'vue-infinite-scroll';


  
const app = new Vue({

	// =========================== #Punto de entrada======================= //
	
	el: '#product-grid',



	// =========================== #Directivas ============================ //
	directives: {infiniteScroll},



	// =========================== #Datos ================================= //
	
	data: {
		apiUrl: apiUrl,		  // Se define en la plantilla html
		products: [],
		categories: [],
		numberItems: 0,
		searchString: "",
		category: '',
		busy: false,
	},



	// =========================== #Computed properties==================== //
	
	computed: {

		// Cálculo de los productos que deben mostartse en el listado
		// afecta: paginación y Filtros
		pagedProducts () {

			let count = 0;
			this.searchString = this.searchString.trim().toLowerCase();

			return this.products.filter((product, index) => {

				// Si no esta filtrado
				if (this.category == '' ) {
					let condition = count < this.numberItems && (product.title.toLowerCase().indexOf(this.searchString) !== -1);
					condition ? count++ : '';
					return condition;

				// Si filtrado solo por tipología
				} else {
					let condition = count < this.numberItems && this.arrayContains(product.categories, this.category) && (product.title.toLowerCase().indexOf(this.searchString) !== -1);
					condition ? count++ : '';
					return condition;
				}
				
			});
		},
		
	},


	// =========================== #Methods =============================== //
	
	methods: {

		// Cambia la tipología o deselecciona si fuese necesario
		changeCat (type = "") {
			this.numberItems = 12;
			this.category == type ? this.category = '' : this.category = type;
		},



		// Carga más elementos en el listado
		loadMore () {
			this.busy = true;
			
			if ( this.numberItems < this.products.length || this.products.length == 0  ) {
				this.numberItems += 12;
			}
	        
			setTimeout(() => {
		        this.busy = false;
		      }, 2000);		
			
		},


		// Animaciones

		beforeEnter: function (el) {
			el.style.opacity = 0
		},

		enter: function (el, done) {
			var delay = ( Number(el.dataset.index) + 12 - this.numberItems) * 150
			setTimeout(function () {
				Velocity(
					el,
					{ opacity: 1 },
					{ complete: done }
				)
			}, delay)
		},

		leave: function (el, done) {
			// el.style.opacity = 0;
			// Velocity(el, {
			// 	opacity: 0
			// }, { complete: function () { el.remove() } })
			el.remove()
		},

		arrayContains: function(data, needle) {
		   	let result = $.grep(data, function(e){ return e.id == needle; });
		   	if (result.length == 0) {
			  	return false;
			} else {
				return true;
			}
		}
	},




	// =========================== #On mounted ============================ //

	created () {
		let self = this;
		axios.get(this.apiUrl).then(function(response){
			self.products = response.data.data;
			self.categories = response.data.categories;
		});
	},
	

	mounted () {
		
	}

});



