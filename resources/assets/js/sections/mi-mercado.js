 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/



	import '../bootstrap';
	
	import { strings } from '../utilities/translations';
	
	import '../utilities/body-load';
	import '../utilities/button-loader';
	import '../utilities/menu';

	import parsley from 'parsleyjs';
	import 'parsleyjs/dist/i18n/es.js';
	import 'parsleyjs/dist/i18n/en.js';
	import 'parsleyjs/dist/i18n/de.js';

	import Vue2Filters from 'vue2-filters'

	import VueSlider from 'vue-slider-component'
	import 'vue-slider-component/theme/default.css'

	import ToggleButton from 'vue-js-toggle-button'
	Vue.use(ToggleButton)
 
	
var charts = new Vue({
	el: '#mi-mercado',

	components: {
		'VueSlider': VueSlider,
	},

	data: {
		formAction: '',
		actions: {
			create: formCreate,
			update: formUpdate,
		},
		apiAll: apiAll,
		api: apiUrl,
		apiProperties: apiUrlProperties,
		apiGet: apiGet,
		inversions: {},
		locale: localeIso,
		inversionTotal: 0,
		inversionWithOrder: false,
		inversionWithoutMoney: false,
		blockEdit: false,
		form: {
			id: '',
			hashid: '',
			type: '',
			property: '',
			quantity: 0,
			price: 0,
			pricePercent: 0,
			status: true,
		},
		range: {
			percent: 0,
			percentSold: 0,
			percentBuy: 0,
			tipFormatter: '{value} €',
			marks: {},
			marksSoldAndBuy: {
				'-15': {label: '-15%'},
				'-10': {label: '-10%'},
				'-5': {label: '-5%'},
				'0': {label: '0%'},
				'5': {label: '5%'},
				'10': {label: '10%'},
				'15': {label: '15%'},
			},
			process2: function(dotsPos) {
				return [[dotsPos[2], dotsPos[4]]];
			},
			process1: dotsPos => [[50, dotsPos[0]]],
		},
		errors: [],
		money: {
			decimal: ',',
			thousands: '',
			prefix: '',
			suffix: ' €',
			precision: 2,
        },
        deleteForm: {
        	id: null,
        	type: null,
        	property: null,
        	hashid: null,
        	quantity: null,
        	price: null,
        },
        modalDeleteOrder: false,
        addOrderButton: false,
	},

	methods: {


		getAllInversions() {
			window.axios.post(this.apiAll)
				.then((response) => {
					let inversions = response.data.data

					if (inversions) {
						this.inversions = {};

						inversions.forEach(inversion => {

							if (!_.has(this.inversions, inversion.type)) {
								this.inversions[inversion.type] = [];
							}

							if (inversion.type == 1) {
								if (!_.has(this.inversions[inversion.type], inversion.inversion.id)) {
									this.inversions[inversion.type][inversion.inversion.id] = []
								}

								if (this.inversions[inversion.type][inversion.property.id] == undefined) {
									this.inversions[inversion.type][inversion.property.id] = []
								}

								this.inversions[inversion.type][inversion.inversion.id].push(inversion)	
							}
							
							if (inversion.type == 2) {
								if (!_.has(this.inversions[inversion.type], inversion.property.id)) {
									this.inversions[inversion.type][inversion.property.id] = []
								}

								if (this.inversions[inversion.type][inversion.property.id] == undefined) {
									this.inversions[inversion.type][inversion.property.id] = []
								}

								this.inversions[inversion.type][inversion.property.id].push(inversion)	
							}

						});
					}
				})
		},
		
		getInversion() {
			this.inversionTotal = 0
			this.form.percent = 0
			this.range.percent = 0
			this.inversionWithOrder = false
			this.inversionWithoutMoney = false
			this.addOrderButton = false

			window.axios.post(this.api, {inversion: this.form.property})
				.then((response) => {
					if (response.data.data) {

						let data = response.data.data;
						let total = parseFloat(data.total);

						if (_.get(this.inversions[1], data.id)) {
							let inversions = _.get(this.inversions[1], data.id);

							let inversionMade = 0;
							inversions.forEach( (item) =>  {
								inversionMade += item.quantity
							})
							this.inversionWithOrder = true
							this.inversionTotal = total - inversionMade;

						} else {
							this.inversionTotal = total;
						}

						if (this.inversionTotal == 0) {
							this.inversionWithoutMoney = true
						}

						this.inversionTotal = Number(parseFloat(this.inversionTotal).toFixed(2));

						this.setMarksFromTotal(this.inversionTotal);
					}
				})
				.catch((error) => {

					// if (typeof error.response.data === 'object') {
					// 	this.errors = _.flatten(_.toArray(error.response.data));
					// } else {
					// 	this.errors = [strings.errors.error_general];
					// }
				});

		},


		editOrder(orderId) {
			window.axios.get(this.apiGet, {
					params: {
						id: orderId
					}
				})
				.then((response) => {
					if (response.data.data) {
						let order = response.data.data
						
						this.form.id = order.id
						this.form.hashid = order.hashid
						this.form.type = order.type
						this.form.price = order.price
						this.form.quantity = order.quantity
						this.form.status = order.status
						this.range.percent = order.quantity

						if (order.type == 1) {
							this.form.property = order.inversion.id
							this.range.percentSold = order.price

							let total = parseFloat(order.inversion.total)

							if (_.has(this.inversions[order.type], order.inversion.id)) {
								let inversions = _.get(this.inversions[order.type], order.inversion.id);
								let inversionMade = 0;
								inversions.forEach( (item) =>  {
									if (item.id != order.id) {
										inversionMade += item.quantity	
									}
								})

								if (inversionMade > 0) {
									this.inversionWithOrder = true	
								}

								this.inversionTotal = total - inversionMade;

							} else {
								this.inversionTotal = total;
							}
						}

						if (order.type == 2) {
							this.form.property = order.property.id
							this.range.percentBuy = order.price

							let total = parseFloat(order.priceMoney);


							if (_.has(this.inversions[order.type], order.property.id)) {
								let inversions = _.get(this.inversions[order.type], order.property.id);

								this.inversionWithOrder = true
								
								this.inversionTotal = total;

							} else {
								this.inversionTotal = total;
							}

						}

						this.setMarksFromTotal(this.inversionTotal);

						this.blockEdit = true
						this.formAction = this.actions.update
					}
				})
				.catch((error) => {
					if (typeof error.response.data === 'object') {
						this.errors = _.flatten(_.toArray(error.response.data));
					} else {
						this.errors = [strings.get('errors.error_general')];
					}
				});

		},


		getPropertyToBuy() {
			this.inversionTotal = 0;
			this.form.percent = 0;
			this.range.percent = 0;
			this.inversionWithOrder = false;
			this.addOrderButton = false

			let route = this.apiProperties + '/' + this.form.property;


			window.axios.get(route)
				.then((response) => {
					if (response.data.data) {

						let data = response.data.data;

						return false;

						let total = parseFloat(data.total);

						if (_.has(this.inversions, data.id)) {
							let inversions = _.get(this.inversions, data.id);

							let inversionMade = 0;
							inversions.forEach( (item) =>  {
								inversionMade += item.quantity
							})

							this.inversionWithOrder = true
							this.inversionTotal = total - inversionMade;

						} else {this.inversionTotal = total;
							this.inversionTotal = total;
						}

						this.inversionTotal = parseFloat(this.inversionTotal).toFixed(2);

						this.setMarksFromTotal(this.inversionTotal);
					}
				})
				.catch((error) => {
					if (typeof error.response.data === 'object') {
						this.errors = _.flatten(_.toArray(error.response.data));
					} else {
						this.errors = [strings.get('errors.error_general')];
					}
				});
		},

		setMarksFromTotal(total) {
			this.range.marks = {}

			if (total < 5) {
				this.range.marks[0] = { label: '0%'}
				this.range.marks[total] = {label: '100%'}
			} else {
				this.range.marks[0] = { label: '0%'}
				this.range.marks[total / 4] = {label: '25%'}
				this.range.marks[total / 2] = {label: '50%'}
				this.range.marks[(75 * total) / 100] = {label: '75%'}
				this.range.marks[total] = {label: '100%'}
			}

			
		},

		cancelEdit()
		{
			this.form.id = ''
			this.form.hashid = ''
			this.form.type = ''
			this.form.quantity = 0
			this.form.price = 0
			this.form.pricePercent = 0
			this.form.status = true
			this.form.property = ''

			this.range.percent = 0
			this.range.percentSold = 0
			this.range.percentBuy = 0
			this.inversionTotal = 0
			this.range.marks = {}
			this.addOrderButton = false
				
			this.inversionWithOrder = false
			this.inversionWithoutMoney = false
			this.blockEdit = false
			this.formAction = this.actions.create
		},

		resetForm() {
			this.form.id = ''
			this.form.hashid = ''
			this.form.quantity = 0
			this.form.price = 0
			this.form.pricePercent = 0
			this.form.status = true
			this.form.property = ''

			this.range.percent = 0
			this.range.percentSold = 0
			this.range.percentBuy = 0
			this.inversionTotal = 0
			this.range.marks = {}
			this.addOrderButton = false

			this.inversionWithOrder = false
			this.inversionWithoutMoney = false
			this.blockEdit = false
			this.formAction = this.actions.create
		},

		removeOrder(current_order = null) {
			let order;
			if (current_order) {
				order = _.find(_.flatMap(this.inversions[current_order.type]), {id: current_order.id });
			} else {
				order = _.find(_.flatMap(this.inversions[this.form.type]), {id: this.form.id });
			}

			this.deleteForm = {
	        	id: order.id,
	        	type: order.type,
	        	property: order.property.nombre,
	        	hashid: order.hashid,
	        	quantity: order.quantity,
	        	quantityPercent: order.percentQuantity,
	        	pricePercent: order.price,
	        	price: order.priceMoney,
	        }

			this.modalDeleteOrder = true
		},

		cancelDelete() {
			this.deleteForm.id = null
			this.modalDeleteOrder = false
		},

		formatCurrency(value) {
			if (!value) {
				value = 0;
			}
			return this.$options.filters.currency(value, '€', 2, {
				symbolOnLeft: false,
				thousandsSeparator: '.',
				decimalSeparator: ',' 
			}) 
		},

		checkMessages() {
			if ($('.alert').length) {
				$('.alert').delay(5000).fadeOut('slow');
			}
		},

		setParsleyInfo() {
	    	Parsley.addMessages(localeIso, {
				defaultMessage: strings.get('parsley.defaultMessage'),
				type: {
					email:        strings.get('parsley.type.email'),
					url:      	  strings.get('parsley.type.url'),
					number:       strings.get('parsley.type.number'),
					integer:      strings.get('parsley.type.integer'),
					digits:       strings.get('parsley.type.digits'),
					alphanum:     strings.get('parsley.type.alphanum'),
				},
				notblank:       strings.get('parsley.notblank'),
				required:       strings.get('parsley.required'),
				pattern:        strings.get('parsley.pattern'),
				min:            strings.get('parsley.min'),
				max:            strings.get('parsley.max'),
				range:          strings.get('parsley.range'),
				minlength:      strings.get('parsley.minlength'),
				maxlength:      strings.get('parsley.maxlength'),
				length:         strings.get('parsley.length'),
				mincheck:       strings.get('parsley.mincheck'),
				maxcheck:       strings.get('parsley.maxcheck'),
				check:          strings.get('parsley.check'),
				equalto:        strings.get('parsley.equalt')
			});

			Parsley.setLocale(this.locale);
		
			var parsleyOptions = {
			    errorClass: 'has-error',
			    successClass: 'has-success',
			    classHandler: function(ParsleyField) {
			        return ParsleyField.$element.parents('.form-group');
			    },
			    errorsContainer: function(ParsleyField) {
			        return ParsleyField.$element.parents('.form-group');
			    },
			    errorsWrapper: '<span class="help-block">',
			    errorTemplate: '<div></div>'
			};
			$('#new-order').parsley( parsleyOptions );
	    },

	},

	computed: {
		
		percentOrder() {
			if (this.form.quantity) {
				return ((this.form.quantity * 100) / this.inversionTotal).toFixed(2);
			}

			return 0;
		},

		priceToBuy() {
			let percent = this.range.percentBuy
			let price = parseFloat(this.form.price);
			price += (price * percent) / 100
			if (price > 0) {
				this.addOrderButton = true
			} else {
				this.addOrderButton = false
			}
			return price;
		},

		formMethod() {
			return  (this.blockEdit) ? 'POST' : 'POST';
		}
		
	},

	watch: {

		'range.percent': function(value) {
			this.form.quantity = value

			let price = this.form.quantity + (this.form.quantity * this.range.percentSold) / 100;
			this.form.price = price.toFixed(2)

			if (price > 0) {
				this.addOrderButton = true
			} else {
				this.addOrderButton = false
			}
			
		},

		'range.percentSold': function(value) {
			let price = this.form.quantity + (this.form.quantity * value) / 100
			this.form.price = price.toFixed(2)
			this.form.pricePercent = value
		},

		'range.percentBuy': function(value) {
			this.form.pricePercent = value
		},

		'form.quantity': function(value) {
			if (this.form.type == 1) {
				return ;
			}
			// this.form.price = parseFloat(value).toFixed(2);
			this.form.price = value
		},

	},

	mounted() {
		this.formAction = this.actions.create
		this.getAllInversions()
		this.setParsleyInfo()

		this.checkMessages()
	},

	created () {
		//
	},

	mixins: [Vue2Filters.mixin],
	
});