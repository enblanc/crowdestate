 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/



	import '../bootstrap';
	// import '../app';
	
	import { strings } from '../utilities/translations';

	import '../utilities/body-load';
	import '../utilities/menu';
	import '../utilities/responsive-nav';
	import '../utilities/mouse-parallax';
	import '../utilities/body-load';
	import '../utilities/dark-div';






	var app = new Vue({
		el: '#app',

		data: {
			msg: 'Hello World! This is a Event listener test.',
			windowWidth: 0,
			windowHeight: 0,
		},

		mounted() {
			this.$nextTick(function() {
				window.addEventListener('resize', this.getWindowWidth);
				window.addEventListener('resize', this.getWindowHeight);

				//Init
				this.getWindowWidth()
				this.getWindowHeight()
			})

		},

		methods: {
			getWindowWidth(event) {
					this.windowWidth = document.documentElement.clientWidth;
				},

			getWindowHeight(event) {
				this.windowHeight = document.documentElement.clientHeight;
			}
		},

		beforeDestroy() {
			window.removeEventListener('resize', this.getWindowWidth);
			window.removeEventListener('resize', this.getWindowHeight);
		}
		
	});




// $(document).ready(function() {

// 	/*	==========================================================================
// 		#VARIABLES
// 		========================================================================== */

// 		// Calculamos las variables generales
// 		let $documentHeight = $(document).height();
// 		let $windowWidth = $(window).width();
// 		let $windowHeight = $(window).height();
// 		let scrollTop = $(window).scrollTop();
// 		let scrollBottom = $(window).scrollTop() + $(window).height();





	
// 	/*	==========================================================================
// 		#FUNCIONES
// 		========================================================================== */
		
		




// 	/*	==========================================================================
// 		#BRUJERÍA
// 		========================================================================== */






// 	/*	==========================================================================
// 		#MOUSEMOVE
// 		========================================================================== */

// 		 BRUJERÍA: Movimiento de ratón

// 			Capturamos la posición del raton en la pantalla. Despues ejecutamos la función de recalculado 
// 			de los fondos estrellados. 






// 	/*	==========================================================================
// 		#CLICKS
// 		========================================================================== */

// 		/* BRUJERÍA: Abrir y cerrar formulario

// 			Cuando clicamos en un botón hacia el formulario abrimos el mismo. Hacemos lo inverso cuando 
// 			clicamos en la X que lo cierra. */

		




// 	/*	==========================================================================
// 		#ON SCROLL
// 		========================================================================== */
		
// 		$(window).scroll(function(e) {

// 			// Calculamos las variables generales
// 			scrollTop = $(window).scrollTop();
// 			scrollBottom = $(window).scrollTop() + $(window).height();
		
// 		})






// 	/*	==========================================================================
// 		#ON RESIZE
// 		========================================================================== */

// 		$(window).resize(function() {

// 			// Calculamos las variables generales
// 			$documentHeight = $(document).height();
// 			$windowWidth = $(window).width();
// 			$windowHeight = $(window).height();
// 			scrollTop = $(window).scrollTop();

// 		});
// });
	
	





