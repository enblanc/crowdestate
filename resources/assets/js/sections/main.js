 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/



	// import '../bootstrap';
	// // import '../app';
	import { strings } from '../utilities/translations';
	import '../utilities/body-load'; 
	import '../utilities/modal';
	import '../utilities/menu';
	import '../utilities/accordion';
	import '../utilities/button-loader';
	import '../utilities/copiar';
	import './loginRegister';


$(document).ready(function() {

	/*	==========================================================================
		#VARIABLES
		========================================================================== */

		// Calculamos las variables generales
		let $documentHeight = $(document).height();
		let $windowWidth = $(window).width();
		let $windowHeight = $(window).height();
		let scrollTop = $(window).scrollTop();
		let scrollBottom = $(window).scrollTop() + $(window).height();





	
	/*	==========================================================================
		#FUNCIONES
		========================================================================== */
		
		



	/*	==========================================================================
		#BRUJERÍA
		========================================================================== */

		
		Parsley.addMessages(localeIso, {
			defaultMessage: strings.get('parsley.defaultMessage'),
			type: {
				email:        strings.get('parsley.type.email'),
				url:      	  strings.get('parsley.type.url'),
				number:       strings.get('parsley.type.number'),
				integer:      strings.get('parsley.type.integer'),
				digits:       strings.get('parsley.type.digits'),
				alphanum:     strings.get('parsley.type.alphanum'),
			},
			notblank:       strings.get('parsley.notblank'),
			required:       strings.get('parsley.required'),
			pattern:        strings.get('parsley.pattern'),
			min:            strings.get('parsley.min'),
			max:            strings.get('parsley.max'),
			range:          strings.get('parsley.range'),
			minlength:      strings.get('parsley.minlength'),
			maxlength:      strings.get('parsley.maxlength'),
			length:         strings.get('parsley.length'),
			mincheck:       strings.get('parsley.mincheck'),
			maxcheck:       strings.get('parsley.maxcheck'),
			check:          strings.get('parsley.check'),
			equalto:        strings.get('parsley.equalt')
		});

		Parsley.setLocale(localeIso);

		// window.Parsley.addValidator('contrasena', {
		//   validateString: function(value) {
		//   	re = ^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$;
		//     return re.test(value);
		//   },
		//   messages: {
		//     es: 'La contraseña debe tener mínimo 8 caracteres con números y letras.'
		//   }
		// });

		var parsleyOptionsGeneral = {
		    errorClass: 'has-error',
		    successClass: 'has-success',
		    classHandler: function(ParsleyField) {
		        return ParsleyField.$element.parents('.form-group');
		    },
		    errorsContainer: function(ParsleyField) {
		        return ParsleyField.$element.parents('.form-group');
		    },
		    errorsWrapper: '<span class="help-block">',
		    errorTemplate: '<div></div>'
		};
		
		if ( $('.validate').length > 0 ) {
			$('.validate').parsley( parsleyOptionsGeneral );
		}




		


	/*	==========================================================================
		#MOUSEMOVE
		========================================================================== */

		/* BRUJERÍA: Movimiento de ratón

			Capturamos la posición del raton en la pantalla. Despues ejecutamos la función de recalculado 
			de los fondos estrellados. */






	/*	==========================================================================
		#CLICKS
		========================================================================== */

		/* BRUJERÍA: Abrir y cerrar formulario

			Cuando clicamos en un botón hacia el formulario abrimos el mismo. Hacemos lo inverso cuando 
			clicamos en la X que lo cierra. */

		




	/*	==========================================================================
		#ON SCROLL
		========================================================================== */
		
		$(window).scroll(function(e) {

			// Calculamos las variables generales
			scrollTop = $(window).scrollTop();
			scrollBottom = $(window).scrollTop() + $(window).height();
		
		})






	/*	==========================================================================
		#ON RESIZE
		========================================================================== */

		$(window).resize(function() {

			// Calculamos las variables generales
			$documentHeight = $(document).height();
			$windowWidth = $(window).width();
			$windowHeight = $(window).height();
			scrollTop = $(window).scrollTop();

		});
});
	
	





