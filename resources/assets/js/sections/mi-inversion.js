 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/



	// import '../bootstrap';
	// // import '../app';
	import { strings } from '../utilities/translations';
	// import '../utilities/modal';
	import '../utilities/body-load';
	import '../utilities/menu';
	// import '../utilities/accordion';
	// import '../utilities/button-loader';
	import './loginRegister';
	import Chart from 'chart.js';

 


var charts = new Vue({
	el: '#inversion',
	data: {
		api: apiUrl,
		months: [
			strings.get('months.enero'),
			strings.get('months.febrero'),
			strings.get('months.marzo'),
			strings.get('months.abril'),
			strings.get('months.mayo'),
			strings.get('months.junio'),
			strings.get('months.julio'),
			strings.get('months.agosto'),
			strings.get('months.septiembre'),
			strings.get('months.octubre'),
			strings.get('months.noviembre'),
			strings.get('months.diciembre'),
		],
		meses: [],
		allMeses: [],
		datos: [],
		resultados: [],
		actualMonth: 0,
	},

	mounted() {
		
	},

	methods: {
		chartBuild () {
			let ctx = document.getElementById("myChart");
			let myChart = new Chart(ctx, {
			    type: 'bar',
			    data: {
			        labels: this.meses,
			        datasets: [{
			            data: this.datos,
			            backgroundColor: '#7b3eee',
			        }]
			    },
			    options: {
			    	legend: {
				        display: false
				    },
			        scales: {
			        	xAxes: [{
			        		barThickness : 65,
			        		// barPercentage: 1,
			        		// categoryPercentage: 1,
			                gridLines: {
			                    display:false
			                }
			            }],
			            yAxes: [{
			                ticks: {
			                    beginAtZero:true
			                },
			            }]
			        }
			    }
			});

		},

		formatNumber(num) {
			 return (Math.round((num * 1000)/10)/100).toFixed(2)
		},

		number_format (number, decimals, decPoint, thousandsSep) { // eslint-disable-line camelcase
		  //  discuss at: http://locutus.io/php/number_format/
		  // original by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		  // improved by: Kevin van Zonneveld (http://kvz.io)
		  // improved by: davook
		  // improved by: Brett Zamir (http://brett-zamir.me)
		  // improved by: Brett Zamir (http://brett-zamir.me)
		  // improved by: Theriault (https://github.com/Theriault)
		  // improved by: Kevin van Zonneveld (http://kvz.io)
		  // bugfixed by: Michael White (http://getsprink.com)
		  // bugfixed by: Benjamin Lupton
		  // bugfixed by: Allan Jensen (http://www.winternet.no)
		  // bugfixed by: Howard Yeend
		  // bugfixed by: Diogo Resende
		  // bugfixed by: Rival
		  // bugfixed by: Brett Zamir (http://brett-zamir.me)
		  //  revised by: Jonas Raoni Soares Silva (http://www.jsfromhell.com)
		  //  revised by: Luke Smith (http://lucassmith.name)
		  //    input by: Kheang Hok Chin (http://www.distantia.ca/)
		  //    input by: Jay Klehr
		  //    input by: Amir Habibi (http://www.residence-mixte.com/)
		  //    input by: Amirouche
		  //   example 1: number_format(1234.56)
		  //   returns 1: '1,235'
		  //   example 2: number_format(1234.56, 2, ',', ' ')
		  //   returns 2: '1 234,56'
		  //   example 3: number_format(1234.5678, 2, '.', '')
		  //   returns 3: '1234.57'
		  //   example 4: number_format(67, 2, ',', '.')
		  //   returns 4: '67,00'
		  //   example 5: number_format(1000)
		  //   returns 5: '1,000'
		  //   example 6: number_format(67.311, 2)
		  //   returns 6: '67.31'
		  //   example 7: number_format(1000.55, 1)
		  //   returns 7: '1,000.6'
		  //   example 8: number_format(67000, 5, ',', '.')
		  //   returns 8: '67.000,00000'
		  //   example 9: number_format(0.9, 0)
		  //   returns 9: '1'
		  //  example 10: number_format('1.20', 2)
		  //  returns 10: '1.20'
		  //  example 11: number_format('1.20', 4)
		  //  returns 11: '1.2000'
		  //  example 12: number_format('1.2000', 3)
		  //  returns 12: '1.200'
		  //  example 13: number_format('1 000,50', 2, '.', ' ')
		  //  returns 13: '100 050.00'
		  //  example 14: number_format(1e-8, 8, '.', '')
		  //  returns 14: '0.00000001'
		  number = (number + '').replace(/[^0-9+\-Ee.]/g, '')
		  var n = !isFinite(+number) ? 0 : +number
		  var prec = !isFinite(+decimals) ? 0 : Math.abs(decimals)
		  var sep = (typeof thousandsSep === 'undefined') ? ',' : thousandsSep
		  var dec = (typeof decPoint === 'undefined') ? '.' : decPoint
		  var s = ''
		  var toFixedFix = function (n, prec) {
		    var k = Math.pow(10, prec)
		    return '' + (Math.round(n * k) / k)
		      .toFixed(prec)
		  }
		  // @todo: for IE parseFloat(0.55).toFixed(0) = 0;
		  s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.')
		  if (s[0].length > 3) {
		    s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep)
		  }
		  if ((s[1] || '').length < prec) {
		    s[1] = s[1] || ''
		    s[1] += new Array(prec - s[1].length + 1).join('0')
		  }
		  return s.join(dec)
		}
	},

	computed: {

		mesVista () {
			return this.resultados[this.actualMonth];
		},

		otrasPartidas () {
			return this.mesVista.amortizacion > 0 || this.mesVista.subvenciones > 0 || this.mesVista.enajenacion_inmovilizado > 0;
		}
		
	},

	created () {
		let self = this;
		let resultadoTotal = 0;
		axios.get(this.api).then(function(response){
			
			let data = response.data.data.evolutions;
			
			data = data ? data : [];

			data.sort(function(a, b) {
			    a = new Date(a.fecha);
			    b = new Date(b.fecha);
			    return a>b ? -1 : a<b ? 1 : 0;
			});
			
			self.resultados = data;


			let maxMonths;
			if ( data.length > 6 ) {
				maxMonths = data.length - 6 + 1;

				for (var i = data.length - 1; i > data.length - maxMonths; i--) {
					resultadoTotal += data[i].resultado_explotacion;
				}

			} else {
				maxMonths = 1;
			}

			

			for (var i = data.length - maxMonths; i >= 0; i--) {
				let date = new Date(data[i].fecha);
				resultadoTotal += data[i].resultado_explotacion;

				self.meses.push( self.months[date.getMonth()] + ' ' + date.getFullYear() );
				self.datos.push( resultadoTotal );
			}

			for (var i = 0; i < data.length; i++) {
				let date = new Date(data[i].fecha);

				// let ObjCust = {
				// 	'mes': self.months[date.getMonth()] + ' ' + date.getFullYear(),
				// 	'id' : 
				// }
				self.allMeses.push( self.months[date.getMonth()] + ' ' + date.getFullYear() );
				// self.datos.push( resultadoTotal );
			}

			self.chartBuild();
		});
	},

	
});