
import '../bootstrap';
import { strings } from '../utilities/translations';
import parsley from 'parsleyjs';
// import './../utilities/parsley.js'

// import '../../../../node_modules/parsleyjs/dist/parsleyjs.js';
import 'parsleyjs/dist/i18n/es.js';
import Webcam from 'webcamjs'
// import VueRecaptcha from 'vue-recaptcha';
import errorForms from '../components/error-forms.vue'
import countries from 'i18n-iso-countries';

import { VTooltip, VPopover, VClosePopover } from 'v-tooltip'


var app = new Vue({
	el: '#app',
	components: {
		'error-form': errorForms,
		VPopover
		// VueRecaptcha,
	},
	directives: {
		'tooltip': VTooltip,
		'close-popover': VClosePopover
	},
	data: {
		accountType: (typeof tipoCuenta === 'undefined' || tipoCuenta === null) ? '': tipoCuenta,
		tipoSubida: 1,
		webcam: {
			photo: false,
			current: 'front',
			saving: false,
			front: null,
			back: null,
		},
		login: {
			email: null,
			password: null,
			loading: false,
		},
		register: {
			nombre: null,
			apellido1: null,
			apellido2: null,
			email: null,
			password: null,
			password_confirm:null,
			loading: false,
		},
		email : {
			result: false,
			loading: false,
		},
		errors: {
			login: [],
			register: [],
			email: []
		},
		sendingForm: false,
		rentabilidadAcumulada: 0,
		limiteInversion: 0,
		inversion: 0,
		bank_iban: null,
		bank_country: null,
		closeAccount:{
			password: null,
			loading: false,
			error: null
		},
		tributeOpen: false,
	},

	mounted() {
		this.rentabilidadAcumulada = $("#rentabilidad_acumulada").val()
		this.limiteInversion = $("#minimo_invertir").val()
		this.inversion = $("#defecto_invertir").val()
		this.formsValidation()
		// this.initReCaptcha()
	},

	methods: {
		initReCaptcha: function() {
	        var self = this;
	        setTimeout(function() {
	            if(typeof grecaptcha === 'undefined') {
	                self.initReCaptcha();
	            }
	            else {
	                grecaptcha.render('recaptcha', {
	                    sitekey: siteKey,
	                    size: 'invisible',
	                    badge: 'bottomright',
	                    callback: self.postRegister
	                });
	            }
	        }, 100);
	    },
		limitMin () {
			// console.log( typeof( Number(this.inversion) ), typeof( Number(this.limiteInversion) ) );
			// console.log( this.inversion, this.limiteInversion );
			// console.log( Number(this.inversion) < Number(this.limiteInversion) );
			if ( Number(this.inversion) < Number(this.limiteInversion) ) {
				this.inversion = this.limiteInversion;
			}
		},
		postLogin() {
			
			if(!this.checkLoginFields()) {
				return false
			}

			if (this.login.loading) {
				return false
			}

			this.login.loading = true;
			this.errors.login = [];
			let formData = new FormData(document.getElementById('login-form'));
			window.axios.post(routeLogin, formData)
				.then((response) => {
					this.login.loading = false;
					location.reload();
				})
				.catch((error) => {
					this.login.loading = false;
					if (error.response.data.error) {
						if (error.response.data.error.code == 'not_confirmed') {
							this.hideModals();
							$("#obtener").addClass('modal--is-open');
						}
					} else {
						// this.errors.login = errors.response.data;
						if (typeof error.response.data === 'object') {
	                        this.errors.login = _.flatten(_.toArray(error.response.data));
	                    } else {
	                        this.errors.login = [strings.get('errors.error_general')];
	                    }
                    	console.log(error);
					}
				});
		},

		postRegister() {
			console.log("funciona")
			if(!this.checkRegisterFields()) {
				return false
			}

			if (this.register.loading) {
				return false
			}

			this.register.loading = true;
			let formData = new FormData(document.getElementById('register-form'));

			console.log(formData);
			
			// formData.append('g-recaptcha-response', $("#g-recaptcha-response").val());
			window.axios.post(routeRegister, formData)
				.then((response) => {
					this.hideModals();
					$("#obtener").addClass('modal--is-open');
					this.register.loading = false;
				})
				.catch((error) => {
					this.register.loading = false;
					if (typeof error.response.data === 'object') {
                        this.errors.register = _.flatten(_.toArray(error.response.data));
                    } else {
                        this.errors.register = [strings.get('errors.error_general')];
                    }
                    console.log(error);
				});
		},

		validate: function() {
	        grecaptcha.reset();
	        grecaptcha.execute();
	    },

		postResetPassword() {

			if(!this.checkResetPasswordFields()) {
				return false
			}

			if (this.email.loading) {
				return false
			}

			this.email.loading = true;
			this.errors.email = [];
			let formData = new FormData(document.getElementById('email-form'));
			window.axios.post(routeResetEmail, formData)
				.then((response) => {
					this.email.result = true
					this.email.loading = false;
					setTimeout( () => {
						this.email.result = false
						this.hideModals();
					},3000)
				})
				.catch((errors) => {
					this.email.loading = false;
					if (errors.response.data.error) {
						console.log(errors.response.data.error);
						this.errors.email.push(errors.response.data.error.message)
					}
				});
		},

		checkLoginFields() {

			$('#login-form').parsley().validate();

            if ($('#login-form').parsley().isValid()){
                return true
            } else {
            	return false
            }
		},

		checkRegisterFields() {

			$('#register-form').parsley().validate();

            if ($('#register-form').parsley().isValid()){
                return true
            } else {
            	return false
            }
		},

		checkResetPasswordFields() {

			$('#email-form').parsley().validate();

            if ($('#email-form').parsley().isValid()){
                return true
            } else {
            	return false
            }
		},


		formsValidation() {

	    	Parsley.addMessages(localeIso, {
				defaultMessage: strings.get('parsley.defaultMessage'),
				type: {
					email:        strings.get('parsley.type.email'),
					url:      	  strings.get('parsley.type.url'),
					number:       strings.get('parsley.type.number'),
					integer:      strings.get('parsley.type.integer'),
					digits:       strings.get('parsley.type.digits'),
					alphanum:     strings.get('parsley.type.alphanum'),
				},
				notblank:       strings.get('parsley.notblank'),
				required:       strings.get('parsley.required'),
				pattern:        strings.get('parsley.pattern'),
				min:            strings.get('parsley.min'),
				max:            strings.get('parsley.max'),
				range:          strings.get('parsley.range'),
				minlength:      strings.get('parsley.minlength'),
				maxlength:      strings.get('parsley.maxlength'),
				length:         strings.get('parsley.length'),
				mincheck:       strings.get('parsley.mincheck'),
				maxcheck:       strings.get('parsley.maxcheck'),
				check:          strings.get('parsley.check'),
				equalto:        strings.get('parsley.equalt')
			});

			Parsley.setLocale(localeIso);
		
			var parsleyOptions = {
			    errorClass: 'has-error',
			    successClass: 'has-success',
			    classHandler: function(ParsleyField) {
			        return ParsleyField.$element.parents('.form-group');
			    },
			    errorsContainer: function(ParsleyField) {
			        return ParsleyField.$element.parents('.form-group');
			    },
			    errorsWrapper: '<span class="help-block">',
			    errorTemplate: '<div></div>'
			};
			$('#login-form').parsley( parsleyOptions );
			$('#register-form').parsley( parsleyOptions );
			$('#email-form').parsley( parsleyOptions );
			
	    },

		hideModals() {
			$('html').removeClass('modal-is-open');
			$('.modal').removeClass('modal--is-open');
		},
		checkCountry () {
			let pais = countries.getName(this.bank_iban.slice(0, 2), localeIso);
			this.bank_country = pais;
		},

		checkPassword(){
			this.closeAccount.loading = true;
			this.closeAccount.error = null;


			$('#borrar-cuenta').find('.help-block').not('.custom').remove();
			window.axios.post('/api/v1/usuarios/check-password', {'password': this.closeAccount.password} )
				.then((response) => {
					console.log("Exito");
					this.closeAccount.loading = false;
				})
				.catch((error) => {
					console.log("error");
					this.closeAccount.error = strings.get('errors.pasword_coincide');
					this.closeAccount.loading = false;


					// $('#password-borrar').parsley().UI.manageError({error: 'La contraseña especificada no coincide con la actual.'});

				});
		},

		openModalWebcamFront() {
			$('#webcam').addClass('modal--is-open');
			this.webcam.current = 'front'
			this.createWebcamService()
		},

		openModalWebcamBack() {
			$('#webcam').addClass('modal--is-open');
			this.webcam.current = 'back'
			this.createWebcamService()
		},

		createWebcamService(part)
		{
			this.webcam.photo = false
			Webcam.reset();
			Webcam.set({
				width: 380,
				height: 285,
				dest_width: 640,
				dest_height: 480,
				image_format: 'jpeg',
				jpeg_quality: 90,
				upload_name: this.webcam.current
			});
			setTimeout( () => {
				Webcam.attach( '#my_camera' );
			},300)
		},

		createSnapshot() {
			Webcam.snap( data_uri => {
			  this.webcam.photo = data_uri
			  // Webcam.reset();
			} );
			console.log(Webcam.params.upload_name);
			Webcam.reset();
		},

		snapshotChossen() {
			this.webcam.saving = true;
			window.axios.post('/api/v1/webcam/upload', {'photo': this.webcam.photo, type: this.webcam.current} )
				.then((response) => {
					if (this.webcam.current == 'front') {
						this.webcam.front = response.data.data
						document.getElementById("imagen_dni_front").required = false;
					}
					if (this.webcam.current == 'back') {
						this.webcam.back = response.data.data
						document.getElementById("imagen_dni_back").required = false;
					}
					this.webcam.saving = false;
					$('#webcam').removeClass('modal--is-open');
				})
				.catch((error) => {
					this.webcam.saving = false;
				});
		},

	},

	watch: {
		tipoSubida: function (val) {
			if (val == 2) {

				setTimeout( () => {
					if (this.webcam.front != null) {
						document.getElementById("imagen_dni_front").required = false;
					}
					if (this.webcam.back != null) {
						document.getElementById("imagen_dni_back").required = false;
					}
				},300)

				
				
			} 
	    },
	},

	computed: {

		beneficioTotal () {
			let beneficioAcumulado = this.inversion * this.rentabilidadAcumulada / 100,
				depaso =  Math.round( (beneficioAcumulado ) * 100 ) / 100;
			
			depaso = String(depaso).replace('.', ',');
			
			return depaso;
		}
		
	},

	
});