 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/



	// import '../bootstrap';
	import { strings } from '../utilities/translations';
	// import '../app';
	import '../utilities/menu'; 
	import '../utilities/mouse-parallax';
	import '../utilities/responsive-nav';
	import '../utilities/body-load';



$(document).ready(function() {

	/*	==========================================================================
		#VARIABLES
		========================================================================== */

		// Calculamos las variables generales
		let $documentHeight = $(document).height();
		let $windowWidth = $(window).width();
		let $windowHeight = $(window).height();
		let scrollTop = $(window).scrollTop();
		let scrollBottom = $(window).scrollTop() + $(window).height();





	
	/*	==========================================================================
		#FUNCIONES
		========================================================================== */
		
		




	/*	==========================================================================
		#BRUJERÍA
		========================================================================== */






	/*	==========================================================================
		#MOUSEMOVE
		========================================================================== */

		/* BRUJERÍA: Movimiento de ratón

			Capturamos la posición del raton en la pantalla. Despues ejecutamos la función de recalculado 
			de los fondos estrellados. */






	/*	==========================================================================
		#CLICKS
		========================================================================== */

		/* BRUJERÍA: Abrir y cerrar formulario

			Cuando clicamos en un botón hacia el formulario abrimos el mismo. Hacemos lo inverso cuando 
			clicamos en la X que lo cierra. */

		




	/*	==========================================================================
		#ON SCROLL
		========================================================================== */
		
		$(window).scroll(function(e) {

			// Calculamos las variables generales
			scrollTop = $(window).scrollTop();
			scrollBottom = $(window).scrollTop() + $(window).height();
		
		})






	/*	==========================================================================
		#ON RESIZE
		========================================================================== */

		$(window).resize(function() {

			// Calculamos las variables generales
			$documentHeight = $(document).height();
			$windowWidth = $(window).width();
			$windowHeight = $(window).height();
			scrollTop = $(window).scrollTop();

		});
});
	
	





