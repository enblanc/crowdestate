 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/








/*	==========================================================================
	#IMPORT
	========================================================================== */

	import '../bootstrap';
	import { strings } from '../utilities/translations';
	// import '../app';
	// import '../utilities/events';
	import '../utilities/body-load';
	import '../utilities/modal';
	import '../utilities/menu';


	import './loginRegister';




/*	==========================================================================
	#IMPORT
	========================================================================== */

	let map,
		map2,
		image;

	image = {
		path: "M10,4c-4.6,0-8.4,3.8-8.4,8.4C1.6,18.7,10,28,10,28s8.4-9.3,8.4-15.6C18.4,7.8,14.6,4,10,4z M10,15.4c-1.7,0-3-1.3-3-3s1.3-3,3-3s3,1.3,3,3S11.7,15.4,10,15.4z", 
		fillColor: '#36d9b6',
		fillOpacity: 1,
		origin: new google.maps.Point(-22,-22),
		anchor: new google.maps.Point(10,30),
		strokeWeight: 0,
		scale: 2
    };


	

		



       
   







	$(document).ready(function() {


		let body = document.getElementsByClassName("body");

		setTimeout(function () {
			body[0].className += ' body--mounted';
		



			



		},1000)



		$("body").on('click touchstart', '.js-tab-selector', function() {
			
			let ref = $(this).attr('data-ref');

			$(".tab-selector").removeClass('tab-selector--is-active');
			$(this).addClass('tab-selector--is-active');

			$(".tabs-selector__item").removeClass('tabs-selector__item--is-active');
			$('#' + ref).addClass('tabs-selector__item--is-active');

			$(".tab-selector__selected").html( $(this).find('.tab-selector__title').text() );
			$('.tabs-selector').removeClass('tab-selector--is-open');

			if ( $('#map2').length > 0 ) {
				loadMap2();
			}
			
		});

		$("body").on('click touchstart', '.tab-selector__active', function() {
			$('.tabs-selector').addClass('tab-selector--is-open');
		});

		


		$("body").on('click', '.js-open-info', function(e) {

			e.preventDefault();
			
			let ref = $(this).attr('data-ref');
			$('#' + ref).addClass('info__explanation--is-open');

		}).on('click', '.js-close-info', function(e) {

			e.preventDefault();
			
			$(".info__explanation").removeClass('info__explanation--is-open');

		});








		 // latlng
		let latlng = new google.maps.LatLng( $("#map").attr('data-lat'), $("#map").attr('data-lng') );

        map = new google.maps.Map(document.getElementById('map'), {
			zoom: 16,
			center: latlng,
			scrollwheel: false,
        });


        // create marker
		let markerCustom = new google.maps.Marker({
			position : latlng,
			map		 : map,
			icon	 : image,
		});

	
		
		function loadMap2() {
			 map2 = new google.maps.Map(document.getElementById('map2'), {
				zoom: 16,
				center: latlng,
				scrollwheel: false,
	        });

			let markerCustom2 = new google.maps.Marker({
				position : latlng,
				map		 : map2,
				icon	 : image,
			});
		}



	});

	  




