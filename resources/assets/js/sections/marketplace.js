 /**
	==========================================================================
	#JAVASCRIPT *
	========================================================================== 
	*
	***	CONTENTS ***
	*
	* COMPONENTS ON READY
	* 	Variables..................Variables generales.
	* 	Funciones..................Definición de funciones que se repetirán.
	* 	On load....................Elementos que se ejecutan en el load.
	* 	Brujería...................Elementos que se ejecutan en el ready.
	* 	Mousemove..................Elementos que se ejecutan cuando movemos el ratón.
	* 	clicks.....................Elementos que se ejecutan en el click.
	* 	On scroll..................Elementos que se ejecutan en el scroll.
	* 	On resize..................Elementos que se ejecutan en el reescalado.
	*
	*/

	import '../bootstrap';

	import { strings } from '../utilities/translations';
	
	import '../utilities/body-load';
	import '../utilities/button-loader';
	import '../utilities/menu';
	
	import Property from '../components/PropertyListItem'

	import ToggleButton from 'vue-js-toggle-button'
	Vue.use(ToggleButton)

	// import the styles
	import 'vue-good-table/dist/vue-good-table.css'
	import { VueGoodTable } from 'vue-good-table'

	import Vue2Filters from 'vue2-filters'

var charts = new Vue({
	el: '#market',
	components: {
		'property': Property,
		'vue-good-table': VueGoodTable
	},
	data: {
		api: api,
		orders: {},
		errors: [],
		type: '',
		propertyId: '',
		city: '',
		maxPrice: null,
		myInvested: false,
		loading: true,
		columns: [
			// {
			// 	label: strings.get('general.tipo'),
			// 	field: 'typeText'
			// },
			{
				label: strings.get('general.propiedad'),
				field: 'property.nombre'
			},
			{
				label: strings.get('general.ciudad'),
				field: 'property.ubicacion'
			},
			{
				label: strings.get('general.cantidad'),
				field: 'quantity',
				type: 'number',
				sortable: true,
			},
			{
				label: strings.get('general.precio'),
				field: 'priceMoney',
				type: 'number',
				sortable: true,
			},
	        {
				label: strings.get('general.plusvaliadescuento'),
				field: 'pricePercentOf',
				type: 'percentage',
	        },
	        {
				label: strings.get('general.tir'),
				field: 'property.tir',
				type: 'number',
	        },
			{
			    label: strings.get('general.accion'),
			    field: 'linkButton',
			    html: true,
			    sortable: false,
			}
		]
	},
	methods: {
		getMarketOrders() {
			let data = {
				type: this.type,
				property: this.propertyId,
				price: this.maxPrice,
				city: this.city,
				invested: this.myInvested
			}

			this.loading = true;

			window.axios.get(this.api, {params: data})
				.then((response) => {
					this.orders = response.data.data;
					this.loading = false;
				})
				.catch((error) => {
					if (typeof error.response.data === 'object') {
						this.errors = _.flatten(_.toArray(error.response.data));
					} else {
						this.errors = [strings.get('errors.error_general')];
					}
					this.loading = false;
				})
		},

		filterData() {
			this.getMarketOrders()
		},

		filterDataPrice: _.debounce(function (e) {
		    this.getMarketOrders()
		}, 1000),
		
		toogleInvested() {
			this.myInvested = !this.myInvested
			this.filterData()
		},

		formatCurrency(value) {
			if (!value) {
				value = 0;
			}
			return this.$options.filters.currency(value, '€', 2, {
				symbolOnLeft: false,
				thousandsSeparator: '.',
				decimalSeparator: ',' 
			}) 
		},
	},

	mounted() {
		this.getMarketOrders()
	},
	mixins: [Vue2Filters.mixin],
});