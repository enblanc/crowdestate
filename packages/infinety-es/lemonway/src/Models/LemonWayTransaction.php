<?php

namespace Infinety\LemonWay\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Infinety\LemonWay\LemonWay;

class LemonWayTransaction extends Model
{

    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {

        foreach ($this->fillableFromArray($attributes) as $key => $value) {
            $key = $this->removeTableFromKey($key);

            $key = strtolower($key);

            $this->setAttribute($key, $value);
        }

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateAttribute()
    {
        return $this->getDate()->format('d/m/Y H:i:s');
    }

    public function getDate()
    {
        return Carbon::createFromFormat('d/m/Y H:i:s', $this->attributes['date']);
    }
}
