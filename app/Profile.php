<?php

namespace App;

use App\Document;
use App\Traits\Searchable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use League\ISO3166\ISO3166;

class Profile extends Model
{
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'tipo',
        'nacionalidad',
        'ciudad',
        'dni',
        'document_id',
        'telefono',
        'direccion',
        'codigo_postal',
        'localidad',
        'pais',
        'provincia',
        'sexo',
        'profesion',
        'estado_civil',
        'acreditado',
        'estado',
        'birthdate',
        'company_cif',
        'company_rsocial',
        'company_description',
        'company_website',
        'company_country',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'acreditado' => 'boolean',
        'estado'     => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    /**
     * Set fields be able to search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return ['tipo', 'nacionalidad', 'dni', 'document_id', 'telefono', 'direccion', 'codigo_postal', 'localidad', 'pais', 'provincia', 'sexo', 'profesion', 'estado_civil', 'acreditado', 'estado'];
    }

    /**
     * @param $value
     */
    public function getTipoAttribute($value)
    {
        if ($value == 1) {
            return __('Particular');
        } elseif ($value == 2) {
            return __('Empresa');
        } else {
            return false;
        }
    }

    /**
     * @param $value
     */
    public function getSexoAttribute($value)
    {
        if ($value == 1) {
            return __('Masculino');
        } elseif ($value == 2) {
            return __('Femenino');
        } else {
            return false;
        }
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getBirthdateAttribute($value)
    {
        if ($value) {
            return $value;
        }

        return null;
    }

    /**
     * @param $value
     */
    public function getEstadoCivilTextAttribute()
    {
        switch ($this->estado_civil) {
            case 0:
                $estado = __('Sin elegir');
                break;
            case 1:
                $estado = __('Soltero/a');
                break;
            case 2:
                $estado = __('Casado/a');
                break;
            case 3:
                $estado = __('Separado/a');
                break;
            case 4:
                $estado = __('Divorciado/a');
                break;
            case 5:
                $estado = __('Viudo/a');
                break;
            default:
                $estado = __('Sin elegir');
                break;
        }

        return $estado;
    }

    /**
     * @return null
     */
    public function getCountryAlpha3Attribute()
    {
        return $this->getIso3FromAlpha2($this->pais);
    }

    /**
     * @return null
     */
    public function getCompanyCountryAlpha3Attribute()
    {
        return $this->getIso3FromAlpha2($this->company_country);
    }

    /**
     * @param $alpha2
     * @return mixed
     */
    private function getIso3FromAlpha2($alpha2)
    {
        try {
            $data = (new ISO3166())->alpha2($alpha2);

            return $data['alpha3'];
        } catch (Exception $e) {
            //Deveolvemos españa por defecto
            return 'ESP';
        }
    }

    // /**
    //  * @param $value
    //  * @return mixed
    //  */
    // public function setBirthdateAttribute($value)
    // {
    //     $this->attributes['birthdate'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    // }
}
