<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use App\AutoinvestsOrders;
use App\User;

class AutoInvest extends Model
{

    /**
     * @var string
     */
    protected $table = 'autoinvests';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'min_investment',
        'max_investment',
        'portfolio_size',
        'period',
        'country',
        'location',
        'investment',
        'developer_id',
        'property_type_id',
        'tir',
        'risk_scoring',
        'status',
    ];

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Return evolutions for current Orders
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders ()
    {
        return $this->hasMany(AutoinvestsOrders::class, 'autoinvest_id');
    }

    /**
     * Set fecha in good format
     *
     * @param string $value
     *
     * @return void
     */
    public function setPeriodAttribute($value)
    {
        $formatted = Carbon::createFromFormat('d/m/Y', $value);

        $this->attributes['period'] = $formatted;
    }

    /**
     * Obtiene los AutoInvest que tengan el mismo tipo de propiedad
     *
     * @param $query
     * @param integer $property_type_id
     * @return void
     */
    public function scopeByPropertyType ($query, $property_type_id)
    {
        return $query->where('property_type_id', $property_type_id)->orWhere('property_type_id', -1);
    }

    public function scopeByDeveloper ($query, $developer_id)
    {
        return $query->where('developer_id', $developer_id)->orWhere('developer_id', -1);
    }

    public function scopeByTir ($query, $tir)
    {
        return $query->where('tir', '>' , $tir);
    }

    public function scopeByLocation ($query, $location)
    {
        return $query->where('location', $location)->orWhere('location', 'all');
    }

    public function scopeCanInvert ($query)
    {
        return $query->whereRaw('investment < portfolio_size')
                     ->where('period', '>', date("Y-m-d"))
                     ->where('status', true);
    }
}
