<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var mixed
     */
    protected $nombre;

    /**
     * @var mixed
     */
    protected $email;

    /**
     * @var mixed
     */
    protected $telefono;

    /**
     * @var mixed
     */
    protected $comentario;


    protected $tipo;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($nombre, $email, $telefono, $comentario, $tipo = 'contacto')
    {
        $this->nombre = $nombre;
        $this->email = $email;
        $this->telefono = $telefono;
        $this->comentario = $comentario;
        $this->tipo = $tipo;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail =  $this->from(['info@brickstarter.com', 'Brickstarter'])
            ->markdown('emails.contact');

        if ($this->tipo == 'prescriptor') {
            $mail->subject('Contacto desde la página web para ser prescriptor');
        } else {
            $mail->subject('Contacto desde la página web');
        }
        
        $mail->with([
                'nombre'      => $this->nombre,
                'email'       => $this->email,
                'telefono'    => $this->telefono,
                'comentarios' => $this->comentario,
            ]);

        return $mail;
    }
}
