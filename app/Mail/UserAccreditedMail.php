<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserAccreditedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var mixed
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['info@brickstarter.com', 'Brickstarter'])
            ->markdown('emails.nuevo-acreditado')
            ->subject('Nuevo usuario acreditado!');
    }
}
