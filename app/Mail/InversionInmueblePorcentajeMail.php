<?php

namespace App\Mail;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InversionInmueblePorcentajeMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var mixed
     */
    public $property;

    /**
     * @var mixed
     */
    public $porcentaje;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Property $property, $porcentaje)
    {
        $this->property = $property;
        $this->porcentaje = $porcentaje;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(['info@brickstarter.com', 'Brickstarter'])
            ->markdown('emails.inmueble-porcentaje')
            ->subject('El inmueble está ya al '.$this->porcentaje.' %');
    }
}
