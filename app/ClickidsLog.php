<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClickidsLog extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'click_id',
        'provider',
        'postback',
        'status',
        'message'
    ];
}
