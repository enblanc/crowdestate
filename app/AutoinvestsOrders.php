<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class AutoinvestsOrders extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'autoinvest_id',
        'property_id',
        'market_order_id',
        'investment',
        'transaction_id',
        'autoinvests_event_id'
    ];

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function autoinvest()
    {
        return $this->belongsTo(\App\AutoInvest::class);
    }

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function property()
    {
        return $this->belongsTo(\App\Property::class);
    }

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function market_order()
    {
        return $this->belongsTo(\App\MarketOrder::class);
    }
}
