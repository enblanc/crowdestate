<?php

namespace App\Exceptions;

use Exception;
use HttpOz\Roles\Exceptions\GroupDeniedException;
use HttpOz\Roles\Exceptions\RoleDeniedException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        if ($exception instanceof RoleDeniedException || $exception instanceof GroupDeniedException) {
            return false;
        }
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($exception instanceof RoleDeniedException || $exception instanceof GroupDeniedException) {
            // return response()->view('vendor.roles.error', compact('exception'), 403);
            return redirect()->to('/');
        }

        if ($exception instanceof LemonWayExceptions) {
            switch ($exception->getCode()) {
                case 241:
                    session()->flash('error_lemon', __('Ya hay un documento siendo validado. Por favor, espere.'));

                    return redirect()->route('panel.user.accredit.show');
                    break;
                case 295:
                    session()->flash('error_lemon', __('Su solicitud se está atendiendo. Por favor, espere.'));

                    return redirect()->route('panel.user.accredit.show');
                    break;
                case 172:
                    session()->flash('error_lemon', __('Ha ocurrido un error con esta tarjeta y el pago no se ha podido realizar. Por favor, pruebe con otra tarjeta.'));

                    return redirect()->route('panel.user.money.show');
                    break;
                case 101:
                    session()->flash('error_lemon', __('Ha superado el límite de transacciónes. Por favor, consulte con nuestro soporte.'));

                    return redirect()->route('panel.user.money.show');
                    break;

                case 10:
                    session()->flash('error_lemon', __('Lo sentimos pero el IBAN introducido no es válido. Por favor, inténtalo de nuevo.'));

                    return redirect()->route('panel.user.bankaccounts.show');
                    break;
                default:
                    session()->flash('error_lemon', __('Ha ocurrido un error con LemonWay y no hemos podido procesar su petición. Su dinero está a salvo. Por favor, consulte con nuestro soporte.'));

                    return redirect()->route('panel.user.accredit.show');
                    break;
            }
        }

        if ($exception instanceof TokenMismatchException) {
            //Redirect to login form if session expires
            return redirect()->to('/');
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        if ($request->expectsJson()) {
            return response()->json(['error' => 'Unauthenticated.'], 401);
        }

        return redirect()->guest('/login');
    }
}
