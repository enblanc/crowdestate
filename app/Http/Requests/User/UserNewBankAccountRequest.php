<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserNewBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado && auth()->user()->activado) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'iban'     => ['required'], //, 'regex:/^([A-Z]{2}[0-9]{32})$/'
            'titular'  => 'required|alpha_spaces',
            'pais'     => 'required|alpha_spaces',
            'extracto' => 'required|image|max:3072',
        ];
    }
}
