<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;

class UserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->guest() || auth()->user()->isRole('super|admin')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'    => 'required|allowed_username|alpha_spaces',
            'apellido1' => 'required|allowed_username|alpha_spaces',
            'apellido2' => 'required|allowed_username|alpha_spaces',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|min:8|regex:/^\S+(?=.*[a-z])(?=.*\d).\S+$/',
        ];
    }

    /**
     * Get the proper failed validation response for the request.
     *
     * @param array $errors
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return new JsonResponse($errors, 422);
    }
}
