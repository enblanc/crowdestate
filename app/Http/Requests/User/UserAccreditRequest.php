<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class UserAccreditRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'tipo'           => 'required|in:1,2',
            'telefono'       => 'required',
            'nacionalidad'   => 'required|filled',
            'sexo'           => 'required|in:1,2',
            'birthdate'      => 'required|date_format:d/m/Y',
            'estado_civil'   => 'required|in:1,2,3,4,5,6',
            'profesion'      => 'required|filled',
            'direccion'      => ['required', 'regex:/([a-z_0-9_ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞßàáâãäåæçèéêëìíîïðñòóôõöùúûüýøþÿÐdŒ\\s])+$/i'],
            'localidad'      => 'required|filled',
            'codigo_postal'  => 'required',
            'provincia'      => 'required|filled',
            'pais'           => 'required|filled',
            'acreditado'     => 'required|in:0,1',
            'imagen_dni'     => 'image|max:3072',
            'imagen_dni_dos' => 'image|max:3072',
        ];

        if (Request::get('tipo') == 2) {
            $validations['company_cif'] = 'required';
            $validations['company_rsocial'] = 'required';
            $validations['company_description'] = 'required|string';
            $validations['company_website'] = 'required|valid_url';
            $validations['company_country'] = 'required|filled';
        }

        if (!auth()->user()->activado) {
            if (auth()->user()->profile->dni == $this->dni) {
                $validations['dni'] = ['required']; //, 'regex:/^[XYZ]?([0-9]{7,8})([A-Z])$/i'
            } else {
                $validations['dni'] = ['required']; //, 'regex:/^[XYZ]?([0-9]{7,8})([A-Z])$/i'
            }

            if (Request::get('tipoArchivo') == 1) {
                $validations['imagen_dni'] = 'required|image|max:3072';
                $validations['imagen_dni_dos'] = 'required|image|max:3072';
            } else {
                $validations['webcam_front'] = 'required';
                $validations['webcam_back'] = 'required';
            }
        }

        if (auth()->user()->profile->dni != Request::get('dni')) {
            $validations['dni'] = ['required']; //, 'regex:/^[XYZ]?([0-9]{7,8})([A-Z])$/i'
            // $validations['imagen_dni'] = 'required|image|max:3072';
            // $validations['imagen_dni_dos'] = 'required|image|max:3072';
            if (Request::get('tipoArchivo') == 1) {
                $validations['imagen_dni'] = 'required|image|max:3072';
                $validations['imagen_dni_dos'] = 'required|image|max:3072';
            } else {
                $validations['webcam_front'] = 'required';
                $validations['webcam_back'] = 'required';
            }
        }

        return $validations;
    }

    public function messages()
    {
        return [
            'birthdate.required'           => __('El campo fecha de nacimiento es obligatorio'),
            'birthdate.date_format'        => __('El campo fecha de nacimiento no tiene una fecha correcta. EL formato correcto es dd/mm/YYYY'),
            'company_cif.required'         => __('El cif de la compañía es obligatorio'),
            'company_rsocial.required'     => __('La razón social de la compañía es obligatoria'),
            'company_description.required' => __('La descripción de la compañía es obligatoria'),
            'company_website.required'     => __('La página web de la compañía es obligatoria'),
            'company_website.valid_url'    => __('La página web de la compañía debe ser una url válida'),
            'company_country.required'     => __('El país de registro de la compañía es obligatorio'),
            'webcam_front.required'        => __('Debes agregar una imagen de la parte trasera del documento'),
            'webcam_back.required'         => __('Debes agregar una imagen de la parte trasera del documento'),
        ];
    }
}
