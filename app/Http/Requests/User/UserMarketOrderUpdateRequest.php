<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserMarketOrderUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'id'       => 'required',
            'status'   => 'required',
            'quantity' => 'required|numeric',
            'price'    => 'required|numeric',
        ];

        if (request()->has('type') && request()->get('tipo') == 'sell') {
            $validations['inversion'] = 'required|exists:marketplace_orders';
        }

        return $validations;
    }
}
