<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserRemoveBankAccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado && auth()->user()->activado) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cuenta_id' => 'required|exists:bank_accounts,id',
        ];
    }

    public function messages()
    {
        return [
            'cuenta_id.required' => __('No se ha podido encontrar esta cuenta. Por favor, prueba de nuevo.'),
            'cuenta_id.exists'   => __('No se ha podido encontrar esta cuenta. Por favor, prueba de nuevo.'),
        ];
    }
}
