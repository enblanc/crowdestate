<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InvertirInmuebleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado && auth()->user()->activado) {
            return true;
        }

        if (auth()->user() && !auth()->user()->activado && pago_sin_confirmar()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $cards = ['new', 'fondos'];

        foreach (auth()->user()->cards as $card) {
            $cards[] = $card->ID;
        }

        return [
            'property_id' => 'required|exists:properties,id',
            'inversion'   => 'required|numeric',
            'forma-pago'  => [
                'required',
                Rule::in($cards),
            ],
            'terminos'    => 'required',
        ];
    }
}
