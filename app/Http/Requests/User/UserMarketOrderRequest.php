<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserMarketOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validations = [
            'type'     => 'required|in:1,2',
            'status'   => 'required',
            'quantity' => 'required|numeric',
            'price'    => 'required|numeric',
        ];

        $type = request()->get('type');

        if ($type == '1') {
            $validations['inversion'] = 'required|exists:property_user,id';
        }

        if ($type == '2') {
            $validations['property'] = 'required|exists:properties,id';
        }

        return $validations;
    }
}
