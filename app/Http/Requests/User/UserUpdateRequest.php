<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->guest() || auth()->user()->isRole('super|admin')) {
            return true;
        }

        return false;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'    => 'required|allowed_username|alpha_spaces',
            'apellido1' => 'required|allowed_username|alpha_spaces',
            'apellido2' => 'required|allowed_username|alpha_spaces',
            'email'     => 'required|email|min:3|max:255|unique:users,email,'.\Request::get('id'),
            'password'  => 'confirmed|max:255',
        ];
    }
}
