<?php

namespace App\Http\Requests\User;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user() && auth()->user()->confirmado) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::user()->id;

        return [
            'nombre'    => 'required|allowed_username|alpha_spaces',
            'apellido1' => 'required|allowed_username|alpha_spaces',
            'apellido2' => '',
            'email'     => 'required|email|unique:users,email,'.$id,
        ];
    }
}
