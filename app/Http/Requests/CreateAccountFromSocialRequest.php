<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateAccountFromSocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->guest()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre'      => 'required|max:255',
            'apellido1'   => 'required|max:255',
            'apellido2'   => 'required|max:255',
            'email'       => 'required|email|max:255|unique:users',
            'provider'    => 'required|in:facebook,linkedin,google',
            'provider_id' => 'required',
            'terminos'    => 'required',
        ];
    }
}
