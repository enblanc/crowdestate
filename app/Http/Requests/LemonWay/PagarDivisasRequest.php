<?php

namespace App\Http\Requests\LemonWay;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PagarDivisasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user()) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mes'         => [
                'required',
                Rule::in(['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre']),
            ],
            'year'        => 'required|date_format:Y',
            'property_id' => 'required|exists:properties,id',
        ];
    }
}
