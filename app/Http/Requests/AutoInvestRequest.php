<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AutoInvestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validator = [
            'period'            => 'required|date_format:d/m/Y',
            'portfolio_size'    => 'required|numeric',
            'prices'            => 'required',
            'location'          => 'required',
            'tir'               => 'required|numeric'
        ];

        if (request()->has('property_type_id') && request()->property_type_id != -1) {
            $validator['property_type_id'] = 'exists:property_types,id';
        }

        if (request()->has('developer_id') && request()->developer_id != -1) {
            $validator['developer_id'] = 'exists:developers,id';
        }

        return $validator;
    }
}
