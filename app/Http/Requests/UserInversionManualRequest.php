<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserInversionManualRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user()->isRole('super|admin')) {
            return true;
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id'     => 'required|exists:users,id',
            'property_id' => 'required|exists:properties,id',
            'amount'      => 'required|numeric',
        ];
    }
}
