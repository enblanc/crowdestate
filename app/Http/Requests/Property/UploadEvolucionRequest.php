<?php

namespace App\Http\Requests\Property;

use Illuminate\Foundation\Http\FormRequest;

class UploadEvolucionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user()->isRole('super|admin')) {
            return true;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'evolucion' => 'required|file|sheet_name:evolucion',
            'mes'       => 'required|in:0,1,2,3,4,5,6,7,8,9,10,11',
            'year'      => 'required|date_format:Y',
        ];
    }
}
