<?php

namespace App\Http\Middleware;

use Closure;
use Cookie;
// use Symfony\Component\HttpFoundation\Cookie;

class VerifyClickid
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Obtiene el CLICK_ID de los parametros si existe en la URL para _FINANCEADS
        if ($clickid = $request->input('s_id')) {
            $this->setCookie(config('clickid.cookie-click-id'), $clickid);
            $this->setCookie(config('clickid.cookie-provider'), 'financeads');
        } else {
            // Obtiene el PROVIDER de los parametros si existe en la URL para DOAFF
            $provider = $request->input('utm_source');
            $clickid = $request->input('v');
            if ($clickid && $provider) {
                $this->setCookie(config('clickid.cookie-click-id'), $clickid);
                $this->setCookie(config('clickid.cookie-provider'), $provider);
            }
        }

        return $next($request);
    }
    
    /**
     * Crea una cookie sin fecha de expiración
     *
     * @param   string  $key    nombre de la cookie
     * @param   string  $value  valor de la cookie
     *
     * @return  void
     */
    private function setCookie ($key, $value) {
        Cookie::queue(Cookie::forever($key, $value));
    }
}
