<?php

namespace App\Http\Middleware;

use App\Traits\GetBrowserLocaleTrait;
use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class LocaleMiddleware
{
    use GetBrowserLocaleTrait;
    /**
     * @var mixed
     */
    protected $request;

    /**
     * @var mixed
     */
    protected $supportedLocales;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $this->request = $request;
        $this->supportedLocales = localization()->getSupportedLocales();

        // $this->getDefaultLocale(), $this->getSupportedLocales()

        $user = auth()->user();

        if ($user) {
            $locale = $user->locale;

            //Comprueba si el usuario ha cambiado el locale y se lo guarda al usuario
            if (array_key_exists('change_locale', $request->query())) {
                $newLocale = $request->query('change_locale');
                $this->setNewLocaleToUser($user, $newLocale);
                Cookie::queue('localization', $newLocale, 30 * 120);
                $locale = $newLocale;
            }

            Cookie::queue('localization', $locale, 30 * 120);
            Cookie::queue('locale', $locale, 30 * 120);

            if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
                App::setLocale($locale);
                localization()->setLocale($locale);

                return redirect($this->translateRoute($request, $locale));
            }
        }

        if (!$request->cookie('localization')) {

            // $locale = $this->getFromAcceptedLanguagesHeader();

            $locale = 'es';

            Cookie::queue('localization', $locale, 30 * 120);

            if (!$locale) {
                $locale = current_locale();
            }

            //If request is property show we just return the page without translate it
            if ($request->route()) {
                if ($request->route()->getName() == 'front.property.show') {
                    return $next($request);
                }
                if (strpos($request->route()->getName(), 'front.blog') !== false) {
                    return $next($request);
                }
            }

            return redirect($this->translateRoute($request, $locale));
        }

        if ($request->has('locale')) {
            $locale = $request->get('locale');
            App::setLocale($locale);
            localization()->setLocale($locale);

            Cookie::queue('localization', $locale, 30 * 120);
            Cookie::queue('locale', $locale, 30 * 120);

            return redirect($this->translateRoute($request, $locale));
        }

        // if ($request->cookie('locale')) {
        //     $locale = $request->cookie('locale');
        //     App::setLocale($locale);
        //     localization()->setLocale($locale);

        //     return redirect($this->translateRoute($request, $locale));
        // }

        if (!$request->cookie('locale')) {
            $locale = 'en';
            if ($request->cookie('locale')) {
                $locale = $request->cookie('locale');
            }

            if ($request->cookie('localization')) {
                $locale = $request->cookie('localization');
            }

            App::setLocale($locale);
            localization()->setLocale($locale);
            $locale = current_locale();
            // dump($locale);
            // dd('esaap');

            Cookie::queue('localization', $locale, 30 * 120);
            Cookie::queue('locale', $locale, 30 * 120);

            return redirect($this->translateRoute($request, $locale));
        }

        $this->correctCallbacksLanguage();

        return $next($request);
    }

    private function correctCallbacksLanguage()
    {
        $locale = localization()->getCurrentLocale();

        $this->setDriverRoute('facebook', $locale);
        $this->setDriverRoute('google', $locale);
        $this->setDriverRoute('linkedin', $locale);
    }

    /**
     * @param $locale
     */
    private function setDriverRoute($driver, $locale)
    {
        $route = config('services.'.$driver.'.redirect');
        $routeTranslated = route('social.callback', ['driver' => $driver]);

        config(['services.'.$driver.'.redirect' => $routeTranslated]);
    }

    /**
     * Traduce la ruta dada a la del usuario
     *
     * @param   \Illuminate\Http\Request  $request
     *
     * @return  string
     */
    private function translateRoute($request, $locale)
    {
        if (!$locale) {
            $locale = current_locale();
        }

        localization()->setLocale($locale);
        $routeName = $request->route()->getName();

        $routeTranslated = $this->getRoutePathFromRoute($routeName);

        $urlLocalized = localization()->getUrlFromRouteName($locale, $routeTranslated, $request->query());

        if (array_key_exists('logged', $request->query())) {
            $urlLocalized .= '?logged';
        }

        return $urlLocalized;
    }

    /**
     * Devuelve la ruta correcta
     *
     * @param $route
     */
    private function getRoutePathFromRoute($route)
    {
        $route = str_replace('.', '-', $route);

        return config('brickstarter.rutas.'.$route);
    }

    /**
     * Cambia el locale del usuario al nuevo
     *
     * @param  App\User  $user
     * @param  string  $locale
     */
    private function setNewLocaleToUser($user, $locale)
    {
        $user->locale = $locale;
        $user->save();
    }
}
