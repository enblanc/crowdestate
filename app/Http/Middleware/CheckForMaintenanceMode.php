<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;

class CheckForMaintenanceMode
{
    /**
     * The application implementation.
     *
     * @var \Illuminate\Contracts\Foundation\Application
     */
    protected $app;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Foundation\Application  $app
     * @return void
     */
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->app->isDownForMaintenance() && !in_array($request->ip(), $this->getIpsAsArray())) {
            $data = json_decode(file_get_contents($this->app->storagePath().'/framework/down'), true);

            throw new MaintenanceModeException($data['time'], $data['retry'], $data['message']);
        }

        return $next($request);
    }

    /**
     * Devuelve las ips en modo array
     *
     * @return mixed
     */
    private function getIpsAsArray()
    {
        $ipsFromEnv = env('MAINTENANCE_IPS');
        if ($ipsFromEnv != '') {
            $ipsFromEnv = explode(',', $ipsFromEnv);
        } else {
            $ipsFromEnv = [];
        }

        //Only when is not in dev process
        $whitelist = ['127.0.0.1', '::1'];

        if (!in_array($_SERVER['REMOTE_ADDR'], $whitelist)) {
            return array_merge($ipsFromEnv, ['127.0.0.1']);
        }

        return $ipsFromEnv;
    }
}
