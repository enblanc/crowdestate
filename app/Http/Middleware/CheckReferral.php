<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckReferral
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasCookie('referral')) {
            if ($request->route()->getName() == 'referred') {
                return redirect($request->fullUrl())->withCookie(cookie('referral', $request->route('token'), time() + 60 * 60 * 24 * 20));
            }
        }

        if (auth()->check()) {
            Cookie::queue(Cookie::forget('referral'));
        }

        return $next($request);
    }
}
