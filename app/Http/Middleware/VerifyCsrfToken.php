<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'pagos/correcto',
        'pagos/cancelado',
        'pagos/error',
        'payments/success',
        'payments/cancel',
        'payments/error',
        'en/payments/success',
        'en/payments/cancel',
        'en/payments/error',
    ];
}
