<?php

namespace App\Http\Middleware;

use Closure;

class AccreditedMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!auth()->user()->hasLemonAccount()) {
            return redirect()->route('panel.user.accredit.show')->withErrors([__('Debes acreditar tu cuenta antes de poder acceder a más apartados')]);
        }
        if (!auth()->user()->activado && !pago_sin_confirmar()) {
            return redirect()->route('panel.user.accredit.show')->withErrors([__('Debes acreditar tu cuenta antes de poder acceder a más apartados')]);
        }

        return $next($request);
    }
}
