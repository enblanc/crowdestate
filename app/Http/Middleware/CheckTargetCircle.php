<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Cookie;

class CheckTargetCircle
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->hasCookie('click_id')) {
            if ($request->has('click_id')) {
                return redirect($request->fullUrl())->withCookie(cookie('click_id', $request->get('click_id'), time() + 60 * 60 * 24 * 20));
            }
        }

        if (auth()->check()) {
            Cookie::queue(Cookie::forget('click_id'));
        }

        return $next($request);
    }
}
