<?php

// Home
Breadcrumbs::register('dashboard', function ($breadcrumbs) {
    $breadcrumbs->push('Dashboard', route('dashboard'));
});

// Users
Breadcrumbs::register('user.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Usuarios', route('user.index'));
});

// Users
Breadcrumbs::register('user.edit', function ($breadcrumbs, $user) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('Editar usuario: '.$user->name, route('user.edit', $user->id));
});

//Users > Create
Breadcrumbs::register('user.create', function ($breadcrumbs) {
    $breadcrumbs->parent('user.index');
    $breadcrumbs->push('Crear Usuario', route('user.create'));
});

// Home > Blog > [Category] > [Page]
Breadcrumbs::register('pages.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Páginas', route('pages.index'));
});

Breadcrumbs::register('pages.edit', function ($breadcrumbs, $page) {
    $breadcrumbs->parent('pages.index');
    $breadcrumbs->push('Editar página ', route('pages.edit', $page->id));
});

// Blog
Breadcrumbs::register('blog.index', function ($breadcrumbs) {
    $breadcrumbs->parent('dashboard');
    $breadcrumbs->push('Blog', route('dashboard.blog.index'));
});

Breadcrumbs::register('blog.create', function ($breadcrumbs) {
    $breadcrumbs->parent('blog.index');
    $breadcrumbs->push('Crear entrada', route('dashboard.blog.create'));
});

Breadcrumbs::register('blog.edit', function ($breadcrumbs, $post) {
    $breadcrumbs->parent('blog.index');
    $breadcrumbs->push('Editar entrada', route('dashboard.blog.edit', $post->id));
});
