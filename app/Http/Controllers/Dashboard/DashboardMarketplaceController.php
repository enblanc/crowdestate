<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\MarketOrder;
use Carbon\Carbon;
use ConfigHelper;
use Illuminate\Http\Request;

class DashboardMarketplaceController extends Controller
{

    /**
     * @return mixed
     */
    public function index(Request $request)
    {
        $myInversions = auth()->user()->properties->pluck('id')->toArray();
        $myPropertiesIds = auth()->user()->getListPropertiesInvertidos()->pluck('id')->toArray();
        $marketOrders = MarketOrder::with(['user', 'property'])
            ->where('status', true)
            ->where('executed', false)
            ->where('user_id', '!=', auth()->id())
            ->whereType($request->get('type'))
            ->whereNotIn('property_id', $myInversions)
            ->whereInvested($request->get('invested'), $myPropertiesIds)
            ->whereCity($request->get('city'))
            ->whereProperty($request->get('property'))
            ->paginate(20);

        return view('dashboard.marketplace.index', ['orders' => $marketOrders]);
    }

}