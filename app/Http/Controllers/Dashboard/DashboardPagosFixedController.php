<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use Carbon\Carbon;
use Prologue\Alerts\Facades\Alert;

class DashboardPagosFixedController extends Controller
{
    
    public function index (Request $request)
    {
        if ($request->has('fecha_inicio') && $request->has('fecha_fin')) {
            $startOfDay = Carbon::createFromFormat('d/m/Y', $request->fecha_inicio)->startOfDay();
            $endOfDay = Carbon::createFromFormat('d/m/Y', $request->fecha_fin)->endOfDay();
            if ($startOfDay > $endOfDay) {
                Alert::warning('"Fecha final" no puede ser menor que "Fecha inicial"')->flash();
                return redirect()->back();
            }

            $transactions = Transaction::where('type', 13)
                ->where('status', 3)
                ->whereBetween('fecha', [$startOfDay->toDateTimeString(), $endOfDay->toDateTimeString()])
                ->get();
        } else {
            $transactions = [];
        }

        return view('dashboard.pagos-fixed.index', ['transactions' => $transactions]);
    }

    public function post (Request $request)
    {
        if ($request->has('fecha_inicio') && $request->has('fecha_fin')) {
            $startOfDay = Carbon::createFromFormat('d/m/Y', $request->fecha_inicio)->startOfDay();
            $endOfDay = Carbon::createFromFormat('d/m/Y', $request->fecha_fin)->endOfDay();
            if ($startOfDay > $endOfDay) {
                Alert::warning('"Fecha final" no puede ser menor que "Fecha inicial"')->flash();
                return redirect()->back();
            }
            
            $transactions = Transaction::where('type', 13)
                ->where('status', 3)
                ->whereBetween('fecha', [$startOfDay->toDateTimeString(), $endOfDay->toDateTimeString()])
                ->get();

            foreach ($transactions as $key => $transaction) {
                $explode_comment = explode(' ', $transaction->comment);
                $new_year = end($explode_comment);
                $fecha_nueva = Carbon::parse($transaction->fecha);
                $fecha_nueva->year = $new_year;
                $transaction->fecha = $fecha_nueva->toDateTimeString();
                $transaction->save();
            }
            
            Alert::info('Datos de la propiedad actualizados')->flash();
            return redirect()->route('dashboard.pagos.fixed');
        }
    }
}
