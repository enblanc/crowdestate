<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\PaymentsDataTable;
use App\Http\Controllers\Controller;
use App\Jobs\ProcesarPagos;
use App\Payment;
use App\User;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Prologue\Alerts\Facades\Alert;

class DashboardPagosController extends Controller
{

    /**
     * @var array
     */
    protected $goodHeaders = [
        'tipo',
        'usuario-id',
        'precio',
        'concepto',
        'comentario',
    ];

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PaymentsDataTable $dataTable)
    {
        return $dataTable->render('dashboard.payments.index');
    }

    /**
     * @param Request $request
     */
    public function uploadFile(Request $request)
    {
        Excel::selectSheetsByIndex(0)->load($request->excel, function ($reader) {
            // Getting all results
            $results = $reader->toArray();
            $first = reset($results);

            if (!$first) {
                Alert::info('No hay registros en el excel')->flash();

                return redirect()->back();
            }

            if ($first) {
                $headers = array_keys($first);

                if ($this->goodHeaders != $headers) {
                    Alert::info('Error en la cabecera del excel')->flash();

                    return redirect()->back();
                }
            }

            foreach ($results as $item) {
                $pago = $this->createPayment($item);
                if ($pago != false) {
                    $paymentJob = new ProcesarPagos($pago);
                    dispatch($paymentJob);
                }
            }
        });

        Alert::info('Pagos cargados correctamente')->flash();

        return redirect()->back();
    }

    /**
     * @param $id
     */
    public function restart($id)
    {
        $payment = Payment::findOrFail($id);
        $payment->update(['status' => 2]);

        $paymentJob = new ProcesarPagos($payment);
        dispatch($paymentJob);
    }

    /**
     * @param $item
     */
    private function createPayment($item)
    {
        $pago = Payment::create([
            'type'    => $item['tipo'],
            'user_id' => $item['usuario-id'],
            'price'   => $item['precio'],
            'subject' => $item['concepto'],
            'comment' => $item['comentario'],
            'status'  => 2,
        ]);

        // Comprobamos si el tipo es válido
        if (!in_array($pago->type, $pago->validTypes)) {
            $pago->update(['status' => 3]);

            return false;
        }

        // Comprobamos si el usuario existe
        $user = User::find($item['usuario-id']);

        if (!$user) {
            $pago->update(['status' => 5]);

            return false;
        }

        return $pago;
    }
}
