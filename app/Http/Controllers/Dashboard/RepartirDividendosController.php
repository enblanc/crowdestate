<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Http\Requests\LemonWay\PagarDivisasRequest;
use App\Jobs\RepartirDividendos;
use App\Property;
use App\PropertyUser;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;

class RepartirDividendosController extends Controller
{

    /**
     * Lista de meses
     *
     * @var array
     */
    protected $months = [
        'enero'      => 1,
        'febrero'    => 2,
        'marzo'      => 3,
        'abril'      => 4,
        'mayo'       => 5,
        'junio'      => 6,
        'julio'      => 7,
        'agosto'     => 8,
        'septiembre' => 9,
        'octubre'    => 10,
        'noviembre'  => 11,
        'diciembre'  => 12,
    ];

    /**
     * Construct
     */
    public function __construct()
    {
        $this->months = collect($this->months);
    }

    /**
     * Paga las divisas para la propiedad, el año y el mes dado
     *
     * @param Request $request
     */
    public function pagarDivisas(PagarDivisasRequest $request)
    {
        $property = Property::find($request->get('property_id'));
        $mesString = $request->get('mes');
        $year = $request->get('year');
        $mes = $this->months->get($mesString);

        logger('PAGAR DIVIDENDOS => ' . $request->get('property_id') . ' ' . $mes . '-' . $year);
        logger('App\Http\Controllers\Dashboard\RepartirDividendosController::49');

        $evolucion = $property->evolutions()->year($year)->month($mes)->first();

        $evolucion->update(['abonado' => 2]);

        $cantidadAbonarTotal = $evolucion->dividendos_abonar;

        $i = 10;
        foreach ($property->usersGroup as $user) {
            $movimientosDividendos = $user->transactions()->where('type', 13)->where('status', 3)->whereRaw('MONTH(fecha)  = ?', $mes)->whereRaw('YEAR(fecha)  = ?', $year)->where('extras', $property->id)->first();
            if (!$movimientosDividendos) {
                $job = (new RepartirDividendos($property, $user, $evolucion, $cantidadAbonarTotal, $mes, $year))
                    ->delay(Carbon::now()->addSeconds($i));
                dispatch($job);
                $i = $i + 10;
            } else {
                Log::info('El usuario ya está pagado '.$user->id);
            }
        }
    }

    /**
     * Función de prueba
     *
     * @return [type]
     */
    public function testPago()
    {
        $property = Property::find(15);
        $mesString = 'febrero';
        $year = 2018;
        $mes = $this->months->get($mesString);

        $evolucion = $property->evolutions()->year($year)->month($mes)->first();

        $evolucion->update(['abonado' => 2]);

        $cantidadAbonarTotal = $evolucion->dividendos_abonar;

        $i = 10;
        foreach ($property->usersGroup as $user) {
            $movimientosDividendos = $user->transactions()->where('type', 13)->where('status', 3)->whereRaw('MONTH(fecha)  = ?', $mes)->whereRaw('YEAR(fecha)  = ?', $year)->where('extras', $property->id)->first();
            if (!$movimientosDividendos) {
                $job = (new RepartirDividendos($property, $user, $evolucion, $cantidadAbonarTotal, $mes, $year))
                    ->delay(Carbon::now()->addSeconds($i));
                dispatch($job);
                $i = $i + 10;
            } else {
                Log::info('El usuario ya está pagado '.$user->id);
            }
        }
    }

    /**
     * Obtiene el listado de los dividendo del la propiedad con respecto al mes y año.
     *
     * @param integer $property_id
     * @param integer $year
     * @param integer $mes
     * 
     * @return html
     */
    public function listDivisasById ($property_id, $year, $mes)
    {
        $property = Property::find($property_id);

        $mes = isset($this->months[$mes]) ? $this->months[$mes] : $mes;
        if (!$property)
            dd('Propiedad no existe');

        $evolucion = $property->evolutions()->year($year)->month($mes)->first();

        if (!$evolucion)
            dd('No posee Evolutions');

        $cantidadAbonarTotal = $evolucion->dividendos_abonar;
        $totalDividios = 0;
        $datos = collect();
        $datos->propiedad_name = $property->nombre;
        $datos->propiedad_id = $property->id;
        $datos->cantidadAbonarTotal = $evolucion->dividendos_abonar;
        $datos->datos_users = [];
        $datos->objetivo = $property->objetivo;

        foreach ($property->usersGroup as $user) {
                $movimientosDividendos = $user->transactions()
                                          ->where('type', 13)
                                          ->where('status', 3)
                                          ->whereRaw('MONTH(fecha)  = ?', $mes)
                                          ->whereRaw('YEAR(fecha)  = ?', $year)
                                          ->where('extras', $property->id)
                                          ->first();

                $inversiones = PropertyUser::where('property_id', $property->id)->where('user_id', $user->id)->get();
                
                $datos_user['inversiones'] = $inversiones->count();

                $cantidadAbonarUsuario = 0;
                $totalInvertido = 0;
                foreach ($inversiones as $inversion) {
                    $totalInvertido += $inversion->total;
                    $datos_user['totalInvertido'] = $totalInvertido;
                    $cantidadAbonarUsuario += round(($inversion->total * $cantidadAbonarTotal) / $property->objetivo, 2);
                }
                
                if ($user->tribute) {
                    $cantidadAbonarUsuario = round($cantidadAbonarUsuario * 0.81, 2);
                }
                $totalDividios = $totalDividios + $cantidadAbonarUsuario;
                
                $datos_user['tribute'] = $user->tribute;
                $datos_user['cantidadAbonarUsuario'] = $cantidadAbonarUsuario;
                $datos_user['totalInvertido'] = $totalInvertido;
                $datos_user['totalDividios'] = $totalDividios;
                $datos_user['user_id'] = $user->id;
                $datos_user['user_nombre'] = $user->nombre_completo;
                $datos_user['pagado'] = $movimientosDividendos;
                
                $datos->datos_users[] = $datos_user;
        }

        usort($datos->datos_users, function ($item1, $item2) {
            return $item1['cantidadAbonarUsuario'] < $item2['cantidadAbonarUsuario'] ? 1 : -1;
        });

        return view('dashboard.properties._list-divisas', ['datos' => $datos]);
    }
}
