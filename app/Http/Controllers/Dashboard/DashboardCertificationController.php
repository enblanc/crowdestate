<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\CertificationsDataTable;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Prologue\Alerts\Facades\Alert;
use App\Helpers\CertificationHelper;
use App\User;
use App\Certification;
use Illuminate\Http\Request;

class DashboardCertificationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CertificationsDataTable $dataTable)
    {
        return $dataTable->render('dashboard.certifications.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show (Request $request, Certification $certification)
    {
        $certificationHelper = new CertificationHelper($certification);

        $certificationHelper->generate();    
        
        return response()->download($certificationHelper->getPath());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload (Request $request, Certification $certification)
    {
        $certification->update($request->all());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy (Certification $certification)
    {
        $certification->delete();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function post (Request $request)
    {
        Excel::selectSheetsByIndex(0)->load($request->excel, function ($reader) {
            // Getting all results
            $rows = $reader->toArray();
            $first = reset($rows);

            if (!$first) {
                Alert::info('No hay registros en el excel')->flash();

                return redirect()->back();
            }
            
            foreach ($rows as $key => $row) {
                if (!User::find($row['user-id'])) {
                    Alert::error("No se pudo encontrar el ID del usuario {$row['user-id']}. Por favor, revise la línea #$key del documento de Excel para obtener más información.")->flash();

                    return redirect()->back();
                }
            }

            foreach ($rows as $row) {
                $certification = Certification::create([
                    'user_id' => $row["user-id"],
                    'direccion_1' => $row["direccion-1"],
                    'direccion_2' => $row["direccion-2"],
                    'direccion_3' => $row["direccion-3"],
                    'year' => $row["year"],
                    'nif' => $row["nif"],
                    'name' => $row["name"],
                    'total' => number_format($row["total"], 2, '.', ''),
                    'retention' => number_format($row["retention"], 2, '.', ''),
                    'paid' => number_format($row["paid"], 2, '.', ''),
                    'business_name' => $row["business-name"],
                    'cif' => $row["cif"],
                    'public_name' => $row["public-name"],
                    'presented_with_date' => $row["presented-with-date"],
                    'receipt_number' => $row["receipt-number"],
                    'date' => $row["date"],
                    'signature' => $row["signature"]
                ]);
                
                $certification->generatePDF();
            }

            Alert::info('¡Excelente! Los certificados han sido subidos correctamente y están listos para ser revisados.')->flash();
    
        });
        
        return redirect()->back();
    }
}