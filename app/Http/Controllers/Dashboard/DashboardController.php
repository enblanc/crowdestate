<?php

namespace App\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Property;
use App\Transaction;
use Carbon\Carbon;
use ConfigHelper;
use Illuminate\Http\Request;

class DashboardController extends Controller
{

    /**
     * @return mixed
     */
    public function index()
    {
        // Propiedades out date
        $properties = Property::all();
        $now = Carbon::now();
        $propertiesOutDate = $properties->filter(function ($property) use ($now) {
            $endDate = $property->fecha_publicacion->addDays($property->periodo_financiacion);
            $diferencia = $now->diffInDays($endDate, false);
            if ($diferencia < 1 && $property->property_state_id == 2) {
                return $property;
            }
        });

        // Últimas inversiones
        $inversiones = Transaction::whereType(2)->whereStatus(3)->orderBy('created_at', 'DESC')->get()->take(25);

        return view('dashboard.home', compact('propertiesOutDate', 'inversiones'));
    }

    /**
     * @param Request $request
     */
    public function pagoSinValidar(Request $request)
    {
        $file = str_replace(config_path().DIRECTORY_SEPARATOR, '', 'brickstarter.php');

        ConfigHelper::save($file, 'pago_sin_confirmar', ($request->tipo == 1) ? true : false);

        return redirect()->route('dashboard');
    }
}
