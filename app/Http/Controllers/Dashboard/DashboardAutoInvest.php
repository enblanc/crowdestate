<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\AutoInvest;
use App\AutoinvestsOrders;
use App\AutoInvestsEvents;
use App\Developer;
use App\PropertyType;
use Carbon\Carbon;

class DashboardAutoInvest extends Controller
{
    /**
     * @return mixed
     */
    public function index()
    {        
        $developers = array('-1' => 'Todos');
        foreach (Developer::all() as $key => $developer) {
            $developers[$developer->id] = $developer->nombre;
        }

        $types = array('-1' => 'Todos');
        foreach (PropertyType::all() as $key => $type) {
            $types[$type->id] = $type->nombre;
        }
        
        return view('dashboard.autoinvest.index', [
            'autoinvests'             => AutoInvest::with('user')->with('orders')->orderBy('updated_at', 'desc')->paginate(100),
            'autoinvests_deactivated' => AutoInvest::where('status', false)->count(),
            'autoinvests_active'      => AutoInvest::where('status', true)->count(),
            'autoinvests_used'        => AutoInvest::where('investment', '>', 0)->count(),
            'properties'              => [
                'developers'  =>       $developers,
                'types'       =>       $types
            ]
        ]);
    }


    /**
     * Mostrar las ordenes (inversiones) que provienen del Autoinvest
     *
     * @return void
     */
    public function orders ()
    {
        return view('dashboard.autoinvest.orders', [
            'orders'               => AutoinvestsOrders::with(['autoinvest', 'autoinvest.user', 'property', 'market_order'])->orderBy('updated_at', 'desc')->paginate(100),
            'orders_properties'    => AutoinvestsOrders::where('market_order_id', null)->count(),
            'orders_marketplaces'  => AutoinvestsOrders::where('property_id', null)->count()
        ]);
    }

    public function events ()
    {
        $developers = array('-1' => 'Todos');
        foreach (Developer::all() as $key => $developer) {
            $developers[$developer->id] = $developer->nombre;
        }

        $types = array('-1' => 'Todos');
        foreach (PropertyType::all() as $key => $type) {
            $types[$type->id] = $type->nombre;
        }

        return view('dashboard.autoinvest.events', [
            'events'           => AutoInvestsEvents::orderBy('updated_at', 'desc')->paginate(100),
            'events_today'     => AutoInvestsEvents::whereDate('created_at', Carbon::today()->format('Y-m-d'))->count(),
            'events_yesterday' => AutoInvestsEvents::whereDate('created_at', Carbon::yesterday()->format('Y-m-d'))->count(),
            'properties'              => [
                'developer_id'     => $developers,
                'property_type_id' => $types
            ]
        ]);
    }
}
