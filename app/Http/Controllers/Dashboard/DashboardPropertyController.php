<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\PropertiesDataTable;
use App\Developer;
use App\Helpers\ImportadorExcel;
use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Property\UploadEstimacionRequest;
use App\Http\Requests\Property\UploadEvolucionRequest;
use App\Http\Requests\Property\UploadResumenRequest;
use App\Http\Requests\Property\StorePropertyRequest;
use App\Investment;
use App\Media;
use App\Property;
use App\PropertyState;
use App\PropertyType;
use App\Transaction;
use Carbon\Carbon;
use Flugg\Responder\Facades\Responder;
use Illuminate\Http\Request;
use Infinety\CRUD\Models\Locale;
use Infinety\LemonWay\Facades\LemonWay;
use Jenssegers\Date\Date;
use Prologue\Alerts\Facades\Alert;

class DashboardPropertyController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware(['auth', 'role:super|admin']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(PropertiesDataTable $dataTable)
    {
        return $dataTable->render('dashboard.properties.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Date::setLocale('es');

        $types = PropertyType::all();
        $states = PropertyState::all();
        $investments = Investment::all();
        $developers = Developer::all();

        return view('dashboard.properties.create', compact('types', 'states', 'investments', 'developers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StorePropertyRequest $request)
    {
        $data = $request->all();

        // $data = RequestsHelpers::addDefaultResumen($data);

        $property = Property::create($data);

        return redirect()->route('inmuebles.edit', $property->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Property  $property
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Property $property)
    {
        // return response(file_get_contents(public_path('37.json')));

        return responder()->success($property);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Property  $property
     *
     * @return Illuminate\Http\Response
     */
    public function edit(Property $property, Request $request)
    {
        Date::setLocale('es');

        $locales = Locale::getAvailables();
        $types = PropertyType::all();
        $states = PropertyState::all();
        $investments = Investment::all();
        $developers = Developer::all();
        $details = $property->details->keyBy('locale');

        return view('dashboard.properties.edit', compact('property', 'locales', 'details', 'types', 'states', 'investments', 'developers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Illuminate\Http\Request  $request
     * @param  App\Property  $property
     *
     * @return Illuminate\Http\Response
     */
    public function update(Request $request, Locale $localeModel, Property $property)
    {
        $result = $property->update($request->all());

        $locales = $localeModel->whereState(1)->get();
        foreach ($locales as $locale) {
            if ($request->has($locale->iso)) {
                $data = (object) $request->get($locale->iso);
                $details = $property->details()->whereLocale($locale->iso)->first();

                if ($details === null) {
                    $details = $property->details()->create(['locale' => $locale->iso]);
                }

                // Actualizamos el título y el slug
                if (isset($data->title)) {
                    $details->title = $data->title;
                }
                if (isset($data->slug)) {
                    $details->slug = $data->slug;
                }
                $details->save();
            }
        }

        if ($result) {
            Alert::info('Datos de la propiedad actualizados')->flash();
        }

        return redirect()->back();
    }

    /**
     * Actualiza la información de la propiedad
     *
     * @param  Illuminate\Http\Request  $request
     * @param  App\Property $property
     *
     * @return Illuminate\Http\Response
     */
    public function updateDetails(Request $request, Locale $localeModel, Property $property)
    {
        try {
            if ($request->has('details')) {
                $locales = $localeModel->whereState(1)->get();
    
                foreach ($locales as $locale) {
                    $details = $property->details()->whereLocale($locale->iso)->first();
    
                    if ($details === null) {
                        $details = $property->details()->create(['locale' => $locale->iso]);
                    }
    
                    $data = (object) $request->get($locale->iso);
    
                    // Procesamos las imágenes
                    if ($data->similares_first_image) {
                        $details->clearMediaCollection($details->mediaRelatedFirstCollection);
                        $name = md5(date('c'));
                        $details->uploadFileToMedia($data->similares_first_image, $name, false, true, $details->mediaRelatedFirstCollection, false);
                    }
    
                    if ($data->similares_second_image) {
                        $details->clearMediaCollection($details->mediaRelatedSecondCollection);
                        $name = md5(date('c'));
                        $details->uploadFileToMedia($data->similares_second_image, $name, false, true, $details->mediaRelatedSecondCollection, false);
                    }
    
                    // Actualizamos los demás campos de la ficha
                    $details->update((array) $data);
                }
    
                Alert::info('Informacion de la propiedad actualizados')->flash();
    
                return redirect()->back();
            }
    
            Alert::error('No ha sido posible guardar la informacion')->flash();
    
        } catch (\Throwable $th) {
            logger($th);
            Alert::error($th->getMessage())->flash();
        }

        return redirect()->back();
    }

    /**
     * Sube y procesa el archivo de resumen xlsx para un Inmueble dado
     *
     * @param  UploadResumenRequest  $request
     * @param  Property $property
     *
     * @return json
     */
    public function uploadResumenFile(UploadResumenRequest $request, Property $property)
    {
        $resumen = ImportadorExcel::readResumenExcel($request->resumen);

        if (!is_object($resumen)) {
            return responder()->error('error', 500, 'Error al leer el excel. Comprueba el archivo.');
        }

        return responder()->success(null, (array) $resumen);
    }

    /**
     * Sube un nuevo archivo de resumen para la propiedad dada
     *
     * @param  UploadResumenRequest $request
     * @param  Property             $property
     *
     * @return json
     */
    public function updateResumen(UploadResumenRequest $request, Property $property)
    {
        $name = $this->getRequestNameWithoutExtension($request->resumen);

        $file = $property->uploadFileToMedia($request->resumen, $name, false, true, $property->mediaCollectionExcelResumen, true, ['date' => date('d-m-Y')]);

        $resumen = ImportadorExcel::readResumenExcel($file->getPath());

        if (!is_object($resumen)) {
            return responder()->error('error', 500, 'Error al leer el excel. Comprueba el archivo.');
        }

        //Falta aplicar validación para comprobar que el objeto es correcto.

        //Añadimos al objeto el resumen hecho
        $resumen->resumen = true;

        // Actualizamos la propiedad con los datos del resumen parseados
        $property->update((array) $resumen);

        Alert::info('Resumen de la propiedad subido y procesado')->flash();

        return responder()->success($property);
    }

    /**
     * Sube y procesa el archivo de estimación xlsx para un Inmueble dado
     *
     * @param  UploadEstimacionRequest  $request
     * @param  Property $property
     *
     * @return json
     */
    public function uploadEstimacionFile(UploadEstimacionRequest $request, Property $property)
    {
        $estimacion = ImportadorExcel::readEstimacionExcel($request->estimacion);

        if (!is_object($estimacion)) {
            return responder()->error('error', 500, 'Error al leer el excel. Comprueba el archivo.');
        }

        return responder()->success(null, (array) $estimacion);
    }

    /**
     * Sube y procesa el archivo de estimación para la propiedad dada
     *
     * @param  UploadResumenRequest $request
     * @param  Property             $property
     *
     * @return json
     */
    public function updateEstimacion(UploadEstimacionRequest $request, Property $property)
    {
        $name = $this->getRequestNameWithoutExtension($request->estimacion);

        $file = $property->uploadFileToMedia($request->estimacion, $name, false, true, $property->mediaCollectionExcelEstimacion, true, ['date' => date('d-m-Y')]);

        $estimacion = ImportadorExcel::readEstimacionExcel($file->getPath());

        if (!is_object($estimacion)) {
            return responder()->error('error', 500, 'Error al leer el excel. Comprueba el archivo.');
        }

        //Falta aplicar validación para comprobar que el objeto es correcto.

        //Añadimos al objeto la fecha y el tipo (enum)
        $fecha = Carbon::createFromDate(date('Y'), 1, 1); //Añadimos más 1 para corresponder con el objeto carbon.
        $estimacion->fecha = $fecha->format('Y-m-d');
        $estimacion->tipo = 'prevision';
        $estimacion->media_id = $file->id;

        $propertyPrediction = $property->predictedAll()
            ->where('fecha', $estimacion->fecha)
            ->where('tipo', 'prevision')
            ->first();

        // Añadimos el archivo de evolucion a la propiedad
        if ($propertyPrediction) {
            $propertyPrediction->update((array) $estimacion);
        } else {
            $property->predictedAll()->create((array) $estimacion);
        }

        Alert::info('Archivo de estimacion de la propiedad subido y procesado')->flash();

        return responder()->success($property);
    }

    /**
     * Return data of evolution by given property and year
     *
     * @param  Property $property
     * @param  int   $year
     *
     * @return json
     */
    public function getEvolucionByYear(Property $property, $year = null)
    {
        if (!$year) {
            $year = date('Y');
        }

        return responder()->success(null, ['data' => $property->evolutionsByYear($year)]);
    }

    /**
     * Sube y procesa el archivo de evolucion xlsx para un Inmueble dado
     *
     * @param  UploadEvolucionRequest  $request
     * @param  Property $property
     *
     * @return json
     */
    public function uploadEvolucionFile(UploadEvolucionRequest $request, Property $property)
    {
        $evolucion = ImportadorExcel::readEvolucionExcel($request->evolucion, $request->get('mes'));

        if (!is_object($evolucion)) {
            return responder()->error('error', 500, 'Error al leer el excel. Comprueba el archivo.');
        }

        return responder()->success(null, (array) $evolucion);
    }

    /**
     * Sube un nuevo archivo de resumen para la propiedad dada
     *
     * @param  UploadEvolucionRequest $request
     * @param  Property             $property
     *
     * @return json
     */
    public function updateEvolucion(UploadEvolucionRequest $request, Property $property)
    {
        $name = $this->getRequestNameWithoutExtension($request->evolucion);

        $file = $property->uploadFileToMedia($request->evolucion, $name, false, true, $property->mediaCollectionExcelEvolucion, true, ['date' => date('d-m-Y')]);

        $evolucion = ImportadorExcel::readEvolucionExcel($file->getPath(), $request->get('mes'));

        if (!is_object($evolucion)) {
            return responder()->error('error', 500, 'Error al leer el excel. Comprueba el archivo.');
        }

        //Falta aplicar validación para comprobar que el objeto es correcto.[HECHO]

        $today = Carbon::today();
        $fecha = Carbon::createFromDate($request->get('year'), $request->get('mes') + 1, 1); //Añadimos más 1 para corresponder con el objeto carbon.

        $evolucionData = $property->evolutions()->year($today->year)->month($fecha->month)->first();
        if ($evolucionData) {
            $evolucionData->update((array) $evolucion);
        } else {
            //Añadimos al objeto la fecha y el tipo (enum)

            $evolucion->fecha = $fecha->format('Y-m-d');
            $evolucion->tipo = 'seguimiento';
            $evolucion->media_id = $file->id;

            // Añadimos el archivo de evolucion a la propiedad
            $property->evolutions()->create((array) $evolucion);
        }

        Alert::info('Archivo de evolucion de la propiedad subido y procesado')->flash();

        return responder()->success($property);
    }

    /**
     * Guarda una nueva foto  a la propiedad dada según el request
     *
     * @param  Illuminate\Http\Request  $request
     * @param  App\Property $property
     *
     * @return json
     */
    public function storePhoto(Request $request, Property $property)
    {
        $name = $this->getRequestNameWithoutExtension($request->file);

        $mediaFile = $property->uploadFileToMedia($request->file, $name, false, true, $property->mediaCollection, true);

        $media = Media::find($mediaFile->id);
        $photo = Responder::transform($media)->toCollection();

        return responder()->success(null, ['data' => $photo['data']]);
    }

    /**
     * Elimina una foto según su ID
     *
     * @param  Illuminate\Http\Request $request
     *
     * @return json
     */
    public function destroyPhoto(Request $request)
    {
        if (!$request->has('mediaId')) {
            return responder()->error('error', '500', 'El id dado no es válido');
        }

        $media = Media::find($request->get('mediaId'));

        $media->delete();

        return responder()->success(null, ['data' => 'Imagen borrada correctamente']);
    }

    /**
     * Pone una imagen como principal
     *
     * @param Request $request
     * @param Property $property
     *
     * @return json
     */
    public function principalPhoto(Request $request, Property $property)
    {
        $count = 1;
        foreach ($property->getMedia($property->mediaCollection) as $media) {
            if ($media->id == $request->get('mediaId')) {
                $media->order_column = 1;
                $media->save();
            } else {
                $count++;
                $media->order_column = $count;
                $media->save();
            }
        }

        return responder()->success($property->getMedia($property->mediaCollection));
    }

    /**
     * Añade el texto a una imagen de un inmueble
     *
     * @param Request $request
     * @param Property $property
     */
    public function setTextPhoto(Request $request)
    {
        if (!$request->has('mediaId')) {
            return responder()->error('error', '500', 'El id dado no es válido');
        }

        $media = Media::find($request->get('mediaId'));

        $media->setCustomProperty('text', $request->get('text'));
        $media->save();

        return responder()->success(null, ['data' => 'Texto guardado correctamente']);
    }

    /**
     * Guarda un documento público a la propiedad dada
     *
     * @param Request $request
     * @param App\Property $property
     */
    public function storeDocumentPublic(Request $request, Property $property)
    {
        $locale = $request->header('Locale');
        if (!$locale) {
            $locale = 'es';
        }

        if ($request->file->getMimeType() !== 'application/pdf') {
            return responder()->error($locale);
        }

        $name = $this->getRequestNameWithoutExtension($request->file);

        $meta = ['visibility' => 'public', 'locale' => $locale];
        $mediaFile = $property->uploadFileToMedia($request->file, $name, false, true, $property->mediaCollectionDocuments, true, $meta);

        $media = Media::find($mediaFile->id);
        $documento = Responder::transform($media)->toCollection();

        return responder()->success(null, ['data' => $documento['data']]);
    }

    /**
     * Guarda un documento privado a la propiedad dada
     *
     * @param Request $request
     * @param App\Property $property
     */
    public function storeDocumentPrivate(Request $request, Property $property)
    {
        $locale = $request->header('Locale');
        if (!$locale) {
            $locale = 'es';
        }

        if ($request->file->getMimeType() !== 'application/pdf') {
            return responder()->error($locale);
        }

        $name = $this->getRequestNameWithoutExtension($request->file);

        $meta = ['visibility' => 'private', 'locale' => $locale];
        $mediaFile = $property->uploadFileToMedia($request->file, $name, false, true, $property->mediaCollectionDocuments, true, $meta);

        $media = Media::find($mediaFile->id);
        $documento = Responder::transform($media)->toCollection();

        return responder()->success(null, ['data' => $documento['data']]);
    }

    /**
     * Borra un documento
     *
     * @param Request $request
     * @return json
     */
    public function destroyDocument(Request $request)
    {
        if (!$request->has('mediaId')) {
            return responder()->error('error', '500', 'El id dado no es válido');
        }

        $media = Media::find($request->get('mediaId'));

        $media->delete();

        return responder()->success(null, ['data' => 'Documento borrado correctamente']);
    }

    /**
     * @param Request $request
     */
    public function payDiference(Property $property)
    {
        $diferencia = (double) $property->objetivo - (double) $property->invertido;

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        $cantidadAbonarSC = number_format($diferencia, 2, '.', '');

        $mensaje = 'Pago de diferencia para activar inmueble '.$property->ref;
        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => 11,
            'type'        => 13,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidadAbonarSC,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'DIF-'),
        ];

        $transaction = auth()->user()->transactions()->create($transaction);

        try {
            $result = LemonWay::walletToWallet($walletSC, $property->wallet, $cantidadAbonarSC, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
        } catch (Exception $e) {
            $transaction->update(['status' => 4]);

            return responder()->error('error', 500, 'Error al procesar el pago. Refresca la página.');
        }

        return responder()->success(null, ['data' => 'Pago procesado correctamente']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Property  $property
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Property $property)
    {
        if ($property->publicado) {
            return responder()->error('error', 500, 'El inmueble está publicado y no se puede eliminar.');
        }

        if ($property->users->count() > 0) {
            return responder()->error('error', 500, 'El inmueble tiene inversores y no se puede eliminar');
        }

        if ($property->delete()) {
            return responder()->success(null, ['data' => 'Propiedad borrada correctamente']);
        }
    }

    /**
     * Devuelve el nombre sin la extensión del archivo dado por request
     *
     * @param  Request $requestFile
     *
     * @return string
     */
    private function getRequestNameWithoutExtension($requestFile)
    {
        return pathinfo($requestFile->getClientOriginalName(), PATHINFO_FILENAME);
    }
}
