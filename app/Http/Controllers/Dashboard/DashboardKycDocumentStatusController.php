<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\DataTables\KycDocumentStatusDataTable;
use App\Http\Controllers\Controller;
use App\KycDocumentStatus;

class DashboardKycDocumentStatusController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(KycDocumentStatusDataTable $dataTable)
    {
        return $dataTable->render('dashboard.kyc-document-statuses.index');
    }
}