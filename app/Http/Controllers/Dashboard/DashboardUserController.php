<?php

namespace App\Http\Controllers\Dashboard;

use App\DataTables\UsersDataTable;
use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserAddMoneyManualRequest;
use App\Http\Requests\UserInversionManualRequest;
use App\Http\Requests\User\UserDashboardCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\Notifications\UserInvertirConfirmNotification;
use App\Notifications\UserAccreditedNotification;
use Illuminate\Support\Facades\Log;
use App\Property;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use HttpOz\Roles\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Infinety\LemonWay\Facades\LemonWay;
use Maatwebsite\Excel\Facades\Excel;
use Prologue\Alerts\Facades\Alert;

class DashboardUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(UsersDataTable $dataTable)
    {
        return $dataTable->render('dashboard.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::where('id', '!=', 1)->orderBy('id', 'DESC')->get();

        return view('dashboard.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserDashboardCreateRequest $request)
    {
        $user = User::create($request->all());

        if (!$user) {
            return redirect()->back();
        }

        Alert::info('Usuario creado. Se le ha enviado una confirmación de cuenta por correo')->flash();

        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        $roles = Role::where('id', '!=', 1)->orderBy('id', 'DESC')->get();

        $propiedadesDisponibles = Property::where('property_state_id', 2)->get();

        $propiedades = $propiedadesDisponibles->each->visible();

        return view('dashboard.users.edit-v2', compact('user', 'roles', 'propiedades'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function editaOld(User $user)
    {
        $roles = Role::where('id', '!=', 1)->orderBy('id', 'DESC')->get();

        $propiedadesDisponibles = Property::where('property_state_id', 2)->get();

        $propiedades = $propiedadesDisponibles->each->visible();

        return view('dashboard.users.edit', compact('user', 'roles', 'propiedades'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, User $user)
    {
        // Actualizamos los campos
        $user->update($request->except(['password', 'password_confirmation', '_method', '_token', 'id', 'roles', 'activado']));

        // Asigamos los roles
        if ($request->has('roles')) {
            $user->syncRoles($request->get('roles'));
        }

        // Confirmamos el usuario si hace falta
        if ($request->has('activado') && $request->get('activado') == 1) {
            $confirm = $user->confirm;
            if ($confirm->is_confirmed != true) {
                $confirm->is_confirmed = true;
                $confirm->hash = null;
                $confirm->save();
            }
        }

        // Actualizamos la contraseña si hace falta
        if ($request->has('password') && $request->get('password') != null && $request->get('password') == $request->get('password_confirmation')) {
            $user->password = $request->get('password');
            $user->save();
        }

        Alert::info('Usuario actualizado correctamente.')->flash();

        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * Exporta los usuarios a un excel
     */
    public function exportUsers()
    {
        $usuarios = User::setEagerLoads([])->get();

        $usuarios = $usuarios->map(function ($user) {
            $usuario = collect($user);
            $profile = collect($user->profile);
            $joined = $usuario->merge($profile)->toArray();
            unset($joined['document_id']);
            unset($joined['created_at']);
            unset($joined['updated_at']);

            if (!isset($joined['estado_civil'])) {
                $joined['estado_civil'] = '';
            } else {
                switch ($joined['estado_civil']) {
                    case '1':
                        $estado = 'Soltero/a';
                        break;
                    case '2':
                        $estado = 'Casado/a separación de bienes';
                        break;
                    case '3':
                        $estado = 'Separado/a';
                        break;
                    case '4':
                        $estado = 'Divorciado/a';
                        break;
                    case '5':
                        $estado = 'Viudo/a';
                        break;
                    case '6':
                        $estado = 'Casado/a gananciales';
                        break;
                    default:
                        $estado = '';
                        break;
                }
                $joined['estado_civil'] = $estado;
            }

            if ($user->properties->count() > 0) {
                $joined['inversor'] = true;
            } else {
                $joined['inversor'] = false;
            }

            return $joined;
        });

        $date = date('Y-m-d');
        Excel::create('Usuarios '.$date, function ($excel) use ($usuarios) {
            $excel->sheet('Usuarios', function ($sheet) use ($usuarios) {
                $sheet->fromArray($usuarios);
            });
        })->download('xlsx');
    }

    /**
     * Crea una inversión manual
     *
     * @param Request $request
     */
    public function manualInversion(UserInversionManualRequest $request)
    {
        $user = User::find($request->user_id);
        $property = Property::find($request->property_id);
        $totalInvertir = $request->amount;

        $this->crearTransferenciaBancaria($user, $totalInvertir);

        $transaccion = $this->crearTransferenciaInversion($user, $property, $totalInvertir);

        if ($transaccion) {
            $user->properties()->save($property, ['total' => (double) $totalInvertir, 'transaction_id' => $transaccion->id]);
        }

        $appLocale = current_locale();
        localization()->setLocale($user->locale);
        $user->notify(new UserInvertirConfirmNotification($property, $totalInvertir));
        localization()->setLocale($appLocale);

        Alert::info('Inversión realizada correctamente.')->flash();

        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * @param UserAddMoneyManualRequest $request
     */
    public function addMoneyFromLemonway(UserAddMoneyManualRequest $request)
    {
        if ($request->get('amount') == 0) {
            Alert::error('La cantidad debe de ser mayor a 0')->flash();

            return redirect()->route('dashboard.users.edit', $user->id);
        }

        $cantidad = number_format($request->get('amount'), 2, '.', '');
        $user = User::find($request->get('user_id'));
        $mensaje = 'Ingreso por transferencia manual';

        $transaction = [
            'wallet'  => $user->lemonway_id,
            'type'    => 3,
            'status'  => 0,
            'fecha'   => Carbon::now()->toDateTimeString(),
            'debit'   => 0,
            'credit'  => $cantidad,
            'iban_id' => null,
            'comment' => $mensaje,
            'refund'  => 0,
            'token'   => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'TRM-'),
        ];

        $transaction = $user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $result = LemonWay::walletToWallet($walletSC, $user->wallet, $cantidad, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);

            Alert::info('Importe añadido correctamente.')->flash();
            // Tests
            //$transaction->update(['status' => 55, 'lemonway_id' => 0]);
        } catch (\Exception $e) {
            Alert::error('Error al añadir la transferencia')->flash();
            $transaction->delete();
        }

        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * @param UserAddMoneyManualRequest $request
     */
    public function addManualPromotion(UserAddMoneyManualRequest $request)
    {
        if ($request->get('amount') == 0) {
            Alert::error('La cantidad debe de ser mayor a 0')->flash();

            return redirect()->route('dashboard.users.edit', $user->id);
        }

        $cantidad = number_format($request->get('amount'), 2, '.', '');
        $user = User::find($request->get('user_id'));

        Alert::info('Importe promocional añadido correctamente.')->flash();
        $user->gifts()->create(['importe' => $cantidad]);

        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * @param $user
     * @param $amount
     */
    private function crearTransferenciaBancaria($user, $amount)
    {
        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $user->lemonway_id,
            'type'        => 3,
            'status'      => 3,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'credit'      => (double) $amount,
            'token'       => TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'MAN-'),
            'refund'      => 0,
        ];

        $user->transactions()->create($transaction);
    }

    /**
     * @param $user
     * @param $amount
     */
    private function crearTransferenciaInversion($user, $property, $amount)
    {
        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $user->lemonway_id,
            'type'        => 2,
            'status'      => 3,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => (double) $amount,
            'credit'      => 0,
            'iban_id'     => null,
            'comment'     => 'Inversión en Inmueble '.$property->id,
            'refund'      => 0,
            'token'       => TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'MAN-'),
            'extras'      => $property->id,
        ];

        $transaction = $user->transactions()->create($transaction);

        return $transaction;
    }

    /**
     * Te loguea como el usuario
     * @param User $user
     */
    public function loginAsUser(User $user)
    {
        Auth::logout();
        Auth::login($user);

        return redirect('/');
    }

    /**
     * Borrar regalos
     *
     * @param Request $request
     */
    public function borrarRegalos(User $user, Request $request)
    {
        if ($request->has('borrar') && $request->get('borrar') == 1) {
            $user->gifts()->delete();

            Alert::info('Importe de regalos borrados correctamente.')->flash();

            return redirect()->route('dashboard.users.edit', $user->id);
        }

        Alert::error('No se ha podido realizar la petición.')->flash();

        return redirect()->route('dashboard.users.edit', $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if ($user->properties->count() == 0 && $user->balance == 0) {
            // Creamos un nuevo email random para el email de lemonway
            $newEmail = 'closed-'.str_random(5).'-'.$user->email;

            $result = LemonWay::updateWalletDetails($user->wallet, ['newEmail', $newEmail]);

            $result = LemonWay::updateWalletStatus($user->wallet, 12);

            $user->delete();

            Alert::info('Usuario eliminado correctamente.')->flash();

            return redirect()->route('dashboard.users.index');
        }

        Alert::error('El usuario no puede ser eliminado porque tiene balance o inversiones')->flash();

        return redirect()->back();
    }

    public function updateEstado (Request $request, User $user)
    {
        $user->profile->update(['estado' => true]);

        if ($request->has('notific'))
            $user->notify(new UserAccreditedNotification());

        Log::info('Cuenta activa - Usuario ID: DASHBOARD' . $user->id);

        Alert::info('La cuenta del usuario fue acreditada correctamente.')->flash();

        return redirect()->back();
    }
}
