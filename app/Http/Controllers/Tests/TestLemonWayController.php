<?php

namespace App\Http\Controllers\Tests;

use App\Helpers\ImportadorExcel;
use App\Http\Controllers\Controller;
use App\Property;
use App\Traits\TargetCircleTrait;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class TestLemonWayController extends Controller
{
    use TargetCircleTrait;

    public function testCircle()
    {
        $transaction = Transaction::find(2613);

        $this->investmentEvent($transaction);
    }

    public function test()
    {
        $user = User::find(11);
        $property = Property::find(12);

        // $mes = 5;
        // $year = 2017;

        // $procesoAbonarFinalizado = true;
        // foreach ($property->users as $user) {

        //     $movimientosDividendos = $user->transactions()->where('type', 13)->where('status', 3)->whereRaw('MONTH(fecha)  = ?', $mes)->whereRaw('YEAR(fecha)  = ?', $year)->where('property_user_id', $user->pivot->id)->first();

        //     if (!$movimientosDividendos) {
        //         $procesoAbonarFinalizado = false;
        //     }
        // }

        // dump($procesoAbonarFinalizado);
        // dd();

        // $evolutions = $property->evolutionsByYear(2017);

        $fake = '4.66';

        $file = public_path('tests/excel/').'ficha evolucion.xlsx';

        $excel = ImportadorExcel::readEvolucionExcel($file, 3);

        dump($excel);

        // $result = LemonWay::updateWalletDetails($user->wallet, ['newEmail' => $user->email]);

        // $walletSC = LemonWay::getWalletDetails(null, 'SC');
        // $walletInmueble = LemonWay::getWalletDetails(null, 'INMUEBLE_12');
        // $test = LemonWay::validatePaymentWithCardId(62);
        //
        // $result = LemonWay::walletToWallet($user->wallet, $walletSC, $fake);

        // $user->invertir($property, '50.00');
        // $user->properties()->save($property, ['total' => (double) '50.00']);
        // dump($result);
        // $result = LemonWay::registerIban($user->wallet, 'Eric Lagarda Martínez', 'ES9121000418450200051332', $user->profile->direccion, $user->profile->localidad);
        //
        // $dniDocumentBuffer = Storage::disk('tests')->get('images/dni.png');

        // $dniFile = public_path('tests/').'images/dni.png';
        //
        // $bank = $eric->bankAccounts->first();

        // foreach ($eric->wallet->transactions->reverse() as $transactionData) {

        //     $eric->addTransaction($transactionData);
        // }
        // // dump($eric->wallet);
        // $result = LemonWay::checkMoneyOut(31);
        // dump($user->wallet);
        // dump($eric->wallet->transactions->reverse());

        dd();

        // $ericUser = LemonWay::setWalletUser('DA51DAEAD31', 'eric@infinety.es', 'Eric', 'Lagarda Martínez');
        // $ericWallet = LemonWay::getWalletDetails($ericUser->clientMail);

        // $returnUrl = 'https://crowdestatedev.localtunnel.me/pagos/ok'; //route('payment.ok');
        // $cancelUrl = 'https://crowdestatedev.localtunnel.me/pagos/cancelado'; //route('payment.cancel');
        // $errorUrl = 'https://crowdestatedev.localtunnel.me/pagos/error'; //route('payment.error');
        // $notifyUrl = 'https://crowdestatedev.localtunnel.me/api/v1/lemonway/payment/notify'; //route('payment.error');

        // $datosPago = ['wkToken' => 'MIPAGO0001', 'returnUrl' => $returnUrl, 'errorUrl' => $errorUrl, 'cancelUrl' => $cancelUrl, 'notifUrl' => $notifyUrl, 'comment' => 'Test first payment', 'registerCard' => 1];
        // $demo = LemonWay::createPaymentForm($ericWallet, '50.00', $datosPago, true, 'es', 'https://www.lemonway.fr/mercanet_lw.css');

        // $moneyInCardId = LemonWay::createPaymentWithCardId($eric->wallet, '1.00', 17);
        //

        // $moneyOut = LemonWay::walletMoneyToBank($eric->wallet, '50.00', $bank->document->lemonway_id);

        // dump($moneyOut);

        dd();
        // $inmueble1User = LemonWay::setWalletUser('INMUEBLE01', 'inmueble1@crowdestate.com', 'Inmueble', 'Primero', ['isCompany' => 1, 'payerOrBeneficiary' => 2]);
        // $inmueble1Wallet = LemonWay::getWalletDetails($inmueble1User);
        // dump($inmueble1Wallet);

        $dniDocumentBuffer = Storage::disk('tests')->get('images/dni.png');

        // $ericWallet->uploadFile('dni1.jpg', 0, base64_encode($dniDocumentBuffer));
        // $test = LemonWay::uploadFileToWallet($ericWallet, 'dni.jpg', 0, base64_encode($dniDocumentBuffer));
        // dump($test);

        // $ibanDocumentBuffer = Storage::disk('public')->get('tests/images/payment-domiciliacion.jpg');
        // $test = LemonWay::uploadFileToWallet($ericWallet, 'iban.jpg', 2, base64_encode($ibanDocumentBuffer));
        // dump($test);

        // $ismaelUser = LemonWay::setWalletUser('DA51DAEAD32', 'ismael@infinety.es', 'Ismael', 'Martínez');
        // $ismaelWallet = LemonWay::getWalletDetails($ismaelUser);

        // $mariaUser = LemonWay::setWalletUser('DA51DAEAD33', 'maria@infinety.es', 'Maria', 'Moreno');
        // $mariaWallet = LemonWay::getWalletDetails($mariaUser);

        // $diegoUser = LemonWay::setWalletUser('DA51DAEAD34', 'diego@infinety.es', 'Diego', 'Pertusa');
        // $diegoWallet = LemonWay::getWalletDetails($diegoUser);

        // $tomasUser = LemonWay::setWalletUser('DA51DAEAD35', 'tomas@infinety.es', 'Tomas', 'Martínez');
        // $tomasWallet = LemonWay::getWalletDetails($tomasUser);

        // $demo = LemonWay::createPaymentForm($ericWallet, '10.00', ['wkToken' => 'MIPAGO0001', 'returnUrl' => url('/return'), 'errorUrl' => url('/error'), 'cancelUrl' => url('/cancel'), 'comment' => 'Test first payment', 'registerCard' => 1]);
        // dump($demo);
        dump($ericWallet);
        // dump($ismaelWallet);
        // dump($mariaWallet);
        // dump($ismaelWallet);
        // dump($tomasWallet);
        dd();

        $yesterday = Carbon::yesterday();

        // $demo = LemonWay::getWalletsModified($yesterday->timestamp);
        // dump($ericWallet->transactions);
        // dd();

        // $balances = LemonWay::getBalances(false, '12', '12');
        // dump($balances);
        // dd();

        // $transactions = LemonWay::getTransactionsHistory($ericWallet, $yesterday->timestamp);
        // dump($transactions);
        // dd();

        // $demo = LemonWay::createPaymentForm($ericWallet, '12.00', ['wkToken' => 'MIPAGO0001', 'returnUrl' => url('/return'), 'errorUrl' => url('/error'), 'cancelUrl' => url('/cancel'), 'comment' => 'Test first payment', 'registerCard' => 1]);

        // $newIban = LemonWay::registerIban($ericWallet, 'ERIC LAGARDA MARTINEZ', 'ES9121000418450200051332', 'Calle Comedias 5', '8028 BARCELONA', 'NOLADE21STS', 'esto es un ejemplo');
        // dump($newIban);
        dump('dada');
    }
}
