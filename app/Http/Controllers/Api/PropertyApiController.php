<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Property;
use App\Transformers\FrontPropertyTransformer;
use App\Transformers\PropertyInversorsTransformer;
use Flugg\Responder\Facades\Responder;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class PropertyApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return responder()->success(Property::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $property
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $property = Property::find($id);
        if (!$property) {
            return responder()->error('No property found');
        }

        return responder()->success($property);
    }

    /**
     * @param $id
     */
    public function info($id)
    {
        if (!auth()->user()) {
            return responder()->error('Not logged');
        }

        $userProperties = auth()->user()->getListPropertiesInvertidos()->pluck('id');
        
        if ($userProperties->search(intval($id)) === false) {
            return responder()->error('You don\'t own this property');
        }

        $property = Property::find($id);

        if (!$property) {
            return responder()->error('No property found');
        }

        return Responder::transform($property, new FrontPropertyTransformer())->respond();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($property)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Get Inversores by Property
     *
     * @param  Property  $property
     * @return \Illuminate\Http\Response
     */
    public function inversores (Property $property)
    {
        return Responder::transform($property->userExtends, new PropertyInversorsTransformer())->respond();
    }

    /**
     * Export Inversors by Property
     *
     * @param  Property  $property
     */
    public function inversoresExport (Property $property)
    {
        try {
            return Excel::create('Inversiones inmueble '.$property->nombre, function ($excel) use ($property) {
                $excel->sheet('Resumen Inversiones Clientes', function ($sheet) use ($property) {
                    $users = $property->userExtends;
                    $title = 'Inversiones inmueble ' . $property->nombre;
                    $sheet->loadView('dashboard.properties.exports.csv-inversors', compact('users', 'title'));
                });
            })->download('xls');
        } catch (Exception $e) {
            report($e);
            abort(500);
        }
    }
}
