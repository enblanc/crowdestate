<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserCreateRequest;
use App\MarketOrder;
use App\PropertyUser;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Transformers\TransactionTransformer;

class UserApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return responder()->success(User::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $user = User::create($request->all());

        return responder()->success($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $user
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::find($id);
        if (!$user) {
            return responder()->error('No user found');
        }

        return responder()->success($user);
    }

    public function myTransactions()
    {
        $transactions = auth()->user()->transactions()->orderBy('created_at', 'DESC')->get();

        return responder()->success($transactions);
    }

    /**
     * Api para el listado de mis inversiones
     *
     * @return Flugg\Responder\Facades\Responder
     */
    public function misInversiones()
    {
        $datos['items']['total'] = [
            'capital_invertido'          => 0,
            'capital_comprometido'       => 0,
            'beneficios'                 => 0,
            'abonados'                   => 0,
            'promo'                      => 0,
            'dividendos'                 => 0,
            'plusvalia'                  => 0,
            'valor_inmueble'             => 0,
            'valor_inicial'              => 0,
            'revalorizacion'             => 0,
            'revalorizacion_patrimonial' => 0,
            'valor_patrimonial_total'    => 0,
            'patrimonio_total'           => 0,
            'nombre'                     => __('Todo'),
            'marketplace'                => 0,
        ];
        if (auth()->user()) {
            $user = auth()->user();

            $propiedades = $user->getListPropertiesInvertidos();

            $historial = $user->transactions()->whereIn('type', [20, 21])->get();

            $marketOrders = MarketOrder::where('executed', true)
                ->whereIn('id', $historial->pluck('extras')->toArray())
                ->orderByDesc('updated_at')
                ->get();

            foreach ($propiedades as $propiedad) {
                $capitalInvertido = PropertyUser::where('property_id', $propiedad->id)->where('user_id', $user->id)->get()->sum('total');
                $capitalComprometido = 0;

                //Si el enmieble está en explotación el capital Invertido pasa a ser captial comprometido
                if ($propiedad->property_state_id == 2) {
                    $capitalComprometido = $capitalInvertido;
                    $capitalInvertido = 0;
                }

                //Beneficios
                $beneficios = $user->transactions()->whereIn('type', [13, 14, 18])->where('extras', $propiedad->id)->get()->sum('credit');
                //Si el inmueble ya está vendido le restamos el captial Invertido
                if ($propiedad->property_state_id == 5) {
                    $beneficios -= $capitalInvertido;
                }

                //Promociones
                $promo = $user->transactions()->whereIn('type', [15, 18])->where('extras', $propiedad->id)->get()->sum('credit');

                //Dividendos
                $dividendos = $user->transactions()->whereType(13)->where('extras', $propiedad->id)->get()->sum('credit');

                //Recubido venta - invertida
                $plusvalia = 0;

                //Valor Inmueble y revalorización

                $ultimaEvolucion = null;
                if (count($propiedad->evolutions)) {
                    $ultimaEvolucion = $propiedad->evolutions->last();
                }

                $valorOriginalInmueble = $propiedad->valor_compra;
                $valorActualInmueble = $propiedad->valor_compra;

                $valorInmueble = 0;
                $revalorizacion = 0;
                $revalorizacionPatrimonial = 0;
                $revalorizacionPatrimonioPersonal = 0;
                $patrimonio = 0;

                if ($ultimaEvolucion) {
                    $valorActualInmueble = $ultimaEvolucion->valor_actual_inmueble;
                    $valorInmueble = $valorActualInmueble;

                    $revalorizacion = round($valorInmueble - $propiedad->objetivo);

                    if ($propiedad->property_state_id == 2 || $propiedad->property_state_id == 4) {
                        $revalorizacionPatrimonial = $valorActualInmueble - $propiedad->objetivo;

                        $revalorizacionPatrimonioPersonal = round((($capitalInvertido * $valorActualInmueble) / $propiedad->objetivo) - $capitalInvertido, 2);

                        $revalorizacionPatrimonioPersonal += $beneficios;
                    }

                    //
                    // dump('invertido: '.$capitalInvertido);
                    // dump('valorActualInmueble: '.$valorActualInmueble);
                    // dump('valorOriginalInmueble: '.$valorOriginalInmueble);
                    // dump('objetivo: '.$propiedad->objetivo);
                    // dump('revalorizacionPatrimonial: '.$revalorizacionPatrimonial);
                    // dump($revalorizacionPatrimonioPersonal);
                    // dd();
                }

                $porcentaje = $capitalInvertido / $propiedad->objetivo;

                $patrimonio = $beneficios + $revalorizacionPatrimonioPersonal;
                // dump('Patrimonio::: Invertido: '.$capitalInvertido.' - Comprometido: '.$capitalComprometido.' - Beneficios: '.$beneficios.' - Porcentaje: '.$porcentaje);

                // dump($revalorizacion);

                //Si el inmueble está vendido quitar el capiital invertido
                if ($propiedad->property_state_id == 5) {
                    $recibidoVenta = $user->transactions()->where('type', 14)->where('extras', $propiedad->id)->get()->sum('credit');
                    $plusvalia = $recibidoVenta - $capitalInvertido;

                    $capitalInvertido = 0;
                }

                //Marketplace
                $marketplace = 0;

                if (count($marketOrders) > 0) {
                    foreach ($marketOrders as $order) {
                        if ($order->getPropertyId() == $propiedad->id) {
                            if ($order->price != 0) {
                                if ($order->user_id == $user->id) {
                                    if ($order->price < 0) {
                                        $marketplace -= ($order->priceMoney - $order->quantity);
                                    }
                                    $marketplace += ($order->priceMoney - $order->quantity);
                                } else {
                                    if ($order->price < 0) {
                                        $marketplace += ($order->priceMoney - $order->quantity);
                                    }
                                    $marketplace -= ($order->priceMoney - $order->quantity);
                                }
                            }
                        }
                    }
                }

                $beneficios += $marketplace;

                $datos['items'][$propiedad->id] = [
                    'capital_invertido'          => $capitalInvertido,
                    'capital_comprometido'       => $capitalComprometido,
                    'beneficios'                 => $beneficios,
                    'promo'                      => $promo,
                    'dividendos'                 => $dividendos,
                    'plusvalia'                  => $plusvalia,
                    'valor_inmueble'             => $valorInmueble,
                    'valor_inicial'              => $propiedad->objetivo,
                    'revalorizacion'             => $revalorizacion,
                    'revalorizacion_patrimonial' => $revalorizacionPatrimonial,
                    'valor_patrimonial_total'    => $revalorizacionPatrimonioPersonal,
                    'patrimonio_total'           => $patrimonio,
                    'nombre'                     => $propiedad->nombre,
                    'marketplace'                => $marketplace,
                ];
            }

            foreach ($datos['items'] as $key => $data) {
                if ($key != 'total') {
                    $datos['items']['total'] = [
                        'capital_invertido'          => $datos['items']['total']['capital_invertido'] + $data['capital_invertido'],
                        'capital_comprometido'       => $datos['items']['total']['capital_comprometido'] + $data['capital_comprometido'],
                        'beneficios'                 => $datos['items']['total']['beneficios'] + $data['beneficios'],
                        'promo'                      => $datos['items']['total']['promo'] + $data['promo'],
                        'dividendos'                 => $datos['items']['total']['dividendos'] + $data['dividendos'],
                        'plusvalia'                  => $datos['items']['total']['plusvalia'] + $data['plusvalia'],
                        'valor_inmueble'             => $datos['items']['total']['valor_inmueble'] + $data['valor_inmueble'],
                        'valor_inicial'              => $datos['items']['total']['valor_inicial'] + $data['valor_inicial'],
                        'revalorizacion'             => $datos['items']['total']['revalorizacion'] + $data['revalorizacion'],
                        'revalorizacion_patrimonial' => $datos['items']['total']['revalorizacion_patrimonial'] + $data['revalorizacion_patrimonial'],
                        'valor_patrimonial_total'    => $datos['items']['total']['valor_patrimonial_total'] + $data['valor_patrimonial_total'],
                        'patrimonio_total'           => $datos['items']['total']['patrimonio_total'] + $data['patrimonio_total'],
                        'nombre'                     => __('Todo'),
                        'marketplace'                => $datos['items']['total']['marketplace'] + $data['marketplace'],
                    ];
                }
            }

            foreach ($datos['items'] as $key => $dataArray) {
                foreach ($dataArray as $k => $data) {
                    if ($k != 'nombre') {
                        switch ($k) {
                            case 'valor_inmueble':
                                $datos['items'][$key][$k] = formatThousandsNotDecimals($data);
                                break;
                            case 'valor_inicial':
                                $datos['items'][$key][$k] = formatThousandsNotDecimals($data);
                                break;
                            default:
                                $datos['items'][$key][$k] = formatThousandsNotZero($data);
                                break;
                        }
                    }
                }
            }

            return responder()->success(null, ['data' => $datos['items']]);
        }

        return responder()->error();
    }

    /**
     * Comprueba si la contraseña es igual que la del usuario
     *
     * @param Request $request
     */
    public function checkPassword(Request $request)
    {
        if (!$request->has('password')) {
            return responder()->error();
        }

        if (Hash::check($request->get('password'), auth()->user()->password)) {
            return responder()->success(null, ['data' => __('Datos correctos')]);
        }

        return responder()->error();
    }

    /**
     * @param $number
     */
    private function formatNumbers($number)
    {
        return str_replace(',00', '', number_format((float) $number, 2, ',', '.'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function transactions (Request $request, User $user)
    {
        $transactions = $user->transactions()->orderBy('created_at', 'DESC')->paginate(20);
        
        return \Responder::transform($transactions, new TransactionTransformer)->respond();
    }
}
