<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\MarketOrder;
use App\Property;
use App\PropertyUser;
use Flugg\Responder\Facades\Responder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class MarketApiController extends Controller
{
    /**
     * @param Request $request
     */
    public function getListOrders(Request $request)
    {
        // \Illuminate\Support\Facades\Auth::loginUsingId(5);
        $myPropertiesIds = auth()->user()->getListPropertiesInvertidos()->pluck('id')->toArray();
        $marketOrders = MarketOrder::with(['user', 'property'])
            ->where('status', true)
            ->where('executed', false)
            ->where('user_id', '!=', auth()->id())
            ->whereType($request->get('type'))
            // ->whereNotIn('property_id', $myInversions)
            ->whereInvested($request->get('invested'), $myPropertiesIds)
            ->whereCity($request->get('city'))
            ->whereProperty($request->get('property'))
            ->get();

        $myInversions = auth()->user()->properties->pluck('id')->toArray();

        $marketOrders = $marketOrders->filter(function ($order) use ($myInversions) {
            if ($order->type == 2) {
                return in_array($order->property_id, $myInversions);
            }

            return true;
        });
        
        $data = $this->generateDataForLists($marketOrders);

        return responder()->success(null, ['data' => $data]);

        // return Responder::transform($marketOrders, new MarketOrderTransformer)->respond();
    }

    /**
     * Get all market orders
     *
     * @return  responder
     */
    public function orders()
    {
        return responder()->success(auth()->user()->marketOrders);
    }

    /**
     * @param $inversion
     * @param Request $request
     */
    public function inversion(Request $request)
    {
        $propertyUser = PropertyUser::find($request->inversion);

        return responder()->success($propertyUser);
    }

    /**
     * @param Request $request
     */
    public function getOrder(Request $request)
    {
        if ($request->has('id')) {
            $order = MarketOrder::find($request->get('id'));

            if ($order) {
                return responder()->success($order);
            }
        }

        return responder()->error();
    }

    /**
     * @param $marketOrders
     */
    private function generateDataForLists($marketOrders)
    {
        $properties = Property::all();

        $data = collect([]);

        foreach ($marketOrders as $order) {
            $dataOrder = [
                'id'                => (int) $order->id,
                'hashid'            => (string) $order->hashId,
                'type'              => (int) $order->type,
                'typeText'          => (string) $order->typeText,
                'position'          => (int) $order->position,
                'quantity'          => (double) $order->quantity,
                'percentQuantity'   => (double) $order->percentQuantity,
                'percentQuantityOf' => (double) ($order->percentQuantity / 100),
                'price'             => (double) formatThousands($order->price),
                'pricePercentOf'    => (double) ($order->price / 100),
                'priceMoney'        => (double) $order->priceMoney,
                'status'            => (bool) $order->status,
                'link'              => (string) $order->link,
                'linkButton'        => (string) $this->generateButton($order),
                'user'              => [
                    'id'     => $order->user->id,
                    'nombre' => $order->user->nombreCompleto,
                ],
            ];

            $propertyId = $order->property_id;
            if ($order->type == 1) {
                $propertyId = $order->inversion->property_id;
            }

            $property = $properties->where('id', $propertyId)->first();

            if ($order->type == 1) {
                $dataOrder['inversion'] = [
                    'id'             => (int) $order->inversion->id,
                    'total'          => (double) $order->inversion->total,
                    'transaction_id' => (int) $order->inversion->transaction_id,
                ];
            }

            $dataOrder['property'] = $this->getPropertiesDataCache($property);

            $data->push($dataOrder);
        }

        return $data;
    }

    /**
     * @param $order
     */
    private function generateButton($order)
    {
        if ($order->type == 1) {
            return '<a href="'.$order->link.'" class="button button--small bg-success button--full nowrap">'.__('Compra ahora').'</a>';
        }

        return '<a href="'.$order->link.'" class="button button--small button--full nowrap">'.__('Vende ahora').'</a>';
    }

    /**
     * @param $property
     */
    private function getPropertiesDataCache($property)
    {
        return Cache::remember('property_market_data_'.$property->id, 5000, function () use ($property) {
            return [
                'id'        => (int) $property->id,
                'ref'       => (string) $property->ref,
                'slug'      => (string) $property->slug,
                'nombre'    => (string) $property->nombre,
                'ubicacion' => (string) $property->ubicacion,
                'direccion' => (string) $property->direccion,
                //'estado'    => (string) $property->state->nombre,
                'url'       => (string) $property->url,
                'image'     => (string) $property->getFirstMediaUrl($property->mediaCollection, 'thumb'),
                'remaining' => (string) $property->remainingDate,
                'tir'       => $property->tasa_interna_rentabilidad,
            ];
        });
    }
}
