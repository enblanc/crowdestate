<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Property;

class ExternalApiController extends Controller
{
    /**
     * @param $slug
     */
    public function property($slug)
    {
        $property = Property::whereSlug($slug)->first();

        if ($property) {
            $data = [
                'funded'    => $property->invertido,
                'total'     => $property->objetivo,
                'investors' => $property->inversoresTotales,
            ];

            return responder()->success(null, ['data' => $data]);
        }

        return responder()->error('resource_not_found');
    }
}
