<?php

namespace App\Http\Controllers\Notifications;

use App\Document;
use App\Http\Controllers\Controller;
use App\Mail\UserAccreditedMail;
use App\Notifications\AdministratorNotifications;
use App\Notifications\BankAccountConfirmed;
use App\Notifications\UserAccreditedNotification;
use App\Notifications\UserChargueBackNotification;
use App\Notifications\UserDocumentFailNotification;
use App\Notifications\UserMoneyOutCanceledNotification;
use App\Notifications\UserWireInNotification;
use App\Transaction;
use App\User;
use App\KycDocumentStatus;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Helpers\KYCLog;

class LemonWayNotificationsController extends Controller
{

    /**
     * NOTA! Notificar los usuarios
     */

    /**
     * Notification for wallet status changed from LemonWay BackOffice
     *
     * @param Request $request
     */
    public function walletStatus(Request $request)
    {
        logger('walletStatus');
        $data = $request->all();

        if (isset($data['NotifCategory'])) {
            if ($data['NotifCategory'] == 8) {
                $lemonWayId = $data['ExtId'];
                $newStatus = $data['Status'];

                $user = User::where('lemonway_id', $lemonWayId)->first();
                if ($user) {
                    if ($newStatus == 6) {
                        $user->profile->update(['estado' => true]);

                        $locale = $user->locale;
                        if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
                            localization()->setLocale($locale);
                        }

                        $user->notify(new UserAccreditedNotification());

                        Mail::to('contacto@brickstarter.com')->send(new UserAccreditedMail($user));
                        Log::info('Cuenta activa - Usuario ID: ' . $user->id);
                        Log::info($data);
                        try {
                            $user->profile->update(['acreditado' => true]);
                        } catch (\Throwable $th) {
                            logger($th->getMessage());
                        }

                    } else {
                        $user->profile->update(['estado' => false]);
                        //Notificar al usuario
                        Log::info('User Deactivated - Usuario ID: ' . $user->id);
                        Log::info($data);
                    }
                } else {
                    Log::info($data);
                    Log::info('Not user found');
                }
            }
        }
    }

    /**
     * Notification for document status changed from LemonWay BackOffice
     * 
     * Status Table Lemonway https://documentation.lemonway.com/reference/kyc-document-status
     *
     * @param Request $request
     */
    public function documentStatus(Request $request)
    {
        logger('documentStatus');
        $data = $request->all();

        if (isset($data['NotifCategory'])) {

            if ($data['NotifCategory'] == 9) {

                $lemonWayId = $data['ExtId'];

                $docId      = $data['DocId'];

                $docType    = $data['DocType'];

                $newStatus  = $data['Status'];

                $user = User::where('lemonway_id', $lemonWayId)->first();

                $KYCLog = new KYCLog($request->all());

                if (!$user) {
                    Log::info('Not user found');

                    Log::info($data);

                    $KYCLog->setComment('Not user found');

                    $KYCLog->save();

                    return;
                }

                $locale = $user->locale;
                if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
                    localization()->setLocale($locale);
                }

                $document = Document::where('lemonway_id', $docId)->where('user_id', $user->id)->first();

                if ($document) {

                    $document->status = $newStatus;

                    $document->save();

                    $dataMailSatus = [];

                    switch ($newStatus) {
                        case 0 : {
                            // Document put on hold, waiting for another document
                            $dataMailSatus = [
                                'subject'   => trans('emails.account_document_fail.subject'),
                                'greeting' => trans('emails.account_document_fail.greetings', ['name' => $user->nombre]),
                                'message1'  => trans('emails.account_document_fail.message1'),
                                'message2'  => trans('emails.account_document_fail.message2', ['estado' => $document->statusText]),
                                'message3'  => trans('emails.account_document_fail.message3'),
                                'action'    => trans('emails.account_document_fail.action'),
                                'accepted'  => false,
                            ];
                            break;
                        }
                        case 1 : {
                            // Received (waiting for manual verification). Default Status
                            $dataMailSatus = [
                                'subject'   => trans('emails.account_document_fail.subject'),
                                'greeting' => trans('emails.account_document_fail.greetings', ['name' => $user->nombre]),
                                'message1'  => trans('emails.account_document_fail.message1'),
                                'message2'  => trans('emails.account_document_fail.message2', ['estado' => $document->statusText]),
                                'message3'  => trans('emails.account_document_fail.message3'),
                                'action'    => trans('emails.account_document_fail.action'),
                                'accepted'  => false,
                            ];
                            break;
                        }
                        case 2 : {
                            // Received (waiting for manual verification). Default Status
                            $dataMailSatus = [
                                'subject' => trans('emails.document_fail.status_2_accepted.subject'),
                                'greeting' => trans('emails.document_fail.status_2_accepted.greetings', ['name' => $user->nombre]),
                                'message1' => trans('emails.document_fail.status_2_accepted.message1'),
                                'message2' => trans('emails.document_fail.status_2_accepted.message2'),
                                'message3' => trans('emails.document_fail.status_2_accepted.message3'),
                                'message4' => trans('emails.document_fail.status_2_accepted.message4'),
                                'action' => trans('emails.document_fail.status_2_accepted.action'),
                                'accepted' => true,
                            ];

                            break;
                        }
                        case 3 : {
                            // 	Rejected
                            $dataMailSatus = [
                                'subject' => trans('emails.document_fail.status_3_rejected.subject'),
                                'greeting' => trans('emails.document_fail.status_3_rejected.greetings', ['name' => $user->nombre]),
                                'message1' => trans('emails.document_fail.status_3_rejected.message1'),
                                'message2' => trans('emails.document_fail.status_3_rejected.message2'),
                                'message3' => trans('emails.document_fail.status_3_rejected.message3'),
                                'message4' => trans('emails.document_fail.status_3_rejected.message4'),
                                'action' => trans('emails.document_fail.status_3_rejected.action'),
                                'accepted' => false,
                            ];
                            break;
                        }
                        case 4 : {
                            // Rejected. Document unreadable (Cropped, blur, glare…)
                            $dataMailSatus = [
                                'subject' => trans('emails.document_fail.status_4_rejected_doc_unreadable.subject'),
                                'greeting' => trans('emails.document_fail.status_4_rejected_doc_unreadable.greetings', ['name' => $user->nombre]),
                                'message1' => trans('emails.document_fail.status_4_rejected_doc_unreadable.message1'),
                                'message2' => trans('emails.document_fail.status_4_rejected_doc_unreadable.message2'),
                                'message3' => trans('emails.document_fail.status_4_rejected_doc_unreadable.message3'),
                                'message4' => trans('emails.document_fail.status_4_rejected_doc_unreadable.message4'),
                                'action' => trans('emails.document_fail.status_4_rejected_doc_unreadable.action'),
                                'accepted' => false,
                            ];
                            break;
                        }
                        case 5 : {
                            // 	Rejected. Document expired (Expiration Date is passed)
                            $dataMailSatus = [
                                'subject' => trans('emails.document_fail.status_5_rejected_doc_expired.subject'),
                                'greeting' => trans('emails.document_fail.status_5_rejected_doc_expired.greetings', ['name' => $user->nombre]),
                                'message1' => trans('emails.document_fail.status_5_rejected_doc_expired.message1'),
                                'message2' => trans('emails.document_fail.status_5_rejected_doc_expired.message2'),
                                'message3' => trans('emails.document_fail.status_5_rejected_doc_expired.message3'),
                                'message4' => trans('emails.document_fail.status_5_rejected_doc_expired.message4'),
                                'action' => trans('emails.document_fail.status_5_rejected_doc_expired.action'),
                                'accepted' => false,
                            ];
                            break;
                        }
                        case 6 : {
                            // Rejected. Wrong type (Document not accepted)
                            $dataMailSatus = [
                                'subject' => trans('emails.document_fail.status_6_rejected_wrong_type.subject'),
                                'greetings' => trans('emails.document_fail.status_6_rejected_wrong_type.greetings', ['name' => $user->nombre]),
                                'message1' => trans('emails.document_fail.status_6_rejected_wrong_type.message1'),
                                'message2' => trans('emails.document_fail.status_6_rejected_wrong_type.message2'),
                                'message3' => trans('emails.document_fail.status_6_rejected_wrong_type.message3'),
                                'message4' => trans('emails.document_fail.status_6_rejected_wrong_type.message4'),
                                'action' => trans('emails.document_fail.status_6_rejected_wrong_type.action'),
                                'accepted' => false,
                            ];
                            break;
                        }
                        case 7 : {
                            // Rejected. Wrong name (Name not matching user information)
                            $dataMailSatus = [
                                'subject' => trans('emails.document_fail.status_7_rejected_wrong_name.subject'),
                                'greeting' => trans('emails.document_fail.status_7_rejected_wrong_name.greetings', ['name' => $user->nombre]),
                                'message1' => trans('emails.document_fail.status_7_rejected_wrong_name.message1'),
                                'message2' => trans('emails.document_fail.status_7_rejected_wrong_name.message2'),
                                'message3' => trans('emails.document_fail.status_7_rejected_wrong_name.message3'),
                                'message4' => trans('emails.document_fail.status_7_rejected_wrong_name.message4'),
                                'action' => trans('emails.document_fail.status_7_rejected_wrong_name.action'),
                                'accepted' => false,
                            ];
                            break;
                        }
                        case 8 : {
                            // 	Rejected. Duplicated document. The same document is already on file
                            $dataMailSatus = [
                                'subject'   => trans('emails.document_fail.status_8_rejected_duplicate_document.subject'),
                                'greetings' => trans('emails.document_fail.status_8_rejected_duplicate_document.greetings', ['name' => $user->nombre]),
                                'message1'  => trans('emails.document_fail.status_8_rejected_duplicate_document.message1'),
                                'message2'  => trans('emails.document_fail.status_8_rejected_duplicate_document.message2'),
                                'message3'  => trans('emails.document_fail.status_8_rejected_duplicate_document.message3'),
                                'message4'  => trans('emails.document_fail.status_8_rejected_duplicate_document.message4'),
                                'action'    => trans('emails.document_fail.status_8_rejected_duplicate_document.action'),
                                'accepted'  => false,
                            ];
                            break;
                        } default : {
                            return 'DEFAULT';
                        }
                    }

                    //Por el momento solo se desea notificar por correo los estados > 2
                    if ($newStatus != 2) {
                        $this->sendStatusNotification($user, $document, $dataMailSatus, $data);
                        
                        //Detener la función para todos los estados excepto el 2
                        return;
                    }

                    //Activamos al usuario si el documento es un DNI y además está aceptado
                    logger([
                        $docType == 0,
                        $user->profile->estado,
                        $user->profile->estado == false,
                        $user->profile->estado === false,
                        $user->dnisCounts() > 0,
                        $user->wallet->STATUS,
                    ]);

                    if ($docType == 0 && $user->profile->estado == false && $user->dnisCounts() > 0) {
                        // $user->profile->estado == false &&
                        $status = $user->wallet->STATUS;
                        
                        logger('entro');
                        //if ($status == 6 || $status == '6') {
                            $user->profile->update(['estado' => true]);

                            $user->notify(new UserAccreditedNotification());

                            Log::info('Cuenta activa - Usuario ID: -' . $user->id);

                            Log::info($data);
                        //}
                    }

                    //Comprueba si el documento es una cuenta bancaria
                    if ($docType == '2' || $docType == 2)
                    {
                        $bankAccount = $user->bankAccounts->where('document_id', $document->id)->first();

                        if (!$bankAccount) {
                            Log::info('Bank Account not found');

                            Log::info($data);

                            $KYCLog->setComment('Bank Account not found');

                            $KYCLog->save();

                            return;
                        }

                        $bankAccount->estado = 1;

                        $bankAccount->save();

                        $user->notify(new BankAccountConfirmed());

                        Log::info('Banco activo - Usuario ID: ' . $user->id);

                        $KYCLog->setComment('Banco activo - Usuario ID: ' . $user->id);
                    } else {
                        $KYCLog->setComment('Document updated - Usuario ID: ' . $user->id);
                    }

                    //Notificar al usuario
                    Log::info('Document updated - Usuario ID: ' . $user->id);

                    Log::info($data);
                } else {
                    Log::info('Not document found');

                    Log::info($data);

                    $KYCLog->setComment('Not document found');
                }

                $KYCLog->save();
            }
        }
    }

    /**
     * Send Notifiaction to User
     *
     * @param User $user
     * @param Document $document
     * @param [type] $dataMailSatus
     * @param [type] $data
     * @return void
     */
    public function sendStatusNotification(User $user, Document $document, $dataMailSatus, $data)
    {
        try {
            $user->notify(new UserDocumentFailNotification($dataMailSatus));
    
            if ($admin = User::find(2))
            {
                $admin->notify(new AdministratorNotifications($user, 'Error documento', $document->statusText));
            }
    
            Log::info('Documento erroneo - Usuario ID: ' . $user->id);
    
            Log::info($data);
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    /**
     * Notification for new wire aceppted from LemonWay BackOffice
     *
     * @param Request $request
     */
    public function moneyInWireRecieved(Request $request)
    {
        $data = $request->all();

        if (isset($data['NotifCategory'])) {
            if ($data['NotifCategory'] == 10) {
                $lemonWayId = $data['ExtId'];
                $internalId = $data['IntId'];
                $transactionId = $data['IdTransaction'];
                $dateNotification = $data['NotifDate'];
                $amount = $data['Amount'];
                $statusWire = $data['Status'];

                switch ($statusWire) {
                    case '0':
                        $status = 3;
                        break;
                    case '4':
                        $status = 0;
                        break;
                    case '6':
                        $status = 4;
                        break;
                    default:
                        $status = 4;
                        break;
                }

                $user = User::where('lemonway_id', $lemonWayId)->first();
                if ($user) {
                    $locale = $user->locale;
                    if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
                        localization()->setLocale($locale);
                    }

                    $transaction = [
                        'lemonway_id' => $transactionId,
                        'wallet' => $user->lemonway_id,
                        'type' => 3,
                        'status' => $status,
                        'fecha' => Carbon::parse($dateNotification, 'Europe/Paris')->toDateTimeString(),
                        'credit' => (double)$amount,
                        'refund' => 0,
                    ];

                    $user->transactions()->create($transaction);
                    //Notificar al usuario
                    $user->notify(new UserWireInNotification($amount));
                    Log::info('Transaction successfull - Usuario ID: ' . $user->id);
                } else {
                    Log::info('Not user found');
                    Log::info($data);
                }
            }
        }
    }

    /**
     * @param Request $request
     */
    public function chargeBack(Request $request)
    {
        $data = $request->all();

        if (isset($data['NotifCategory'])) {
            if ($data['NotifCategory'] == 14) {
                $lemonWayId = $data['ExtId'];
                $transactionId = $data['IdTransaction'];
                $amount = $data['Amount'];
                $status = $data['Status'];

                $transaction = Transaction::lemonWayId($lemonWayId)->first();
                $user = User::where('lemonway_id', $lemonWayId)->first();

                if ($transaction && $user) {
                    $locale = $user->locale;
                    if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
                        localization()->setLocale($locale);
                    }

                    $transaction->status = $status;
                    $transactions->credit = $amount;
                    $transaction->save();

                    //Notificar al usuario
                    $user->notify(new UserChargueBackNotification());
                    Log::info('Transaction chargeback');
                } else {
                    Log::info('Not transaction or user found');
                    Log::info($data);
                }
            }
        }
    }

    /**
     * Notification for  Money Out canceled from LemonWay BackOffice
     *
     * @param Request $request
     */
    public function moneyOutCanceled(Request $request)
    {
        $data = $request->all();

        if (isset($data['NotifCategory'])) {
            if ($data['NotifCategory'] == 15) {
                $lemonWayId = $data['ExtId'];
                $transactionId = $data['IdTransaction'];
                $amount = $data['Amount'];
                $status = $data['Status'];

                $transaction = Transaction::lemonWayId($lemonWayId)->first();
                $user = User::where('lemonway_id', $lemonWayId)->first();

                if ($transaction && $user) {
                    $locale = $user->locale;
                    if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
                        localization()->setLocale($locale);
                    }

                    $transaction->status = $status;
                    $transactions->credit = $amount;
                    $transaction->save();
                    //Notificar al usuario

                    $user->notify(new UserMoneyOutCanceledNotification());
                    Log::info('Transaction canceled - Usuario ID: ' . $user->id);
                } else {
                    Log::info('Not transaction or user found');
                    Log::info($data);
                }
            }
        }
    }

    /**
     * @param Request $request
     */
    public function paymentNotification(Request $request)
    {
        Log::info('Notificación pago');
        Log::info($request->all());
    }
}
