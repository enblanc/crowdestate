<?php

namespace App\Http\Controllers;

use App\Helpers\ImportadorExcel;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'confirmed']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function checkResumenExcel()
    {
        $file = public_path('tests/excel/ficha_resumen.xlsx');
        $result = ImportadorExcel::readResumenExcel($file);

        dump($result);
    }


    public function checkEvolucionExcel()
    {
        $file = public_path('tests/excel/ficha evolucion.xlsx');

        $now = Carbon::now('Europe/Madrid');
        $mes = $now->month -1;
        $result = ImportadorExcel::readEvolucionExcel($file, $mes);

        // $result->tipo = 'seguimiento';
        // $result->fecha = $now->format('Y-m-d');


        // $property = \App\Property::find(1);
        // $property->evolutions()->create((array) $result);

        dump($result);
    }
}
