<?php

namespace App\Http\Controllers\Dev;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Property;
use App\AutoInvest;

class AutoInvestPropertyDevController extends Controller
{
    protected $verificaction = true;

    public function testByProperty (Property $property)
    {
        $properties = Property::orderBy('property_state_id')->get();

        $exceptions = $this->checkProperty($property);

        $autoinvests = $this->getAutoinvestByProperty($property);

        $investments = collect();

        foreach ($autoinvests as $autoinvest) { 
            if (!$this->checkUser($autoinvest->user))
                $investments->push((object)[
                    'autoinvest' => $autoinvest,
                    'user' => $autoinvest->user,
                    'message' => 'Usuario no valido',
                    'status' => false
                ]);
            else if (!$amount = $this->checkAutoInvest($autoinvest, $property))
                $investments->push((object)[
                    'autoinvest' => $autoinvest,
                    'user' => $autoinvest->user,
                    'message' => 'El precio no cumple con el usuario',
                    'status' => false
                ]);
            else
                $investments->push((object)[
                    'autoinvest' => $autoinvest,
                    'user' => $autoinvest->user,
                    'message' => 'Puede invertir '. $amount,
                    'status' => true
                ]);
        }

        return view('dev.autoinvest', ['exceptions' => $exceptions, 'investments' => $investments, 'property' => $property, 'properties' => $properties]);
    }

    /**
     * Verificamos que la propiedad cumpla con los estados requeridos
     * @return boolean
     */
    protected function checkProperty  (Property $property)
    {
        $errors = [];

        if (!$property->publicado) {
            $errors['publicado'] = 'La propiedad no se encuentra publicada.';
        }

        if (!$property->estado) {
            $errors['estado'] = 'La propiedad se encuentra oculta.';
        }
        
        if ($property->private) {
            $errors['private'] = 'La propiedad se encuentra privada.';
        }

        if ($property->property_state_id != 2) {
            $errors['property_state_id'] = 'El estado de la propiedad es incorecto.';
        }

        if ($property->porcentajeCompletado == 100) {
            $errors['porcentaje_completado'] = 'El porcentaje de la propiedad ya alzancó el 100%';
        }

        if ($property->remainingDays < 1) {
            $errors['remaining_days'] = 'Días restantes < 1';
        }
        
        return $errors;
    }

    /**
     * Verifica que el usuario cumpla con los requisitos de su perfil.
     *
     * @param User $user
     * @return void
     */
    protected function checkUser ($user)
    {
        if (!$this->verificaction)
            return true;

        if ($user->confirmado && $user->activado && $user->hasLemonAccount()) {
            return true;
        }

        if (!$user->activado && pago_sin_confirmar() && $user->hasLemonAccount()) {
            return true;
        }

        return false;
    }

    /**
     * Verificamos que el autoinvest del usuario cumple con la propiedad
     *
     * @param AutoInvest $autoinvest
     * @return boolean
     */
    protected function checkAutoInvest ($autoinvest, $property)
    {
        $invertidoTotal = ($property->invertido + $autoinvest->max_investment);
        if ($invertidoTotal <= $property->objetivo) {
            if ($autoinvest->portfolio_size >= $autoinvest->investment + $autoinvest->max_investment)
                return $autoinvest->max_investment;
        }
        
        // min_investment
        $invertidoTotal = ($property->invertido + $autoinvest->min_investment);
        if ($invertidoTotal <= $property->objetivo) {
            if ($autoinvest->portfolio_size >= $autoinvest->investment + $autoinvest->min_investment)
                return $autoinvest->min_investment;
        }

        return false;
    }

    protected function  getAutoinvestByProperty (Property $property)
    {
        return AutoInvest::canInvert()
            ->byPropertyType($property->property_type_id)
            ->byDeveloper($property->developer_id)
            ->byLocation($property->ubicacion)
            ->byTir($property->tasa_interna_rentabilidad)
            ->whereDoesntHave('orders', function ($query) use ($property) {
                return $query->where('property_id', $property->id);
            })
            ->get();
    }
}
