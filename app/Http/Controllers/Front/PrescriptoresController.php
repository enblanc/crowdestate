<?php

namespace App\Http\Controllers\Front;

use App\Helpers\SeoHelpers;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PrescriptoresController extends Controller
{
    //
    /**
     * @param Request $request
     */
    public function send(Request $request)
    {
        $this->validate($request, [
            'nombre'      => 'required',
            'email'       => 'email',
            'comentarios' => 'required',
        ]);

        $nombre = $request->input('nombre');
        $comentarios = $request->input('comentarios');
        $email = $request->input('email');
        $telefono = $request->input('telefono');

        Mail::to('contacto@brickstarter.com')->send(new ContactMail($nombre, $email, $telefono, $comentarios, 'prescriptor'));

        $ok = true;

        SeoHelpers::setSEO(__('Prescriptor'), __('Ser prescriptor de BrickStarter'));

        return view('front.contact.prescriptores', compact('ok', 'nombre'));
    }
}
