<?php

namespace App\Http\Controllers\Front;

use App\Helpers\SeoHelpers;
use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\AcceptOrderRequest;
use App\MarketOrder;
use App\Notifications\MarketPlaceOrderBought;
use App\Notifications\MarketPlaceOrderSold;
use App\Property;
use App\PropertyUser;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;
use Infinety\LemonWay\Facades\LemonWay;

class MarketController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'accredited']);
    }

    /**
     * Ver mi mercado
     *
     * @return  \Illuminate\View\View
     */
    public function grid()
    {
        SeoHelpers::setSEO(__('Marketplace'), __('Our Marketplace'));

        $marketOrders = MarketOrder::where('status', true)
            ->where('executed', false)
            ->where('user_id', '!=', auth()->id())
            ->get();

        $myInversions = auth()->user()->properties->pluck('id')->toArray();

        $marketOrders = $marketOrders->filter(function ($order) use ($myInversions) {
            if ($order->type == 2) {
                return in_array($order->property_id, $myInversions);
            }

            return true;
        });

        $propertiesIds = $marketOrders->pluck('property_id');
        $propertiesUsersIds = $marketOrders->pluck('property_user_id');
        $propertiesFromInversions = PropertyUser::whereIn('id', $propertiesUsersIds)->get()
            ->pluck('property_id')
            ->filter()
            ->unique();

        $propertiesIds = $propertiesIds->merge($propertiesFromInversions)->filter();

        $properties = Property::where('property_state_id', 4)
            ->whereIn('id', $propertiesIds->toArray())
            ->orderByRaw('- orden DESC')
            ->get()
            ->filter(function ($item) {
                return $item->publicado === true && $item->private === false;
            });

        $cities = $properties->pluck('ubicacion')->unique()->toArray();

        return view('front/marketplace/market-list', compact('properties', 'cities'));
    }

    /**
     * @param $token
     */
    public function show($token)
    {
        $order = MarketOrder::where('executed', false)
            ->where('user_id', '!=', auth()->id())
            ->get()
            ->filter(function ($order) use ($token) {
                return $order->hashId == $token;
            })->first();

        if (!$order) {
            return redirect()->route('front.market.list');
        }

        if ($order) {
            SeoHelpers::setSEO(__('Orden de compra'), __('Our Marketplace'));
        } else {
            SeoHelpers::setSEO(__('Orden de venta'), __('Our Marketplace'));
        }

        return view('front.marketplace.order', compact('order'));
    }

    /**
     * @param AcceptOrderRequest $request
     */
    public function acceptOrder(AcceptOrderRequest $request)
    {
        $order = MarketOrder::find($request->order);

        if (!$order) {
            return redirect()->back()->withErrors([__('No se encuentra la orden de mercado')]);
        }

        DB::beginTransaction();

        if ($order->type == 1) {
            //Compra
            $oldInversion = $order->inversion;
            try {
              $oldInversion->total = (double) $oldInversion->total - (double) $order->quantity;
              $oldInversion->save();
            } catch (\Throwable $th) {
              $oldInversion->total = $oldInversion->total - $order->quantity;
              $oldInversion->save();                        
              logger([
                'oldInversion->total'       => $oldInversion->total,
                '$order->quantity'          => $order->quantity,
                '(double) $order->quantity' => (double) $order->quantity
              ]);
            }

            $property = $order->inversion->property;

            $success = $this->sendMoneyBetweenUsers(auth()->user(), $order->user, $order, $property);

            if ($success) {
                $order->execute();

                auth()->user()->notify(new MarketPlaceOrderBought($order));

                $appLocale = current_locale();
                localization()->setLocale($order->user->locale);
                $order->user->notify(new MarketPlaceOrderSold($order));
                localization()->setLocale($appLocale);

                DB::commit();

                return redirect()->route('front.market.successfull');
            } else {
                DB::rollBack();
            }
        }

        if ($order->type == 2) {
            //Vender
            $puede = false;

            //Comprobamos si tiene al menos una inversión que cumpla los requisitos
            $inversion = PropertyUser::where('user_id', auth()->id())
                ->where('property_id', $order->getPropertyId())
                ->where('total', '>=', $order->quantity)
                ->first();

            if ($inversion) {
                $puede = true;

                $inversion->total = $inversion->total - (double) $order->quantity;
                $inversion->save();
            }

            if (!$puede) {
                //Si no tiene una sola inversión que cumpla, comprobamos si VARIAS inversiones cumplen para la venta.

                $inversiones = PropertyUser::where('user_id', auth()->id())
                    ->where('property_id', $order->getPropertyId())
                    ->orderByDesc('total')
                    ->get();

                $suma = 0;
                $inversionsToSell = collect([]);

                foreach ($inversiones as $inversion) {
                    if ($inversion->total > 0) {
                        $suma += $inversion->total;
                        $inversionsToSell->push($inversion);
                        if ($suma >= $order->quantity) {
                            break;
                        }
                    }
                }

                if ($suma >= $order->quantity) {
                    $puede = true;
                    $totalARestar = $order->quantity;

                    foreach ($inversionsToSell as $inversion) {
                        $resultado = $inversion->total - $totalARestar;

                        /*********************************************************************************
                         * Si el resultado es menor de 0 quiere decir que se restará de más inversiones.
                         * Así que hay que restar todo el importe de la inversión.
                         *
                         * En caso contrario se quita solo el totalARestar que queda
                         *********************************************************************************/

                        if ($resultado < 0) {
                            $restar = $inversion->total;
                        } else {
                            $restar = $totalARestar;
                        }

                        $totalARestar -= $restar;

                        $inversion->total = $inversion->total - $restar;
                        $inversion->save();
                    }
                }
            }

            if (!$puede) {
                DB::rollBack();

                return redirect()->back()->withErrors([__('No tienes ninguna inversión que cumpla los requisitos para poder vender esta orden')]);
            }

            if ($order->user->balance < $order->priceMoney) {
                DB::rollBack();

                return redirect()->back()->withErrors([__('El comprador no tiene fondos suficientes para procesar la operación')]);
            }

            $success = $this->sendMoneyBetweenUsers($order->user, auth()->user(), $order, $inversion->property);

            if ($success) {
                $order->execute();

                auth()->user()->notify(new MarketPlaceOrderSold($order));

                $appLocale = current_locale();
                localization()->setLocale($order->user->locale);
                $order->user->notify(new MarketPlaceOrderBought($order));
                localization()->setLocale($appLocale);

                DB::commit();

                return redirect()->route('front.market.successfull');
            } else {
                DB::rollBack();
            }
        }

        return redirect()->back()->withErrors([__('Error al procesar los datos. Por favor, contacte con nosotros.')]);
    }

    /**
     * @param $sender
     * @param $reciever
     * @param $money
     */
    private function sendMoneyBetweenUsers(User $sender, User $reciever, MarketOrder $order, Property $property)
    {
        $totalMoney = number_format($order->priceMoney, 2, '.', '');

        $mensaje = __('Marketplace order #').$order->id;

        try {
            $transactionSender = $this->generateSenderTransaction($sender, $order, $totalMoney, $mensaje);
            $transactionReciever = $this->generateRecieverTransaction($reciever, $order, $totalMoney, $mensaje);
        } catch (\Exception $e) {
            return false;
        }

        try {
            $result = LemonWay::walletToWallet($sender->wallet, $reciever->wallet, $totalMoney, $mensaje);
            $transactionSender->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
            $transactionReciever->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);

            $sender->properties()->save($property, [
                'total'          => (double) $order->quantity,
                'transaction_id' => $transactionReciever->id,
            ]);

            $order->setTransaction($transactionReciever->id);

            return true;
        } catch (LemonWayExceptions $e) {
            logger('Línea 150 - '.$e->getMessage());

            $transactionSender->update(['status' => 4]);
            $transactionReciever->update(['status' => 4]);

            return false;
        }
    }

    /**
     * Generate transaction for reciever
     *
     * @param   \App\User         $reciever
     * @param   \App\MarketOrder  $order
     * @param   double       $total
     * @param   string       $message
     *
     * @return  \App\Transaction
     */
    private function generateSenderTransaction(User $sender, MarketOrder $order, $total, $message)
    {
        $transaction = [
            'lemonway_id'      => 0,
            'wallet'           => $sender->lemonway_id,
            'type'             => 20,
            'status'           => 0,
            'fecha'            => Carbon::now()->toDateTimeString(),
            'debit'            => 0,
            'credit'           => $total,
            'iban_id'          => null,
            'comment'          => $message,
            'refund'           => 0,
            'token'            => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'MKO-'),
            'extras'           => $order->id,
            'property_user_id' => $order->getPropertyId(),
        ];

        return $sender->transactions()->create($transaction);
    }

    /**
     * Generate transaction for reciever
     *
     * @param   \App\User         $reciever
     * @param   \App\MarketOrder  $order
     * @param   double       $total
     * @param   string       $message
     *
     * @return  \App\Transaction
     */
    private function generateRecieverTransaction(User $reciever, MarketOrder $order, $total, $message)
    {
        $transaction = [
            'lemonway_id'      => 0,
            'wallet'           => $reciever->lemonway_id,
            'type'             => 21,
            'status'           => 0,
            'fecha'            => Carbon::now()->toDateTimeString(),
            'debit'            => $total,
            'credit'           => 0,
            'iban_id'          => null,
            'comment'          => $message,
            'refund'           => 0,
            'token'            => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'MKO-'),
            'extras'           => $order->id,
            'property_user_id' => $order->getPropertyId(),
        ];

        return $reciever->transactions()->create($transaction);
    }
}
