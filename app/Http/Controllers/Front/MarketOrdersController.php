<?php

namespace App\Http\Controllers\Front;

use App\Helpers\SeoHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserMarketOrderRequest;
use App\MarketOrder;
use App\Notifications\MarketPlaceOrderPlaced;
use App\Notifications\MarketPlaceOrderUpdated;
use App\Property;
use App\PropertyUser;
use Illuminate\Http\Request;
use Carbon\Carbon;

class MarketOrdersController extends Controller
{
    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'accredited']);
    }

    /**
     * Ver mi mercado
     *
     * @return  \Illuminate\View\View
     */
    public function verMiMercado()
    {
        $orders = auth()->user()->marketOrders;

        $inversions = auth()->user()->properties()->where('property_state_id', 4)->get();

        $availables = Property::where('property_state_id', 4)
            ->orderByRaw('- orden DESC')
            ->get()
            ->filter(function ($item) {
                return $item->publicado === true && $item->private === false;
            });

        SeoHelpers::setSEO(__('Mi Mercado'), __('Mis ordenes de compra/venta'));

        return view('front/dashboard/mi-mercado', compact('inversions', 'availables', 'orders'));
    }

    /**
     * Agrega una nueva orden de mercado
     *
     * @param   \App\Http\Requests\User\UserMarketOrderRequest  $request
     *
     * @return  \Illuminate\View\View
     */
    public function nuevaOrdenMercado(UserMarketOrderRequest $request)
    {
        $type = $request->get('type');
        $quantity = (double) $request->get('quantity');
        $price = (double) $request->get('price');

        if ($quantity < 0) {
            session()->flash('order_error', __('Ocurrió un error al crear la Orden'));
            return redirect()->back();
        }
        
        $status = ($request->has('status') && $request->get('status') == 'true') ? true : false;

        $position = MarketOrder::max('id') + 1;
        $order = new MarketOrder;
        $order->type = $type;
        $order->position = $position;
        $order->user_id = auth()->user()->id;
        
        if ($type == 1)
        {
            $propertyUser = PropertyUser::find($request->get('inversion'));

            $quantity_available = (double) $propertyUser->total;
            
            if ($quantity_available < 0 || $quantity_available < $quantity) {
                session()->flash('order_error', __('Ocurrió un error al crear la Orden'));
                return redirect()->back();
            }
    
            $order->property_user_id = $propertyUser->id;
        }

        if ($type == 2) {
            $order->property_id = Property::find($request->get('property'))->id;
        }

        $order->quantity = $quantity;
        $order->price = $price;
        $order->status = $status;
        $order->save();

        auth()->user()->notify(new MarketPlaceOrderPlaced($order));

        session()->flash('order_success', __('Orden añadida correctamente'));

        if ($type == 1) {
            $job = (new \App\Jobs\AutoInvestMarketOrderJob($order))->delay(Carbon::now()->addMinutes(2));
            dispatch($job);
        }

        return redirect()->route('panel.user.marketplace.orders');
    }

    /**
     * @param Request $request
     */
    public function updateOrdenMercado(Request $request)
    {
        if (!$order = MarketOrder::find($request->get('id'))) {
            session('errors', true);
            
            return redirect()->back();
        }
        
        $quantity = (double) $request->get('quantity');

        if ($order->type == 1) {

            $quantity_available = $order->inversion ? (double) $order->inversion->total : 0;
                
            if ($quantity < 0 || $quantity_available < $quantity || $quantity_available <= 0) {
                session()->flash('order_error', __('Ocurrió un error al crear la Orden'));
                return redirect()->back();
            }
        }

        $order->update([
            'quantity' => $quantity,
            'status' => ($request->has('status') && $request->get('status') == 'true') ? true : false,
            'price' => (double) $request->get('price'),
        ]);

        auth()->user()->notify(new MarketPlaceOrderUpdated($order));

        if ($order->type == 1) {
            $job = (new \App\Jobs\AutoInvestMarketOrderJob($order))->delay(Carbon::now()->addMinutes(2));
            dispatch($job);
        }
        
        session()->flash('order_success', __('Orden actualizada correctamente'));

        return redirect()->route('panel.user.marketplace.orders');
    }

    /**
     * @param MarketOrder $order
     */
    public function deleteOrdenMercado(Request $request)
    {
        $order = MarketOrder::find($request->get('id'));
        if (!$order) {
            session('errors', true);

            return redirect()->back();
        }

        if (auth()->id() != $order->user_id) {
            abort(403, 'Access denied');
        }

        $order->delete();
        session()->flash('order_success', __('Orden eliminada correctamente'));

        return redirect()->route('panel.user.marketplace.orders');
    }

    /**
     * Ver histórico
     *
     * @return  view
     */
    public function verHistorico()
    {
        $historial = auth()->user()->transactions()->whereIn('type', [20, 21])->get();

        $historical = MarketOrder::where('executed', true)
            ->whereIn('id', $historial->pluck('extras')->toArray())
            ->orderByDesc('updated_at')
            ->get();

        return view('front/dashboard/mercado-historico', compact('historical'));
    }
}
