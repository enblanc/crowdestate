<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\AutoInvestRequest;
use App\Helpers\SeoHelpers;
use App\AutoInvest;
use App\PropertyType;
use App\Developer;
use App\Property;

use Carbon\Carbon;

class MarketAutoinvestController extends Controller
{

    /**
     * Instantiate a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'accredited']);
    }

    public function index () {

        SeoHelpers::setSEO(__('Auto Invest'), __('Auto Invest'));

        $types = PropertyType::all();

        $developers = Developer::all();

        $autoinvest = auth()->user()->autoinvest;
        
        $autoinvest = $autoinvest ? $autoinvest : new AutoInvest();

        $locations = Property::all()->pluck('ubicacion')->unique();

        return view('front/dashboard/autoinvest', compact('types', 'developers', 'autoinvest', 'locations'));
    }

    /**
     * Actualiza o crea el autoinvest del usuario
     *
     * @param   AutoInvestRequest  $request
     *
     * @return  redirect
     */
    public function updateOrCreate (AutoInvestRequest $request)
    {
        $user = auth()->user();

        $prices = explode(",", $request->prices);

        $request['min_investment'] = $prices[0];

        $request['max_investment'] = $prices[1];

        $user->autoinvest()->updateOrCreate([], $request->only([
            'min_investment',
            'max_investment',
            'portfolio_size',
            'period',
            'country',
            'location',
            'city',
            'developer_id',
            'property_type_id',
            'tir',
            'risk_scoring',
            'status'
        ]));

        $user->autoinvest->status = $request->has('status') ? true : false;

        $user->autoinvest->save();

        return redirect()->route('panel.user.marketplace.autoinvest', ['message' => __('Su auto-inversión se creó')]);
    }

    public function propiedades () 
    {
        $user = auth()->user();

        if (!($user->confirmado && $user->activado)) {
            return null;
        }

        if (!(!$user->activado && pago_sin_confirmar())) {
            // return null;
        }

        $properties = Property::where('estado', true)->where('tasa_interna_rentabilidad', '>', $user->autoinvest->tir);

        if ($user->autoinvest->location != 'all') {
            $properties->where('ubicacion', ucfirst($user->autoinvest->location));
        }
        
        if ($user->autoinvest->developer_id != -1) {
            $properties->where('developer_id', $user->autoinvest->developer_id);
        }

        if ($user->autoinvest->property_type_id != -1) {
            $properties->where('property_type_id', $user->autoinvest->property_type_id);
        }

        $properties = $properties->get();
        
        $autoinvest = $user->autoinvest;
        
        $properties = $properties->filter(function ($property, $key) {
            if ($property->publicado) {
                if ($property->state->id == 2 && $property->porcentajeCompletado != 100 && $property->remainingDays > 0) {
                    return true;
                }
            }
            return false;
        });
        
        $filtered = $properties->filter(function ($property, $key) use ($autoinvest, $user) {

            if ($property->users()->find($user->id)) {
                return false;
            }

            $invertidoTotal = ($property->invertido + $autoinvest->max_investment);
            
            if ($invertidoTotal <= $property->objetivo) {
                return true;
            } else {
                $maxInvertir = $property->objetivo - $property->invertido;
                if ($maxInvertir >= $autoinvest->min_investment) {
                    return true;
                }
            }

            return false;
        });

        foreach ($filtered as $key => $value) {
            echo $value->id . "\n";
        }
    }
}