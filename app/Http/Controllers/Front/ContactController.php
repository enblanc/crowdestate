<?php

namespace App\Http\Controllers\Front;

use App\Helpers\SeoHelpers;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    //
    /**
     * @param Request $request
     */
    public function send(Request $request)
    {
        $this->validate($request, [
            'nombre'      => 'required',
            'email'       => 'email',
            'telefono'    => 'required',
            'comentarios' => 'required',
        ]);

        $nombre = $request->input('nombre');
        $comentarios = $request->input('comentarios');
        $email = $request->input('email');
        $telefono = $request->input('telefono');

        Mail::to('contacto@brickstarter.com')->send(new ContactMail($nombre, $email, $telefono, $comentarios));
        // Mail::to('ismael@martinezgomez.es')->send(new ContactMail($nombre, $email, $telefono, $comentarios));

        $ok = true;

        SeoHelpers::setSEO(__('Contacto'), __('Contacta con BrickStarter'));

        return view('front.contact.contact', compact('ok', 'nombre'));
    }
}
