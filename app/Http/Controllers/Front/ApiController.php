<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;
use Infinety\CRUD\Models\Locale;
use Intervention\Image\Facades\Image;

class ApiController extends Controller
{
    public function __construct()
    {
        $this->middleware('web', ['except' => ['messages']]);
    }

    /**
     * @param Request $request
     */
    public function uploadWebcam(Request $request)
    {
        $file = $request->type.'-'.time().'.jpg';
        $path = 'img/temp/'.$file;
        Image::make(file_get_contents($request->photo))->save(public_path().'/'.$path);

        return responder()->success(null, ['data' => $path]);
    }

    /**
     * @return mixed
     */
    public function messages()
    {
        $currenLocale = localization()->getCurrentLocale();

        $locales = Locale::getAvailables();

        $messages = collect([]);

        foreach ($locales as $locale) {
            App::setLocale($locale->iso);

            $parsley = [
                'defaultMessage' => __('Este campo es erróneo.'),
                'type'           => [
                    'email'    => __('Debes introducir un correo válido.'),
                    'url'      => __('Debes introducir una URL válida.'),
                    'number'   => __('Debes introducir un número válido.'),
                    'integer'  => __('Debes introducir un número válido.'),
                    'digits'   => __('Debes introducir un dígito válido.'),
                    'alphanum' => __('Debes introducir un valor alfanumérico.'),
                ],
                'notblank'       => __('Este campo no debe estar en blanco.'),
                'required'       => __('Este campo es obligatorio.'),
                'pattern'        => __('La contraseña debe tener mínimo 8 caracteres con números y letras.'),
                'min'            => __('Este campo no debe ser menor que %s.'),
                'max'            => __('Este campo no debe ser mayor que %s.'),
                'range'          => __('Este campo debe estar entre %s y %s.'),
                'minlength'      => __('Este campo es muy corto. La longitud mínima es de %s caracteres.'),
                'maxlength'      => __('Este campo es muy largo. La longitud máxima es de %s caracteres.'),
                'length'         => __('La longitud de este campo debe estar entre %s y %s caracteres.'),
                'mincheck'       => __('Debe seleccionar al menos %s opciones.'),
                'maxcheck'       => __('Debe seleccionar %s opciones o menos.'),
                'check'          => __('Debe seleccionar entre %s y %s opciones.'),
                'equalto'        => __('Este campo debe ser idéntico.'),
            ];

            $months = [
                'enero'      => __('Enero'),
                'febrero'    => __('Febrero'),
                'marzo'      => __('Marzo'),
                'abril'      => __('Abril'),
                'mayo'       => __('Mayo'),
                'junio'      => __('Junio'),
                'julio'      => __('Julio'),
                'agosto'     => __('Agosto'),
                'septiembre' => __('Septiembre'),
                'octubre'    => __('Octubre'),
                'noviembre'  => __('Noviembre'),
                'diciembre'  => __('Diciembre'),
            ];

            $errors = [
                'error_general'    => __('Algo ha fallado. Por favor, prueba de nuevo.'),
                'pasword_coincide' => __('La contraseña especificada no coincide con la actual.'),
            ];

            $language = Cache::remember('translations_'.$locale, 30 * 24 * 60, function () use ($locale) {
                $langFile = resource_path("lang/$locale->iso.json");

                $data = collect(getFileContent($langFile));

                return $data->mapWithKeys(function ($item, $key) {
                    return [str_slug($key) => $item];
                });
            });

            $messages->put($locale->iso.'.parsley', $parsley);
            $messages->put($locale->iso.'.months', $months);
            $messages->put($locale->iso.'.errors', $errors);
            $messages->put($locale->iso.'.general', $language->toArray());
        }
        App::setLocale($currenLocale);

        $file = 'var messages = '.$messages->toJson().PHP_EOL.PHP_EOL.'export { messages };';

        Storage::disk('resources')->put('assets/js/messages.js', $file);

        return response()->json($messages);
    }
}
