<?php

namespace App\Http\Controllers\Front;

use App\BankAccount;
use App\Helpers\SeoHelpers;
use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\LemonWay\NewMoneyInRequest;
use App\Http\Requests\User\InviteRequest;
use App\Http\Requests\User\UserMoneyOutBankRequest;
use App\Http\Requests\User\UserNewBankAccountRequest;
use App\Http\Requests\User\UserNewsletterRequest;
use App\Http\Requests\User\UserRemoveBankAccountRequest;
use App\Notifications\InviteNotification;
use App\Certification;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;
use Infinety\LemonWay\Facades\LemonWay;
use Maatwebsite\Excel\Facades\Excel;
use Newsletter;

class UserController extends Controller
{

    /**
     * Muestra el formulario para resetear la ocntraseña
     *
     * @param Request $request
     * @param $token
     */
    public function showResetForm(Request $request, $token = null)
    {
        SeoHelpers::setSEO(__('Cambiar contraseña'), __('Cambiar contraseña en BrickStarter'));

        return view('front/access/nueva-contrasena')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    /**
     * Muestra la vista para agregar nuevos fondos
     *
     * @return Illuminate\View\View
     */
    public function vistaAgregarFondos()
    {
        SeoHelpers::setSEO(__('Agregar Fondos'), __('Agregar fondos en BrickStarter'));

        return view('front/dashboard/agregar-fondos');
    }

    /**
     * Procesa el nuevo fondo agregado
     *
     * @param  Request $request
     *
     * @return redirect
     */
    public function agregarFondos(NewMoneyInRequest $request)
    {
        $tarjeta = $request->get('tarjeta');
        $money = number_format($request->get('money'), 2, '.', '');
        $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'TARJ-');

        // Mínimo 1 €
        if ($money <= 0.5) {
            session()->flash('error_lemon', __('La cantidad mínima a ingresar es 1€'));

            return redirect()->route('panel.user.money.show');
        }

        if ($tarjeta == 'new') {
            //Tarjeta nueva
            $datosPago = [
                'wkToken'      => $token,
                'returnUrl'    => route('payment.ok'),
                'errorUrl'     => route('payment.error'),
                'cancelUrl'    => route('payment.cancel'),
                'notifUrl'     => env('URL_NOTIFY'),
                'comment'      => 'PagoId: '.$token,
                'registerCard' => 1,
            ];

            $transaction = [
                'lemonway_id' => 0,
                'wallet'      => auth()->user()->lemonway_id,
                'type'        => 0,
                'status'      => 0,
                'fecha'       => Carbon::now()->toDateTimeString(),
                'debit'       => 0,
                'credit'      => $money,
                'iban_id'     => null,
                'comment'     => $datosPago['comment'],
                'refund'      => 0,
                'token'       => $token,
            ];

            $transaction = auth()->user()->transactions()->create($transaction);

            try {
                $urlToPay = LemonWay::createPaymentForm(auth()->user()->wallet, $money, $datosPago, true, auth()->user()->locale, 'https://www.lemonway.fr/mercanet_lw.css');
            } catch (LemonWayExceptions $e) {
                $transaction->update(['status' => 4]);

                Log::info($e->getMessage());
                session()->flash('error_lemon', __('Ha ocurrido un error el pago no se ha podido realizar. Por favor, pruebe de nuevo.'));

                return redirect()->route('panel.user.money.show');
            }

            $transaction->update(['status' => 3]);
        } else {
            //USar tarjeta
            $transaction = [
                'lemonway_id' => 0,
                'wallet'      => auth()->user()->lemonway_id,
                'type'        => 0,
                'status'      => 0,
                'fecha'       => Carbon::now()->toDateTimeString(),
                'debit'       => 0,
                'credit'      => $money,
                'iban_id'     => null,
                'comment'     => 'PagoId: '.$token,
                'refund'      => 0,
                'token'       => $token,
            ];

            $transaction = auth()->user()->transactions()->create($transaction);

            //Usar Tarjeta acreditada
            try {
                $moneyInCardId = LemonWay::createPaymentWithCardId(auth()->user()->wallet, $money, $tarjeta);

                $logData = json_decode(json_encode($moneyInCardId), true);

                Log::info($logData);
                $transaction->update(['status' => $moneyInCardId->STATUS, 'lemonway_id' => $moneyInCardId->ID]);
            } catch (LemonWayExceptions $e) {
                $transaction->update(['status' => 4]);
                Log::info($e->getMessage());
                session()->flash('error_lemon', __('Ha ocurrido un error con esta tarjeta y el pago no se ha podido realizar. Por favor, pruebe con otra tarjeta.'));

                return redirect()->route('panel.user.money.show');
            }

            if (LemonWay::validatePaymentWithCardId($moneyInCardId->ID)) {
                session()->flash('fondos_agregados', __('Fondos agregados con tarjeta guardada'));
            } else {
                session()->flash('error_th_guardada', __('Error al pagar con la tarjeta guardada'));
            }
            Log::info((array) $moneyInCardId);

            $urlToPay = route('panel.user.money.show');
        }

        return redirect()->to($urlToPay);
    }

    /**
     * Página para ver los movimientos del usuario
     *
     * @return [type]
     */
    public function verMovimientos()
    {
        $transactions = auth()->user()->transactions->reverse();

        SeoHelpers::setSEO(__('Mis movimientos'), __('Ver mis movimientos en BrickStarter'));

        return view('front/dashboard/movimientos', compact('transactions'));
    }

    /**
     * ver cuentas Bancarias
     *
     * @return [type]
     */
    public function verCuentasBancarias()
    {
        $bankAccounts = auth()->user()->bankAccounts()->whereNotNull('document_id')->get();

        SeoHelpers::setSEO(__('Mis cuentas bancarias'), __('Ver mis cuentas bancarias en BrickStarter'));

        return view('front/dashboard/cuentas-bancarias', compact('bankAccounts'));
    }

    /**
     * Listado de Certificaciones
     *
     * @return void
     */
    public function cetifications ()
    {
        return view('front/dashboard/certifications', [
            'certifications' => auth()->user()->certifications()->orderBy('created_at', 'DESC')->get()
        ]);
    }

    public function downloadCetification (Certification $certification)
    {
        if (auth()->user()->id != $certification->user->id) 
            return abort(404);

        $certificationHelper = new \App\Helpers\CertificationHelper($certification);

        $certificationHelper->generate();
        
        return response()->download($certificationHelper->getPath());
        
    }

    /**
     * Agregar cuenta bancaria
     *
     * @param UserNewBankAccountRequest $request
     */
    public function postAgregarCuentaBancaria(UserNewBankAccountRequest $request)
    {
        //Fqkeamos el bic_swift
        $request->request->add(['bic_swift' => 'empty']);

        $bankAccount = auth()->user()->bankAccounts()->create($request->all());

        auth()->user()->addExtractoBancario($request->extracto, $bankAccount);

        $result = LemonWay::registerIban(auth()->user()->wallet, $request->get('titular'), $request->get('iban'), auth()->user()->profile->direccion, auth()->user()->profile->localidad);

        $bankAccount->update(['lemonway_id' => $result->ID]);

        session()->flash('user_accrediting', true);

        return redirect()->route('panel.user.bankaccounts.show');
    }

    /**
     * Elimina una cuenta bancaria del sistema.
     * No de lemonway que creo que no se puede hacer
     *
     * @param UserRemoveBankAccountRequest $request
     */
    public function postEliminarCuentaBancaria(UserRemoveBankAccountRequest $request)
    {
        $bankAccount = auth()->user()->bankAccounts()->where('id', $request->cuenta_id)->first();

        if ($bankAccount) {
            $bankAccount->document->delete();
            $bankAccount->delete();

            session()->flash('bank_deleted', true);
        } else {
            session()->flash('bank_deleted_error', true);
        }

        return redirect()->route('panel.user.bankaccounts.show');
    }

    /**
     * ver retirar Bancarias
     *
     * @return [type]
     */
    public function verRetirarFondos()
    {
        $bankAccounts = auth()->user()->bankAccounts->where('estado', 1);

        $moneyToOut = auth()->user()->balance;

        SeoHelpers::setSEO(__('Retirar Fondos'), __('Retirar fondos en BrickStarter'));

        return view('front/dashboard/retirar-fondos', compact('bankAccounts', 'moneyToOut'));
    }

    /**
     * Retira dondos de la cuenta de LwmonWay a la cuenta IBAN del usuario
     *
     * @param UserMoneyOutBankRequest $request
     */
    public function postRetirarFondos(UserMoneyOutBankRequest $request)
    {
        $maxMoneyOut = auth()->user()->balance;
        $cantidad = number_format(abs($request->get('cantidad')), 2, '.', '');

        if ($cantidad > $maxMoneyOut) {
            return redirect()->back()->withErrors([__('La cantidad solicitada es mayor de la que puede retirar')]);
        }

        $concepto = $request->get('concepto');
        $bankAccount = BankAccount::find($request->get('iban'));

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => auth()->user()->lemonway_id,
            'type'        => 1,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => $cantidad,
            'credit'      => 0,
            'iban_id'     => $bankAccount->id,
            'comment'     => $concepto,
            'refund'      => 0,
            'token'       => TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'OUT-'),
        ];
        $transaction = auth()->user()->transactions()->create($transaction);

        try {
            $resultado = LemonWay::walletMoneyToBank(auth()->user()->wallet, $cantidad, $bankAccount->lemonway_id, 0, ['message' => $concepto]);
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            $transaction->update(['status' => 4]);

            session()->flash('moneyout_error', true);

            return redirect()->back();
        }

        //Comprobamos el estado de la transacción
        $moneyOutResult = LemonWay::checkMoneyOut($resultado->ID);

        if ($moneyOutResult) {
            $transaction->lemonway_id = $resultado->ID;
            $transaction->status = $moneyOutResult[0]->STATUS;
            $transaction->save();

            session()->flash('moneyout_success', true);
        }

        return redirect()->back();
    }

    /**
     * @param $user
     */
    private function getMaxMoneyToTransfer($user)
    {
        //Últimos ingresos de dinero por TJ o por transferencia
        $lastTransfersOut = $this->getLastTransfersFromTransaction(auth()->user());
        $balance = $user->balance;
        $moneyOut = $balance - $lastTransfersOut;

        return $moneyOut;
    }

    /**
     * @param $user
     */
    private function getLastTransfersFromTransaction($user)
    {
        $transferOut = 0;
        $transactionsDesc = $user->transactions()->orderBy('id', 'DESC')->get();
        foreach ($transactionsDesc as $transaction) {
            if ($transaction->type == 0 || $transaction->type == 3) {
                $transferOut += $transaction->money;
            } else {
                break;
            }
        }

        return $transferOut;
    }

    /**
     * Elimina una tarjeta asociada a un wallet
     *
     * @param   Request  $request
     *
     * @return  redirect
     */
    public function eliminarTarjeta(Request $request)
    {
        if ($request->has('card')) {
            $result = LemonWay::unregisterCard(auth()->user()->wallet, $request->get('card'));

            if ($request->get('card') == (integer) $result) {
                session('removed_card', true);
            } else {
                session('removed_card', false);
            }
        } else {
            session('removed_card', false);
        }

        return redirect()->back();
    }

    /**
     * Agrega un email a la lista de newsletter de MailChimp
     * @param UserNewsletterRequest $request
     */
    public function addToNewsletter(UserNewsletterRequest $request)
    {
        try {
            Newsletter::subscribeOrUpdate(
                $request->email,
                ['FNAME' => '', 'LNAME' => ''],
                'subscribers',
                ['language' => current_locale()]
            );

            SeoHelpers::setSEO(__('Newsletter'), __('Newsletter de BrickStarter'));

            return view('front.access.newsletter');
        } catch (Exception $e) {
            return redirect()->back()->withErrors(['error' => __('Ha ocurrido un error al intentar agregar tu email. Prueba de nuevo.')], 'newsletter');
        }
    }

    /**
     * Va a la vista de invitar usuarios
     *
     * @param Request $request
     */
    public function invitar()
    {
        $user = auth()->user();

        SeoHelpers::setSEO(__('¡Gana 15€!'), __('¡Gana 15€ en BrickStarter!'));

        return view('front.dashboard.invitar', compact('user'));
    }

    /**
     * Lanza la invitación
     *
     * @param Request $request
     */
    public function lanzarinvitacion(InviteRequest $request)
    {
        $user = auth()->user();
        $visit = new User();
        $visit->email = $request->email;

        $appLocale = current_locale();
        localization()->setLocale($user->locale);
        $visit->notify(new InviteNotification($user));
        localization()->setLocale($appLocale);

        session()->flash('user_invited', true);

        $email_invited = $request->email;

        SeoHelpers::setSEO(__('¡Gana 15€!'), __('¡Gana 15€ en BrickStarter!'));

        return view('front.dashboard.invitar', compact('user', 'email_invited'));
    }

    /**
     * @return int
     */
    public function cerrarCuenta(Request $request)
    {
        if (!$request->has('password')) {
            session()->flash('errors_close', __('No has introducido una contraseña válida.'));

            return redirect()->back();
        }

        if (!Hash::check($request->get('password'), auth()->user()->password)) {
            session()->flash('errors_close', __('La contraseña especificada no coincide con la actual.'));

            return redirect()->back();
        }

        if (auth()->user()->hasLemonAccount()) {
            if (auth()->user()->balance > 0) {
                session()->flash('errors_close', __('Actualmente dispones de capital en tu cuenta. Por favor,').'<a href="'.route('panel.user.moneyof.show').'">'.__('retira tus fondos').'</a>'.__('antes de cerrar tu cuenta.'));

                return redirect()->back();
            }

            if (auth()->user()->balance == 0 && auth()->user()->properties->count() > 0) {
                session()->flash('errors_close', __('Tiene inversiones por lo que no podemos cerrar su cuenta. Por favor contacte con nuestro soporte.'));

                return redirect()->back();
            }

            try {
                // Creamos un nuevo email random para el email de lemonway
                $newEmail = 'closed-'.str_random(5).'-'.auth()->user()->email;

                $result = LemonWay::updateWalletDetails(auth()->user()->wallet, ['newEmail', $newEmail]);

                $result = LemonWay::updateWalletStatus(auth()->user()->wallet, 12);

                $idUser = auth()->user()->id;

                Auth::logout();

                User::find($idUser)->delete();

                session()->flash('modal', 'closedAccount');

                return redirect()->to('/');
            } catch (LemonWayExceptions $e) {
                session()->flash('errors_close', __('Se ha producido un error al cerrar su cuenta. Por favor contacte con nuestro soporte.'));

                return redirect()->back();
            }
        } else {
            //Como no tiene cuenta en lemonway directmanete eliminamos la cuenta
            $idUser = auth()->user()->id;

            Auth::logout();

            User::find($idUser)->delete();

            session()->flash('modal', 'closedAccount');

            return redirect()->to('/');
        }
    }

    /**
     * Exporta todas las transacciones del usuario.
     *
     * @return Maatwebsite\Excel\Facades\Excel
     */
    protected function exportTransactions (Request $request, User $user)
    {
        try {
            $transactions = $user->transactions()->orderBy('created_at', 'DESC')->get();
        
            return Excel::create(_('Mis movimientos') . " - {$user->nombre}", function($excel) use ($transactions) {
                $excel->sheet('mySheet', function($sheet) use ($transactions) {
                    $sheet->appendRow(1, [
                        __('Tipo'),
                        __('Importe'),
                        __('Fecha'),
                        __('Estado')
                    ]);

                    $sheet->cells('A1:D1', function ($cells) {
                        $cells->setFont(array(
                            'bold'       =>  true
                        ));
                    });

                    foreach ($transactions as $key => $transaction) {
                        $importe = $this->formatNumber($transaction);

                        $sheet->appendRow($key + 2, array(
                            str_replace(['<i>', '</i>'], ['', ''], $transaction->typeText),
                            $importe,
                            $transaction->created_at->format('d/m/Y'),
                            $transaction->statusText
                        ));
                        
                        if ($transaction->type == 1 || $transaction->type == 2 || $transaction->type == 15 || $transaction->type == 20) {
                            $sheet->cells('B'.($key + 2), function ($cells) {
                                $cells->setBackground('#E6B8B7');
                            });
                        }
                    }
                });
            })->download('xlsx');
        } catch (\Throwable $th) {
            logger($th->getMessage());
            return Excel::create("Movimientos - ERROR", function($excel) {
                $excel->sheet('mySheet', function($sheet) {
                    $sheet->appendRow(1, [__('Error al procesar los datos. Por favor, contacte con nosotros.')]);
                });
            })->download('xlsx');
        }
    }

    /**
     * @param $transaction
     */
    private function formatNumber(Transaction $transaction)
    {
        $money = '';
        if ($transaction->type == 1 || $transaction->type == 2 || $transaction->type == 15 || $transaction->type == 20) {
            $money .= '-';
        }

        $money .= number_format($transaction->money, 2, ',', '.') . '€';

        return $money; 
    }
}
