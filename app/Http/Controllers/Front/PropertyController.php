<?php

namespace App\Http\Controllers\Front;

use App\Helpers\SeoHelpers;
use App\Http\Controllers\Controller;
use App\Property;
use App\PropertyState;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class PropertyController extends Controller
{
    /**
     * @param Request $request
     */
    public function grid(Request $request)
    {
        if ($request->has('estado') && $request->get('estado') != 'todos') {
            $properties = Property::where('property_state_id', $request->get('estado'))->orderByRaw('- orden DESC')->get()->filter(function ($item) {
                return $item->publicado === true && $item->private === false;
            });
        } else {
            // Ordenamos por orden fijo y después por el orden de estado.
            // El menos delante sirve para poner los nulls al final
            $properties = Property::orderByRaw('- orden DESC')->orderByRaw("FIELD(property_state_id , '2', '1', '3', '4', '5', '6') ASC")->get()->filter(function ($item) {
                return $item->publicado === true && $item->private === false;
            });
        }

        $states = PropertyState::whereIn('id', $properties->pluck('property_state_id'))->get();

        //Activamos paginación manual
        $page = ($request->has('page') ? $request->get('page') : 1);
        $perPage = 6;
        $properties = new LengthAwarePaginator(
            $properties->forPage($page, $perPage),
            $properties->count(),
            $perPage,
            $page
        );

        SeoHelpers::setSEO(__('Inmuebles'), __('Inmuebles de BrickStarter'));

        $properties = $properties->setPath($request->url());

        return view('front/property-list/property-list', compact('properties', 'states'));
    }

    /**
     * @param $slug
     */
    public function show($slug)
    {
        $property = Property::whereSlug($slug)->first();
        if (!$property) {
            $property = Property::checkSlugAndReturn($slug);

            if (!$property) {
                return redirect()->to('/');
            }
        }
        // Solo se pueden ver inmuebles publicados
        if (!$property->publicado) {
            return redirect()->to('/');
        }

        $images = $this->getImagesAsArray($property->getMedia($property->mediaCollection));

        SeoHelpers::setSEO($property->nombre, __('Inmueble').' '.$property->nombre, $images);

        $details = $property->detailsTranslated;

        return view('front/property-single/property', compact(['property', 'details', 'propertyType', 'investment']));
    }

    /**
     * @param $imagesMedia
     * @return mixed
     */
    private function getImagesAsArray($imagesMedia)
    {
        $images = [];
        foreach ($imagesMedia as $media) {
            $images[] = url($media->getUrl());
        }

        return $images;
    }

    /**
     * @param Property $property
     */
    public function datosInversion($propertyId)
    {
        $property = Property::find($propertyId);

        if ($property->state->id != 4) {
            return redirect()->to($property->url);
        }

        $ids = auth()->user()->properties->pluck('id');

        if (in_array($propertyId, $ids->toArray())) {
            $invertido = 0;
            $misInversiones = auth()->user()->properties()->wherePivot('property_id', $propertyId)->get();
            foreach ($misInversiones as $inversion) {
                $invertido += $inversion->pivot->total;
            }

            $beneficios = auth()->user()->transactions()->whereIn('type', [13, 14, 18])->where('extras', $property->id)->get()->sum('credit');

            SeoHelpers::setSEO(__('Mi inversión en ').$property->nombre, __('Mi inversión en ').$property->nombre);

            $predicted = $property->predicted->first();

            return view('front/dashboard-inversion/mi-inversion', compact('property', 'predicted', 'invertido', 'beneficios'));
        } else {
            return redirect()->to($property->url);
        }
    }
}
