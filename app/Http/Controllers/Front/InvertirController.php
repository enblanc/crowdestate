<?php

namespace App\Http\Controllers\Front;

use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\CheckInvertirRequest;
use App\Http\Requests\User\InvertirInmuebleRequest;
use App\Property;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;
use Infinety\LemonWay\Facades\LemonWay;

class InvertirController extends Controller
{
    public function success()
    {
        return view('front/access/inversion-success');
    }

    /**
     * Comprueba si el usuario está registrado, acreditado, y con fondos
     *
     * @param $slug
     */
    public function checkInvertir(CheckInvertirRequest $request)
    {
        $property = Property::find($request->get('property_id'));

        if (!auth()->user()->hasLemonAccount()) {
            session()->flash('msg_invertir', __('Necesitas acreditar tu cuenta antes de poder invertir'));

            return redirect()->route('panel.user.accredit.show');
        }

        if (!auth()->user()->activado && !pago_sin_confirmar()) {
            session()->flash('msg_invertir', __('Necesitas acreditar tu cuenta antes de poder invertir'));

            return redirect()->route('panel.user.accredit.show');
        }

        $tjOnly = false;
        $balance = (double) auth()->user()->balance;
        $importeMinimo = (double) $property->importe_minimo;
        $importeAPagarMinimo = 1;
        if ($balance < $importeMinimo) {
            $tjOnly = true;

            $importeAPagarMinimo = round($importeMinimo - $balance);
        }

        return view('front/invertir/invertir', compact('property', 'tjOnly', 'importeAPagarMinimo'));
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function checkInvertirGet(Request $request, $id)
    {
        $property = Property::find($id);

        if ($property) {
            if (!auth()->user()->activado && !pago_sin_confirmar()) {
                session()->flash('msg_invertir', __('Necesitas acreditar tu cuenta antes de poder invertir'));

                return redirect()->route('panel.user.accredit.show');
            }

            if (!$property->publicado || $property->state->id != 2) {
                return redirect()->to('/');
            }

            if (!$property->porcentajeCompletado == 100 || $property->remainingDays < 1) {
                return redirect()->to('/');
            }

            $tjOnly = false;
            $balance = (double) auth()->user()->balance;
            $importeMinimo = (double) $property->importe_minimo;
            $importeAPagarMinimo = 1;
            if ($balance < $importeMinimo) {
                $tjOnly = true;

                $importeAPagarMinimo = round($importeMinimo - $balance);
            }

            return view('front/invertir/invertir', compact('property', 'tjOnly', 'importeAPagarMinimo'));
        }

        return redirect()->to('/');
    }

    /**
     * Invierte en un inmueble
     *
     * @param InvertirInmuebleRequest $request
     */
    public function invertirEnInmueble(InvertirInmuebleRequest $request)
    {
        $property = Property::find($request->get('property_id'));
        $inversion = number_format($request->get('inversion'), 2, '.', '');
        $formaPago = $request->get('forma-pago');

        if ((!auth()->user()->activado && pago_sin_confirmar())) {
            if ($inversion > 250) {
                return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('Lo máximo que puedes invertir en este inmueble es un total de 250 €')]);
            }
        }

        //Comprobamos que no sobrepase la inversión
        $invertidoTotal = $property->invertido;
        if (($invertidoTotal + $inversion) > $property->objetivo) {
            $maxInvertir = $property->objetivo - $invertidoTotal;

            return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('Lo máximo que puedes invertir en este inmueble es un total de ').$maxInvertir.' €']);
        }

        if ($formaPago == 'new') {
            //Bueva tarjeta
            return $this->pagarNuevaTarjeta($inversion, $property);
        } elseif ($formaPago == 'fondos') {
            //Con sus fondos
            if ((double) auth()->user()->balance < $inversion) {
                return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('Tienes menos fondos que los que deseas invertir.')]);
            }

            return $this->invertirConFondos($inversion, $property);
        } else {
            // Elegir tarjeta
            return $this->pagarConTarjetaGuardada($inversion, $property, $request->get('forma-pago'));
        }
    }

    /**
     * Función para pagar con una nueva tarjeta
     *
     * @param $token
     * @param $money
     */
    private function pagarNuevaTarjeta($money, $property)
    {
        $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'TARJ-');
        //Tarjeta nueva
        $datosPago = [
            'wkToken'      => $token,
            'returnUrl'    => route('invertir.payment.success.get'),
            'errorUrl'     => env('URL_ERROR'),
            'cancelUrl'    => env('URL_CANCEL'),
            'notifUrl'     => env('URL_NOTIFY'),
            'comment'      => 'PagoId: '.$token,
            'registerCard' => 1,
        ];

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => auth()->user()->lemonway_id,
            'type'        => 0,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $money,
            'iban_id'     => null,
            'comment'     => $datosPago['comment'],
            'refund'      => 0,
            'token'       => $token,
            'extras'      => $property->id,
        ];

        $transaction = auth()->user()->transactions()->create($transaction);

        try {
            $urlToPay = LemonWay::createPaymentForm(auth()->user()->wallet, $money, $datosPago, true, 'es', 'https://www.lemonway.fr/mercanet_lw.css');
        } catch (LemonWayExceptions $e) {
            $transaction->update(['status' => 4]);

            logger('Linea 173 - '.$e->getMessage());

            session()->flash('error_lemon', __('Ha ocurrido un error con esta tarjeta y el pago no se ha podido realizar. Por favor, pruebe con otra tarjeta.'));

            return redirect()->back();
        }

        return redirect()->to($urlToPay);
    }

    /**
     * Pahar con una tarjeta guardada
     *
     * @param $money
     * @param $property
     */
    private function pagarConTarjetaGuardada($money, $property, $tarjeta)
    {
        $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'TARJ-');

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => auth()->user()->lemonway_id,
            'type'        => 0,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $money,
            'iban_id'     => null,
            'comment'     => 'PagoId: '.$token,
            'refund'      => 0,
            'token'       => $token,
            'extras'      => $property->id,
        ];

        $transaction = auth()->user()->transactions()->create($transaction);

        //Usar Tarjeta acreditada
        try {
            $moneyInCardId = LemonWay::createPaymentWithCardId(auth()->user()->wallet, $money, $tarjeta);
            $transaction->update(['status' => 3, 'lemonway_id' => $moneyInCardId->ID]);
        } catch (LemonWayExceptions $e) {
            $transaction->update(['status' => 4]);

            logger('Linea 217 - '.$e->getMessage());

            return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('Ha ocurrido un error con esta tarjeta y el pago no se ha podido realizar. Por favor, pruebe con otra tarjeta.')]);
        }

        $transaction->update(['status' => 3]);

        if (LemonWay::validatePaymentWithCardId($moneyInCardId->ID)) {
            if ($this->invertir($property, $money)) {
                session()->flash('inversion_success', true);
                
                return redirect()->route('invertir.successfull');
            } else {
                return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('El dinero se ha añadido a tu cuenta pero no hemos podido realizar la inversión. Contacta con nuestro soporte.')]);
            }
        } else {
            session()->flash('error_lemon', __('Error al pagar con la tarjeta guardada'));

            logger('Linea 235 - '.$e->getMessage());

            return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('Error al pagar con la tarjeta guardada')]);
        }
    }

    /**
     * Invierte con sus fondos
     *
     * @param $money
     * @param $property
     */
    public function invertirConFondos($money, $property)
    {
        if ($this->invertir($property, $money)) {
            session()->flash('inversion_success', true);
                        
            return redirect()->route('invertir.successfull');
        } else {
            return redirect()->route('front.property.invertir.get', ['id' => $property->id])->withErrors([__('No se ha podido realizar la inversión. El dinero permanece en tu cuenta. Contacta con nuestro soporte.')]);
        }
    }

    /**
     * @param $property
     * @param $inversion
     */
    private function invertir($property, $inversion)
    {
        return auth()->user()->invertir($property, $inversion);
    }
}
