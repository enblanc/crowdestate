<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Property;
use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PaymentsController extends Controller
{
    /**
     * Muestra la vista de pago correcto
     *
     * @param Request $request
     */
    public function success(Request $request)
    {
        $token = $request->get('response_wkToken');

        $transaction = Transaction::token($token)->first();

        if ($transaction) {
            $transaction->update(['status' => 3]);
            session()->flash('payment_card', true);

            return redirect()->route('panel.user.money.show');
        } else {
            Log::info(__('No se encuentra la transacción'));
        }

        Log::info('Notificación: Dinero añadido correctamente por el usuario');
        Log::info($request->all());

        return redirect()->to('/');
    }

    /**
     * Muestra la vista de pago cancelado
     *
     * @param Request $request
     */
    public function cancel(Request $request)
    {
        $token = $request->get('response_wkToken');

        $transaction = Transaction::token($token)->first();

        if ($transaction) {
            $transaction->update(['status' => 32]);
            session()->flash('payment_card_cancel', true);

            return redirect()->route('panel.user.money.show');
        } else {
            Log::info('No se encuentra la transacción');
        }
        Log::info('Notificación: Añadir dinero cancelado por el usuario');
        Log::info($request->all());

        return redirect()->to('/');
    }

    /**
     * Muestra la vista de pago error
     *
     * @param Request $request
     */
    public function error(Request $request)
    {
        $token = $request->get('response_wkToken');

        $transaction = Transaction::token($token)->first();

        if ($transaction) {
            $transaction->update(['status' => 4]);
            session()->flash('payment_card_error', true);

            return redirect()->route('panel.user.money.show');
        } else {
            Log::info(__('No se encuentra la transacción'));
        }

        Log::info('Notificación: Añadir dinero error');
        Log::info($request->all());

        return redirect()->to('/');
    }

    /**
     * Comprueba el pago e invierte en el inmueble
     *
     * @param  Request $request+
     *
     * @return redirect
     */
    public function invertirSuccess(Request $request)
    {
        $token = $request->get('response_wkToken');

        $transaction = Transaction::token($token)->first();

        if (!$transaction) {
            Log::info(__('No se encuentra la transacción'));
            Log::info($request->all());

            return redirect()->to('/');
        }

        $transaction->update(['status' => 3]);
        $property = Property::find((int) $transaction->extras);

        if (!$property) {
            Log::info(__('No se encuentra la propiedad para poder realizar la inversión'));
            Log::info($request->all());

            return redirect()->route('panel.user.money.show');
        }

        $user = $transaction->user;

        $money = number_format($transaction->credit, 2, '.', '');

        if ($user->invertir($property, $money)) {
            session()->flash('inversion_success', true);

            return redirect()->route('invertir.successfull');
        } else {
            session()->flash('error_inversion', true);

            return redirect()->route('panel.user.money.show');
        }
    }
}
