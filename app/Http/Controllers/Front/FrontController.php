<?php

namespace App\Http\Controllers\Front;

use App\Blog\Models\Post;
use App\Crud\Pages\Models\Page;
use App\Helpers\SeoHelpers;
use App\Http\Controllers\Controller;
use App\Property;
use App\PropertyState;
use Carbon\Carbon;
use Illuminate\Cookie\CookieJar;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;

class FrontController extends Controller
{
    /**
     * @var array
     */
    protected $cuponesDisponibles = [
        'cupon5' => 'CEHVYSLF',
    ];

    /**
     * Muestra la página de Cómo funciona.
     *
     * @return view
     */
    public function home(CookieJar $cookieJar, Request $request)
    {
        $cupon = false;

        if (auth()->check() === false) {
            if ($request->has('promo')) {
                $cupon = $this->aplicarCupon($request->get('promo'));
                $cookieJar->queue(cookie('referral', $cupon, time() + 60 * 60 * 24 * 20)); //20 días
            } elseif (Cookie::has('referral')) {
                $referral = Cookie::get('referral');
                $bueno = array_search($referral, $this->cuponesDisponibles);
                if ($bueno !== false) {
                    $cupon = $referral;
                }
            }
        }

        $pageFound = Page::find(3);
        if ($pageFound) {
            $locale = current_locale();
            $page = $pageFound->translate($locale);

            $extras = json_decode($page->extras_trans);
            $images = json_decode($pageFound->extras);

            SeoHelpers::setSEO($page->name, '', (array) $images);

            $properties = Property::orderByRaw('- orden DESC')->orderByRaw("FIELD(property_state_id , '2', '1', '3', '4', '5', '6') ASC")->get()->filter(function ($item) {
                return $item->publicado === true && $item->private === false;
            })->take(3);

            $states = PropertyState::has('properties')->get();

            return view('front/home/home', compact('page', 'extras', 'images', 'properties', 'states', 'cupon'));
        }

        return redirect()->to('/');
    }

    /**
     * @param $cupon
     * @return mixed
     */
    private function aplicarCupon($cupon)
    {
        if (isset($this->cuponesDisponibles[$cupon])) {
            return $this->cuponesDisponibles[$cupon];
        }
    }

    /**
     * Muestra la página de Nosotros.
     *
     * @return view
     */
    public function quienesSomos()
    {
        $pageFound = Page::find(1);
        if ($pageFound) {
            $page = $pageFound->translate(current_locale());

            $extras = json_decode($page->extras_trans);

            $images = json_decode($pageFound->extras);

            SeoHelpers::setSEO($page->name, '', (array) $images);

            return view('front/corporate/quienes-somos', compact('page', 'extras', 'images'));
        }

        return redirect()->to('/');
    }

    /**
     * Muestra la página de Cómo funciona.
     *
     * @return view
     */
    public function comoFunciona()
    {
        $pageFound = Page::find(2);
        if ($pageFound) {
            $page = $pageFound->translate(current_locale());

            $extras = json_decode($page->extras_trans);
            $images = json_decode($pageFound->extras);

            SeoHelpers::setSEO($page->name, '', (array) $images);

            return view('front/corporate/como-funciona', compact('page', 'extras', 'images'));
        }

        return redirect()->to('/');
    }

    /**
     * Muestra la página de Cómo funciona.
     *
     * @return view
     */
    public function legal()
    {
        $pageFound = Page::find(4);
        if ($pageFound) {
            $page = $pageFound->translate(current_locale());

            $extras = json_decode($page->extras_trans);
            $images = json_decode($pageFound->extras);

            SeoHelpers::setSEO($page->name, '', (array) $images);

            return view('front/corporate/legal', compact('page', 'extras', 'images'));
        }

        return redirect()->to('/');
    }

    /**
     * Muestra la página de Cómo funciona.
     *
     * @return view
     */
    public function legalLemonWay()
    {
        $pageFound = Page::find(5);

        if ($pageFound) {
            $page = $pageFound->translate(current_locale());

            $extras = json_decode($page->extras_trans);
            $images = json_decode($pageFound->extras);

            SeoHelpers::setSEO($page->name, '', (array) $images);

            return view('front/corporate/legal', compact('page', 'extras', 'images'));
        }

        return redirect()->to('/');
    }

    /**
     * @return mixed
     */
    public function postsJsonFeed()
    {
        $data = [
            'version'       => 'https://jsonfeed.org/version/1',
            'title'         => 'brickstarter Feed',
            'home_page_url' => 'https://brickstarter.com/',
            'feed_url'      => 'https://brickstarter.com/feed/json',
            'icon'          => 'https://brickstarter.com/apple-touch-icon.png',
            'favicon'       => 'https://brickstarter.com/apple-touch-icon.png',
            'items'         => [],
        ];

        $today = date('Y-m-d');
        $posts = Post::where('state', 1)->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->orderby('created_at', 'DESC')->get();

        foreach ($posts as $key => $post) {
            $data['items'][$key] = [
                'id'             => $post->id,
                'title'          => $post->title,
                'url'            => $post->url,
                'image'          => $post->imageFull,
                'content_html'   => $post->content,
                'date_published' => Carbon::parse($post->publish_date)->tz('UTC')->toRfc3339String(),
                'date_modified'  => $post->updated_at->tz('UTC')->toRfc3339String(),
                'author'         => [
                    'name' => 'Brickstarter',
                ],
            ];
        }

        return $data;
    }

    public function xmlFeed()
    {
        $today = date('Y-m-d');
        $posts = Post::where('state', 1)->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->orderby('created_at', 'DESC')->get();
        $content = view('front.blog.feed', compact('posts'));

        return response($content, 200)
            ->header('Content-Type', 'text/xml');
    }
}
