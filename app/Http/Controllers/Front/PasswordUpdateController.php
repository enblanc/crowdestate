<?php

namespace App\Http\Controllers\Front;

use App\Helpers\SeoHelpers;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\PasswordUpdateRequest;
use App\User;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PasswordUpdateController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();

        SeoHelpers::setSEO(__('Cambiar contraseña'), __('Cambiar contraseña de BrickStarter'));

        return view('front/dashboard/cambiar-contrasena', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PasswordUpdateRequest $request)
    {
        $oldpassword = $request->get('oldpassword');
        $newpassword = $request->get('newpassword');
        $repeatpassword = $request->get('repeatpassword');

        if ($newpassword == $repeatpassword) {
            $user = auth()->user();

            if (Hash::check($oldpassword, $user->password)) {
                $user->password = $newpassword;
                $user->save();

                session()->flash('pass_saved', true);
            } else {
                session()->flash('pass_no_match', true);
            }
        } else {
            session()->flash('pass_no_same', true);
        }

        return redirect()->route('panel.user.password.show');
    }
}
