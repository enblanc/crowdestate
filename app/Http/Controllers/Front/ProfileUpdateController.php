<?php

namespace App\Http\Controllers\Front;

use App\EmailResets;
use App\Helpers\SeoHelpers;
use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\User\ProfileUpdateRequest;
use App\Http\Requests\User\UserAccreditRequest;
use App\Notifications\ConfirmEmailResetsNotification;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;
use Infinety\LemonWay\Facades\LemonWay;
use Intervention\Image\Facades\Image;
use Newsletter;

class ProfileUpdateController extends Controller
{
    /**
     * Muestra la vista básica para editar el usuario
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();
        $newsletter = $user->isSubscribedToNewsletter();

        SeoHelpers::setSEO(__('Mis datos'), __('Ver mis datos básicos en BrickStarter'));

        return view('front/dashboard/datos-basicos', compact('user', 'newsletter'));
    }

    /**
     * Actualiza los datos básicos del usaurio
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProfileUpdateRequest $request)
    {
        $user = auth()->user();
        $user->nombre = $request->get('nombre');
        $user->apellido1 = $request->get('apellido1');
        $user->apellido2 = $request->get('apellido2');
        $user->locale = $request->get('lang');

        if ($request->has('tribute')) {
            $user->tribute = true;
        } else {
            $user->tribute = false;
        }

        $userEmail = $user->email;

        if ($user->email != $request->get('email')) {
            $emailResets = EmailResets::where('user_id', auth()->user()->id)->first();

            if ($emailResets) {
                $emailResets->token = str_random(30);
                $emailResets->email = $request->get('email');
                $emailResets->save();
            } else {
                EmailResets::create([
                    'user_id' => auth()->user()->id,
                    'token'   => str_random(30),
                    'email'   => $request->get('email'),
                ]);
            }
            $temp_user = new User();
            $temp_user->email = $request->email;

            $userEmail = $temp_user->email;

            $appLocale = current_locale();
            localization()->setLocale($user->locale);
            $temp_user->notify(new ConfirmEmailResetsNotification($user));
            localization()->setLocale($appLocale);

            session()->flash('email_sent', true);
        }

        $user->save();

        //Hay que comprobar si tiene cuenta en LemonWay.
        //En caso de tener cuenta hay que actualizar los datos en LemonWay
        if ($user->hasLemonAccount()) {
            LemonWay::updateWalletDetails($user->wallet, ['newFirstName' => $user->nombre, 'newLastName' => $user->apellido1.' '.$user->apellido2]);
        }

        //Newsletter
        if ($request->has('newsletter')) {
            //Check if user was subscribed
            $wasSubscribed = $user->isSubscribedToNewsletter();

            if (!$wasSubscribed) {
                try {
                    Newsletter::subscribeOrUpdate(
                        $user->email,
                        ['FNAME' => $user->nombre, 'LNAME' => $user->apellido1],
                        'subscribers',
                        ['language' => $user->locale]
                    );
                } catch (\Exception $e) {
                    return redirect()->back()->withErrors(['error' => __('Ha ocurrido un error al intentar agregar tu email al newsletter. Prueba de nuevo.')], 'newsletter');
                }
            }
        } else {
            //Check if user was subscribed
            $wasSubscribed = $user->isSubscribedToNewsletter();

            if ($wasSubscribed) {
                try {
                    Newsletter::unsubscribe($user->email);
                } catch (Exception $e) {
                    return redirect()->back()->withErrors(['error' => __('Ha ocurrido un error al intentar eliminar tu email del newsletter. Prueba de nuevo.')], 'newsletter');
                }
            }
        }

        session()->flash('user_saved', true);

        return redirect()->route('panel.user.basic.show');
    }

    /**
     * Confirma el nuvo cambio de email del usuario
     *
     * @param  true
     */
    public function confirmemailreset($emailresettoken)
    {
        $emailResets = EmailResets::where('token', $emailresettoken)->first();

        if (!$emailResets) {
            return redirect()->to('/');
        }

        $user = User::find($emailResets->user_id);
        $user->email = $emailResets->email;
        $user->save();

        //Hay que comprobar si tiene cuenta en LemonWay.
        //En caso de tener cuenta hay que actualizar los datos en LemonWay
        if ($user->hasLemonAccount()) {
            LemonWay::updateWalletDetails($user->wallet, ['newEmail' => $user->email]);
        }

        $emailResets->delete();
        session()->flash('email_changed', true);

        // Login and "remember" the given user...
        Auth::loginUsingId($user->id);

        return redirect()->route('panel.user.basic.show');
    }

    /**
     * Muestra la vista para acreditar el usuario
     *
     * @return Illuminate\View\View
     */
    public function showAcreditar()
    {
        SeoHelpers::setSEO(__('Acreditar cuenta'), __('Acreditar cuenta en BrickStarter'));

        $activado = false;
        $pendiente = false;
        $noForm = false;

        //Está pendiente de validar por LemonWay
        if (!auth()->user()->activado && auth()->user()->DniDocument && auth()->user()->DniDocument->status == 0) {
            $pendiente = true;
            $noForm = true;

            return view('front/dashboard/acreditar-cuenta', compact('activado', 'pendiente', 'noForm'));
        }

        //La validación de LwmonWay ha sido erronea
        if (!auth()->user()->activado && auth()->user()->DniDocument && !auth()->user()->DniDocument->isValid) {
            $pendiente = true;
            $activado = false;

            return view('front/dashboard/acreditar-cuenta', compact('activado', 'pendiente', 'noForm'));
        }

        //Si el usuario ya está correctamente validado por LemonWay
        if (auth()->user()->activado && auth()->user()->DniDocument && auth()->user()->DniDocument->isValid) {
            $pendiente = false;
            $activado = true;
            $noForm = false;

            return view('front/dashboard/acreditar-cuenta', compact('activado', 'pendiente', 'noForm'));
        }

        //Usuario no acreditado
        if (!auth()->user()->activado && auth()->user()->DniDocument == null) {
            $noForm = false;

            return view('front/dashboard/acreditar-cuenta', compact('activado', 'pendiente', 'noForm'));
        }

        //Está pendiente de validar por LemonWay pero su DNI está validado
        if (!auth()->user()->activado && auth()->user()->DniDocument && auth()->user()->DniDocument->status == 2) {
            $pendiente = true;
            $noForm = true;

            session()->flash('error_lemon', __('Tu cuenta no ha podido ser valida ya que requiere más pasos. Por favor, ponte en contacto con nosotros.'));

            return view('front/dashboard/acreditar-cuenta', compact('activado', 'pendiente', 'noForm'));
        }

        $activado = false;
        $pendiente = true;
        $noForm = true;
        session()->flash('error_lemon', __('Tenemos problemas para determinar tu cuenta. Por favor, ponte en contacto con nosotros.'));

        return view('front/dashboard/acreditar-cuenta', compact('activado', 'pendiente', 'noForm'));
    }

    /**
     * Guarda la acreditación del usuario
     *
     * @param UserAccreditRequest $request
     */
    public function postAcreditar(User $user, UserAccreditRequest $request)
    {
        $birthdate = $request->birthdate;

        $birthdateFormatted = Carbon::createFromFormat('d/m/Y', $birthdate)->toDateString();

        $request->merge(['birthdate' => $birthdateFormatted]);

        $primeraVez = false;

        auth()->user()->profile->update($request->all());

        if (auth()->user()->hasLemonAccount() === false) {
            $result = $this->createNewLemonWayAccount(auth()->user());
            if ($result === false) {
                session()->flash('error_lemon', __('Ha ocurrido un error y su cuenta LemonWay no ha podido ser creada. Pruebe de nuevo o contacte con nosotros.'));

                return redirect()->back();
            }

            $primeraVez = true;
            session()->flash('esperando_primera_acreditacion', true);
        } else {
            // Actualizamos los datos en Lemonway
            $result = $this->updateLemonWayAccount(auth()->user());
            if ($result === false) {
                session()->flash('error_lemon', __('Ha ocurrido un error y su cuenta LemonWay no ha podido ser actualizada. Pruebe de nuevo o contacte con nosotros.'));

                return redirect()->back();
            }
        }

        if ($request->imagen_dni && $request->imagen_dni_dos) {
            $firstImage = Image::make($request->imagen_dni);
            $secondImage = Image::make($request->imagen_dni_dos);

            $totalWidth = $firstImage->width() + $secondImage->width();
            $totalHeight = $firstImage->height() + $secondImage->height();

            $newImage = Image::canvas($totalWidth, $totalHeight);
            $newImage->insert($firstImage);
            $newImage->insert($secondImage, 'top-right'); // add offset

            $firstImage->destroy();
            $secondImage->destroy();

            auth()->user()->addDniDocument($newImage);

            $newImage->destroy();

            $profile = auth()->user()->profile;
            $profile->estado = 0;
            $profile->save();

            session()->flash('user_accrediting', true);
        }

        if ($request->webcam_front && $request->webcam_back) {
            $frontImage = public_path().'/'.$request->webcam_front;
            $backImage = public_path().'/'.$request->webcam_back;

            $firstImage = Image::make($frontImage);
            $secondImage = Image::make($backImage);

            $totalWidth = $firstImage->width() + $secondImage->width();
            $totalHeight = $firstImage->height();

            $newImage = Image::canvas($totalWidth, $totalHeight);
            $newImage->insert($firstImage);
            $newImage->insert($secondImage, 'top-right'); // add offset

            $firstImage->destroy();
            $secondImage->destroy();

            auth()->user()->addDniDocument($newImage);

            $newImage->destroy();

            // Eliminamos las imagenes que se han hecho
            if (file_exists($frontImage)) {
                @unlink($frontImage);
            }
            if (file_exists($backImage)) {
                @unlink($backImage);
            }

            $profile = auth()->user()->profile;
            $profile->estado = 0;
            $profile->save();

            session()->flash('user_accrediting', true);
        }

        ;

        if ($primeraVez) {
            return redirect()->route('panel.user.accredit.show', ['message' => __('Gracias por acreditarte')]);
        }

        return redirect()->route('panel.user.accredit.show');
    }

    /**
     * Create new LemonWay Account for current user
     *
     * @param  App\User $user
     *
     * @return boolean
     */
    private function createNewLemonWayAccount($user)
    {
        $newToken = TokensHelper::generateUniqueToken('App\User', 'lemonway_id', 10, true, 'BRCKSTRT-');

        $extras = [
            'birthdate'          => $this->getBirthdateFormatted($user),
            'nationality'        => $user->profile->countryAlpha3,
            'birthCity'          => $user->profile->ciudad,
            'birthcountry'       => $user->profile->ciudad,
            'city'               => $user->profile->ciudad,
            'ctry'               => 'ESP',
            'payerOrBeneficiary' => 2,
        ];

        if ($user->profile->tipo == 'Empresa') {
            $extras['isCompany'] = 1;
            $extras['companyIdentificationNumber'] = $user->profile->company_cif;
            $extras['companyName'] = $user->profile->company_rsocial;
            $extras['companyDescription'] = $user->profile->company_description;
            $extras['companyWebsite'] = $user->profile->company_website;
            $extras['ctry'] = $user->profile->companyCountryAlpha3;
        }

        $newLemonWayUser = LemonWay::setWalletUser($newToken, $user->email, $user->nombre, $user->apellido1.' '.$user->apellido2, $extras);

        try {
            $wallet = LemonWay::createWallet($newLemonWayUser);

            $user->lemonway_id = $newToken;

            $user->lemonway_walletid = $wallet->LWID;

            $user->save();
        } catch (LemonWayExceptions $e) {
            logger($e->getMessage());

            return false;
        }

    }

    /**
     * Actualiza los datos de la cuenta en LemonWay
     * @param $user
     *
     * @return boolean
     */
    private function updateLemonWayAccount($user)
    {
        $dataToUpdate = [
            'newFirstname'  => 'Javier.',
            'newName'       => 'Javier.',
            'newBirthDate'   => $this->getBirthdateFormatted($user),
            'newBirthcity'   => $user->profile->ciudad.'x',
            'newBirthcountry'   => $user->profile->ciudad.'x',
            'newCity'   => $user->profile->ciudad.'x'
        ];

        if ($user->profile->tipo == 'Empresa') {
            $dataToUpdate['newIsCompany'] = 1;
            $dataToUpdate['newCompanyName'] = $user->profile->company_rsocial;
            $dataToUpdate['newCompanyWebsite'] = $user->profile->company_website;
            $dataToUpdate['newCompanyDescription'] = $user->profile->company_description;
            $dataToUpdate['newCompanyIdentificationNumber'] = $user->profile->company_cif;
            $dataToUpdate['newCtry'] = $user->profile->companyCountryAlpha3;
        } else {
            $dataToUpdate['newIsCompany'] = 0;
        }

        try {
            LemonWay::updateWalletDetails($user->wallet, $dataToUpdate);

            return true;
        } catch (LemonWayExceptions $e) {
            logger($e);

            return false;
        }
    }

    protected function getBirthdateFormatted ($user)
    {
        try {
            return Carbon::createFromFormat('Y-m-d', $user->profile->birthdate)->format('d/m/Y');
        } catch (\Throwable $th) {
            logger($th->getMessage());
            return $user->profile->birthdate;
        }
    }
}
