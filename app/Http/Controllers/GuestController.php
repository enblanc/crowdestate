<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Cookie\CookieJar;

class GuestController extends Controller
{
    /**
     * @param CookieJar $cookieJar
     * @param $token
     */
    public function referred(CookieJar $cookieJar, $token)
    {
        $user = User::where('affiliate_id', $token)->first();
        if ($user) {
            $cookieJar->queue(cookie('referral', $token, time() + 60 * 60 * 24 * 20)); //20 días
        }

        return redirect()->route('registro');
    }
}
