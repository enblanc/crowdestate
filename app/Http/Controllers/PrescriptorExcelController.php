<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Property;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use HttpOz\Roles\Models\Role;
use Illuminate\Support\Facades\App;
use Maatwebsite\Excel\Facades\Excel;

class PrescriptorExcelController extends Controller
{

    /**
     * @var array
     */
    protected $tiposActivos = [
        0  => 'Pago con tarjeta',
        1  => 'Retirada fondos',
        2  => 'Inversión Inmueble',
        3  => 'Ingreso por transferencia',
        4  => 'Devolución',
        13 => 'Reparto Dividendos',
        14 => 'Venta',
        15 => 'Regalo',
        16 => 'Devolución por inmueble no conseguido',
    ];

    public function index()
    {
        $now = Carbon::now();
        Excel::create('Report - '.$now, function ($excel) {

            // Set the title
            $excel->setTitle('Informe prescriptores');

            // Chain the setters
            $excel->setCreator('Me')->setCompany('Brickstarter');

            $excel->setDescription('Informe prescriptores');

            $this->PrimeraPagina($excel);
            $this->SegundaPagina($excel);
        })->download('xls');
    }

    public function blade()
    {
        ini_set('memory_limit', '-1');
        Excel::create('New file', function ($excel) {
            $userTransactions = Transaction::whereIn('type', [0, 1, 2, 3, 4, 13, 14, 15])->whereStatus(3)->get();
            $arrayResume = $this->createArrayData($userTransactions);
            $user = User::find(53);
            $tiposActivos = $this->tiposActivos;
            $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
            $year = Carbon::today()->year;

            $excel->sheet('Resumen Inversiones CLientes', function ($sheet) use ($user, $userTransactions, $arrayResume, $months, $year, $tiposActivos) {
                // $user = User::all();
                // $userTransactions = $user->transactions;
                $sheet->loadView('excelPrescriptor.page1', compact('user', 'userTransactions', 'arrayResume', 'months', 'year', 'tiposActivos'));
            });

            $excel->sheet('Resumen Global por Cliente', function ($sheet) use ($user, $userTransactions, $arrayResume, $months, $year, $tiposActivos) {
                // $user = User::all();
                // $userTransactions = $user->transactions;
                $sheet->loadView('excelPrescriptor.page2', compact('user', 'userTransactions', 'arrayResume', 'months', 'year', 'tiposActivos'));
            });
        })->store('xlsx');
    }

    public function blade_view()
    {
        $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];

        $year = Carbon::today()->year;

        $prescriptor = Role::find(3)->users->first();

        $transactions = collect([]);
        if ($prescriptor->referrals->count() > 0) {
            foreach ($prescriptor->referrals as $referido) {
                $userTrans = $referido->transactions()->whereYear('fecha', $year)->whereIn('type', [0, 1, 2, 3, 4, 13, 14, 15])->whereStatus(3)->get();
                $transactions->push($userTrans);
            }
            $userTransactions = $transactions->flatten();

            if ($userTransactions->count() > 0) {
                $arrayResume = $this->createArrayData($userTransactions);
            }
        }

        // $userTransactions = Transaction::whereIn('type', [0, 1, 2, 3, 4, 13, 14, 15])->whereStatus(3)->get();

        // $arrayResume = $this->createArrayData($userTransactions);

        // dump($arrayResume);
        // dd();
        $user = $prescriptor;

        $tiposActivos = $this->tiposActivos;

        return view('excelPrescriptor.page2', compact('user', 'userTransactions', 'arrayResume', 'months', 'year', 'tiposActivos'));
    }

    /**
     * @return mixed
     */
    private function createArrayData($transactions)
    {
        $arrayResume = [];
        foreach ($transactions as $transaction) {
            $userId = $transaction->user_id;

            /* CONSULTA TIPOS */
            $transId = $transaction->id;
            $tipo_mov = $this->tipoMovimiento($transaction);

            $importe = $tipo_mov['importe'];
            $tipo = $tipo_mov['tipo'];
            $month = Carbon::parse($transaction->fecha)->format('m');
            $year = Carbon::parse($transaction->fecha)->format('Y');

            $tipoId = $transaction->type;
            $inmuebleRef = 'NoRef';
            if ($transaction->type == 13) {
                $inmueble = Property::whereHas('users', function ($q) use ($transId) {
                    $q->where('transaction_id', $transId);
                })->first();

                if ($inmueble) {
                    $inmuebleRef = $inmueble->ref;
                }

                // dump($inmueble);
                // dd();
            } else {
                // dump($transaction->extras);
                $inmueble = Property::where('id', $transaction->extras)->first();
                // dump($inmueble);
                if (isset($inmueble)) {
                    $inmuebleRef = $inmueble->ref;
                } else {
                    $inmuebleRef = 'NoRef';
                }
                // dd();
                // $inmuebleRef = $inmueble->ref;
            }

            // dump($transaction->type);
            // dump($inmueble);
            // dump($transId);

            /* ANALISIS DE SI EXISTE EL USUARIO EN EL ARRAY */
            // Eric- Añaduido el iset del año para que no falle
            if (!isset($arrayResume[$userId]) || !isset($arrayResume[$userId]['totales'][$year])) {
                $arrayResume[$userId]['totales'][$year] = $importe;
                $arrayResume[$userId]['totales'][$month] = $importe;
                $arrayResume[$userId]['totales_movimientos'][$tipoId][$year] = $importe;
                $arrayResume[$userId]['totales_movimientos'][$tipoId][$month] = $importe;
                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year] = $importe;
                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month] = $importe;
                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef]['objetivo'] = null;
            } else {
                $importe_ant = $arrayResume[$userId]['totales'][$year];
                $arrayResume[$userId]['totales'][$year] = $importe + $importe_ant;

                /* TOTALES MESES */

                if (!isset($arrayResume[$userId]['totales'][$month])) {
                    $arrayResume[$userId]['totales'][$month] = $importe;
                } else {
                    $importe_ant_month = $arrayResume[$userId]['totales'][$month];
                    $arrayResume[$userId]['totales'][$month] = $importe + $importe_ant_month;
                };

                /* TOTALES TIPO DE MOVIMIENTO */

                if (!isset($arrayResume[$userId]['totales_movimientos'][$tipoId][$year])) {
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$year] = $importe;
                } else {
                    $importeAntTipoYear = $arrayResume[$userId]['totales_movimientos'][$tipoId][$year];
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$year] = $importe + $importeAntTipoYear;
                };

                if (!isset($arrayResume[$userId]['totales_movimientos'][$tipoId][$month])) {
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$month] = $importe;
                } else {
                    $importeAntTipo = $arrayResume[$userId]['totales_movimientos'][$tipoId][$month];
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$month] = $importe + $importeAntTipo;
                };

                /* TOTALES INVERSIONES */

                if (!isset($arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year])) {
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year] = $importe;
                } else {
                    $importeAntInversionYear = $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year];
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year] = $importe + $importeAntInversionYear;
                };

                if (!isset($arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month])) {
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month] = $importe;
                } else {
                    $importeAntInversion = $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month];
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month] = $importe + $importeAntInversion;
                };

                // OBJETIVO DE INVERSIÓN DE LOS INMUEBLES

                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef]['objetivo'] = $inmueble['objetivo'];

                // COMISIONES DE LOS MOVIMIENTOS

                if (!isset($arrayResume[$userId]['totales_comisiones'][$tipoId][$year])) {
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$year] = abs($transaction->prescriptor);
                } else {
                    $importeAntComision = $arrayResume[$userId]['totales_comisiones'][$tipoId][$year];
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$year] = abs($transaction->prescriptor) + $importeAntComision;
                };

                if (!isset($arrayResume[$userId]['totales_comisiones'][$tipoId][$month])) {
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$month] = $transaction->prescriptor;
                } else {
                    $importeAntComision = $arrayResume[$userId]['totales_comisiones'][$tipoId][$month];
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$month] = $transaction->prescriptor + $importeAntComision;
                };
            }
        }

        return $arrayResume;
    }

    /**
     * @param $transaction
     */
    private function tipoMovimiento($transaction)
    {
        switch ($transaction->type) {
            case 0:
                $tipo = 'Pago con tarjeta';
                break;
            case 1:
                $tipo = 'Retirada de fondos';
                break;
            case 2:
                $tipo = 'Inversión en Inmueble';
                break;
            case 3:
                $tipo = 'Ingreso por transferencia';
                break;
            case 4:
                return 'Devolución';
                // Quizá es refond
                break;
            case 13:
                $tipo = 'Divindendos generados por Inmueble';
                break;
            case 14:
                $tipo = 'Venta';
                break;
            case 15:
                $tipo = 'Regalo';
            // no break
            case 15:
                $tipo = 'Devolución por inmueble no conseguido';
                break;
            default:
                $tipo = '';
                break;
        }

        return [
            'tipo'    => $tipo,
            'importe' => $transaction->money,
        ];
    }
}
