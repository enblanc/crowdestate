<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
     */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = '/';
    // protected $redirectTo = '/?logged';

    /**
     * @var string
     */
    protected $loginPath = '/login';

    /**
     * @var string
     */
    protected $errorBag = '/?getlogin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function redirectTo()
    {
        $lang = localization()->getCurrentLocale();
        if ($lang !== 'es') {
            return '/'.$lang.'/?logged';
        }

        return '/?logged';
    }

    /**
     * @param Request $request
     * @param $user
     */
    public function authenticated(Request $request, $user)
    {
        if (!$user->confirm->is_confirmed) {
            auth()->logout();

            return redirect()->route('login')->withErrors(['confirmed' => __('El usuario no está confirmado. Por favor, valida tu cuenta mediante el email que te enviamos al registrarte.')]);
            // return back()->withErrors(['activation', 'You need to confirm your account. We have sent you an activation code, please check your email.']);
        }

        return redirect()->intended($this->redirectPath());
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        $lang = localization()->getCurrentLocale();
        if ($lang !== 'es') {
            return redirect('/'.$lang);
        }

        return redirect('/');
    }
}
