<?php

namespace App\Http\Controllers\Auth;

use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateAccountFromSocialRequest;
use App\Http\Requests\LinkAccountRequest;
use App\Social\FacebookServiceProvider;
use App\Social\GoogleServiceProvider;
use App\Social\LinkedinServiceProvider;
use App\User;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Laravel\Socialite\Two\InvalidStateException;
use League\OAuth1\Client\Credentials\CredentialsException;

class SocialController extends Controller
{
    /**
     * @var array
     */
    protected $providers = [
        'facebook' => FacebookServiceProvider::class,
        'google'   => GoogleServiceProvider::class,
        'linkedin' => LinkedinServiceProvider::class,
    ];

    /**
     * @var array
     */
    protected $requiredFields = [
        'nombre'     => null,
        'apellido1'  => null,
        'apellido2'  => null,
        'email'      => null,
        'provider'   => null,
        'providerId' => null,
    ];

    /**
     *  Create a new controller instance
     *
     * @return  void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     *  Redirect the user to provider authentication page
     *
     *  @param  string $driver
     *  @return \Illuminate\Http\Response
     */
    public function redirectToProvider($driver)
    {
        $provider = $this->providers[$driver];

        return (new $provider())->redirect();
    }

    /**
     *  Handle provider response
     *
     *  @param  string $driver
     *  @return \Illuminate\Http\Response
     */
    /**
     * @param $data
     */
    public function handleProviderCallback($driver)
    {
        $provider = $this->providers[$driver];

        try {
            return (new $provider())->handle();
        } catch (InvalidStateException $e) {
            return $this->redirectToProvider($driver);
        } catch (ClientException $e) {
            return $this->redirectToProvider($driver);
        } catch (CredentialsException $e) {
            return $this->redirectToProvider($driver);
        }
    }

    /**
     * @param Request $request
     */
    public function showRegistrationForm(Request $request)
    {
        $fields = array_merge($this->requiredFields, $request->all());
        extract($fields);

        return view('auth.register-oauth', compact('nombre', 'apellido1', 'apellido2', 'email', 'provider', 'providerId'));
    }

    /**
     * @param Request $request
     */
    public function showLinkAccountForm(Request $request)
    {
        $user = User::whereEmail($request->email)->first();
        $provider = $request->provider;
        $providerId = $request->provider_id;

        return view('auth.link-oauth', compact('user', 'provider', 'providerId'));
    }

    /**
     * @param LinkAccountRequest $request
     */
    public function linkAccountSocial(LinkAccountRequest $request)
    {
        $user = User::find($request->user_id);
        $user->provider = $request->provider;
        $user->provider_id = $request->provider_id;
        $user->save();

        auth()->login($user);

        return redirect()->intended('/');
    }

    /**
     * @param CreateAccountFromSocialRequest $request
     */
    public function createAccountSocial(CreateAccountFromSocialRequest $request)
    {
        event(new Registered($user = $this->create($request->all())));

        $url = '/?altaOk';
        $lang = localization()->getCurrentLocale();
        if ($lang !== 'es') {
            $url = '/'.$lang.'/?altaOk';
        }

        return redirect()->to($url);
    }

    /**
     * Crea un nuevo usuario
     *
     * @param $request
     */
    private function create(array $data)
    {
        if (isset($data['referred'])) {
            session()->flash('referred', $data['referred']);
        }

        return User::create([
            'nombre'       => $data['nombre'],
            'apellido1'    => $data['apellido1'],
            'apellido2'    => $data['apellido2'],
            'email'        => $data['email'],
            'password'     => bcrypt(str_random(8)),
            'provider'     => $data['provider'],
            'provider_id'  => $data['provider_id'],
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
        ]);
    }
}
