<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class BeforeConfirmController extends Controller
{
    
	public function action () {
        try {
        	$user = auth()->user();

        	if ($clickProvider = $user->clickProvider) {

        		$providers = config('clickid.providers');
                
				$currentProvider = $providers[$clickProvider->provider];

                $endpoints = $currentProvider['endpoint'];
                
                if (array_key_exists('before_register', $endpoints)) {
                    $endpoint = str_replace(['click_id', 'user_id', 'provider'], [
                        $clickProvider->click_id,
                        $user->id,
                        $clickProvider->current_provider
                    ], $endpoints['before_register']);

                    $this->emitCallBack($endpoint);
            	}
            }
        } catch (\Throwable $th) {
            logger('CLICK_ID:BEFORE_REGISTER:ERROR');
            logger($th);
        }

 		return redirect()->route('welcome');
	}


    /**
     * Hace un llamado tipo GET a la URL del proveedor
     *
     * @param   string  $endpoint
     *
     * @return  void
     */
    protected function emitCallBack ($endpoint)
    {     
        try {
            $client = new Client();
            $res = $client->request('GET', $endpoint, ['http_errors' => false]);
        } catch (\Exception $e) {
            logger('CLICK_ID:BEFORE_REGISTER:ERROR');
            logger($e);
        }
    }
}
