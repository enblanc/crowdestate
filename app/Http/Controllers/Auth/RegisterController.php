<?php

namespace App\Http\Controllers\Auth;

use Anhskohbo\NoCaptcha\NoCaptcha;
use App\Helpers\TokensHelper;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
     */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    // protected $redirectTo = '/?altaOk';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $referral = false;
        if (session()->has('referred')) {
            $referral = session()->get('referred');
        } elseif (Cookie::has('referral')) {
            $referral = Cookie::get('referral');
        }

        $captcha = new NoCaptcha(env('INVISIBLE_RECAPTCHA_SECRETKEY'), env('INVISIBLE_RECAPTCHA_SITEKEY'));

        return view('auth.register', compact('referral', 'captcha'));
    }

    public function redirectTo()
    {
        $locale = localization()->getCurrentLocale();

        if ($locale !== 'es') {
            return '/'.$locale.'/?altaOk';
        }

        Cookie::queue('localization', $locale, 30 * 120);
        Cookie::queue('locale', $locale, 30 * 120);

        return '/?altaOk';
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nombre'               => 'required|max:255',
            'apellido1'            => 'required|max:255',
            'apellido2'            => 'max:255',
            'email'                => 'required|email|max:255|unique:users',
            'password'             => ['required', 'min:8', 'regex:/^((?=\S*?[a-zA-Z])(?=\S*?[0-9]).{7,})\S$/'],
            'terminos'             => 'required',
            'lang'                 => 'required|in:es,en,de',
            'g-recaptcha-response' => 'required|captcha',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        if (isset($data['referred'])) {
            session()->flash('referred', $data['referred']);
        }

        if (isset($data['newsletter'])) {
            session()->flash('newsletter', true);
        }

        $tribute = false;

        if (isset($data['tribute'])) {
            $tribute = true;
        }

        $lang = $data['lang'];
        App::setLocale($lang);
        localization()->setLocale($lang);

        return User::create([
            'nombre'       => $data['nombre'],
            'apellido1'    => $data['apellido1'],
            'apellido2'    => $data['apellido2'],
            'email'        => $data['email'],
            'password'     => $data['password'],
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
            'locale'       => $lang,
            'tribute'      => $tribute,
        ]);
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        auth()->logout();

        return redirect()->to($this->redirectTo());
    }
}
