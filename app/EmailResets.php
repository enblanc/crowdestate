<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EmailResets extends Model
{
    protected $fillable = [
        'user_id',
        'email',
        'token'
    ];
}
