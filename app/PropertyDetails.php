<?php

namespace App;

use App\Traits\MediaHelpersTrait;
use App\Traits\Searchable;
use App\Transformers\PropertyDetailsTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Webpatser\Uuid\Uuid;

class PropertyDetails extends Model implements HasMediaConversions, Transformable
{
    use HasMediaTrait, MediaHelpersTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'property_id',
        'locale',
        'title',
        'slug',
        'descripcion',
        'ficha',
        'similares_first_name',
        'similares_first_data_1',
        'similares_first_data_2',
        'similares_second_name',
        'similares_second_data_1',
        'similares_second_data_2',
        'invertir_first_title',
        'invertir_first_data',
        'invertir_second_title',
        'invertir_second_data',
        'invertir_third_title',
        'invertir_third_data',
        'localizacion_first_title',
        'localizacion_first_data',
        'localizacion_second_title',
        'localizacion_second_data',
        'localizacion_third_title',
        'localizacion_third_data',
        'mercado_first_title',
        'mercado_first_data',
        'mercado_second_title',
        'mercado_second_data',
        'mercado_third_title',
        'mercado_third_data',
        'rendimiento_mercado',
    ];

    /**
     * Get default collection for media files
     * Is the uuid for 'property_related_1'
     *
     * @var string
     */
    public $mediaRelatedFirstCollection = '74ead808-9ba3-5674-8e58-4a26f8e74ac5';

    /**
     * Get default collection for media files
     * Is the uuid for 'property_related_2'
     *
     * @var string
     */
    public $mediaRelatedSecondCollection = 'd4684d20-310f-5323-86ec-8b1462102324';

    /**
     * The transformer used to transform the model data.
     *
     * @return Transformer|callable|string|null
     */
    public static function transformer()
    {
        return PropertyDetailsTransformer::class;
    }

    /**
     * Set fields be able current search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return [
            'similares_first_name',
            'similares_first_data_1',
            'similares_first_data_2',
            'similares_second_name',
            'similares_second_data_1',
            'similares_second_data_2',
            'invertir_first_title',
            'invertir_first_data',
            'invertir_second_title',
            'invertir_second_data',
            'invertir_third_title',
            'invertir_third_data',
            'localizacion_first_title',
            'localizacion_first_data',
            'localizacion_second_title',
            'localizacion_second_data',
            'localizacion_third_title',
            'localizacion_third_data',
            'mercado_first_title',
            'mercado_first_data',
            'mercado_second_title',
            'mercado_second_data',
            'mercado_third_title',
            'mercado_third_data',
            'rendimiento_mercado',
        ];
    }

    /**
     * Return state for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Set slug for property
     *
     * @param string $value
     *
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * Return mediaId encoded
     *
     * @return string
     */
    public function getMediaIdAttribute(): string
    {
        return Uuid::generate(5, $this->id.$this->user_id, Uuid::NS_DNS);
    }

    /**
     * Return first image
     *
     * @return  string
     */
    public function getFirstRelatedImage(): string
    {
        return $this->getFirstMediaUrl($this->mediaRelatedFirstCollection, 'thumb');
    }

    /**
     * Return second image
     *
     * @return  string
     */
    public function getSecondRelatedImage(): string
    {
        return $this->getFirstMediaUrl($this->mediaRelatedSecondCollection, 'thumb');
    }

    /**
     * Performs conversions for each image
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->setManipulations(['w' => 368, 'h' => 232])
            ->performOnCollections('*');
    }
}
