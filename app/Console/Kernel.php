<?php

namespace App\Console;

use App\Console\Commands\DirectoryCleanupCommand;
use App\Console\Commands\ExcelPrescriptoresCommand;
use App\Console\Commands\FixRegalosInversionTransacciones;
use App\Console\Commands\FixRoleUser;
use App\Console\Commands\PagarPrescriptoresCommand;
use App\Console\Commands\ComprobarVentaInmueble;
use App\Console\Commands\AutoInvestProperty;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // ImportarResumenCommand::class,
        PagarPrescriptoresCommand::class,
        ExcelPrescriptoresCommand::class,
        DirectoryCleanupCommand::class,
        ComprobarVentaInmueble::class,
        AutoInvestProperty::class,
        // CopiarDatosTraduccion::class,
        // SetTributeToSpanishUsers::class,
        // EmailDividendosEmail::class,
        FixRegalosInversionTransacciones::class,
        FixRoleUser::class,

        // \App\Console\Commands\EmailErrorVentaLemonway::class,
        // \App\Console\Commands\EnvioMailsCincoFix::class,
        // \App\Console\Commands\RepartirVentaError::class,
        // FixError::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('queue:work --daemon')->everyMinute()->withoutOverlapping();
        $schedule->command('clean:directories')->daily();
        // Esto se debe poner en los Observadores del modelos correspondientes
        $schedule->command('fix:role:inversor')->daily();

        $schedule->command('autoinvest:property')->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
