<?php

namespace App\Console\Commands;

use App\Notifications\PrescriptorInformeMensualNotification;
use App\Property;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use HttpOz\Roles\Models\Role;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class ExcelPrescriptoresCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crowdestate:excel-prescriptores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía los excel de resumen a los prescriptores';

    /**
     * @var array
     */
    protected $tiposActivos = [
        0  => 'Pago con tarjeta',
        1  => 'Retirada fondos',
        2  => 'Inversión Inmueble',
        3  => 'Ingreso por transferencia',
        4  => 'Devolución',
        13 => 'Reparto Dividendos',
        14 => 'Venta',
        15 => 'Regalo',
        16 => 'Devolución por inmueble no conseguido',
    ];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $prescriptores = Role::find(3)->users;
        $year = Carbon::today()->year;

        $contador = 0;

        foreach ($prescriptores as $prescriptor) {
            $transactions = collect([]);
            if ($prescriptor->referrals->count() > 0) {
                foreach ($prescriptor->referrals as $referido) {
                    $userTrans = $referido->transactions()->whereYear('fecha', $year)->whereIn('type', [0, 1, 2, 3, 4, 13, 14, 15])->whereStatus(3)->get();
                    $transactions->push($userTrans);
                }
                $userTransactions = $transactions->flatten();

                if ($userTransactions->count() > 0) {
                    $excelFile = $this->createExcel($userTransactions, $prescriptor);

                    $appLocale = current_locale();
                    localization()->setLocale($prescriptor->locale);

                    $prescriptor->notify(new PrescriptorInformeMensualNotification($excelFile['full']));

                    localization()->setLocale($appLocale);

                    $contador++;
                }
            }
        }

        $this->info('Se han enviado un total de '.$contador.' informes.');
    }

    /**
     * Genera el excel y devuelve la ruta al excel generado
     *
     * @param  Illuminate\Database\Eloquent\Collection $userTransactions
     * @param  App\User       $user
     *
     * @return array
     */
    private function createExcel(Collection $userTransactions, User $user)
    {
        try {
            $excelFile = Excel::create($user->id, function ($excel) use ($userTransactions, $user) {
                $arrayResume = $this->createArrayData($userTransactions);

                $tiposActivos = $this->tiposActivos;
                $months = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
                $year = Carbon::today()->year;

                $excel->sheet('Resumen Inversiones Clientes', function ($sheet) use ($user, $userTransactions, $arrayResume, $months, $year, $tiposActivos) {
                    $sheet->loadView('excelPrescriptor.page1', compact('user', 'userTransactions', 'arrayResume', 'months', 'year', 'tiposActivos'));
                });

                $excel->sheet('Resumen Global por Cliente', function ($sheet) use ($user, $userTransactions, $arrayResume, $months, $year, $tiposActivos) {
                    $sheet->loadView('excelPrescriptor.page2', compact('user', 'userTransactions', 'arrayResume', 'months', 'year', 'tiposActivos'));
                });
            })->store('xlsx', false, true);

            return $excelFile;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Genera el array usado para rellenar el excel
     *
     * @param  Illuminate\Database\Eloquent\Collection $transactions
     *
     * @return array
     */
    private function createArrayData(Collection $transactions)
    {
        $arrayResume = [];
        foreach ($transactions as $transaction) {
            $userId = $transaction->user_id;

            /* CONSULTA TIPOS */
            $transId = $transaction->id;
            $tipo_mov = $this->tipoMovimiento($transaction);

            $importe = $tipo_mov['importe'];
            $tipo = $tipo_mov['tipo'];
            $month = Carbon::parse($transaction->fecha)->format('m');
            $year = Carbon::parse($transaction->fecha)->format('Y');
            $tipoId = $transaction->type;
            $inmuebleRef = 'NoRef';
            if ($transaction->type == 13) {
                $inmueble = Property::whereHas('users', function ($q) use ($transId) {
                    $q->where('transaction_id', $transId);
                })->first();

                if ($inmueble) {
                    $inmuebleRef = $inmueble->ref;
                }

                // dump($inmueble);
                // dd();
            } else {
                // dump($transaction->extras);
                $inmueble = Property::where('id', $transaction->extras)->first();
                // dump($inmueble);
                if (isset($inmueble)) {
                    $inmuebleRef = $inmueble->ref;
                } else {
                    $inmuebleRef = 'NoRef';
                }
                // dd();
                // $inmuebleRef = $inmueble->ref;
            }

            // dump($transaction->type);
            // dump($inmueble);
            // dump($transId);

            /* ANALISIS DE SI EXISTE EL USUARIO EN EL ARRAY */
            // Eric- Añaduido el iset del año para que no falle
            if (!isset($arrayResume[$userId]) || !isset($arrayResume[$userId]['totales'][$year])) {
                $arrayResume[$userId]['totales'][$year] = $importe;
                $arrayResume[$userId]['totales'][$month] = $importe;
                $arrayResume[$userId]['totales_movimientos'][$tipoId][$year] = $importe;
                $arrayResume[$userId]['totales_movimientos'][$tipoId][$month] = $importe;
                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year] = $importe;
                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month] = $importe;
            } else {
                $importe_ant = $arrayResume[$userId]['totales'][$year];
                $arrayResume[$userId]['totales']['2017'] = $importe + $importe_ant;

                /* TOTALES MESES */

                if (!isset($arrayResume[$userId]['totales'][$month])) {
                    $arrayResume[$userId]['totales'][$month] = $importe;
                } else {
                    $importe_ant_month = $arrayResume[$userId]['totales'][$month];
                    $arrayResume[$userId]['totales'][$month] = $importe + $importe_ant_month;
                };

                /* TOTALES TIPO DE MOVIMIENTO */

                if (!isset($arrayResume[$userId]['totales_movimientos'][$tipoId][$year])) {
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$year] = $importe;
                } else {
                    $importeAntTipoYear = $arrayResume[$userId]['totales_movimientos'][$tipoId][$year];
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$year] = $importe + $importeAntTipoYear;
                };

                if (!isset($arrayResume[$userId]['totales_movimientos'][$tipoId][$month])) {
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$month] = $importe;
                } else {
                    $importeAntTipo = $arrayResume[$userId]['totales_movimientos'][$tipoId][$month];
                    $arrayResume[$userId]['totales_movimientos'][$tipoId][$month] = $importe + $importeAntTipo;
                };

                /* TOTALES INVERSIONES */

                if (!isset($arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year])) {
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year] = $importe;
                } else {
                    $importeAntInversionYear = $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year];
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$year] = $importe + $importeAntInversionYear;
                };

                if (!isset($arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month])) {
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month] = $importe;
                } else {
                    $importeAntInversion = $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month];
                    $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef][$month] = $importe + $importeAntInversion;
                };

                // OBJETIVO DE INVERSIÓN DE LOS INMUEBLES

                $arrayResume[$userId]['totales_movimientos_inmuebles'][$tipoId][$inmuebleRef]['objetivo'] = $inmueble['objetivo'];

                // COMISIONES DE LOS MOVIMIENTOS

                if (!isset($arrayResume[$userId]['totales_comisiones'][$tipoId][$year])) {
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$year] = abs($transaction->prescriptor);
                } else {
                    $importeAntComision = $arrayResume[$userId]['totales_comisiones'][$tipoId][$year];
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$year] = abs($transaction->prescriptor) + $importeAntComision;
                };

                if (!isset($arrayResume[$userId]['totales_comisiones'][$tipoId][$month])) {
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$month] = $transaction->prescriptor;
                } else {
                    $importeAntComision = $arrayResume[$userId]['totales_comisiones'][$tipoId][$month];
                    $arrayResume[$userId]['totales_comisiones'][$tipoId][$month] = $transaction->prescriptor + $importeAntComision;
                };
            }
        }

        return $arrayResume;
    }

    /**
     * Devuelve un array con el tipo de transacción y el importe
     *
     * @param  Transaction $transaction
     *
     * @return array
     */
    private function tipoMovimiento(Transaction $transaction)
    {
        switch ($transaction->type) {
            case 0:
                $tipo = 'Pago con tarjeta';
                break;
            case 1:
                $tipo = 'Retirada de fondos';
                break;
            case 2:
                $tipo = 'Inversión en Inmueble';
                break;
            case 3:
                $tipo = 'Ingreso por transferencia';
                break;
            case 4:
                return 'Devolución';
                // Quizá es refond
                break;
            case 13:
                $tipo = 'Divindendos generados por Inmueble';
                break;
            case 14:
                $tipo = 'Venta';
                break;
            case 15:
                $tipo = 'Regalo';
            // no break
            case 16:
                $tipo = 'Devolución por inmueble no conseguido';
                break;
            case 18:
                $tipo = 'Diviendos generados del 5% sobre lo invertido';
                break;
            default:
                $tipo = '';
                break;
        }

        return [
            'tipo'    => $tipo,
            'importe' => $transaction->money,
        ];
    }
}
