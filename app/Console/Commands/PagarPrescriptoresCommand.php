<?php

namespace App\Console\Commands;

use App\Helpers\TokensHelper;
use App\Notifications\PrescriptorImporteReferidosNotification;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use HttpOz\Roles\Models\Role;
use Illuminate\Console\Command;
use Infinety\LemonWay\Facades\LemonWay;
use Jenssegers\Date\Date;

class PagarPrescriptoresCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crowdestate:pagar-prescriptores';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Paga a los prescriptores';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $prescriptores = Role::find(3)->users;

        foreach ($prescriptores as $prescriptor) {
            $porcentajeDividendos = (double) $prescriptor->porcentaje_ganado;
            $porcentajeInvertido = (double) $prescriptor->porcentaje_invertido;
            $totalAPagarDividendos = 0;
            $totalAPagarInvertido = 0;
            $idsTransacciones = [];
            if ($prescriptor->referrals->count() > 0) {
                foreach ($prescriptor->referrals as $referido) {
                    // HAy que comprobar si el usuario tiene inversiones
                    if ($referido->properties->count() > 0) {
                        foreach ($referido->properties as $property) {
                            // Comprobamos que la inversión está pagada
                            if ($property->pivot->abonado === 0) {
                                // dump($property->pivot->prescriptor);
                                //Comprobamos si ya se le ha pagado al prescriptor esa inversión o no.

                                if ($property->pivot->prescriptor === null) {
                                    $aPagarInvertido = round(($porcentajeInvertido * (double) $property->pivot->total) / 100, 2);
                                    $totalAPagarInvertido += $aPagarInvertido;
                                    $referido->properties()->updateExistingPivot($property->id, ['abonado' => 1, 'prescriptor' => $aPagarInvertido]);
                                }
                            }
                            // dump('Total generado por Inversion: '.$aPagarInvertido.' €');

                            // dump('El usuario '.$referido->id.' ha invertido en el inmueble '.$property->nombre);
                            // $today = Carbon::today();
                            // $movimientoDividendos = $referido->transactions()->where('type', 13)->whereRaw('MONTH(fecha)  = ?', $today->month)->whereRaw('YEAR(fecha)  = ?', $today->year)->where('property_user_id', $property->pivot->id)->first();

                            $movimientoDividendos = $referido->transactions()->where('type', 13)->where('property_user_id', $property->pivot->id)->whereNull('prescriptor')->get();
                            // Comprobamos que se le hayan pagado al usuario los dividendos
                            foreach ($movimientoDividendos as $transaccion) {
                                $aPagarAlReferidor = round(($porcentajeDividendos * $transaccion->credit) / 100, 2);
                                $totalAPagarDividendos += $aPagarAlReferidor;
                                $idsTransacciones[$transaccion->id] = $aPagarAlReferidor;
                                // dump('  Hay que pagarle la transacción '.$transaccion->id.': '.$aPagarAlReferidor.' €');
                            }
                        }
                    }
                }

                if ($totalAPagarDividendos !== 0 || $totalAPagarInvertido !== 0) {
                    $totalAPagar = round($totalAPagarDividendos + $totalAPagarInvertido, 2);
                    $totalAPagar = number_format($totalAPagar, 2, '.', '');
                    $this->table(['Variable', 'Valor'], [
                        ['nombre' => 'Porcentaje Dividendos', 'valor' => $porcentajeDividendos.' %'],
                        ['nombre' => 'Dividendos Generados Importe', 'valor' => $totalAPagarDividendos.' €'],
                        ['nombre' => 'Porcentaje Invertido', 'valor' => $porcentajeInvertido.' %'],
                        ['nombre' => 'Cantidad Invertida Importe', 'valor' => $totalAPagarInvertido.' €'],
                        ['nombre' => '------------', 'valor' => '------'],
                        ['nombre' => 'Cantidad Total a Abonar', 'valor' => $totalAPagar.' €'],
                    ]);

                    if ($this->pagarUsuario($prescriptor, $totalAPagar)) {
                        foreach ($idsTransacciones as $transactionId => $importe) {
                            $trans = Transaction::find($transactionId);
                            $trans->prescriptor = $importe;
                            $trans->save();
                        }
                        $fechaMes = Date::today();

                        $appLocale = current_locale();
                        localization()->setLocale($prescriptor->locale);

                        $prescriptor->notify(new PrescriptorImporteReferidosNotification($totalAPagar, ucfirst($fechaMes->format('F'))));

                        localization()->setLocale($appLocale);

                        dump('USUARIO '.$prescriptor->id.' PAGADO! - '.$totalAPagar);
                    }
                }
            }
        }
    }

    /**
     * @param User $user
     */
    private function pagarUsuario(User $user, $total)
    {
        $fechaMes = Date::today();
        $mensaje = 'Dividendos generados de tus referidos. '.ucfirst($fechaMes->format('F')).' de '.$fechaMes->format('Y');

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $user->lemonway_id,
            'type'        => 13,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $total,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'DIV-'),
        ];

        $transaction = $user->transactions()->create($transaction);
        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $result = LemonWay::walletToWallet($walletSC, $user->wallet, $total, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
        } catch (Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }

        return true;
    }
}
