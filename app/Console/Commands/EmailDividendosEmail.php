<?php

namespace App\Console\Commands;

use App\Notifications\UserDividendosNotification;
use App\Notifications\UserDividendosNoTributeNotification;
use App\Property;
use App\Transaction;
use Illuminate\Console\Command;

class EmailDividendosEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:email-dividendos-email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia un email a todos los que les ha fallado el email de notificación';

    /**
     * @var array
     */
    protected $ids = [10834, 10835, 10836, 10837, 10838, 10839, 10840, 10841, 10842, 10843, 10844, 10845, 10846, 10847, 10848, 10849, 10850, 10851, 10852, 10853, 10854, 10855, 10856, 10857, 10858, 10859, 10860, 10861, 10862, 10863, 10864, 10865, 10866, 10867, 10869, 10868, 10870, 10871, 10872, 10873, 10874, 10875, 10876, 10877, 10878, 10879, 10880, 10881, 10882, 10883, 10884, 10885, 10886, 10887, 10888, 10889, 10890, 10891, 10892, 10893, 10894, 10895, 10896, 10897, 10899, 10898, 10900, 10901, 10902, 10903, 10904, 10905, 10906, 10908, 10907, 10909, 10911, 10910, 10912, 10913, 10914, 10915, 10916, 10917, 10918, 10920, 10919, 10921, 10922, 10923, 10924, 10925, 10926, 10927, 10929, 10928, 10930, 10932, 10931, 10933, 10935, 10934, 10936, 10937, 10938, 10939, 10940, 10941, 10942, 10943, 10944, 10945, 10946, 10947, 10948, 10949, 10950, 10951, 10952, 10953, 10954, 10955, 10956, 10957, 10958, 10959, 10960, 10961, 10962, 10963, 10964, 10965, 10966, 10967, 10968, 10969, 10970, 10971, 10972, 10973, 10974, 10975, 10976, 10977, 10978, 10979, 10980, 10981, 10982, 10983, 10984, 10985, 10986, 10987, 10988, 10989, 10990, 10991, 10992, 10993, 10994, 10995, 10996, 10997, 10998, 10999, 11000, 11001, 11002, 11003, 11004, 11005, 11006, 11008, 11007, 11009, 11010, 11011, 11012, 11013, 11014, 11016, 11015, 11017, 11018, 11019, 11020, 11021, 11022, 11023, 11024, 11025, 11026, 11028, 11027, 11029, 11030, 11032, 11031, 11033, 11034, 11035, 11036, 11037, 11038, 11039, 11040, 11041, 11042, 11044, 11043, 11045, 11046, 11047, 11048, 11049, 11050, 11051, 11052, 11053, 11054, 11056, 11055, 11057, 11058, 11060, 11059, 11061, 11062, 11063, 11064, 11065, 11066, 11067, 11068, 11069, 11070, 11072, 11071, 11073, 11074, 11075, 11076, 11077, 11078, 11080, 11079, 11081, 11082, 11084, 11083, 11085, 11086, 11088, 11087, 11089, 11090, 11092, 11091, 11093, 11094, 11096, 11095, 11097, 11099, 11100, 11101, 11104, 11103, 11106, 11107, 11108, 11109, 11112, 11111, 11115, 11116, 11117, 11118, 11120, 11119, 11122, 11123, 11124, 11125, 11127, 11129, 11130, 11132, 11131, 11135, 11136, 11137, 11140, 11141, 11143, 11144, 11145, 11147, 11148, 11150, 11152, 11151, 11153, 11155, 11156, 11157, 11158, 11160, 11159, 11162, 11164, 11163, 11166, 11168, 11167, 11169, 11170, 11171, 11172, 11173, 11174, 11176, 11175, 11177, 11178, 11179, 11180, 11181, 11182, 11184, 11183, 11185, 11186, 11187, 11188, 11189, 11190, 11191, 11192, 11193, 11194, 11195, 11196, 11197, 11198, 11199, 11200, 11201, 11202, 11203, 11204, 11205, 11206, 11208, 11207, 11209, 11210, 11211, 11212, 11213, 11214, 11215, 11216, 11217, 11218, 11220, 11219, 11221, 11222, 11224, 11223, 11225, 11226, 11228, 11227, 11229, 11230, 11231, 11232, 11233, 11235, 11234, 11236, 11238, 11237, 11239, 11240, 11241, 11242, 11244, 11243, 11245, 11246, 11247, 11248, 11250, 11249, 11251, 11252, 11253, 11254, 11256, 11255, 11257, 11259, 11258, 11260, 11261, 11262, 11263, 11265, 11264, 11266, 11267, 11268, 11269, 11270, 11271, 11272, 11273, 11274, 11275, 11276, 11277, 11278, 11279, 11280, 11281, 11282, 11283, 11284, 11285, 11286, 11287, 11288, 11289, 11290, 11291, 11292, 11293, 11294, 11295, 11296, 11297, 11298, 11299, 11300, 11301, 11302, 11303, 11304, 11305, 11306, 11307, 11308, 11310, 11339, 11340, 11341, 11342, 11343, 11344, 11345, 11346, 11347, 11348, 11349, 11350, 11351, 11352, 11353, 11354, 11355, 11356, 11357, 11358];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions = Transaction::whereIn('id', $this->ids)->get();

        if ($this->confirm('Se van a enviar '.count($transactions).' emails de dividendos. ¿Estás de acuerdo?')) {
            if ($this->confirm('¿Estás de acuerdo?')) {
                $this->enviarCorreos($transactions);
                $this->info('Proceso finalizado');
            }
        }
    }

    /**
     * @param $transacciones
     */
    private function enviarCorreos($transacciones)
    {
        foreach ($transacciones as $transaction) {
            try {
                $property = Property::find($transaction->extras);
                $user = $transaction->user;
                $mes = 'Febrero';

                if ($property) {
                    $appLocale = current_locale();
                    localization()->setLocale($transaction->user->locale);

                    if ($user->tribute) {
                        $user->notify(new UserDividendosNotification($property, $transaction->credit, $mes));
                    } else {
                        $user->notify(new UserDividendosNoTributeNotification($property, $transaction->credit, $mes));
                    }

                    localization()->setLocale($appLocale);

                    $this->info('Email enviado correcto al usuario: '.$transaction->user_id.' - '.$transaction->user->nombreCompleto.' - '.$transaction->credit.'€ - Transacción:'.$transaction->id);
                    logger('Email enviado correcto al usuario: '.$transaction->user_id.' - '.$transaction->user->nombreCompleto.' - '.$transaction->credit.'€ - Transacción:'.$transaction->id);
                }
            } catch (\Exception $e) {
                dump($e->getMessage());
                $this->error('Ha fallado enviar la notificación a la transacción: '.$transaction->id);

                return true;
            }

            sleep(3);
        }
    }

    /**
     * @param $id
     */
    private function getProperty($id)
    {
        return Property::findOrFail($id);
    }
}
