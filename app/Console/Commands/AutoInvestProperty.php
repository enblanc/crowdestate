<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use App\Jobs\AutoInvestPropertyJob;
use App\Property;

class AutoInvestProperty extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'autoinvest:property  {--test}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Lanzar el Autoinvest de tipo Propiedad';

    /**
     * Properties
     *
     * @var collect(Property)
     */
    protected $properties;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->properties = $this->getProperties();

        foreach ($this->properties as $key => $property) {
            $job = (new AutoInvestPropertyJob($property, $this->option('test')))->delay(Carbon::now()->addMinutes($key * 2));

            dispatch($job);

            $this->info("AutoInvest de la propiedad $property->nombre ({$property->id}) fue lanzado");
        }

        $this->info("Total de propiedades que lanzarán AutoInvests => ({$this->properties->count()})");

        return;
    }

    /**
     * Obtener las propiedades para el AutoInvest
     *
     * @return void
     */
    protected function getProperties ()
    {
        return Property::where('property_state_id', 2)->orderByRaw('- orden DESC')->get()->filter(function ($item) {
            return $item->publicado === true && $item->private === false;
        });
    }
}
