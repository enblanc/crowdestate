<?php

namespace App\Console\Commands;

use App\Helpers\TokensHelper;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Console\Command;

class FixRegalosInversionTransacciones extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:regalos-inversion';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando fix para añadir las transacciones olvidadas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transactions = Transaction::type(15)->get();
        $total = count($transactions);

        if ($this->confirm('Se van a crear '.$total.' transacciones. ¿Estás de acuerdo?')) {
            foreach ($transactions as $transaction) {
                $this->info('Creando regalo para la transaccion '.$transaction->id.' - Usuario: '.$transaction->user_id);
                $this->createGiftTransaction($transaction);
            }

            $this->info('Proceso finalizado correctamente');
        }
    }

    /**
     * @param Transaction $transactionDone
     */
    private function createGiftTransaction(Transaction $transactionDone)
    {
        $fecha = Carbon::parse($transactionDone->fecha)->subSecond();

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $transactionDone->lemonway_id,
            'type'        => 22,
            'status'      => 3,
            'fecha'       => $fecha->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $transactionDone->debit,
            'iban_id'     => null,
            'comment'     => 'Regalo de Brickstarter',
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'REG-'),
            'extras'      => $transactionDone->id,
            'updated_at'  => $transactionDone->updated_at->subSecond(),
            'created_at'  => $transactionDone->created_at->subSecond(),
        ];

        $transaction = $transactionDone->user->transactions()->create($transaction);
    }
}
