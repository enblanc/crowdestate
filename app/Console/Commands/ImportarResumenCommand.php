<?php

namespace App\Console\Commands;

use App\Property;
use Illuminate\Console\Command;

class ImportarResumenCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'crowdestate:resumen {propertyId} {filePath}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importar resumen para la propiedad';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->checkArguments();
    }

    private function checkArguments()
    {
        $propertyId = $this->argument('propertyId');

        //Comprobamos que exista en la DB
        $property = Property::find($propertyId);
        if (! $property) {
            $this->error('El inmueble no existe');
        }
    }
}
