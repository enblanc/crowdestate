<?php

namespace App\Console\Commands;

use App\Helpers\TokensHelper;
use App\Property;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class RepartirVentaError extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:repartir-venta-error
                            {force : Solo si se añade la opción lo lanza}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reparte los dividendos generados por error solo en lemonway';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!$this->checkArguments()) {
            $this->error('Hay que fozarlo con "true"');

            return false;
        }

        $this->importaExcel();
        $this->info('Proceso finalizado');
    }

    private function importaExcel()
    {
        $property = Property::find(15);

        $file = storage_path('excel_errores_venta.csv');

        Excel::setDelimiter(';');
        $filas = Excel::load($file)->get();

        $walletsNotFound = [];

        foreach ($filas as $fila) {
            $user = User::where('lemonway_id', $fila->wallet)->first();
            if ($user === null) {
                $walletsNotFound[] = $fila->wallet;
            }
            $dividendos = number_format($this->convertToDouble($fila->venta), 2, '.', '');

            // $this->creaTransaccion($user, $property, $dividendos);

            $transaction = $user->transactions()->whereType(14)->first();

            $inversion = $this->getTotalInvertidoPorInmueble($user, $property);
            if ($transaction) {
                $transaction->credit = $transaction->credit + $inversion;
                $transaction->save();
            }

            $this->info($user->id.' - '.$user->nombreCompleto.': '.$inversion.' €');
        }
        if (count($walletsNotFound) > 0) {
            dump($walletsNotFound);
        }
    }

    /**
     * @param $user
     * @param $inmueble
     * @return mixed
     */
    private function getTotalInvertidoPorInmueble($user, $inmueble)
    {
        $total = 0;
        foreach ($user->properties as $property) {
            if ($property->id === $inmueble->id) {
                $total += $property->pivot->total;
            }
        }

        return $total;
    }

    /**
     * @param $user
     * @param $property
     * @param $total
     */
    private function creaTransaccion($user, $property, $cantidad)
    {
        $mensaje = __('Diviendos generados de la venta del inmueble:').' '.$property->nombre;

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $user->lemonway_id,
            'type'        => 14,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidad,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'VEN-'),
            'extras'      => $property->id,
            'status'      => 3,
            'lemonway_id' => 9999,
        ];

        $transaction = $user->transactions()->create($transaction);
    }

    /**
     * @return  [type]  [description]
     */
    private function checkArguments()
    {
        $force = $this->argument('force');

        if ($force === 'true') {
            return true;
        }

        return false;
    }

    /**
     * @param $string
     */
    public static function convertToDouble($string)
    {
        // Limpiamos la variable
        $string = trim(str_replace(',', '.', $string));

        return round($string, 2);
    }
}
