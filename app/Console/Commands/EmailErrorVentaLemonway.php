<?php

namespace App\Console\Commands;

use App\Notifications\DividendosVentaNotificacion;
use App\Property;
use App\Transaction;
use Illuminate\Console\Command;

class EmailErrorVentaLemonway extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:email-venta-error
                            {property : The ID of the property}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envía un correo avisando del pago a los que ha dado error el proceso de venta por Lemonway';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $propertyId = $this->argument('property');
        $property = $this->getProperty($propertyId);

        $this->info('Se va a enviar un email a los inversores del inmueble '.$property->nombre.' a los que no se les pudo pagar por lemonway debido a los límites.');
        if ($this->confirm('¿Has enviado los pagos manualmente mediante Lemonway?')) {
            $transactionsWithErrors = Transaction::whereTypeAndExtrasAndStatus(14, $property->id, 4)->get();
            if ($transactionsWithErrors->count() > 0) {
                foreach ($transactionsWithErrors as $transaction) {
                    $this->info('Se enviará un email a: '.$transaction->user->nombreCompleto.' informando del pago de: '.$transaction->credit.' €');
                }

                if ($this->confirm('¿Estás de acuerdo?')) {
                    $this->enviarCorreos($property, $transactionsWithErrors);
                    $this->info('Proceso finalizado');
                }
            }
        }
    }

    /**
     * @param $transacciones
     */
    private function enviarCorreos($property, $transacciones)
    {
        foreach ($transacciones as $transaction) {
            try {
                $appLocale = current_locale();
                localization()->setLocale($transaction->user->locale);
                $transaction->user->notify(new DividendosVentaNotificacion($property, $transaction->credit));
                localization()->setLocale($appLocale);

                $this->info('Email enviado correcto al usuario: '.$transaction->user_id.' - '.$transaction->user->nombreCompleto);
            } catch (\Exception $e) {
                $this->error('Ha fallado enviar la notificación a la transacción: '.$transaction->id);

                return true;
            }
        }
    }

    /**
     * @param $id
     */
    private function getProperty($id)
    {
        return Property::findOrFail($id);
    }
}
