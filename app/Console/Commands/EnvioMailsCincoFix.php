<?php

namespace App\Console\Commands;

use App\Notifications\FivePercentNotification;
use App\Property;
use App\Transaction;
use Illuminate\Console\Command;

class EnvioMailsCincoFix extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mails-cinco-fix';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envia un correo con las cantidades correctas';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transacciones = Transaction::where('type', 18)->where('extras', 51)->get();

        $property = Property::find(51);

        $totalEnvios = $transacciones->count();

        $this->info("Se van a enviar $totalEnvios emails");

        $totalAbonado = 0;
        $count = 0;

        foreach ($transacciones as $transaction) {
            $user = $transaction->user;
            $abonado = $transaction->credit;

            $resto = (19 * $abonado) / 81;

            $abonado = number_format($abonado, 2, '.', '');
            $resto = number_format($resto, 2, '.', '');

            $count++;
            $totalAbonado += $abonado;

            //Notificamos al usuario
            try {
                $appLocale = current_locale();
                localization()->setLocale($user->locale);
                $user->notify(new FivePercentNotification($property, $abonado, $resto));
                localization()->setLocale($appLocale);

                $userLocale = strtoupper($user->locale);

                $this->info("[OK][$count] $user->nombre $user->apellido1 - Abonado: $abonado - Retenido: $resto - Idioma: $userLocale");
            } catch (\Exception $e) {
                $this->error("[ERROR] $user->nombre $user->apellido1 - Abonado: $abonado - Retenido: $resto");
            }

            sleep(5);
        }

        $this->info('Total abonado: '.$totalAbonado.' €');

        $this->info('Correos enviados');
    }
}
