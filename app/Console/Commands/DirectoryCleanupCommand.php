<?php

namespace App\Console\Commands;

use App\Helpers\DirectoryCleaner;
use Illuminate\Console\Command;

class DirectoryCleanupCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'clean:directories';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clean up directories.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->comment('Cleaning directories...');

        $directories = collect(config('directory-cleanup.directories'));

        collect($directories)->each(function ($config, $directory) {
            $this->deleteFilesIfOlderThanMinutes($directory, $config['deleteAllOlderThanMinutes']);
        });

        $this->comment('All done!');
    }

    /**
     * @param string $directory
     * @param int $minutes
     */
    protected function deleteFilesIfOlderThanMinutes(string $directory, int $minutes)
    {
        $deletedFiles = app(DirectoryCleaner::class)
            ->setDirectory($directory)
            ->deleteFilesOlderThanMinutes($minutes);

        $this->info("Deleted {$deletedFiles->count()} file(s) from {$directory}.");
    }
}
