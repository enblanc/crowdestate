<?php

namespace App\Console\Commands;

use App\Blog\Models\CategoriesTranslation;
use App\Blog\Models\PostTranslation;
use App\Property;
use Illuminate\Console\Command;

class CopiarDatosTraduccion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'traducciones:arreglar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mueve los datos de una tablas a otras para que las traducciones funcionen correctamente';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $properties = Property::all();

        foreach ($properties as $property) {
            $this->copyDescriptionAndFicha($property);
            $this->documentsToLocale($property);
        }
        $this->info('Datos de los inmuebles copiados');

        $categories = CategoriesTranslation::all();
        foreach ($categories as $category) {
            $this->createCategoryInEnglish($category);
        }
        $this->info('Categorias del blog a inglés');

        $posts = PostTranslation::all();
        foreach ($posts as $post) {
            $this->createPostsInEnglish($post);
        }
        $this->info('Entradas del blog a inglés');
    }

    /**
     * Copia la descripción y la ficha a los detalles
     *
     * @param $property
     */
    private function copyDescriptionAndFicha($property)
    {
        $this->info($property->id.' - '.$property->nombre);
        $descripcion = $property->descripcion;
        $ficha = $property->ficha;

        $details = $property->details()->where('locale', 'es')->first();

        if (empty($details->descripcion)) {
            $details->descripcion = $descripcion;
        }

        if (empty($details->ficha)) {
            $details->ficha = $ficha;
        }

        $details->save();

        $this->info('   '.$property->id.' - '.$property->nombre.' - Datos Movidos');
    }

    /**
     * @param $property
     */
    private function documentsToLocale($property)
    {
        //Public Documents
        $publicDocuments = $property->getMedia($property->mediaCollectionDocuments, ['visibility' => 'public']);
        if ($publicDocuments) {
            foreach ($publicDocuments as $publicDoc) {
                $publicDoc->setCustomProperty('locale', 'es');
                $publicDoc->save();
            }
        }

        //Private Documents
        $privateDocuments = $property->getMedia($property->mediaCollectionDocuments, ['visibility' => 'private']);
        if ($privateDocuments) {
            foreach ($privateDocuments as $privateDoc) {
                $privateDoc->setCustomProperty('locale', 'es');
                $privateDoc->save();
            }
        }

        $this->info('   '.$property->id.' - '.$property->nombre.' - Documentos actualizados');
    }

    /**
     * @param $category
     */
    private function createCategoryInEnglish($category)
    {
        $nueva = $category->replicate();
        $nueva->locale = 'en';
        $nueva->save();
    }

    /**
     * @param $post
     */
    private function createPostsInEnglish($post)
    {
        $nuevo = $post->replicate();
        $nuevo->locale = 'en';
        $nuevo->save();
    }
}
