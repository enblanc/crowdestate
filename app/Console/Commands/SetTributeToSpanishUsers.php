<?php

namespace App\Console\Commands;

use App\User;
use Illuminate\Console\Command;

class SetTributeToSpanishUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'users:tribute';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if need spanish tribute';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $spanishUsers = User::whereHas('profile', function ($query) {
            return $query->where('pais', 'es');
        })->get();

        foreach ($spanishUsers as $user) {
            $user->tribute = true;
            $user->save();
        }

        $this->info('Spanish users tributting');
    }
}
