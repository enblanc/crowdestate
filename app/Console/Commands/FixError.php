<?php

namespace App\Console\Commands;

use App\Transaction;
use Illuminate\Console\Command;
use Infinety\LemonWay\Facades\LemonWay;

class FixError extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:error';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $transacciones = Transaction::whereType(13)->where('status', 3)->where('extras', 15)->whereRaw('Date(created_at) = CURDATE()')->get();

        $total = 0;

        foreach ($transacciones as $transaction) {
            $user = $transaction->user;
            $total += $transaction->credit;

            $cantidadDevolver = number_format($transaction->credit, 2, '.', '');

            $walletSC = LemonWay::getWalletDetails(null, 'SC');
            $result = LemonWay::walletToWallet($user->wallet, $walletSC, $cantidadDevolver, 'Reembolso fix');

            if ($result->STATUS == 3) {
                $transaction->delete();
            }
        }

        dump('Proceso acabado. Se ha devuelto: '.$total);
    }
}
