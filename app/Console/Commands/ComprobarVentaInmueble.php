<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Property;
use App\PropertyUser;

/**
 * Comando para testear el porcentaje de la devolucion inmueble.
 * Tarea de ClickUp https://app.clickup.com/t/qt9uyh
 * @author Carlos Anselmi <anselmi.dev@gmail.com>
 */
class ComprobarVentaInmueble extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'comprobar:inmueble {--property_id=*} {--simple} {--logger}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para comprobar la venta de un inmueble.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info(PHP_EOL);

        // Obtengo el id de la propiedad ingresada en el comando
        if ($property_id = $this->option('property_id')) {        
            $property_id = $property_id[0];
            
            if ($property = $this->getProperty($property_id)) {
                // última evolución subida.
                $evolucion = $property->evolutions()->get()->last();
                if ($evolucion) {
                    $this->info("_____________________________________________________");
                    foreach ($property->usersGroup as $user) {
                        $this->monto_abonar($property, $user, $evolucion);
                    }
                    $this->info(PHP_EOL."TOTAL DE INVERSORES PROCESADOS: ".count($property->usersGroup));

                } else {
                    $this->info("[ERROR: La propiedad no posee una última evolución]");
                }
            }

        } else {
            $this->info("Debe ingresar un ID de una propiedad para consultar '--property_id=15'");
        }    
    }

    /**
     * Obtiene la propiedad mediente el ID de la propiedad
     * 
     * @param $id El id de la propiead a buscar.
     * @return \App\Property
     */
    private function getProperty($id)
    {
        try {
            // Obtengo la propiedad mediante el ID
            $property = Property::findOrFail($id);

            $this->info("PROPIEDAD: {$property->nombre}". PHP_EOL);
            if ($this->option('logger'))
                logger("PROPIEDAD: {$property->nombre}". PHP_EOL);

            return $property;
        } catch (\Throwable $th) {
            $this->info("No se encontro ninguna propiedad con el ID:{$property_id}". PHP_EOL);
            return null;
        }
    }

    /**
     * Obtiene la cantidad del monto a abonar
     *
     * @param   \App\Property  $property       Propiedad
     * @param   \App\User  $user           Usuario de la
     * @param   Object  $evolucion 
     * @return  Void
     */
    private function monto_abonar ($property, $user, $evolucion)
    {
        $totalRepartir = $evolucion->dividendos_abonar;

        $inversiones = PropertyUser::where('property_id', $property->id)->where('user_id', $user->id)->get();

        $cantidadAbonarUsuario = 0;

        $cantidadInvertidaDelUsuario = 0;

        foreach ($inversiones as $inversion) {
            // Explicación : (cantidadInvertida * totalAbonarDelMes) / Objetivo inicial = $cantidad de € a abonar
            $cantidadInvertidaDelUsuario += $inversion->total;
        }

        //Cantidad a abonar al usuario = (La cantidad que invirtió * el total a invertir) / el objetivo inicial
        $cantidadaPorcentaje = ($cantidadInvertidaDelUsuario * $totalRepartir) / $property->objetivo;
        
        $ganado = $cantidadaPorcentaje - $cantidadInvertidaDelUsuario;
        
        $cantidadAbonarUsuario = $cantidadaPorcentaje;
        
        if ($user->tribute) {
            $ganado = $ganado * 0.81;
        }
        
        $cantidadAbonarUsuario = $cantidadInvertidaDelUsuario + $ganado;
        if (!$this->option('simple')) {
            $this->print("USUARIO: {$user->nombre} <TRIBUTE:{$user->tribute}> <id:{$user->id}>");
            $this->print("INVERSION (Cantidad invertida por el usuario): ".PHP_EOL.">>{$cantidadInvertidaDelUsuario}");
            $this->print("PRECIO DE VENTA (total a repartir): ".PHP_EOL.">>{$totalRepartir}");
            $this->print("PRECIO INICIAL (Objetivo): ".PHP_EOL.">>{$property->objetivo}");
            $this->print("CANTIDAD A PORCENTAJE ((INVERSION * PRECIO DE VENTA) / PRECIO INICIAL): ".PHP_EOL.">>{$cantidadaPorcentaje}");
            $this->print("GANADO (CANTIDAD A PORCENTAJE - INVERSION): ".PHP_EOL.">>{$ganado}");
            $this->print("CANTIDAD A ABONAR: ".PHP_EOL.">>{$cantidadAbonarUsuario}");
            $this->print("-----------------------------------------------------". PHP_EOL);
        } else {
            $this->print(PHP_EOL."USUARIO: {$user->nombre}");
            $this->print("INVIRTIO: {$cantidadInvertidaDelUsuario}");
            $this->print("CANTIDAD A ABONAR: {$cantidadAbonarUsuario}");
        }
    }

    /**
     * Imprime el texto ya sea por consola o por log dependiendo del parametro
     *
     * @param   String  $text 
     * @return  Void
     */
    private function print ($text)
    {
        if ($this->option('logger')) {
            logger($text);
        } else {
            $this->info($text);
        }
    }
}
