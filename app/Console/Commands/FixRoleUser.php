<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use HttpOz\Roles\Models\Role;
use App\User;

class FixRoleUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:role:inversor';

    /**
     * Users
     *
     * @var mixed
     */
    protected $users;

    /**
     *
     * @var Role
     */
    protected $inversorRole;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Hay usuarios que SOLO invierten en el mercado secundario y que no cambia la asignación de su rol. Es decir, cualquier user que hay invertido en el Marketplace o que tenga inversiones, que tenga rol inversor=4';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->users = User::whereNotNull('lemonway_id')->get();

        $this->inversorRole = Role::whereSlug('inversor')->first();

        if (!$this->inversorRole)
            $this->error("Rol no encontrado");

        $this->info("Usuarios a procesar => {$this->users->count()}");
        $progressBar = $this->output->createProgressBar($this->users->count());
        $progressBar->setFormat("%message%\n %current%/%max% [%bar%] %percent:3s%%");
        
        $processed = 0;
        foreach ($this->users as $key => $user) {
            if ($this->verification($user)) {
                $user->attachRole($this->inversorRole);
                $processed++;
            }
            $progressBar->setMessage("Usuario: {$user->email}");
            $progressBar->advance();
        }

        $progressBar->setMessage("Total de Usuarios procesados => {$processed}");
        $progressBar->finish();

        $this->info(PHP_EOL."Total de Usuarios procesados => {$processed}");
    }

    /**
     * Verificar si el usuario se debe agregar el rol 'inversor'
     *
     * @param User $user
     * @return void
     */
    protected function verification ($user)
    {
        // Que no posea el rol 'inversor'
        if ($user->hasRole('inversor'))
            return false;

        // Que sea de rol 'User'
        if (!$user->hasRole('user'))
            return false;

        // 2 : __('Inversión en Inmueble')
        // 13: __('Divindendos generados por Inmueble')
        // 18: __('Diviendos generados del 5% sobre lo invertido en')
        // 20: __('Inversión Inmueble (Mercado)')
        // 21: __('Desinversión inmueble (Mercado)')
        if ($user->transactions()->whereIn('type', [2, 13, 18, 20, 21])->count())
            return true;

        return false;
    }
}
