<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;
use App\Traits\UserClickIdEventsTrait;

class RegisterClickId
{
    use UserClickIdEventsTrait;

    public function handle(Registered $event)
    {
        $this->callEventRegisterClickId($event->user);
    }
}
