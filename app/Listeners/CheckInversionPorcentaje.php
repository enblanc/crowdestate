<?php

namespace App\Listeners;

use App\Events\UserInvierteInmueble;
use App\Mail\InversionInmueblePorcentajeMail;
use Illuminate\Support\Facades\Mail;

class CheckInversionPorcentaje
{

    /**
     * @var int
     */
    protected $cien = 1;

    /**
     * @var int
     */
    protected $ochenta = 2;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserInvierteInmueble  $event
     * @return void
     */
    public function handle(UserInvierteInmueble $event)
    {
        $property = $event->property;

        if ($property->porcentajeCompletado == 100) {

            // Comprobamos si ya se le ha enviado la notificación
            if ($property->mailNotifications()->whereType($this->cien)->first() != null) {

                return false;
            }

            //mail está al 100%;
            Mail::to('contacto@brickstarter.com')->send(new InversionInmueblePorcentajeMail($property, $property->porcentajeCompletado));

            $property->mailNotifications()->create(['type' => $this->cien]);

            return;
        }

        if ($property->porcentajeCompletado >= 80) {

            // Comprobamos si ya se le ha enviado la notificación
            if ($property->mailNotifications()->whereType($this->ochenta)->first() != null) {

                return false;
            }

            Mail::to('contacto@brickstarter.com')->send(new InversionInmueblePorcentajeMail($property, $property->porcentajeCompletado));

            $property->mailNotifications()->create(['type' => $this->ochenta]);

            return;
        }
    }
}
