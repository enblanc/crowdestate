<?php

namespace App\Listeners;

use App\ReferralGift;
use App\Traits\UserRegistrationEventsTrait;
use App\User;
use HttpOz\Roles\Models\Role;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Cookie;

class UserRegisteredActionsListener
{
    use UserRegistrationEventsTrait;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        $this->performEventsActions($event->user);
    }
}
