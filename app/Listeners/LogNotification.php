<?php

namespace App\Listeners;

use App\Notifications\PrescriptorInformeMensualNotification;
use Illuminate\Notifications\Events\NotificationSent;

class LogNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NotificationSent  $event
     * @return void
     */
    public function handle(NotificationSent $event)
    {
        // Si la notificación corresponde al Informe Mensual enviado a los prescriptores borramos el archivo del storage
        if ($event->notification instanceof PrescriptorInformeMensualNotification) {
            $file = $event->notification->file;
            @unlink($file);
        }
    }
}
