<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\Events\MediaHasBeenAdded;

class MediaLogger
{
    /**
     * Handle the event.
     *
     * Saves url of asset to the model
     *
     * @param  SpatieMediaLibraryEventsMediaHasBeenAdded  $event
     * @return void
     */
    public function handle(MediaHasBeenAdded $events)
    {
        foreach ($events as $event) {
            $model = app($event->model_type)->find($event->model_id);
            $image = $event->getUrl();
            $path = dirname($event->getPath());
            try {
                @chmod(public_path('media'), 0777);
                @chmod($path, 0777);
            } catch (Exception $e) {
                Log::info('Error al agregar los permisos de la carpeta: '.$path);
            }
            $convert = $event->getCustomProperty('setModel', false);
            if ($convert) {
                $collection = $event->collection_name;
                if (isset($model->$collection)) {
                    $model->$collection = asset($image);
                    $model->save();
                }
            }
        }
    }
}
