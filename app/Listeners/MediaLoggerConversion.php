<?php

namespace App\Listeners;

use Illuminate\Support\Facades\Log;
use Spatie\MediaLibrary\Events\ConversionHasBeenCompleted;
use Spatie\MediaLibrary\PathGenerator\PathGeneratorFactory;

class MediaLoggerConversion
{
    /**
     * Handle the event.
     *
     * Saves url of asset to the model
     *
     * @param  Spatie\MediaLibrary\Events\ConversionHasBeenCompleted  $event
     * @return void
     */
    public function handle(ConversionHasBeenCompleted $event)
    {
        $pathGenerator = PathGeneratorFactory::create();
        $media = $event->media;
        $conversionPath = $pathGenerator->getPathForConversions($media);

        $conversionPath = public_path('media'.DIRECTORY_SEPARATOR.$conversionPath);
        try {
            @chmod(public_path('media'), 0777);
            @chmod($conversionPath, 0777);
            Log::info($conversionPath);
        } catch (Exception $e) {
            Log::info('Error al agregar los permisos de la carpeta: '.$conversionPath);
        }
    }
}
