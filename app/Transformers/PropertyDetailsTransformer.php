<?php

namespace App\Transformers;

use App\PropertyDetails;
use Flugg\Responder\Transformer;

class PropertyDetailsTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  PropertyDetails $propertyDetails
     * @return array
     */
    public function transform(PropertyDetails $propertyDetails): array
    {
        return [
            'id'                        => (int) $propertyDetails->id,
            'locale'                    => (string) $propertyDetails->locale,
            'similares_first_name'      => (string) $propertyDetails->similares_first_name,
            'similares_first_data_1'    => (string) $propertyDetails->similares_first_data_1,
            'similares_first_data_2'    => (string) $propertyDetails->similares_first_data_2,
            'similares_first_image'     => (string) $propertyDetails->getFirstMediaUrl($propertyDetails->mediaRelatedFirstCollection, 'thumb'),
            'similares_second_name'     => (string) $propertyDetails->similares_second_name,
            'similares_second_data_1'   => (string) $propertyDetails->similares_second_data_1,
            'similares_second_data_2'   => (string) $propertyDetails->similares_second_data_2,
            'similares_second_image'    => (string) $propertyDetails->getFirstMediaUrl($propertyDetails->mediaRelatedSecondCollection, 'thumb'),
            'invertir_first_title'      => (string) $propertyDetails->invertir_first_title,
            'invertir_first_data'       => (string) $propertyDetails->invertir_first_data,
            'invertir_second_title'     => (string) $propertyDetails->invertir_second_title,
            'invertir_second_data'      => (string) $propertyDetails->invertir_second_data,
            'invertir_third_title'      => (string) $propertyDetails->invertir_third_title,
            'invertir_third_data'       => (string) $propertyDetails->invertir_third_data,
            'localizacion_first_title'  => (string) $propertyDetails->localizacion_first_title,
            'localizacion_first_data'   => (string) $propertyDetails->localizacion_first_data,
            'localizacion_second_title' => (string) $propertyDetails->localizacion_second_title,
            'localizacion_second_data'  => (string) $propertyDetails->localizacion_second_data,
            'localizacion_third_title'  => (string) $propertyDetails->localizacion_third_title,
            'localizacion_third_data'   => (string) $propertyDetails->localizacion_third_data,
            'mercado_first_title'       => (string) $propertyDetails->mercado_first_title,
            'mercado_first_data'        => (string) $propertyDetails->mercado_first_data,
            'mercado_second_title'      => (string) $propertyDetails->mercado_second_title,
            'mercado_second_data'       => (string) $propertyDetails->mercado_second_data,
            'mercado_third_title'       => (string) $propertyDetails->mercado_third_title,
            'mercado_third_data'        => (string) $propertyDetails->mercado_third_data,
            'rendimiento_mercado'       => (string) $propertyDetails->rendimiento_mercado,
        ];
    }
}
