<?php

namespace App\Transformers;

use App\User;
use Flugg\Responder\Transformer;

class PropertyInversorsTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  User $user
     * @return array
     */
    public function transform(User $user): array
    {
        return [
            'id'              => (int) $user->id,
            'nombre'          => (string) $user->nombre,
            'email'           => (string) $user->email,
            'apellido1'       => (string) $user->apellido1,
            'apellido2'       => (string) $user->apellido2,
            'created_at'      => (string) $user->pivot->created_at->format('d/m/Y h:m'),
            'total'           => (string) $user->pivot->total . ' €',
            'route_edit'      => (string) route('dashboard.users.edit', $user->id),
        ];
    }
}
