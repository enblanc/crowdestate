<?php

namespace App\Transformers;

use App\Property;
use Flugg\Responder\Transformer;

class FrontPropertyTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['evolutions'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  Property $property
     * @return array
     */
    public function transform(Property $property): array
    {
        return [
            'id'                          => (int) $property->id,
            'ref'                         => (string) $property->ref,
            'slug'                        => (string) $property->slug,
            'nombre'                      => (string) $property->nombre,
            'catastro'                    => (string) $property->catastro,
            'ubicacion'                   => (string) $property->ubicacion,
            'barrio'                      => (string) $property->barrio,
            'zona_premium'                => (bool) $property->zona_premium,
            'habitaciones'                => (int) $property->habitaciones,
            'toilets'                     => (int) $property->toilets,
            'metros'                      => (int) $property->metros,
            'gestor'                      => (string) $property->gestor,
            'garaje'                      => (int) $property->garaje,
            'piscina'                     => (int) $property->piscina,
            'ascensor'                    => (int) $property->ascensor,
            'direccion'                   => (string) $property->direccion,
            'lat'                         => (double) $property->lat,
            'lng'                         => (double) $property->lng,
            'antiguedad'                  => ($property->antiguedad == 0) ? null : (int) $property->antiguedad,
            'descripcion'                 => (string) $property->getTranslationOrDefault()->descripcion,
            'ficha'                       => (string) $property->ficha,
            'periodo_financiacion'        => (int) $property->periodo_financiacion,
            'horizonte_temporal'          => (string) $property->horizonte_temporal,
            'valor_mercado'               => (double) $property->valor_mercado,
            'valor_compra'                => (double) $property->valor_compra,
            'coste_total'                 => (double) $property->coste_total,
            'hipoteca'                    => (double) $property->hipoteca,
            'objetivo'                    => (double) $property->objetivo,
            'importe_minimo'              => (int) $property->importe_minimo,
            'rentabilidad_alquiler_bruta' => (double) $property->rentabilidad_alquiler_bruta,
            'rentabilidad_alquiler_neta'  => (double) $property->rentabilidad_alquiler_neta,
            'rentabilidad_objetivo_bruta' => (double) $property->rentabilidad_objetivo_bruta,
            'rentabilidad_objetivo_neta'  => (double) $property->rentabilidad_objetivo_neta,
            'rentabilidad_anual'          => (double) $property->rentabilidad_anual,
            'rentabilidad_total'          => (double) $property->rentabilidad_total,
            'tasa_interna_rentabilidad'   => (double) $property->tasa_interna_rentabilidad,
            'coste_obras'                 => (double) $property->coste_obras,
            'impuestos_tramites'          => (double) $property->impuestos_tramites,
            'registro_sociedad'           => (double) $property->registro_sociedad,
            'otros_costes'                => (double) $property->otros_costes,
            'imprevistos'                 => (double) $property->imprevistos,
            'caja_minima'                 => (double) $property->caja_minima,
            'honorarios'                  => (double) $property->honorarios,
            'ingresos_anuales_alquiler'   => (double) $property->ingresos_anuales_alquiler,
            'importe_venta'               => (double) $property->importe_venta,
            'compra_venta_margen_bruto'   => (double) $property->compra_venta_margen_bruto,
            'currentEvolution'            => (array) $property->currentEvolution,
            'estado'                      => (bool) $property->estado,
            'resumen'                     => (bool) $property->resumen,
            'fecha_publicacion'           => (string) $property->fecha_publicacion->format('d/m/Y'),
            'porcentajeCompletado'        => (double) $property->porcentajeCompletado,
        ];
    }
}
