<?php

namespace App\Transformers;

use App\Media;
use App\Property;
use Flugg\Responder\Transformer;

class MediaTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * @var mixed
     */
    protected $property;

    /**
     * construct
     */
    public function __construct()
    {
        $this->property = new Property();
    }

    /**
     * Transform the model data into a generic array.
     *
     * @param  AppMedia $appMedia
     * @return array
     */
    public function transform(Media $media): array
    {
        $data = [
            'id'                => (int) $media->id,
            'model_id'          => (int) $media->model_id,
            'model_type'        => (string) $media->model_type,
            'collection_name'   => (string) $media->collection_name,
            'name'              => (string) $media->name,
            'file_name'         => (string) $media->file_name,
            'url'               => (string) $media->getUrl(),
            'custom_properties' => (array) $media->custom_properties,
            'order_column'      => (string) $media->order_column,
            'created_at'        => $media->created_at,
            'updated_at'        => $media->updated_at,
        ];

        if ($media->collection_name == $this->property->mediaCollection || $media->collection_name == $this->property->mediaCollectionDocuments) {
            $data['thumb'] = $media->getUrl('thumb');
        }

        if ($media->collection_name == $this->property->mediaCollection) {
            $data['text'] = $media->getCustomProperty('text', '');
        }

        if ($media->hasCustomProperty('locale')) {
            $data['locale'] = $media->getCustomProperty('locale', '');
        }

        return $data;
    }
}
