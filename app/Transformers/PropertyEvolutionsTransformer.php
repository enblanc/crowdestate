<?php

namespace App\Transformers;

use App\PropertyEvolutions;
use Flugg\Responder\Transformer;

class PropertyEvolutionsTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  PropertyEvolutions $propertyEvolutions
     * @return array
     */
    public function transform(PropertyEvolutions $evolution): array
    {
        return [
            'id'                             => (int) $evolution->id,
            'fecha'                          => (string) $evolution->fecha->format('Y-m-d'),
            'tipo'                           => (string) $evolution->tipo,
            'numero_reservas'                => (double) $evolution->numero_reservas,
            'grado_ocupacion'                => (double) $evolution->grado_ocupacion,
            'dias_reservados'                => (double) $evolution->dias_reservados,
            'dias_no_reservados'             => (double) $evolution->dias_no_reservados,
            'dias_bloqueados'                => (double) $evolution->dias_bloqueados,
            'tarifa_media_dia'               => (double) $evolution->tarifa_media_dia,
            'estimacion_ingresos_anuales'    => (double) $evolution->estimacion_ingresos_anuales,
            'satisfaccion_clientes'          => (double) $evolution->satisfaccion_clientes,
            'numero_invitados'               => (double) $evolution->numero_invitados,
            'ingresos_totales'               => (double) $evolution->ingresos_totales,
            'ingresos_alquiler'              => (double) $evolution->ingresos_alquiler,
            'ingresos_venta'                 => (double) $evolution->ingresos_venta,
            'otros_ingresos'                 => (double) $evolution->otros_ingresos,
            'gastos_explotacion'             => (double) $evolution->gastos_explotacion,
            'publicidad_prensa'              => (double) $evolution->publicidad_prensa,
            'otro_material'                  => (double) $evolution->otro_material,
            'gastos_ventas'                  => (double) $evolution->gastos_ventas,
            'i_mas_d'                        => (double) $evolution->i_mas_d,
            'arrendamientos'                 => (double) $evolution->arrendamientos,
            'comisiones_crowdestate'         => (double) $evolution->comisiones_crowdestate,
            'gestion'                        => (double) $evolution->gestion,
            'suministros'                    => (double) $evolution->suministros,
            'ibi'                            => (double) $evolution->ibi,
            'otras_tasas'                    => (double) $evolution->otras_tasas,
            'amortizacion'                   => (double) $evolution->amortizacion,
            'subvenciones'                   => (double) $evolution->subvenciones,
            'enajenacion_inmovilizado'       => (double) $evolution->enajenacion_inmovilizado,
            'resultado_explotacion'          => (double) $evolution->resultado_explotacion,
            'resultado_financiero'           => (double) $evolution->resultado_financiero,
            'ingresos_financieros'           => (double) $evolution->ingresos_financieros,
            'gastos_financieros'             => (double) $evolution->gastos_financieros,
            'otros_instrumentos_financieros' => (double) $evolution->otros_instrumentos_financieros,
            'resultado_antes_impuestos'      => (double) $evolution->resultado_antes_impuestos,
            'impuestos'                      => (double) $evolution->impuestos,
            'resultados_ejercicio'           => (double) $evolution->resultados_ejercicio,
            'dividendos_generados'           => (double) $evolution->dividendos_generados,
            'dividendos_abonar'              => (double) $evolution->dividendos_abonar,
            'total_dividendos_abonados'      => (double) $evolution->total_dividendos_abonados,
            'dividendos_cartera'             => (double) $evolution->dividendos_cartera,
            'total_dividendos_devengados'    => (double) $evolution->total_dividendos_devengados,
            'revalorizacion_inmueble'        => (double) $evolution->revalorizacion_inmueble,
            'valor_actual_inmueble'          => (double) $evolution->valor_actual_inmueble,
            'abonado'                        => (bool) $evolution->abonado,
        ];
    }
}
