<?php

namespace App\Transformers;

use App\Transaction;
use Flugg\Responder\Transformer;

class TransactionTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = ['*'];

    /**
     * Transform the model data into a generic array.
     *
     * @param  Transaction $transaction
     * @return array
     */
    public function transform(Transaction $transaction): array
    {
        return [
            'id'      => (int) $transaction->id,
            'type'    => (string) $transaction->typeText,
            'money'   => (string) $this->formatNumber($transaction),
            'money_format'   => (string) $this->formatNumber($transaction, true),
            'type_money'     => (int) $transaction->type,
            'comment'        => (string) $transaction->comment,
            'date'           => (string) $transaction->fechaCreated->toDateTimeString(),
            'fecha'          => (string) $transaction->fechaFormatted,
            'status'         => (string) $transaction->statusText,
            'autoinvest'     => (string) $transaction->autoinvest
        ];
    }

    /**
     * @param $transaction
     */
    private function formatNumber(Transaction $transaction, $format = false)
    {
        $money = '';
        if ($transaction->type == 1 || $transaction->type == 2 || $transaction->type == 15 || $transaction->type == 20) {
            $money .= '-';
        }

        // $money .= formatThousandsNotZero($transaction->money).'€';
        $money .= $transaction->money;

        return $format ? number_format($transaction->money, 2, ',', '.') . '€' : $money;
    }
}
