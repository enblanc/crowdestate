<?php

namespace App\Transformers;

use App\MarketOrder;
use Flugg\Responder\Transformer;

class MarketOrderTransformer extends Transformer
{
    /**
     * A list of all available relations.
     *
     * @var array
     */
    protected $relations = [];

    /**
     * Transform the model data into a generic array.
     *
     * @param  \App\MarketOrder $order
     * @return array
     */
    public function transform(MarketOrder $order): array
    {
        $data = [
            'id'                => (int) $order->id,
            'hashid'            => (string) $order->hashId,
            'type'              => (int) $order->type,
            'typeText'          => (string) $order->typeText,
            'position'          => (int) $order->position,
            'quantity'          => (double) $order->quantity,
            'percentQuantity'   => (double) $order->percentQuantity,
            'percentQuantityOf' => (double) ($order->percentQuantity / 100),
            'price'             => (double) formatThousands($order->price),
            'pricePercentOf'    => (double) ($order->price / 100),
            'priceMoney'        => (double) $order->priceMoney,
            'status'            => (bool) $order->status,
            'link'              => (string) $order->link,
            'linkButton'        => (string) $this->generateButton($order),
            'user'              => [
                'id'     => $order->user->id,
                'nombre' => $order->user->nombreCompleto,
            ],
        ];

        $property = $order->getProperty();

        if ($order->type == 1) {
            $data['inversion'] = [
                'id'             => (int) $order->inversion->id,
                'total'          => (double) $order->inversion->total,
                'transaction_id' => (int) $order->inversion->transaction_id,
            ];

            $data['property'] = [
                'id'        => (int) $property->id,
                'ref'       => (string) $property->ref,
                'slug'      => (string) $property->slug,
                'nombre'    => (string) $property->nombre,
                'ubicacion' => (string) $property->ubicacion,
                'direccion' => (string) $property->direccion,
                //'estado'    => (string) $property->state->nombre,
                'url'       => (string) $property->url,
                'image'     => (string) $property->getFirstMediaUrl($property->mediaCollection, 'thumb'),
                'remaining' => (string) $property->remainingDate,
                'tir'       => $property->tasa_interna_rentabilidad,
            ];
        }

        if ($order->type == 2) {
            $data['property'] = [
                'id'        => (int) $property->id,
                'ref'       => (string) $property->ref,
                'slug'      => (string) $property->slug,
                'nombre'    => (string) $property->nombre,
                'ubicacion' => (string) $property->ubicacion,
                'direccion' => (string) $property->direccion,
                //'estado'    => (string) $property->state->nombre,
                'url'       => (string) $property->url,
                'image'     => (string) $property->getFirstMediaUrl($property->mediaCollection, 'thumb'),
                'remaining' => (string) $property->remainingDate,
                'tir'       => $property->tasa_interna_rentabilidad,
            ];
        }

        return $data;
    }

    /**
     * @param $order
     */
    private function generateButton($order)
    {
        if ($order->type == 1) {
            return '<a href="'.$order->link.'" class="button button--small bg-success button--full nowrap">'.__('Compra ahora').'</a>';
        }

        return '<a href="'.$order->link.'" class="button button--small button--full nowrap">'.__('Vende ahora').'</a>';
    }
}
