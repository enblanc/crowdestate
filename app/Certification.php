<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use App\Helpers\CertificationHelper;
use App\User;

class Certification extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'direccion_1',
        'direccion_2',
        'direccion_3',
        'year',
        'nif',
        'name',
        'total',
        'retention',
        'paid',
        'business_name',
        'cif',
        'public_name',
        'presented_with_date',
        'receipt_number',
        'date',
        'signature',
        'link',
    ];

    /**
     * Devuelve el usuario
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function filename () : string
    {
        return "certificado-de-retenciones-{$this->id}-{$this->user->email}";
    }
    
    /**
     * Undocumented function
     *
     * @return null|bool
     */
    public function getStoragePathAttribute ()
    {
        return storage_path("app/public/certificaciones/{$this->user->id}/{$this->filename()}.pdf");
    }

    public function getExistsAttribute () : bool
    {
        return Storage::disk('certifications')->exists("{$this->user->id}/{$this->filename()}.pdf");
    }

    /**
     * Obtener el enlace del certificado
     *
     * @return string|null
     */
    public function getLinkAttribute ()
    {
        if (!$this->getExistsAttribute() && !$this->generatePDF())
            return null;

        return Storage::disk('certifications')->url("{$this->user->id}/{$this->filename()}.pdf");    
    }

    public function getLabelAttribute () : string
    {
        return __('Certificado') . ' - ' . $this->year;
    }

    /**
     * Generar el documento PDF
     *
     * @return bool
     */
    public function generatePDF () : bool
    {
        try {
            $certificationHelper = new CertificationHelper($this);

            $certificationHelper->generate();

            return true;

        } catch (\Throwable $th) {
            
            Log::critical($th->getMessage());

            return false;
        }
    }
}
