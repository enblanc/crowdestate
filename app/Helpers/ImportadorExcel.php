<?php

namespace App\Helpers;

use Maatwebsite\Excel\Facades\Excel;

class ImportadorExcel
{

    /**
     * Lee el excel de resumen y devuelve los datos.
     * La hoja se tiene que llamar 'resumen' !importante
     *
     * @param  string $path - ruta del archivo a leer
     *
     * @return object
     */
    public static function readResumenExcel($path)
    {
        $sheet = Excel::selectSheets('resumen')->load($path)->byConfig('importacion.sheets')->first();

        $resumenData = (object) [
            'periodo_financiacion'        => (int) $sheet->periodo,
            'horizonte_temporal'          => (int) $sheet->horizonte,
            'valor_mercado'               => self::convertToDouble($sheet->valor_mercado),
            'valor_compra'                => self::convertToDouble($sheet->valor_compra),
            'coste_total'                 => self::convertToDouble($sheet->coste_total),
            'hipoteca'                    => self::convertToDouble($sheet->hipoteca),
            'objetivo'                    => self::convertToDouble($sheet->objetivo),
            'importe_minimo'              => (int) $sheet->importe_minimo,
            'rentabilidad_alquiler_bruta' => self::convertToDouble($sheet->rentabilidad_alquiler_bruta),
            'rentabilidad_alquiler_neta'  => self::convertToDouble($sheet->rentabilidad_alquiler_neta),
            'rentabilidad_objetivo_bruta' => self::convertToDouble($sheet->rentabilidad_objetivo_bruta),
            'rentabilidad_objetivo_neta'  => self::convertToDouble($sheet->rentabilidad_objetivo_neta),
            'rentabilidad_anual'          => self::convertToDouble($sheet->rentabilidad_anual),
            'rentabilidad_total'          => self::convertToDouble($sheet->rentabilidad_total),
            'tasa_interna_rentabilidad'   => self::convertToDouble($sheet->tasa_interna_rentabilidad),
            'coste_obras'                 => self::convertToDouble($sheet->coste_obras),
            'impuestos_tramites'          => self::convertToDouble($sheet->impuestos_tramites),
            'registro_sociedad'           => self::convertToDouble($sheet->registro_sociedad),
            'otros_costes'                => self::convertToDouble($sheet->otros_costes),
            'imprevistos'                 => self::convertToDouble($sheet->imprevistos),
            'caja_minima'                 => self::convertToDouble($sheet->caja_minima),
            'honorarios'                  => self::convertToDouble($sheet->honorarios),
            'ingresos_anuales_alquiler'   => self::convertToDouble($sheet->ingresos_anuales_alquiler),
            'importe_venta'               => self::convertToDouble($sheet->importe_venta),
            'compra_venta_margen_bruto'   => self::convertToDouble($sheet->compra_venta_margen_bruto),
        ];

        return $resumenData;
    }

    /**
     * Lee el excel de evoluciones y devuelve los datos.
     * La hoja se tiene que llamar 'evolucion' !importante
     *
     * @param string $path - ruta del archivo a leer
     *
     * @return object
     */
    public static function readEstimacionExcel($path)
    {
        $sheet = Excel::selectSheets('estimacion')->load($path)->byConfig('importacion.sheets')->first();

        $estimacionData = (object) [
            'numero_reservas'                => self::convertToDouble($sheet->numero_reservas),
            'grado_ocupacion'                => self::convertToDouble($sheet->grado_ocupacion),
            'dias_reservados'                => self::convertToDouble($sheet->dias_reservados),
            'dias_no_reservados'             => self::convertToDouble($sheet->dias_no_reservados),
            'dias_bloqueados'                => self::convertToDouble($sheet->dias_bloqueados),
            'tarifa_media_dia'               => self::convertToDouble($sheet->tarifa_media_dia),
            'estimacion_ingresos_anuales'    => self::convertToDouble($sheet->estimacion_ingresos_anuales),
            'satisfaccion_clientes'          => self::convertToDouble($sheet->satisfaccion_clientes),
            'numero_invitados'               => self::convertToDouble($sheet->numero_invitados),
            'ingresos_totales'               => self::convertToDouble($sheet->ingresos_totales),
            'ingresos_alquiler'              => self::convertToDouble($sheet->ingresos_alquiler),
            'ingresos_venta'                 => self::convertToDouble($sheet->ingresos_venta),
            'otros_ingresos'                 => self::convertToDouble($sheet->otros_ingresos),
            'gastos_explotacion'             => self::convertToDouble($sheet->gastos_explotacion),
            'publicidad_prensa'              => self::convertToDouble($sheet->publicidad_prensa),
            'otro_material'                  => self::convertToDouble($sheet->otro_material),
            'gastos_ventas'                  => self::convertToDouble($sheet->gastos_ventas),
            'i_mas_d'                        => self::convertToDouble($sheet->i_mas_d),
            'arrendamientos'                 => self::convertToDouble($sheet->arrendamientos),
            'comisiones_crowdestate'         => self::convertToDouble($sheet->comisiones_crowdestate),
            'gestion'                        => self::convertToDouble($sheet->gestion),
            'suministros'                    => self::convertToDouble($sheet->suministros),
            'ibi'                            => self::convertToDouble($sheet->ibi),
            'otras_tasas'                    => self::convertToDouble($sheet->otras_tasas),
            'amortizacion'                   => self::convertToDouble($sheet->amortizacion),
            'subvenciones'                   => self::convertToDouble($sheet->subvenciones),
            'enajenacion_inmovilizado'       => self::convertToDouble($sheet->enajenacion_inmovilizado),
            'resultado_explotacion'          => self::convertToDouble($sheet->resultado_explotacion),
            'resultado_financiero'           => self::convertToDouble($sheet->resultado_financiero),
            'ingresos_financieros'           => self::convertToDouble($sheet->ingresos_financieros),
            'gastos_financieros'             => self::convertToDouble($sheet->gastos_financieros),
            'otros_instrumentos_financieros' => self::convertToDouble($sheet->otros_instrumentos_financieros),
            'resultado_antes_impuestos'      => self::convertToDouble($sheet->resultado_antes_impuestos),
            'impuestos'                      => self::convertToDouble($sheet->impuestos),
            'resultados_ejercicio'           => self::convertToDouble($sheet->resultados_ejercicio),
            'dividendos_generados'           => self::convertToDouble($sheet->dividendos_generados),
            'dividendos_abonar'              => self::convertToDouble($sheet->dividendos_abonar),
            'total_dividendos_abonados'      => self::convertToDouble($sheet->total_dividendos_abonados),
            'dividendos_cartera'             => self::convertToDouble($sheet->dividendos_cartera),
            'total_dividendos_devengados'    => self::convertToDouble($sheet->total_dividendos_devengados),
            'revalorizacion_inmueble'        => self::convertToDouble($sheet->revalorizacion_inmueble),
            'valor_actual_inmueble'          => self::convertToDouble($sheet->valor_actual_inmueble),
        ];

        return $estimacionData;
    }

    /**
     * Lee el excel de evoluciones y devuelve los datos.
     * La hoja se tiene que llamar 'evolucion' !importante
     *
     * @param string $path - ruta del archivo a leer
     * @param integer $$mes - MEs en formato integer del 0 al 11
     *
     * @return object
     */
    public static function readEvolucionExcel($path, $mes)
    {
        $months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
        $mesKey = $months[$mes];

        $sheet = Excel::selectSheets('evolucion')->load($path)->byConfig('importacion.sheets')->first();

        $evolucionData = (object) [
            'numero_reservas'                => self::convertToDouble($sheet->{$mesKey.'_numero_reservas'}),
            'grado_ocupacion'                => self::convertToDouble($sheet->{$mesKey.'_grado_ocupacion'}),
            'dias_reservados'                => self::convertToDouble($sheet->{$mesKey.'_dias_reservados'}),
            'dias_no_reservados'             => self::convertToDouble($sheet->{$mesKey.'_dias_no_reservados'}),
            'dias_bloqueados'                => self::convertToDouble($sheet->{$mesKey.'_dias_bloqueados'}),
            'tarifa_media_dia'               => self::convertToDouble($sheet->{$mesKey.'_tarifa_media_dia'}),
            'estimacion_ingresos_anuales'    => self::convertToDouble($sheet->{$mesKey.'_estimacion_ingresos_anuales'}),
            'satisfaccion_clientes'          => self::convertToDouble($sheet->{$mesKey.'_satisfaccion_clientes'}),
            'numero_invitados'               => self::convertToDouble($sheet->{$mesKey.'_numero_invitados'}),
            'ingresos_totales'               => self::convertToDouble($sheet->{$mesKey.'_ingresos_totales'}),
            'ingresos_alquiler'              => self::convertToDouble($sheet->{$mesKey.'_ingresos_alquiler'}),
            'ingresos_venta'                 => self::convertToDouble($sheet->{$mesKey.'_ingresos_venta'}),
            'otros_ingresos'                 => self::convertToDouble($sheet->{$mesKey.'_otros_ingresos'}),
            'gastos_explotacion'             => self::convertToDouble($sheet->{$mesKey.'_gastos_explotacion'}),
            'publicidad_prensa'              => self::convertToDouble($sheet->{$mesKey.'_publicidad_prensa'}),
            'otro_material'                  => self::convertToDouble($sheet->{$mesKey.'_otro_material'}),
            'gastos_ventas'                  => self::convertToDouble($sheet->{$mesKey.'_gastos_ventas'}),
            'i_mas_d'                        => self::convertToDouble($sheet->{$mesKey.'_i_mas_d'}),
            'arrendamientos'                 => self::convertToDouble($sheet->{$mesKey.'_arrendamientos'}),
            'comisiones_crowdestate'         => self::convertToDouble($sheet->{$mesKey.'_comisiones_crowdestate'}),
            'gestion'                        => self::convertToDouble($sheet->{$mesKey.'_gestion'}),
            'suministros'                    => self::convertToDouble($sheet->{$mesKey.'_suministros'}),
            'ibi'                            => self::convertToDouble($sheet->{$mesKey.'_ibi'}),
            'otras_tasas'                    => self::convertToDouble($sheet->{$mesKey.'_otras_tasas'}),
            'amortizacion'                   => self::convertToDouble($sheet->{$mesKey.'_amortizacion'}),
            'subvenciones'                   => self::convertToDouble($sheet->{$mesKey.'_subvenciones'}),
            'enajenacion_inmovilizado'       => self::convertToDouble($sheet->{$mesKey.'_enajenacion_inmovilizado'}),
            'resultado_explotacion'          => self::convertToDouble($sheet->{$mesKey.'_resultado_explotacion'}),
            'resultado_financiero'           => self::convertToDouble($sheet->{$mesKey.'_resultado_financiero'}),
            'ingresos_financieros'           => self::convertToDouble($sheet->{$mesKey.'_ingresos_financieros'}),
            'gastos_financieros'             => self::convertToDouble($sheet->{$mesKey.'_gastos_financieros'}),
            'otros_instrumentos_financieros' => self::convertToDouble($sheet->{$mesKey.'_otros_instrumentos_financieros'}),
            'resultado_antes_impuestos'      => self::convertToDouble($sheet->{$mesKey.'_resultado_antes_impuestos'}),
            'impuestos'                      => self::convertToDouble($sheet->{$mesKey.'_impuestos'}),
            'resultados_ejercicio'           => self::convertToDouble($sheet->{$mesKey.'_resultados_ejercicio'}),
            'dividendos_generados'           => self::convertToDouble($sheet->{$mesKey.'_dividendos_generados'}),
            'dividendos_abonar'              => self::convertToDouble($sheet->{$mesKey.'_dividendos_abonar'}),
            'total_dividendos_abonados'      => self::convertToDouble($sheet->{$mesKey.'_total_dividendos_abonados'}),
            'dividendos_cartera'             => self::convertToDouble($sheet->{$mesKey.'_dividendos_cartera'}),
            'total_dividendos_devengados'    => self::convertToDouble($sheet->{$mesKey.'_total_dividendos_devengados'}),
            'revalorizacion_inmueble'        => self::convertToDouble($sheet->{$mesKey.'_revalorizacion_inmueble'}),
            'valor_actual_inmueble'          => self::convertToDouble($sheet->{$mesKey.'_valor_actual_inmueble'}),
        ];

        return $evolucionData;
    }

    /**
     * Genera los arrays necesarios para capturar las celdas del excel de evolución
     *
     * @param  string $month - MEs en minúsculas y en español
     *
     * @return array
     */
    public static function createDynamicConfig($month)
    {
        $headers = ['B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M'];
        $months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
        $rows = [
            'numero_reservas'                => 2,
            'grado_ocupacion'                => 3,
            'dias_reservados'                => 4,
            'dias_no_reservados'             => 5,
            'dias_bloqueados'                => 6,
            'tarifa_media_dia'               => 7,
            'estimacion_ingresos_anuales'    => 8,
            'satisfaccion_clientes'          => 9,
            'numero_invitados'               => 10,
            'ingresos_totales'               => 13,
            'ingresos_alquiler'              => 14,
            'ingresos_venta'                 => 15,
            'otros_ingresos'                 => 16,
            'gastos_explotacion'             => 17,
            'publicidad_prensa'              => 18,
            'otro_material'                  => 19,
            'gastos_ventas'                  => 20,
            'i_mas_d'                        => 21,
            'arrendamientos'                 => 22,
            'comisiones_crowdestate'         => 23,
            'gestion'                        => 24,
            'suministros'                    => 25,
            'ibi'                            => 26,
            'otras_tasas'                    => 27,
            'amortizacion'                   => 28,
            'subvenciones'                   => 29,
            'enajenacion_inmovilizado'       => 30,
            'resultado_explotacion'          => 31,
            'resultado_financiero'           => 32,
            'ingresos_financieros'           => 33,
            'gastos_financieros'             => 34,
            'otros_instrumentos_financieros' => 35,
            'resultado_antes_impuestos'      => 36,
            'impuestos'                      => 37,
            'resultados_ejercicio'           => 38,
            'dividendos_generados'           => 41,
            'dividendos_abonar'              => 42,
            'total_dividendos_abonados'      => 43,
            'dividendos_cartera'             => 44,
            'total_dividendos_devengados'    => 45,
            'revalorizacion_inmueble'        => 48,
            'valor_actual_inmueble'          => 49,
        ];

        $configData = collect([]);

        //Letra del mes actual;
        $correctLetter = $headers[$month - 1];
        $counter = 0;

        foreach ($months as $month) {
            $xValue = $headers[$counter];
            foreach ($rows as $key => $value) {
                $configData->put($month.'_'.$key, (string) $xValue.$value);
            }
            $counter++;
        }

        dump($configData);
        dd();
    }

    /**
     * @param $string
     */
    public static function convertToDouble($string)
    {
        // Limpiamos la variable
        $string = trim(str_replace(',', '.', $string));

        return round($string, 2);
    }
}
