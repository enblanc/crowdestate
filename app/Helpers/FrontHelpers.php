<?php

use App\Blog\Models\Categories;
use App\Blog\Models\Post;
use App\Property;

/**
 * Return value formatted in thousands
 *
 * @param $value
 */
function formatThousands($value)
{
    return number_format($value, 2, ',', '.');
}

/**
 * @param $value
 */
function formatThousandsNotZero($value)
{
    $val = floatval($value) + 0;

    $decimals = is_numeric($val) && floor($val) != $val;

    if ($decimals) {
        return number_format($val, 2, ',', '.');
    } else {
        return number_format($val, 0, ',', '.');
    }
}

/**
 * @param $value
 */
function formatThousandsNotDecimals($value)
{
    $val = $value + 0;
    $decimals = is_numeric($val) && floor($val) != $val;

    return number_format($val, 0, ',', '.');
}

/**
 * @param $value
 */
function nodecimals($value)
{
    $val = $value + 0;
    $decimals = is_numeric($val) && floor($val) != $val;

    return $val;
}

/**
 * @param $datos_tmp
 * @return mixed
 */
function cleanArray($datos_tmp)
{
    $datos = [];
    foreach ($datos_tmp as $dato_tmp) {
        if ($dato_tmp[1] != 0) {
            $datos[] = $dato_tmp;
        }
    }

    return $datos;
}

function pago_sin_confirmar()
{
    return config('brickstarter.pago_sin_confirmar');
}

/**
 * @param $years
 */
function years_before($years = 0)
{
    return Carbon\Carbon::today()->subYears($years)->format('d/m/Y');
}

if (!function_exists('checkValue')) {

    /**
     * Check if current route is visited
     *
     * @param $route
     * @param $defaultClass
     * @param $appendClass
     */
    function checkValue($oldName, $model, $modelValue, $return = null)
    {
        $oldValue = old($oldName);
        $returnValue = ($return) ? $return : $oldValue;

        if ($oldValue) {
            return $returnValue;
        }

        if (isset($model) && $model != null) {
            $returnValue = ($return) ? $return : $model->{$modelValue};
            if ($model->{$modelValue}) {
                return $returnValue;
            } else {
                return;
            }
        }

        return $returnValue;
    }
}

if (!function_exists('checkValueTranslated')) {

    /**
     * Check if current route is visited
     *
     * @param $route
     * @param $defaultClass
     * @param $appendClass
     */
    function checkValueTranslated($oldName, $model, $locale, $modelValue, $return = null)
    {
        $oldValue = old($oldName);
        $returnValue = ($return) ? $return : $oldValue;

        if ($oldValue) {
            return $returnValue;
        }

        if (isset($model[$locale]) && $model[$locale] != null) {
            $returnValue = ($return) ? $return : $model[$locale]->{$modelValue};
            if ($model[$locale]->{$modelValue}) {
                return $returnValue;
            } else {
                return;
            }
        }

        return $returnValue;
    }
}

if (!function_exists('current_locale')) {

    /**
     * Returns current language
     *
     * @return string
     */
    function current_locale()
    {
        return localization()->getCurrentLocale();
    }
}

if (!function_exists('current_locale_date_format_js')) {

    /**
     * Returns current language
     *
     * @return string
     */
    function current_locale_date_format_js()
    {
        if (current_locale() == 'en') {
            return 'MM/dd/yyyy';
        }

        return 'dd/MM/yyyy';
    }
}

if (!function_exists('get_url')) {
    /**
     * Get a translated url from a route name and gicen attributes and locale
     *
     * @param string $key - Route Name
     * @param array  $attributes - Route Attributes
     * @param string $locale - Locale (CODE 2)
     *
     * @return string|url
     */
    function get_url($routeName, $attributes = array(), $locale = null)
    {
        if ($locale === null) {
            $locale = localization()->getCurrentLocale();
        }
        $routeName = str_replace('.', '-', $routeName);

        $transKey = config("brickstarter.rutas.$routeName");

        if (str_contains($routeName, 'blog-show') && isset($attributes['slug'])) {
            $post = Post::whereTranslationLike('slug', '%'.$attributes['slug'].'%')->first();

            if ($post) {
                if ($post->translate($locale)) {
                    $attributes['slug'] = $post->translate($locale)->slug;
                } else {
                    $attributes['slug'] = $post->translate('es')->slug;
                }
            }
        }

        if (str_contains($routeName, 'blog-categories') && isset($attributes['slug'])) {
            $category = Categories::whereTranslationLike('slug', '%'.$attributes['slug'].'%')->first();

            if ($category) {
                if ($category->translate($locale)->slug) {
                    $attributes['slug'] = $category->translate($locale)->slug;
                } else {
                    $attributes['slug'] = $category->translate('es')->slug;
                }
            }
        }

        if (str_contains($routeName, 'property-show') && isset($attributes['property'])) {
            if (current_locale() === 'es') {
                $property = Property::whereSlug($attributes['property'])->first();
                if ($property) {
                    $details = $property->details()->whereLocale($locale)->first();
                    if ($details) {
                        if ($details->slug) {
                            $attributes['property'] = $details->slug;
                        }
                    }
                }
            } else {
                $property = Property::checkSlugAndReturn($attributes['property']);
                if ($property) {
                    if ($property->slug) {
                        $attributes['property'] = $property->slug;
                    }
                }
            }
        }

        $finalUrl = localization()->getUrlFromRouteName($locale, $transKey, $attributes);

        if (auth()->user() && current_locale() != $locale) {
            $finalUrl .= '?change_locale='.$locale;
        }

        return $finalUrl;
    }
}

/**
 * @return mixed
 */
function strings_to_js()
{
    $parsley = [
        'defaultMessage' => __('Este campo es erróneo.'),
        'type'           => [
            'email'    => __('Debes introducir un correo válido.'),
            'url'      => __('Debes introducir una URL válida.'),
            'number'   => __('Debes introducir un número válido.'),
            'integer'  => __('Debes introducir un número válido.'),
            'digits'   => __('Debes introducir un dígito válido.'),
            'alphanum' => __('Debes introducir un valor alfanumérico.'),
        ],
        'notblank'       => __('Este campo no debe estar en blanco.'),
        'required'       => __('Este campo es obligatorio.'),
        'pattern'        => __('La contraseña debe tener mínimo 8 caracteres con números y letras.'),
        'min'            => __('Este campo no debe ser menor que %s.'),
        'max'            => __('Este campo no debe ser mayor que %s.'),
        'range'          => __('Este campo debe estar entre %s y %s.'),
        'minlength'      => __('Este campo es muy corto. La longitud mínima es de %s caracteres.'),
        'maxlength'      => __('Este campo es muy largo. La longitud máxima es de %s caracteres.'),
        'length'         => __('La longitud de este campo debe estar entre %s y %s caracteres.'),
        'mincheck'       => __('Debe seleccionar al menos %s opciones.'),
        'maxcheck'       => __('Debe seleccionar %s opciones o menos.'),
        'check'          => __('Debe seleccionar entre %s y %s opciones.'),
        'equalto'        => __('Este campo debe ser idéntico.'),
    ];

    $months = [
        'enero'      => __('Enero'),
        'febrero'    => __('Febrero'),
        'marzo'      => __('Marzo'),
        'abril'      => __('Abril'),
        'mayo'       => __('Mayo'),
        'junio'      => __('Junio'),
        'julio'      => __('Julio'),
        'agosto'     => __('Agosto'),
        'septiembre' => __('Septiembre'),
        'octubre'    => __('Octubre'),
        'noviembre'  => __('Noviembre'),
        'diciembre'  => __('Diciembre'),
    ];

    $errors = [
        'error_general'    => __('Algo ha fallado. Por favor, prueba de nuevo.'),
        'pasword_coincide' => __('La contraseña especificada no coincide con la actual.'),
    ];

    $locale = localization()->getCurrentLocale();

    $language = Cache::remember('translations_'.$locale, 30 * 24 * 60, function () use ($locale) {
        $langFile = resource_path("lang/$locale.json");

        $data = collect(getFileContent($langFile));

        return $data->mapWithKeys(function ($item, $key) {
            return [str_slug($key) => $item];
        });
    });

    $general = ['enviando' => __('Enviando')];

    return json_encode(['parsley' => $parsley, 'months' => $months, 'errors' => $errors, 'general' => $general]);
}

if (!function_exists('getFileContent')) {
    /**
     * @param string $key
     * @param int $count
     *
     * @return string
     */
    function getFileContent(string $filePath)
    {
        $content = (string) file_get_contents($filePath) ?? '';

        return json_decode(str_replace("\n", ' ', $content));
    }
}

if (!function_exists('__choice')) {
    /**
     * Trans choice
     *
     * @param   string  $key
     * @param   int     $count
     *
     * @return  string
     */
    function __choice(string $key, int $count): string
    {
        return trans_choice(__($key), $count);
    }
}
