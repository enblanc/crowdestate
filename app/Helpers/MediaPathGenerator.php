<?php

namespace App\Helpers;

use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\PathGenerator\PathGenerator;

class MediaPathGenerator implements PathGenerator
{
    /*
     * Get the path for the given media, relative to the root storage path.
     */
    /**
     * @param Media $media
     * @return mixed
     */
    public function getPath(Media $media): string
    {
        $model = $media->model()->withTrashed()->first();

        if (!$model) {
            $model = app($media->model_type);
            $model->id = $media->model_id;
        }

        return $media->collection_name.'/'.$model->mediaId.'/'.$media->id.'/';
    }
    /*
     * Get the path for conversions of the given media, relative to the root storage path.
     * @return string
     */
    /**
     * @param Media $media
     * @return mixed
     */
    public function getPathForConversions(Media $media): string
    {
        $model = $media->model()->withTrashed()->first();

        if (!$model) {
            $model = app($media->model_type);
            $model->id = $media->model_id;
        }

        return $media->collection_name.'/'.$model->mediaId.'/'.$media->id.'/c/';
    }
}
