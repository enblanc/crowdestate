<?php
namespace App\Helpers;

use Carbon\Carbon;
use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_Cell_DefaultValueBinder;
use PHPExcel_Cell_IValueBinder;
use PHPExcel_Shared_Date;

class ExcelEvolucionBinder extends PHPExcel_Cell_DefaultValueBinder implements PHPExcel_Cell_IValueBinder
{
    protected $columnDates = [
        'B1', 'C1', 'D1', 'E1', 'F1', 'G1',
        'B12', 'C12', 'D12', 'E12', 'F12', 'G12',
        'B40', 'C40', 'D40', 'E40', 'F40', 'G40',
        'B47', 'C47', 'D47', 'E47', 'F47', 'G47',
    ];

    public function bindValue(PHPExcel_Cell $cell, $value = null)
    {


        if (is_numeric($value)) {
            if (in_array($cell->getCoordinate(), $this->columnDates)) {
                $carbonDate = new Carbon("1899-12-30 + $value days");
                // $carbonDate = Carbon::parse($dateTime);
                $cell->setValue($carbonDate->format('Y-m-d'));
            } else {
                $cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_NUMERIC);
            }

            return true;
        }

        // else return default behavior
        return parent::bindValue($cell, $value);
    }
}
