<?php

namespace App\Helpers;

class TokensHelper
{
    /**
     * Return a uniqueToken checking with the model given
     *
     * @param  class  $model
     * @param  string  $column
     * @param  integer $length
     * @param  bool $upper
     * @return string
     */
    public static function generateUniqueToken($model, $column = 'id', $length = 8, $upper = true, $append = '')
    {
        $exit = false;
        while ($exit == false) {
            $token = $append.str_random($length);
            $exists = $model::where($column, $token)->first();
            if (!$exists) {
                $exit = true;
            }
        }

        return ($upper) ? mb_strtoupper($token) : $token;
    }
}
