<?php

namespace App\Helpers;

use Artesaos\SEOTools\Facades\SEOTools as SEO;

class SeoHelpers
{
    /**
     * @var string
     */
    public static $routeFile = 'routes';

    /**
     * @param $title
     * @param $description
     * @param $image
     * @param null $appendDefault
     * @param array $extras
     */
    public static function setSEO($title, $description, $image = null, $appendDefault = true, $extras = [])
    {
        $description = str_replace('"', "'", str_limit($description, 152));

        SEO::metatags()->setTitle($title, $appendDefault);
        SEO::opengraph()->setTitle($title);
        SEO::twitter()->setTitle($title);
        SEO::opengraph()->setUrl(url()->current());
        SEO::twitter()->setUrl(url()->current());
        SEO::setDescription($description);
        SEO::opengraph()->setDescription($description);
        SEO::twitter()->setDescription($description);

        //Locales
        // SEO::opengraph()->addProperty('locale', localization()->getCurrentLocaleRegional());
        // $otherLocales = localization()->getSupportedLocales()->values()->all();
        // $hasOtherLocales = false;
        // foreach ($otherLocales as $locale) {
        //     if ($locale->key() !== localization()->getCurrentLocale()) {
        //         $hasOtherLocales[] = $locale->regional();
        //     }
        // }
        // if ($hasOtherLocales !== false && is_Array($hasOtherLocales)) {
        //     SEO::opengraph()->addProperty('locale:alternate', $hasOtherLocales);
        // }

        //Extras
        if (count($extras) > 0) {
            foreach ($extras as $key => $extra) {
                SEO::opengraph()->addProperty($key, $extra);
                SEO::metatags()->addMeta($key, $extra, 'property');
            }
        }

        if ($image != null) {
            if (is_array($image) || is_object($image)) {
                SEO::opengraph()->addImages($image);
                SEO::twitter()->addImage($image);
            } else {
                SEO::opengraph()->addImage(asset($image));
                SEO::twitter()->addImage(asset($image));
            }
        } else {
            SEO::opengraph()->addImage(asset('assets/images/logo.svg'));
            SEO::twitter()->addImage(asset('assets/images/logo.svg'));
        }
    }

    /**
     * Get simple url.
     *
     * @param string $key
     * @param array  $attributes
     * @param string $locale
     *
     * @return string | url
     */
    public static function getSimpleUrl($key, $attributes = array(), $locale = null)
    {
        if ($locale === null) {
            $locale = localization()->getCurrentLocale();
        }

        return localization()->getUrlFromRouteName($locale, self::$routeFile.'.'.$key, $attributes);
    }

    /**
     * @param $family
     */
    public static function getFamilyUrl($family)
    {
        return url(str_replace('{slug}', $family, self::getSimpleUrl('products')));
    }

}
