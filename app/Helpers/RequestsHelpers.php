<?php

namespace App\Helpers;

class RequestsHelpers
{

    /**
     * Función para añadir resumen por defecto a la nueva propiedad
     *
     * @param array $data
     *
     * @return array
     */
    public static function addDefaultResumen(array $data)
    {
        $resumen = [
            'periodo_financiacion' => null,
            'horizonte_temporal' => null,
            'valor_mercado' => null,
            'valor_compra' => null,
            'coste_total' => null,
            'hipoteca' => null,
            'objetivo' => null,
            'importe_minimo' => null,
            'rentabilidad_alquiler_bruta' => null,
            'rentabilidad_alquiler_neta' => null,
            'rentabilidad_objetivo_bruta' => null,
            'rentabilidad_objetivo_neta' => null,
            'rentabilidad_anual' => null,
            'rentabilidad_total' => null,
            'valor_actual_neto' => null,
            'valor_operacion' => null,
            'tasa_interna_rentabilidad' => null,
            'coste_obras' => null,
            'impuestros_tramites' => null,
            'registro_sociedad' => null,
            'otros_costes' => null,
            'imprevistos' => null,
            'caja_minima' => null,
            'honorarios' => null,
            'ingresos_anuales_alquiler' => null,
            'importe_venta' => null,
            'compra_venta_margen_bruto' => null,
            'resumen' => false,
        ];

        $final = array_merge($data, $resumen);

        return $final;
    }
}
