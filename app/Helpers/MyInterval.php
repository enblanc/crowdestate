<?php

namespace App\Helpers;

use DateInterval;

class MyInterval extends DateInterval
{
    /**
     * @var mixed
     */
    private $dateInterval;

    /**
     * @param DateInterval $dateInterval
     */
    public function __construct($dateInterval)
    {
        $this->dateInterval = $dateInterval;
        $this->instance();
    }

    /*
     *  Create instance
     */
    public function instance()
    {
        $date = array_filter(array(
            'Y' => $this->dateInterval->y,
            'M' => $this->dateInterval->m,
            'D' => $this->dateInterval->d,
        ));

        $time = array_filter(array(
            'H' => $this->dateInterval->h,
            'M' => $this->dateInterval->i,
            'S' => $this->dateInterval->s,
        ));

        $specString = 'P';

        foreach ($date as $key => $value) {
            $specString .= $value.$key;
        }

        if (count($time) > 0) {
            $specString .= 'T';
            foreach ($time as $key => $value) {
                $specString .= $value.$key;
            }
        }

        // return new self($specString === 'P' ? 'PT0S' : $specString);
    }

    /**
     * Format an interval to show all existing components. Human Readable Time
     * If the interval doesn't have a time component (years, months, etc)
     * That component won't be displayed.
     *
     * @return string Formatted interval string.
     */
    public function forHumans($hoursAndMinutes = false)
    {
        $locale = localization()->getCurrentLocale();

        $result = '';
        if ($this->dateInterval->y) {
            $years = $this->dateInterval->y;
            $result .= $this->dateInterval->format('%y ').__choice('auth.years', $years).', ';
        }
        if ($this->dateInterval->m) {
            $months = $this->dateInterval->m;
            $result .= $this->dateInterval->format('%m ').trans_choice('auth.months', $months).' ';
        }
        if ($this->dateInterval->d) {
            $days = $this->dateInterval->d;
            $result .= __('auth.and').$this->dateInterval->format(' %d ').trans_choice('auth.days', $days);
        }

        if ($hoursAndMinutes) {
            if ($this->dateInterval->h) {
                $hours = $this->dateInterval->h;
                $result .= $this->dateInterval->format(' %h ').trans_choice('auth.hours', $hours).' ';
            }
            if ($this->dateInterval->i) {
                $minutes = $this->dateInterval->i;
                $result .= $this->dateInterval->format('%i ').trans_choice('auth.minutes', $minutes).' ';
            }
            if ($this->dateInterval->s) {
                $seconds = $this->dateInterval->s;
                $result .= $this->dateInterval->format('%s ').trans_choice('auth.seconds', $seconds);
            }
        }

        return $result;
    }
}
