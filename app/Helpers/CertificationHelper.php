<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\TemplateProcessor;
use PhpOffice\PhpWord\IOFactory;
use App\Certification;

class CertificationHelper
{
    /**
     * Certification
     *
     * @var Certification|null
     */
    protected $certification;

    /**
     * Path del certificado
     *
     * @var string|null
     */
    protected $path = null;

    public function __construct(Certification $certification = null)
    {
        $this->setCertification($certification);
    }

    /**
     * Obtener el path del documento
     *
     * @return string|null
     */
    public function getPath ()
    {
        return $this->path;
    }

    /**
     * Set el path del documento
     *
     * @return void
     */
    private function setPath (string $path)
    {
        $this->path = $path;
    }

    /**
     * Inicializar el modelo
     *
     * @param Certification $certification
     * @return void
     */
    public function setCertification (Certification $certification)
    {
        $this->certification = $certification;
    }

    /**
     * Generar documento PDF del certificado
     *
     */
    public function generate ()
    {
        $certification = $this->certification;
        
        if (is_null($certification))
            return $this;

        /* Set the PDF Engine Renderer Path */
        $domPdfPath = base_path('vendor/dompdf/dompdf');
        Settings::setPdfRendererPath($domPdfPath);
        Settings::setPdfRendererName('DomPDF');

        /*@ Reading doc file */
        $template = new TemplateProcessor(storage_path('document-certificado.docx'));

        /*@ Replacing variables in doc file */
        $template->setValue('user_id', $certification->user_id);
        $template->setValue('direccion_1', $certification->direccion_1);
        $template->setValue('direccion_2', $certification->direccion_2);
        $template->setValue('direccion_3', $certification->direccion_3);
        $template->setValue('year', $certification->year);
        $template->setValue('nif', $certification->nif);
        $template->setValue('name', $certification->name);
        $template->setValue('total', $certification->total);
        $template->setValue('retention', $certification->retention);
        $template->setValue('paid', $certification->paid);
        $template->setValue('business_name', $certification->business_name);
        $template->setValue('cif', $certification->cif);
        $template->setValue('public_name', $certification->public_name);
        $template->setValue('receipt_number', $certification->receipt_number);
        $template->setValue('presented_with_date', $certification->presented_with_date);
        $template->setValue('date', $certification->date);
        $template->setValue('signature', $certification->signature);

        /*@ Save Temporary Word File With New Name */
        $saveDocPath = public_path($certification->filename() . '.docx');
        $template->saveAs($saveDocPath);

        // Load temporarily create word file
        $content = IOFactory::load($saveDocPath);

        //Save it into PDF
        $savePdfPath = $certification->storage_path;

        if (!\File::isDirectory(storage_path("app/public/certificaciones/{$certification->user->id}")))
            \File::makeDirectory(storage_path("app/public/certificaciones/{$certification->user->id}"), 0777, true, true);

        /*@ If already PDF exists then delete it */
        if (file_exists($savePdfPath)) {
            unlink($savePdfPath);
        }

        //Save it into PDF
        $PDFWriter = IOFactory::createWriter($content, 'PDF');
        $PDFWriter->save($savePdfPath);

        /*@ Remove temporarily created word file */
        if (file_exists($saveDocPath)) {
            unlink($saveDocPath);
        }

        $this->setPath($savePdfPath);

        return $this;
    }
}
