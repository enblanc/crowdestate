<?php

namespace App\Helpers;

use App\KycDocumentStatus;

class KYCLog
{

    /**
     *
     * @var KycDocumentStatus
     */
    protected $model;

    /**
     */
    public function __construct(array $attributes = [])
    {
        $this->model = new KycDocumentStatus();

        $this->setAttributes($attributes);
    }

    /** 
     *
     * @return void
     */
    public function save()
    {
        try {
            $this->model->save();
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    public function setDocumentId ($document_id)
    {
        try {
            $this->model->document_id = $document_id;
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    public function setUserId ($user_id)
    {
        try {
            $this->model->user_id = $user_id;
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    public function setComment ($comment)
    {
        try {
            $this->model->comment = $comment;
        } catch (\Throwable $th) {
            logger($th);
        }
    }
    
    /** 
     *
     * @param array $attributes
     * @return void
     */
    public function setAttributes($attributes)
    {
        
        try {
            
            $attributes = $this->transformAttributes($attributes);

            $this->model->fill($attributes);

        } catch (\Throwable $th) {
            logger($th);
        }
    }

    protected function transformAttributes (array $attributes = [])
    {
        return array_merge([
                'status'      => $attributes['Status'],
                'type'        => $attributes['DocType'],
                'lemonway_id' => $attributes['ExtId'],
                'document_id' => $attributes['DocId']
            ],
            $attributes
        );
    }
}