<?php

namespace App\Helpers;

use App\AutoInvestsEvents;
use App\AutoinvestsOrders;

class AutoInvestHelper
{

    /**
     * @var AutoInvestsEvents
     */
    private $model;

    /**
     */
    public function __construct()
    {
        $this->model = new AutoInvestsEvents();
    }

    /** 
     *
     * @return void
     */
    public function save ()
    {
        try {
            $this->model->save();
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    /** 
     *
     */
    public function getId()
    {
        return $this->model->id;
    }

    /** 
     *
     * @param string $description
     * @return void
     */
    public function setDescription(string $description)
    {
        $this->model->description = $description;
    }

    /** 
     *
     * @param string $type
     * @return void
     */
    public function setType(string $type)
    {
        $this->model->type = $type;
    }

    /** 
     *
     * @return void
     */
    public function setTypeProperty()
    {
        $this->setType('property');
    }

    /** 
     *
     * @return void
     */
    public function setTypeMarketplace()
    {
        $this->setType('marketplace');
    }

    /** 
     *
     * @param string $evaluated
     * @return void
     */
    public function setEvaluated($evaluated)
    {
        $this->model->evaluated = $evaluated;
    }

    /** 
     *
     * @param string $evaluated
     * @return void
     */
    public function setLog($log)
    {
        try {
            $ops = $this->model->logs;
            if (is_array($ops)) {
                $ops[] = $log;
                $this->model->logs = $ops;
            } elseif (is_null($ops)) {
                $this->model->logs = [$log];
            }
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    /** 
     *
     * @param string $evaluated
     * @return void
     */
    public function setParameters($parameters)
    {
        try {
            if (is_array($parameters)) {
                $ops = $this->model->parameters;
                if (is_array($ops)) {
                    $ops = array_merge($ops, $parameters);
                    $this->model->parameters = $ops;
                } elseif (is_null($ops)) {
                    $this->model->parameters = $parameters;
                }
            }
        } catch (\Throwable $th) {
            logger($th);
        }
    }

    public function makeOrder ($autoinvest_id, $investment, $market_order_id, $property_id, $transaction_id)
    {
        try {
            $autoinvestsOrders = AutoinvestsOrders::create([
                'autoinvest_id'        => $autoinvest_id,
                'investment'           => $investment,
                'market_order_id'      => $market_order_id,
                'property_id'          => $property_id,
                'transaction_id'       => $transaction_id,
                'autoinvests_event_id' => $this->model->id
            ]);

            $autoinvest = $autoinvestsOrders->autoinvest;

            $autoinvest->investment = $autoinvestsOrders->investment + $autoinvest->investment;

            $autoinvest->save();

        } catch (\Throwable $th) {
            logger('ERROR:AUTOINVEST:CREAR:ORDER');
            logger($th->getMessage());
        }
    }
}