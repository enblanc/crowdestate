<?php

namespace App\Crud\Property\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Infinety\CRUD\Http\Controllers\CrudController;

class InvestmentCrudController extends CrudController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(config('infinety-crud.middleware', 'auth'));
        parent::__construct();
    }

    /**
     * @var array
     */
    public $crud = array(

        // what's the namespace for your entity's model
        'model'              => \App\Investment::class,

        // what name will show up on the buttons, in singural (ex: Add entity)
        'entity_name'        => 'Tipo de Inversión',

        // what name will show up on the buttons, in plural (ex: Delete 5 entities)
        'entity_name_plural' => 'Tipos de Inversiones',

        // what route have you defined for your entity? used for links.
        'route'              => 'dashboard/property-investment',

        'details_row'        => false,

        'ajax_load'          => true,

        'is_translate'       => true,

        'model_translate'    => \App\InvestmentTranslation::class,

        // *****
        // COLUMNS
        // *****
        //
        // Define the columns for the table view as an array:
        //

        'columns'            => [

            [
                'name'  => 'id',
                'label' => 'id',
            ],
            [
                'name'  => 'nombre',
                'label' => 'Nombre',
                'type'  => 'text',
            ],
        ],

        // *****
        // FIELDS
        // *****
        //
        // Define the fields for the "Edit item" and "Add item" views as an array:
        //

        'fields'             => [

            [
                'name'      => 'nombre',
                'label'     => 'Nombre',
                'type'      => 'text',
                'translate' => true,
            ],

        ],

    );

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        return parent::storeCrud();
    }
    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}
