<?php 

namespace App\Crud\Pages\Models;


use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;

class PageTranslation extends Model implements HasMediaConversions
{
    use Sluggable, HasMediaTrait, SluggableScopeHelpers;


    protected $slugKeyName = 'slug';

    /**
     * A list of methods protected from mass assignment.
     *
     * @var string[]
     */
    protected $guarded = ['_token', '_method'];


    protected $fillable = ['name', 'slug', 'content', 'extras_trans', 'page_id', 'locale'];

    
    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }


    /**
     * Media files conversion
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->setManipulations(['w' => 368, 'h' => 232])
            ->performOnCollections('*');
    }
}