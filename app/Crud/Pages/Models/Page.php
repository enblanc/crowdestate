<?php

namespace App\Crud\Pages\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Infinety\CRUD\CrudTrait;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Dimsav\Translatable\Translatable;

class Page extends Model implements HasMediaConversions
{
    use CrudTrait, Translatable, HasMediaTrait, SluggableScopeHelpers;

    protected $table = 'page';
    protected $fakeColumns = ['extras'];
    protected $fillable = ['name', 'slug', 'content', 'extras', 'template'];

    protected $translatedAttributes = ['name', 'slug', 'content', 'extras_trans'];

    // /**
    //  * Return the sluggable configuration array for this model.
    //  *
    //  * @return array
    //  */
    // public function sluggable()
    // {
    //     return [
    //         'slug' => [
    //             'source' => 'name',
    //         ],
    //     ];
    // }

    // /**
    //  * Get the translations relation.
    //  *
    //  * @return \Illuminate\Database\Eloquent\Relations\HasMany
    //  */
    // public function translations(): HasMany
    // {
    //     return $this->hasMany(\App\Crud\Pages\Models\PageTranslation::class, 'page_id', 'id');
    // }

    /**
     * Media files conversion.
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->setManipulations(['w' => 368, 'h' => 232])
            ->performOnCollections('*');
    }
}
