<?php

namespace App\Crud\Pages\Controllers;

use Illuminate\Http\Request;
use Infinety\CRUD\Http\Controllers\CrudController;
use Infinety\CRUD\Http\Requests\CrudRequest;

// VALIDATION: change the requests to match your own file names if you need form validation

class PageCrudController extends CrudController
{
    public function __construct()
    {
        $this->middleware('auth');
        parent::__construct();
    }

    /**
     * @var array
     */
    public $crud = array(
        // what's the namespace for your entity's model
        'model'              => \App\Crud\Pages\Models\Page::class,
        // what name will show up on the buttons, in singural (ex: Add entity)
        'entity_name'        => 'Página',
        // what name will show up on the buttons, in plural (ex: Delete 5 entities)
        'entity_name_plural' => 'Páginas',
        // what route have you defined for your entity? used for links.
        'route'              => 'dashboard/pages',
        'prepend-url-upload' => 'assets/',
        'ajax_load'          => true,
        'details_row'        => true,
        'is_translate'       => true,
        'model_translate'    => \App\Crud\Pages\Models\PageTranslation::class,
        'redirect_self'      => true,

        // *****
        // COLUMNS
        // *****
        //
        // Define the columns for the table view as an array:
        //
        'columns'            => [
            [
                'name'  => 'id',
                'label' => 'id',

            ],
            [
                'name'  => 'name',
                'label' => 'Page name',
            ],
            [
                'name'  => 'content',
                'label' => 'Content',
            ],
            [
                'name'  => 'slug',
                'label' => 'Slug',
            ],
            [
                'name'  => 'template',
                'label' => 'template',
            ],
            [
                'name'  => 'extras',
                'label' => 'extras',
            ],

        ],
        // *****
        // FIELDS
        // *****
        //
        // Define the fields for the "Edit item" and "Add item" views as an array:
        //
        'fields'             => [
            [
                'name'        => 'template',
                'label'       => 'Template',
                'type'        => 'select_template',
                'options'     => [], // populated automatically in the useTemplate method
                'allows_null' => false,
            ],
        ],
    );

    public function index()
    {
        if (auth()->user()->isRole('super')) {
            return parent::index();
        }

        return redirect()->to('dashboard');
    }

    // Overwrites the CrudController create() method to add template usage.
    /**
     * @param $template
     */
    public function create($template = false)
    {
        $this->useTemplate($template);

        return parent::create();
    }

    // Overwrites the CrudController store() method to add template usage.
    /**
     * @param CrudRequest $request
     */
    public function store(CrudRequest $request)
    {
        $this->useTemplate(\Request::input('template'));

        return parent::storeCrud($request);
    }

    // Overwrites the CrudController edit() method to add template usage.
    /**
     * @param $id
     * @param $template
     */
    public function edit($id, $template = false)
    {
        // use the template in the GET parameter if it exists
        if ($template) {
            $this->useTemplate($template);
        }
        // otherwise use the template value stored in the database
        else {
            $model = $this->crud['model'];
            $this->data['entry'] = $model::findOrFail($id);
            $this->useTemplate($this->data['entry']->template);
        }

        return parent::edit($id);
    }

    // Overwrites the CrudController update() method to add template usage.
    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        $this->useTemplate(\Request::input('template'));

        return parent::updateCrud();
    }
    // -----------------------------------------------
    // Methods that are particular to the PageManager.
    // -----------------------------------------------
    /**
     * @return mixed
     */
    private function getTemplates()
    {
        // get the files from config/dick/page_templates

        //$template_files = realpath(base_path('resources/views/page_templates'));
        //dd($template_files);
        $template_files = \Storage::disk('resources')->files('views/page_templates');

        if (!count($template_files)) {
            abort('403', 'Template files are missing.');
        }
        $templates = [];
        foreach ($template_files as $k => $template_file) {
            // get the file name
            $file_name = str_replace('.php', '', last(explode('/', $template_file)));
            $file_name = str_replace('.blade', '', $file_name);
            if ($file_name != '.DS_Store') {
                // get the pretty template name
                $templates[$file_name] = config('infinety.page_templates.'.$file_name.'.template_name');
            }
        }

        return $templates;
    }
    /**
     * @param $file_name
     */
    private function useTemplate($file_name = false)
    {
        if (!$file_name) {
            $file_name = array_keys($this->getTemplates())[0];
        }

        // merge the fields defined above and the ones set in the template file
        $this->crud['fields'] = array_merge($this->crud['fields'], config('infinety.page_templates.'.$file_name.'.fields'));

        // set the possible options for the "templates" field and select the default value
        foreach ($this->crud['fields'] as $key => $field) {
            if ($field['name'] == 'template') {
                $this->crud['fields'][$key]['value'] = $file_name;
                $this->crud['fields'][$key]['options'] = $this->getTemplates();
            }
        }
    }
}
