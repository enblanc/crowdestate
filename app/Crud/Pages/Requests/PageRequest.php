<?php namespace starter\Admin\Pages\Requests;

use starter\Http\Requests\Request;
class PageRequest extends \Infinety\CRUD\Http\Requests\CrudRequest {
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // only allow updates if the user is logged in
        return \Auth::check();
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //'name' => 'required|min:4|max:255',
            //'content' => 'required|min:4',
            'slug' => 'unique:page,slug,'.\Request::get('id')
        ];
    }
}