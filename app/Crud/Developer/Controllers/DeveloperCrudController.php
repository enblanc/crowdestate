<?php

namespace App\Crud\Developer\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Infinety\CRUD\Http\Controllers\CrudController;

class DeveloperCrudController extends CrudController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(config('infinety-crud.middleware', 'auth'));
        parent::__construct();
    }

    /**
     * @var array
     */
    public $crud = array(

        // what's the namespace for your entity's model
        'model'              => \App\Developer::class,

        // what name will show up on the buttons, in singural (ex: Add entity)
        'entity_name'        => 'Promotor',

        // what name will show up on the buttons, in plural (ex: Delete 5 entities)
        'entity_name_plural' => 'Promotores',

        // what route have you defined for your entity? used for links.
        'route'              => 'dashboard/developer',

        'details_row'        => false,

        'ajax_load'          => true,

        'is_translate'       => false,

        // *****
        // COLUMNS
        // *****
        //
        // Define the columns for the table view as an array:
        //

        'columns'            => [

            [
                'name'  => 'id',
                'label' => 'id',
            ],
            [
                'name'  => 'nombre',
                'label' => 'Nombre',
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
                'type'  => 'email',
            ],
            [
                'name'  => 'direccion',
                'label' => 'Direccion',
                'type'  => 'text',
            ],
        ],

        // *****
        // FIELDS
        // *****
        //
        // Define the fields for the "Edit item" and "Add item" views as an array:
        //

        'fields'             => [

            [
                'name'  => 'nombre',
                'label' => 'Nombre',
                'type'  => 'text',
            ],
            [
                'name'  => 'direccion',
                'label' => 'Direccion',
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
                'type'  => 'email',
            ],
            [
                'name'  => 'contacto',
                'type'  => 'text',
                'label' => 'Contacto',
            ],

        ],

    );

    /**
     * @param Request $request
     */
    public function store(Request $request)
    {
        return parent::storeCrud();
    }
    /**
     * @param Request $request
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}
