<?php

CRUD::resource('dashboard/developer', 'Developer\Controllers\DeveloperCrudController');

CRUD::resource('dashboard/property-state', 'Property\Controllers\PropertyStateCrudController');

CRUD::resource('dashboard/property-type', 'Property\Controllers\PropertyTypeCrudController');

CRUD::resource('dashboard/property-investment', 'Property\Controllers\InvestmentCrudController');
