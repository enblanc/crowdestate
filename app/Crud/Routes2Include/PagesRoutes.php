<?php

// Route::group(['middleware' => 'role:admin'], function()
// {

	CRUD::resource('dashboard/pages',  'Pages\Controllers\PageCrudController');

	Route::get('dashboard/pages/create/{template}', 'Pages\Controllers\PageCrudController@create')->name('dashboard.pages.create.template');
	Route::get('dashboard/pages/{id}/edit/{template}', 'Pages\Controllers\PageCrudController@edit')->name('dashboard.pages.edit.template');


// });

