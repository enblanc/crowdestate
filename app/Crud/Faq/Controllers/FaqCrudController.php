<?php

namespace App\Crud\Faq\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Infinety\CRUD\Http\Controllers\CrudController;

class FaqCrudController extends CrudController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(config('infinety-crud.middleware', 'auth'));
        parent::__construct();
    }

    /**
     * @var array
     */
    public $crud = array(

        // what's the namespace for your entity's model
        'model'              => \App\Faq::class,

        // what name will show up on the buttons, in singural (ex: Add entity)
        'entity_name'        => 'FAQ',

        // what name will show up on the buttons, in plural (ex: Delete 5 entities)
        'entity_name_plural' => 'FAQs',

        // what route have you defined for your entity? used for links.
        'route'              => 'dashboard/faqs',

        'details_row'        => false,

        'ajax_load'          => true,

        'is_translate'       => true,

        'model_translate'    => \App\FaqTranslation::class,

        // *****
        // COLUMNS
        // *****
        //
        // Define the columns for the table view as an array:
        //

        'columns'            => [

            [
                'name'  => 'id',
                'label' => 'id',
            ],
            [
                'name'      => 'nombre',
                'label'     => 'Nombre',
                'type'      => 'text',
                'translate' => true,
            ],
            [
                'name'      => 'texto',
                'type'      => 'redactor',
                'label'     => 'Texto',
                'translate' => true,
            ],
            [
                'name'          => 'faq_types_id',
                'label'         => 'Tipo de Faq',
                'type'          => 'model_function',
                'function_name' => 'typeName',
            ],

        ],

        // *****
        // FIELDS
        // *****
        //
        // Define the fields for the "Edit item" and "Add item" views as an array:
        //

        'fields'             => [

            [
                'name'      => 'nombre',
                'label'     => 'Nombre',
                'type'      => 'text',
                'translate' => true,
            ],
            [
                'name'      => 'texto',
                'type'      => 'redactor',
                'label'     => 'Texto',
                'height'    => '200',
                'translate' => true,
            ],
            [
                'name'      => 'faq_types_id',
                'label'     => 'FAQ Type',
                'type'      => 'select2',
                'entity'    => 'faq_types_id', // the method that defines the relationship in your Model
                'attribute' => 'nombre', // foreign key attribute that is shown to user
                'model'     => "\App\FaqType", // foreign key model
                'pivot'     => false,
            ],

        ],

    );

    /**
     * @param StoreRequest $request
     */
    public function store(Request $request)
    {
        return parent::storeCrud();
    }
    /**
     * @param UpdateRequest $request
     */
    public function update(Request $request)
    {
        return parent::updateCrud();
    }
}
