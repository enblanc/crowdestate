<?php

namespace App\Crud\User\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UserCreateRequest;
use App\Http\Requests\User\UserUpdateRequest;
use App\User;
use Infinety\CRUD\Http\Controllers\CrudController;

class UserCrudController extends CrudController
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware(config('infinety-crud.middleware', 'auth'));
        parent::__construct();
    }

    /**
     * @var array
     */
    public $crud = array(

        // what's the namespace for your entity's model
        'model'              => \App\User::class,

        // what name will show up on the buttons, in singural (ex: Add entity)
        'entity_name'        => 'User',

        // what name will show up on the buttons, in plural (ex: Delete 5 entities)
        'entity_name_plural' => 'Users',

        // what route have you defined for your entity? used for links.
        'route'              => 'dashboard/userv1',

        'details_row'        => false,

        'ajax_load'          => true,

        'is_translate'       => false,

        // *****
        // COLUMNS
        // *****
        //
        // Define the columns for the table view as an array:
        //

        'columns'            => [

            [
                'name'  => 'id',
                'label' => 'id',
            ],
            [
                'name'  => 'nombre',
                'label' => 'Nombre',
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
                'type'  => 'email',
            ],
            [
                'name'      => 'roles',
                'label'     => 'Roles',
                'type'      => 'select2_multiple',
                'entity'    => 'roles', // the method that defines the relationship in your Model
                'attribute' => 'name', // foreign key attribute that is shown to user
                'model'     => "\HttpOz\Roles\Models\Role", // foreign key model
                'pivot'     => true,
            ],

        ],

        // *****
        // FIELDS
        // *****
        //
        // Define the fields for the "Edit item" and "Add item" views as an array:
        //

        'fields'             => [

            [
                'name'  => 'nombre',
                'label' => 'Nombre',
                'type'  => 'text',
            ],
            [
                'name'  => 'apellido1',
                'label' => 'Apellido 1',
                'type'  => 'text',
            ],
            [
                'name'  => 'apellido2',
                'label' => 'Apellido 2',
                'type'  => 'text',
            ],
            [
                'name'  => 'email',
                'label' => 'Email',
                'type'  => 'email',
            ],
            [
                'name'  => 'password',
                'type'  => 'password',
                'label' => 'Contraseña',
                'value' => '',
            ],
            [
                'name'  => 'password_confirmation',
                'type'  => 'password',
                'label' => 'Confirmar contraseña',
            ],
            [
                'name'  => 'porcentaje_invertido',
                'type'  => 'number',
                'label' => 'Prescriptor: Porcentaje sobre el invertido de sus referidos',
            ],
            [
                'name'  => 'porcentaje_ganado',
                'type'  => 'number',
                'label' => 'Prescriptor: Porcentaje sobre lo ganado de sus referidos',
            ],

            [
                'name'      => 'roles',
                'label'     => 'Roles',
                'type'      => 'select2_multiple',
                'entity'    => 'roles',
                'attribute' => 'name',
                'model'     => "\HttpOz\Roles\Models\Role",
                'pivot'     => true,
            ],

        ],

    );

    /**
     * @param StoreRequest $request
     */
    public function store(UserCreateRequest $request)
    {

        $user = User::create($request->all());

        // $user->confirm()->create(['is_confirmed' => true, 'hash' => null]);
        $user->attachRole($request->get('roles'));

        // dump($user->id);
        // dd();
        // redirect the user where he chose to be redirected
        switch ($request->get('redirect_after_save')) {
            case 'current_item_edit':
                $url = redirect()->to($this->crud['route'].'/'.$item->id.'/edit');
                break;

            default:
                $url = redirect()->to(\Request::input('redirect_after_save'));
                break;
        }

        return $url;
    }
    /**
     * @param UpdateRequest $request
     */
    public function update(UserUpdateRequest $request)
    {
        $model = $this->crud['model'];

        $this->prepareFields($model::find(\Request::input('id')));
        $all_variables = \Request::all();

        // if the change_password field has been filled, change the password
        if ($all_variables['password_confirmation'] != '') {
            $all_variables['password'] = $all_variables['password_confirmation'];
        }
        $item = $model::find(\Request::input('id'))->update(parent::compactFakeFields($all_variables));

        if (\Request::has('roles')) {
            // if it's a relationship with a pivot table, also sync that
            foreach ($this->crud['fields'] as $k => $field) {
                if (isset($field['pivot']) && $field['pivot'] == true) {
                    $model::find(\Request::input('id'))->{$field['entity']}()->sync(\Request::input($field['name']));
                }
            }
        }
        // show a success message

        return \Redirect::to($this->crud['route']);

        return parent::updateCrud();
    }
}
