<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clickid extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'click_id',
        'provider'
    ];

    /**
     * Return user bank accounts
     *
     */
    public function logs()
    {
        return $this->hasMany(\App\ClickidsLog::class, 'user_id', 'user_id');
    }

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(\App\User::class);
    }
}
