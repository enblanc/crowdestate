<?php

namespace App;

use App\Property;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Infinety\CRUD\CrudTrait;

class PropertyType extends Model
{
    use Translatable, CrudTrait;

    /**
     * @var array
     */
    protected $fillable = ['property_type_id', 'nombre', 'locale'];

    /**
     * @var array
     */
    public $translatedAttributes = ['nombre'];

    /**
     * Get properties of a investment
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties(): HasMany
    {
        return $this->hasMany(Property::class);
    }

}
