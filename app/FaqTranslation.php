<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqTranslation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['faq_id', 'faq_types_id', 'nombre', 'texto', 'locale'];
}
