<?php

namespace App;

use App\Property;
use App\Traits\Searchable;
use App\Transformers\MarketOrderTransformer;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class MarketOrder extends Model implements Transformable
{
    use Searchable, SoftDeletes;

    /**
     * @var string
     */
    protected $table = 'marketplace_orders';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'position',
        'user_id',
        'property_id',
        'property_user_id',
        'quantity',
        'price',
        'status'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'status'   => 'boolean',
        'executed' => 'boolean'
    ];

    /**
     * @var array
     */
    protected $with = ['user'];

    /**
     * User of the MarketOrder
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)->setEagerLoads([]);
    }

    /**
     * Inversion of the MarketOrder
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function inversion()
    {
        return $this->belongsTo(PropertyUser::class, 'property_user_id');
    }

    /**
     * Property of the MarketOrder
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Property of the MarketOrder : v2
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function inversion_property ()
    {
        return $this->belongsTo(Property::class, 'inversion_property_id', 'id');
    }

    /**
     * Set fields be able to search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return ['type', 'position', 'quantity', 'price'];
    }

    /**
     * Execute order
     */
    public function execute()
    {
        $this->executed = true;
        $this->save();
    }

    /**
     * Set the transaction
     */
    public function setTransaction($id)
    {
        $this->transaction_id = $id;
        $this->save();
    }

    /**
     * Devuelve el tipo en texto
     *
     * @return  string
     */
    public function getTypeTextAttribute(): string
    {
        if ($this->type == 1) {
            return __('Venta');
        }

        return __('Compra');
    }

    /**
     * Devuelve el tipo en texto
     *
     * @return  string
     */
    public function getStatusTextAttribute(): string
    {
        if ($this->status) {
            return __('Activa');
        }

        return __('No activa');
    }

    /**
     * Get percent quantity
     *
     * @return  float
     */
    public function getPercentQuantityAttribute(): float
    {
        if ($this->type == 2) {
            return 0;
        }

        if ($this->inversion->total == 0) {
            return 0;
        }
                try {
            return ($this->quantity * 100) / $this->inversion->total;
        } catch (\Throwable $th) {
            return 0;
        }

//        return number_format(($this->quantity * 100) / $this->inversion->total, 2);
    }

    /**
     * Get percent price
     *
     * @return  float
     */
    public function getPriceMoneyAttribute(): float
    {
        if ($this->price == 0) {
            return $this->quantity;
        }

        return (($this->price * $this->quantity) / 100) + $this->quantity;

        // return number_format((($this->price * $this->quantity) / 100) + $this->quantity, 2, ',', '');
    }

    public function getNumberSign()
    {
        $so = ($this->price > 0) - ($this->price < 0);

        if ($so == 0) {
            return '';
        }

        return ($so) ? '+' : '-';
    }

    /**
     * Get the property Id
     *
     * @return  \App\Property
     */
    public function getProperty()
    {
        return ($this->type == 1) ? $this->inversion->property : $this->property;
        // $propertyId = $this->property_id;
        // if ($this->type == 1) {
        //     $propertyId = $this->inversion->property_id;
        // }

        // return Cache::rememberForever('property_'.$propertyId, function () use ($propertyId) {
        //     return Property::find($propertyId);
        // });
    }

    /**
     * Get the property Id
     *
     * @return  integer
     */
    public function getPropertyId()
    {
        return ($this->type == 1) ? $this->inversion->property_id : $this->property_id;
    }

    /**
     * Get the property name
     *
     * @return  string
     */
    public function getPropertyName()
    {
        return ($this->type == 1) ? $this->inversion->property->nombre : $this->property->nombre;
    }

    /**
     * Get order link
     *
     * @return  string
     */
    public function getLinkAttribute()
    {
        return route('front.market.show', $this->hashId);
    }

    /**
     * @return mixed
     */
    public function getUpdatedDateFormattedAttribute()
    {
        $locale = localization()->getCurrentLocale();

        if ($locale === 'en') {
            return $this->updated_at->format('m/d/Y');
        }

        return $this->updated_at->format('d/m/Y');
    }

    /**
     * @return mixed
     */
    public function getUpdatedDateTimeFormattedAttribute()
    {
        $locale = localization()->getCurrentLocale();

        if ($locale === 'en') {
            return $this->updated_at->format('m/d/Y h:i');
        }

        return $this->updated_at->format('d/m/Y h:i');
    }

    /**
     * Get HasIdAttribute
     *
     * @return  string
     */
    public function getHashIdAttribute()
    {
        return strtoupper(Hashids::connection('orders')->encode($this->id));
    }

    /**
     * Scope a query to only include executed orders.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeExecuted($query)
    {
        return $query->where('executed', true);
    }

    /**
     * Scope a query to only include executed orders.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereType($query, $type)
    {
        if ($type)
            return $query->where('type', $type);
        return $query;
    }
    
    public function scopeWhereProperty($query, $property_id)
    {
        if ($property_id) {
            return $query->whereHas('property', function ($q) use ($property_id) {
                $q->where('id', $property_id);
            })->orWhereHas('inversion', function ($q) use ($property_id) {
                $q->whereHas('property', function ($q) use ($property_id) {
                    $q->where('id', $property_id);
                });
            });
        }
        return $query;
    }

    public function scopeWhereCity($query, $city)
    {
        if ($city) {
            return $query->whereHas('property', function ($q) use ($city) {
                $q->where('ubicacion', $city);
            })->orWhereHas('inversion', function ($q) use ($city) {
                $q->whereHas('property', function ($q) use ($city) {
                    $q->where('ubicacion', $city);
                });
            });
        }
        return $query;
    }

    public function scopeWhereInvested($query, $invested, $myInversions)
    {
        if ($invested === 'true') {
            return $query->whereHas('property', function ($q) use ($myInversions) {
                $q->whereIn('id', $myInversions);
            })->orWhereHas('inversion', function ($q) use ($myInversions) {
                $q->whereHas('property', function ($q) use ($myInversions) {
                    $q->whereIn('id', $myInversions);
                });
            });
        }
    }

    /**
     * The transformer used to transform the model data.
     *
     * @return Transformer|callable|string|null
     */
    public static function transformer()
    {
        return MarketOrderTransformer::class;
    }
}
