<?php

namespace App\Traits;

use App\Helpers\TokensHelper;
use App\ReferralGift;
use App\Traits\TargetCircleTrait;
use App\User;
use HttpOz\Roles\Models\Role;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\Notification;
use Infinety\EmailConfirmation\Notifications\ConfirmEmailNotification;
use Infinety\EmailConfirmation\Satellite;
use Newsletter;

trait UserRegistrationEventsTrait
{
    use TargetCircleTrait;

    /**
     * New user model
     *
     * @var App\User
     */
    protected $newUser;

    /**
     * @param User $user
     */
    public function performEventsActions(User $user)
    {
        $this->newUser = $user;

        try {
            //Creamos los datos para que confirme su cuenta
            $this->createConfirmData();

            //Comprobamos si tiene un referido
            $this->checkReferral();

            //Le creamos su código de afiliado
            $this->setAffiliateId();

            //Asignamos el Rol de usuario
            $this->setUserDefaultRole();

            //Creamos el perfil pero vacío
            $this->createNewProfile();

            //Asignamos el idioma del usuario
            $this->setLocaleForUser();

            //Comprobamos si tiene un referido
            $this->checkClickId();

            //Comprobamos si hay que registrarlo en el newsletter
            $this->checkNewsletter();

            //Le creamos la cuenta LemonWay
            // $this->createNewLemonWayAccount();

            return true;
        } catch (Exception $e) {
            return false;
        }

        return false;
    }

    private function createConfirmData()
    {
        Satellite::createUnconfirmed($this->newUser);
        $notificationClass = app(config('email-confirmation.notifiable_class', ConfirmEmailNotification::class));
        Notification::send($this->newUser, new $notificationClass($this->newUser));
    }

    /**
     * Check if referral cookie is set
     */
    private function checkReferral()
    {
        if (session()->has('referred')) {
            $referral = session()->get('referred');
            $referralUser = User::where('affiliate_id', $referral)->first();

            if ($referralUser) {
                $this->newUser->setReferral($referralUser);
                $this->createReferralGift($referralUser);
            }
        } elseif (Cookie::has('referral')) {
            $referral = Cookie::get('referral');
            $referralUser = User::where('affiliate_id', $referral)->first();
            if ($referralUser) {
                $this->newUser->setReferral($referralUser);
                $this->createReferralGift($referralUser);
            }
        }
        session()->forget('referred');
    }

    /**
     * Create a referral gift for the given user and referral user
     */
    private function createReferralGift($referralUser)
    {
        //Asigamos el regalo sin ejecutar

        $giftUser = ($referralUser->gift_referidor !== null) ? $referralUser->gift_referidor : env('GIFT_USER', 0);

        $gift = [
            'user_id'         => $this->newUser->id,
            'referral_id'     => $referralUser->id,
            'user_amount'     => $giftUser,
            'referral_amount' => env('GIFT_REFERRAL', 0),
            'executed'        => false,
        ];

        ReferralGift::create($gift);

        $this->newUser->gifts()->create(['importe' => $giftUser]);
    }

    /**
     * Set unique afiliate id
     */
    private function setAffiliateId()
    {
        $this->newUser->affiliate_id = TokensHelper::generateUniqueToken('App\User');
        $this->newUser->save();
    }

    /**
     * Set default user role
     */
    private function setUserDefaultRole()
    {
        $userRole = Role::whereSlug('user')->first();
        $this->newUser->attachRole($userRole);
    }

    /**
     * Saves empty profile for user
     */
    private function createNewProfile()
    {
        $this->newUser->profile()->create([]);
    }

    /**
     * Se the current app locale to the user
     */
    private function setLocaleForUser()
    {
        $locale = localization()->getCurrentLocale();
        $this->newUser->locale = $locale;
        $this->newUser->save();
    }

    /**
     * Check if clickId cookie is set
     */
    private function checkClickId()
    {
        $clickId = false;
        if (session()->has('click_id')) {
            $clickId = session()->get('click_id');
        } elseif (Cookie::has('click_id')) {
            $clickId = Cookie::get('click_id');
        }

        if ($clickId != false) {
            $this->newUser->click_id = $clickId;
            $this->newUser->save();

            $this->callCircleTarget($this->newUser, 'register');
        }

        session()->forget('click_id');
    }

    private function checkNewsletter()
    {
        if (session()->has('newsletter')) {
            Newsletter::subscribeOrUpdate(
                $this->newUser->email,
                ['FNAME' => $this->newUser->nombre, 'LNAME' => $this->newUser->apellido1],
                'subscribers',
                ['language' => $this->newUser->locale]
            );
        }
    }
}
