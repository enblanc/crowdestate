<?php

namespace App\Traits;

use App\ReferralGift;
use App\User;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Trait ReferralsTrait.
 * Get relations for user referrals and referred by.
 * Also has the methods to set or remove referral
 */
trait ReferralsTrait
{
    /**
     * Get the referred user if is
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function referrer(): BelongsTo
    {
        return $this->belongsTo('App\User', 'referred_by');
    }

    /**
     * Return referrals for current user
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function referrals(): HasMany
    {
        return $this->hasMany('App\User', 'referred_by');
    }

    /**
     * Return gifts as referral user
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function giftsAsReferral(): HasMany
    {
        return $this->hasMany(ReferralGift::class, 'referral_id')->where('executed', 0);
    }

    /**
     * Return gifts as referred by user
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function giftsAsReferred(): HasMany
    {
        return $this->hasMany(ReferralGift::class, 'user_id')->where('executed', 0);
    }

    /**
     * Set referral for a given referral and give referred users
     *
     * @param App\User $referral
     */
    public function setReferral(User $referral): bool
    {
        $this->referred_by = $referral->id;

        return $this->save();
    }
}
