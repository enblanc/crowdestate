<?php

namespace App\Traits;

use Illuminate\Support\Facades\File;

trait MediaHelpersTrait
{
    /**
     * Add file to media collection
     *
     * @param  string  $file
     * @param  string  $name
     * @param  boolean $encryptName
     * @param  boolean $preserve
     * @param  string  $collection
     *
     * @return object
     */
    public function uploadFileToMedia($file, $name, $encryptName = true, $preserve = false, $collection = 'default', $request = false, array $customProperties = [])
    {
        $ext = pathinfo($file, PATHINFO_EXTENSION);

        if ($request == true) {
            $ext = $file->getClientOriginalExtension();
        }

        if ($encryptName) {
            $name = md5($name);
        }

        $newMedia = $this->addMedia($file);

        if (count($customProperties) > 0) {
            $newMedia->withCustomProperties($customProperties);
        }

        if ($preserve) {
            $newMedia->preservingOriginal();
        }

        $mediaFile = $newMedia->usingName($name)->usingFileName($name.'.'.$ext)->toCollection($collection);

        return $mediaFile;
    }

    /**
     * Add file stream to media collection
     *
     * @param  string  $file
     * @param  string  $name
     * @param  boolean $encryptName
     * @param  boolean $preserve
     * @param  string  $collection
     *
     * @return object
     */
    public function uploadFileStreamToMedia($file, $name, $encryptName = true, $preserve = false, $collection = 'default', array $customProperties = [])
    {
        $folder = $file_image_location = storage_path('images_dnis');
        //create temporary folder
        if (!file_exists($folder)) {
            mkdir($folder, 0777, true);
        }

        File::cleanDirectory(storage_path('images_dnis'));

        $tempFilePath = $folder.'/'.uniqid().'.jpg';
        $file->save($tempFilePath);

        if ($encryptName) {
            $name = md5($name);
        }

        $newMedia = $this->addMedia($tempFilePath);

        if (count($customProperties) > 0) {
            $newMedia->withCustomProperties($customProperties);
        }

        if ($preserve) {
            $newMedia->preservingOriginal();
        }

        $mediaFile = $newMedia->usingName($name)->usingFileName($name.'.jpg')->toCollection($collection);

        @unlink($tempFilePath);

        return $mediaFile;
    }
}
