<?php

namespace App\Traits;

use App\Transaction;
use App\User;
use GuzzleHttp\Client;

trait TargetCircleTrait
{

    /**
     * @var string
     */
    protected $baseUrl = 'https://p.trackmytarget.com/';

    /**
     * @var mixed
     */
    protected $user;

    /**
     * @param String $type
     */
    public function callCircleTarget(User $user, String $type)
    {
        $this->user = $user;

        if ($type == 'register') {
            $this->registerEvent();
        }
    }

    /**
     * Inform new registration on Target Circle
     *
     * @return  null
     */
    public function registerEvent()
    {
        $clickId = $this->user->click_id;

        if ($clickId == null) {
            return true;
        }

        $client = new Client();

        try {
            $res = $client->request('GET', $this->baseUrl, [
                'query'       => [
                    'tmt_data'  => $clickId,
                    'type'      => 'lead',
                    'event_sid' => 'k5rdhv',
                    'id'        => $this->user->id,
                ],
                'http_errors' => false,
            ]);
        } catch (\Exception $e) {
            return true;
        }
    }

    /**
     * Inform new investment on Target Circle
     *
     * @param   \App\Transaction  $transaction
     *
     * @return  null
     */
    public function investmentEvent(Transaction $transaction)
    {
        $clickId = $transaction->user->click_id;

        if ($clickId == null) {
            return true;
        }

        $client = new Client();

        try {
            $res = $client->request('GET', $this->baseUrl, [
                'query'       => [
                    'tmt_data'  => $clickId,
                    'type'      => 'sale',
                    'event_sid' => 'gce9ec',
                    'id'        => $transaction->id,
                    'amount'    => $transaction->debit,
                    'currency'  => 'EUR',
                ],
                'http_errors' => false,
            ]);
        } catch (\Exception $e) {
            return true;
        }
    }

    /**
     * Inform new investment on Target Circle
     *
     * @param   \App\Transaction  $transaction
     *
     * @return  null
     */
    public function firstInvestmentEvent(Transaction $transaction)
    {
        $clickId = $transaction->user->click_id;

        if ($clickId == null) {
            return true;
        }

        $client = new Client();

        try {
            $res = $client->request('GET', $this->baseUrl, [
                'query'       => [
                    'tmt_data'  => $clickId,
                    'type'      => 'sale',
                    'event_sid' => 'ra1cff',
                    'id'        => $transaction->id,
                    'amount'    => $transaction->debit,
                    'currency'  => 'EUR',
                ],
                'http_errors' => false,
            ]);
        } catch (\Exception $e) {
            return true;
        }
    }
}
