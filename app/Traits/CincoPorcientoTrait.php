<?php

namespace App\Traits;

use App\PropertyUser;
use Carbon\Carbon;

trait CincoPorcientoTrait
{

    /**
     * @param Carbon $date
     */
    public function getPorcentaje5()
    {
        $total = 0;
        $noProperties = [23, 58];

        if ($this->properties->count() > 0) {
            foreach ($this->getListPropertiesInvertidos() as $property) {
                $pagado = $this->transactions()->whereType(18)->whereStatus(3)->where('extras', $property->id)->first();

                if (!$pagado && !in_array($property->id, $noProperties)) {
                    $inversiones = PropertyUser::where('property_id', $property->id)->where('user_id', $this->id)->get();

                    foreach ($inversiones as $inversion) {
                        $eurosPorciento = ($inversion->total * 5) / 100;

                        $diasPasadosDesdeInversion = $this->getDiasPasados($inversion);
                        $total += ($eurosPorciento * $diasPasadosDesdeInversion) / (365 + Carbon::now()->format('L'));
                    }
                }
            }
        }

        return number_format($total, 2);
    }

    /**
     * @return mixed
     */
    private function getDiasPasados(PropertyUser $inversion)
    {
        $diaInversion = $inversion->created_at;
        $hoy = Carbon::now();

        return $hoy->diffInDays($diaInversion);
    }
}
