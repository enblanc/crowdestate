<?php

namespace App\Traits;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Cookie;

use App\User;
use App\Clickid;
use App\ClickidsLog;

trait UserClickIdEventsTrait
{

    /**
     * @var mixed
     */
    protected $user;

    /**
     * Click_id 
     *
     * @var string
     */
    protected $click_id;
    
    /**
     * Provider
     *
     * @var string
     */
    protected $current_provider;

    /**
     * Providers
     *
     * @var array
     */
    protected $providers;

    /**
     * @param String $type
     */
    public function callEventRegisterClickId(User $user)
    {
        $this->user = $user;
        
        $this->click_id = $this->callCookie(config('clickid.cookie-click-id'));

        $this->current_provider = $this->callCookie(config('clickid.cookie-provider'));

        $this->providers = config('clickid.providers');
        
        $this->registerEvent();
    }

    /**
     * Inform new registration on Target Circle
     *
     * @return  null
     */
    public function registerEvent()
    {
        if ($this->current_provider && $this->click_id) {
            $data = [
                'click_id'  =>  $this->click_id,
                'provider'  =>  $this->current_provider,
                'user_id' =>  $this->user->id
            ];

            Clickid::create($data);
            
            $this->callEventPostBack($data);
        }
    }
    
    /**
     * Inform new registration on Target Circle
     *
     * @return  null
     */
    public function callEventPostBack($data)
    {
        try {
            if (array_key_exists($this->current_provider, $this->providers)) {
                $provider = $this->providers[$this->current_provider]['endpoint'];
                if (array_key_exists('register', $provider)) {
                    $endpoint = str_replace(['click_id', 'provider', 'user_id'], $data, $provider['register']);
                    $this->emitCallBack($endpoint);
                }
            }
        } catch (\Throwable $th) {
            logger('CLICK_ID:ERROR');
            logger($th);
        }
    }

    /**
     * Hace un llamado tipo GET a la URL del proveedor
     *
     * @param   string  $endpoint
     *
     * @return  void
     */
    protected function emitCallBack ($endpoint)
    {
        try {
            $client = new Client();
            $res = $client->request('GET', $endpoint, ['http_errors' => true]);

            $this->createLog([
                'user_id'   =>  $this->user->id,
                'click_id'  =>  $this->click_id,
                'provider'  =>  $this->current_provider,
                'postback'  =>  $endpoint,
                'status'    =>  1,
                'message'   =>  'CLICK_ID:REGISTER'
            ], 'EVENTO:CLICK_ID:REGISTER');

        } catch (\Throwable $th) {
            $this->createLog([
                'user_id'   =>  $this->user->id,
                'click_id'  =>  $this->click_id,
                'provider'  =>  $this->current_provider,
                'postback'  =>  $endpoint,
                'message'   =>  $th->getMessage()
            ], $th);
        }
    }

    /**
     * Obtiene la cookie y la elimina
     *
     * @param   string  $name  key de la cookie
     *
     * @return  string         Valor de la cookie
     */
    private function callCookie ($name)
    {
        $cookie = Cookie::get($name);

        Cookie::queue(Cookie::forget($name));
        
        return $cookie;
    }

    /**
     * Recibe un array
     *
     * @param   array  $log 
     *
     * @return  void       
     */
    private function createLog ($log, $logger = null)
    {
        if ($logger) {
            logger($logger);
        }

        try {
            ClickidsLog::create($log);
        } catch (\Throwable $th) {
            logger($th);
        }
    }
}
