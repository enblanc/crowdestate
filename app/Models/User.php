<?php

namespace App\Models;

use App\User as ModelUser;

class User extends ModelUser
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Defaults relations to load within
     *
     * @var array
     */
    protected $with = [];
}
