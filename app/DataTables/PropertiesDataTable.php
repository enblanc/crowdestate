<?php

namespace App\DataTables;

use App\Property;
use Yajra\Datatables\Services\DataTable;

class PropertiesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('image', function ($property) {
                if (count($property->getMedia($property->mediaCollection)) > 0) {
                    return '<img class="img-responsive" src="'.$property->getMedia($property->mediaCollection)->first()->getUrl('thumb').'">';
                } else {
                    return '<img class="img-responsive" src="'.asset('assets/img/no_image.png').'">';
                }
            })
            ->editColumn('fecha_publicacion', function ($property) {
                return $property->fecha_publicacion->format('d/m/Y');
            })
            ->addColumn('publicado', function ($property) {
                if ($property->publicado) {
                    return '<span class="label label-primary">Publicado</span>';
                } else {
                    return '<span class="label label-default">No publicado</span>';
                }
            })
            ->addColumn('estado', function ($property) {

                switch ($property->state->id) {
                    case 1:
                        return '<span class="label label-success">'.$property->state->nombre.'</span>';
                        break;
                    case 2:
                        return '<span class="label label-primary">'.$property->state->nombre.'</span>';
                        break;
                    case 3:
                        return '<span class="label label-alert">'.$property->state->nombre.'</span>';
                        break;
                    case 4:
                        return '<span class="label label-system">'.$property->state->nombre.'</span>';
                        break;
                    case 5:
                        return '<span class="label label-info">'.$property->state->nombre.'</span>';
                        break;
                    case 6:
                        return '<span class="label label-dark">'.$property->state->nombre.'</span>';
                        break;
                    default:
                        return '<span class="label label-default">'.$property->state->nombre.'</span>';
                        break;
                }

                if ($property->state->id == 1) {
                    return '<span class="label label-primary">'.$property->state->nombre.'</span>';
                } else {
                    return '<span class="label label-default">'.$property->state->nombre.'</span>';
                }
            })
            ->addColumn('action', function ($property) {
                $data = '<a href="'.route('inmuebles.edit', $property->id).'" class="btn btn-sm btn-info get-info">Editar inmueble</a>';

                if ($property->users->count() == 0) {

                    $data .= '<a href="'.route('inmuebles.destroy', $property->id).'" class="btn btn-sm  btn-danger ml5" data-button-type="delete" ><i class="fa fa-trash-o"></i> Eliminar</a>';
                }

                return $data;
            })
            ->orderColumn('orden', '- orden $1')
            ->orderColumn('fecha_publicacion', 'fecha_publicacion $1')
            ->rawColumns(['image', 'publicado', 'estado', 'action']);
    }

/**
 * Get the query object to be processed by dataTables.
 *
 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
 */
    public function query()
    {
        $query = Property::query();

        // Log::info($query);

        $data = $this->applyScopes($query);

        return $data;
    }

/**
 * Optional method if you want to use html builder.
 *
 * @return \Yajra\Datatables\Html\Builder
 */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['title' => 'Opciones', 'width' => '250px'])
            ->parameters([
                'dom'      => '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                'language' => [
                    'url' => '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json',
                ],
                'order'    => [[1, 'desc']],
            ]);
    }

/**
 * Get columns.
 *
 * @return array
 */
    public function getColumns()
    {
        return [
            [
                'title'      => 'Imagen',
                'data'       => 'image',
                'name'       => 'image',
                'width'      => '250px',
                'orderable'  => false,
                'searchable' => false,
            ],
            // 'id',
            [
                'title'      => 'Orden',
                'data'       => 'orden',
                'name'       => 'orden',
                'orderable'  => true,
                'searchable' => false,
            ],
            'ref',
            'nombre',
            'ubicacion',
            'barrio',
            [
                'title'      => 'Fecha',
                'data'       => 'fecha_publicacion',
                'name'       => 'fecha_publicacion',
                'orderable'  => true,
                'searchable' => false,
            ],
            [
                'title'      => 'Estado',
                'data'       => 'estado',
                'name'       => 'estado',
                'orderable'  => false,
                'searchable' => false,
            ],
            [
                'title'      => 'Publicado',
                'data'       => 'publicado',
                'name'       => 'publicado',
                'orderable'  => false,
                'searchable' => false,
            ],
        ];
    }

/**
 * Get filename for export.
 *
 * @return string
 */
    public function filename()
    {
        return 'Property_'.time();
    }
}
