<?php

namespace App\DataTables;

use App\Payment;
use Yajra\Datatables\Services\DataTable;

class PaymentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('type', function ($payment) {
                return $payment->typeText;
            })
            ->addColumn('user', function ($payment) {
                if ($payment->user) {
                    return $payment->user->nombreCompleto.' (ID: '.$payment->user_id.')';
                }

                return 'Error (Usuario no encontrado)';
            })
            ->editColumn('price', function ($payment) {
                return $payment->price.'€';
            })
            ->editColumn('subject', function ($payment) {
                return $payment->subject;
            })
            ->editColumn('comment', function ($payment) {
                return $payment->comment;
            })
            ->addColumn('transaction', function ($payment) {
                return $payment->transaction_id;
            })
            ->editColumn('status', function ($payment) {
                if ($payment->status == 1) {
                    return '<span class="label label-info">'.$payment->statusText.'</span>';
                }

                if ($payment->status == 2) {
                    return '<span class="label label-warning">'.$payment->statusText.'</span>';
                }

                if ($payment->status == 3) {
                    return '<span class="label label-danger">'.$payment->statusText.'</span>';
                }

                if ($payment->status == 4) {
                    return '<span class="label label-warning">'.$payment->statusText.'</span>';
                }

                if ($payment->status == 5) {
                    return '<span class="label label-danger">'.$payment->statusText.'</span>';
                }

                return '<span class="label label-default">No procesado</span>';
            })
            ->addColumn('action', function ($payment) {
                if ($payment->status == 4) {
                    return '<a href="'.route('dashboard.pagos.tryagain', $payment->id).'" class="btn btn-sm  btn-warning ml5" data-button-type="restart" ><i class="fa fa-refresh"></i> Reintentar</a>';
                }

                return '';
            })
            ->rawColumns(['status', 'action']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Payment::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['title' => 'Opciones', 'width' => '150px'])
            ->parameters([
                'dom'        => '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                'language'   => [
                    'url' => '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json',
                ],
                'order'      => [[2, 'desc']],
                'pageLength' => '100',
                'stateSave'  => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            [
                'title'      => 'Tipo',
                'data'       => 'type',
                'name'       => 'type',
                'orderable'  => false,
                'searchable' => true,
            ],
            [
                'title'      => 'Usuario',
                'data'       => 'user',
                'name'       => 'user',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Precio',
                'data'       => 'price',
                'name'       => 'price',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Concepto',
                'data'       => 'subject',
                'name'       => 'subject',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Comentario',
                'data'       => 'comment',
                'name'       => 'comment',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Transacción',
                'data'       => 'transaction',
                'name'       => 'transaction',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Estado',
                'data'       => 'status',
                'name'       => 'status',
                'orderable'  => true,
                'searchable' => true,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'paymentsdatatable_'.time();
    }
}
