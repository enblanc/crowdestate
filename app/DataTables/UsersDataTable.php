<?php

namespace App\DataTables;

use App\User;
use Yajra\Datatables\Services\DataTable;

class UsersDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->editColumn('nombre', function ($user) {
                return $user->nombre.' '.$user->apellido1.' '.$user->apellido2;
            })
            ->editColumn('fecha', function ($user) {
                return $user->created_at->format('d/m/Y h:m');
            })
            ->editColumn('roles', function ($user) {
                return $user->roles->implode('name', ', ');
            })
            ->addColumn('action', function ($user) {
                $data = '<a href="'.route('dashboard.users.edit', $user->id).'" class="btn btn-sm btn-info get-info">Editar usuario</a>';

                // if ($user->properties->count() == 0 && $user->balance == 0) {

                //     $data .= '<a href="'.route('dashboard.users.destroy', $user->id).'" class="btn btn-sm  btn-danger ml5" data-button-type="delete" ><i class="fa fa-trash-o"></i> Eliminar</a>';
                // }

                return $data;
            });
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = User::query();

        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['title' => 'Opciones', 'width' => '150px'])
            ->parameters([
                'dom'        => '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                'language'   => [
                    'url' => '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json',
                ],
                'order'      => [[2, 'desc']],
                'pageLength' => '100',
                'stateSave'  => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'nombre',
            'email',
            [
                'title'      => 'Fecha registro',
                'data'       => 'created_at',
                'name'       => 'created_at',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Roles',
                'data'       => 'roles',
                'name'       => 'roles',
                'orderable'  => false,
                'searchable' => false,
            ],
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'usersdatatable_'.time();
    }
}
