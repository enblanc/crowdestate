<?php

namespace App\DataTables;

use App\Certification;
use Yajra\Datatables\Services\DataTable;

class CertificationsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('user', function ($certification) {
                return $certification->user->email . ' ('.$certification->user->id.')';
            })
            ->addColumn('action', function ($certification) {
                $data = '<a href="'.route('dashboard.certifications.show', $certification->id).'" class="btn btn-sm btn-info get-info"><i class="fa fa-trash-o"></i></a>';
                $data .= '<a href="'.route('dashboard.certifications.destroy', $certification->id).'" class="btn btn-sm  btn-danger ml5" data-button-type="delete" ><i class="fa fa-trash-o"></i></a>';
                return $data;
            })
            ->rawColumns(['link', 'action']);
    }

    /**
     * Get the query object to be processed by dataTables.
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
     */
    public function query()
    {
        $query = Certification::query();

        $query->orderBy('id', 'desc');
        
        return $this->applyScopes($query);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\Datatables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            ->addAction(['title' => 'Opciones', 'width' => '150px'])
            ->parameters([
                'dom'        => '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                'language'   => [
                    'url' => '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json',
                ],
                'order'      => [[1, 'desc']],
                'pageLength' => '100',
                'stateSave'  => true,
            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'user',
            'year',
            'date',
            'nif',
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'certificactions_'.time();
    }
}
