<?php

namespace App\DataTables;

use App\KycDocumentStatus;
use Yajra\Datatables\Services\DataTable;

class KycDocumentStatusDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @return \Yajra\Datatables\Engines\BaseEngine
     */
    public function dataTable()
    {
        return $this->datatables
            ->eloquent($this->query())
            ->addColumn('user', function ($kyc) {
                if ($kyc->user)
                    return "{$kyc->user->nombreCompleto} ({$kyc->user->id})";

                return "N/D";
            })
            ->addColumn('status', function ($kyc) {
                return $kyc->label_status;
            })
            ->addColumn('type', function ($kyc) {
                return $kyc->label_type;
            })
            ->addColumn('fecha', function ($kyc) {
                return $kyc->created_at->format('d/m/Y h:m');
            });
    }

/**
 * Get the query object to be processed by dataTables.
 *
 * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Query\Builder|\Illuminate\Support\Collection
 */
    public function query()
    {
        $query = KycDocumentStatus::query();

        return $this->applyScopes($query);
    }

/**
 * Optional method if you want to use html builder.
 *
 * @return \Yajra\Datatables\Html\Builder
 */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->ajax('')
            // ->addAction(['title' => 'Opciones', 'width' => '250px'])
            ->parameters([
                'dom'      => '<"dt-panelmenu clearfix"lfr>t<"dt-panelfooter clearfix"ip>',
                'language' => [
                    'url' => '//cdn.datatables.net/plug-ins/1.10.13/i18n/Spanish.json',
                ],
                'order'      => [[5, 'desc']],
                'pageLength' => '100',
            ]);
    }

/**
 * Get columns.
 *
 * @return array
 */
    public function getColumns()
    {
        return [
            [
                'title'      => 'Usuario',
                'data'       => 'user',
                'name'       => 'user',
                'width'      => '200px',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Lemonway ID',
                'data'       => 'lemonway_id',
                'name'       => 'lemonway_id',
                'width'      => '150px',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Document ID',
                'data'       => 'document_id',
                'name'       => 'document_id',
                'width'      => '150px',
                'orderable'  => true,
                'searchable' => true,
            ],
            [
                'title'      => 'Tipo',
                'data'       => 'type',
                'name'       => 'type',
                'width'      => '0px',
                'orderable'  => true,
                'searchable' => false,
            ],
            [
                'title'      => 'Estado',
                'data'       => 'status',
                'name'       => 'status',
                'width'      => 'auto',
                'orderable'  => true,
                'searchable' => false,
            ],
            [
                'title'      => 'Creado el',
                'data'       => 'created_at',
                'name'       => 'created_at',
                'width'      => '150px',
                'orderable'  => true,
                'searchable' => false,
            ],
        ];
    }

/**
 * Get filename for export.
 *
 * @return string
 */
    public function filename()
    {
        return 'Kyc_document_status_'.time();
    }
}
