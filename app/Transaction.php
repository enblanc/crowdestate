<?php

namespace App;

use App\Property;
use App\Transformers\TransactionTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;
use Jenssegers\Date\Date;

class Transaction extends Model implements Transformable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'lemonway_id',
        'wallet',
        'type',
        'status',
        'fecha',
        'debit',
        'credit',
        'iban_id',
        'comment',
        'refund',
        'token',
        'extras',
        'property_user_id',
        'prescriptor',
        'autoinvest',
        'created_at',
        'updated_at'
    ];

    /**
     * @var array
     */
    protected $dates = [
        'fecha',
        'created_at',
        'updated_at'
    ];

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return mixed
     */
    public function propertyUser()
    {
        if ($this->property_user_id != null) {
            $propertyUserId = $this->property_user_id;

            $property = Property::whereHas('users', function ($q) use ($propertyUserId) {
                $q->where('property_user.user_id', $propertyUserId);
            })->first();

            if ($property) {
                return $property;
            }
        }

        if ($this->extras != null) {
            $propertyId = $this->extras;
            $property = Property::find($propertyId);

            if ($property) {
                return $property;
            }
        }

        return false;
    }

    /**
     * Return type in text mode
     *
     * @return string
     */
    public function getTypeTextAttribute()
    {
        switch ($this->type) {
            case 0:
                return __('Pago con tarjeta');
                break;
            case 1:
                return __('Ingreso a mi cuenta');
                break;
            case 2:
                if ($this->extras != null) {
                    $property = Property::find($this->extras);
                    if ($property) {
                        if ($this->autoinvest) {
                            return __('Inversión en Inmueble').': <i>'.$property->nombre.'</i> - AutoInvest';
                        }
                        return __('Inversión en Inmueble').': <i>'.$property->nombre.'</i>';
                    }
                }

                return __('Inversión en Inmueble');
                break;
            case 3:
                return __('Ingreso por transferencia');
                break;
            case 4:
                return __('Devolución');
                break;
            case 13:
                $property = $this->getProperty($this->extras);
                if ($property) {
                    return __('Divindendos generados por Inmueble').': <i>'.$property->nombre.'</i>';
                } else {
                    return __('Divindendos generados por Inmueble: Inmueble Eliminado');
                }
                break;
            case 14:
                if ($this->extras != null) {
                    $property = $this->getProperty($this->extras);
                    if ($property) {
                        return __('Venta de Inmueble').': <i>'.$property->nombre.'</i>';
                    }
                }

                return 'Venta de Inmueble';
            case 15:
                if ($this->extras != null) {
                    $property = $this->getProperty($this->extras);
                    if ($property) {
                        return __('Regalo: Inversión en').': <i>'.$property->nombre.'</i>';
                    }
                }

                return 'Regalo';
            case 16:
                if ($this->extras != null) {
                    $property = $this->getProperty($this->extras);
                    if ($property) {
                        return __('Devolución inmueble no conseguido').': <i>'.$property->nombre.'</i>';
                    }
                }

                return __('Devolución inmueble no conseguido');
            case 17:
                return $this->comment;
                break;
            case 18:
                if ($this->extras != null) {
                    $property = $this->getProperty($this->extras);
                    if ($property) {
                        return __('Diviendos generados del 5% sobre lo invertido en').': <i>'.$property->nombre.'</i>';
                    }
                }

                return $this->comment;
                break;

            case 20:
                $property = $this->getProperty($this->property_user_id);
                if ($property) {
                    return __('Inversión Inmueble').': <i>'.$property->nombre.'</i> ('.__('Mercado').')';
                }

                return __('Inversión Inmueble (Mercado)');
                break;

            case 21:
                $property = $this->getProperty($this->property_user_id);
                if ($property) {
                    return __('Desinversión inmueble').': <i>'.$property->nombre.'</i> ('.__('Mercado').')';
                }

                return __('Desinversión inmueble (Mercado)');
                break;

            case 22:
                return __('Regalo Brickstarter');
                break;
            default:
                return __('Error LemonWay type');
                break;
        }
    }

    /**
     * @param $id
     */
    private function getProperty($id)
    {
        $name = 'transactions_property_id_'.$id;

        return Cache::remember($name, 60 * 12, function () use ($id) {
            return Property::find($id);
        });
    }

    /**
     * Return status in text mode
     *
     * @return string
     */
    public function getStatusTextAttribute()
    {
        switch ($this->status) {
            case 0:
                return __('No realizado');
                break;
            case 3:
                return __('Éxito');
                break;
            case 4:
                return __('Error');
                break;
            case 32:
                return __('Cancelado');
                break;
            case 16:
                return __('Reservation made successfully');
                break;
            default:
                return __('Error LemonWay status');
                break;
        }
    }

    /**
     * @return mixed
     */
    public function getMoneyAttribute()
    {
        if ($this->type == 0 || $this->type == 3 || $this->type == 13 || $this->type == 14 || $this->type == 16 || $this->type == 17 || $this->type == 18 || $this->type == 20 || $this->type == 22) {
            return $this->credit;
        } else {
            return $this->debit;
        }
    }

    /**
     * Devuelve la fecha localizada
     *
     * @param $value
     */
    public function getFechaAttribute($value)
    {
        Date::setLocale(localization()->getCurrentLocale());

        return Date::parse($value);
    }

    /**
     * @return mixed
     */
    public function getFechaCreatedAttribute()
    {
        Date::setLocale(localization()->getCurrentLocale());

        return Date::parse($this->created_at);
    }

    /**
     * @return mixed
     */
    public function getFechaFormattedAttribute()
    {
        $locale = localization()->getCurrentLocale();

        if ($locale === 'en') {
            return $this->created_at->format('m/d/Y');
        }

        return $this->created_at->format('d/m/Y');
    }

    /**
     * Scope a query to only include trasnactions of given LemonWay Transaction ID
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $lemonWayId
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeLemonWayId($query, $lemonWayId)
    {
        return $query->where('lemonway_id', $lemonWayId);
    }

    /**
     * Scope a query to only include trasnactions of given token
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $token
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeToken($query, $token)
    {
        return $query->where('token', $token);
    }

    /**
     * Scope a query to only include trasnactions of given token
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $token
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeType($query, $token)
    {
        return $query->where('type', $token);
    }

    /**
     * The transformer used to transform the model data.
     *
     * @return Transformer|callable|string|null
     */
    public static function transformer()
    {
        return TransactionTransformer::class;
    }
}
