<?php

namespace App\Providers;

use App\Social\LinkedinV2ServiceProvider;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\ServiceProvider;
use Laravel\Dusk\DuskServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (env('APP_SSL')) {
            URL::forceScheme('https');
        }

        $this->bootSpotifySocialite();

        Schema::defaultStringLength(191);
    }

    /**
     * @return mixed
     */
    private function bootSpotifySocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');

        $socialite->extend(
            'linkedinv2',
            function ($app) use ($socialite) {
                $config = $app['config']['services.linkedin'];

                return $socialite->buildProvider(LinkedinV2ServiceProvider::class, $config);
            }
        );
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local', 'testing')) {
            $this->app->register(DuskServiceProvider::class);
        }

        Blade::directive('ifNoTManteinance', function ($expression) {
            return '<?php $manteinance = App::isDownForMaintenance();  if($manteinance !== true) : ?>';
        });
        Blade::directive('endNotManteinance', function ($expression) {
            return '<?php endif; ?>';
        });
    }
}
