<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('allowed_username', 'App\Validation\AllowedUsernameValidator@validate');

        Validator::extend('sheet_name', 'App\Validation\SheetExistsValidator@validate');

        Validator::extend('alpha_spaces', function ($attribute, $value) {
            return preg_match('/^[\pL\s-]+$/u', $value);
        });

        Validator::extend('valid_url', function ($attribute, $value) {
            $pattern = '/(?:https?:\/\/)?(?:[a-zA-Z0-9.-]+?\.(?:[a-zA-Z])|\d+\.\d+\.\d+\.\d+)/';

            return preg_match($pattern, $value);
        });

        Validator::extend('max_balance', function ($attribute, $money) {
            $maxMoneyOut = auth()->user()->balance;

            $cantidad = abs(number_format($money, 2, '.', ''));

            if ($cantidad > $maxMoneyOut) {
                return false;
            } else {
                return true;
            }
        });
    }

    /**
     * @param $user
     */
    private function getMaxMoneyToTransfer($user)
    {
        //Últimos ingresos de dinero por TJ o por transferencia
        $lastTransfersOut = $this->getLastTransfersFromTransaction(auth()->user());
        $balance = $user->balance;
        $moneyOut = $balance - $lastTransfersOut;

        return $moneyOut;
    }

    /**
     * @param $user
     */
    private function getLastTransfersFromTransaction($user)
    {
        $transferOut = 0;
        $transactionsDesc = $user->transactions()->orderBy('id', 'DESC')->get();
        foreach ($transactionsDesc as $transaction) {
            if ($transaction->type == 0 || $transaction->type == 3) {
                $transferOut += $transaction->money;
            } else {
                break;
            }
        }

        return $transferOut;
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
