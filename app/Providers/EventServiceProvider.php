<?php

namespace App\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserInvierteInmueble'                       => [
            'App\Listeners\CheckInversionPorcentaje',
        ],
        'Spatie\MediaLibrary\Events\MediaHasBeenAdded'          => [
            'App\Listeners\MediaLogger',
        ],
        'Spatie\MediaLibrary\Events\ConversionHasBeenCompleted' => [
            'App\Listeners\MediaLoggerConversion',
        ],
        'Illuminate\Notifications\Events\NotificationSent'      => [
            'App\Listeners\LogNotification',
        ],
        'Illuminate\Auth\Events\Registered' => [
            'App\Listeners\RegisterClickId',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
