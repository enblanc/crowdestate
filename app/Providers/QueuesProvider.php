<?php

namespace App\Providers;

use Illuminate\Queue\Events\JobProcessed;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\ServiceProvider;

class QueuesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Queue::after(function (JobProcessed $event) {
            // dump($event);
            $payload = $event->job->payload();
            $fullName = $event->job->resolveName();
            $parts = explode('\\', $fullName);
            $name = end($parts);

            if ($name == 'RepartirDividendos') {
                $data = unserialize($payload['data']['command']);
                $property = $data->getProperty();
                $evolucion = $data->getEvolucion();
                $mes = $data->getMes();
                $year = $data->getYear();

                $procesoAbonarFinalizado = true;
                foreach ($property->usersGroup as $user) {
                    $movimientosDividendos = $user->transactions()->where('type', 13)->where('status', 3)->whereRaw('MONTH(fecha)  = ?', $mes)->whereRaw('YEAR(fecha)  = ?', $year)->where('extras', $property->id)->first();

                    if (!$movimientosDividendos) {
                        $procesoAbonarFinalizado = false;
                    }
                }

                if ($procesoAbonarFinalizado) {
                    $evolucion->abonado = 1;
                    $evolucion->save();
                    Log::info('El proceso de pago ha finalizado. Inmueble: '.$property->id.' - Evolución: '.$evolucion->id);
                }
            }

            if ($name == 'RepartirVentaInmueble') {
                logger('Passa venta');
                $data = unserialize($payload['data']['command']);
                $property = $data->getProperty();
                $evolucion = $data->getEvolucion();
                $procesoAbonarFinalizado = true;
                foreach ($property->usersGroup as $user) {
                    $movimientoVenta = $user->transactions()->where('type', 14)->where('extras', $property->id)->first();

                    if (!$movimientoVenta) {
                        $procesoAbonarFinalizado = false;
                    }
                }

                if ($procesoAbonarFinalizado) {
                    $evolucion->abonado = 1;
                    $evolucion->save();
                    Log::info('El proceso de pago de la venta del inmueble ha finalizado. Inmueble: '.$property->id.' - Evolución: '.$evolucion->id);
                }
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
