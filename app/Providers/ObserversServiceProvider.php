<?php

namespace App\Providers;

use App\Observers\PropertyObserver;
use App\Observers\TransactionObserver;
use App\Observers\UserObserver;
use App\Property;
use App\Transaction;
use App\User;
use Illuminate\Support\ServiceProvider;

class ObserversServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Property::observe(PropertyObserver::class);
        Transaction::observe(TransactionObserver::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
