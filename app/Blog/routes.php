<?php

Route::group(['prefix' => 'dashboard', 'middleware' => ['web', 'auth', 'role:super|admin'], 'namespace' => 'App\Blog\Controllers'], function () {

    // "admin/blog"                   => "admin/blog",
    //    "admin/blog/new"                => "admin/blog/nuevo",
    //    "admin/blog/store"              => "admin/blog/guardar",
    //    "admin/blog/edit"               => "admin/blog/editar/{id}",
    //    "admin/blog/update"             => "admin/blog/actualizar",
    //    "admin/blog/remove"             => "admin/blog/eliminar",
    //    "admin/blog/remove_force"       => "admin/blog/eliminar_completo",
    //    "admin/blog/trash"              => "admin/blog/papelera",
    //    "admin/blog/restore"            => "admin/blog/restaurar",
    //    "admin/blog/categories/store"   => "admin/blog/categorias/guardar",
    //    "admin/blog/categories/edit"    => "admin/blog/categorias/editar",
    //    "admin/blog/categories/update"  => "admin/blog/categorias/actualizar",
    //    "admin/blog/categories/remove"  => "admin/blog/categorias/eliminar",
    //HOTELS

    Route::get('blog', ['as' => 'dashboard.blog.index', 'uses' => 'PostController@index']);

    Route::get('blog/data_blog', ['as' => 'dashboard.blog,data', 'uses' => 'PostController@data_blog']);
    Route::get('blog/papelera', ['as' => 'dashboard.blog,trash', 'uses' => 'PostController@index(true)']);
    Route::get('blog/nuevo', ['as' => 'dashboard.blog.create', 'uses' => 'PostController@create']);
    Route::post('blog/guardar', ['as' => 'dashboard.blog.store', 'uses' => 'PostController@store']);
    Route::get('blog/editar/{id}', ['as' => 'dashboard.blog.edit', 'uses' => 'PostController@edit']);
    Route::put('blog/actualizar', ['as' => 'dashboard.blog.update', 'uses' => 'PostController@update']);
    Route::delete('blog/eliminar/{id}', ['as' => 'dashboard.blog.delete', 'uses' => 'PostController@destroy']);
    Route::delete('blog/eliminar_completo/{id}', ['as' => 'dashboard.blog.delete_full', 'uses' => 'PostController@forceDestroy']);
    Route::post('blog/restaurar', ['as' => 'dashboard.blog.restore', 'uses' => 'PostController@restore']);

    Route::post('blog/categorias/guardar', ['as' => 'dashboard.blog.category.store', 'uses' => 'CategoriesController@store']);
    Route::get('blog/categorias/editar', ['as' => 'dashboard.blog.category.edit', 'uses' => 'CategoriesController@edit']);
    Route::post('blog/categorias/actualizar', ['as' => 'dashboard.blog.category.update', 'uses' => 'CategoriesController@update']);
    Route::post('blog/categorias/eliminar', ['as' => 'dashboard.blog.category.delete', 'uses' => 'CategoriesController@destroy']);

    //IMAGE TEXT EDITOR
    Route::post('blog/upload_editor_image', 'PostController@upload_image_editor');
    Route::post('blog/remove_editor_image', 'PostController@remove_editor_images');
});

// Route::localizedGroup(function () {
//         Route::transGet('routes.menu.public_blog', 'PostController@index_front');

//         Route::transGet('routes.menu.public_blog_item', 'PostController@show')->where('slug', '[a-z0-9-]+');
//         Route::transGet('routes.menu.public_blog_category', 'PostController@showCategory')->where('slug', '[a-z0-9-]+');
//         Route::transGet('routes.menu.public_blog_tag', 'PostController@showTag')->where('slug', '[a-z0-9-]+');
// });
