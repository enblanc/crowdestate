<?php

namespace App\Blog\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    use Translatable;

    /**
     * @var string
     */
    protected $table = 'categories';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'name'];

    /**
     * @var array
     */
    protected $translatedAttributes = ['slug', 'name'];

    /**
     * Get the translations relation.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // public function translations(): HasMany
    // {
    //     return $this->hasMany(CategoriesTranslation::class);
    // }

    /**
     * Belongs to most posts.
     *
     * @return Eloquent
     */
    public function posts()
    {
        return $this->belongsToMany('App\Blog\Models\Post', 'post_blog_categories', 'categories_id', 'post_id');
    }

    /**
     * Belongs to most posts.
     *
     * @return Eloquent
     */
    public function active()
    {
        return $this->belongsToMany('App\Blog\Models\Post', 'post_blog_categories', 'categories_id', 'post_id')->where('state', 1);
    }

    /**
     * Find a post by translation slug.
     *
     * @param string $slug
     *
     * @return Eloquent Post
     */
    public static function findBySlug($slug)
    {
        $exist = self::whereHas('translations', function ($query) use ($slug) {
            $query->whereSlug($slug);
        })->first();

        return $exist;
    }
}
