<?php

namespace App\Blog\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comment';

    protected $fillable = ['content'];

    /**
     * Post belongs to User.
     *
     * @return Eloquent
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the comments for the post.
     *
     * @return Eloquent
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }
}
