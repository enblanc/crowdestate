<?php

namespace App\Blog\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class PostTranslation extends Model
{
    use Sluggable;

    public $timestamps = false;

    protected $fillable = ['slug', 'title', 'content'];

    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'titleSlug',
            ],
        ];
    }

    public function getTitleSlugAttribute()
    {
        if ($this->title) {
            return $this->title;
        }

        return $this->post->title;
    }
}
