<?php

namespace App\Blog\Models;

use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use Sluggable;

    protected $table = 'tag';

    protected $fillable = ['name'];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name',
            ],
        ];
    }

    /**
     * Belongs to most posts.
     *
     * @return Eloquent
     */
    public function post()
    {
        return $this->belongsToMany(Post::class, 'post_tag', 'post_id', 'tag_id');
    }

    /**
     * Belongs to most posts.
     *
     * @return Eloquent
     */
    public function posts_active()
    {
        return $this->belongsToMany(Post::class, 'post_tag', 'tag_id', 'post_id')->where('state', 1);
    }
}
