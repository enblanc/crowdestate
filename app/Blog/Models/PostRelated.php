<?php

namespace App\Blog\Models;

use Illuminate\Database\Eloquent\Model;

class PostRelated extends Model
{
    protected $table = 'post_related';

    protected $fillable = ['type', 'data', 'title', 'post_id'];

    /**
     * Belongs to one post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    public function getFileName()
    {
        return basename($this->data);
    }
}
