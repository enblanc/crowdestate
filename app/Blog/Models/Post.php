<?php

namespace App\Blog\Models;

use App\User;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Jenssegers\Date\Date;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Webpatser\Uuid\Uuid;

class Post extends Model implements HasMediaConversions
{
    use Translatable;
    use SoftDeletes;
    use HasMediaTrait;

    /**
     * @var string
     */
    protected $table = 'post';

    /**
     * @var array
     */
    protected $fillable = ['slug', 'title', 'content', 'featured_image', 'publish_date', 'state'];

    /**
     * @var array
     */
    protected $translatedAttributes = ['slug', 'title', 'content', 'meta_title', 'meta_description', 'meta_tags'];

    /**
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * @var string
     */
    protected $collection = 'post_feature';

    // public function translations()
    // {
    //     return $this->hasMany(PostTranslation::class);
    // }

    /**
     * Post belongs to User.
     *
     * @return Eloquent
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get the comments for the post.
     *
     * @return Eloquent
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Get all categories for the post.
     *
     * @return Eloquent
     */
    public function categories()
    {
        return $this->belongsToMany(Categories::class, 'post_blog_categories', 'post_id', 'categories_id');
    }

    /**
     * Get all tags for the post.
     *
     * @return Eloquent
     */
    public function tag()
    {
        return $this->belongsToMany(Tag::class, 'post_tag', 'post_id', 'tag_id');
    }

    /**
     * Get related items.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function related()
    {
        return $this->hasMany(PostRelated::class);
    }

    /**
     * Get Active posts. Need to set get() or other function after this.
     *
     * @return mixed
     */
    public static function getActive()
    {
        $today = date('Y-m-d');

        return self::where('state', 1)->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC');
    }

    /**
     * Get last posts.
     *
     * @param null $count
     *
     * @return mixed
     */
    public static function getActivePosts($count = null)
    {
        $today = date('Y-m-d');
        if ($count) {
            return self::where('state', 1)->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->take($count)->get();
        } else {
            return self::where('state', 1)->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->get();
        }
    }

    public function getExcerptAttribute()
    {
        return Str::words(strip_tags($this->content), 20);
    }

    /**
     * @return mixed
     */
    public function getImageThumbAttribute()
    {
        return $this->getFirstMediaUrl('post_feature', 'thumb');
    }

    /**
     * @return mixed
     */
    public function getImageFullAttribute()
    {
        return $this->getFirstMediaUrl('post_feature');
    }

    public function getFechaFormatAttribute()
    {
        $currentLocale = App::getLocale();
        $date = new Date();
        $date->setLocale($currentLocale);
        $date = $date->parse($this->publish_date);

        return ucfirst($date->format('d F Y'));
    }

    public function getUrlAttribute()
    {
        $route = 'news-post';
        $cat = $this->categories->first();
        if ($cat->name == 'Blog') {
            $route = 'blog-post';
        }

        return route('front.blog.show', $this->slug);
    }

    /**
     * Get meta desc
     *
     * @return string
     */
    public function getMetaDesc()
    {
        return str_limit($this->content, 155);
    }

    /**
     * Find a post by translation slug.
     *
     * @param string $slug
     *
     * @return Eloquent Post
     */
    public static function findBySlug($slug)
    {
        $exist = self::whereHas('translations', function ($query) use ($slug) {
            $query->whereSlug($slug);
        })->first();

        return $exist;
    }

    /**
     * Return mediaId encoded
     *
     * @return string
     */
    public function getMediaIdAttribute()
    {
        return Uuid::generate(5, $this->id, Uuid::NS_DNS);
    }

    /**
     * Redimensiona las fotos.
     */
    public function registerMediaConversions()
    {
        // Square size
        $this->addMediaConversion('square')
            ->setManipulations(['w' => 150, 'h' => 150, 'fit' => 'crop'])
            ->performOnCollections('post_feature', 'images_extra')
            ->nonQueued();

        // Thumb size
        $this->addMediaConversion('thumb')
            ->setManipulations(['w' => 550])
            ->performOnCollections('post_feature', 'images_extra')
            ->nonQueued();

        // Large size
        $this->addMediaConversion('large')
            ->setManipulations(['w' => 1024])
            ->performOnCollections('post_feature')
            ->nonQueued();
    }
}
