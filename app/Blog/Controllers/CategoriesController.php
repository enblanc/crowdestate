<?php

namespace App\Blog\Controllers;

use App\Blog\Models\Categories;
use App\Blog\Requests\CategoriesRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Infinety\CRUD\Models\Locale;

class CategoriesController extends Controller
{
    /**
     * @var App\Blog\Models\Categories
     */
    protected $categories;

    /**
     * Get Locale Model.
     *
     * @var App\Blog\Models\Post\Http\Locale
     */
    protected $locales;

    /**
     * Holds the post data.
     *
     * @var object
     */
    protected $data;

    /**
     * @param Categories $categories
     * @param Locale $locales
     */
    public function __construct(Categories $categories, Locale $locales)
    {
        $this->middleware('auth');
        //$this->middleware('admin');

        $this->categories = $categories;
        $this->locales = $locales;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(CategoriesRequest $request)
    {
        $this->data = $request->except([
            '_token',
        ]);

        $category = $this->storeOrUpdateCategory();
        if ($category != false) {
            return json_encode($category);
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        };
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Response
     */
    public function edit(Request $request)
    {
        $this->data = $request->except([
            '_token',
        ]);
        $category = Categories::findOrFail($this->data['id']);
        if ($category) {
            $languages = Locale::all();
            $datos = array();
            $datos['id'] = $category->id;
            foreach ($languages as $lang) {
                if ($category->translate($lang->iso)) {
                    $datos['languages'][$lang->iso]['name'] = $category->translate($lang->iso)->name;
                }
            }

            return $datos;
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(CategoriesRequest $request)
    {
        $this->data = $request->except([
            '_token',
        ]);

        $category = $this->storeOrUpdateCategory();
        if ($category != false) {
            return json_encode($category);
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        };
    }

    /**
     * @return mixed
     */
    private function storeOrUpdateCategory()
    {
        if (!empty($this->data['catId'])) {
            $category = $this->categories->find($this->data['catId']);
        } else {
            $category = new Categories();
        }
        if (isset($this->data['color'])) {
            $category->color = $this->data['color'];
        }
        $category->save();

        $languages = Locale::all();

        foreach ($languages as $lang) {
            $category->translateOrNew($lang->iso)->name = $this->data['name_'.$lang->iso];
        }

        if ($category->save()) {
            return $category;
        } else {
            return false;
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy(Request $request)
    {
        $this->data = $request->except([
            '_token',
        ]);

        $category = $this->categories->findOrFail($this->data['id']);
        if ($category->delete()) {
            return json_encode($category->id);
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        }
    }
}
