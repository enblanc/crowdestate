<?php

namespace App\Blog\Controllers;

use App\Blog\Models\Categories;
use App\Blog\Models\Post;
use App\Blog\Models\PostRelated;
use App\Blog\Models\Tag;
use App\Blog\Requests\PostRequest;
use App\Blog\Requests\PostUpdateRequest;
use App\Http\Controllers\Controller;
use App\Http\CustomPagination;
use App\User;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Image;
use Infinety\CRUD\Models\Locale;
use Jenssegers\Date\Date as Date;
use Lang;
use Response;
use SEO;
use Session;
use Spatie\MediaLibrary\Media;

class PostController extends Controller
{
    /**
     * @var starter\Blog\Models\Post
     */
    protected $post;

    /**
     * @var starter\Users\Models\User
     */
    protected $user;

    /**
     * @var starter\Blog\Models\Categories
     */
    protected $categories;

    /**
     * @var starter\Blog\Models\Tag
     */
    protected $tags;

    /**
     * Get Locale Model.
     *
     * @var starter\Http\Locale
     */
    protected $locales;

    /**
     * Holds the image file.
     *
     * @var object
     */
    protected $image;

    /**
     * Holds the post data.
     *
     * @var object
     */
    protected $data;

    /**
     * Holds the Data Request Type.
     *
     * @var [type]
     */
    protected $type;

    /**
     * @param Post $post
     * @param User $user
     * @param Tag $tags
     * @param Categories $categories
     * @param Locale $locales
     */
    public function __construct(
        Post $post,
        User $user,
        Tag $tags,
        Categories $categories,
        Locale $locales
    ) {

        //$this->middleware('auth');
        //$this->middleware('admin');

        $this->post = $post;
        $this->user = $user;
        $this->categories = $categories;
        $this->tags = $tags;
        $this->locales = $locales;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($trashed = false)
    {
        $all = $this->post->count();
        $published = $this->post->where('state', 1)->count();
        $trash = $this->post->onlyTrashed()->count();

        return $this->firstViewThatExists('dashboard/blog/index', 'blog::admin.index', compact('all', 'published', 'trash'));
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function index_front(Request $request)
    {
        SEO::setTitle('Blog');
        SEO::setDescription('Blog de Brickstarter');
        $today = date('Y-m-d');
        $posts = $this->post->where('state', 1)
            ->where('publish_date', '<=', $today)
            ->orderby('publish_date', 'DESC')
            ->orderby('created_at', 'DESC')
            ->paginate(5, ['*'], 'pagina');

        $categories = $this->categories->has('active')->get();
        if ($request->ajax()) {
            $date = new Date();
            $currentLocale = App::getLocale();
            $date->setLocale($currentLocale);

            foreach ($posts as $post) {
                $post->translate();
                // $post->color = $post->categories->first()->color;
                // $post->cat = $post->categories->first()->name;
                $post->date = $date->parse($new->publish_date)->format('j \d\e F \d\e Y');
            }

            return response()->json($posts);
        }
        $total = $this->post->where('state', 1)->where('publish_date', '<=', $today)->count();

        $page = 'home';

        return $this->firstViewThatExists('front/blog/archive', 'blog::front.home', compact('posts', 'total', 'categories', 'page'));
    }

    /**
     * Devuelve las noticias antiguas.
     *
     * @param Request $request
     *
     * @return HTTP
     */
    public function old_posts(Request $request)
    {
        SEO::setTitle('Blog');
        SEO::setDescription('Noticias antiguas');
        $today = date('Y-m-d');
        $firstPosts = $this->post->where('state', 1)->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->take(5)->lists('id');

        $posts = $this->post->where('state', 1)->whereNotIn('id', $firstPosts->toArray())->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->paginate(5);

        $presenter = new CustomPagination($posts);

        return $this->firstViewThatExists('front/comunicacion/blog/blog_old', 'blog::front.home', compact('posts', 'presenter'));
    }

    /**
     * Get te list of post in Datatable view.
     *
     * @return {Datatable}
     */
    public function data_blog(Request $request)
    {
        $datos = $request->except([
            '_token',
        ]);

        switch ($datos['type']) {
            case 'all':
                $data = $this->post->select(['id', 'featured_image', 'publish_date', 'state'])->get();
                break;
            case 'published':
                $data = $this->post->select(['id', 'featured_image', 'publish_date', 'state'])->where('state', 1)->get();
                break;
            case 'trash':
                $data = $this->post->select(['id', 'featured_image', 'publish_date', 'state'])->onlyTrashed()->get();

                break;
            default:
                $data = $this->post->select(['id', 'featured_image', 'publish_date', 'state'])->get();
                break;
        }

        $this->type = $datos['type'];

        return Datatables::of($data)
            ->editColumn('photo', function ($info) {
                $image = asset('assets/img/no_image.png');

                if ($info->getMedia('post_feature')->first()) {
                    $image = asset($info->getMedia('post_feature')->first()->getUrl('thumb'));
                }

                return '<img data-src="'.$image.'" class="img-thumbnail lazy loading-gif" alt="'.$info->title.'">';
            })
            ->editColumn('title', function ($info) {
                $title = $info->title;

                return $title;
            })
            ->editColumn('content', function ($info) {
                $description = $info->content;
                $description = strip_tags(preg_replace("/<img[^>]+\>/i", '', $description));

                return $this->trim_text($description, 120, true, true);
            })
            ->editColumn('publish_date', function ($info) {
                return "<div class='text-center'>".$info->publish_date.'</div>';
            })
            ->editColumn('state', function ($info) {
                if ($info->state == 0) {
                    return '<span class="label label-default">Borrador</span>';
                } else {
                    return '<span class="label label-primary">Publicada</span>';
                }
            })
            ->addColumn('opciones', '')
            ->editColumn('opciones', function ($info) {
                if ($this->type == 'trash') {
                    $options = "<button type='button' class='btn btn-sm btn-primary restorePost' data-id='".$info->id."' >Restaurar</button>";
                    $options .= "<button type='button' class='btn btn-sm btn-danger ml5 removeForcePost' data-button-type='forceDelete' data-id='".$info->id."' href='".route('dashboard.blog.delete_full', $info->id)."'>Eliminar</button>";
                } else {
                    $options = "<a type='button' href='".route('dashboard.blog.edit', $info->id)."' class='btn btn-sm btn-info get-info'>Editar</a>";
                    $options .= "<button type='button' class='btn btn-sm btn-danger ml5 removePost' data-button-type='delete' data-id='".$info->id."' href='".route('dashboard.blog.delete', $info->id)."'>Eliminar</button>";
                }

                return $options;
            })
            ->setRowId('id')
            ->rawColumns(['photo', 'publish_date', 'type', 'state', 'opciones'])
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $locales = Locale::getAvailables();

        return $this->firstViewThatExists('dashboard/blog/create', 'blog::admin.create', ['categories' => $this->categories->all(), 'locales' => $locales]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(PostRequest $request)
    {
        $this->data = $request->except([
            '_token',
        ]);

        $this->image = $this->data['featured_image'];
        // $this->tags = $this->storeOrUpdateTags();
        $this->categories = $this->data['categories'];
        $post = $this->storeOrUpdatePost();

        if ($post != false) {
            Session::flash('success', Lang::get('blog::blog.database.post_saved'));

            return redirect()->route('dashboard.blog.index');
        } else {
            Session::flash('error', Lang::get('blog::blog.database.error'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($slug, Request $request)
    {
        $post = $this->post->findBySlug($slug);

        if (!$post) {
            return redirect()->route('front.blog.index');
        }
        $currentLocale = App::getLocale();
        $date = new Date();
        $date->setLocale($currentLocale);
        $date = $date->parse($post->publish_date);

        $previousId = $post->where('id', '<', $post->id)->max('id');
        $previousPost = $post->where('id', $previousId)->first();
        $nextID = $post->where('id', '>', $post->id)->min('id');
        $nextPost = $post->where('id', $nextID)->first();

        $category = $post->categories->first();
        $today = date('Y-m-d');
        $related = $category->posts()->where('id', '!=', $post->id)->where('state', 1)->where('publish_date', '<=', $today)->get()->take(3);

        $categories = $this->categories->has('posts')->get();

        SEO::setTitle($post->title);

        if ($post->meta_title) {
            SEO::metatags()->addMeta('title', $post->meta_title);
        }

        if ($post->meta_description) {
            SEO::setDescription($post->meta_description);
        } else {
            SEO::setDescription($post->getMetaDesc());
        }

        if ($post->meta_tags) {
            SEO::metatags()->addKeyword(explode(',', $post->meta_tags));
        }

        SEO::metatags()->addMeta('article:published_time', $date->parse($post->publish_date)->toW3CString(), 'property');
        SEO::metatags()->addMeta('article:section', $post->categories->first()->name, 'property');
        SEO::opengraph()->addProperty('locale', $currentLocale);
        SEO::opengraph()->setUrl($request->url());
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::opengraph()->addImage(asset($post->getFirstMediaUrl('post_feature')));

        $page = 'single';

        return $this->firstViewThatExists('front/blog/single', 'blog::front.single', compact('post', 'date', 'categories', 'previousPost', 'nextPost', 'related', 'page'));
    }

    /**
     * Display the posts from selected category.
     *
     * @param int $id
     *
     * @return Response
     */
    public function showCategory($slug, Request $request)
    {
        $cat = $this->categories->findBySlug($slug);
        if (!$cat) {
            return redirect()->route('front.blog.index');
        }
        $today = date('Y-m-d');
        $posts = $cat->active()->where('publish_date', '<=', $today)->orderby('publish_date', 'DESC')->orderby('created_at', 'DESC')->paginate(6);

        $categories = $this->categories->has('posts')->get();
        if ($request->ajax()) {
            $date = new Date();
            $currentLocale = localization()->getCurrentLocale();
            $date->setLocale($currentLocale);

            foreach ($posts as $post) {
                $post->translate();
                $post->color = $post->categories->first()->color;
                $post->cat = $post->categories->first()->name;
                $post->date = $date->parse($post->publish_date)->format('d / m / Y');
                $post->image = asset($post->getFeaturedImage());
            }

            return response()->json($posts);
        }

        $page = $cat->id;

        return $this->firstViewThatExists('front/blog/archive', 'blog::front.home', compact('trans', 'posts', 'categories', 'page'));
    }

    /**
     * Display the posts from selected tag.
     *
     * @param int $id
     *
     * @return Response
     */
    public function showTag($slug, Request $request)
    {
        $trans = $this->tags->findBySlug($slug);
        if (!$trans) {
            return redirect()->to(LaravelLocalization::transRoute('routes.public_blog'));
        }
        $tag = $this->tags->find($trans->id);
        $posts = $tag->active()->orderBy('featured', 'DESC')->orderBy('publish_date', 'DESC')->orderby('created_at', 'DESC')->paginate(7);

        $categories = $this->categories->get();
        if ($request->ajax()) {
            $date = new Date();
            $currentLocale = localization()->getCurrentLocale();
            $date->setLocale($currentLocale);

            foreach ($posts as $post) {
                $post->translate();
                $post->color = $post->categories->first()->color;
                $post->cat = $post->categories->first()->name;
                $post->date = $date->parse($post->publish_date)->format('d / m / Y');
                $post->image = asset($post->getFeaturedImage());
            }

            return response()->json($posts);
        }

        return $this->firstViewThatExists('front/blog/category', 'blog::front.home', compact('trans', 'posts', 'categories'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $post = $this->post->find($id);
        if ($post) {
            $locales = Locale::getAvailables();

            return $this->firstViewThatExists('dashboard/blog/edit', 'blog::admin.edit', ['post' => $post, 'categories' => $this->categories->all(), 'locales' => $locales]);
        } else {
            Session::flash('error', Lang::get('blog::blog.database.error'));

            return redirect()->back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function update(PostUpdateRequest $request)
    {
        $this->data = $request->except([
            '_token',
        ]);

        $this->image = $this->data['featured_image'];
        // $this->tags = $this->storeOrUpdateTags();
        $this->categories = $this->data['categories'];
        $post = $this->storeOrUpdatePost();
        if ($post != false) {
            Session::flash('success', trans('blog::blog.database.post_updated'));

            return redirect()->route('dashboard.blog.index');
        } else {
            Session::flash('error', trans('blog::blog.database.error'));

            return redirect()->back()->withInput();
        }
    }

    /**
     * Save new post or update current.
     *
     * @return bool or Post->Id
     */
    private function storeOrUpdatePost()
    {
        $upload_image = true;

        if (!empty($this->data['postId'])) {
            $post = $this->post->find($this->data['postId']);

            if ($this->data['image_changed'] == 0) {
                $upload_image = false;
            }
        } else {
            $post = new Post();
        }
        $languages = $this->locales->all();

        $correctDate = str_replace('/', '-', $this->data['publish_date']);

        $post->publish_date = date('Y-m-d', strtotime($correctDate));
        $post->state = $this->data['state'];
        $post->save();

        foreach ($languages as $key => $language) {
            $post->translateOrNew($language->iso)->title = $this->data['title-'.$language->iso];
            $post->translateOrNew($language->iso)->content = $this->data['content-'.$language->iso];
            if (!empty($this->data['postId'])) {
                $post->translateOrNew($language->iso)->slug = $this->data['slug-'.$language->iso];
            }
            $post->translateOrNew($language->iso)->meta_title = $this->data['meta-title-'.$language->iso];
            $post->translateOrNew($language->iso)->meta_description = $this->data['meta-description-'.$language->iso];
            $post->translateOrNew($language->iso)->meta_tags = $this->data['tags-'.$language->iso];
        }

        if ($upload_image) {
            $originalName = basename($this->data['featured_image']);
            $extension = pathinfo($this->data['featured_image'], PATHINFO_EXTENSION);
            $withoutExt = preg_replace('/\\.[^.\\s]{3,4}$/', '', $originalName);
            $newName = $this->sanitize($withoutExt).'.'.$extension;
            $post->featured_image = $newName;
        }

        if ($post->save()) {
            if ($upload_image) {
                $post->clearMediaCollection('post_feature');
                $post->addMedia($this->data['featured_image'])
                    ->usingFileName($newName)
                    ->preservingOriginal()
                    ->toCollection('post_feature');
            }

            // if (count($this->tags) > 0) {
            //     $post->tag()->sync($this->tags);
            // }
            $post->categories()->sync($this->categories);

            if (count($post->related) > 0) {
                $post->related()->delete();
            }

            if (isset($this->data['related']) && count($this->data['related']) > 0) {
                foreach ($this->data['related'] as $lang => $relatedArray) {
                    $count = 1;
                    foreach ($relatedArray as $key => $relatedValues) {
                        $related = new PostRelated();
                        $related->type = 1;
                        $related->data = $relatedValues['val'];

                        $related->post_id = $post->id;
                        $related->locale_id = $lang;
                        $related->position = $count;
                        $related->save();
                        ++$count;
                    }
                }
            }

            // Images Extras
            if (isset($this->data['extraimages']) && count($this->data['extraimages']) > 0) {
                $imagesExtra = $post->getMedia('images_extra');
                $extrasFromView = $this->data['extraimages'];
                foreach ($imagesExtra as $extra) {
                    $imageThumb = $extra->getUrl('thumb');
                    $existe = array_search($imageThumb, $extrasFromView);
                    if ($existe != false) {
                        unset($extrasFromView[$existe]);
                    } else {
                        $extraMedia = Media::find($extra->id);
                        $extraMedia->delete();
                    }
                }

                if (count($extrasFromView) > 0) {
                    foreach ($extrasFromView as $imageExtra) {
                        if (!empty($imageExtra)) {
                            $post->addMedia($imageExtra)
                                ->preservingOriginal()
                                ->toCollection('images_extra');
                        }
                    }
                }
            } else {
                $post->clearMediaCollection('images_extra');
            }

            return $post;
        } else {
            return false;
        }
    }

    /**
     * Saves or updates tags.
     *
     * @return array
     */
    private function storeOrUpdateTags()
    {
        $tagArray = array();
        $tags = explode(',', $this->data['tags']);
        if (count($tags) > 0) {
            foreach ($tags as $addtag) {
                if (!empty($addtag)) {
                    $exists = Tag::where('name', '=', $addtag)->first();
                    if (count($exists) > 0) {
                        $newTag = $exists;
                    } else {
                        $newTag = new Tag();
                    }
                    $newTag->name = $addtag;
                    $newTag->save();
                    array_push($tagArray, $newTag->id);
                }
            }

            return $tagArray;
        }
    }

    /**
     * Function to handle image editor uploads.
     *
     * @param Request $request file
     *
     * @return JSON
     */
    public function upload_image_editor(Request $request)
    {
        $this->image = $request->file('file');
        $this->data['publish_date'] = date('Y-m-d H:i:s');
        $file = $this->uploadImage(true);
        $folder = date('m-Y', strtotime($this->data['publish_date']));
        $bigFolder = config('blog.big_folder');
        $destination = url().'/blog_assets/uploads/'.$folder.'/'.$bigFolder.'/';

        return json_encode(['filelink' => $destination.$file, 'name' => $file, 'folder' => $folder]);
    }

    /**
     * Uploads Images to folders with
     * year and month folders.
     *
     * @param bool $redimension
     *
     * @return string Name of file
     */
    private function uploadImage($redimension = true)
    {
        $file = $this->image;
        if (!isset($this->image)) {
            return redirect()->back()->withInput();
        }
        $filename = pathinfo(str_replace(' ', '_', $file->getClientOriginalName()), PATHINFO_FILENAME).'_'.str_random(5);
        $filenameWithExt = $filename.'.'.$file->getClientOriginalExtension();

        $correctDate = str_replace('/', '-', $this->data['publish_date']);

        $destination_original = public_path().'/blog_assets/uploads/original/';
        $destination = public_path().'/blog_assets/uploads/'.date('m-Y', strtotime($correctDate)).'/';

        //dd($destination);
        if (!file_exists($destination_original)) {
            mkdir($destination_original, 0777, true);
        }
        if (!file_exists($destination)) {
            mkdir($destination, 0777, true);
        }

        $file->move($destination_original, $filenameWithExt);

        if ($redimension) {
            $img = Image::make($destination_original.$filenameWithExt);
            $dimensions = $img->width().'x'.$img->height();
            $img->backup();
            $formats = app('config')->get('blog.image_formats');
            $imagen_backup = false;
            foreach ($formats as $name => $format) {
                if (!$imagen_backup) {
                    $new_image = $img->reset();
                } else {
                    $new_image = $imagen_backup->reset();
                }
                if (!file_exists($destination.$name.'/')) {
                    mkdir($destination.$name.'/', 0777, true);
                }

                $new_image = $img->reset();
                switch ($format['a']) {
                    case 0:
                        $new_image->resize(
                            $format['w'],
                            $format['h'],
                            function ($constraint) {
                                $constraint->aspectRatio();
                            }
                        );
                        break;
                    case 1:
                        $new_image->fit($format['w'], $format['h']);
                        break;
                }
                $new_image->save($destination.$name.'/'.$filenameWithExt);
                if ($name == '1920') {
                    $imagen_backup = $new_image->backup();
                }
            }
            $img->destroy();
            //borramos la imagen original
            @unlink($destination_original.$filenameWithExt);
        }

        return $filenameWithExt;
    }

    /**
     * Remove Featured Image.
     *
     * @param var $image image file
     *
     * @return bool
     */
    private function removeImages($image)
    {
        if (strpos($image, 'http') !== false) {
            return true;
        }

        $correctDate = str_replace('/', '-', $this->data['publish_date']);
        $time_folder = date('m-Y', strtotime($correctDate));
        $formats = app('config')->get('blog.image_formats');
        $destination = public_path().'/blog_assets/uploads/'.$time_folder.'/';

        $error = false;
        foreach ($formats as $name => $format) {
            @unlink($destination.$name.'/'.$image);
        }
        @unlink($destination.$image);

        return true;
    }

    /**
     * Function to handle image editor uploads.
     *
     * @param Request $request file
     *
     * @return JSON
     */
    public function upload_image_editor_old(Request $request)
    {
        $this->image = $request->file('file');
        $this->data['publish_date'] = date('Y-m-d H:i:s');
        $file = $this->uploadImage(false);
        $folder = date('m-Y', strtotime($this->data['publish_date']));
        $destination = url().'/blog_assets/uploads/'.$folder.'/';

        return json_encode(['filelink' => $destination.$file, 'name' => $file, 'folder' => $folder]);

        return false;
    }

    /**
     * Remove editor image from folder.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function remove_editor_images(Request $request)
    {
        $image = $request['name'];
        $folder = $request['folder'];

        $formats = config('blog.image_formats');
        $destination = public_path().'/blog_assets/uploads/'.$folder.'/';

        foreach ($formats as $name => $format) {
            @unlink($destination.$name.'/'.$image);
        }

        //Borramos la original si se encuentra
        @unlink(public_path().'/blog_assets/uploads/original/'.$name);

        return json_encode(true);
    }

    /**
     * trims text to a space then adds ellipses if desired.
     *
     * @param string $input      text to trim
     * @param int    $length     in characters to trim to
     * @param bool   $ellipses   if ellipses (...) are to be added
     * @param bool   $strip_html if html tags are to be stripped
     *
     * @return string
     */
    public function trim_text($input, $length, $ellipses = true, $strip_html = true)
    {
        //strip tags, if desired
        if ($strip_html) {
            $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
            return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
            $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

    /**
     * @param $string
     * @param bool $force_lowercase
     * @param bool $anal
     *
     * @return bool|mixed|string
     */
    public function sanitize($string, $force_lowercase = true, $anal = true)
    {
        $strip = array('~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '_', '=', '+', '[', '{', ']',
            '}', '\\', '|', ';', ':', '"', "'", '&#8216;', '&#8217;', '&#8220;', '&#8221;', '&#8211;', '&#8212;',
            'â€”', 'â€“', ',', '<', '.', '>', '/', '?');
        $clean = trim(str_replace($strip, '-', strip_tags($string)));
        $clean = preg_replace('/\s+/', '-', $clean);
        $clean = ($anal) ? preg_replace('/[^a-zA-Z0-9]/', '-', $clean) : $clean;

        return ($force_lowercase) ?
        (function_exists('mb_strtolower')) ?
        mb_strtolower($clean, 'UTF-8') :
        strtolower($clean) :
        $clean;
    }

    /**
     * Restore posts.
     *
     * @param int $id
     *
     * @return Response
     */
    public function restore(Request $request)
    {
        $this->data = $request->except([
            '_token',
        ]);

        $post = $this->post->withTrashed()->find($this->data['id']);
        if ($post->restore()) {
            Session::flash('message', Lang::get('blog::blog.database.post_restored'));

            return json_encode(true);
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        }
    }

    /**
     * Send post to trash.
     *
     * @param int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $post = $this->post->find($id);
        if ($post->deletePreservingMedia()) {
            Session::flash('message', Lang::get('blog::blog.database.post_trash'));

            return json_encode(true);
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return Response
     */
    public function forceDestroy($id)
    {
        $post = $this->post->withTrashed()->find($id);
        // $image = $post->featured_image;

        if ($post) {
            // $this->removeImages($image);
            $post->forceDelete();
            Session::flash('message', Lang::get('blog::blog.database.post_trash'));

            return json_encode(true);
        } else {
            header('HTTP/1.1 503 Service Unavailable');
        }
    }

    /**
     * Allow replace the default views by placing a view with the same name in
     * /resources/views/vendor/dick/crud/. If no such view exists, load the one from the package.
     *
     * @param view  $first_view  - the first view to try, ex: vendor.dick.crud.edit
     * @param view  $second_view - the second view to try, ex: crud::edit
     * @param array $information - the information to send to the view, usually $this->data
     *
     * @return HTTP Response
     */
    protected function firstViewThatExists($first_view, $second_view, $information)
    {

        // load the first view if it exists, otherwise load the second one
        if (view()->exists($first_view)) {
            return view($first_view, $information);
        } else {
            return view($second_view, $information);
        }
    }
}
