<?php

namespace App\Blog\Requests;

use App\Blog\Models\Post;
use Illuminate\Foundation\Http\FormRequest;

class PostUpdateRequest extends FormRequest
{
    /**
     * @param App\Blog\Models\Post $post
     */
    protected $post;

    /**
     * @param App\Blog\Models\Post $post
     */
    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'categories'    => 'required',
            'state'         => 'required',
            'publish_date'  => 'required|date_format:d/m/Y',
            'image_changed' => 'required',
        ];

        return $rules;
    }
}
