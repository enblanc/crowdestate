<?php

namespace App\Blog\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Blog\Models\Tag;

class TagRequest extends FormRequest
{
    /**
     * @param App\Blog\Models\Tag $tag
     */
    protected $tag;

    /**
     * @param App\Blog\Models\Tag $tag
     */
    public function __construct(Tag $tag)
    {
        $this->tag = $tag;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
        ];
    }
}
