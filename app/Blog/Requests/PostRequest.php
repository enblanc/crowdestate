<?php

namespace App\Blog\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class PostRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'categories'     => 'required',
            'state'          => 'required',
            'publish_date'   => 'required|date_format:d/m/Y',
            'featured_image' => 'required',
        ];

    }

    public function messages()
    {
        return [
            'featured_image.required' => 'La imagen principal es obligatoria',
            'categories.required'     => 'Es obligatorio elegir una categoría',
        ];
    }
}
