<?php

namespace App\Blog\Requests;

use App\Blog\Models\Categories;
use Illuminate\Foundation\Http\FormRequest;
use Infinety\CRUD\Models\Locale;

class CategoriesRequest extends FormRequest
{
    /**
     * @param App\Blog\Models\Categories $tag
     */
    protected $category;

    /**
     * @param App\Blog\Models\Categories $tag
     */
    public function __construct(Categories $category)
    {
        $this->category = $category;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        $languages = Locale::getAvailables();
        foreach ($languages as $lang) {
            $rules['name_'.$lang->iso] = 'required';
        }

        return $rules;
    }
}
