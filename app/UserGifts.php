<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserGifts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'transaction_id',
        'importe',
        'executed',
    ];

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
