<?php

namespace App\Validation;

use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SheetExistsValidator
{

    protected $allowedExtensions = ['xls', 'xlsx'];

    /**
     * Validate whether the given username is allowed.
     *
     * @param  string  $attribute
     * @param  string  $username
     * @return bool
     */
    public function validate($sheetName, $file)
    {
        if (!$sheetName) {
            return false;
        }
         
        if (! $this->checkExtension($file)) {
            return false;
        }
        
        if (! $this->checkSheetExists($file, $sheetName)) {
            return false;
        }
        
        return true;
    }


    private function checkExtension($file)
    {
        if ($file instanceof UploadedFile) {
            $extension = pathinfo($file->getClientOriginalName(), PATHINFO_EXTENSION);
        } else {
            $extension =  pathinfo($file, PATHINFO_EXTENSION);
        }
        
        if (in_array($extension, $this->allowedExtensions)) {
            return true;
        }

        return false;
    }


    /**
     * Check if sheet name exists in given excel file
     *
     * @param  Symfony\Component\HttpFoundation\File\UploadedFile $file
     * @param  string $sheetName
     *
     * @return bool
     */
    private function checkSheetExists($file, $sheetName)
    {
        $excelReader = Excel::load($file);

        if ($excelReader->excel->getSheetByName($sheetName) !== null) {
            return true;
        }
        
        return false;
    }
}
