<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvestmentTranslation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['investment_id', 'nombre', 'locale'];
}
