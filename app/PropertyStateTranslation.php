<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyStateTranslation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['property_state_id', 'nombre', 'locale'];
}
