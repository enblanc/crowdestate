<?php

namespace App;

use App\Property;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Infinety\CRUD\CrudTrait;

class PropertyState extends Model
{
    use Translatable, CrudTrait;

    /**
     * @var array
     */
    protected $fillable = ['property_state_id', 'nombre', 'locale'];

    /**
     * @var array
     */
    public $translatedAttributes = ['nombre'];

    /**
     * Get properties of a investment
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function properties(): HasMany
    {
        return $this->hasMany(Property::class);
    }

    //ESTADOS
    // 1 - En estudio
    // 2 - En adquisición
    // 3 - En Obras
    // 4 - En Explotación
    // 5 - Vendido
}
