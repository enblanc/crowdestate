<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PropertyTypeTranslation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['property_type_id', 'nombre', 'locale'];
}
