<?php

namespace App\Events;

use GuzzleHttp\Client;
use Psy\ExecutionLoop\Loop;

class UserInvierteClickId
{

    protected $user;

    protected $transaction;

    /**
     * Providers
     *
     * @var array
     */
    protected $providers;
    

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $transaction)
    {
        
        $this->user = $user;

        $this->transaction = $transaction;

        $this->providers = config('clickid.providers');

        $this->emit();
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function emit()
    {
        try {
            if ($clickProvider = $this->user->clickProvider) {
                logger($clickProvider->click_id);
                $currentProvider = $this->providers[$clickProvider->provider];
                $endpoints = $currentProvider['endpoint'];
                $days_between = $this->getDaysBetweenTwoDates($clickProvider->created_at);
                
                $first_transaction = \App\ClickidsLog::where('user_id', $clickProvider->user_id)
                                                      ->where('click_id', $clickProvider->click_id)
                                                      ->where('message', 'FIRST-TRANSACTION')
                                                      ->get();

                if (!count($first_transaction)) {
                    if (isset($endpoints['first_payment'])) {
                        $endpoint = str_replace(['click_id', 'mount', 'user_id'], [
                            $clickProvider->click_id,
                            $this->transaction->debit,
                            $clickProvider->user_id
                        ], $endpoints['first_payment']);
                        
                        $this->emitCallBack($endpoint, 'FIRST-TRANSACTION');
                    }
                    
                    if (isset($endpoints['sale'])) {
                        $endpoint = str_replace(['click_id', 'order_id'], [
                            $clickProvider->click_id,
                            $this->transaction->id
                        ], $endpoints['sale']);
                        
                        $this->emitCallBack($endpoint, 'FIRST-TRANSACTION');
                    }
                }

                if (isset($endpoints['saleOff']) && $days_between <= 90) {
                    $sale_name = 'sale90';

                    $credit = $this->transaction->debit;

                    $gifs = \App\UserGifts::where('user_id', $this->user->id)->where('transaction_id', $this->transaction->id)->sum('importe');
                    
                    $endpoint = str_replace(['click_id', 'order_id', 'mount', 'sale_off'], [
                        $clickProvider->click_id,
                        $this->transaction->id,
                        $credit - $gifs,
                        $sale_name
                    ], $endpoints['saleOff']);
                    
                    $this->emitCallBack($endpoint, 'TRANSACTION:90DAYS');
                }
            }
        } catch (\Throwable $th) {
            $this->createLog([
                'user_id'   =>  $this->user->id,
                'click_id'  =>  $this->user->clickProvider->click_id,
                'provider'  =>  $this->user->clickProvider->provider,
                'postback'  =>  null,
                'message'   =>  $th->getMessage()
            ], $th);
        }
    }

    /**
     * Hace un llamado tipo GET a la URL del proveedor
     *
     * @param   string  $endpoint
     *
     * @return  void
     */
    protected function emitCallBack ($endpoint, $message = null)
    {     
        try {
            $client = new Client();
            $res = $client->request('GET', $endpoint, ['http_errors' => false]);
            $this->createLog([
                'user_id'   =>  $this->user->id,
                'click_id'  =>  $this->user->clickProvider->click_id,
                'provider'  =>  $this->user->clickProvider->provider,
                'postback'  =>  $endpoint,
                'status'    =>  1,
                'message'   =>  $message ? $message : 'TRANSACTION'
            ], 'CLICK_ID:TRANSACTION:SUCCESS:'.$endpoint);

        } catch (\Exception $e) {
            $this->createLog([
                'user_id'   =>  $this->user->id,
                'click_id'  =>  $this->user->clickProvider->click_id,
                'provider'  =>  $this->user->clickProvider->provider,
                'postback'  =>  $endpoint,
                'message'   =>  $e->getMessage()
            ], $e);
        }
    }

    /**
     * Retorna los dias entre dos fechas
     *
     * @param   datatime  $created_at 
     *
     * @return  integer
     */
    protected function getDaysBetweenTwoDates ($created_at)
    {
        $datetime1 = new \DateTime($created_at);

        $datetime2 = new \DateTime();

        $interval = $datetime1->diff($datetime2);

        return (integer)$interval->format('%a');
    }

    /**
     * Recibe un array
     *
     * @param   array  $log 
     *
     * @return  void       
     */
    private function createLog ($log, $logger = null)
    {
        if ($logger) {
            logger($logger);
        }

        try {
            \App\ClickidsLog::create($log);
        } catch (\Throwable $th) {
            logger($th);
        }
    }
}
