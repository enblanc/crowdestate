<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class KycDocumentStatus extends Model
{

    /**
     * @var array
     */
    protected $fillable = [
        'lemonway_id',
        'document_id',
        'type',
        'status',
        'comment',
    ];

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'lemonway_id', 'lemonway_id');
    }

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class, 'document_id');
    }

    public function getLabelStatusAttribute ()
    {
        switch ($this->status) {
            case 0 : {
                return __('emails.account_document_fail.status_0_doc_put_on_hold');
                break;
            }
            case 1 : {
                return __('emails.account_document_fail.status_1_received_default_status');
                break;
            }
            case 2 : {
                return __('emails.account_document_fail.status_2_accepted');
                break;
            }
            case 3 : {
                return __('emails.account_document_fail.status_3_rejected');
                break;
            }
            case 4 : {
                return __('emails.account_document_fail.status_4_rejected_doc_unreadable');
                break;
            }
            case 5 : {
                return __('emails.account_document_fail.status_5_rejected_doc_expired');
                break;
            }
            case 6 : {
                return __('emails.account_document_fail.status_6_rejected_wrong_type');
                break;
            }
            case 7 : {
                return __('emails.account_document_fail.status_7_rejected_wrong_name');
                break;
            }
            case 8 : {
                return __('emails.account_document_fail.status_8_rejected_duplicate_document');
                break;
            } default : {
                return 'N/D';
            }
        }
    }

    public function getLabelTypeAttribute ()
    {
        switch ($this->type) {
            case 0 : {
                return "Identity Card (both sides in one file)";
                break;
            } case 1 : {
                return "Proof of address";
                break;
            } case 2 : {
                return "Proof of Bank Information (IBAN or other)";
                break;
            } case 3 : {
                return "Passport (European Community)";
                break;
            } case 4 : {
                return "Passport (outside the European Community) ";
                break;
            } case 5 : {
                return "Residence permit (both sides in one file)";
                break;
            } case 7 : {
                return "Official company registration document (Kbis extract or equivalent)";
                break;
            } case 11 : {
                return "Driving licence (both sides in one file)";
                break;
            } case 12 : {
                return "Status";
                break;
            } case 13 : {
                return "Selfie";
                break;
            } case 21 : {
                return "SDD mandate";
                break;
            } default : {
                return "Other documents";
            }
        }
    }
}
