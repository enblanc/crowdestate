<?php

namespace App;

use App\Developer;
use App\Helpers\MyInterval;
use App\Investment;
use App\MailNotifications;
use App\PropertyDetails;
use App\PropertyEvolutions;
use App\PropertyState;
use App\PropertyType;
use App\Traits\MediaHelpersTrait;
use App\Traits\Searchable;
use App\Transformers\PropertyTransformer;
use App\User;
use Carbon\Carbon;
use Flugg\Responder\Contracts\Transformable;
use Flugg\Responder\Facades\Responder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Infinety\CRUD\Models\Locale;
use Infinety\LemonWay\Facades\LemonWay;
use Jenssegers\Date\Date;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMediaConversions;
use Webpatser\Uuid\Uuid;

class Property extends Model implements HasMediaConversions, Transformable
{
    use HasMediaTrait, MediaHelpersTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ref',
        'nombre',
        'slug',
        'catastro',
        'investment_id',
        'property_type_id',
        'ubicacion',
        'barrio',
        'zona_premium',
        'habitaciones',
        'property_state_id',
        'toilets',
        'metros',
        'gestor',
        'garaje',
        'piscina',
        'ascensor',
        'direccion',
        'lat',
        'lng',
        'antiguedad',
        'descripcion',
        'ficha',
        'meses_neta',
        'meses_bruta',
        'developer_id',
        'periodo_financiacion',
        'horizonte_temporal',
        'valor_mercado',
        'valor_compra',
        'coste_total',
        'hipoteca',
        'objetivo',
        'importe_minimo',
        'rentabilidad_alquiler_bruta',
        'rentabilidad_alquiler_neta',
        'rentabilidad_objetivo_bruta',
        'rentabilidad_objetivo_neta',
        'rentabilidad_anual',
        'rentabilidad_total',
        'tasa_interna_rentabilidad',
        'coste_obras',
        'impuestos_tramites',
        'registro_sociedad',
        'otros_costes',
        'imprevistos',
        'caja_minima',
        'honorarios',
        'ingresos_anuales_alquiler',
        'importe_venta',
        'compra_venta_margen_bruto',
        'resumen',
        'estado',
        'fecha_publicacion',
        'lemonway_id',
        'lemonway_walletid',
        'fake_inversores',
        'fake_invertido',
        'orden',
        'private',
    ];

    /**
     * Get default collection for media files
     *
     * @var string
     */
    public $mediaCollection = '256956ab-b067-54b3-8873-f6ef0fadce64'; //Is the uuid for 'properties'

    /**
     * Get default collection for media files documents
     *
     * @var string
     */
    public $mediaCollectionDocuments = 'a71f8be1-092e-5d22-b3d1-247c980817a9'; //Is the uuid for 'properties_documentos'

    /**
     * Get default collection for media files excel resumen
     *
     * @var string
     */
    public $mediaCollectionExcelResumen = '8846b98f-2bf8-56f8-ba9a-a6d80cfe485e'; //Is the uuid for 'properties_excel_resumen'

    /**
     * Get default collection for media files excel resumen
     *
     * @var string
     */
    public $mediaCollectionExcelEstimacion = 'a0a7b3c2-8949-5bc2-9f08-6783e414ec6b'; //Is the uuid for 'properties_excel_estimacion'

    /**
     * Get default collection for media files excel resumen
     *
     * @var string
     */
    public $mediaCollectionExcelEvolucion = '2b8167cd-463c-554c-95d5-eb678442a3b1'; //Is the uuid for 'properties_excel_evolucion'

    /**
     * Defaults relations to load within
     *
     * @var array
     */
    protected $with = ['details', 'investment', 'type', 'state', 'developer', 'media', 'evolutions', 'predicted'];
    

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'resumen' => 'boolean',
        'estado'  => 'boolean',
        'private' => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha_publicacion',
        'created_at',
        'updated_at',
    ];

    /**
     * The transformer used to transform the model data.
     *
     * @return Transformer|callable|string|null
     */
    public static function transformer()
    {
        return PropertyTransformer::class;
    }

    /**
     * Set fields be able current search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return [
            'ref',
            'nombre',
            'slug',
            'catastro',
            'ubicacion',
            'barrio',
            'zona_premium',
            'habitaciones',
            'toilets',
            'metros',
            'gestor',
            'garaje',
            'piscina',
            'ascensor',
            'direccion',
            'antiguedad',
            'descripcion',
            'ficha',
        ];
    }

    /**
     * Return investment for the property
     *
     * @return Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function investment(): BelongsTo
    {
        return $this->belongsTo(Investment::class);
    }

    /**
     * Return type for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(PropertyType::class, 'property_type_id');
    }

    /**
     * Return state for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(PropertyState::class, 'property_state_id');
    }

    /**
     * Return developer for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function developer(): BelongsTo
    {
        return $this->belongsTo(Developer::class);
    }

    /**
     * Return evolutions for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function evolutions(): HasMany
    {
        return $this->hasMany(PropertyEvolutions::class)->whereTipo('seguimiento')->orderBy('fecha');
    }

    /**
     * Return predicted values for current property filtered by current year
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function predicted(): hasMany
    {
        return $this->hasMany(PropertyEvolutions::class)->whereTipo('prevision')
            ->whereRaw('year(`fecha`) = ?', array(date('Y')))
            ->orWhereRaw('year(`fecha`) = ?', array(date('Y', strtotime('-1 year'))));
    }

    /**
     * Return predicted values for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\hasMany
     */
    public function predictedAll(): hasMany
    {
        return $this->hasMany(PropertyEvolutions::class)->whereTipo('prevision');
    }

    /**
     * Return details data
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function details(): HasMany
    {
        return $this->hasMany(PropertyDetails::class);
    }

    /**
     * Return details data
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function detailsTranslated(): HasOne
    {
        return $this->hasOne(PropertyDetails::class)->whereLocale(current_locale());
    }

    /**
     * Devuelve las propiedas en las que el usuario ha invertido
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->withPivot('id', 'total', 'abonado', 'total_abonado', 'transaction_id', 'prescriptor')->withTimestamps()->orderBy('pivot_created_at', 'DESC');
    }


    /**
     * Devuelve las propiedas en las que el usuario ha invertido
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function userExtends(): BelongsToMany
    {
        return $this->belongsToMany(\App\Models\User::class)->withPivot('id', 'total', 'abonado', 'total_abonado', 'transaction_id', 'prescriptor')->withTimestamps()->orderBy('pivot_created_at', 'DESC');
    }

    /**
     * Devuelve las propiedas en las que el usuario ha invertido agrupados por usuarios
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function usersGroup(): BelongsToMany
    {
        return $this->belongsToMany(User::class)->withPivot('id', 'user_id', 'total', 'abonado', 'total_abonado', 'transaction_id', 'prescriptor')->withTimestamps()->groupBy('pivot_user_id');
    }

    /**
     * Devuelve las notificaciones de email enviadas
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function mailNotifications(): HasMany
    {
        return $this->hasMany(MailNotifications::class);
    }

    /**
     * Set the polymorphic relation.
     *
     * @return Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function media(): MorphMany
    {
        return $this->morphMany(config('laravel-medialibrary.media_model'), 'model');
    }

    /**
     * Set slug for property
     *
     * @param string $value
     *
     * @return void
     */
    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * Set fecha in good format
     *
     * @param string $value
     *
     * @return void
     */
    public function setFechaPublicacionAttribute($value)
    {
        $formatted = Carbon::createFromFormat('d/m/Y', $value);
        $this->attributes['fecha_publicacion'] = $formatted;
    }

    /**
     * @return mixed
     */
    public function getNombreAttribute($value)
    {
        if (current_locale() === 'en') {
            if ($this->detailsTranslated) {
                if (!empty($this->detailsTranslated->title)) {
                    return $this->detailsTranslated->title;
                }
            }
        }

        return $value;
    }

    /**
     * Return only photos for the property
     *
     * @return App\Media
     */
    public function getPhotosAttribute()
    {
        $photos = Responder::transform($this->getMedia($this->mediaCollection))->toCollection();

        return $photos['data'];
    }

    /**
     * Return only photos for the property
     *
     * @return App\Media
     */
    public function getDocumentsAttribute()
    {
        $locales = Locale::getAvailables();

        $documents = [];
        foreach ($locales as $locale) {
            $documents[$locale->iso] = [
                'public'  => $this->getDocumentsByTypeAndLocale('public', $locale->iso),
                'private' => $this->getDocumentsByTypeAndLocale('private', $locale->iso),
            ];
        }

        return $documents;
    }

    /**
     * Return public documents
     *
     * @return mixed
     */
    public function getDocumentsPublicAttribute()
    {
        $documents = Responder::transform($this->getMedia($this->mediaCollectionDocuments, ['visibility' => 'public']))->toCollection();

        return $documents['data'];
    }

    /**
     * Return private documents
     *
     * @return mixed
     */
    public function getDocumentsPrivateAttribute()
    {
        $documents = Responder::transform($this->getMedia($this->mediaCollectionDocuments, ['visibility' => 'private']))->toCollection();

        return $documents['data'];
    }

    /**
     * Return mediaId encoded
     *
     * @return string
     */
    public function getMediaIdAttribute(): string
    {
        return Uuid::generate(5, $this->id.$this->user_id, Uuid::NS_DNS);
    }

    /**
     * Returns wallet for current user
     *
     * @return  \Infinety\LemonWay\Models\LemonWayWallet
     */
    public function getWalletAttribute()
    {
        return LemonWay::getWalletDetails(null, $this->lemonway_id);
    }

    /**
     * Return Balance
     *
     * @return mixed
     */
    public function getBalanceAttribute()
    {
        return $this->wallet->BAL;
    }

    /**
     * Return url for current property
     *
     * @return [type]
     */
    public function getUrlAttribute(): string
    {
        $slug = $this->slug;
        if (current_locale() === 'en') {
            if ($this->detailsTranslated) {
                if (!empty($this->detailsTranslated->slug)) {
                    $slug = $this->detailsTranslated->slug;
                }
            }
        }

        return route('front.property.show', $slug);
    }

    /**
     * Return current year of evolution data
     *
     * @return array
     */
    public function getCurrentEvolutionAttribute()
    {
        return $this->evolutionsByYear(date('Y'));
    }

    /**
     * Return if property is published or not
     *
     * @return bool
     */
    public function getPublicadoAttribute()
    {
        $today = Carbon::today();
        // $today->gte($this->fecha_publicacion) &&
        if ($this->getMedia($this->mediaCollection)->count() > 0 && $this->getMedia($this->mediaCollectionExcelResumen)->count() > 0 && $this->getMedia($this->mediaCollectionExcelEstimacion)->count() > 0 && $today->gte($this->fecha_publicacion)) {
            return true;
        }

        return false;
    }

    /**
     * Devuelve los inversores. SI el campo fake es null o cero devolvemos el campo real.
     * Si el campo fake es diferente de null o diferente de 0 mostramos el fake
     *
     * @return mixed
     */
    public function getFakeOrRealInversoresAttribute()
    {
        if ($this->fake_inversores == null || $this->fake_inversores == 0) {
            return $this->inversoresTotales;
        } else {
            return $this->fake_inversores + $this->inversoresTotales;
        }
    }

    /**
     * Devuelve los inversores. SI el campo fake es null o cero devolvemos el campo real.
     * Si el campo fake es diferente de null o diferente de 0 mostramos el fake
     *
     * @return mixed
     */
    public function getFakeOrRealInvertidoAttribute()
    {
        //Cuando el inmueble está en obras, en explotación o vendido, ponemos el 100%
        if (in_array($this->property_state_id, [3, 4, 5])) {
            return $this->objetivo;
        }

        if ($this->fake_invertido == null || $this->fake_invertido == 0) {
            return $this->invertido;
        } else {
            $totalInvertidoFake = $this->fake_invertido + $this->invertido;
            if ($totalInvertidoFake > $this->objetivo) {
                return $this->objetivo;
            }

            return $this->fake_invertido + $this->invertido;
        }
    }

    /**
     * Devuelve los inversores totales
     *
     * @return mixed
     */
    public function getInversoresTotalesAttribute()
    {
        return $this->usersGroup->count();
    }

    /**
     * Devuelve los inversores totales
     *
     * @return mixed
     */
    public function getInvertidoAttribute()
    {
        $total = 0;
        if ($this->users->count() > 0) {
            foreach ($this->users as $user) {
                $pivot_total = $user->pivot->total;
                if (is_string($pivot_total))
                  if (!str_contains($pivot_total, '.')) {
                     $total += intval($pivot_total);
                  } else {
                     $total += doubleval($pivot_total);
                     //logger(['id' => $user->pivot->id, 'total' => $pivot_total, 'double' => doubleval($pivot_total)]);
                  }
                else {
                $total += $pivot_total;
                }
            }
        }

        return $total;
    }

    /**
     * Devuelve el prodentaje invertido. También lo saca si el valor es fake.
     *
     * @return [type]
     */
    public function getPorcentajeCompletadoAttribute()
    {
        if ($this->fakeOrRealInvertido == 0) {
            return 0;
        }

        return round(($this->fakeOrRealInvertido * 100) / $this->objetivo, 2);
    }

    /**
     * Returns sumatory of all costs
     *
     * @return array
     */
    public function getTotalGastosAttribute()
    {
        return $this->valor_compra + $this->hipoteca + $this->coste_obras + $this->impuestos_tramites + $this->registro_sociedad + $this->otros_costes + $this->imprevistos + $this->caja_minima + $this->honorarios;
    }

    /**
     * Returns días restantes
     *
     * @return array
     */
    public function getRemainingDaysAttribute()
    {
        $endDate = $this->fecha_publicacion->addDays($this->periodo_financiacion);
        $now = Carbon::now();
        $diferencia = $now->diffInDays($endDate, false);

        return $diferencia;
    }

    /**
     * Returns días restantes
     *
     * @return array
     */
    public function getRemainingDateAttribute()
    {
        $endDate = Date::parse($this->fecha_publicacion)->addDays($this->periodo_financiacion);
        $endDate->addYears(3);

        $today = new Date();
        $dif = $endDate->diffInDays($today);

        $remain = $endDate->diff($today);

        $myDiff = new MyInterval($remain);

        return $myDiff->forHumans();
    }

    /**
     * Devuelve la fecha de finalización
     *
     * @return string
     */
    public function getFechaFinalizacionAttribute()
    {
        return $this->fecha_publicacion->addDays($this->periodo_financiacion);
    }

    /**
     * Returns the years have passed since the property was created. Eample: [2015, 2016, 2017]
     *
     * @return array
     */
    public function getAvailableYearsAttribute()
    {
        $today = Carbon::today();

        $yearsPassed = $this->created_at->diffInYears($today);

        $data[] = $today->year;

        if ($yearsPassed === 0) {
            return $data;
        }

        $data[] = $this->created_at->year;

        for ($i = 0; $i < $yearsPassed; $i++) {
            $data[] = $this->created_at->copy()->addYear()->year;
        }
        // Lo convertimos a años únicos
        $data = array_unique($data);
        // Ordenamos por año
        sort($data);

        return $data;
    }

    /**
     * @return mixed
     */
    public function getAvailableEvolutionsYearsAttribute()
    {
        $years = $this->evolutions()->groupBy('fecha')->pluck('fecha');
        $data = [];
        $today = Carbon::today();
        $data[] = $today->year;

        foreach ($years as $year) {
            $data[] = $year->year;
        }
        // Lo convertimos a años únicos
        $data = array_unique($data);

        // Ordenamos por año
        sort($data);

        return $data;
    }

    /**
     * Return an array with all months and evolution data for given year and current property
     *
     * @param  int $year
     *
     * @return array
     */
    public function evolutionsByYear($year)
    {
        $data = [];
        $months = ['enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'];
        for ($m = 1; $m <= 12; $m++) {
            $values = $this->evolutions()->year($year)->month($m)->first();
            if (!$values) {
                $propertyEmpty = new PropertyEvolutions();
                $values = $propertyEmpty->getEmptyAttributes();
            } else {
                $values = $values->getAttributesFixed();
            }

            $data[$months[$m - 1]] = $values;
        }

        return $data;
    }

    /**
     * Scope a query to filter by month.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVisible($query)
    {
        $today = Carbon::today();

        return $query->whereDate('fecha_publicacion', '=>', $today)->whereResumen(1)->whereEstado(1);
    }

    /**
     * Return docuemts by Type an Locale
     *
     * @param   string  $type
     * @param   string  $locale
     *
     * @return  array
     */
    public function getDocumentsByTypeAndLocale($type = 'visibility', $locale = null)
    {
        if ($locale == null) {
            $locale = current_locale();
        }

        $documents = Responder::transform(
            $this->getMedia($this->mediaCollectionDocuments, ['visibility' => $type, 'locale' => $locale])
        )->toCollection();

        return $documents['data'];
    }

    /**
     * @param $slug
     */
    public static function checkSlugAndReturn($slug)
    {
        $locale = current_locale();
        $details = PropertyDetails::whereLocale($locale)->whereSlug($slug)->first();

        if ($details) {
            return $details->property;
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getTranslationOrDefault()
    {
        $details = $this->detailsTranslated;
        if ($details != null) {
            return $details;
        }

        return $this->details()->whereLocale('en')->first();
    }

    /**
     * Performs conversions for each image
     */
    public function registerMediaConversions()
    {
        $this->addMediaConversion('thumb')
            ->setManipulations(['w' => 368, 'h' => 232])
            ->performOnCollections($this->mediaCollection); //, $this->mediaCollectionDocuments
    }
}
