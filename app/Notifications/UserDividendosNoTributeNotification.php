<?php

namespace App\Notifications;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserDividendosNoTributeNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $dividendos;

    /**
     * @var mixed
     */
    protected $mes;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Property $property, $dividendos, $mes)
    {
        $this->property = $property;
        $this->dividendos = $dividendos;
        $this->mes = __($mes);
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $total = number_format($this->dividendos, 2, '.', '');

        return (new MailMessage())
            ->subject(trans('emails.user_dividendos_no_tribute.subject'))
            ->greeting(trans('emails.user_dividendos_no_tribute.greetings', ['name' => $notifiable->nombre]))
            ->line(trans('emails.user_dividendos_no_tribute.message1', ['inmueble' => $this->property->nombre, 'mes' => $this->mes, 'total' => $total]))
            ->action(trans('emails.user_dividendos_no_tribute.action'), $this->getMyInversionsUrl());
    }

    /**
     * Get my inversions route translated
     *
     * @return  string
     */
    private function getMyInversionsUrl()
    {
        $locale = current_locale();

        return get_url('panel.user.inversion.general', [], $locale);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
