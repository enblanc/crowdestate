<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Jenssegers\Date\Date;

class PrescriptorInformeMensualNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    public $file;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($file)
    {
        $this->file = $file;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $today = Date::today();

        return (new MailMessage())
            ->subject(trans('emails.prescriptor_excel.subject'))
            ->greeting(trans('emails.prescriptor_excel.greetings', ['name' => $notifiable->nombre]))
            ->line(trans('emails.prescriptor_excel.message1'))
            ->attach($this->file, ['as' => 'Brickstarter - Infome '.ucfirst($today->format('F')).' de '.$today->year.'.xlsx']);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
