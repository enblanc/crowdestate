<?php

namespace App\Notifications;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DividendosVentaNotificacion extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $totalDevuelto;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Property $property, $total)
    {
        $this->property = $property;
        $this->total = $total;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(trans('emails.diviendos_venta.subject'))
            ->greeting(trans('emails.diviendos_venta.greetings', ['name' => $notifiable->nombre]))
            ->line(trans('emails.diviendos_venta.message1', ['inmueble' => $this->property->nombre, 'precio' => $this->total]))
            ->action(trans('emails.diviendos_venta.action'), route('panel.user.transactions.show'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
