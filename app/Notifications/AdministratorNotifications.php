<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AdministratorNotifications extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $usuario;

    /**
     * @var mixed
     */
    protected $tipo;

    /**
     * @var mixed
     */
    protected $error;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user, $tipo, $error)
    {
        $this->user = $user;
        $this->tipo = $tipo;
        $this->error = $error;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject('Error detectado en usuario')
            ->level('warning')
            ->line('Usuario: '.$this->user->nombreCompleto.' - '.$this->user->lemonway_id)
            ->line('Tipo de Error: '.$this->tipo)
            ->line('Error: '.$this->error);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
