<?php

namespace App\Notifications;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ConfirmEmailResetsNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $user;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $url = route('emails.confirmar-cambio-email', ['emailresettoken' => $this->user->emailresets->token]);

        return (new MailMessage())
            ->subject(trans('emails.email_resets.subject'))
            ->greeting(trans('emails.email_resets.greetings', ['name' => $this->user->nombre]))
            ->line(trans('emails.email_resets.message1'))
            ->line(trans('emails.email_resets.message2'))
            ->action(trans('emails.email_resets.action'), $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
