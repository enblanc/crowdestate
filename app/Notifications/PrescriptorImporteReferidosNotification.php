<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PrescriptorImporteReferidosNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $total;

    /**
     * @var mixed
     */
    protected $mes;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($total, $mes)
    {
        $this->total = $total;
        $this->mes = $mes;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(trans('emails.prescriptor_importe.subject'))
            ->greeting(trans('emails.prescriptor_importe.greetings', ['name' => $notifiable->nombre]))
            ->line(trans('emails.prescriptor_importe.message1', ['mes' => $this->mes, 'precio' => $this->total]))
            ->action(trans('emails.prescriptor_importe.action'), route('panel.user.inversion.general'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
