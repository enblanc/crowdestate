<?php

namespace App\Notifications;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class InmuebleNoConseguidoNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $totalDevuelto;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Property $property, $totalDevuelto)
    {
        $this->property = $property;
        $this->totalDevuelto = $totalDevuelto;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(trans('emails.property_failed.subject'))
            ->greeting(trans('emails.property_failed.greetings', ['name' => $notifiable->nombre]))
            ->line(trans('emails.property_failed.message1', ['inmueble' => $this->property->nombre, 'total' => $this->totalDevuelto]))
            ->action(trans('emails.property_failed.action'), route('panel.user.inversion.general'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
