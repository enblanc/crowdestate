<?php

namespace App\Notifications;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserInvertirConfirmNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $totalInversion;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Property $property, $totalInversion)
    {
        $this->property = $property;
        $this->totalInversion = $totalInversion;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage())
            ->subject(trans('emails.user_inversion.subject'))
            ->greeting(trans('emails.user_inversion.greetings', ['name' => $notifiable->nombre]))
            ->line(trans('emails.user_inversion.message1', ['inmueble' => $this->property->nombre, 'total' => $this->totalInversion]))
            ->action(trans('emails.user_inversion.action'), route('panel.user.inversion.general'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
