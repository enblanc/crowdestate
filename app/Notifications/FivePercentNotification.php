<?php

namespace App\Notifications;

use App\Property;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class FivePercentNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $abonado;

    /**
     * @var mixed
     */
    protected $retenido;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Property $property, $abonado, $retenido)
    {
        $this->property = $property;
        $this->abonado = $abonado;
        $this->retenido = $retenido;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $locale = $notifiable->locale;
        if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
            localization()->setLocale($locale);
        }

        $mail = (new MailMessage())
            ->subject(trans('emails.user_five_percent.subject'))
            ->greeting(trans('emails.user_five_percent.greetings', ['name' => $notifiable->nombre]));

        //Si el estado es No Conseguido, cambiamos el primer mensaje
        if ($this->property->property_state_id == 6) {
            $mail->line(trans('emails.user_five_percent.message_not_achieved', ['inmueble' => $this->property->nombre]));
        } else {
            $mail->line(trans('emails.user_five_percent.message1', ['inmueble' => $this->property->nombre]));
        }

        //Si no retiene en españa
        if ($this->retenido == false) {
            $mail->line(trans('emails.user_five_percent.message_not_spanish', ['abonado' => $this->abonado]));
        } else {
            $mail->line(trans('emails.user_five_percent.message2', ['abonado' => $this->abonado, 'retenido' => $this->retenido]));
        }

        $mail->line(trans('emails.user_five_percent.message3'))
            ->action(trans('emails.user_five_percent.action'), route('panel.user.inversion.general'));

        return $mail;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
