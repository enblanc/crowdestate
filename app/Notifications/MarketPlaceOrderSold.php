<?php

namespace App\Notifications;

use App\MarketOrder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class MarketPlaceOrderSold extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(MarketOrder $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(__('Nueva venta en Mercado'))
            ->greeting(__('Enhorabuena, se ha ejecutado tu orden de venta.'))
            ->line(__('A continuación te adjuntamos los datos:'))
            ->line(__('ID').': '.$this->order->hashId)
            ->line(__('Inmueble').': '.$this->order->getPropertyName())
            ->line(__('Inversión').': '.formatThousandsNotZero($this->order->quantity).'€')
            ->line(__('Precio').': '.formatThousandsNotZero($this->order->priceMoney).'€')
            ->line(__('Descuento/Prima').': '.$this->order->price.'%')
            ->line(__('TIR').': '.formatThousands($this->order->getProperty()->tasa_interna_rentabilidad).'%')
            ->line(__('Estado').': '.__('Ejecutada'))
            ->action(__('Ver mis órdenes de mercado'), route('panel.user.marketplace.orders'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
