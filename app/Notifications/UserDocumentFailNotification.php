<?php

namespace App\Notifications;

use App\Document;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class UserDocumentFailNotification extends Notification
{
    use Queueable;

    /**
     * @var mixed
     */
    protected $dataMailSatus;

    /**
     * Create a new notification instance.
     * @param $textStatus
     *
     */
    public function __construct($dataMailSatus)
    {
        $this->dataMailSatus = $dataMailSatus;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $actionUrl = $this->dataMailSatus['accepted'] ? $actionUrl = route('home') : $actionUrl = route('panel.user.accredit.show');

        return (new MailMessage())
            ->subject($this->dataMailSatus['subject'])
            ->greeting($this->dataMailSatus['greeting'])
            ->line($this->dataMailSatus['message1'])
            ->line($this->dataMailSatus['message2'])
            ->line($this->dataMailSatus['message3'])
            ->line($this->dataMailSatus['message4'])
            ->action($this->dataMailSatus['action'], $actionUrl);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
