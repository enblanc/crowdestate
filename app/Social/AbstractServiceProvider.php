<?php

namespace App\Social;

use App\User;
use Laravel\Socialite\Facades\Socialite;

abstract class AbstractServiceProvider
{
    /**
     * @var mixed
     */
    protected $provider;

    /**
     *  Create a new SocialServiceProvider instance
     */
    public function __construct()
    {
        $this->provider = Socialite::driver($this->providerName());
    }

    /**
     *  Logged in the user
     *
     *  @param  \App\User $user
     *  @return \Illuminate\Http\Response
     */
    protected function login($user)
    {
        auth()->login($user);

        if (!$user->confirm->is_confirmed) {
            auth()->logout();

            return redirect()->route('login')->withErrors(['confirmed' => __('El usuario no está confirmado. Por favor, valida tu cuenta mediante el email que te enviamos al registrarte.')]);
        }

        return redirect()->intended('/');
    }

    /**
     *  Register the user
     *
     *  @param  array $user
     *  @return User $user
     */
    protected function goToRegister($nombre, $apellido1, $apellido2, $email, $provider, $providerId)
    {
        return redirect()->route('registro.oauth', compact('nombre', 'apellido1', 'apellido2', 'email', 'provider', 'providerId'));
    }

    /**
     * @param $userSocial
     * @param User $user
     */
    protected function goToLinkAccount($userSocial, User $user)
    {
        return redirect()->route('link.oauth', [
            'email'       => $user->email,
            'provider'    => $this->providerName(),
            'provider_id' => $userSocial->getId(),
        ]);
    }

    /**
     * @param $user
     */
    protected function checkUserExists($user)
    {
        if ($this->checkByEmail($user)) {
            return $this->checkByEmail($user);
        }

        if ($this->checkByProvider($user)) {
            return $this->checkByProvider($user);
        }

        return false;
    }

    /**
     * @param $user
     */
    private function checkByProvider($user)
    {
        return User::where('provider', $this->providerName())->where('provider_id', $user->getId())->first();
    }

    /**
     * @param $user
     */
    private function checkByEmail($user)
    {
        return User::where('email', $user->getEmail())->first();
    }

    /**
     *  Redirect the user to provider authentication page
     *
     *  @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return $this->provider->redirect();
    }

    /**
     *  Handle data returned by the provider
     *
     *  @return \Illuminate\Http\Response
     */
    abstract public function handle();

    /**
     * @var mixed
     */
    abstract public function providerName();
}
