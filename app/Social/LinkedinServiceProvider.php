<?php

namespace App\Social;

use App\User;

class LinkedinServiceProvider extends AbstractServiceProvider
{
    /**
     * @var string
     */
    protected $providerName = 'linkedinv2';

    /**
     * Obtiene el nombre del proveeder
     *
     * @return mixed
     */
    public function providerName()
    {
        return $this->providerName;
    }

    /**
     *  Handle Linkedin response
     *
     *  @return Illuminate\Http\Response
     */
    public function handle()
    {
        $user = $this->provider->user();

        $userData = (object) $user->user;

        $userExists = $this->checkUserExists($user);

        if ($userExists) {
            if ($userExists->provider === null && $userExists->provider_id === null) {
                return $this->goToLinkAccount($user, $userExists);
            }

            return $this->login($userExists);
        }

        return $this->goToRegister(
            $this->getName($userData, $user->getName()),
            $this->getFamilyName($userData),
            null,
            $user->getEmail(),
            $this->providerName,
            $user->getId()
        );
    }

    /**
     * Obtiene el nombre
     *
     * @param $user
     */
    private function getName($user, $default)
    {
        if (isset($user->firstName)) {
            return $user->firstName;
        }

        return $default;
    }

    /**
     * Obtiene el nombre
     *
     * @param $user
     */
    private function getFamilyName($user)
    {
        if (isset($user->lastName)) {
            return $user->lastName;
        }

        return null;
    }
}
