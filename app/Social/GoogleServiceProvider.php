<?php

namespace App\Social;

use App\User;

class GoogleServiceProvider extends AbstractServiceProvider
{
    /**
     * @var string
     */
    protected $providerName = 'google';

    /**
     * Obtiene el nombre del proveeder
     *
     * @return mixed
     */
    public function providerName()
    {
        return $this->providerName;
    }

    /**
     *  Handle Google response
     *
     *  @return Illuminate\Http\Response
     */
    public function handle()
    {
        $user = $this->provider->user();
        $userData = (object) $user->user;

        $userExists = $this->checkUserExists($user);

        if ($userExists) {
            if ($userExists->provider === null && $userExists->provider_id === null) {
                return $this->goToLinkAccount($user, $userExists);
            }

            return $this->login($userExists);
        }

        return $this->goToRegister(
            $this->getName($userData),
            $this->getFamilyName($userData),
            null,
            $user->getEmail(),
            $this->providerName,
            $user->getId()
        );
    }

    /**
     * Obtiene el nombre
     *
     * @param $user
     */
    private function getName($user)
    {
        if (isset($user->name)) {
            $name = $user->name;
            if (isset($name['givenName'])) {
                return $name['givenName'];
            }
        }

        return $user->displayName;
    }

    /**
     * Obtiene el nombre
     *
     * @param $user
     */
    private function getFamilyName($user)
    {
        if (isset($user->name)) {
            $name = $user->name;
            if (isset($name['familyName'])) {
                return $name['familyName'];
            }
        }

        return null;
    }
}
