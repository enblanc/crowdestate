<?php

namespace App\Social;

use App\User;

class FacebookServiceProvider extends AbstractServiceProvider
{
    /**
     * @var string
     */
    protected $providerName = 'facebook';

    /**
     * Obtiene el nombre del proveeder
     *
     * @return mixed
     */
    public function providerName()
    {
        return $this->providerName;
    }

    /**
     * @var array
     */
    protected $fieldsData = [
        'name',
        'first_name',
        'last_name',
        'email',
        'gender',
        'birthday',
        'location',
        'verified',
    ];

    /**
     * @var array
     */
    protected $scopes = [
        'user_birthday',
        'user_location',
    ];

    /**
     *  Handle Facebook response
     *
     *  @return Illuminate\Http\Response
     */
    public function handle()
    {
        $user = $this->provider->fields($this->fieldsData)->user();
        $userData = (object) $user->user;

        $userExists = $this->checkUserExists($user);

        if ($userExists) {
            if ($userExists->provider === null && $userExists->provider_id === null) {
                return $this->goToLinkAccount($user, $userExists);
            }

            return $this->login($userExists);
        }

        return $this->goToRegister(
            $userData->first_name,
            $userData->last_name,
            null,
            $user->getEmail(),
            'facebook',
            $user->getId()
        );
    }

    /**
     *  Redirect the user to provider authentication page
     *
     *  @return \Illuminate\Http\Response
     */
    public function redirect()
    {
        return $this->provider
            ->fields($this->fieldsData)
            ->redirect();
    }
}
