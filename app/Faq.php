<?php

namespace App;

use App\FaqType;
use App\Traits\Searchable;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Infinety\CRUD\CrudTrait;

class Faq extends Model
{
    use Translatable, CrudTrait, Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'faq_id',
        'nombre',
        'texto',
        'faq_types_id',
        'locale',
    ];

    /**
     * @var array
     */
    public $translatedAttributes = ['nombre', 'texto'];

    /**
     * @return mixed
     */
    public function type()
    {
        return $this->belongsTo(FaqType::class, 'faq_types_id');
    }

    /**
     * @return mixed
     */
    public function typeName()
    {
        return $this->type->nombre;
    }

    /**
     * Set fields be able to search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return ['nombre', 'texto'];
    }
}
