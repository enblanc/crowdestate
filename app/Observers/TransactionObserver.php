<?php

namespace App\Observers;

use App\Traits\TargetCircleTrait;
use App\Transaction;
use HttpOz\Roles\Models\Role;
use Illuminate\Support\Facades\Log;

class TransactionObserver
{
    use TargetCircleTrait;

    /**
     * @var mixed
     */
    protected $transaction;

    /**
     * Evento para comprobar si una transacción es de tipo 'inversión' y se ha efectuado con éxito.
     * Si es la primera vez le añade el rol de inversor pues ya habrá invertido.
     *
     * Comprueba si es la primera inversión del usuario y si ha sido referido por alguien.
     * En ese caso le da al usaurio referidor un regalo.
     *
     * @param Transaction $transaction
     *
     * @return void
     */
    public function saved(Transaction $transaction)
    {
        if ($transaction->type == 2 && $transaction->status == 3) {
            if (!str_contains($transaction->comment, 'Regalo') || !str_contains($transaction->comment, 'Gift')) {
                $user = $transaction->user;


                if (!$user->hasRole('inversor')) {
                    Log::info('Usuario: ' . $user->id . ' - Es su primera inversion');
                    // Le añadimos el rol de inversor
                    $inversorRole = Role::whereSlug('inversor')->first();
                    $user->attachRole($inversorRole);


                    if ($user->properties->count() == 0) {
                        // Si ha sido referido le creamos un regalo al referidor
                        if ($user->referrer) {
                            $referrer = $user->referrer;

                            //Comprobamos si el usuario tiene asignado algún importe
                            $giftUser = ($referrer->gift_recibe !== null) ? $referrer->gift_recibe : env('GIFT_REFERRAL', 0);

                            $referrer->gifts()->create(['importe' => $giftUser]);
                        }
                    }

                    // Como es la primera vez lanzamos el evento de TargetCIrcle
                    $this->firstInvestmentEvent($transaction);
                }
            }

            $this->investmentEvent($transaction);

            try {
                event(new \App\Events\UserInvierteClickId($transaction->user, $transaction));
            } catch (\Throwable $th) {
                logger($th);
            }
        }
    }
}
