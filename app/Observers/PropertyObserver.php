<?php

namespace App\Observers;

use App\Jobs\DevolverInversiones;
use App\Jobs\RepartirPorcentajes5;
use App\Jobs\RepartirVentaInmueble;
use App\Jobs\AutoInvestPropertyJob;
use App\Jobs\RemoveMarketOrder;
use App\Property;
use App\PropertyUser;
use App\Transaction;
use App\MarketOrder;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Infinety\CRUD\Models\Locale;
use Infinety\LemonWay\Facades\LemonWay;
use Prologue\Alerts\Facades\Alert;

class PropertyObserver
{

    /**
     * @var mixed
     */
    protected $newProperty;

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(Property $property)
    {
        try {
            $this->newProperty = $property;

            $this->setDisabledByDefault();

            $this->setNewDetails($property);

            $this->createNewLemonWayAccount();

            if ($property->publicado)
                $this->autoInvest($property);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * Listen to the Property updating event.
     *
     * @param  Property $property
     *
     * @return bool
     */
    public function updating(Property $property)
    {
        if ($property->isDirty('property_state_id')) {
            $oldState = $property->getOriginal('property_state_id');
            $currentState = $property->property_state_id;

            if ($oldState == 2 && $currentState == 3) {
                //Psar dinero a cuenta wallet SC
                // if (!$this->checkInvertido($property)) {
                //     Alert::error('El procentaje invertido es menor al objetivo')->flash();

                //     return false;
                // }

                // * Se comento el Septiembre-Octubre del 2021
                // * Ya que no dejaba generar el reparto de los diviendos.
                // 
                // if (!$this->transferirTodasInversiones($property)) {
                //    Alert::error('La transferencia no se ha procesado correctamente')->flash();
                //    return false;
                // }
                
                //Repartimos los diviendos del 5%
                $this->generarCincoPorciento($property);
            }

            if ($oldState == 2 && $currentState == 4) {
                //Psar dinero a cuenta wallet SC
                // if (!$this->checkInvertido($property)) {
                //     Alert::error('El procentaje invertido es menor al obetivo')->flash();

                //     return false;
                // }

                if (!$this->transferirTodasInversiones($property)) {
                    Alert::error('La transferencia no se ha procesado correctamente')->flash();

                    return false;
                }

                //Repartimos los diviendos del 5%
                $this->generarCincoPorciento($property);
            }

            // VENTA DEL INMUEBLE
            if ($currentState == 5) {
                //Repartimos los últimos dividendos de la venta
                if (!$this->transferirDineroVenta($property)) {
                    Alert::error('Falta dinero en el WalletSC o el campo "dividendos abonar" de la última evolución está vacío')->flash();

                    return false;
                }
            }
        }

        if ($property->isDirty(['property_type_id', 'estado', 'tasa_interna_rentabilidad', 'developer_id', 'ubicacion', 'fecha_publicacion', 'private'])) {
            $this->autoInvest($property);
        }
    }

    /**
     * Listen to the Property updating event.
     *
     * @param Property $property
     */
    public function updated(Property $property)
    {
        // Ahora hay que comprobar si el estado en 6 - No conseguido.
        // En cuyo caso hay que devolver a todos el importe que invirtieron.
        if ($property->isDirty('property_state_id')) {
            $currentState = $property->property_state_id;
            if ($currentState == 5) {
                $this->removeMarketOrder($property);
            } if ($currentState == 6) {
                foreach ($property->users as $user) {
                    if ($user->pivot->transaction_id !== null) {
                        $transaccion = Transaction::where('type', 2)
                            ->where('user_id', $user->id)
                            ->where('extras', $property->id)
                            ->where('id', $user->pivot->transaction_id)
                            ->first();

                        if ($transaccion) {
                            if (!str_contains($transaccion->token, 'MAN')) {
                                $movimientosDevolucionInmueble = Transaction::where('type', 16)->where('extras', $property->id)->first();
                                if (!$movimientosDevolucionInmueble) {
                                    $relacion = PropertyUser::find($user->pivot->id);
                                    $job = (new DevolverInversiones($property, $user, $relacion))->delay(Carbon::now()->addMinutes(1));
                                    dispatch($job);
                                } else {
                                    Log::info('El usuario: '.$user->nombre.' ya ha recibido su devolución');
                                }
                            }
                        }
                    } else {
                        $movimientosDevolucionInmueble = Transaction::where('type', 16)->where('extras', $property->id)->first();
                        if (!$movimientosDevolucionInmueble) {
                            $relacion = PropertyUser::find($user->pivot->id);
                            $job = (new DevolverInversiones($property, $user, $relacion))->delay(Carbon::now()->addMinutes(1));
                            dispatch($job);
                        } else {
                            Log::info('El usuario: '.$user->nombre.' ya ha recibido su devolución');
                        }
                    }
                }

                //Repartimos los diviendos del 5% aunque se haya devuelto el inmueble
                $this->generarCincoPorciento($property);
            }
        }
    }

    /**
     * Set estado to false
     */
    private function setDisabledByDefault()
    {
        $this->newProperty->estado = true;
        $this->newProperty->property_state_id = 1;
        $this->newProperty->save();
    }

    /**
     * Create null details record
     */
    private function setNewDetails(Property $property)
    {
        $locales = Locale::getAvailables();
        foreach ($locales as $locale) {
            $property->details()->create(['locale' => $locale->iso]);
        }
    }

    /**
     * Create new LemonWay Account for current property
     */
    private function createNewLemonWayAccount()
    {   
        /* 
        * SE COMENTO ESTA SECCIÓN PORQUE DEJÓ DE FUNCIONAR
        * CAMBIO REALIZADO EL 4/4/2022
        $newToken = 'INMUEBLE_'.$this->newProperty->id.'_'.mb_strtoupper(str_random(3));
        $fakeEmail = str_slug($this->newProperty->ref).'_'.mb_strtoupper(str_random(3)).'@brickstarter.com';
        $fakeNombre = 'INMUEBLE';
        $fakeApellidos = $this->newProperty->nombre;

        $extras = [
            'birthdate'          => date('d/m/Y'),
            'nationality'        => 'ESP',
            'ctry'               => 'ESP',
            'payerOrBeneficiary' => 2,
            'isTechWallet'       => 1,
        ];
        $newLemonWayUser = LemonWay::setWalletUser($newToken, $fakeEmail, $fakeNombre, $fakeApellidos, $extras);
        $wallet = LemonWay::createWallet($newLemonWayUser);
        $this->newProperty->lemonway_id = $newToken;
        $this->newProperty->lemonway_walletid = $wallet->LWID;
        */
        
        if (is_null($this->newProperty->lemonway_id)) {
            $this->newProperty->lemonway_id = 'INMUEBLE_62_FAK';
            $this->newProperty->lemonway_walletid = 1064;
            $this->newProperty->save();
        }
    }

    /**
     * Comprueba si el invertido es igual al objetivo
     *
     * @param App\Property $property
     */
    private function checkInvertido(Property $property)
    {
        if ($property->balance >= $property->objetivo) {
            return true;
        }

        logger("$property->nombre tiene un balance de $property->balance €");

        return false;
    }

    /**
     * Genera los porcentajes del 5 %
     *
     * @param App\Property $property
     */
    private function generarCincoPorciento(Property $property)
    {
        foreach ($property->usersGroup as $user) {
            logger($user->nombre.' Repartir 5%');
            $movimientoCincoPorcentajeInmueble = Transaction::where('user_id', $user->id)->where('type', 18)->where('extras', $property->id)->first();
            if (!$movimientoCincoPorcentajeInmueble) {
                $relacion = PropertyUser::find($user->pivot->id);
                $job = (new RepartirPorcentajes5($property, $user, $relacion))->delay(Carbon::now()->addSeconds(20));
                dispatch($job);
            } else {
                logger('El usuario: '.$user->nombre.' ya ha recibido su dividendo del 5%');
            }
        }
    }

    /**
     * Transfiere el dinero de la propiedad a la cuenta SC
     *
     * @param App\Property $property
     *
     * @return bool
     */
    private function transferirTodasInversiones(Property $property)
    {
        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        if ($property->balance == 0) {
            return true;
        }

        try {
            $result = LemonWay::walletToWallet($property->wallet, $walletSC, $property->balance, 'Tranferencia inmueble: '.$property->ref);

            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param Property $property
     */
    private function transferirDineroVenta(Property $property)
    {
        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        if ($walletSC->BAL == 0) {
            return false;
        }

        $evolucion = $property->evolutions()->get()->last();
        if ($evolucion->dividendos_abonar == 0 || $property->dividendos_abonar > $walletSC->BAL) {
            return false;
        }

        $evolucion->update(['abonado' => 2]);

        $totalRepartir = $evolucion->dividendos_abonar;

        logger('Total a repartir venta '.$property->nombre.': '.$totalRepartir);

        return $this->transfererirVentaPorUsuario($property, $evolucion, $totalRepartir);
    }

    /**
     * @param Property $property
     */
    private function transfererirVentaPorUsuario(Property $property, $evolucion, $totalRepartir)
    {
        $i = 10;
        foreach ($property->usersGroup as $user) {
            logger($user->nombre.' Venta del inmueble:'.$property->id);
            $movimientoVentaInmueble = $user->transactions()->where('type', 14)->where('extras', $property->id)->first();
            if (!$movimientoVentaInmueble) {
                $job = (new RepartirVentaInmueble($property, $user, $evolucion, $totalRepartir))->delay(Carbon::now()->addSeconds($i));
                dispatch($job);
                $i = $i + 10;
            } else {
                logger('El usuario: '.$user->nombre.' ya ha recibido su porcentaje de la venta');
            }
        }

        return true;
    }

    /**
     *
     * @return void
     */
    private function autoInvest ($property)
    {
        try {
            logger("AUTOINVEST:RUN:JOB => property_state_id => " . $property->property_state_id);
            $job = (new AutoInvestPropertyJob($property))->delay(Carbon::now()->addMinutes(1));
            dispatch($job);
        } catch (\Throwable $th) {
            logger("AUTOINVEST:ERROR:RUN:JOB => " . $th->getMessage());
        }
    }

    /**
     * Remover las ordenes del Marketplace
     *
     * @param Property $property
     * @return void
     */
    private function removeMarketOrder (Property $property)
    {
        $job = (new RemoveMarketOrder($property))->delay(Carbon::now()->addMinutes(1));
        dispatch($job);
    }
}
