<?php

namespace App\Observers;

use App\Traits\UserRegistrationEventsTrait;
use App\User;

class UserObserver
{
    use UserRegistrationEventsTrait;

    /**
     * @var mixed
     */
    protected $newUser;

    /**
     * Listen to the User created event.
     *
     * @param  User  $user
     * @return void
     */
    public function created(User $user)
    {
        return $this->performEventsActions($user);

        auth()->logout();
    }

    /**
     * Listen to the User deleting event.
     *
     * @param  User  $user
     * @return void
     */
    public function deleting(User $user)
    {
        if ($user->hasRole('inversor')) {
            return false;
        }

        return true;
    }
}
