<?php

namespace App;

use App\Media;
use App\Transformers\PropertyEvolutionsTransformer;
use Flugg\Responder\Contracts\Transformable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Jenssegers\Date\Date;

class PropertyEvolutions extends Model implements Transformable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['property_id', 'fecha', 'tipo', 'media_id', 'numero_reservas', 'grado_ocupacion', 'dias_reservados', 'dias_no_reservados', 'dias_bloqueados', 'tarifa_media_dia', 'estimacion_ingresos_anuales', 'satisfaccion_clientes', 'numero_invitados', 'ingresos_totales', 'ingresos_alquiler', 'ingresos_venta', 'otros_ingresos', 'gastos', 'gastos_explotacion', 'publicidad_prensa', 'otro_material', 'gastos_ventas', 'i_mas_d', 'arrendamientos', 'comisiones_crowdestate', 'gestion', 'suministros', 'ibi', 'otras_tasas', 'amortizacion', 'subvenciones', 'enajenacion_inmovilizado', 'resultado_explotacion', 'resultado_financiero', 'ingresos_financieros', 'gastos_financieros', 'otros_instrumentos_financieros', 'resultado_antes_impuestos', 'impuestos', 'resultados_ejercicio', 'dividendos_generados', 'dividendos_abonar', 'total_dividendos_abonados', 'dividendos_cartera', 'total_dividendos_devengados', 'revalorizacion_inmueble', 'valor_actual_inmueble', 'abonado',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'fecha',
        'created_at',
        'updated_at',
    ];

    /**
     * The transformer used to transform the model data.
     *
     * @return Transformer|callable|string|null
     */
    public static function transformer()
    {
        return PropertyEvolutionsTransformer::class;
    }

    /**
     * Return state for current property
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * Return media file for current evolution
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function media(): BelongsTo
    {
        return $this->belongsTo(Media::class);
    }

    /**
     * Scope a query to filter by year.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $year
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeYear($query, $year)
    {
        return $query->whereYear('fecha', $year);
    }

    /**
     * Scope a query to filter by month.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int $month
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeMonth($query, $month)
    {
        return $query->whereMonth('fecha', $month);
    }

    /**
     * Devuelve la fecha localizada
     *
     * @param $value
     */
    public function getFechaAttribute($value)
    {
        Date::setLocale('es');

        return Date::parse($value);
    }

    /**
     * @return mixed
     */
    public function getEmptyAttributes()
    {
        $fields = $this->fillable;
        $dataEmpty = [];
        foreach ($fields as $field) {
            if ($field != 'tipo' && $field != 'id' && $field != 'property_id' && $field != 'created_at' && $field != 'updated_at' && $field != 'fecha') {
                $dataEmpty[$field] = null;
            }
        }

        return $dataEmpty;
    }

    /**
     * @return mixed
     */
    public function getAttributesFixed()
    {
        $fields = $this->getAttributes();
        $dataEmpty = [];
        foreach ($fields as $field => $value) {
            if ($field != 'tipo' && $field != 'id' && $field != 'property_id' && $field != 'created_at' && $field != 'updated_at' && $field != 'fecha') {
                $dataEmpty[$field] = $value;
            }
        }

        return $dataEmpty;
    }
}
