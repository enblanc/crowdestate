<?php

namespace App;

use App\Traits\MediaHelpersTrait;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\Interfaces\HasMedia;
use Webpatser\Uuid\Uuid;

class Document extends Model implements HasMedia
{
    use HasMediaTrait, MediaHelpersTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'name', 'type', 'status', 'lemonway_id', 'valid_date', 'comment',
    ];

    /**
     * Defaults relations to load within
     *
     * @var array
     */
    protected $with = ['media'];

    /**
     * Get default collection for media files
     *
     * @var string
     */
    public $mediaDniCollection = '50ffab30-43b9-583b-a257-544e440f03fc'; //Is the uuid for for 'dni'

    /**
     * Get default collection for media files
     *
     * @var string
     */
    public $mediaBankCollection = '7129ade6-7d1b-53dd-8916-3cec9f619c01'; //Is the uuid for 'bank_accounts'

    /**
     * Return if this document is valid
     *
     * @return  bool
     */
    public function getIsValidAttribute()
    {
        if ($this->status == 2) {
            return true;
        }

        return false;
    }

    /**
     * Return the status string
     *
     * @return  string
     */
    public function getStatusTextAttribute()
    {
        switch ($this->status) {
            case 0:
                $estado = __('Pendiente de validación') . '. ' . trans('emails.account_document_fail.status_0_doc_put_on_hold') ;
                break;
            case 1:
                $estado = __('Documento no verificado') . '. ' . trans('emails.account_document_fail.status_1_received_default_status');
                break;
            case 2:
                $estado = __('Documento aceptado');
                break;
            case 3:
                $estado = __('Documento no aceptado') . '. ' . trans('emails.account_document_fail.status_3_rejected');
                break;
            case 4:
                $estado = __('Documento ilegible') . '. ' . trans('emails.account_document_fail.status_4_rejected_doc_unreadable');
                break;
            case 5:
                $estado = __('Documento expirado') . '. ' . trans('emails.account_document_fail.status_5_rejected_doc_expired');
                break;
            case 6:
                $estado = __('Tipo de documento incorrecto') . '. ' . trans('emails.account_document_fail.status_6_rejected_wrong_type');
                break;
            case 7:
                $estado = __('Nombre equivocado') . '. ' . trans('emails.account_document_fail.status_7_rejected_wrong_name');
                break;
            case 8:
                $estado = __('Documento duplicado') . '. ' . trans('emails.account_document_fail.status_8_rejected_duplicate_document');
                break; 
            default:
                $estado = __('Esperando');
                break;
        }

        return $estado;
    }

    /**
     * Return the DNI
     *
     * @return mixed
     */
    public function getDni()
    {
        return $this->getFirstMedia($this->mediaDniCollection);
    }

    /**
     * Return the IBAN
     *
     * @return mixed
     */
    public function getIban()
    {
        return $this->getFirstMedia($this->mediaBankCollection);
    }

    /**
     * Return mediaId encoded
     *
     * @return string
     */
    public function getMediaIdAttribute()
    {
        return Uuid::generate(5, $this->id.$this->user_id, Uuid::NS_DNS);
    }

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
