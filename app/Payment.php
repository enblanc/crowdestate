<?php

namespace App;

use App\Traits\Searchable;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Payment extends Model
{
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type',
        'user_id',
        'price',
        'subject',
        'comment',
        'transaction_id',
        'status',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'status' => 'integer',
    ];

    /**
     * @var array
     */
    public $validTypes = [0, 3, 4, 13, 14, 15, 16, 18, 22];

    /**
     * Set fields be able to search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return ['type', 'user_id', 'price', 'subject', 'comment', 'status'];
    }

    /**
     * Devuelve el inmueble
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getStatusTextAttribute()
    {
        switch ($this->status) {
            case 0:
                return 'No procesado';
                break;
            case 1:
                return 'Procesado';
                break;
            case 2:
                return 'Procesando';
                break;
            case 3:
                return 'Error de tipo (No procesable)';
                break;
            case 4:
                return 'Error al procesar';
                break;
            case 5:
                return 'Usuario no encontrado';
                break;
            default:
                return 'Error';
                break;
        }
    }

    /**
     * Return type in text mode
     *
     * @return string
     */
    public function getTypeTextAttribute()
    {
        switch ($this->type) {
            case 0:
                return __('Pago con tarjeta');
                break;
            case 3:
                return __('Ingreso por transferencia');
                break;
            case 4:
                return __('Devolución');
                break;
            case 13:
                return __('Divindendos generados por Inmueble');
                break;
            case 14:
                return 'Venta de Inmueble';
            case 15:
                return 'Regalo';
            case 16:
                return __('Devolución inmueble no conseguido');
            case 18:
                return __('Diviendos generados del 5%');
                break;
            case 22:
                return __('Regalo Brickstarter');
                break;
            default:
                return __('Tipo no correcto');
                break;
        }
    }
}
