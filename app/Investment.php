<?php

namespace App;

use App\Property;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Concerns\hasMany;
use Illuminate\Database\Eloquent\Model;
use Infinety\CRUD\CrudTrait;

class Investment extends Model
{
    use Translatable, CrudTrait;

    /**
     * @var array
     */
    public $translatedAttributes = ['nombre'];

    /**
     * @var array
     */
    protected $fillable = ['investment_id', 'nombre', 'locale'];

    /**
     * Get properties of a investment
     *
     * @return Illuminate\Database\Eloquent\Concerns\hasMany
     */
    public function properties(): hasMany
    {
        return $this->hasMany(Property::class);
    }
}
