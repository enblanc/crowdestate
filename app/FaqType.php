<?php

namespace App;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Infinety\CRUD\CrudTrait;

class FaqType extends Model
{
    use Translatable, CrudTrait;

    /**
     * @var array
     */
    protected $fillable = ['faq_type_id', 'nombre', 'locale'];

    /**
     * @var array
     */
    public $translatedAttributes = ['nombre'];

    public function faqItems()
    {
    	return $this->hasMany('App\Faq', 'faq_types_id');
	}
}
