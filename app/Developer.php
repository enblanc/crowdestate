<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Infinety\CRUD\CrudTrait;

class Developer extends Model
{
    use CrudTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'direccion',
        'email',
        'contacto',
    ];

    /**
     * Get properties of a developer
     *
     * @return Illuminate\Database\Eloquent\Concerns\hasMany
     */
    public function properties(): HasMany
    {
        return $this->hasMany(Property::class);
    }
}
