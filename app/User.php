<?php

namespace App;

use App\BankAccount;
use App\Document;
use App\EmailResets;
use App\Events\UserInvierteInmueble;
use App\Helpers\TokensHelper;
use App\MarketOrder;
use App\Notifications\CustomResetPassword;
use App\Notifications\UserInvertirConfirmNotification;
use App\Profile;
use App\Property;
use App\PropertyUser;
use App\User;
use App\Certification;
use App\Traits\CincoPorcientoTrait;
use App\Traits\ReferralsTrait as Referrals;
use App\Traits\Searchable;
use App\Transaction;
use App\UserGifts;
use Carbon\Carbon;
use HttpOz\Roles\Contracts\HasRole as HasRoleContract;
use HttpOz\Roles\Traits\HasRole;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\File;
use Infinety\CRUD\CrudTrait;
use Infinety\EmailConfirmation\Traits\EmailConfirmation;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;
use Infinety\LemonWay\Facades\LemonWay;
use Newsletter;

class User extends Authenticatable implements HasRoleContract
{
    use Notifiable, EmailConfirmation, HasRole, Searchable, Referrals, CrudTrait, CincoPorcientoTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
        'apellido1',
        'apellido2',
        'email',
        'password',
        'provider',
        'provider_id',
        'referred_by',
        'affiliate_id',
        'lemonway_id',
        'lemonway_walletid',
        'porcentaje_invertido',
        'porcentaje_ganado',
        'gift_referidor',
        'gift_recibe',
        'tribute',
        'locale',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'provider_id',
    ];

    /**
     * Defaults relations to load within
     *
     * @var array
     */
    protected $with = ['roles', 'profile', 'bankAccounts', 'confirm'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'tribute' => 'boolean',
    ];

    /**
     * Set fields be able to search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return ['nombre', 'apellido1', 'apellido2', 'email', 'lemonway_id', 'lemonway_walletid', 'tribute'];
    }

    /**
     * Get the profile for the user
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile(): HasOne
    {
        return $this->hasOne(Profile::class);
    }

    /**
     * Get the emails resets for the user
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function emailResets(): HasOne
    {
        return $this->hasOne(EmailResets::class);
    }

    /**
     * Return user bank accounts
     *
     * @return Illuminate\Database\Eloquent\Concerns\hasMany
     */
    public function bankAccounts()
    {
        return $this->hasMany(BankAccount::class);
    }
    
    /**
     * Return user certifications
     *
     * @return Illuminate\Database\Eloquent\Concerns\hasMany
     */
    public function certifications ()
    {
        return $this->hasMany(Certification::class);
    }

    /**
     * Return user bank accounts
     *
     * @return Illuminate\Database\Eloquent\Concerns\hasMany
     */
    public function documents()
    {
        return $this->hasMany(Document::class);
    }

    /**
     * Return user bank accounts
     *
     * @return Illuminate\Database\Eloquent\Concerns\hasMany
     */
    public function gifts()
    {
        return $this->hasMany(UserGifts::class)->where('executed', 0);
    }

    /**
     * Return user bank accounts
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }

    /**
     * Return user marketplace orders
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function marketOrders(): HasMany
    {
        return $this->hasMany(MarketOrder::class)->where('executed', false);
    }

    /**
     * Return user marketplace orders
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function marketOrdersHistorical(): HasMany
    {
        return $this->hasMany(MarketOrder::class)->where('executed', true);
    }

    /**
     * Devuelve las propiedas en las que el usuario ha invertido
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function properties(): BelongsToMany
    {
        return $this->belongsToMany(Property::class)->withPivot('id', 'total', 'abonado', 'total_abonado', 'transaction_id', 'prescriptor')->withTimestamps();
    }

    /**
     * Obtiene los inmuebles invertidos
     *
     * @param array $exception
     * @return Collection
     */
    public function getRealProperties($exceptions = [])
    {
        $properties = $this->getListPropertiesInvertidos();
        $items = [];

        //Añadir beneficios de Marketplace
        $historial = $this->transactions()->whereIn('type', [20, 21])->get();

        $marketOrders = MarketOrder::where('executed', true)
            ->whereIn('id', $historial->pluck('extras')->toArray())
            ->orderByDesc('updated_at')
            ->get();

        foreach ($properties as $property) {
            $beneficios = $this->transactions()->whereIn('type', [13, 14, 18])->where('extras', $property->id)->get()->sum('credit');

            $marketplace = 0;

            if (count($marketOrders) > 0) {
                foreach ($marketOrders as $order) {
                    if ($order->getPropertyId() == $property->id) {
                        if ($order->price != 0) {
                            if ($order->user_id == $this->id) {
                                if ($order->price < 0) {
                                    $marketplace -= ($order->priceMoney - $order->quantity);
                                }
                                $marketplace += ($order->priceMoney - $order->quantity);
                            } else {
                                if ($order->price < 0) {
                                    $marketplace += ($order->priceMoney - $order->quantity);
                                }
                                $marketplace -= ($order->priceMoney - $order->quantity);
                            }
                        }
                    }
                }
            }

            $beneficios += $marketplace;

            $capitalInvertido = PropertyUser::where('property_id', $property->id)->where('user_id', auth()->user()->id)->get()->sum('total');

            if ($property->state->id == 2) {
                if ($property->porcentajeCompletado == 100) {
                    $diasCorrectos = __('Completado');
                } elseif ($property->remainingDays < 0) {
                    $diasCorrectos = __('No conseguido');
                } else {
                    $diasCorrectos = __('Quedan').' '.$property->remainingDays.' '.__('días');
                }
            } else {
                $diasCorrectos = $property->state->nombre;
            }

            $items[$property->id] = [
                'id'                   => $property->id,
                'url'                  => $property->url,
                'invertido'            => formatThousandsNotZero($capitalInvertido),
                'beneficios'           => formatThousandsNotZero($beneficios),
                'nombre'               => $property->nombre,
                'estado'               => $property->state->nombre,
                'estado_id'            => $property->state->id,
                'direccion'            => $property->direccion,
                'porcentajeCompletado' => formatThousandsNotDecimals($property->porcentajeCompletado),
                'objetivo'             => formatThousandsNotDecimals($property->objetivo),
                'fakeOrRealInversores' => $property->fakeOrRealInversores,
                'remainingDays'        => $diasCorrectos,
                'fakeOrRealInvertido'  => formatThousandsNotDecimals($property->fakeOrRealInvertido),
                'imagen'               => $property->getFirstMediaUrl($property->mediaCollection, 'thumb'),
                'tipo'                 => $property->property_state_id,
            ];
        }


        $items = Arr::where($items, function($value,$key) use($exceptions) {
            if (is_array($exceptions))
                return !in_array($value['estado_id'], $exceptions);

            return $value['estado_id'] == $exceptions;
        });

        $items = collect($items)->reject(function ($property) {
            return $property['invertido'] == 0;
        })->sortBy('estado_id');

        return $items;
    }

    /**
     * Obtiene los inmuebles invertidos agrupados para no mostrar repetidos
     *
     * @return  Collection
     */
    public function getListPropertiesInvertidos()
    {
        return $this->properties()->where('property_state_id', '!=', 6)->groupBy('property_id')->get();
    }

    /**
     * Return affiliate url for current user
     *
     * @return string
     */
    public function getAffiliateUrlAttribute()
    {
        return route('referred', $this->affiliate_id);
    }

    /**
     * Return full name of user
     *
     * @return string
     */
    public function getNombreCompletoAttribute()
    {
        return $this->nombre.' '.$this->apellido1.' '.$this->apellido2;
    }

    /**
     * Returns wallet for current user
     *
     * @return  \Infinety\LemonWay\Models\LemonWayWallet
     */
    public function getWalletAttribute()
    {
        return LemonWay::getWalletDetails(null, $this->lemonway_id);
    }

    /**
     * Return Balance
     *
     * @return double
     */
    public function getBalanceAttribute()
    {
        try {
            if ($this->hasLemonAccount()) {
                return $this->wallet->BAL;
            }
        } catch (\Exception $e) {
            return 0;
        }

        return 0;
    }

    /**
     * Return Balance
     *
     * @return double
     */
    public function getBalanceFormattedAttribute()
    {
        return formatThousands($this->wallet->BAL);
    }

    /**
     * Return total invertido
     *
     * @return double
     */
    public function getTotalInvertidoAttribute()
    {
        if ($this->properties->count() > 0) {
            $total = 0;
            foreach ($this->properties as $property) {
                $total += $property->pivot->total;
            }

            return $total;
        }

        return 0;
    }

    /**
     * Return Cards array
     *
     * @return mixed
     */
    public function getCardsAttribute()
    {
        return $this->wallet->CARDS;
    }

    /**
     * Return dni document
     *
     * @return mixed
     */
    public function getDniDocumentAttribute()
    {
        return $this->profile->document;
    }

    /**
     * comrpueba si el usaurio está confirmado
     *
     * @return [type]
     */
    public function getConfirmadoAttribute()
    {
        if ($this->confirm->is_confirmed) {
            return true;
        }

        return false;
    }

    /**
     * Devuelve bool dependiendo si el usuario está activado o no
     *
     * @return bool
     */
    public function getActivadoAttribute()
    {
        if ($this->profile->estado == 1) {
            return true;
        }

        return false;
    }

    /**
     * Devuelve bool dependiendo si el usuario está activado o no
     *
     * @return bool
     */
    public function getActivadoOrInvertidoAttribute()
    {
        if ($this->profile->estado == 1 || (pago_sin_confirmar() && $this->properties->count() > 0)) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getImporteRegaloAttribute()
    {
        $total = 0;
        if ($this->gifts->count() > 0) {
            foreach ($this->gifts as $gift) {
                $total += $gift->importe;
            }
        }

        return $total;
    }

    /**
     * Set password encryption automatically
     *
     * @param string $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomResetPassword($token));
    }

    /**
     * Comprueba si el usuario tiene cuenta en LemonWay
     *
     * @return boolean
     */
    public function hasLemonAccount()
    {
        try {
            $wallet = LemonWay::getWalletDetails(null, $this->lemonway_id);

            return true;
        } catch (LemonWayExceptions $e) {
            return false;
        }
    }

    /**
     * Comprueba si el usuario tiene cuentas bancarias
     *
     * @return boolean
     */
    public function hasValidBankAccounts()
    {
        if ($this->bankAccounts->count() > 0) {
            foreach ($this->bankAccounts as $account) {
                if ($account->estado == 1) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Comprueba si el usuario ha invertido en una propiedad en concreto
     *
     * @param  App\Property $property
     *
     * @return boolean
     */
    public function hasInversion(Property $property)
    {
        $inmueblesIdUser = $this->properties->pluck('id');
        if ($inmueblesIdUser->contains($property->id)) {
            return true;
        }

        return false;
    }

    /**
     * @return mixed
     */
    public function dnisCounts()
    {
        return $this->documents()->where('type', 0)->get()->count();
    }

    /**
     * Sube el dni a lemonway y al sistema de medias y lo añade a la tabla de documentos
     *
     * @param $file
     */
    public function addDniDocument($file)
    {
        $documentLW = LemonWay::uploadFileToWallet($this->wallet, 'dni_'.$this->id.'.jpg', 0, base64_encode($file->stream('jpg')));

        $document = [
            'lemonway_id' => $documentLW->ID,
            'type'        => 0,
            'status'      => 0,
        ];

        $document = $this->documents()->create($document);
        $media = $document->uploadFileStreamToMedia($file, 'dni', true, true, $document->mediaDniCollection);
        $this->profile->update(['document_id' => $document->id]);
    }

    /**
     * Sube extractos bancarios a lemonway y al sistema de medias y lo añade a la tabla de documentos
     *
     * @param $file
     */
    public function addExtractoBancario($file, BankAccount $bankAccount)
    {
        $buffer = File::get($file);
        $documentLW = LemonWay::uploadFileToWallet($this->wallet, 'extracto_'.$this->id.'.jpg', 2, base64_encode($buffer));

        $document = [
            'lemonway_id' => $documentLW->ID,
            'type'        => 2,
            'status'      => 0,
        ];

        $document = $this->documents()->create($document);
        $media = $document->uploadFileToMedia($file, 'extracto', true, true, $document->mediaBankCollection);

        $bankAccount->update(['document_id' => $document->id]);
    }

    /**
     * Añade una trasacción según los datos dados
     *
     * @param $transactionData
     */
    public function addTransaction($transactionData)
    {
        $transaction = [
            'lemonway_id' => $transactionData->id,
            'wallet'      => $this->lemonway_id,
            'type'        => $transactionData->type,
            'status'      => $transactionData->status,
            'fecha'       => $transactionData->getDate()->toDateTimeString(),
            'debit'       => (double) $transactionData->deb,
            'credit'      => (double) $transactionData->cred,
            'iban_id'     => ($transactionData->type == 1) ? 3 : null,
            'comment'     => $transactionData->msg,
            'refund'      => ($transactionData->refund == null) ? 0 : 1,
            'token'       => $transactionData->mtoken,
        ];

        $this->transactions()->create($transaction);
    }

    /**
     * Crea la relación a invertir y realiza la transacción
     *
     * @param Property $property
     * @param $totalInvertir
     */
    public function invertir(Property $property, $totalInvertir, $mensaje = null)
    {
        $totalInvertir = number_format($totalInvertir, 2, '.', '');

        $transactionImporteRegalo = false;

        if ($mensaje == null) {
            $mensaje = __('Inversión en Inmueble').' '.$property->id;
        }

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->lemonway_id,
            'type'        => 2,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => (double) $totalInvertir,
            'credit'      => 0,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'P2P-'),
            'extras'      => $property->id,
        ];

        $transaction = $this->transactions()->create($transaction);

        try {
            $result = LemonWay::walletToWallet($this->wallet, $property->wallet, $totalInvertir, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);

            $transactionImporteRegalo = $this->invertirCheckImporteRegalo($property);

            if ($transactionImporteRegalo != false) {
                $totalInvertidoEmail = $transactionImporteRegalo['importe'] + $totalInvertir;
            } else {
                $totalInvertidoEmail = $totalInvertir;
            }

            $this->notify(new UserInvertirConfirmNotification($property, $totalInvertidoEmail));
        } catch (LemonWayExceptions $e) {
            logger('Línea 536 - '.$e->getMessage());

            $transaction->update(['status' => 4]);

            return false;
        }

        $this->properties()->save($property, ['total' => (double) $totalInvertir, 'transaction_id' => $transaction->id]);

        if ($transactionImporteRegalo != false) {
            $this->properties()->save($property, [
                'total'          => (double) $transactionImporteRegalo['importe'],
                'transaction_id' => $transactionImporteRegalo['transaction_id'],
            ]);
        }

        Event::fire(new UserInvierteInmueble($property));

        // try {
        //     event(new \App\Events\UserInvierteClickId(auth()->user(), $transaction));
        // } catch (\Throwable $th) {
        //     logger($th);
        // }

        return true;
    }

    /**
     * Crea la relación a invertir y realiza la transacción
     *
     * @param Property $property
     * @param $totalInvertir
     */
    public function invertirFromAutoinvest(Property $property, $totalInvertir, $user_id, $test = false)
    {
        $totalInvertir = number_format($totalInvertir, 2, '.', '');

        $transactionImporteRegalo = false;

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->lemonway_id,
            'type'        => 2,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => (double) $totalInvertir,
            'credit'      => 0,
            'iban_id'     => null,
            'comment'     => __('Inversión en Inmueble'). "{$property->id}  - AutoInvest",
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'P2P-'),
            'extras'      => $property->id,
            'autoinvest'  => 'property',
        ];

        $transaction = $this->transactions()->create($transaction);

        $mensaje = __('Inversión en Inmueble').' '.$property->id . ' AutoInvest ';

        try {
            
            if (!$test) {
                $result = LemonWay::walletToWallet($this->wallet, $property->wallet, $totalInvertir, $mensaje);
            
                $transaction->update([
                    'status'      => $result->STATUS,
                    'lemonway_id' => $result->ID
                ]);

                $transactionImporteRegalo = $this->invertirCheckImporteRegalo($property, $user_id);

                if ($transactionImporteRegalo != false) {
                    $totalInvertidoEmail = $transactionImporteRegalo['importe'] + $totalInvertir;
                } else {
                    $totalInvertidoEmail = $totalInvertir;
                }
                
                $this->notify(new UserInvertirConfirmNotification($property, $totalInvertidoEmail));

            } else {
                $transaction->update([
                    'status'      => 3,
                    'lemonway_id' => 0
                ]);
            }
        } catch (LemonWayExceptions $e) {
            logger('Línea 536 - '.$e->getMessage());

            $transaction->update(['status' => 4]);

            return false;
        }

        $this->properties()->save($property, ['total' => (double) $totalInvertir, 'transaction_id' => $transaction->id]);

        if ($transactionImporteRegalo != false) {
            $this->properties()->save($property, [
                'total'          => (double) $transactionImporteRegalo['importe'],
                'transaction_id' => $transactionImporteRegalo['transaction_id'],
            ]);
        }

        if (!$test)
            Event::fire(new UserInvierteInmueble($property));
        
        return $transaction->id;
    }

    /**
     * @param Property $property
     * @return int
     */
    private function invertirCheckImporteRegalo(Property $property, $user_id = null)
    {
        if ($user_id)
            $user = User::find($user_id);
        else 
            $user = auth()->user();

        if ($user->gifts->count() > 0) {
            $importeRegalo = number_format($user->importeRegalo, 2, '.', '');

            sleep(1);

            $transaction = [
                'lemonway_id' => 0,
                'wallet'      => $this->lemonway_id,
                'type'        => 15,
                'status'      => 0,
                'fecha'       => Carbon::now()->toDateTimeString(),
                'debit'       => (double) $importeRegalo,
                'credit'      => 0,
                'iban_id'     => null,
                'comment'     => __('Inversión con Importe Regalo por referido'),
                'refund'      => 0,
                'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'P2P-'),
                'extras'      => $property->id,
            ];

            $transaction = $this->transactions()->create($transaction);

            try {
                $walletSC = LemonWay::getWalletDetails(null, 'SC');
                $result = LemonWay::walletToWallet($walletSC, $property->wallet, $importeRegalo, __('Inversión con Importe Regalo por referido'));

                $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);

                foreach ($user->gifts as $gift) {
                    $gift->executed = 1;
                    $gift->transaction_id = $transaction->id;
                    $gift->save();
                }

                //Creamos la transacción en la DB como si hubiera existido
                $this->crearTransaccionRegaloAlUsuario($transaction);
            } catch (LemonWayExceptions $e) {
                logger('Línea 592 - '.$e->getMessage());

                return false;
            }

            return ['importe' => $importeRegalo, 'transaction_id' => $transaction->id];
        }

        return false;
    }

    /**
     * @param $cantidad
     */
    public function crearTransaccionRegaloAlUsuario(Transaction $transaction)
    {
        try {
            $fecha = Carbon::parse($transaction->fecha)->subSecond();

            $transactionGift = [
                'lemonway_id' => 0,
                'wallet'      => $this->lemonway_id,
                'type'        => 22,
                'status'      => 3,
                'fecha'       => $fecha->toDateTimeString(),
                'debit'       => 0,
                'credit'      => $transaction->debit,
                'iban_id'     => null,
                'comment'     => __('Regalo de Brickstarter'),
                'refund'      => 0,
                'token'       => TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'REG-'),
                'extras'      => $transaction->id,
                'updated_at'  => $transaction->updated_at->subSecond(),
                'created_at'  => $transaction->created_at->subSecond(),
            ];

            $this->transactions()->create($transactionGift);
            sleep(1);
        } catch (\Exception $e) {
            //
        }
    }

    /**
     * Reeturn if is subscribed to the newsletter
     *
     * @return  boolean
     */
    public function isSubscribedToNewsletter()
    {
        return Newsletter::isSubscribed($this->email);
    }

    /**
     * Get the clickId associated with the user.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clickProvider()
    {
        return $this->hasOne(\App\Clickid::class);
    }

    /**
     * Get the clickId associated with the user.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function autoinvest (): HasOne
    {
        return $this->hasOne(\App\AutoInvest::class);
    }
}
