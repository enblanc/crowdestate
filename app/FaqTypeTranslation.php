<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaqTypeTranslation extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['faq_type_id', 'nombre', 'locale'];
}
