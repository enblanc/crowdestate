<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AutoInvestsEvents extends Model
{
    /**
     * @var string
     */
    protected $table = 'autoinvests_events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'description',
        'type',
        'evaluated',
        'parameters',
        'logs'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'parameters' => 'array',
        'logs'       => 'array'
    ];

    /**
     * Is property
     *
     *
     * @return boolean
     */
    public function getIsPropertyAttribute()
    {
        return $this->type == 'property';
    }

    /**
     * Return evolutions for current Orders
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders ()
    {
        return $this->hasMany(AutoinvestsOrders::class, 'autoinvests_event_id');
    }

}
