<?php

namespace App;

use App\Transformers\MediaTransformer;
use Flugg\Responder\Contracts\Transformable;
use Spatie\MediaLibrary\Media as BaseMedia;

class Media extends BaseMedia implements Transformable
{

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    // protected $hidden = ['model'];

    /**
     * The transformer used to transform the model data.
     *
     * @return Transformer|callable|string|null
     */
    public static function transformer()
    {
        return MediaTransformer::class;
    }
}
