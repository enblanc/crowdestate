<?php

namespace App;

use App\Property;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class MailNotifications extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['type', 'property_id'];

    /**
     * Devuelve el inmueble
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function properties(): BelongsTo
    {
        return $this->belongsTo(Property::class);
    }
}
