<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ReferralGift extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'referral_id',  'user_amount', 'referral_amount', 'property', 'executed',
    ];

    /**
     * User of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Referr of the gift
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function referrer()
    {
        return $this->belongsTo(User::class, 'referral_id');
    }
}
