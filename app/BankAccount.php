<?php

namespace App;

use App\Document;
use App\Traits\Searchable;
use App\User;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends Model
{
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'titular', 'iban', 'bic_swift', 'pais', 'estado', 'document_id', 'lemonway_id',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'estado' => 'boolean',
    ];

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * User of the profile
     *
     * @return Illuminate\Database\Eloquent\Concerns\belongsTo
     */
    public function document()
    {
        return $this->belongsTo(Document::class);
    }

    /**
     * Set fields be able to search
     *
     * @return array
     */
    public function searchableFields(): array
    {
        return ['titular', 'iban', 'bic_swift', 'pais'];
    }
}
