<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Helpers\TokensHelper;
use App\Notifications\MarketPlaceOrderBought;
use App\Notifications\MarketPlaceOrderSold;
use App\Property;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Infinety\LemonWay\Exceptions\LemonWayExceptions;
use Infinety\LemonWay\Facades\LemonWay;
use App\AutoInvest;
use App\MarketOrder;
use App\Helpers\AutoInvestHelper;

class AutoInvestMarketOrderJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     * @var MarketOrder
     */
    protected $order;

    /**
     *
     * @var AutoInvestHelper
     */
    protected $autoinvestEvent;

    /**
     *
     * @var boolean
     */
    protected $test;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MarketOrder $order, $test = false)
    {
        $this->order = $order;

        $this->test  = $test;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->autoinvestEvent = new AutoInvestHelper();

        $this->autoinvestEvent->setLog('Lanzamiento del Autoinvests-MarketplaceOrder');

        $this->autoinvestEvent->setDescription("Lanzamiento del Autoinvest-MarketplaceOrder - {$this->order->inversion->property->nombre}({$this->order->id})");

        $this->autoinvestEvent->setTypeMarketplace();

        $this->autoinvestEvent->setParameters([
            'price'                     => $this->order->priceMoney,
            'property_id'               => $this->order->inversion->property->id,
            'property_type_id'          => $this->order->inversion->property->property_type_id,
            'developer_id'              => $this->order->inversion->property->developer_id,
            'ubicacion'                 => $this->order->inversion->property->ubicacion,
            'tasa_interna_rentabilidad' => $this->order->inversion->property->tasa_interna_rentabilidad,
        ]);

        $this->autoinvestEvent->setLog('Obteniendo los AutoInvests que cumpla con los parametros de la Orden');

        $autoinvests = AutoInvest::where('user_id', '!=', $this->order->user_id)
                        ->byDeveloper($this->order->inversion->property->developer_id)
                        ->byLocation($this->order->inversion->property->ubicacion)
                        ->byPropertyType($this->order->inversion->property->property_type_id)
                        ->where('min_investment', '<=', $this->order->priceMoney)
                        ->where('max_investment', '>=', $this->order->priceMoney)
                        ->byTir($this->order->inversion->property->tasa_interna_rentabilidad - $this->order->price)
                        ->whereDoesntHave('orders', function ($query) {
                            return $query->where('property_id', $this->order->inversion->property->id);
                        })
                        ->canInvert()
                        ->get();

        $this->autoinvestEvent->setEvaluated(count($autoinvests));

        $this->autoinvestEvent->save();

        if ($this->verifyOrder()) {

            foreach ($autoinvests as $key => $autoinvest) {
                $this->refreshModel();

                if (!$this->verifyOrder()) {
                    return 0;
                }

                if (($autoinvest->investment + $this->order->priceMoney) <= $autoinvest->portfolio_size) {
                    $this->autoinvestEvent->setLog([
                        1 => "Verificando el AutoInvest ({$autoinvest->id}) del usuario: {$autoinvest->user->email} ({$autoinvest->user->id})"
                    ]);

                    DB::beginTransaction();
                    $oldInversion = $this->order->inversion;
                    $oldInversion->total = $oldInversion->total - $this->order->quantity;
                    $oldInversion->save();
                    $property     = $this->order->inversion->property;
                    $success      = $this->sendMoneyBetweenUsers($autoinvest->user, $this->order->user, $this->order, $property);
                    if ($success) {
                        
                        if (!$this->test) {
                            $this->order->execute();

                            $autoinvest->user->notify(new MarketPlaceOrderBought($this->order));

                            $appLocale = current_locale();

                            localization()->setLocale($this->order->user->locale);

                            $this->order->user->notify(new MarketPlaceOrderSold($this->order));

                            localization()->setLocale($appLocale);
                        }

                        DB::commit();

                        $this->autoinvestEvent->makeOrder($autoinvest->id, $this->order->priceMoney, $this->order->id, $this->order->inversion->property->id, $success);

                        $this->autoinvestEvent->setLog([
                            3 => "El AutoInvest ({$autoinvest->id}) del usuario: {$autoinvest->user->email} ({$autoinvest->user->id}) invirtió ". number_format($this->order->priceMoney, 2, '.', '')
                        ]);
                        
                        $this->autoinvestEvent->setLog([
                            1 => "Finalizó el proceso del AutoInvests para la Orden #{$this->order->id}"
                        ]);
                
                        $this->autoinvestEvent->save();

                        return true;
                    } else {
                        DB::rollBack();
                    }
                }
            }

            $this->autoinvestEvent->setLog("Finalizó la revisión de los AutoInvests Orden #{$this->order->id}");
        }

        $this->autoinvestEvent->save();
    }

    private function refreshModel () {
        $this->order = MarketOrder::find($this->order->id);
    }

    /**
     * Verificamos que la orden se encuentre disponible
     */
    private function verifyOrder()
    {
        if (!$this->order->status) {
            $this->autoinvestEvent->setLog([
                2 => "El estado de la orden ({$this->order->id}) no es correcta para continuar."
            ]);
            $this->autoinvestEvent->save();
            return false;
        }

        if ($this->order->executed) {
            $this->autoinvestEvent->setLog([
                2 => "La orden ({$this->order->id}) ya se encuentra ejecutada."
            ]);
            $this->autoinvestEvent->save();
            return false;
        }

        if ($this->order->type != 1) {
            $this->autoinvestEvent->setLog([
                2 => "La orden ({$this->order->id}) es de tipo (1) compra."
            ]);
            $this->autoinvestEvent->save();
            return false;
        }

        return true;
    }

    /**
     * @param $sender
     * @param $reciever
     * @param $money
     */
    private function sendMoneyBetweenUsers(User $sender, User $reciever, MarketOrder $order, Property $property)
    {
        $totalMoney = number_format($order->priceMoney, 2, '.', '');

        $mensaje = __('Marketplace order #').$order->id . ' - AutoInvest Marketplace';

        try {
            $transactionSender   = $this->generateSenderTransaction($sender, $order, $totalMoney, $mensaje);
            $transactionReciever = $this->generateRecieverTransaction($reciever, $order, $totalMoney, $mensaje);
        } catch (\Exception $e) {
            logger($e);
            $this->autoinvestEvent->setLog([
                2 => "Error generar las Transacciones"
            ]);
            $this->autoinvestEvent->setLog([
                2 => $e->getMessage()
            ]);
            return false;
        }

        try {
            if (!$this->test) {
                $result = LemonWay::walletToWallet($sender->wallet, $reciever->wallet, $totalMoney, $mensaje);
                $transactionSender->update(['status'   => $result->STATUS, 'lemonway_id' => $result->ID]);
                $transactionReciever->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
            }

            $sender->properties()->save($property, [
                'total'          => (double) $order->quantity,
                'transaction_id' => $transactionReciever->id,
            ]);

            $order->setTransaction($transactionReciever->id);

            return $transactionSender->id;
        } catch (LemonWayExceptions $e) {
            $transactionSender->update(['status' => 4]);

            $transactionReciever->update(['status' => 4]);

            $this->autoinvestEvent->setLog([
                2 => "Error en la transacción de LemonWay"
            ]);

            $this->autoinvestEvent->setLog([
                2 => $e->getMessage()
            ]);

            return false;
        }
    }

    /**
     * Generate transaction for reciever
     *
     * @param   \App\User         $reciever
     * @param   \App\MarketOrder  $order
     * @param   double       $total
     * @param   string       $message
     *
     * @return  \App\Transaction
     */
    private function generateSenderTransaction(User $sender, MarketOrder $order, $total, $message)
    {
        $transaction = [
            'lemonway_id'      => 0,
            'wallet'           => $sender->lemonway_id,
            'type'             => 20,
            'status'           => 0,
            'fecha'            => Carbon::now()->toDateTimeString(),
            'debit'            => 0,
            'credit'           => $total,
            'iban_id'          => null,
            'comment'          => $message,
            'refund'           => 0,
            'token'            => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'MKO-'),
            'extras'           => $order->id,
            'property_user_id' => $order->getPropertyId(),
            'autoinvest'       => 'marketplace'
        ];

        return $sender->transactions()->create($transaction);
    }

    /**
     * Generate transaction for reciever
     *
     * @param   \App\User         $reciever
     * @param   \App\MarketOrder  $order
     * @param   double       $total
     * @param   string       $message
     *
     * @return  \App\Transaction
     */
    private function generateRecieverTransaction(User $reciever, MarketOrder $order, $total, $message)
    {
        $transaction = [
            'lemonway_id'      => 0,
            'wallet'           => $reciever->lemonway_id,
            'type'             => 21,
            'status'           => 0,
            'fecha'            => Carbon::now()->toDateTimeString(),
            'debit'            => $total,
            'credit'           => 0,
            'iban_id'          => null,
            'comment'          => $message,
            'refund'           => 0,
            'token'            => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'MKO-'),
            'extras'           => $order->id,
            'property_user_id' => $order->getPropertyId(),
            'autoinvest'       => 'marketplace'
        ];

        return $reciever->transactions()->create($transaction);
    }
}
