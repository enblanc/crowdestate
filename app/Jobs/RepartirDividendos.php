<?php

namespace App\Jobs;

use App\Helpers\TokensHelper;
use App\Notifications\UserDividendosNotification;
use App\Notifications\UserDividendosNoTributeNotification;
use App\Property;
use App\PropertyEvolutions;
use App\PropertyUser;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Infinety\LemonWay\Facades\LemonWay;

class RepartirDividendos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Lista de meses
     *
     * @var array
     */
    protected $months = [
        1  => 'Enero',
        2  => 'Febrero',
        3  => 'Marzo',
        4  => 'Abril',
        5  => 'Mayo',
        6  => 'Junio',
        7  => 'Julio',
        8  => 'Agosto',
        9  => 'Septiembre',
        10 => 'Octubre',
        11 => 'Noviembre',
        12 => 'Diciembre',
    ];

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $user;

    /**
     * @var mixed
     */
    protected $evolucion;

    /**
     * @var mixed
     */
    protected $cantidadInvertida;

    /**
     * @var mixed
     */
    protected $amountAbonarTotal;

    /**
     * @var mixed
     */
    protected $mes;

    /**
     * @var mixed
     */
    protected $year;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Property $property, User $user, PropertyEvolutions $evolucion, $amountAbonarTotal, $mes, $year)
    {
        $this->property = $property;
        $this->user = $user;
        $this->evolucion = $evolucion;
        $this->amountAbonarTotal = $amountAbonarTotal;
        $this->mes = $mes;
        $this->year = $year;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        logger('DIVIDENDO => USER_ID:'.$this->user->id);
        $locale = $this->user->locale;
        if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
            localization()->setLocale($locale);
        }

        $inversiones = PropertyUser::where('property_id', $this->property->id)->where('user_id', $this->user->id)->get();
        $cantidadAbonarUsuario = 0;
        foreach ($inversiones as $inversion) {
            // Explicación : (cantidadInvertida * totalAbonarDelMes) / Objetivo inicial = $cantidad de € a abonar
            try {
                // Problema con la suba, ya que tota viene con un formato string. Se debe solucionar.
                $amountInversion = round(($inversion->total * $this->amountAbonarTotal) / $this->property->objetivo, 2);
            } catch (\Throwable $th) {
                $amountInversion = round(((double)$inversion->total * $this->amountAbonarTotal) / $this->property->objetivo, 2);
            }
            
            $cantidadAbonarUsuario += $amountInversion;
        }

        //Check if user tributes to spain
        if ($this->user->tribute) {
            $cantidadAbonarUsuario = round($cantidadAbonarUsuario * 0.81, 2);
            logger($this->user->nombreCompleto.': '.$cantidadAbonarUsuario);
            // VALIDACION:DIVIDENDOS
            if ($cantidadAbonarUsuario > 0) {
                $transaction = $this->generarTransaccion($cantidadAbonarUsuario);
            } else {
                $transaction = false;
            }
        } else {
            logger($this->user->nombreCompleto.': '.$cantidadAbonarUsuario);
            // VALIDACION:DIVIDENDOS
            if ($cantidadAbonarUsuario > 0) {
                $transaction = $this->generarTransaccionNoTribute($cantidadAbonarUsuario);
            } else {
                $transaction = false;
            }
        }

        // //Para Pruebas
        // $transaction = $this->generarTransaccionTests($cantidadAbonarUsuario);

        if ($transaction != false) {
            //Notificamos al usuario
            try {
                $appLocale = current_locale();
                localization()->setLocale($this->user->locale);

                if ($this->user->tribute) {
                    $this->user->notify(new UserDividendosNotification($this->property, $cantidadAbonarUsuario, $this->months[$this->mes]));
                } else {
                    $this->user->notify(new UserDividendosNoTributeNotification($this->property, $cantidadAbonarUsuario, $this->months[$this->mes]));
                }

                localization()->setLocale($appLocale);
            } catch (\Exception $e) {
                logger('Ha fallado enviar la notificación a la transacción: '.$transaction->id);

                return true;
            }
        }
    }

    /**
     * @param $cantidadAbonarUsuario
     */
    private function generarTransaccion($cantidadAbonarUsuario)
    {
        $cantidadAbonarUsuario = number_format($cantidadAbonarUsuario, 2, '.', '');

        //Devolvemos false si la cantidad a enviar es de 0 €
        if ($cantidadAbonarUsuario == 0) {
            return false;
        }

        $fechaMes = Carbon::now();
        $fechaMes->month = $this->mes;
        $fechaMes->year = $this->year;

        $mensaje = __('Dividendos').' '.$this->property->nombre.' - '.$this->months[$this->mes].' '.$this->year;

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->user->lemonway_id,
            'type'        => 13,
            'status'      => 0,
            'fecha'       => $fechaMes->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidadAbonarUsuario,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'DIV-'),
            'extras'      => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $result = LemonWay::walletToWallet($walletSC, $this->user->wallet, $cantidadAbonarUsuario, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }

        return $transaction;
    }

    /**
     * @param $cantidadAbonarUsuario
     * @return mixed
     */
    private function generarTransaccionNoTribute($cantidadAbonarUsuario)
    {
        $cantidadAbonarUsuario = number_format($cantidadAbonarUsuario, 2, '.', '');

        //Devolvemos false si la cantidad a enviar es de 0 €
        if ($cantidadAbonarUsuario == 0) {
            return false;
        }

        $fechaMes = Carbon::now();
        $fechaMes->month = $this->mes;
        $fechaMes->year = $this->year;

        $mensaje = __('Dividendos').' '.$this->property->nombre.' - '.$this->months[$this->mes].' '.$this->year;

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->user->lemonway_id,
            'type'        => 13,
            'status'      => 0,
            'fecha'       => $fechaMes->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidadAbonarUsuario,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'DIV-'),
            'extras'      => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            //first Move to Poland Wallet
            $mensajeExternal = __('Dividendos').' - UserId: '.$this->user->id.' '.$this->property->nombre.' - '.$this->months[$this->mes].' '.$this->year;

            $walletExternal = LemonWay::getWalletDetails(null, env('NO_TRIBUTE_WALLET'));
            $resultExternal = LemonWay::walletToWallet($walletSC, $walletExternal, $cantidadAbonarUsuario, $mensajeExternal);

            if ($resultExternal->STATUS == 3) {
                $result = LemonWay::walletToWallet($walletExternal, $this->user->wallet, $cantidadAbonarUsuario, $mensaje);
                $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
            } else {
                logger('Error al pasar dinero a la cuenta extranjera');
                $transaction->update(['status' => 4]);

                return false;
            }
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }

        return $transaction;
    }

    /**
     * Función para pruebas
     *
     * @param $cantidadAbonarUsuario
     * @return mixed
     */
    private function generarTransaccionTests($cantidadAbonarUsuario)
    {
        $cantidadAbonarUsuario = number_format($cantidadAbonarUsuario, 2, '.', '');

        $fechaMes = Carbon::now();
        $fechaMes->month = $this->mes;
        $fechaMes->month = $this->year;

        $mensaje = 'Dividendos '.$this->property->nombre.' - '.$this->months[$this->mes].' '.$this->year;

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->user->lemonway_id,
            'type'        => 13,
            'status'      => 0,
            'fecha'       => $fechaMes->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidadAbonarUsuario,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'DIV-'),
            'extras'      => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);
        $transaction->update(['status' => 3, 'lemonway_id' => '000001']);

        return $transaction;
    }

    /**
     * Devuelve la propiedad
     *
     * @return App\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Devuelve el usuario
     *
     * @return App\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getEvolucion()
    {
        return $this->evolucion;
    }

    /**
     * @return mixed
     */
    public function getMes()
    {
        return $this->mes;
    }

    /**
     * @return mixed
     */
    public function getYear()
    {
        return $this->year;
    }
}
