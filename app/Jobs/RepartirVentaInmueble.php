<?php

namespace App\Jobs;

use App\Helpers\TokensHelper;
use App\Property;
use App\PropertyUser;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Infinety\LemonWay\Facades\LemonWay;


class RepartirVentaInmueble implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $user;

    /**
     * @var mixed
     */
    protected $evolucion;

    /**
     * @var mixed
     */
    protected $totalRepartir;

    /**
     * @var array
     */
    protected $noLemonwayUsers = [
        122, 125,
    ];

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Property $property, User $user, $evolucion, $totalRepartir)
    {
        $this->property = $property;
        $this->user = $user;
        $this->evolucion = $evolucion;
        $this->totalRepartir = $totalRepartir;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $locale = $this->user->locale;
        if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
            localization()->setLocale($locale);
        }

        $inversiones = PropertyUser::where('property_id', $this->property->id)->where('user_id', $this->user->id)->get();

        $cantidadAbonarUsuario = 0;
        $cantidadInvertidaDelUsuario = 0;
        foreach ($inversiones as $inversion) {
            // Explicación : (cantidadInvertida * totalAbonarDelMes) / Objetivo inicial = $cantidad de € a abonar
            $cantidadInvertidaDelUsuario += $inversion->total;
        }

        //Cantidad a abonar al usuario = (La cantidad que invirtió * el total a invertir) / el objetivo inicial
        $cantidadaPorcentaje = ($cantidadInvertidaDelUsuario * $this->totalRepartir) / $this->property->objetivo;

        //Se le devuelve al usuario lo que pagó, más la cantidad que lo toca (Esto se desabilitó a partir del inmueble id 19)
        // $cantidadAbonarUsuario = $cantidadInvertidaDelUsuario + $cantidadaPorcentaje;
        $ganado = $cantidadaPorcentaje - $cantidadInvertidaDelUsuario;

        $cantidadAbonarUsuario = $cantidadaPorcentaje;

        if ($this->user->tribute) {
            $ganado = $ganado * 0.81;
        }

        $cantidadAbonarUsuario = $cantidadInvertidaDelUsuario + $ganado;

        //Check if user tributes to spain
        if ($this->user->tribute) {
            logger($this->user->id.' - '.$this->user->nombre.': Hay que pagarle '.$cantidadAbonarUsuario.'€ por la venta del inmueble: '.$this->property->nombre);
            // VALIDACION:DIVIDENDOS
            if ($cantidadAbonarUsuario > 0) {
                $transaction = $this->pagarAlUsuario($cantidadAbonarUsuario);
            } else {
                logger('NO SE LANZO');
            }
        } else {
            logger($this->user->id.' - '.$this->user->nombre.': Hay que pagarle '.$cantidadAbonarUsuario.'€ por la venta del inmueble: '.$this->property->nombre);
            // VALIDACION:DIVIDENDOS
            if ($cantidadAbonarUsuario > 0) {
                $transaction = $this->pagarAlUsuarioNoTribute($cantidadAbonarUsuario);
            } else {
                logger('NO SE LANZO');
            }
        }

        return true;

        // if ($transaction != false) {
        //     //Notificamos al usuario
        //     try {
        //         $appLocale = current_locale();
        //         localization()->setLocale($this->user->locale);

        //         $this->user->notify(new DividendosVentaNotificacion($this->property, $cantidadAbonarUsuario));

        //         localization()->setLocale($appLocale);

        //         return true;
        //     } catch (\Exception $e) {
        //         logger('Ha fallado enviar la notificación a la transacción: '.$transaction->id);

        //         return true;
        //     }
        // }
    }

    /**
     * @param $cantidad
     */
    private function pagarAlUsuario($cantidad)
    {
        $mensaje = __('Diviendos generados de la venta del inmueble:').' '.$this->property->nombre;

        $cantidad = number_format($cantidad, 2, '.', '');

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->user->lemonway_id,
            'type'        => 14,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidad,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'VEN-'),
            'extras'      => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $result = LemonWay::walletToWallet($walletSC, $this->user->wallet, $cantidad, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }
        // }

        return true;
    }

    /**
     * @param $cantidad
     */
    private function pagarAlUsuarioNoTribute($cantidad)
    {
        $mensaje = __('Diviendos generados de la venta del inmueble:').' '.$this->property->nombre;

        $cantidad = number_format($cantidad, 2, '.', '');

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->user->lemonway_id,
            'type'        => 14,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidad,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'VEN-'),
            'extras'      => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $mensajeExternal = __('Diviendos generados de la venta del inmueble:').' '.$this->property->nombre.' - UserId: '.$this->user->id;

            $walletExternal = LemonWay::getWalletDetails(null, env('NO_TRIBUTE_WALLET'));
            $resultExternal = LemonWay::walletToWallet($walletSC, $walletExternal, $cantidad, $mensajeExternal);

            if ($resultExternal->STATUS == 3) {
                $result = LemonWay::walletToWallet($walletExternal, $this->user->wallet, $cantidad, $mensaje);
                $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
            } else {
                logger('Error al pasar dinero a la cuenta extranjera');
                $transaction->update(['status' => 4]);

                return false;
            }
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }
        // }

        return true;
    }

    /**
     * Devuelve la propiedad
     *
     * @return App\Property
     */
    public function getProperty()
    {
        return $this->property;
    }

    /**
     * Devuelve el usuario
     *
     * @return App\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return mixed
     */
    public function getEvolucion()
    {
        return $this->evolucion;
    }

    /**
     * Check if the payment should send by lemonway or not
     *
     * @return  boolean
     */
    public function userNotLemonway()
    {
        if (in_array($this->user->id, $this->noLemonwayUsers)) {
            return true;
        }

        return false;
    }
}
