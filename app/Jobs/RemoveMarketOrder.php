<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use App\Property;
use App\MarketOrder;

class RemoveMarketOrder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     * @var Property
     */
    protected $property;

    /**
     *
     * @var boolean
     */
    protected $delete = true;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Property $property, $delete = true)
    {
        $this->property = $property;

        $this->delete = $delete;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            // Cambio de estado.
            $marketOrders = $this->getMarketOrder($this->property->id);
            $marketOrders->update(['status' => false]);

            // Eliminación de las ordenes si la opción $delete es verdadera.
            if ($this->delete) {
                $marketOrders = $this->getMarketOrder($this->property->id);
                $marketOrders->delete();
            }

        } catch (\Throwable $th) {
            logger('ERROR:PAUSA:MARKETPLACE_PROPERTY');
            logger($th->getMessage());
        }
    }

    /**
     *
     * @return collect()
     */
    protected function getMarketOrder ($property_id)
    {
        return MarketOrder::where('property_id', $property_id)
                ->orWhereHas('inversion', function ($q) use ($property_id) {
                    $q->whereHas('property', function ($q) use ($property_id) {
                        $q->where('id', $property_id);
                    });
                })
                ->where('executed', false);
    }
}
