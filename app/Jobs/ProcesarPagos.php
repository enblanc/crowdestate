<?php

namespace App\Jobs;

use App\Helpers\TokensHelper;
use App\Payment;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Infinety\LemonWay\Facades\LemonWay;

class ProcesarPagos implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var mixed
     */
    protected $payment;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->pagarAlUsuario($this->payment);
    }

    /**
     * @param \App\Payment $payment
     */
    private function pagarAlUsuario(Payment $payment)
    {
        $cantidad = number_format($payment->price, 2, '.', '');

        $transaction = [
            'wallet'  => $payment->user->lemonway_id,
            'type'    => $payment->type,
            'status'  => 0,
            'fecha'   => Carbon::now()->toDateTimeString(),
            'debit'   => 0,
            'credit'  => $cantidad,
            'iban_id' => null,
            'comment' => $payment->subject,
            'refund'  => 0,
            'token'   => TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'PAY-'),
        ];

        $transaction = $payment->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $result = LemonWay::walletToWallet($walletSC, $payment->user->wallet, $cantidad, $payment->subject);

            if ($result->STATUS == 3) {
                $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
                $payment->update(['status' => 1, 'transaction_id' => $transaction->id]);
            } else {
                logger('Error al pasar dinero al procesar el pago');
                $transaction->update(['status' => 4]);
                $payment->update(['status' => 4]);
            }
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);
            $payment->update(['status' => 4]);
            logger($e->getMessage());

            return false;
        }

        return true;
    }
}
