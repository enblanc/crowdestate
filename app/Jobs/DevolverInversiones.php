<?php

namespace App\Jobs;

use App\Helpers\TokensHelper;
use App\Property;
use App\PropertyUser;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Infinety\LemonWay\Facades\LemonWay;

class DevolverInversiones implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var mixed
     */
    protected $property;

    /**
     * @var mixed
     */
    protected $user;

    /**
     * @var mixed
     */
    protected $relacion;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Property $property, User $user, PropertyUser $relacion)
    {
        $this->property = $property;
        $this->user = $user;
        $this->relacion = $relacion;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $locale = $this->user->locale;
        if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
            localization()->setLocale($locale);
        }

        //Sacamos la transacción asociada
        $transaction = Transaction::find($this->relacion->transaction_id);
        $regalo = false;

        // Si la transaccion es un regalo se le hace de nuevo el regalo
        if ($transaction->type == 15) {
            $this->user->gifts()->create(['importe' => $transaction->debit]);
            $relacion = $this->relacion;
            $relacion->abonado = 2;
            $relacion->total_abonado = 1;
            $relacion->save();

            return true;
        }

        $cantidadInvertidaRembolsar = $this->relacion->total;

        // if ($movimientoRegalo) {
        //     $cantidadInvertidaRembolsar = $cantidadInvertidaRembolsar - $movimientoRegalo->credit;
        // }

        $cantidadInvertidaRembolsar = number_format($cantidadInvertidaRembolsar, 2, '.', '');

        $mensaje = __('Devolución de inversión por inmueble no conseguido:').' '.$this->property->nombre;

        $transaction = [
            'lemonway_id' => 0,
            'wallet'      => $this->user->lemonway_id,
            'type'        => 16,
            'status'      => 0,
            'fecha'       => Carbon::now()->toDateTimeString(),
            'debit'       => 0,
            'credit'      => $cantidadInvertidaRembolsar,
            'iban_id'     => null,
            'comment'     => $mensaje,
            'refund'      => 0,
            'token'       => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'REM-'),
            'extras'      => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        try {
            $result = LemonWay::walletToWallet($this->property->wallet, $this->user->wallet, $cantidadInvertidaRembolsar, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);

            // Si el usuario tenía un regalo, se lo volvemos a generar
            // if ($movimientoRegalo) {
            //     $this->user->gifts()->create(['importe' => $movimientoRegalo->credit]);
            // }

            //Actualizamos los datos relacionados
            $relacion = $this->relacion;
            $relacion->abonado = 2;
            $relacion->total_abonado = 1;
            $relacion->transaction_id = $transaction->id;
            $relacion->save();
        } catch (Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }

        try {
            $appLocale = current_locale();
            localization()->setLocale($this->user->locale);

            $this->user->notify(new InmuebleNoConseguidoNotification($this->property, $cantidadInvertidaRembolsar));
            localization()->setLocale($appLocale);
        } catch (Exception $e) {
            logger('Error al enviar la notificación');
        }

        return true;
    }
}
