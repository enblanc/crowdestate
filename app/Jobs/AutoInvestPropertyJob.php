<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;
use App\Helpers\AutoInvestHelper;
use App\AutoInvest;
use App\AutoinvestsOrders;
use App\Property;

/**
 * Se lanza cuando se actualiza o crea un inmueble
 * Recorré todo los AutoInvest de cada usuario
 */
class AutoInvestPropertyJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     *
     * @var AutoInvest
     */
    protected $autoinvests;

    /**
     *
     * @var Property
     */
    protected $property;

    /**
     *
     * @var AutoInvestHelper
     */
    protected $autoinvestEvent;

    /**
     *
     * @var boolean
     */
    protected $test;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($property, $test = false)
    {
        $this->property = $property;

        $this->test     = $test;

        $this->autoinvestEvent = new AutoInvestHelper();
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->autoinvestEvent->setLog([
            0 => 'Lanzamiento del Autoinvests-propiedad'
        ]);

        try {

            $this->autoinvestEvent->setDescription("Lanzamiento del AutoInvest - Propiedad | {$this->property->nombre}({$this->property->id})");
    
            $this->autoinvestEvent->setTypeProperty();
    
            $this->autoinvests = AutoInvest::canInvert()
                                        ->byPropertyType($this->property->property_type_id)
                                        ->byDeveloper($this->property->developer_id)
                                        ->byLocation($this->property->ubicacion)
                                        ->byTir($this->property->tasa_interna_rentabilidad)
                                        ->whereDoesntHave('orders', function ($query) {
                                            return $query->where('property_id', $this->property->id);
                                        })
                                        ->get();
    
            if (!$this->checkProperty() && !$this->test) {

                $this->autoinvestEvent->setLog([
                    2 => "La propiedad ({$this->property->id}) no cumple con los requisitos."
                ]);

                $this->autoinvestEvent->save();
                
                return;
            }
    
            $this->autoinvestEvent->setLog([
                1 => "La propiedad ({$this->property->id}) cumple con los requisitos."
            ]);
    
            $this->autoinvestEvent->setEvaluated($this->autoinvests->count());
    
            $this->autoinvestEvent->setParameters([
                'importe_minimo'            => number_format($this->property->importe_minimo, 2, '.', ''),
                'property_id'               => $this->property->id,
                'property_type_id'          => $this->property->property_type_id,
                'developer_id'              => $this->property->developer_id,
                'ubicacion'                 => $this->property->ubicacion,
                'tasa_interna_rentabilidad' => $this->property->tasa_interna_rentabilidad,
            ]);
    
            // Recorre todo los AutoInvest activos de los usuario
            if ($this->autoinvests->count()) {
                $this->autoinvestEvent->setLog([
                    0 => "Recorriendo la cantidad de ({$this->autoinvests->count()}) AutoInvests para comprobar que puede invertir en la propiedad."
                ]);
            } else {
                $this->autoinvestEvent->setLog([
                    2 => "No existe ningún AutoInvest que cumpla con los parámetros de la Propiedad ({$this->property->id})"
                ]);
            }
    
            $this->autoinvestEvent->save();
    
            foreach ($this->autoinvests as $autoinvest) { 
                if (($this->checkUser($autoinvest->user) && $inversion = $this->checkAutoInvest($autoinvest)) || $this->test) {
                    $this->invertir($this->property, $autoinvest, $inversion);
                }
            }
    
            $this->autoinvestEvent->setLog([
                1 => "Finalizó el proceso del AutoInvests para la propiedad {$this->property->nombre}"
            ]);
    
        } catch (\Throwable $th) {
            $this->autoinvestEvent->setLog([2 => $th->getMessage()]);
        }

        $this->autoinvestEvent->save();

        return true;
    }

    /**
     * Verificamos que el autoinvest del usuario cumple con la propiedad
     *
     * @param AutoInvest $autoinvest
     * @return boolean
     */
    public function checkAutoInvest ($autoinvest)
    {
        //max_investment
        $this->refreshProperty();

        $invertidoTotal = ($this->property->invertido + $autoinvest->max_investment);
        if ($invertidoTotal <= $this->property->objetivo) {
            if ($autoinvest->portfolio_size >= $autoinvest->investment + $autoinvest->max_investment)
                return $autoinvest->max_investment;
        }

        // min_investment
        $invertidoTotal = ($this->property->invertido + $autoinvest->min_investment);
        if ($invertidoTotal <= $this->property->objetivo) {
            if ($autoinvest->portfolio_size >= $autoinvest->investment + $autoinvest->min_investment)
                return $autoinvest->min_investment;
        }

        return false;
    }

    /**
     * Verificamos que la propiedad cumpla con los estados requeridos
     * @return boolean
     */
    public function checkProperty ()
    {
        if (!$this->property->publicado) {
            $this->autoinvestEvent->setLog([2 => 'La propiedad no se encuentra publicada.']);
            return false;
        }

        if (!$this->property->estado) {
            $this->autoinvestEvent->setLog([2 => 'La propiedad se encuentra oculta.']);
            return false;
        }
        
        if ($this->property->private) {
            $this->autoinvestEvent->setLog([2 => 'La propiedad se encuentra privada']);
            return false;
        }

        if ($this->property->property_state_id != 2) {
            $this->autoinvestEvent->setLog([2 => 'El estado de la propiedad es incorecto. Debe estar en "En Explotación"']);
            return false;
        }

        if ($this->property->porcentajeCompletado == 100) {
            $this->autoinvestEvent->setLog([2 => 'El porcentaje ya alzancó el 100%']);
            return false;
        }

        if ($this->property->remainingDays < 1) {
            $this->autoinvestEvent->setLog([2 => 'La fecha de finalización ya caducó (Días restantes < 1).']);
            return false;
        }
        
        return true;
    }

    /**
     * Verifica que el usuario cumpla con los requisitos de su perfil.
     *
     * @param User $user
     * @return void
     */
    private function checkUser ($user) {
        
        if ($this->test)
            return true;

        if ($user->confirmado && $user->activado && $user->hasLemonAccount()) {
            return true;
        }

        if (!$user->activado && pago_sin_confirmar() && $user->hasLemonAccount()) {
            return true;
        }

        return false;
    }

    /**
     * Proceso para invertir en el inmueble
     * 
     * @param $property   Property
     * @param $autoinvest AutoInvest
     * @param $inversion  Float
     */
    private function invertir ($property, $autoinvest, $investment)
    {
        try {
            if ($transaction_id = $autoinvest->user->invertirFromAutoinvest($property, $investment, $autoinvest->user->id, $this->test)) {
                
                $this->autoinvestEvent->makeOrder($autoinvest->id, $investment, NULL, $property->id, $transaction_id);

                $this->autoinvestEvent->setLog([
                    3 => "El AutoInvest ({$autoinvest->id}) del usuario: {$autoinvest->user->email} ({$autoinvest->user->id}) invirtió ". number_format($investment, 2, '.', '')
                ]);
            }
        } catch (\Throwable $th) {
            $this->autoinvestEvent->setLog([
                2 => "Error - Autoinvest ({$autoinvest->id}) del usuario: {$autoinvest->user->email} ({$autoinvest->user->id}) - {$th->getMessage()}"
            ]);
            logger($th);
        }
    }

    /**
     * Obtener la propiedad para obtener los nuevos cambios de la propiedad
     *
     * @return void
     */
    private function refreshProperty ()
    {
        $this->property = Property::find($this->property->id);
    }

}
