<?php

namespace App\Jobs;

use App\Helpers\TokensHelper;
use App\Notifications\FivePercentNotification;
use App\Property;
use App\PropertyUser;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Infinety\LemonWay\Facades\LemonWay;

class RepartirPorcentajes5 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var App\Property
     */
    protected $property;

    /**
     * @var App\User
     */
    protected $user;

    /**
     * @var App\PropertyUser
     */
    protected $relacion;

    /**
     * @var int
     */
    protected $porcentaje = 5;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Property $property, User $user)
    {
        $this->property = $property;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $locale = $this->user->locale;
        if ($locale !== null && localization()->getCurrentLocale() !== $locale) {
            localization()->setLocale($locale);
        }

        $inversiones = PropertyUser::where('property_id', $this->property->id)->where('user_id', $this->user->id)->get();
        $totalGanadoUsuario = 0;
        $cantidadAbonarUsuario = 0;
        $retenidoUsuario = 0;
        foreach ($inversiones as $inversion) {
            $eurosPorciento = ($inversion->total * 5) / 100;

            $diasPasadosDesdeInversion = $this->getDiasPasados($inversion);

            $diasYear = 365 + Carbon::now()->format('L');

            // Total Abonar Lemonway + Hacienda
            $totalGanadoUsuario += $eurosPorciento * ($diasPasadosDesdeInversion / $diasYear);
        }

        $nombreUsuario = $this->user->nombre;

        if ($totalGanadoUsuario == 0) {
            logger($nombreUsuario.': Ha invertido el último día y no tiene porcentaje que recibir');

            return true;
        }

        if ($this->user->tribute) {
            // Abonado 81%
            $cantidadAbonarUsuario = ($totalGanadoUsuario * 81) / 100;

            // Resto
            $retenidoUsuario = ($totalGanadoUsuario * 19) / 100;

            $totalGanadoUsuario = number_format($totalGanadoUsuario, 2, '.', '');
            $cantidadAbonarUsuario = number_format($cantidadAbonarUsuario, 2, '.', '');
            $retenidoUsuario = number_format($retenidoUsuario, 2, '.', '');

            logger($nombreUsuario.': Hay que pagarle '.$totalGanadoUsuario.'€. En lemonway se pagará un total de '.$cantidadAbonarUsuario.'€ y en Hacienda '.$retenidoUsuario.'€ por sus inversiones en el inmueble: '.$this->property->nombre);

            return $this->pagarAlUsuarioTribute($cantidadAbonarUsuario, $retenidoUsuario);
        } else {
            $totalGanadoUsuario = number_format($totalGanadoUsuario, 2, '.', '');
            logger($nombreUsuario.': Hay que pagarle '.$totalGanadoUsuario.'€ por el 5%');

            return $this->pagarAlUsuarioNoTribute($totalGanadoUsuario);
        }
    }

    /**
     * @param $cantidad
     */
    private function pagarAlUsuarioNoTribute($cantidad)
    {
        $mensaje = __('Diviendos generados del 5% sobre lo invertido en:').' '.$this->property->nombre;

        logger('user: '.$this->user->nombre);
        logger("cantidad: $cantidad");

        $transaction = [
            'wallet'  => $this->user->lemonway_id,
            'type'    => 18,
            'status'  => 0,
            'fecha'   => Carbon::now()->toDateTimeString(),
            'debit'   => 0,
            'credit'  => $cantidad,
            'iban_id' => null,
            'comment' => $mensaje,
            'refund'  => 0,
            'token'   => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'POR-'),
            'extras'  => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            //first Move to Poland Wallet
            $mensajeExternal = '5% - UserId: '.$this->user->id.' '.$this->property->nombre;

            $walletExternal = LemonWay::getWalletDetails(null, env('NO_TRIBUTE_WALLET'));
            $resultExternal = LemonWay::walletToWallet($walletSC, $walletExternal, $cantidad, $mensajeExternal);

            if ($resultExternal->STATUS == 3) {
                $result = LemonWay::walletToWallet($walletExternal, $this->user->wallet, $cantidad, $mensaje);
                $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);
            } else {
                logger('Error al pasar dinero a la cuenta extranjera');
                $transaction->update(['status' => 4]);
            }
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }

        //Notificamos al usuario
        try {
            $appLocale = current_locale();
            localization()->setLocale($this->user->locale);

            $this->user->notify(new FivePercentNotification($this->property, $cantidad, false));

            localization()->setLocale($appLocale);
        } catch (\Exception $e) {
            logger('Ha fallado enviar la notificación a la transacción: '.$transaction->id);

            return true;
        }

        return true;
    }

    /**
     * @param $cantidad
     */
    private function pagarAlUsuarioTribute($cantidad, $retenido)
    {
        $mensaje = __('Diviendos generados del 5% sobre lo invertido en:').' '.$this->property->nombre;

        logger('user: '.$this->user->nombre);
        logger("cantidad: $cantidad");
        logger("retenido: $retenido");

        $transaction = [
            'wallet'  => $this->user->lemonway_id,
            'type'    => 18,
            'status'  => 0,
            'fecha'   => Carbon::now()->toDateTimeString(),
            'debit'   => 0,
            'credit'  => $cantidad,
            'iban_id' => null,
            'comment' => $mensaje,
            'refund'  => 0,
            'token'   => $token = TokensHelper::generateUniqueToken(Transaction::class, 'token', 10, true, 'POR-'),
            'extras'  => $this->property->id,
        ];

        $transaction = $this->user->transactions()->create($transaction);

        $walletSC = LemonWay::getWalletDetails(null, 'SC');

        try {
            $result = LemonWay::walletToWallet($walletSC, $this->user->wallet, $cantidad, $mensaje);
            $transaction->update(['status' => $result->STATUS, 'lemonway_id' => $result->ID]);

            // Tests
            //$transaction->update(['status' => 55, 'lemonway_id' => 0]);
        } catch (\Exception $e) {
            $transaction->update(['status' => 4]);

            return false;
        }

        //Notificamos al usuario
        try {
            $appLocale = current_locale();
            localization()->setLocale($this->user->locale);

            $this->user->notify(new FivePercentNotification($this->property, $cantidad, $retenido));

            localization()->setLocale($appLocale);
        } catch (\Exception $e) {
            logger('Ha fallado enviar la notificación a la transacción: '.$transaction->id);

            return true;
        }

        return true;
    }

    /**
     * @return mixed
     */
    private function getDiasPasados(PropertyUser $inversion)
    {
        $diaInversion = $inversion->created_at;
        $hoy = Carbon::now();

        return $hoy->diffInDays($diaInversion);
    }
}
