<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ReferralsGift extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('referral_gifts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->integer('referral_id')->unsigned()->nullable()->index();
            $table->foreign('referral_id')->references('id')->on('users')->onDelete('SET NULL');
            $table->decimal('user_amount', 6, 2);
            $table->decimal('referral_amount', 6, 2);
            $table->integer('property')->unsigned()->nullable()->index();
            $table->boolean('executed')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('referral_gifts');
    }
}
