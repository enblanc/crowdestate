<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewLemonWayColumnsToProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->date('birthdate')->nullable()->after('estado_civil');
            $table->string('company_cif')->nullable()->after('birthdate');
            $table->string('company_rsocial')->nullable()->after('company_cif');
            $table->string('company_description')->nullable()->after('company_rsocial');
            $table->string('company_website')->nullable()->after('company_description');
            $table->string('company_country')->nullable()->after('company_website');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('profiles', function (Blueprint $table) {
            $table->dropColumn('company_country');
            $table->dropColumn('company_website');
            $table->dropColumn('company_description');
            $table->dropColumn('company_rsocial');
            $table->dropColumn('company_cif');
            $table->dropColumn('birthdate');
        });
    }
}
