<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKycDocumentStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kyc_document_statuses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('lemonway_id')->nullbale();
            $table->string('document_id')->nullbale();
            $table->integer('type')->nullbale();
            $table->integer('status')->nullbale(); 
            $table->string('comment')->nullbale();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kyc_document_statuses');
    }
}
