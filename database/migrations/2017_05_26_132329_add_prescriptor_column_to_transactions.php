<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPrescriptorColumnToTransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->integer('property_user_id')->unsigned()->index()->nullable()->after('extras');
            $table->foreign('property_user_id')->references('id')->on('property_user')->onDelete('SET NULL');
            $table->double('prescriptor', 8, 2)->nullable()->after('property_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('transactions', function (Blueprint $table) {
            $table->dropForeign(['property_user_id']);
            $table->dropColumn('prescriptor');
            $table->dropColumn('property_user_id');
        });
    }
}
