<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransactionIdToAutoinvestsOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('autoinvests_orders', function (Blueprint $table) {
            $table->integer('transaction_id')->after('investment')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoinvests_orders', function (Blueprint $table) {
            $table->dropColumn(['transaction_id']);
        });
    }
}
