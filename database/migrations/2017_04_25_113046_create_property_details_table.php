<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable()->unique()->unsigned();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->string('similares_first_name')->nullable();
            $table->string('similares_first_data_1')->nullable();
            $table->string('similares_first_data_2')->nullable();
            $table->string('similares_second_name')->nullable();
            $table->string('similares_second_data_1')->nullable();
            $table->string('similares_second_data_2')->nullable();

            $table->string('invertir_first_title')->nullable();
            $table->text('invertir_first_data')->nullable();
            $table->string('invertir_second_title')->nullable();
            $table->text('invertir_second_data')->nullable();
            $table->string('invertir_third_title')->nullable();
            $table->text('invertir_third_data')->nullable();

            $table->string('localizacion_first_title')->nullable();
            $table->text('localizacion_first_data')->nullable();
            $table->string('localizacion_second_title')->nullable();
            $table->text('localizacion_second_data')->nullable();
            $table->string('localizacion_third_title')->nullable();
            $table->text('localizacion_third_data')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_details');
    }
}
