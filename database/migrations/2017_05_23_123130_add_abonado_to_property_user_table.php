<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAbonadoToPropertyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_user', function (Blueprint $table) {
            $table->boolean('abonado')->default(0)->after('total');
            $table->double('total_abonado', 15, 2)->nullable()->after('abonado');
            $table->integer('transaction_id')->unsigned()->index()->nullable()->after('total_abonado');
            $table->foreign('transaction_id')->references('id')->on('transactions')->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_user', function (Blueprint $table) {
            $table->dropForeign(['transaction_id']);
            $table->dropColumn('transaction_id');
            $table->dropColumn('total_abonado');
            $table->dropColumn('abonado');
        });
    }
}
