<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoinvestsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autoinvests_events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable();
            $table->enum('type', ['marketplace', 'property']);
            $table->integer('evaluated')->nullable()->default(0);
            $table->longText('logs')->nullable();
            $table->longText('parameters')->nullable();
            $table->timestamps();
        });

        Schema::table('autoinvests_orders', function (Blueprint $table) {
            $table->integer('autoinvests_event_id')->nullable()->after('investment');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('autoinvests_orders', function (Blueprint $table) {
            $table->dropColumn(['autoinvests_event_id']);
        });
        
        Schema::dropIfExists('autoinvests_events');
    }
}
