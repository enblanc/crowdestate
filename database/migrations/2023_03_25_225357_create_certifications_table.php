<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCertificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('certifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('direccion_1');
            $table->string('direccion_2')->nullable();
            $table->string('direccion_3')->nullable();
            $table->string('year');
            $table->string('nif');
            $table->string('name');
            $table->string('total');
            $table->string('retention');
            $table->string('paid');
            $table->string('business_name');
            $table->string('cif');
            $table->string('public_name');
            $table->string('date');
            $table->string('signature');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('certifications');
    }
}
