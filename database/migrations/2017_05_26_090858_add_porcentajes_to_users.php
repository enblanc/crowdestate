<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPorcentajesToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->double('porcentaje_invertido', 15, 2)->nullable()->after('lemonway_walletid');
            $table->double('porcentaje_ganado', 15, 2)->nullable()->after('porcentaje_invertido');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('porcentaje_ganado');
            $table->dropColumn('porcentaje_invertido');
        });
    }
}
