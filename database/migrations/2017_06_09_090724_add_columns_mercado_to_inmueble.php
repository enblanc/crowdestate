<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsMercadoToInmueble extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_details', function (Blueprint $table) {
            $table->string('mercado_first_title')->nullable()->after('localizacion_third_data');
            $table->text('mercado_first_data')->nullable()->after('mercado_first_title');
            $table->string('mercado_second_title')->nullable()->after('mercado_first_data');
            $table->text('mercado_second_data')->nullable()->after('mercado_second_title');
            $table->string('mercado_third_title')->nullable()->after('mercado_second_data');
            $table->text('mercado_third_data')->nullable()->after('mercado_third_title');
            $table->text('rendimiento_mercado')->nullable()->after('mercado_third_data');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn('rendimiento_mercado');
            $table->dropColumn('mercado_third_data');
            $table->dropColumn('mercado_third_title');
            $table->dropColumn('mercado_third_title');
            $table->dropColumn('mercado_second_data');
            $table->dropColumn('mercado_second_title');
            $table->dropColumn('mercado_first_data');
            $table->dropColumn('mercado_first_title');
        });
    }
}
