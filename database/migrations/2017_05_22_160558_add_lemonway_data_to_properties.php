<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLemonwayDataToProperties extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->string('lemonway_id')->nullable()->after('fecha_publicacion');
            $table->string('lemonway_walletid')->unique()->nullable()->after('lemonway_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('properties', function (Blueprint $table) {
            $table->dropColumn(['lemonway_id']);
            $table->dropColumn('lemonway_walletid');
        });
    }
}
