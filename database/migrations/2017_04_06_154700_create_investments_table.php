<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('investments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('investment_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('investment_id')->unsigned();
            $table->string('nombre');
            $table->string('locale')->index();
            $table->unique(['investment_id','locale']);
            $table->foreign('investment_id')->references('id')->on('investments')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('investment_translations');
        Schema::dropIfExists('investments');
    }
}
