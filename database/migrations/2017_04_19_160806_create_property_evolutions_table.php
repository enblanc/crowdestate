<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertyEvolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_evolutions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_id')->nullable()->unsigned();
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->date('fecha');
            $table->enum('tipo', ['prevision', 'seguimiento']);
            $table->integer('media_id')->nullable()->unsigned();
            $table->foreign('media_id')->references('id')->on('media')->onDelete('SET NULL');

            //Campos ahora del excel
            $table->double('numero_reservas', 15, 8)->nullable();
            $table->double('grado_ocupacion', 15, 8)->nullable();
            $table->double('dias_reservados', 15, 8)->nullable();
            $table->double('dias_no_reservados', 15, 8)->nullable();
            $table->double('dias_bloqueados', 15, 8)->nullable();
            $table->double('tarifa_media_dia', 15, 8)->nullable();
            $table->double('estimacion_ingresos_anuales', 15, 8)->nullable();
            $table->double('satisfaccion_clientes', 15, 8)->nullable();
            $table->double('numero_invitados', 15, 8)->nullable();
            $table->double('ingresos_totales', 15, 8)->nullable();
            $table->double('ingresos_alquiler', 15, 8)->nullable();
            $table->double('ingresos_venta', 15, 8)->nullable();
            $table->double('otros_ingresos', 15, 8)->nullable();
            $table->double('gastos_explotacion', 15, 8)->nullable();
            $table->double('publicidad_prensa', 15, 8)->nullable();
            $table->double('otro_material', 15, 8)->nullable();
            $table->double('gastos_ventas', 15, 8)->nullable();
            $table->double('i_mas_d', 15, 8)->nullable();
            $table->double('arrendamientos', 15, 8)->nullable();
            $table->double('comisiones_crowdestate', 15, 8)->nullable();
            $table->double('gestion', 15, 8)->nullable();
            $table->double('suministros', 15, 8)->nullable();
            $table->double('ibi', 15, 8)->nullable();
            $table->double('otras_tasas', 15, 8)->nullable();
            $table->double('amortizacion', 15, 8)->nullable();
            $table->double('subvenciones', 15, 8)->nullable();
            $table->double('enajenacion_inmovilizado', 15, 8)->nullable();
            $table->double('resultado_explotacion', 15, 8)->nullable();
            $table->double('resultado_financiero', 15, 8)->nullable();
            $table->double('ingresos_financieros', 15, 8)->nullable();
            $table->double('gastos_financieros', 15, 8)->nullable();
            $table->double('otros_instrumentos_financieros', 15, 8)->nullable();
            $table->double('resultado_antes_impuestos', 15, 8)->nullable();
            $table->double('impuestos', 15, 8)->nullable();
            $table->double('resultados_ejercicio', 15, 8)->nullable();
            $table->double('dividendos_generados', 15, 8)->nullable();
            $table->double('dividendos_abonar', 15, 8)->nullable();
            $table->double('total_dividendos_abonados', 15, 8)->nullable();
            $table->double('dividendos_cartera', 15, 8)->nullable();
            $table->double('total_dividendos_devengados', 15, 8)->nullable();
            $table->double('revalorizacion_inmueble', 15, 8)->nullable();
            $table->double('valor_actual_inmueble', 15, 8)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_evolutions');
    }
}
