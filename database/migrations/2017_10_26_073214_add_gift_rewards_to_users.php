<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGiftRewardsToUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->double('gift_referidor', 15, 2)->nullable()->after('porcentaje_ganado');
            $table->double('gift_recibe', 15, 2)->nullable()->after('gift_referidor');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gift_referidor');
            $table->dropColumn('gift_recibe');
        });
    }
}
