<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ref', 10)->unique();
            $table->string('slug')->unique();
            $table->string('nombre');
            $table->string('catastro', 50);
            $table->integer('investment_id')->nullable()->unsigned();
            $table->foreign('investment_id')->references('id')->on('investments')->onDelete('SET NULL');

            $table->integer('property_type_id')->nullable()->unsigned();
            $table->foreign('property_type_id')->references('id')->on('property_types')->onDelete('SET NULL');

            $table->string('ubicacion');
            $table->string('barrio');
            $table->boolean('zona_premium');
            $table->integer('habitaciones');

            $table->integer('property_state_id')->nullable()->unsigned();
            $table->foreign('property_state_id')->references('id')->on('property_states')->onDelete('SET NULL');

            $table->integer('toilets')->nullable()->default(0);
            $table->integer('metros')->default(0);
            $table->string('gestor');
            $table->integer('garaje')->nullable()->default(0);
            $table->integer('piscina')->nullable()->default(0);
            $table->integer('ascensor')->nullable()->default(0);
            $table->string('direccion')->nullable();
            $table->decimal('lat', 10, 8);
            $table->decimal('lng', 11, 8);
            $table->integer('antiguedad')->default(0);
            $table->mediumText('descripcion')->nullable();
            $table->longText('ficha')->nullable();

            $table->integer('developer_id')->nullable()->unsigned();
            $table->foreign('developer_id')->references('id')->on('developers')->onDelete('SET NULL');

            $table->integer('periodo_financiacion')->nullable();
            $table->string('horizonte_temporal')->nullable();
            $table->double('valor_mercado', 15, 8)->nullable();
            $table->double('valor_compra', 15, 8)->nullable();
            $table->double('coste_total', 15, 8)->nullable();
            $table->double('hipoteca', 15, 8)->nullable();
            $table->double('objetivo', 15, 8)->nullable();
            $table->integer('importe_minimo')->nullable();
            $table->decimal('rentabilidad_alquiler_bruta', 5, 2)->nullable();
            $table->decimal('rentabilidad_alquiler_neta', 5, 2)->nullable();
            $table->decimal('rentabilidad_objetivo_bruta', 5, 2)->nullable();
            $table->decimal('rentabilidad_objetivo_neta', 5, 2)->nullable();
            $table->decimal('rentabilidad_anual', 5, 2)->nullable();
            $table->decimal('rentabilidad_total', 5, 2)->nullable();
            $table->double('valor_operacion', 15, 8)->nullable();
            $table->double('tasa_interna_rentabilidad', 15, 8)->nullable();

            $table->double('coste_obras', 15, 8)->nullable();
            $table->double('impuestos_tramites', 15, 8)->nullable();
            $table->double('registro_sociedad', 15, 8)->nullable();
            $table->double('otros_costes', 15, 8)->nullable();
            $table->double('imprevistos', 15, 8)->nullable();
            $table->double('caja_minima', 15, 8)->nullable();
            $table->double('honorarios', 15, 8)->nullable();
            $table->double('ingresos_anuales_alquiler', 15, 8)->nullable();
            $table->double('importe_venta', 15, 8)->nullable();
            $table->double('compra_venta_margen_bruto', 15, 8)->nullable();

            $table->boolean('resumen')->default(0);
            $table->boolean('estado')->default(0);
            $table->date('fecha_publicacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
