<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('tipo')->default(0);
            $table->string('nacionalidad')->nullable();
            $table->string('dni')->nullable();
            $table->integer('document_id')->nullable()->unsigned()->index();
            $table->foreign('document_id')->references('id')->on('documents')->onDelete('SET NULL');
            $table->string('telefono')->nullable();
            $table->string('direccion')->nullable();
            $table->string('codigo_postal')->nullable();
            $table->string('localidad')->nullable();
            $table->string('pais')->nullable();
            $table->string('provincia')->nullable();
            $table->integer('sexo')->default(0);
            $table->string('profesion')->nullable();
            $table->string('estado_civil')->nullable();
            $table->boolean('acreditado')->default(0);
            $table->boolean('estado')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
