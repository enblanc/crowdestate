<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoinvestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autoinvests', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            
            $table->double('min_investment', 15, 2)->default(50)->description('Lo que quiere invertir como mínimo en un proyecto. El mínimo sería 50 eur.');
            $table->double('max_investment', 15, 2)->description('la cantidad máxima que se quiere invertir en un inmueble.');
            $table->double('portfolio_size', 15, 2)->description('La cantidad máxima que quiere invertir según la orden.');
            $table->double('investment', 15, 2)->default(0)->description('La cantidad ya invertida con respecto al portfolio_size')->nullable();

            $table->date('period', 15, 2)->description('El tiempo por el cual estará activa la orden.');
            $table->string('location');
            $table->string('country')->nullable();
            $table->string('city')->nullable();
            $table->string('developer_id')->description('La variable es "promotor"');
            $table->integer('property_type_id')->nullable()->description('La variable es "Tipo de inmueble"');
            $table->double('tir', 15, 2)->description('Tir del Proyecto.');
            $table->string('risk_scoring')->nullable();
            $table->integer('status')->default(1)->nullable();
            
            $table->timestamps();
        });

        Schema::create('autoinvests_orders', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->integer('autoinvest_id');
            $table->integer('property_id')->nullable();
            $table->integer('market_order_id')->nullable();
            $table->double('investment', 15, 2);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoinvests_orders');
        Schema::dropIfExists('autoinvests');
    }
}
