<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('lemonway_id');
            $table->string('wallet');
            $table->integer('type');
            $table->integer('status');
            $table->dateTime('fecha');
            $table->double('debit', 15, 2)->nullable();
            $table->double('credit', 15, 2)->nullable();
            $table->integer('iban_id')->unsigned()->index()->nullable();
            $table->foreign('iban_id')->references('id')->on('bank_accounts')->onDelete('SET NULL');
            $table->string('comment')->nullable();
            $table->integer('refund')->default(0);
            $table->string('token')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
