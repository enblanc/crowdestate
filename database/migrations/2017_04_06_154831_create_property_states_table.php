<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyStatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property_states', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('property_state_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('property_state_id')->unsigned();
            $table->string('nombre');
            $table->string('locale')->index();
            $table->unique(['property_state_id','locale']);
            $table->foreign('property_state_id')->references('id')->on('property_states')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('property_state_translations');
        Schema::dropIfExists('property_states');
    }
}
