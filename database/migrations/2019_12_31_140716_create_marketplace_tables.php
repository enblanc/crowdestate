<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMarketplaceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('marketplace_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('type');
            $table->integer('position');
            $table->integer('user_id')->unsigned()->index()->nullable();
            $table->integer('property_id')->unsigned()->index()->nullable();
            $table->integer('property_user_id')->unsigned()->index()->nullable();
            $table->double('quantity', 15, 2)->nullable();
            $table->double('price', 15, 2)->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('property_id')->references('id')->on('properties')->onDelete('cascade');
            $table->foreign('property_user_id')->references('id')->on('property_user')->onDelete('cascade');
            $table->boolean('status')->default(1);
            $table->boolean('executed')->default(0);
            $table->integer('transaction_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('marketplace_orders');
    }
}
