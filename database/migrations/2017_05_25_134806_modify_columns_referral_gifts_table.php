<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyColumnsReferralGiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('referral_gifts', function (Blueprint $table) {
            $table->renameColumn('property', 'property_user');
            $table->integer('property_referral')->unsigned()->nullable()->index()->after('property');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('referral_gifts', function (Blueprint $table) {
            $table->dropColumn('property_referral');
            $table->renameColumn('property_user', 'property');
        });
    }
}
