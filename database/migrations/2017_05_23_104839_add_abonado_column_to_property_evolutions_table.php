<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAbonadoColumnToPropertyEvolutionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('property_evolutions', function (Blueprint $table) {
            $table->smallInteger('abonado')->default(0)->after('valor_actual_inmueble');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('property_evolutions', function (Blueprint $table) {
            $table->dropColumn('abonado');
        });
    }
}
