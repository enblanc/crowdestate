<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFaqTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('faq_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
        });

        Schema::create('faq_type_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('faq_type_id')->unsigned();
            $table->string('nombre');
            $table->string('locale')->index();
            $table->unique(['faq_type_id','locale']);
            $table->foreign('faq_type_id')->references('id')->on('faq_types')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('faq_type_translations');
        Schema::dropIfExists('faq_types');
    }
}
