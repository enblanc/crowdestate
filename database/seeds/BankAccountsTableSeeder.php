<?php

use App\BankAccount;
use Faker\Factory;
use HttpOz\Roles\Models\Role;
use Illuminate\Database\Seeder;

class BankAccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersInversores = Role::where('slug', 'inversor')->first()->users;

        $faker = Factory::create('es_ES');

        foreach ($usersInversores as $user) {
            $random = random_int(1, 2);
            for ($i = 1; $i <= $random; $i++) {
                $bankAccount = [
                    'titular'     => $user->nombreCompleto,
                    'iban'        => $faker->bankAccountNumber,
                    'bic_swift'   => $faker->swiftBicNumber,
                    'pais'        => 'ES',
                    'estado'      => 0,
                    'lemonway_id' => 0,
                ];

                $bankAccountModel = new BankAccount($bankAccount);
                $userBank = $user->bankAccounts()->save($bankAccountModel);

                // // Ejemplo documentacion
                // $file = public_path('tests/images/payment-domiciliacion.jpg');
                // $userBank->uploadFileToMedia($file, 'domiciliacion', true, true, $userBank->mediaCollection);
            }
        }
    }
}
