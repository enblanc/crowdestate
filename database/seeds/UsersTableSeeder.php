<?php

use App\Helpers\TokensHelper;
use HttpOz\Roles\Models\Role;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Recogemos los roles
        $superRole = Role::whereSlug('super')->first();
        $adminRole = Role::whereSlug('admin')->first();
        $captadorRole = Role::whereSlug('captador')->first();
        $inversorRole = Role::whereSlug('inversor')->first();
        $userRole = Role::whereSlug('user')->first();

        \App\User::flushEventListeners();

        //Creamos los usuarios
        $super = \App\User::create([
            'nombre'       => 'Infinety',
            'apellido1'    => 'Infinety Labs',
            'apellido2'    => 'We',
            'email'        => 'hello@infinety.es',
            'password'     => 'OldBranding10',
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
        ]);

        $admin = \App\User::create([
            'nombre'       => 'Administrador',
            'apellido1'    => 'Apellido 1',
            'apellido2'    => 'Apellido 2',
            'email'        => 'admin@crowdestate.on',
            'password'     => 'passAdmin',
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
        ]);

        $captador = \App\User::create([
            'nombre'       => 'Captador',
            'apellido1'    => 'Apellido 1',
            'apellido2'    => 'Apellido 2',
            'email'        => 'captador@crowdestate.on',
            'password'     => 'passCaptador',
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
        ]);

        $inversor = \App\User::create([
            'nombre'       => 'Inversor',
            'apellido1'    => 'Apellido 1',
            'apellido2'    => 'Apellido 2',
            'email'        => 'inversor@crowdestate.on',
            'password'     => 'passInversor',
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
        ]);

        $user = \App\User::create([
            'nombre'       => 'Usuario',
            'apellido1'    => 'Apellido 1',
            'apellido2'    => 'Apellido 2',
            'email'        => 'usuario@crowdestate.on',
            'password'     => 'passUsuario',
            'affiliate_id' => TokensHelper::generateUniqueToken('App\User'),
        ]);

        // Activamos los usuarios y les añadimos los roles
        $super->confirm()->create(['is_confirmed' => true, 'hash' => null]);
        $super->attachRole($superRole);

        $admin->confirm()->create(['is_confirmed' => true, 'hash' => null]);
        $admin->attachRole($adminRole);

        $captador->confirm()->create(['is_confirmed' => true, 'hash' => null]);
        $captador->attachRole($captadorRole);
        $captador->attachRole($inversorRole);

        $inversor->confirm()->create(['is_confirmed' => true, 'hash' => null]);
        $inversor->attachRole($inversorRole);

        $user->confirm()->create(['is_confirmed' => false, 'hash' => str_random(23)]);
        $user->attachRole($userRole);
    }
}
