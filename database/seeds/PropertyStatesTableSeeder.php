<?php

use App\PropertyState;
use Illuminate\Database\Seeder;

class PropertyStatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $state = new PropertyState();
        $state->translateOrNew('es')->nombre = 'En Adquisición';
        $state->save();

        $state = new PropertyState();
        $state->translateOrNew('es')->nombre = 'En estudio';
        $state->save();

        $state = new PropertyState();
        $state->translateOrNew('es')->nombre = 'En Obras';
        $state->save();

        $state = new PropertyState();
        $state->translateOrNew('es')->nombre = 'En Explotación';
        $state->save();

        $state = new PropertyState();
        $state->translateOrNew('es')->nombre = 'Vendido';
        $state->save();
    }
}
