<?php

use App\Helpers\TokensHelper;
use App\User;
use HttpOz\Roles\Models\Role;
use Illuminate\Database\Seeder;

class LemonWayTestsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $eric = User::create([
            'nombre'            => 'Eric',
            'apellido1'         => 'Lagarda',
            'apellido2'         => 'Martinez',
            'email'             => 'eric@infinety.es',
            'password'          => 'Demo',
            'affiliate_id'      => TokensHelper::generateUniqueToken('App\User'),
            'lemonway_id'       => 'DA51DAEAD31',
            'lemonway_walletid' => 12,
        ]);

        $eric->confirm()->create(['is_confirmed' => true, 'hash' => null]);

        $userRole = Role::whereSlug('user')->first();
        $eric->attachRole($userRole);

        //DNI
        // $dniFile = public_path('tests/').'images/dni.png';
        // $eric->addDniDocument($dniFile);
    }
}
