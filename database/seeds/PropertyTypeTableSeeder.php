<?php

use App\PropertyType;
use Illuminate\Database\Seeder;

class PropertyTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $types = ['Apartamento', 'Bungalow', 'Chalet', 'Ático', 'Estudio', 'Duplex', 'Rural', 'Nave industrial', 'Trastero', 'Edificio', 'Terreno', 'Oficina', 'Obra nueva', 'Promoción'];

        foreach ($types as $typeName) {
            $type = new PropertyType;
            $type->translateOrNew('es')->nombre = $typeName;
            $type->save();
        }
    }
}
