<?php

use App\Helpers\ImportadorExcel;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon::now('Europe/Madrid');
        $mes = $now->month - 1;
        $fileExcelEvolucion = public_path('tests/excel/ficha_evolucion.xlsx');
        $result = ImportadorExcel::readEvolucionExcel($fileExcelEvolucion, $mes);

        $fileExcelEstimacion = public_path('tests/excel/ficha_estimacion.xlsx');
        $resultEstimacion = ImportadorExcel::readEstimacionExcel($fileExcelEstimacion);

        $properties = factory(App\Property::class, 10)->create()->each(function ($property) use ($fileExcelEvolucion, $result, $fileExcelEstimacion, $resultEstimacion) {
            $imagesCount = random_int(1, 3);
            for ($i = 1; $i <= $imagesCount; $i++) {
                $random = random_int(1, 10);
                $file = public_path('tests/images/property'.$random.'.jpg');
                $name = 'property_'.$property->id.'_'.$i;
                $property->uploadFileToMedia($file, $name, false, true, $property->mediaCollection);
            }

            $documentCount = random_int(1, 3);
            for ($i = 1; $i <= $documentCount; $i++) {
                $random = random_int(1, 2);
                $file = public_path('tests/documentos/doc_'.$random.'.pdf');
                $name = 'doc_'.$random;
                $property->uploadFileToMedia($file, $name, false, true, $property->mediaCollectionDocuments, false, ['visibility' => 'public']);
            }

            $documentCount = random_int(1, 3);
            for ($i = 1; $i <= $documentCount; $i++) {
                $random = random_int(1, 2);
                $file = public_path('tests/documentos/doc_'.$random.'.pdf');
                $name = 'doc_'.$random;
                $property->uploadFileToMedia($file, $name, false, true, $property->mediaCollectionDocuments, false, ['visibility' => 'private']);
            }

            $name = 'ficha_evolucion_'.$property->id;
            $mediaFile = $property->uploadFileToMedia($fileExcelEvolucion, $name, false, true, $property->mediaCollectionExcelEvolucion, false, ['date' => date('d-m-Y')]);
            $result->tipo = 'seguimiento';
            $result->fecha = date('Y-m-d');
            $result->media_id = $mediaFile->id;
            $property->evolutions()->create((array) $result);

            $name = 'ficha_estimacion_'.$property->id;
            $mediaFile = $property->uploadFileToMedia($fileExcelEstimacion, $name, false, true, $property->mediaCollectionExcelEstimacion, false, ['date' => date('d-m-Y')]);
            $fecha = Carbon::createFromDate(date('Y'), 1, 1); //Añadimos más 1 para corresponder con el objeto carbon.
            $resultEstimacion->fecha = $fecha->format('Y-m-d');
            $resultEstimacion->tipo = 'prevision';
            $resultEstimacion->media_id = $mediaFile->id;

            $property->predicted()->create((array) $resultEstimacion);
        });
    }
}
