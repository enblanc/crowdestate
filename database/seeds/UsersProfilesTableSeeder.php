<?php

use App\Profile;
use App\User;
use Faker\Factory;
use HttpOz\Roles\Models\Role;
use Illuminate\Database\Seeder;

class UsersProfilesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $usersInversores = Role::where('slug', 'inversor')->first()->users;

        $faker = Factory::create('es_ES');

        $users = User::all();
        foreach ($users as $user) {
            $user->profile()->create([]);
        }

        foreach ($usersInversores as $user) {
            $profile = [
                'tipo'          => random_int(1, 2),
                'nacionalidad'  => 'ES',
                'dni'           => $faker->dni,
                'telefono'      => $faker->phoneNumber,
                'direccion'     => $faker->streetAddress,
                'codigo_postal' => $faker->postcode,
                'localidad'     => $faker->city,
                'pais'          => 'España',
                'provincia'     => $faker->state,
                'sexo'          => random_int(1, 2),
                'profesion'     => 'Programador',
                'estado_civil'  => 'Casado',
                'acreditado'    => 0,
                'estado'        => 0,
            ];

            $user->profile()->update($profile);

            $userProfile = $user->profile;

            // DNI ejemplo
            // $file = public_path('tests/images/dni.png');
            // $userProfile->uploadFileToMedia($file, 'dni', true, true, $userProfile->mediaCollection);
        }
    }
}
