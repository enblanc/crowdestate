<?php

use App\PropertyDetails;
use Illuminate\Database\Seeder;

class PropertyDetailsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $similares = [
            [
                'name'      => 'Atico en el Centro',
                'details_1' => '10% de rentabilidad alquiler neta',
                'details_2' => '29% de rentabilidad de venta neta',
            ],
            [
                'name'      => 'Estudio Carmen',
                'details_1' => '7% de rentabilidad alquiler neta',
                'details_2' => '14% de rentabilidad de venta neta',
            ],
            [
                'name'      => 'Estudio La Paz',
                'details_1' => '3% de rentabilidad alquiler neta',
                'details_2' => '17% de rentabilidad de venta neta',
            ],
            [
                'name'      => 'Atico en Ruzafa',
                'details_1' => '16% de rentabilidad alquiler neta',
                'details_2' => '21% de rentabilidad de venta neta',
            ],
        ];

        $porque = [
            [
                'title' => 'Amplia demanda de alquilere turísticos',
                'text'  => 'Actualmente hay una gran demanda de alquileres en esta zona de la ciudad de Valencia.',
            ],
            [
                'title' => 'Gran estudio',
                'text'  => 'Es una gran oportunidad teniendo en cuenta los últimos datos de alquileres de la zona.',
            ],
            [
                'title' => 'Rentabilidad',
                'text'  => 'Durante los últimos 3 años la rentabilidad de esta zona ha subido en un 3%.',
            ],
            [
                'title' => 'Grandes ventas',
                'text'  => 'En los últimos 5 años se han comprado inmuebles similares a un gran precio.',
            ],
        ];

        $localizacion = [
            [
                'title' => 'El Centro despierta interés',
                'text'  => 'Etiam sed dolor feugiat, efficitur orci vel, imperdiet metus.',
            ],
            [
                'title' => 'Centro túristico',
                'text'  => 'Etiam suscipit semper sollicitudin. Sed nisi neque, bibendum quis luctus sit amet, mollis nec ex. Curabitur dictum elementum leo vel venenatis.',
            ],
            [
                'title' => 'Muy buena conexión de metro',
                'text'  => 'Sed nisi neque, bibendum quis luctus sit amet, mollis nec ex. Curabitur dictum elementum leo vel venenatis.',
            ],
            [
                'title' => 'El BUS en la misma puerta',
                'text'  => 'Nam et facilisis tortor. Phasellus tincidunt turpis hendrerit leo mollis sodales.',
            ],
        ];

        foreach (PropertyDetails::all() as $details) {

            $details->similares_first_name = $similares[random_int(0, 3)]['name'];
            $details->similares_first_data_1 = $similares[random_int(0, 3)]['details_1'];
            $details->similares_first_data_2 = $similares[random_int(0, 3)]['details_2'];
            $details->similares_second_name = $similares[random_int(0, 3)]['name'];
            $details->similares_second_data_1 = $similares[random_int(0, 3)]['details_1'];
            $details->similares_second_data_2 = $similares[random_int(0, 3)]['details_2'];
            $details->invertir_first_title = $porque[random_int(0, 3)]['title'];
            $details->invertir_first_data = $porque[random_int(0, 3)]['text'];
            $details->invertir_second_title = $porque[random_int(0, 3)]['title'];
            $details->invertir_second_data = $porque[random_int(0, 3)]['text'];
            $details->invertir_third_title = $porque[random_int(0, 3)]['title'];
            $details->invertir_third_data = $porque[random_int(0, 3)]['text'];
            $details->localizacion_first_title = $localizacion[random_int(0, 3)]['title'];
            $details->localizacion_first_data = $localizacion[random_int(0, 3)]['text'];
            $details->localizacion_second_title = $localizacion[random_int(0, 3)]['title'];
            $details->localizacion_second_data = $localizacion[random_int(0, 3)]['text'];
            $details->localizacion_third_title = $localizacion[random_int(0, 3)]['title'];
            $details->localizacion_third_data = $localizacion[random_int(0, 3)]['text'];
            $details->save();

            $random = random_int(1, 10);
            $file = public_path('tests/images/property'.$random.'.jpg');
            $name = md5(date('c'));
            $details->uploadFileToMedia($file, $name, false, true, $details->mediaRelatedFirstCollection, false);

            $random = random_int(1, 10);
            $file = public_path('tests/images/property'.$random.'.jpg');
            $name = md5(date('c'));
            $details->uploadFileToMedia($file, $name, false, true, $details->mediaRelatedSecondCollection, false);

        }
    }
}
