<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LocaleTableSeeder::class);
        $this->call(UserRolesSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(UsersProfilesTableSeeder::class);
        $this->call(BankAccountsTableSeeder::class);
        $this->call(PropertyStatesTableSeeder::class);
        $this->call(InvestmentTableSeeder::class);
        $this->call(PropertyTypeTableSeeder::class);
        $this->call(DeveloperTableSeeder::class);
        $this->call(PropertyTableSeeder::class);
        $this->call(PropertyDetailsSeeder::class);
        // $this->call(LemonWayTestsSeeder::class);
    }
}
