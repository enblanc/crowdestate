<?php

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Seeder;

class DeveloperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $developers = factory(App\Developer::class, 10)->create();
        Model::reguard();
    }
}
