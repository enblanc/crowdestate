<?php

use HttpOz\Roles\Models\Role;
use Illuminate\Database\Seeder;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            'name' => 'Super Admin',
            'slug' => 'super',
            'description' => 'Developers role',
        ]);

        Role::create([
            'name' => 'Admin',
            'slug' => 'admin',
            'description' => 'Admin account',
        ]);

        Role::create([
            'name' => 'Captador',
            'slug' => 'captador',
            'description' => 'Usuario captador. Es inversor y aparte puede tener usuarios referidos por los que cobra un porcentaje.',
        ]);

        Role::create([
            'name' => 'Inversor',
            'slug' => 'inversor',
            'description' => 'Usuario inversor. Tiene cuenta en lemonway.',
        ]);

        Role::create([
            'name' => 'User',
            'slug' => 'user',
            'description' => 'Usuario registrado. No es Inversor',
        ]);

         Role::create([
            'name' => 'Prescriptor',
            'slug' => 'prescriptor',
            'description' => 'Usuario registrado prescriptor',
        ]);
    }
}
