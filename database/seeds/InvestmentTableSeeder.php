<?php

use App\Investment;
use Illuminate\Database\Seeder;

class InvestmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $investment = new Investment;
        $investment->translateOrNew('es')->nombre = "Apartamento vacacional";
        $investment->save();

        $investment = new Investment;
        $investment->translateOrNew('es')->nombre = "Local comercial";
        $investment->save();

        $investment = new Investment;
        $investment->translateOrNew('es')->nombre = "Residencial";
        $investment->save();
    }
}
