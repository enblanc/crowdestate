<?php

use App\Developer;
use App\Investment;
use App\Property;
use App\PropertyState;
use App\PropertyType;
use App\User;
use Carbon\Carbon;
use Faker\Generator;
use Faker\Provider\es_ES\Address;
use Faker\Provider\es_ES\Internet;
use Faker\Provider\es_ES\Person;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(User::class, function (Generator $faker) {
    /**
     * @var mixed
     */
    static $password;

    $faker->addProvider(new Person($faker));

    return [
        'nombre'         => $faker->name,
        'apellido1'      => $faker->lastName,
        'apellido2'      => $faker->lastName,
        'email'          => $faker->unique()->safeEmail,
        'password'       => $password ?: $password = 'secret',
        'remember_token' => str_random(10),
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */

$factory->define(Developer::class, function (Generator $faker) {
    $faker->addProvider(new Person($faker));
    $faker->addProvider(new Internet($faker));

    return [
        'nombre'    => $faker->name,
        'direccion' => $faker->address,
        'email'     => $faker->unique()->companyEmail,
        'contacto'  => $faker->name,
    ];
});

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(Property::class, function (Generator $faker) {
    $faker->addProvider(new Person($faker));
    $faker->addProvider(new Address($faker));
    // $faker->addProvider(new Company($faker));

    $randomInvestment = Investment::all()->random(1)->first();
    $randomType = PropertyType::all()->random(1)->first();
    $randomState = PropertyState::all()->random(1)->first();
    $randomDeveloper = Developer::all()->random(1)->first();

    $fechaPublicacion = Carbon::now();
    $fechaPublicacion->addDays($faker->numberBetween(1, 180));

    $filter = function ($paragraph) {
        return '<p>'.$paragraph.'</p>';
    };

    $textDescription = array_map($filter, $faker->paragraphs(3));
    $textFicha = array_map($filter, $faker->paragraphs(6));

    return [
        'ref'                         => $faker->numberBetween(10000, 100000),
        'nombre'                      => $faker->colorName,
        'slug'                        => str_slug($faker->name),
        'catastro'                    => strtoupper(str_random(20)),
        'investment_id'               => $randomInvestment->id,
        'property_type_id'            => $randomType->id,
        'ubicacion'                   => $faker->state,
        'barrio'                      => $faker->city,
        'zona_premium'                => $faker->boolean,
        'habitaciones'                => $faker->numberBetween(1, 5),
        'property_state_id'           => $randomState->id,
        'toilets'                     => $faker->numberBetween(1, 3),
        'metros'                      => $faker->numberBetween(40, 120),
        'gestor'                      => $faker->name,
        'garaje'                      => $faker->boolean(30),
        'piscina'                     => $faker->boolean(20),
        'ascensor'                    => $faker->boolean(70),
        'direccion'                   => str_replace(',', '', $faker->address),
        'lat'                         => $faker->latitude,
        'lng'                         => $faker->longitude,
        'antiguedad'                  => $faker->year,
        'descripcion'                 => implode('', $textDescription),
        'ficha'                       => implode('', $textFicha),
        'developer_id'                => $randomDeveloper->id,
        // Datos Resumen
        'periodo_financiacion'        => $faker->numberBetween(40, 120),
        'horizonte_temporal'          => $faker->numberBetween(24, 36),
        'valor_mercado'               => $faker->randomFloat(0, 100000, 200000),
        'valor_compra'                => $faker->randomFloat(0, 100000, 200000),
        'coste_total'                 => $faker->randomFloat(0, 100000, 200000),
        'hipoteca'                    => $faker->randomFloat(0, 100000, 200000),
        'objetivo'                    => $faker->randomFloat(0, 100000, 200000),
        'importe_minimo'              => 50,
        'rentabilidad_alquiler_bruta' => $faker->randomFloat(2, 1, 10),
        'rentabilidad_alquiler_neta'  => $faker->randomFloat(2, 1, 10),
        'rentabilidad_objetivo_bruta' => $faker->randomFloat(2, 1, 10),
        'rentabilidad_objetivo_neta'  => $faker->randomFloat(2, 1, 10),
        'rentabilidad_anual'          => $faker->randomFloat(2, 1, 17),
        'rentabilidad_total'          => $faker->randomFloat(2, 1, 17),
        'valor_operacion'             => $faker->randomFloat(2, 1, 10),
        'tasa_interna_rentabilidad'   => $faker->randomFloat(null, 1, 10),
        'coste_obras'                 => $faker->randomFloat(0, 10000, 50000),
        'impuestos_tramites'          => $faker->randomFloat(0, 10000, 50000),
        'registro_sociedad'           => $faker->numberBetween(5, 17),
        'otros_costes'                => $faker->randomFloat(2, 1, 10),
        'imprevistos'                 => $faker->randomFloat(2, 1, 3000),
        'caja_minima'                 => $faker->numberBetween(2, 17),
        'honorarios'                  => $faker->numberBetween(0, 10),
        'ingresos_anuales_alquiler'   => $faker->numberBetween(0, 10),
        'importe_venta'               => $faker->numberBetween(0, 10),
        'compra_venta_margen_bruto'   => $faker->numberBetween(0, 10),
        'resumen'                     => true,
        //publicacion
        'fecha_publicacion'           => $fechaPublicacion->format('d/m/Y'),
    ];
});
