<?php

use Carbon\Carbon;
use App\Property;
use App\User;
use App\MarketOrder;
use App\Jobs\TestQueue;
use App\Jobs\AutoInvestPropertyJob;

/*
|--------------------------------------------------------------------------
| Dev Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
 */

Route::get('/lemonway/{user}', function (User $user) {

    return response()->json([
        'user_id'    => $user->id,
        'STATUS'     => $user->wallet->STATUS,
    ], 200, []);
});

/**
 * Comprobar la queue mediante un email.
 * Se lanza un job "TextQueue" con un minuto de delay.
 */
Route::get('/test-queue/{email}/{queue?}', function (string $email, string $queue = 'database') {
    $delay = Carbon::now()->addMinutes(1);
    
    $job = (new TestQueue($email))->delay($delay)->onConnection($queue);
    
    dispatch($job);

    return response()->json([
        'dispatch' => 'App\Jobs\TestQueue',
        'email'    => $email,
        'queue'    => $queue,
        'delay'    => $delay
    ], 200, []);
});

Route::get('/autoinvest-order/{order}', function (MarketOrder $order) {
    $job = (new \App\Jobs\AutoInvestMarketOrderJob($order, true));
    dispatch($job);
    return response()->json([
        'dispatch'  => 'App\Jobs\AutoInvestMarketOrderJob',
        'order'     => $order,
        'test'      => true,
    ], 200, []);
});

Route::get('/autoinvest-property/{property}/{test?}', function (Property $property, $test = true) {

        
    return  \App\AutoInvest::canInvert()
        ->byPropertyType($property->property_type_id)
        ->byDeveloper($property->developer_id)
        ->byLocation($property->ubicacion)
        ->byTir($property->tasa_interna_rentabilidad)
        ->whereDoesntHave('orders', function ($query) use ($property) {
            return $query->where('property_id', $property->id);
        })
        ->get();


    $job = (new AutoInvestPropertyJob($property, $true));
    
    dispatch($job);

    return response()->json([
        'dispatch'  => 'App\Jobs\AutoInvestPropertyJob',
        'test'      => $true,
        'property'  => $property,
    ], 200, []);
});

Route::get('/autoinvest-event', function () {

    $autoinvestEvent = new \App\Helpers\AutoInvestHelper();

    $autoinvestEvent->setDescription('Evento autoinvest para la Propiedad - Elvita (12)');

    $autoinvestEvent->setTypeProperty();

    $autoinvestEvent->setEvaluated(12);

    $autoinvestEvent->setParameters([
        'property_id'               => 12,
        'property_type_id'          => 1,
        'ubicacion'                 => 'Madrid',
        'tasa_interna_rentabilidad' => 12
    ]);

    $autoinvestEvent->setLog('Se recorrió los autoinvest');

    $autoinvestEvent->setLog('Finalizó');

    $autoinvestEvent->save();
    
    $autoinvestEvent->setParameters(['key3' => 'value3']);

    $autoinvestEvent->save();
    
    return response()->json([
        'autoinvest_event_id' => $autoinvestEvent->getId()
    ], 200, []);
});

Route::get('/property_user', function() {
    $propertyUser = \App\PropertyUser::where('total', '>', 0)->where('total', '<=', 0.0001)->get();
    $propertyUserNegative = \App\PropertyUser::where('total', '<', 0)->get();
    $marketOrders = \App\MarketOrder::with(['user', 'property'])
        ->where('status', true)
        ->where('executed', false)
        ->whereType(1)
        ->get();

    return view('dashboard.fixed.property_user', [
        'propertyUser'         => $propertyUser,
        'propertyUserNegative' => $propertyUserNegative,
        'marketOrders'         => $marketOrders
    ]);
});

// Route::get('/resumen', 'HomeController@checkResumenExcel');
// Route::get('/evolucion', 'HomeController@checkEvolucionExcel');
// Route::get('/hello', 'HelloSignController@createEmbed');
// Route::get('/hello-response', 'HelloSignController@getResponse')->name('sign-response');
// Route::any('/hellosign_callback', function () {
//     echo "It's working!";
// });

// Route::get('/blog', function () {
//     return view('front/blog/archive');
// });

// Route::get('/single', function () {
//     return view('front/blog/single');
// });
//
// Route::get('dedada', '\App\Http\Controllers\Tests\TestLemonWayController@testCircle');


Route::get('/autoinvest/property/{property}', '\App\Http\Controllers\Dev\AutoInvestPropertyDevController@testByProperty')->name('dev.autoinvest-property');

Route::get('/autoinvest-change-status-test', function() {
    // return \App\AutoInvest::canInvert()->get();
    // return \App\AutoInvest::where('status', true)->pluck('id');

    $ids =  [3, 8, 11, 12, 40, 42, 57, 77, 78, 85, 90, 101, 111, 112, 113, 114, 115, 116, 117, 118, 122, 124, 125, 126, 127, 128, 129, 130, 131, 134, 135, 138, 139, 140, 141, 144, 145, 146, 147, 149, 150, 151, 152, 153, 154, 156, 157, 161, 162, 163, 165, 167, 169, 171, 173, 179, 182, 183, 184, 185, 186, 188, 189, 190, 191, 192];

    // \App\AutoInvest::whereIn('id', $ids)->update(['status' => false]);
    \App\AutoInvest::whereIn('id', $ids)->update(['status' => true]);

    // \App\AutoInvest::where('id', 110)->update(['status' => true]);
    \App\AutoInvest::where('id', 110)->update(['status' => false]);

    return response()->json($ids, 200, []);
});