<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => 'v1', 'middleware' => ['web', 'auth', 'role:super|admin']], function () {
    //User resource controller
    Route::resource('user', '\App\Http\Controllers\Api\UserApiController');
    Route::get('user/transactions/{user}', '\App\Http\Controllers\Api\UserApiController@transactions')->name('api.user.transactions');
    Route::resource('property', '\App\Http\Controllers\Api\PropertyApiController');
});

Route::group(['prefix' => 'v1/lemonway'], function () {
    Route::post('status', '\App\Http\Controllers\Notifications\LemonWayNotificationsController@walletStatus')->name('api.lemonway.wallet.status');
    Route::post('documents', '\App\Http\Controllers\Notifications\LemonWayNotificationsController@documentStatus')->name('api.lemonway.documents.status');
    Route::post('payment/notify', '\App\Http\Controllers\Notifications\LemonWayNotificationsController@paymentNotification')->name('api.lemonway.payments.notify');
    Route::post('payment/wire', '\App\Http\Controllers\Notifications\LemonWayNotificationsController@moneyInWireRecieved')->name('api.lemonway.payments.wireIn');
    Route::post('payment/canceled', '\App\Http\Controllers\Notifications\LemonWayNotificationsController@moneyInWireRecieved')->name('api.lemonway.payments.canceled');
    Route::post('payment/chargeback', '\App\Http\Controllers\Notifications\LemonWayNotificationsController@chargeBack')->name('api.lemonway.payments.chargeback');
});
