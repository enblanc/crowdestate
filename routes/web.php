<?php

use App\Helpers\SeoHelpers;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

// Route::get('/demo', function () {
//     $months = [1,2,3,4,5,6,7,8,9,10,11,12];

//     \App\Helpers\ImportadorExcel::createDynamicConfig(1);
// });

// Route::get('/demo', function () {
//     $property = App\Property::find(6);
//     dump($property->evolutions);

//     foreach ($property->evolutions as $evolution) {
//         dump($evolution->fecha);
//     }
// });

// Route::get('/', 'Front\PropertyController@home');

Route::get('/ref/{token}', [
    'as'   => 'referred',
    'uses' => 'GuestController@referred',
]);

Route::get('postback/test', function () {
    $user = auth()->user();
    $transaction = [
        'lemonway_id' => 0,
        'wallet'      => 132131,
        'type'        => 2,
        'status'      => 3,
        'fecha'       => \Carbon\Carbon::now()->toDateTimeString(),
        'credit'      => 15,
        'token'       => \App\Helpers\TokensHelper::generateUniqueToken(\App\Transaction::class, 'token', 10, true, 'MAN-'),
        'refund'      => 0,
    ];

    $user->transactions()->create($transaction);
    
    if (count(\App\ClickidsLog::where('user_id', $user->id)
    ->where('click_id', $user->clickProvider->click_id)
    ->where('message', 'FIRST-TRANSACTION')
    ->get())) {
        return  1;
    }
    return  0;
});

// Route::get('login-test', function () {
//     \Illuminate\Support\Facades\Auth::loginUsingId(5);
// });

// $apiDomain = 'api.'.parse_url(config('app.url'), PHP_URL_HOST);
// Route::domain($apiDomain)->group(function ($router) {
//     $router->get('/property/{slug}', '\App\Http\Controllers\Api\ExternalApiController@property');

//     Route::any('{catch}', function () {
//         return responder()->error('unauthorized', 401);
//     })->where('catch', '.*');
// });

Route::get('assets/js/localization.js', 'Front\ApiController@messages')->name('js.localization');

Route::localizedGroup(function () {

    /**
     * Home
     */

    Route::get('/', 'Front\FrontController@home')->name('home');

    // Authentication Routes...
    Route::transGet('routes.user.acceso', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::post('logout', 'Auth\LoginController@logout')->name('logout');

    // Registration Routes...
    Route::post('register', 'Auth\RegisterController@register')->name('register');

    // Password Reset Routes...
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    /**
     * Registro y OAuth
     */
    Route::transGet('routes.user.registro', 'Auth\RegisterController@showRegistrationForm')->name('registro');
    Route::transGet('routes.user.recuperar', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('recuperar-password');

    Route::get('oauth/{driver}', 'Auth\SocialController@redirectToProvider')->name('social.login');
    Route::get('oauth/{driver}/callback', 'Auth\SocialController@handleProviderCallback')->name('social.callback');

    Route::transGet('routes.user.oauth.registro', 'Auth\SocialController@showRegistrationForm')->name('registro.oauth');
    Route::post('/auth/create', 'Auth\SocialController@createAccountSocial')->name('store.oauth');
    Route::transGet('routes.user.oauth.unir', 'Auth\SocialController@showLinkAccountForm')->name('link.oauth');
    Route::post('/auth/link', 'Auth\SocialController@linkAccountSocial')->name('link.account');
    Route::transGet('routes.user.nueva-pass', 'Front\UserController@showResetForm')->name('password.reset');

    /**
     * INMUEBLES
     */
    Route::transGet('routes.inmuebles.single', 'Front\PropertyController@show')->name('front.property.show');
    Route::transGet('routes.inmuebles.listado', 'Front\PropertyController@grid')->name('front.property.grid');

    /**
     * Panel Usuario
     */
    Route::group(['middleware' => ['web', 'auth', 'confirmed']], function () {
        Route::transGet('routes.panel.datos-basicos', 'Front\ProfileUpdateController@show')->name('panel.user.basic.show');
        Route::post('panel/datos-basicos', 'Front\ProfileUpdateController@update')->name('panel.user.basic.save');

        Route::transGet('routes.panel.cetifications', 'Front\UserController@cetifications')->name('panel.user.certifications.index');
        Route::transGet('routes.panel.cetification', 'Front\UserController@downloadCetification')->name('panel.user.certifications.download');

        Route::transGet('routes.panel.cambiar-contrasena', 'Front\PasswordUpdateController@show')->name('panel.user.password.show');
        Route::post('panel/cambiar-contrasena', 'Front\PasswordUpdateController@update')->name('panel.user.password.update');

        Route::transGet('routes.panel.acreditar-cuenta', 'Front\ProfileUpdateController@showAcreditar')->name('panel.user.accredit.show');
        Route::post('panel/acreditar-cuenta', 'Front\ProfileUpdateController@postAcreditar')->name('panel.user.accredit.save');

        Route::get('panel/user/exportTransactions/{user}', 'Front\UserController@exportTransactions')->name('panel.user.export.transactions');

        Route::transGet('routes.panel.invitar', 'Front\UserController@invitar')
            ->middleware('role:super|admin|captador')
            ->name('panel.user.invite');

        Route::post('panel/invitar', 'Front\UserController@lanzarinvitacion')
            ->middleware('role:super|admin|captador')
            ->name('panel.user.invite.post');

        Route::transGet('routes.panel.agregar-fondos', 'Front\UserController@vistaAgregarFondos')->middleware('accredited')->name('panel.user.money.show');
        Route::post('panel/nuevos-fondos', 'Front\UserController@agregarFondos')->middleware('accredited')->name('panel.user.money.add');
        Route::post('panel/eliminar-tj', 'Front\UserController@eliminarTarjeta')->middleware('accredited')->name('panel.user.tj.remove');

        Route::transGet('routes.panel.movimientos', 'Front\UserController@verMovimientos')->middleware('accredited')->name('panel.user.transactions.show');
        Route::transGet('routes.panel.cuentas-bancarias', 'Front\UserController@verCuentasBancarias')->middleware('accredited')->name('panel.user.bankaccounts.show');
        Route::post('nueva-cuenta-bancaria', 'Front\UserController@postAgregarCuentaBancaria')->middleware('accredited')->name('panel.user.bankaccounts.add');
        Route::post('eliminar-cuenta-bancaria', 'Front\UserController@postEliminarCuentaBancaria')->middleware('accredited')->name('panel.user.bankaccounts.remove');

        Route::transGet('routes.panel.retirar-fondos', 'Front\UserController@verRetirarFondos')->middleware('accredited')->name('panel.user.moneyof.show');
        Route::post('panel/retirar-fondos', 'Front\UserController@postRetirarFondos')->middleware('accredited')->name('panel.user.moneyof.add');

        //Autoinvest
        Route::transGet('routes.panel.autoinvest', 'Front\MarketAutoinvestController@index')->name('panel.user.marketplace.autoinvest');
        Route::get('autoinvest/propiedades', 'Front\MarketAutoinvestController@propiedades');
        Route::post('panel/autoinvest/update', 'Front\MarketAutoinvestController@updateOrCreate')->name('panel.user.marketplace.autoinvest.update');

        //Marketplace
        Route::transGet('routes.panel.mi-mercado', 'Front\MarketOrdersController@verMiMercado')->middleware('accredited')->name('panel.user.marketplace.orders');
        Route::transGet('routes.panel.mercado-historico', 'Front\MarketOrdersController@verHistorico')->middleware('accredited')->name('panel.user.marketplace.historical');

        Route::post('panel/save-market-order', 'Front\MarketOrdersController@nuevaOrdenMercado')->middleware('accredited')->name('panel.user.marketplace.save');
        Route::put('panel/save-market-update', 'Front\MarketOrdersController@updateOrdenMercado')->middleware('accredited')->name('panel.user.marketplace.update');
        Route::delete('panel/delete-order-market', 'Front\MarketOrdersController@deleteOrdenMercado')->middleware('accredited')->name('panel.user.marketplace.delete');

        Route::post('panel/cerrar-cuenta', 'Front\UserController@cerrarCuenta')->name('panel.user.acount.close');

        //inversion éxito
        Route::transGet('routes.invertir.pago_ok', 'Front\PaymentsController@invertirSuccess')->name('invertir.payment.success.get');
        Route::post('invertir/pago/ok', 'Front\PaymentsController@invertirSuccess')->name('invertir.payment.success.post');

        Route::transGet('routes.invertir.exito', 'Front\InvertirController@success')->name('invertir.payment.success.get');

        Route::transGet('routes.invertir.exito', function () {
            return view('front/access/inversion-success');
        })->name('invertir.successfull');

        // A patir de aquí solo pueden acceder los que estan acreditados

        Route::transGet('routes.panel.mis-inversiones', function () {
            SeoHelpers::setSEO(__('Mis inversiones'), __('Mis inversiones en BrickStarter'));

            return view('front/dashboard-inversion/mis-inversiones');
        })->middleware('accredited')->name('panel.user.inversion.general');

        Route::transGet('routes.panel.resumen-patrimonial', function () {
            SeoHelpers::setSEO(__('Mi resumen patrimonial'), __('Mi resumen patrimonial en BrickStarter'));

            return view('front/dashboard-inversion/resumen-patrimonial');
        })->name('panel.user.inversion.resumen');

        Route::transGet('routes.panel.mis-inversiones-single', 'Front\PropertyController@datosInversion')->middleware('accredited')->name('panel.user.inversion.property');

        /**
         * MARKETPLACE
         */
        Route::transGet('routes.mercado.listado', 'Front\MarketController@grid')->name('front.market.list');

        Route::transGet('routes.mercado.show', 'Front\MarketController@show')->name('front.market.show');
        Route::post('/marketplace/order', 'Front\MarketController@acceptOrder')->name('front.market.order');

        Route::transGet('routes.mercado.exito', function () {
            return view('front.access.market-order-success');
        })->name('front.market.successfull');
    });

    /**
     * Apis Usuario
     */
    Route::group(['middleware' => ['web', 'auth', 'confirmed']], function () {
        Route::transPost('routes.invertir.invertir_post', 'Front\InvertirController@checkInvertir')->name('front.property.invertir.check');
        Route::transGet('routes.invertir.invertir_get', 'Front\InvertirController@checkInvertirGet')->name('front.property.invertir.get');
        Route::post('/invertir/inmueble', 'Front\InvertirController@invertirEnInmueble')->name('front.property.invertir.post');

        Route::any('api/v1/usuarios/inversion', '\App\Http\Controllers\Api\UserApiController@misInversiones')->name('api.user.inversiones');
        Route::any('api/v1/usuarios/check-password', '\App\Http\Controllers\Api\UserApiController@checkPassword')->name('api.user.check-password');

        Route::post('api/v1/usuarios/orders', '\App\Http\Controllers\Api\MarketApiController@orders')->name('api.user.all-orders');
        Route::post('api/v1/usuarios/mi-inversion', '\App\Http\Controllers\Api\MarketApiController@inversion')->name('api.user.inversion');

        Route::get('api/v1/usuarios/get-order', '\App\Http\Controllers\Api\MarketApiController@getOrder')->name('api.user.order-get');

        Route::transGet('api/v1/marketplace', '\App\Http\Controllers\Api\MarketApiController@getListOrders')
            ->name('marketplace.all');

        Route::transGet('api/v1/my-transactions', '\App\Http\Controllers\Api\UserApiController@myTransactions')
            ->name('api.user.movements');
    });

    //Confirmations
    Route::transGet('routes.user.confirmar-email', 'Front\ProfileUpdateController@confirmemailreset')->name('emails.confirmar-cambio-email');

    //Welcome Page
    Route::group(['middleware' => ['web', 'auth']], function () {
        Route::transGet('routes.user.bienvenido', function () {
            SeoHelpers::setSEO(__('Bienvenido a BrickStarter'), __('Bienvenido a BrickStarter'));

            return view('front/access/bienvenido');
        })->name('welcome');
    });

    Route::get('/confirm/successfull', '\App\Http\Controllers\Auth\BeforeConfirmController@action')->name('confirm.successfull');
    
    /**
     * Apis Usuario
     */
    Route::group(['middleware' => ['web', 'auth', 'confirmed']], function () {
        Route::post('api/v1/webcam/upload', 'Front\ApiController@uploadWebcam')->name('front.user.webcam.upload');
    });

    //Rutas pagos llamadas lemonway
    //LemonWay Form Urls
    Route::transGet('routes.pagos.exito', 'Front\PaymentsController@success')->name('payment.ok');
    Route::transGet('routes.pagos.cancelado', 'Front\PaymentsController@cancel')->name('payment.cancel');
    Route::transGet('routes.pagos.error', 'Front\PaymentsController@error')->name('payment.error');

    Route::transPost('routes.pagos.exito', 'Front\PaymentsController@success')->name('payment.ok');
    Route::transPost('routes.pagos.cancelado', 'Front\PaymentsController@cancel')->name('payment.cancel');
    Route::transPost('routes.pagos.error', 'Front\PaymentsController@error')->name('payment.error');

    /**
     * PÄGINAS
     */

    // Contacto
    Route::transGet('routes.paginas.contacto', function () {
        SeoHelpers::setSEO(__('Contacto'), __('Contacta con BrickStarter'));

        return view('front/contact/contact');
    })->name('contact');
    Route::post('/contacto', 'Front\ContactController@send')->name('contacto_post');

    // Prescriptores
    Route::transGet('routes.paginas.prescriptores', function () {
        SeoHelpers::setSEO(__('Prescriptor'), __('Ser prescriptor de BrickStarter'));

        return view('front/contact/prescriptores');
    })->name('prescriptores');
    Route::post('/prescriptores', 'Front\PrescriptoresController@send')->name('prescriptores_post');

    // Quienes somos
    Route::transGet('routes.paginas.quienes-somos', 'Front\FrontController@quienesSomos')->name('front.quienesSomos');

    // Cómo funciona
    Route::transGet('routes.paginas.como-funciona', 'Front\FrontController@comoFunciona')->name('front.comoFunciona');

    // Faqs
    Route::transGet('routes.paginas.faq', 'Front\FaqController@index')->name('front.faq.general');
    Route::transGet('routes.paginas.faq_categoria', 'Front\FaqController@categoria')->name('front.faq.category');

    // Blog
    Route::transGet('routes.blog.listado', '\App\Blog\Controllers\PostController@index_front')->name('front.blog.index');
    Route::get('/blog/feed/json', 'Front\FrontController@postsJsonFeed')->name('front.blog.feed.json');
    Route::get('/blog/feed/xml', 'Front\FrontController@xmlFeed')->name('front.blog.feed.xml');
    Route::transGet('routes.blog.entrada', '\App\Blog\Controllers\PostController@show')->name('front.blog.show');
    Route::transGet('routes.blog.categoria', '\App\Blog\Controllers\PostController@showCategory')->name('front.blog.categories');

    // Newsleter
    Route::transGet('routes.paginas.newsletter', function () {
        SeoHelpers::setSEO('Newsletter', 'Newsletter de BrickStarter');

        return view('front.access.newsletter');
    });
    Route::post('newsletter', 'Front\UserController@addToNewsletter')->name('newsletter.add');

    // Legal
    Route::transGet('routes.paginas.informacion-legal', 'Front\FrontController@legal')->name('front.legal');
    Route::transGet('routes.paginas.informacion-legal-lemonway', 'Front\FrontController@legalLemonWay')->name('front.legal-lemonway');

    Route::transGet('/404', function () {
        SeoHelpers::setSEO('404', '');

        return view('front/corporate/404');
    });
});

// Route::get('registro', 'Auth\RegisterController@showRegistrationForm')->name('registro');
// Route::get('recuperar', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('recuperar-password');

Route::group([
    'prefix' => 'dashboard',
    'middleware' => ['web', 'auth', 'role:super|admin']
], function () {
    Route::get('/', 'Dashboard\DashboardController@index')->name('dashboard');

    Route::get('/marketplace', 'Dashboard\DashboardMarketplaceController@index')->name('dashboard.marketplace');
    
    Route::resource('inmuebles', 'Dashboard\DashboardPropertyController', ['parameters' => [
        'inmuebles' => 'property',
    ]]);
    Route::get('property-inversores/{property}', '\App\Http\Controllers\Api\PropertyApiController@inversores')->name('property.inversores');
    Route::get('property-inversores/{property}/export', '\App\Http\Controllers\Api\PropertyApiController@inversoresExport')->name('property.inversores.export');
    Route::get('users/{user}/edit-old', 'Dashboard\DashboardUserController@editav2')->name('dashboard.users.edit-old');
    Route::post('users/update-estado/{user}', 'Dashboard\DashboardUserController@updateEstado')->name('dashboard.users.estado');

    Route::resource('users', 'Dashboard\DashboardUserController', [
        'as' => 'dashboard',
    ]);

    Route::get('certificaciones', 'Dashboard\DashboardCertificationController@index')->name('dashboard.certifications.index');
    Route::get('certificaciones/show/{certification}', 'Dashboard\DashboardCertificationController@show')->name('dashboard.certifications.show');
    Route::delete('certificaciones/eliminar/{certification}', 'Dashboard\DashboardCertificationController@destroy')->name('dashboard.certifications.destroy');
    Route::post('certificaciones/post', 'Dashboard\DashboardCertificationController@post')->name('dashboard.certifications.post');

    Route::put('inmuebles/update_details/{property}', 'Dashboard\DashboardPropertyController@updateDetails')->name('inmuebles.detalles');
    Route::post('inmuebles/update_resumen/{property}', 'Dashboard\DashboardPropertyController@updateResumen')->name('inmuebles.resumen');
    Route::post('inmuebles/update_estimacion/{property}', 'Dashboard\DashboardPropertyController@updateEstimacion')->name('inmuebles.estimacion');
    Route::post('inmuebles/update_evolucion/{property}', 'Dashboard\DashboardPropertyController@updateEvolucion')->name('inmuebles.evolucion');
    Route::post('inmuebles/store_photo/{property}', 'Dashboard\DashboardPropertyController@storePhoto')->name('inmuebles.photos.store');
    Route::post('inmuebles/destroy_photo', 'Dashboard\DashboardPropertyController@destroyPhoto')->name('inmuebles.photos.destroy');
    Route::post('inmuebles/photo_principal/{property}', 'Dashboard\DashboardPropertyController@principalPhoto')->name('inmuebles.photos.principal');
    Route::post('inmuebles/photo_text/', 'Dashboard\DashboardPropertyController@setTextPhoto')->name('inmuebles.photos.texto');
    Route::post('inmuebles/store_document_public/{property}', 'Dashboard\DashboardPropertyController@storeDocumentPublic')->name('inmuebles.documents.public.store');
    Route::post('inmuebles/store_document_private/{property}', 'Dashboard\DashboardPropertyController@storeDocumentPrivate')->name('inmuebles.documents.private.store');
    Route::post('inmuebles/destroy_document', 'Dashboard\DashboardPropertyController@destroyDocument')->name('inmuebles.documents.destroy');

    Route::post('user/manual_inversion', 'Dashboard\DashboardUserController@manualInversion')->name('users.inversion_manual');
    Route::post('user/add_money', 'Dashboard\DashboardUserController@addMoneyFromLemonway')->name('users.add_money');
    Route::post('user/add_promotional', 'Dashboard\DashboardUserController@addManualPromotion')->name('users.add_promotional');
    Route::post('user/borrar_regalos/{user}', 'Dashboard\DashboardUserController@borrarRegalos')->name('users.borrar_regalos');
    Route::post('user/login_as_user/{user}', 'Dashboard\DashboardUserController@loginAsUser')->name('users.login_as');
    Route::get('user/download_report', 'Dashboard\DashboardUserController@exportUsers')->name('users.export');

    Route::get('pagos', 'Dashboard\DashboardPagosController@index')->name('dashboard.pagos.index');
    Route::post('pagos/upload-form', 'Dashboard\DashboardPagosController@uploadFile')->name('dashboard.pagos.upload');
    Route::post('pagos/reintentar/{id}', 'Dashboard\DashboardPagosController@restart')->name('dashboard.pagos.tryagain');
    

    Route::get('/bank-account/errors', function () {
        return \App\BankAccount::where('estado', 0)->whereHas('document', function ($query) {
            $query->where('estado', 2);
        })->get();
    });

    Route::get('/clickids', function () {
        return view('dashboard.clickids', [
            'clickids' => \App\Clickid::orderByDesc('id')->get()
        ]);
    })->name('clickid.index');

    Route::get('pagos-fixed', 'Dashboard\DashboardPagosFixedController@index')->name('dashboard.pagos.fixed');
    Route::post('pagos-fixed/post', 'Dashboard\DashboardPagosFixedController@post')->name('dashboard.pagos.fixed-post');
    Route::get('kyc-documents-status', 'Dashboard\DashboardKycDocumentStatusController@index')->name('dashboard.kyc-documents-status');
    // Route::get('/test', 'Tests\TestLemonWayController@test');
    // Route::get('/divi', 'Dashboard\RepartirDividendosController@testPago');

    Route::prefix('autoinvests')->name('dashboard.autoinvests.')->group(function () {
        Route::get('/', "Dashboard\DashboardAutoInvest@index")->name('index');
        Route::get('/ordenes', "Dashboard\DashboardAutoInvest@orders")->name('orders');
        Route::get('/eventos', "Dashboard\DashboardAutoInvest@events")->name('events');
    });
    
    Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
});

Route::group(['prefix' => 'api/v1/dashboard', 'middleware' => ['web', 'auth', 'role:super|admin']], function () {
    Route::post('inmuebles/subir_resumen/{property}', 'Dashboard\DashboardPropertyController@uploadResumenFile')->name('dashboard.api.inmuebles.resumen');
    Route::post('inmuebles/subir_estimacion/{property}', 'Dashboard\DashboardPropertyController@uploadEstimacionFile')->name('dashboard.api.inmuebles.estimacion');
    Route::get('inmuebles/property-evolution/{property}/{year?}', 'Dashboard\DashboardPropertyController@getEvolucionByYear')->name('dashboard.api.inmuebles.evolution-year');
    Route::post('inmuebles/subir_evolucion/{property}', 'Dashboard\DashboardPropertyController@uploadEvolucionFile')->name('dashboard.api.inmuebles.evolucion');
    Route::post('inmuebles/pagar_diferencia/{property}', 'Dashboard\DashboardPropertyController@payDiference')->name('dashboard.api.inmuebles.pagar-diferencia');
    Route::post('inmuebles/abonar_dividendos', 'Dashboard\RepartirDividendosController@pagarDivisas')->name('dashboard.api.inmuebles.evolucion.abonar');
    Route::get('inmuebles/abonar_dividendos/{property_id}/{year}/{mes}', 'Dashboard\RepartirDividendosController@listDivisasById')->name('dashboard.api.inmuebles.evolucion.abonar.preview');
    Route::post('/activar/pago_sin_confirmar', 'Dashboard\DashboardController@pagoSinValidar')->name('dashboard.api.pagosinvalidar');
});

/**
 * EXCEL
 */
Route::group(['middleware' => ['web', 'auth', 'role:super|admin']], function () {
    Route::get('/excel', 'PrescriptorExcelController@blade_view')->name('export.excel');
    Route::get('/excellence', 'PrescriptorExcelController@blade')->name('export.excel.blade');
});

//Api localizada mi inversión
Route::localizedGroup(function () {
    Route::group(['prefix' => 'api/v1/data/', 'middleware' => ['web', 'auth']], function () {
        Route::get('property/{id}', '\App\Http\Controllers\Api\PropertyApiController@info')
            ->name('property.info');
    });

    Route::transGet('/api/v1/translations', function () {
        return strings_to_js();
    });
});