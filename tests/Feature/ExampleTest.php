<?php

namespace Tests\Feature;

use App\Property;
use App\PropertyUser;
use Carbon\Carbon;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test5Porciento()
    {
        $property = Property::find(19);

        logger($property->nombre);

        foreach ($property->usersGroup as $user) {
            // logger($user->nombreCompleto.' Repartir 5%');
            // $relacion = PropertyUser::find($user->pivot->id);

            $inversiones = PropertyUser::where('property_id', $property->id)->where('user_id', $user->id)->get();
            $cantidadAbonarUsuario = 0;
            foreach ($inversiones as $inversion) {
                // logger('Inversión: '.$inversion->total);
                $eurosPorciento = ($inversion->total * 5) / 100;

                $diasPasadosDesdeInversion = $this->getDiasPasados($inversion);

                $cantidadAbonarUsuario += ($eurosPorciento * $diasPasadosDesdeInversion) / (365 + Carbon::now()->format('L'));
            }
            $old = number_format($cantidadAbonarUsuario, 2, '.', '');
            $cantidadAbonarUsuario = $cantidadAbonarUsuario - ($cantidadAbonarUsuario * 0.19);

            $cantidadAbonarUsuario = number_format($cantidadAbonarUsuario, 2, '.', '');
            logger($user->nombreCompleto.' - Total 5%: '.$old.'€ - Total 5% sin retenciones 19%: '.$cantidadAbonarUsuario);
        }
    }

    /**
     * @return mixed
     */
    private function getDiasPasados(PropertyUser $inversion)
    {
        $diaInversion = $inversion->created_at;
        $hoy = Carbon::now();

        return $hoy->diffInDays($diaInversion);
    }
}
