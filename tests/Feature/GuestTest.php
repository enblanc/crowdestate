<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class GuestTest extends TestCase
{
    /**
     * Comprueba que la cookie dada sea igual a la obtenida
     *
     * @return void
     */
    public function testHasCookie()
    {
        $this->call('GET', 'set-cookie');

        $randomUser = User::get()->random()->first();
 
        $response = $this->get($randomUser->affiliateUrl);

        $response->assertCookie('referral', $randomUser->affiliate_id);
    }
}
