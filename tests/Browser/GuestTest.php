<?php

namespace Tests\Browser;

use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Laravel\Dusk\Browser;
use Tests\DuskTestCase;

class GuestTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $user = User::get()->random()->first();

        $this->browse(function (Browser $browser) use ($user) {
            $browser->visit($user->affiliateUrl)
                    ->assertPathIs('/register')
                    ->visit('/')
                    ->assertCookieValue('referral', $user->affiliate_id);
        });
    }
}
