<?php

/**
 * Infinety-es Crud Config File
 * 20160504.
 */

return [

    'middleware'        => ['web', 'auth', 'role:admin|super'],

    'locale-model'      => Infinety\CRUD\Models\Locale::class,

    'crud-route-prefix' => 'dashboard',

    'assets_folder'     => 'dashboard_theme', //Admin public assets folder

];
