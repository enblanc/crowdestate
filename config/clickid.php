<?php

    /*
    |--------------------------------------------------------------------------
    | Application ClickID
    |--------------------------------------------------------------------------
    | @author Carlos Anselmi <carlos@infinety.es, anselmi.dev@gmail.com>
    | Aquí se configura las opciones de los ADS de cada proveedor
    | Los nombre que se le coloca a las COOKIEs y sus CALLBACK
    |
    */
    return [

        //  Nombre de la cookie pora click_id.
        'cookie-click-id'  => env('APP_PREX_CLICK_ID', 'brickstarter_click_id'),
        
        //  Nombre de la cookie para el provider de click_id.
        'cookie-provider'  => env('APP_PREX_CLICK_ID', 'brickstarter_provider'),

        // Listado de proveedores permitidos.
        'providers' =>  [
            'doaff' =>  [
                'endpoint'  =>  [
                    'register' => 'https://tracker2.doaffiliate.net/api/brickstarter-com?type=CPL&lead=user_id&v=click_id',
                    'first_payment'  =>  'https://tracker2.doaffiliate.net/api/brickstarter-com?type=CPS&lead=user_id&v=click_id&totalCost=mount'
                ]
            ],
            'financeads'  =>  [
                'endpoint'  =>  [
                    'register'   =>  'https://www.financeads.net/tl.php?p=3762&oid=R_user_id&ocategory=lead&s_id=click_id',
                    'sale'  =>  'https://www.financeads.net/tl.php?p=3762&oid=order_id&ocategory=sale&s_id=click_id',
                    'saleOff' =>  'https://www.financeads.net/tl.php?p=3762&oid=s_order_id&ocategory=sale_off&s_id=click_id&ovalue=mount'
                ]
            ]
        ]
    ];
