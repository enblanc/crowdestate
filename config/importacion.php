<?php

return [
    'sheets' => [
        'resumen'    => [
            'periodo'                     => 'B1',
            'horizonte'                   => 'B2',
            'valor_mercado'               => 'B3',
            'valor_compra'                => 'B4',
            'coste_total'                 => 'B5',
            'hipoteca'                    => 'B6',
            'objetivo'                    => 'B7',
            'importe_minimo'              => 'B8',
            'rentabilidad_alquiler_bruta' => 'B9',
            'rentabilidad_alquiler_neta'  => 'B10',
            'rentabilidad_objetivo_bruta' => 'B11',
            'rentabilidad_objetivo_neta'  => 'B12',
            'rentabilidad_anual'          => 'B13',
            'rentabilidad_total'          => 'B14',
            'tasa_interna_rentabilidad'   => 'B15',
            'coste_obras'                 => 'B18',
            'impuestos_tramites'          => 'B19',
            'registro_sociedad'           => 'B20',
            'otros_costes'                => 'B21',
            'imprevistos'                 => 'B22',
            'caja_minima'                 => 'B23',
            'honorarios'                  => 'B24',
            'ingresos_anuales_alquiler'   => 'B25',
            'importe_venta'               => 'B26',
            'compra_venta_margen_bruto'   => 'B27',
        ],

        'estimacion' => [
            'numero_reservas'                => 'B2',
            'grado_ocupacion'                => 'B3',
            'dias_reservados'                => 'B4',
            'dias_no_reservados'             => 'B5',
            'dias_bloqueados'                => 'B6',
            'tarifa_media_dia'               => 'B7',
            'estimacion_ingresos_anuales'    => 'B8',
            'satisfaccion_clientes'          => 'B9',
            'numero_invitados'               => 'B10',
            'ingresos_totales'               => 'B13',
            'ingresos_alquiler'              => 'B14',
            'ingresos_venta'                 => 'B15',
            'otros_ingresos'                 => 'B16',
            'gastos_explotacion'             => 'B17',
            'publicidad_prensa'              => 'B18',
            'otro_material'                  => 'B19',
            'gastos_ventas'                  => 'B20',
            'i_mas_d'                        => 'B21',
            'arrendamientos'                 => 'B22',
            'comisiones_crowdestate'         => 'B23',
            'gestion'                        => 'B24',
            'suministros'                    => 'B25',
            'ibi'                            => 'B26',
            'otras_tasas'                    => 'B27',
            'amortizacion'                   => 'B28',
            'subvenciones'                   => 'B29',
            'enajenacion_inmovilizado'       => 'B30',
            'resultado_explotacion'          => 'B31',
            'resultado_financiero'           => 'B32',
            'ingresos_financieros'           => 'B33',
            'gastos_financieros'             => 'B34',
            'otros_instrumentos_financieros' => 'B35',
            'resultado_antes_impuestos'      => 'B36',
            'impuestos'                      => 'B37',
            'resultados_ejercicio'           => 'B38',
            'dividendos_generados'           => 'B41',
            'dividendos_abonar'              => 'B42',
            'total_dividendos_abonados'      => 'B43',
            'dividendos_cartera'             => 'B44',
            'total_dividendos_devengados'    => 'B45',
            'revalorizacion_inmueble'        => 'B48',
            'valor_actual_inmueble'          => 'B49',
        ],

        'evolucion'  => [
            'enero_numero_reservas'                     => 'B2',
            'enero_grado_ocupacion'                     => 'B3',
            'enero_dias_reservados'                     => 'B4',
            'enero_dias_no_reservados'                  => 'B5',
            'enero_dias_bloqueados'                     => 'B6',
            'enero_tarifa_media_dia'                    => 'B7',
            'enero_estimacion_ingresos_anuales'         => 'B8',
            'enero_satisfaccion_clientes'               => 'B9',
            'enero_numero_invitados'                    => 'B10',
            'enero_ingresos_totales'                    => 'B13',
            'enero_ingresos_alquiler'                   => 'B14',
            'enero_ingresos_venta'                      => 'B15',
            'enero_otros_ingresos'                      => 'B16',
            'enero_gastos_explotacion'                  => 'B17',
            'enero_publicidad_prensa'                   => 'B18',
            'enero_otro_material'                       => 'B19',
            'enero_gastos_ventas'                       => 'B20',
            'enero_i_mas_d'                             => 'B21',
            'enero_arrendamientos'                      => 'B22',
            'enero_comisiones_crowdestate'              => 'B23',
            'enero_gestion'                             => 'B24',
            'enero_suministros'                         => 'B25',
            'enero_ibi'                                 => 'B26',
            'enero_otras_tasas'                         => 'B27',
            'enero_amortizacion'                        => 'B28',
            'enero_subvenciones'                        => 'B29',
            'enero_enajenacion_inmovilizado'            => 'B30',
            'enero_resultado_explotacion'               => 'B31',
            'enero_resultado_financiero'                => 'B32',
            'enero_ingresos_financieros'                => 'B33',
            'enero_gastos_financieros'                  => 'B34',
            'enero_otros_instrumentos_financieros'      => 'B35',
            'enero_resultado_antes_impuestos'           => 'B36',
            'enero_impuestos'                           => 'B37',
            'enero_resultados_ejercicio'                => 'B38',
            'enero_dividendos_generados'                => 'B41',
            'enero_dividendos_abonar'                   => 'B42',
            'enero_total_dividendos_abonados'           => 'B43',
            'enero_dividendos_cartera'                  => 'B44',
            'enero_total_dividendos_devengados'         => 'B45',
            'enero_revalorizacion_inmueble'             => 'B48',
            'enero_valor_actual_inmueble'               => 'B49',
            'febrero_numero_reservas'                   => 'C2',
            'febrero_grado_ocupacion'                   => 'C3',
            'febrero_dias_reservados'                   => 'C4',
            'febrero_dias_no_reservados'                => 'C5',
            'febrero_dias_bloqueados'                   => 'C6',
            'febrero_tarifa_media_dia'                  => 'C7',
            'febrero_estimacion_ingresos_anuales'       => 'C8',
            'febrero_satisfaccion_clientes'             => 'C9',
            'febrero_numero_invitados'                  => 'C10',
            'febrero_ingresos_totales'                  => 'C13',
            'febrero_ingresos_alquiler'                 => 'C14',
            'febrero_ingresos_venta'                    => 'C15',
            'febrero_otros_ingresos'                    => 'C16',
            'febrero_gastos_explotacion'                => 'C17',
            'febrero_publicidad_prensa'                 => 'C18',
            'febrero_otro_material'                     => 'C19',
            'febrero_gastos_ventas'                     => 'C20',
            'febrero_i_mas_d'                           => 'C21',
            'febrero_arrendamientos'                    => 'C22',
            'febrero_comisiones_crowdestate'            => 'C23',
            'febrero_gestion'                           => 'C24',
            'febrero_suministros'                       => 'C25',
            'febrero_ibi'                               => 'C26',
            'febrero_otras_tasas'                       => 'C27',
            'febrero_amortizacion'                      => 'C28',
            'febrero_subvenciones'                      => 'C29',
            'febrero_enajenacion_inmovilizado'          => 'C30',
            'febrero_resultado_explotacion'             => 'C31',
            'febrero_resultado_financiero'              => 'C32',
            'febrero_ingresos_financieros'              => 'C33',
            'febrero_gastos_financieros'                => 'C34',
            'febrero_otros_instrumentos_financieros'    => 'C35',
            'febrero_resultado_antes_impuestos'         => 'C36',
            'febrero_impuestos'                         => 'C37',
            'febrero_resultados_ejercicio'              => 'C38',
            'febrero_dividendos_generados'              => 'C41',
            'febrero_dividendos_abonar'                 => 'C42',
            'febrero_total_dividendos_abonados'         => 'C43',
            'febrero_dividendos_cartera'                => 'C44',
            'febrero_total_dividendos_devengados'       => 'C45',
            'febrero_revalorizacion_inmueble'           => 'C48',
            'febrero_valor_actual_inmueble'             => 'C49',
            'marzo_numero_reservas'                     => 'D2',
            'marzo_grado_ocupacion'                     => 'D3',
            'marzo_dias_reservados'                     => 'D4',
            'marzo_dias_no_reservados'                  => 'D5',
            'marzo_dias_bloqueados'                     => 'D6',
            'marzo_tarifa_media_dia'                    => 'D7',
            'marzo_estimacion_ingresos_anuales'         => 'D8',
            'marzo_satisfaccion_clientes'               => 'D9',
            'marzo_numero_invitados'                    => 'D10',
            'marzo_ingresos_totales'                    => 'D13',
            'marzo_ingresos_alquiler'                   => 'D14',
            'marzo_ingresos_venta'                      => 'D15',
            'marzo_otros_ingresos'                      => 'D16',
            'marzo_gastos_explotacion'                  => 'D17',
            'marzo_publicidad_prensa'                   => 'D18',
            'marzo_otro_material'                       => 'D19',
            'marzo_gastos_ventas'                       => 'D20',
            'marzo_i_mas_d'                             => 'D21',
            'marzo_arrendamientos'                      => 'D22',
            'marzo_comisiones_crowdestate'              => 'D23',
            'marzo_gestion'                             => 'D24',
            'marzo_suministros'                         => 'D25',
            'marzo_ibi'                                 => 'D26',
            'marzo_otras_tasas'                         => 'D27',
            'marzo_amortizacion'                        => 'D28',
            'marzo_subvenciones'                        => 'D29',
            'marzo_enajenacion_inmovilizado'            => 'D30',
            'marzo_resultado_explotacion'               => 'D31',
            'marzo_resultado_financiero'                => 'D32',
            'marzo_ingresos_financieros'                => 'D33',
            'marzo_gastos_financieros'                  => 'D34',
            'marzo_otros_instrumentos_financieros'      => 'D35',
            'marzo_resultado_antes_impuestos'           => 'D36',
            'marzo_impuestos'                           => 'D37',
            'marzo_resultados_ejercicio'                => 'D38',
            'marzo_dividendos_generados'                => 'D41',
            'marzo_dividendos_abonar'                   => 'D42',
            'marzo_total_dividendos_abonados'           => 'D43',
            'marzo_dividendos_cartera'                  => 'D44',
            'marzo_total_dividendos_devengados'         => 'D45',
            'marzo_revalorizacion_inmueble'             => 'D48',
            'marzo_valor_actual_inmueble'               => 'D49',
            'abril_numero_reservas'                     => 'E2',
            'abril_grado_ocupacion'                     => 'E3',
            'abril_dias_reservados'                     => 'E4',
            'abril_dias_no_reservados'                  => 'E5',
            'abril_dias_bloqueados'                     => 'E6',
            'abril_tarifa_media_dia'                    => 'E7',
            'abril_estimacion_ingresos_anuales'         => 'E8',
            'abril_satisfaccion_clientes'               => 'E9',
            'abril_numero_invitados'                    => 'E10',
            'abril_ingresos_totales'                    => 'E13',
            'abril_ingresos_alquiler'                   => 'E14',
            'abril_ingresos_venta'                      => 'E15',
            'abril_otros_ingresos'                      => 'E16',
            'abril_gastos_explotacion'                  => 'E17',
            'abril_publicidad_prensa'                   => 'E18',
            'abril_otro_material'                       => 'E19',
            'abril_gastos_ventas'                       => 'E20',
            'abril_i_mas_d'                             => 'E21',
            'abril_arrendamientos'                      => 'E22',
            'abril_comisiones_crowdestate'              => 'E23',
            'abril_gestion'                             => 'E24',
            'abril_suministros'                         => 'E25',
            'abril_ibi'                                 => 'E26',
            'abril_otras_tasas'                         => 'E27',
            'abril_amortizacion'                        => 'E28',
            'abril_subvenciones'                        => 'E29',
            'abril_enajenacion_inmovilizado'            => 'E30',
            'abril_resultado_explotacion'               => 'E31',
            'abril_resultado_financiero'                => 'E32',
            'abril_ingresos_financieros'                => 'E33',
            'abril_gastos_financieros'                  => 'E34',
            'abril_otros_instrumentos_financieros'      => 'E35',
            'abril_resultado_antes_impuestos'           => 'E36',
            'abril_impuestos'                           => 'E37',
            'abril_resultados_ejercicio'                => 'E38',
            'abril_dividendos_generados'                => 'E41',
            'abril_dividendos_abonar'                   => 'E42',
            'abril_total_dividendos_abonados'           => 'E43',
            'abril_dividendos_cartera'                  => 'E44',
            'abril_total_dividendos_devengados'         => 'E45',
            'abril_revalorizacion_inmueble'             => 'E48',
            'abril_valor_actual_inmueble'               => 'E49',
            'mayo_numero_reservas'                      => 'F2',
            'mayo_grado_ocupacion'                      => 'F3',
            'mayo_dias_reservados'                      => 'F4',
            'mayo_dias_no_reservados'                   => 'F5',
            'mayo_dias_bloqueados'                      => 'F6',
            'mayo_tarifa_media_dia'                     => 'F7',
            'mayo_estimacion_ingresos_anuales'          => 'F8',
            'mayo_satisfaccion_clientes'                => 'F9',
            'mayo_numero_invitados'                     => 'F10',
            'mayo_ingresos_totales'                     => 'F13',
            'mayo_ingresos_alquiler'                    => 'F14',
            'mayo_ingresos_venta'                       => 'F15',
            'mayo_otros_ingresos'                       => 'F16',
            'mayo_gastos_explotacion'                   => 'F17',
            'mayo_publicidad_prensa'                    => 'F18',
            'mayo_otro_material'                        => 'F19',
            'mayo_gastos_ventas'                        => 'F20',
            'mayo_i_mas_d'                              => 'F21',
            'mayo_arrendamientos'                       => 'F22',
            'mayo_comisiones_crowdestate'               => 'F23',
            'mayo_gestion'                              => 'F24',
            'mayo_suministros'                          => 'F25',
            'mayo_ibi'                                  => 'F26',
            'mayo_otras_tasas'                          => 'F27',
            'mayo_amortizacion'                         => 'F28',
            'mayo_subvenciones'                         => 'F29',
            'mayo_enajenacion_inmovilizado'             => 'F30',
            'mayo_resultado_explotacion'                => 'F31',
            'mayo_resultado_financiero'                 => 'F32',
            'mayo_ingresos_financieros'                 => 'F33',
            'mayo_gastos_financieros'                   => 'F34',
            'mayo_otros_instrumentos_financieros'       => 'F35',
            'mayo_resultado_antes_impuestos'            => 'F36',
            'mayo_impuestos'                            => 'F37',
            'mayo_resultados_ejercicio'                 => 'F38',
            'mayo_dividendos_generados'                 => 'F41',
            'mayo_dividendos_abonar'                    => 'F42',
            'mayo_total_dividendos_abonados'            => 'F43',
            'mayo_dividendos_cartera'                   => 'F44',
            'mayo_total_dividendos_devengados'          => 'F45',
            'mayo_revalorizacion_inmueble'              => 'F48',
            'mayo_valor_actual_inmueble'                => 'F49',
            'junio_numero_reservas'                     => 'G2',
            'junio_grado_ocupacion'                     => 'G3',
            'junio_dias_reservados'                     => 'G4',
            'junio_dias_no_reservados'                  => 'G5',
            'junio_dias_bloqueados'                     => 'G6',
            'junio_tarifa_media_dia'                    => 'G7',
            'junio_estimacion_ingresos_anuales'         => 'G8',
            'junio_satisfaccion_clientes'               => 'G9',
            'junio_numero_invitados'                    => 'G10',
            'junio_ingresos_totales'                    => 'G13',
            'junio_ingresos_alquiler'                   => 'G14',
            'junio_ingresos_venta'                      => 'G15',
            'junio_otros_ingresos'                      => 'G16',
            'junio_gastos_explotacion'                  => 'G17',
            'junio_publicidad_prensa'                   => 'G18',
            'junio_otro_material'                       => 'G19',
            'junio_gastos_ventas'                       => 'G20',
            'junio_i_mas_d'                             => 'G21',
            'junio_arrendamientos'                      => 'G22',
            'junio_comisiones_crowdestate'              => 'G23',
            'junio_gestion'                             => 'G24',
            'junio_suministros'                         => 'G25',
            'junio_ibi'                                 => 'G26',
            'junio_otras_tasas'                         => 'G27',
            'junio_amortizacion'                        => 'G28',
            'junio_subvenciones'                        => 'G29',
            'junio_enajenacion_inmovilizado'            => 'G30',
            'junio_resultado_explotacion'               => 'G31',
            'junio_resultado_financiero'                => 'G32',
            'junio_ingresos_financieros'                => 'G33',
            'junio_gastos_financieros'                  => 'G34',
            'junio_otros_instrumentos_financieros'      => 'G35',
            'junio_resultado_antes_impuestos'           => 'G36',
            'junio_impuestos'                           => 'G37',
            'junio_resultados_ejercicio'                => 'G38',
            'junio_dividendos_generados'                => 'G41',
            'junio_dividendos_abonar'                   => 'G42',
            'junio_total_dividendos_abonados'           => 'G43',
            'junio_dividendos_cartera'                  => 'G44',
            'junio_total_dividendos_devengados'         => 'G45',
            'junio_revalorizacion_inmueble'             => 'G48',
            'junio_valor_actual_inmueble'               => 'G49',
            'julio_numero_reservas'                     => 'H2',
            'julio_grado_ocupacion'                     => 'H3',
            'julio_dias_reservados'                     => 'H4',
            'julio_dias_no_reservados'                  => 'H5',
            'julio_dias_bloqueados'                     => 'H6',
            'julio_tarifa_media_dia'                    => 'H7',
            'julio_estimacion_ingresos_anuales'         => 'H8',
            'julio_satisfaccion_clientes'               => 'H9',
            'julio_numero_invitados'                    => 'H10',
            'julio_ingresos_totales'                    => 'H13',
            'julio_ingresos_alquiler'                   => 'H14',
            'julio_ingresos_venta'                      => 'H15',
            'julio_otros_ingresos'                      => 'H16',
            'julio_gastos_explotacion'                  => 'H17',
            'julio_publicidad_prensa'                   => 'H18',
            'julio_otro_material'                       => 'H19',
            'julio_gastos_ventas'                       => 'H20',
            'julio_i_mas_d'                             => 'H21',
            'julio_arrendamientos'                      => 'H22',
            'julio_comisiones_crowdestate'              => 'H23',
            'julio_gestion'                             => 'H24',
            'julio_suministros'                         => 'H25',
            'julio_ibi'                                 => 'H26',
            'julio_otras_tasas'                         => 'H27',
            'julio_amortizacion'                        => 'H28',
            'julio_subvenciones'                        => 'H29',
            'julio_enajenacion_inmovilizado'            => 'H30',
            'julio_resultado_explotacion'               => 'H31',
            'julio_resultado_financiero'                => 'H32',
            'julio_ingresos_financieros'                => 'H33',
            'julio_gastos_financieros'                  => 'H34',
            'julio_otros_instrumentos_financieros'      => 'H35',
            'julio_resultado_antes_impuestos'           => 'H36',
            'julio_impuestos'                           => 'H37',
            'julio_resultados_ejercicio'                => 'H38',
            'julio_dividendos_generados'                => 'H41',
            'julio_dividendos_abonar'                   => 'H42',
            'julio_total_dividendos_abonados'           => 'H43',
            'julio_dividendos_cartera'                  => 'H44',
            'julio_total_dividendos_devengados'         => 'H45',
            'julio_revalorizacion_inmueble'             => 'H48',
            'julio_valor_actual_inmueble'               => 'H49',
            'agosto_numero_reservas'                    => 'I2',
            'agosto_grado_ocupacion'                    => 'I3',
            'agosto_dias_reservados'                    => 'I4',
            'agosto_dias_no_reservados'                 => 'I5',
            'agosto_dias_bloqueados'                    => 'I6',
            'agosto_tarifa_media_dia'                   => 'I7',
            'agosto_estimacion_ingresos_anuales'        => 'I8',
            'agosto_satisfaccion_clientes'              => 'I9',
            'agosto_numero_invitados'                   => 'I10',
            'agosto_ingresos_totales'                   => 'I13',
            'agosto_ingresos_alquiler'                  => 'I14',
            'agosto_ingresos_venta'                     => 'I15',
            'agosto_otros_ingresos'                     => 'I16',
            'agosto_gastos_explotacion'                 => 'I17',
            'agosto_publicidad_prensa'                  => 'I18',
            'agosto_otro_material'                      => 'I19',
            'agosto_gastos_ventas'                      => 'I20',
            'agosto_i_mas_d'                            => 'I21',
            'agosto_arrendamientos'                     => 'I22',
            'agosto_comisiones_crowdestate'             => 'I23',
            'agosto_gestion'                            => 'I24',
            'agosto_suministros'                        => 'I25',
            'agosto_ibi'                                => 'I26',
            'agosto_otras_tasas'                        => 'I27',
            'agosto_amortizacion'                       => 'I28',
            'agosto_subvenciones'                       => 'I29',
            'agosto_enajenacion_inmovilizado'           => 'I30',
            'agosto_resultado_explotacion'              => 'I31',
            'agosto_resultado_financiero'               => 'I32',
            'agosto_ingresos_financieros'               => 'I33',
            'agosto_gastos_financieros'                 => 'I34',
            'agosto_otros_instrumentos_financieros'     => 'I35',
            'agosto_resultado_antes_impuestos'          => 'I36',
            'agosto_impuestos'                          => 'I37',
            'agosto_resultados_ejercicio'               => 'I38',
            'agosto_dividendos_generados'               => 'I41',
            'agosto_dividendos_abonar'                  => 'I42',
            'agosto_total_dividendos_abonados'          => 'I43',
            'agosto_dividendos_cartera'                 => 'I44',
            'agosto_total_dividendos_devengados'        => 'I45',
            'agosto_revalorizacion_inmueble'            => 'I48',
            'agosto_valor_actual_inmueble'              => 'I49',
            'septiembre_numero_reservas'                => 'J2',
            'septiembre_grado_ocupacion'                => 'J3',
            'septiembre_dias_reservados'                => 'J4',
            'septiembre_dias_no_reservados'             => 'J5',
            'septiembre_dias_bloqueados'                => 'J6',
            'septiembre_tarifa_media_dia'               => 'J7',
            'septiembre_estimacion_ingresos_anuales'    => 'J8',
            'septiembre_satisfaccion_clientes'          => 'J9',
            'septiembre_numero_invitados'               => 'J10',
            'septiembre_ingresos_totales'               => 'J13',
            'septiembre_ingresos_alquiler'              => 'J14',
            'septiembre_ingresos_venta'                 => 'J15',
            'septiembre_otros_ingresos'                 => 'J16',
            'septiembre_gastos_explotacion'             => 'J17',
            'septiembre_publicidad_prensa'              => 'J18',
            'septiembre_otro_material'                  => 'J19',
            'septiembre_gastos_ventas'                  => 'J20',
            'septiembre_i_mas_d'                        => 'J21',
            'septiembre_arrendamientos'                 => 'J22',
            'septiembre_comisiones_crowdestate'         => 'J23',
            'septiembre_gestion'                        => 'J24',
            'septiembre_suministros'                    => 'J25',
            'septiembre_ibi'                            => 'J26',
            'septiembre_otras_tasas'                    => 'J27',
            'septiembre_amortizacion'                   => 'J28',
            'septiembre_subvenciones'                   => 'J29',
            'septiembre_enajenacion_inmovilizado'       => 'J30',
            'septiembre_resultado_explotacion'          => 'J31',
            'septiembre_resultado_financiero'           => 'J32',
            'septiembre_ingresos_financieros'           => 'J33',
            'septiembre_gastos_financieros'             => 'J34',
            'septiembre_otros_instrumentos_financieros' => 'J35',
            'septiembre_resultado_antes_impuestos'      => 'J36',
            'septiembre_impuestos'                      => 'J37',
            'septiembre_resultados_ejercicio'           => 'J38',
            'septiembre_dividendos_generados'           => 'J41',
            'septiembre_dividendos_abonar'              => 'J42',
            'septiembre_total_dividendos_abonados'      => 'J43',
            'septiembre_dividendos_cartera'             => 'J44',
            'septiembre_total_dividendos_devengados'    => 'J45',
            'septiembre_revalorizacion_inmueble'        => 'J48',
            'septiembre_valor_actual_inmueble'          => 'J49',
            'octubre_numero_reservas'                   => 'K2',
            'octubre_grado_ocupacion'                   => 'K3',
            'octubre_dias_reservados'                   => 'K4',
            'octubre_dias_no_reservados'                => 'K5',
            'octubre_dias_bloqueados'                   => 'K6',
            'octubre_tarifa_media_dia'                  => 'K7',
            'octubre_estimacion_ingresos_anuales'       => 'K8',
            'octubre_satisfaccion_clientes'             => 'K9',
            'octubre_numero_invitados'                  => 'K10',
            'octubre_ingresos_totales'                  => 'K13',
            'octubre_ingresos_alquiler'                 => 'K14',
            'octubre_ingresos_venta'                    => 'K15',
            'octubre_otros_ingresos'                    => 'K16',
            'octubre_gastos_explotacion'                => 'K17',
            'octubre_publicidad_prensa'                 => 'K18',
            'octubre_otro_material'                     => 'K19',
            'octubre_gastos_ventas'                     => 'K20',
            'octubre_i_mas_d'                           => 'K21',
            'octubre_arrendamientos'                    => 'K22',
            'octubre_comisiones_crowdestate'            => 'K23',
            'octubre_gestion'                           => 'K24',
            'octubre_suministros'                       => 'K25',
            'octubre_ibi'                               => 'K26',
            'octubre_otras_tasas'                       => 'K27',
            'octubre_amortizacion'                      => 'K28',
            'octubre_subvenciones'                      => 'K29',
            'octubre_enajenacion_inmovilizado'          => 'K30',
            'octubre_resultado_explotacion'             => 'K31',
            'octubre_resultado_financiero'              => 'K32',
            'octubre_ingresos_financieros'              => 'K33',
            'octubre_gastos_financieros'                => 'K34',
            'octubre_otros_instrumentos_financieros'    => 'K35',
            'octubre_resultado_antes_impuestos'         => 'K36',
            'octubre_impuestos'                         => 'K37',
            'octubre_resultados_ejercicio'              => 'K38',
            'octubre_dividendos_generados'              => 'K41',
            'octubre_dividendos_abonar'                 => 'K42',
            'octubre_total_dividendos_abonados'         => 'K43',
            'octubre_dividendos_cartera'                => 'K44',
            'octubre_total_dividendos_devengados'       => 'K45',
            'octubre_revalorizacion_inmueble'           => 'K48',
            'octubre_valor_actual_inmueble'             => 'K49',
            'noviembre_numero_reservas'                 => 'L2',
            'noviembre_grado_ocupacion'                 => 'L3',
            'noviembre_dias_reservados'                 => 'L4',
            'noviembre_dias_no_reservados'              => 'L5',
            'noviembre_dias_bloqueados'                 => 'L6',
            'noviembre_tarifa_media_dia'                => 'L7',
            'noviembre_estimacion_ingresos_anuales'     => 'L8',
            'noviembre_satisfaccion_clientes'           => 'L9',
            'noviembre_numero_invitados'                => 'L10',
            'noviembre_ingresos_totales'                => 'L13',
            'noviembre_ingresos_alquiler'               => 'L14',
            'noviembre_ingresos_venta'                  => 'L15',
            'noviembre_otros_ingresos'                  => 'L16',
            'noviembre_gastos_explotacion'              => 'L17',
            'noviembre_publicidad_prensa'               => 'L18',
            'noviembre_otro_material'                   => 'L19',
            'noviembre_gastos_ventas'                   => 'L20',
            'noviembre_i_mas_d'                         => 'L21',
            'noviembre_arrendamientos'                  => 'L22',
            'noviembre_comisiones_crowdestate'          => 'L23',
            'noviembre_gestion'                         => 'L24',
            'noviembre_suministros'                     => 'L25',
            'noviembre_ibi'                             => 'L26',
            'noviembre_otras_tasas'                     => 'L27',
            'noviembre_amortizacion'                    => 'L28',
            'noviembre_subvenciones'                    => 'L29',
            'noviembre_enajenacion_inmovilizado'        => 'L30',
            'noviembre_resultado_explotacion'           => 'L31',
            'noviembre_resultado_financiero'            => 'L32',
            'noviembre_ingresos_financieros'            => 'L33',
            'noviembre_gastos_financieros'              => 'L34',
            'noviembre_otros_instrumentos_financieros'  => 'L35',
            'noviembre_resultado_antes_impuestos'       => 'L36',
            'noviembre_impuestos'                       => 'L37',
            'noviembre_resultados_ejercicio'            => 'L38',
            'noviembre_dividendos_generados'            => 'L41',
            'noviembre_dividendos_abonar'               => 'L42',
            'noviembre_total_dividendos_abonados'       => 'L43',
            'noviembre_dividendos_cartera'              => 'L44',
            'noviembre_total_dividendos_devengados'     => 'L45',
            'noviembre_revalorizacion_inmueble'         => 'L48',
            'noviembre_valor_actual_inmueble'           => 'L49',
            'diciembre_numero_reservas'                 => 'M2',
            'diciembre_grado_ocupacion'                 => 'M3',
            'diciembre_dias_reservados'                 => 'M4',
            'diciembre_dias_no_reservados'              => 'M5',
            'diciembre_dias_bloqueados'                 => 'M6',
            'diciembre_tarifa_media_dia'                => 'M7',
            'diciembre_estimacion_ingresos_anuales'     => 'M8',
            'diciembre_satisfaccion_clientes'           => 'M9',
            'diciembre_numero_invitados'                => 'M10',
            'diciembre_ingresos_totales'                => 'M13',
            'diciembre_ingresos_alquiler'               => 'M14',
            'diciembre_ingresos_venta'                  => 'M15',
            'diciembre_otros_ingresos'                  => 'M16',
            'diciembre_gastos_explotacion'              => 'M17',
            'diciembre_publicidad_prensa'               => 'M18',
            'diciembre_otro_material'                   => 'M19',
            'diciembre_gastos_ventas'                   => 'M20',
            'diciembre_i_mas_d'                         => 'M21',
            'diciembre_arrendamientos'                  => 'M22',
            'diciembre_comisiones_crowdestate'          => 'M23',
            'diciembre_gestion'                         => 'M24',
            'diciembre_suministros'                     => 'M25',
            'diciembre_ibi'                             => 'M26',
            'diciembre_otras_tasas'                     => 'M27',
            'diciembre_amortizacion'                    => 'M28',
            'diciembre_subvenciones'                    => 'M29',
            'diciembre_enajenacion_inmovilizado'        => 'M30',
            'diciembre_resultado_explotacion'           => 'M31',
            'diciembre_resultado_financiero'            => 'M32',
            'diciembre_ingresos_financieros'            => 'M33',
            'diciembre_gastos_financieros'              => 'M34',
            'diciembre_otros_instrumentos_financieros'  => 'M35',
            'diciembre_resultado_antes_impuestos'       => 'M36',
            'diciembre_impuestos'                       => 'M37',
            'diciembre_resultados_ejercicio'            => 'M38',
            'diciembre_dividendos_generados'            => 'M41',
            'diciembre_dividendos_abonar'               => 'M42',
            'diciembre_total_dividendos_abonados'       => 'M43',
            'diciembre_dividendos_cartera'              => 'M44',
            'diciembre_total_dividendos_devengados'     => 'M45',
            'diciembre_revalorizacion_inmueble'         => 'M48',
            'diciembre_valor_actual_inmueble'           => 'M49',
        ],
    ],
];
