<?php

return [

	'template_name' => 'Home',

    "fields" => [
                    [
                        'name' => 'name',
                        'label' => 'Titulo',
                        'type' => 'text',
                        'translate' => true,
                        'section'   => 'General'
                    ],
                    [
                        'name' => 'titulo_seo',
                        'label' => 'Titulo Seo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                    ],
                    [
                        'name' => 'descripcion_seo',
                        'label' => 'Descripción Seo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                    ],
                    [
                        'name' => 'cabecera_title',
                        'label' => 'Titulo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Cabecera'
                    ],
                    [
                        'name' => 'cabecera_intro',
                        'label' => 'Intro',
                        'type' => 'textarea',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Cabecera'
                    ],
                    [
                        'name' => 'cabecera_foto',
                        'label' => "Imagen Cabecera",
                        'type' => 'browse',
                        'fake' => true,
                        'store_in' => 'extras',
                        'usemedia' => true,
                        'section'   => 'Cabecera'
                    ],
                    [
                        'name' => 'porque_title',
                        'label' => 'Titulo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],
                    [
                        'name' => 'porque_text',
                        'label' => 'Intro Por qué Brickstarter',
                        'type' => 'textarea',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],
                    [
                        'name' => 'porque_1',
                        'label' => 'Razón 1',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],
                    [
                        'name' => 'porque_2',
                        'label' => 'Razón 2',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],

                    [
                        'name' => 'porque_3',
                        'label' => 'Razón 3',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],

                    [
                        'name' => 'porque_4',
                        'label' => 'Razón 4',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],

                    [
                        'name' => 'porque_5',
                        'label' => 'Razón 5',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],

                    [
                        'name' => 'porque_6',
                        'label' => 'Razón 6',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],

                    [
                        'name' => 'porque_boton',
                        'label' => 'Texto botón',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],
                    [
                        'name' => 'porque_boton_link',
                        'label' => 'Link botón',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque Por qué Brickstarter'
                    ],
                    [
                        'name' => 'title_rentables',
                        'label' => 'Titulo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque apartamentos rentables'
                    ],
                    [
                        'name' => 'intro_rentables',
                        'label' => 'Intro',
                        'type' => 'textarea',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque apartamentos rentables'
                    ],
                    [
                        'name' => 'title_enterate',
                        'label' => 'Titulo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque entérate de todo'
                    ],
                    [
                        'name' => 'intro_enterate',
                        'label' => 'Intro',
                        'type' => 'textarea',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Bloque entérate de todo'
                    ],


                ],

];