<?php

return [

	'template_name' => 'Legal',

    "fields" => [
                    [
                        'name' => 'name',
                        'label' => 'Titulo',
                        'type' => 'text',
                        'translate' => true,
                        'section'   => 'General'
                    ],
                    [
                        'name' => 'titulo_seo',
                        'label' => 'Titulo Seo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                    ],
                    [
                        'name' => 'descripcion_seo',
                        'label' => 'Descripción Seo',
                        'type' => 'text',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                    ],
                    [
                        'name' => 'contenido',
                        'label' => 'Intro',
                        'type' => 'redactor',
                        'translate' => true,
                        'fake'  => true,
                        'store_in' => 'extras',
                        'section'   => 'Contenido'
                    ]


                ],

];