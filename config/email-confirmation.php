<?php

return [
    'enabled'           => true,

    'route_prefix'      => 'confirm',

    'catch_unconfirmed' => true,

    'notifiable_class'  => \App\Notifications\ConfirmRegistrationNotification::class,
];
