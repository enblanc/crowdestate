<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Información propia de BrickStarter
    |--------------------------------------------------------------------------
    |
    | Aquí se establecerán las opciones del programa
    |
     */

    'pago_sin_confirmar' => true,

    'rutas'              => [
        //usuario

        'home'                              => 'routes.home',
        'login'                             => 'routes.user.acceso',
        'registro'                          => 'routes.user.registro',
        'recuperar-password'                => 'routes.user.recuperar',
        'registro-oauth'                    => 'routes.user.oauth.registro',
        'link-oauth'                        => 'routes.user.oauth.unir',
        'password-reset'                    => 'routes.user.nueva-pass',
        'emails-confirmar-cambio-email'     => 'routes.user.confirmar-email',
        'welcome'                           => 'routes.user.bienvenido',

        //Inmuebles
        'front-property-grid'               => 'routes.inmuebles.listado',
        'front-property-show'               => 'routes.inmuebles.single',

        //panel usuario
        'panel-user-basic-show'             => 'routes.panel.datos-basicos',
        'panel-user-password-show'          => 'routes.panel.cambiar-contrasena',
        'panel-user-accredit-show'          => 'routes.panel.acreditar-cuenta',
        'panel-user-invite'                 => 'routes.panel.invitar',
        'panel-user-money-show'             => 'routes.panel.agregar-fondos',
        'panel-user-transactions-show'      => 'routes.panel.movimientos',
        'panel-user-bankaccounts-show'      => 'routes.panel.cuentas-bancarias',
        'panel-user-moneyof-show'           => 'routes.panel.retirar-fondos',
        'panel-user-money-show'             => 'routes.panel.agregar-fondos',
        'panel-user-inversion-general'      => 'routes.panel.mis-inversiones',
        'panel-user-inversion-property'     => 'routes.panel.mis-inversiones-single',
        'panel-user-inversion-resumen'      => 'routes.panel.resumen-patrimonial',
        'panel-user-marketplace-historical' => 'routes.panel.mercado-historico',

        'contact'                           => 'routes.paginas.contacto',
        'prescriptores'                     => 'routes.paginas.prescriptores',
        'front-quienesSomos'                => 'routes.paginas.quienes-somos',
        'front-comoFunciona'                => 'routes.paginas.como-funciona',
        'front-faq-general'                 => 'routes.paginas.faq',
        'front-faq-category'                => 'routes.paginas.faq_categoria',

        'front-blog-index'                  => 'routes.blog.listado',
        'front-blog-show'                   => 'routes.blog.entrada',
        'front-blog-categories'             => 'routes.blog.categoria',

        'front-market-list'                 => 'routes.mercado.listado',
        'front-market-show'                 => 'routes.mercado.show',

        //páginas
    ],
];
